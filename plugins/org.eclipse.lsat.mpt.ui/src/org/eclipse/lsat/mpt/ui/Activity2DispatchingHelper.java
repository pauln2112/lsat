/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.ui;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.lsat.common.emf.ecore.resource.Persistor;
import org.eclipse.lsat.common.emf.ecore.resource.PersistorFactory;
import org.eclipse.lsat.common.emf.ecore.resource.ResourceSetUtil;
import org.eclipse.lsat.common.mpt.MaxPlusSpecification;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.mpt.transformation.Activity2Graph;
import org.eclipse.lsat.mpt.transformation.ExpandActivities;
import org.eclipse.lsat.mpt.transformation.ReduceActivityDispatching;
import org.eclipse.lsat.mpt.transformation.Specification2MptMatrix;
import org.eclipse.lsat.mpt.transformation.Specification2MptMatrixInput;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.service.OperationCanceledError;

import activity.ActivitySet;
import activity.util.Event2Resource;
import activity.util.PassiveClaimTransformer;
import dispatching.ActivityDispatching;
import machine.util.ImportsFlattener;
import setting.SettingUtil;
import setting.Settings;

public class Activity2DispatchingHelper {
    private static final String BULLET = "\u2022";

    private final ActivityTransformationOptions options;

    private ActivitySet activitySet;

    private URI dispatchingFileURI;

    private String dispatchingFile;

    private URI resultFolderURI;

    private boolean isExpanded;

    private ActivitySet orgActivitySet;

    private URI intermediateBaseURI;

    public Activity2DispatchingHelper(ActivityTransformationOptions options) {
        this.options = options;
    }

    protected MaxPlusSpecification createMaxPlusSpecification(IProgressMonitor monitor)
            throws IOException, QvtoTransformationException, CoreException
    {
        monitor.beginTask("Converting activity specification to max-plus model", 100);
        monitor.subTask("Converting " + options.getActivityFile().getName());

        PersistorFactory factory = new PersistorFactory();
        URI modelURI = URI.createPlatformResourceURI(options.getActivityFile().getFullPath().toString(), true);
        URI cifURI = URI.createPlatformResourceURI(options.getCifFile().getFullPath().toString(), true);

        IFolder saveIFolder = options.getActivityFile().getProject().getFolder("result");
        resultFolderURI = URI.createPlatformResourceURI(saveIFolder.getFullPath().toString(), true);

        String cifFileName = cifURI.trimFileExtension().lastSegment();
        String dispatchingFileName = cifFileName.concat(options.getDispatchingFileExtension());
        URI saveBaseURI = resultFolderURI.appendSegment(dispatchingFileName);
        intermediateBaseURI = resultFolderURI.appendSegment(".intermediate").appendSegment(dispatchingFileName);

        dispatchingFileURI = saveBaseURI.appendFileExtension("dispatching");
        dispatchingFile = dispatchingFileName + ".dispatching";

        Persistor<ActivitySet> activityPersistor = factory.getPersistor(ActivitySet.class);
        orgActivitySet = activityPersistor.loadOne(modelURI);
        Settings settings = SettingUtil.getSettings(orgActivitySet.eResource());

        // flatten the import containers to a single root per type and grab them again
        ResourceSet resourceSet = ImportsFlattener.flatten(intermediateBaseURI, orgActivitySet, settings);
        activitySet = ResourceSetUtil.getObjectByType(resourceSet, ActivitySet.class);
        settings = ResourceSetUtil.getObjectByType(resourceSet, Settings.class);

        monitor.subTask("Processing passive claims...");

        PassiveClaimTransformer.transform(activitySet);

        monitor.subTask("Expanding activities...");

        ExpandActivities.expand(activitySet);

        isExpanded = !EcoreUtil.equals(activitySet, orgActivitySet);

        if (isExpanded) {
            ResourceSetUtil.saveResources(resourceSet, Collections.emptyMap());
            IFolder intermediateFolder = saveIFolder.getFolder(".intermediate");
            intermediateFolder.setHidden(true);

        }

        monitor.subTask("Preparing activity specification...");

        Activity2Graph activities2Graph = new Activity2Graph();
        activitySet = activities2Graph.transformModel(activitySet, monitor);

        monitor.worked(30);

        monitor.subTask("Adding CIF supervisor " + options.getCifFile().getName());

        URI cifSpecificationURI = URI.createPlatformResourceURI(options.getCifFile().getFullPath().toString(), true);
        Persistor<Specification> cifSpecificationPersistor = factory.getPersistor(Specification.class);
        Specification cifSpecification = cifSpecificationPersistor.loadOne(cifSpecificationURI);

        Event2Resource.replaceEventsWithClaimRelease(activitySet);

        Specification2MptMatrixInput mptMatrixInput = new Specification2MptMatrixInput(activitySet, settings,
                cifSpecification);
        Specification2MptMatrix specification2mptMatrix = new Specification2MptMatrix();
        return specification2mptMatrix.transformModel(mptMatrixInput, monitor);
    }

    protected void createDispatchingFile(CharSequence dispatchingContent) throws URISyntaxException, IOException {
        ExtensibleURIConverterImpl convertor = new ExtensibleURIConverterImpl();
        if (isExpanded) {
            URI expanded = intermediateBaseURI.appendFileExtension(dispatchingFileURI.fileExtension());
            try (PrintWriter writer = new PrintWriter(convertor.createOutputStream(expanded))) {
                writer.write(dispatchingContent.toString());
            }
            PersistorFactory factory = new PersistorFactory();
            Persistor<ActivityDispatching> savePersistor = factory.getPersistor(ActivityDispatching.class);
            ActivityDispatching dispatching = savePersistor.loadOne(expanded);
            ReduceActivityDispatching.reduce(dispatching, orgActivitySet);
            SaveOptions options = SaveOptions.newBuilder().format().getOptions();
            savePersistor.save(dispatchingFileURI, options.toOptionsMap(), dispatching);
            validate(dispatching);
        } else {
            try (PrintWriter writer = new PrintWriter(convertor.createOutputStream(dispatchingFileURI))) {
                writer.write(dispatchingContent.toString());
            }
        }
    }

    private void validate(ActivityDispatching dispatching) throws OperationCanceledError, IOException {
        Diagnostic diagnostics = Diagnostician.INSTANCE.validate(dispatching);
        if (diagnostics.getSeverity() == Diagnostic.ERROR) {
            String msg = "\nGenerated dispatching file invalid:\n\n" + diagnostics.getChildren().stream()
                    .map(d -> "  " + BULLET + ' ' + d.getMessage()).collect(Collectors.joining("\n"));
            throw new IllegalArgumentException(msg);
        }
    }

    public ActivitySet getActivitySet() {
        return activitySet;
    }

    public URI getResultFolderURI() {
        return resultFolderURI;
    }

    public String getDispatchingFile() {
        return dispatchingFile;
    }

    public String round(Double d) {
        DecimalFormat df = new DecimalFormat("###.######");
        return df.format(d);
    }

    public static class SuccessDialog extends MessageDialog {
        public SuccessDialog(Shell parent, String title, String message) {
            super(parent, title, null, message, INFORMATION, new String[] {IDialogConstants.OK_LABEL}, 0);
        }

        @Override
        public Image getImage() {
            return null;
        }
    }
}
