/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import static org.eclipse.lsat.common.util.IteratorUtil.flatMap;
import static org.eclipse.lsat.common.util.IteratorUtil.join;

import java.util.Iterator;
import java.util.function.Function;

public class ClosureIterator<E> implements BranchIterator<E> {
    private final AppendableIterator<E> closureCandidates;

    private final UniqueIterator<E> closureResults;

    private boolean canPrune = false;

    public ClosureIterator(Iterator<? extends E> source, boolean includeSource,
            Function<E, Iterator<? extends E>> closureFunctor)
    {
        if (includeSource) {
            this.closureCandidates = new AppendableIterator<E>();
            // The first closure results are the source items theirself, concatenated (joined) with the closure
            // candidates
            this.closureResults = new UniqueIterator<E>(join(source, flatMap(closureCandidates, closureFunctor)));
        } else {
            // The initial candidates for collecting the closure are the source items, but during
            // the closure new candidates might be added, hence the AppendableIterator
            this.closureCandidates = new AppendableIterator<E>(source);
            // Now collect all closure results, but each closure result is also a closure
            // candidate. Using a an unique iterator in between (super class) will guard for
            // cycles and guarantee that each element is returned once only
            this.closureResults = new UniqueIterator<E>(flatMap(closureCandidates, closureFunctor));
        }
    }

    @Override
    public boolean hasNext() {
        canPrune = false;
        return closureResults.hasNext();
    }

    @Override
    public E next() {
        final E next = closureResults.next();
        closureCandidates.append(next);
        canPrune = true;
        return next;
    }

    @Override
    public void prune() {
        if (canPrune) {
            closureCandidates.undoAppend();
            canPrune = false;
        } else {
            throw new IllegalStateException("Prune is not allowed");
        }
    }
}
