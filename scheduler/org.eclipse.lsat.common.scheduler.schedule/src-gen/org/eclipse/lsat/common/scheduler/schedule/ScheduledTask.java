/**
 */
package org.eclipse.lsat.common.scheduler.schedule;

import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.common.scheduler.graph.Task;

import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scheduled Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getSequence <em>Sequence</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getDuration <em>Duration</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledTask()
 * @model
 * @generated
 */
public interface ScheduledTask<T extends Task> extends Node {
	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(BigDecimal)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledTask_StartTime()
	 * @model required="true"
	 * @generated
	 */
	BigDecimal getStartTime();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(BigDecimal)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledTask_EndTime()
	 * @model required="true"
	 * @generated
	 */
	BigDecimal getEndTime();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task</em>' reference.
	 * @see #setTask(Task)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledTask_Task()
	 * @model
	 * @generated
	 */
	T getTask();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getTask <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task</em>' reference.
	 * @see #getTask()
	 * @generated
	 */
	void setTask(T value);

	/**
	 * Returns the value of the '<em><b>Sequence</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getScheduledTasks <em>Scheduled Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sequence</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequence</em>' reference.
	 * @see #setSequence(Sequence)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledTask_Sequence()
	 * @see org.eclipse.lsat.common.scheduler.schedule.Sequence#getScheduledTasks
	 * @model opposite="scheduledTasks" required="true"
	 * @generated
	 */
	Sequence<?> getSequence();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getSequence <em>Sequence</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sequence</em>' reference.
	 * @see #getSequence()
	 * @generated
	 */
	void setSequence(Sequence<?> value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledTask_Duration()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	BigDecimal getDuration();

} // ScheduledTask
