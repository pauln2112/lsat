/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MPS</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPS()
 * @model
 * @generated
 */
public interface MPS extends Graph<MPSConfiguration, MPSTransition> {
} // MPS
