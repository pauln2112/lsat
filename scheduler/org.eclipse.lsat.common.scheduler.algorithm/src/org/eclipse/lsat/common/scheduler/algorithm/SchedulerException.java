/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.scheduler.algorithm;

public class SchedulerException extends Exception {
    private static final long serialVersionUID = 1L;

    public SchedulerException(CharSequence aDeveloperMessage) {
        super(String.valueOf(aDeveloperMessage));
    }

    public SchedulerException(CharSequence aDeveloperMessage, Throwable cause) {
        super(String.valueOf(aDeveloperMessage), cause);
    }
}
