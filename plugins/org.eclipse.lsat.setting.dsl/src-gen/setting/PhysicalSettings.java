/**
 */
package setting;

import machine.ActionType;
import machine.Axis;
import machine.HasResourcePeripheral;
import machine.IResource;
import machine.Peripheral;
import org.eclipse.emf.common.util.EMap;
import timing.Timing;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link setting.PhysicalSettings#getTimingSettings <em>Timing Settings</em>}</li>
 *   <li>{@link setting.PhysicalSettings#getMotionSettings <em>Motion Settings</em>}</li>
 *   <li>{@link setting.PhysicalSettings#getResource <em>Resource</em>}</li>
 *   <li>{@link setting.PhysicalSettings#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link setting.PhysicalSettings#getSettings <em>Settings</em>}</li>
 *   <li>{@link setting.PhysicalSettings#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see setting.SettingPackage#getPhysicalSettings()
 * @model
 * @generated
 */
public interface PhysicalSettings extends HasResourcePeripheral {
	/**
     * Returns the value of the '<em><b>Timing Settings</b></em>' map.
     * The key is of type {@link machine.ActionType},
     * and the value is of type {@link timing.Timing},
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timing Settings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Timing Settings</em>' map.
     * @see setting.SettingPackage#getPhysicalSettings_TimingSettings()
     * @model mapType="setting.TimingSettingsMapEntry&lt;machine.ActionType, timing.Timing&gt;"
     * @generated
     */
	EMap<ActionType, Timing> getTimingSettings();

	/**
     * Returns the value of the '<em><b>Motion Settings</b></em>' map.
     * The key is of type {@link machine.Axis},
     * and the value is of type {@link setting.MotionSettings},
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Motion Settings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Motion Settings</em>' map.
     * @see setting.SettingPackage#getPhysicalSettings_MotionSettings()
     * @model mapType="setting.MotionSettingsMapEntry&lt;machine.Axis, setting.MotionSettings&gt;"
     * @generated
     */
	EMap<Axis, MotionSettings> getMotionSettings();

	/**
     * Returns the value of the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Resource</em>' reference.
     * @see #setResource(IResource)
     * @see setting.SettingPackage#getPhysicalSettings_Resource()
     * @model required="true"
     * @generated
     */
	IResource getResource();

	/**
     * Sets the value of the '{@link setting.PhysicalSettings#getResource <em>Resource</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resource</em>' reference.
     * @see #getResource()
     * @generated
     */
	void setResource(IResource value);

	/**
     * Returns the value of the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripheral</em>' reference.
     * @see #setPeripheral(Peripheral)
     * @see setting.SettingPackage#getPhysicalSettings_Peripheral()
     * @model required="true"
     * @generated
     */
	Peripheral getPeripheral();

	/**
     * Sets the value of the '{@link setting.PhysicalSettings#getPeripheral <em>Peripheral</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Peripheral</em>' reference.
     * @see #getPeripheral()
     * @generated
     */
	void setPeripheral(Peripheral value);

	/**
     * Returns the value of the '<em><b>Settings</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Settings</em>' reference.
     * @see #setSettings(Settings)
     * @see setting.SettingPackage#getPhysicalSettings_Settings()
     * @model
     * @generated
     */
	Settings getSettings();

	/**
     * Sets the value of the '{@link setting.PhysicalSettings#getSettings <em>Settings</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Settings</em>' reference.
     * @see #getSettings()
     * @generated
     */
	void setSettings(Settings value);

	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see setting.SettingPackage#getPhysicalSettings_Name()
     * @model transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	String getName();

} // PhysicalSettings
