/**
 */
package org.eclipse.lsat.common.scheduler.schedule.impl;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.common.graph.directed.impl.DirectedGraphImpl;

import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.resources.ResourceModel;

import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.SchedulePackage;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.scheduler.schedule.Sequence;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Schedule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduleImpl#getResourceModel <em>Resource Model</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduleImpl#getSequences <em>Sequences</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduleImpl#isEpochTime <em>Epoch Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScheduleImpl<T extends Task> extends DirectedGraphImpl<ScheduledTask<T>, ScheduledDependency> implements Schedule<T> {
	/**
	 * The cached value of the '{@link #getResourceModel() <em>Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceModel()
	 * @generated
	 * @ordered
	 */
	protected ResourceModel resourceModel;

	/**
	 * The cached value of the '{@link #getSequences() <em>Sequences</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequences()
	 * @generated
	 * @ordered
	 */
	protected EList<Sequence<T>> sequences;

	/**
	 * The default value of the '{@link #isEpochTime() <em>Epoch Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEpochTime()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EPOCH_TIME_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEpochTime() <em>Epoch Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEpochTime()
	 * @generated
	 * @ordered
	 */
	protected boolean epochTime = EPOCH_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScheduleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SchedulePackage.Literals.SCHEDULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * This is specialized for the more specific element type known in this context.
	 * @generated
	 */
	@Override
	public EList<ScheduledDependency> getEdges() {
		if (edges == null) {
			edges = new EObjectContainmentWithInverseEList<ScheduledDependency>(ScheduledDependency.class, this, SchedulePackage.SCHEDULE__EDGES, DirectedGraphPackage.EDGE__GRAPH) { private static final long serialVersionUID = 1L; @Override public Class<?> getInverseFeatureClass() { return Edge.class; } };
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * This is specialized for the more specific element type known in this context.
	 * @generated
	 */
	@Override
	public EList<ScheduledTask<T>> getNodes() {
		if (nodes == null) {
			nodes = new EObjectContainmentWithInverseEList<ScheduledTask<T>>(ScheduledTask.class, this, SchedulePackage.SCHEDULE__NODES, DirectedGraphPackage.NODE__GRAPH) { private static final long serialVersionUID = 1L; @Override public Class<?> getInverseFeatureClass() { return Node.class; } };
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceModel getResourceModel() {
		if (resourceModel != null && resourceModel.eIsProxy()) {
			InternalEObject oldResourceModel = (InternalEObject)resourceModel;
			resourceModel = (ResourceModel)eResolveProxy(oldResourceModel);
			if (resourceModel != oldResourceModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SchedulePackage.SCHEDULE__RESOURCE_MODEL, oldResourceModel, resourceModel));
			}
		}
		return resourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceModel basicGetResourceModel() {
		return resourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setResourceModel(ResourceModel newResourceModel) {
		ResourceModel oldResourceModel = resourceModel;
		resourceModel = newResourceModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SchedulePackage.SCHEDULE__RESOURCE_MODEL, oldResourceModel, resourceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Sequence<T>> getSequences() {
		if (sequences == null) {
			sequences = new EObjectContainmentWithInverseEList<Sequence<T>>(Sequence.class, this, SchedulePackage.SCHEDULE__SEQUENCES, SchedulePackage.SEQUENCE__SCHEDULE);
		}
		return sequences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isEpochTime() {
		return epochTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEpochTime(boolean newEpochTime) {
		boolean oldEpochTime = epochTime;
		epochTime = newEpochTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SchedulePackage.SCHEDULE__EPOCH_TIME, oldEpochTime, epochTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SchedulePackage.SCHEDULE__SEQUENCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSequences()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SchedulePackage.SCHEDULE__SEQUENCES:
				return ((InternalEList<?>)getSequences()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SchedulePackage.SCHEDULE__RESOURCE_MODEL:
				if (resolve) return getResourceModel();
				return basicGetResourceModel();
			case SchedulePackage.SCHEDULE__SEQUENCES:
				return getSequences();
			case SchedulePackage.SCHEDULE__EPOCH_TIME:
				return isEpochTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SchedulePackage.SCHEDULE__RESOURCE_MODEL:
				setResourceModel((ResourceModel)newValue);
				return;
			case SchedulePackage.SCHEDULE__SEQUENCES:
				getSequences().clear();
				getSequences().addAll((Collection<? extends Sequence<T>>)newValue);
				return;
			case SchedulePackage.SCHEDULE__EPOCH_TIME:
				setEpochTime((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SchedulePackage.SCHEDULE__RESOURCE_MODEL:
				setResourceModel((ResourceModel)null);
				return;
			case SchedulePackage.SCHEDULE__SEQUENCES:
				getSequences().clear();
				return;
			case SchedulePackage.SCHEDULE__EPOCH_TIME:
				setEpochTime(EPOCH_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SchedulePackage.SCHEDULE__RESOURCE_MODEL:
				return resourceModel != null;
			case SchedulePackage.SCHEDULE__SEQUENCES:
				return sequences != null && !sequences.isEmpty();
			case SchedulePackage.SCHEDULE__EPOCH_TIME:
				return epochTime != EPOCH_TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (epochTime: ");
		result.append(epochTime);
		result.append(')');
		return result.toString();
	}

} //ScheduleImpl
