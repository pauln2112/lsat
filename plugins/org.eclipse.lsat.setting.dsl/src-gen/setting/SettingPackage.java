/**
 */
package setting;

import machine.MachinePackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see setting.SettingFactory
 * @model kind="package"
 * @generated
 */
public interface SettingPackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "setting";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://www.eclipse.org/lsat/setting";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "setting";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	SettingPackage eINSTANCE = setting.impl.SettingPackageImpl.init();

	/**
     * The meta object id for the '{@link setting.impl.SettingsImpl <em>Settings</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.SettingsImpl
     * @see setting.impl.SettingPackageImpl#getSettings()
     * @generated
     */
	int SETTINGS = 0;

	/**
     * The feature id for the '<em><b>Imports</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SETTINGS__IMPORTS = MachinePackage.IMPORT_CONTAINER__IMPORTS;

	/**
     * The feature id for the '<em><b>Declarations</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SETTINGS__DECLARATIONS = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Physical Settings</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SETTINGS__PHYSICAL_SETTINGS = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Settings</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SETTINGS_FEATURE_COUNT = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 2;

	/**
     * The operation id for the '<em>Load All</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SETTINGS___LOAD_ALL = MachinePackage.IMPORT_CONTAINER___LOAD_ALL;

	/**
     * The operation id for the '<em>Get Transitive Physical Settings</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SETTINGS___GET_TRANSITIVE_PHYSICAL_SETTINGS = MachinePackage.IMPORT_CONTAINER_OPERATION_COUNT + 0;

	/**
     * The operation id for the '<em>Get Physical Settings</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SETTINGS___GET_PHYSICAL_SETTINGS__IRESOURCE_PERIPHERAL = MachinePackage.IMPORT_CONTAINER_OPERATION_COUNT + 1;

	/**
     * The number of operations of the '<em>Settings</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SETTINGS_OPERATION_COUNT = MachinePackage.IMPORT_CONTAINER_OPERATION_COUNT + 2;

	/**
     * The meta object id for the '{@link setting.impl.ProfileSettingsMapEntryImpl <em>Profile Settings Map Entry</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.ProfileSettingsMapEntryImpl
     * @see setting.impl.SettingPackageImpl#getProfileSettingsMapEntry()
     * @generated
     */
	int PROFILE_SETTINGS_MAP_ENTRY = 1;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PROFILE_SETTINGS_MAP_ENTRY__NAME = 0;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PROFILE_SETTINGS_MAP_ENTRY__KEY = 1;

	/**
     * The feature id for the '<em><b>Value</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PROFILE_SETTINGS_MAP_ENTRY__VALUE = 2;

	/**
     * The number of structural features of the '<em>Profile Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PROFILE_SETTINGS_MAP_ENTRY_FEATURE_COUNT = 3;

	/**
     * The number of operations of the '<em>Profile Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PROFILE_SETTINGS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link setting.impl.LocationSettingsMapEntryImpl <em>Location Settings Map Entry</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.LocationSettingsMapEntryImpl
     * @see setting.impl.SettingPackageImpl#getLocationSettingsMapEntry()
     * @generated
     */
	int LOCATION_SETTINGS_MAP_ENTRY = 2;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_SETTINGS_MAP_ENTRY__NAME = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_SETTINGS_MAP_ENTRY__VALUE = 1;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_SETTINGS_MAP_ENTRY__KEY = 2;

	/**
     * The number of structural features of the '<em>Location Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_SETTINGS_MAP_ENTRY_FEATURE_COUNT = 3;

	/**
     * The number of operations of the '<em>Location Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_SETTINGS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link setting.impl.TimingSettingsMapEntryImpl <em>Timing Settings Map Entry</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.TimingSettingsMapEntryImpl
     * @see setting.impl.SettingPackageImpl#getTimingSettingsMapEntry()
     * @generated
     */
	int TIMING_SETTINGS_MAP_ENTRY = 3;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIMING_SETTINGS_MAP_ENTRY__NAME = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIMING_SETTINGS_MAP_ENTRY__VALUE = 1;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIMING_SETTINGS_MAP_ENTRY__KEY = 2;

	/**
     * The number of structural features of the '<em>Timing Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIMING_SETTINGS_MAP_ENTRY_FEATURE_COUNT = 3;

	/**
     * The number of operations of the '<em>Timing Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIMING_SETTINGS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link setting.impl.PhysicalSettingsImpl <em>Physical Settings</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.PhysicalSettingsImpl
     * @see setting.impl.SettingPackageImpl#getPhysicalSettings()
     * @generated
     */
	int PHYSICAL_SETTINGS = 4;

	/**
     * The feature id for the '<em><b>Timing Settings</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS__TIMING_SETTINGS = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Motion Settings</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS__MOTION_SETTINGS = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS__RESOURCE = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS__PERIPHERAL = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Settings</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS__SETTINGS = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS__NAME = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 5;

	/**
     * The number of structural features of the '<em>Physical Settings</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS_FEATURE_COUNT = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 6;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS___FQN = MachinePackage.HAS_RESOURCE_PERIPHERAL___FQN;

	/**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS___GET_RESOURCE = MachinePackage.HAS_RESOURCE_PERIPHERAL___GET_RESOURCE;

	/**
     * The operation id for the '<em>Get Peripheral</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS___GET_PERIPHERAL = MachinePackage.HAS_RESOURCE_PERIPHERAL___GET_PERIPHERAL;

	/**
     * The operation id for the '<em>Rp Equals</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS___RP_EQUALS__HASRESOURCEPERIPHERAL = MachinePackage.HAS_RESOURCE_PERIPHERAL___RP_EQUALS__HASRESOURCEPERIPHERAL;

	/**
     * The number of operations of the '<em>Physical Settings</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_SETTINGS_OPERATION_COUNT = MachinePackage.HAS_RESOURCE_PERIPHERAL_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link setting.impl.MotionSettingsMapEntryImpl <em>Motion Settings Map Entry</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.MotionSettingsMapEntryImpl
     * @see setting.impl.SettingPackageImpl#getMotionSettingsMapEntry()
     * @generated
     */
	int MOTION_SETTINGS_MAP_ENTRY = 5;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS_MAP_ENTRY__NAME = 0;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS_MAP_ENTRY__KEY = 1;

	/**
     * The feature id for the '<em><b>Value</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS_MAP_ENTRY__VALUE = 2;

	/**
     * The feature id for the '<em><b>Settings</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS_MAP_ENTRY__SETTINGS = 3;

	/**
     * The number of structural features of the '<em>Motion Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS_MAP_ENTRY_FEATURE_COUNT = 4;

	/**
     * The number of operations of the '<em>Motion Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link setting.impl.MotionSettingsImpl <em>Motion Settings</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.MotionSettingsImpl
     * @see setting.impl.SettingPackageImpl#getMotionSettings()
     * @generated
     */
	int MOTION_SETTINGS = 6;

	/**
     * The feature id for the '<em><b>Location Settings</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS__LOCATION_SETTINGS = 0;

	/**
     * The feature id for the '<em><b>Profile Settings</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS__PROFILE_SETTINGS = 1;

	/**
     * The feature id for the '<em><b>Distance Settings</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS__DISTANCE_SETTINGS = 2;

	/**
     * The feature id for the '<em><b>Entry</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS__ENTRY = 3;

	/**
     * The number of structural features of the '<em>Motion Settings</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS_FEATURE_COUNT = 4;

	/**
     * The number of operations of the '<em>Motion Settings</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_SETTINGS_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link setting.impl.PhysicalLocationImpl <em>Physical Location</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.PhysicalLocationImpl
     * @see setting.impl.SettingPackageImpl#getPhysicalLocation()
     * @generated
     */
	int PHYSICAL_LOCATION = 7;

	/**
     * The feature id for the '<em><b>Default Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_LOCATION__DEFAULT_EXP = 0;

	/**
     * The feature id for the '<em><b>Min Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_LOCATION__MIN_EXP = 1;

	/**
     * The feature id for the '<em><b>Max Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_LOCATION__MAX_EXP = 2;

	/**
     * The feature id for the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_LOCATION__DEFAULT = 3;

	/**
     * The feature id for the '<em><b>Min</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_LOCATION__MIN = 4;

	/**
     * The feature id for the '<em><b>Max</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_LOCATION__MAX = 5;

	/**
     * The number of structural features of the '<em>Physical Location</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_LOCATION_FEATURE_COUNT = 6;

	/**
     * The number of operations of the '<em>Physical Location</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PHYSICAL_LOCATION_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link setting.impl.MotionProfileSettingsImpl <em>Motion Profile Settings</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.MotionProfileSettingsImpl
     * @see setting.impl.SettingPackageImpl#getMotionProfileSettings()
     * @generated
     */
	int MOTION_PROFILE_SETTINGS = 8;

	/**
     * The feature id for the '<em><b>Motion Profile</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_PROFILE_SETTINGS__MOTION_PROFILE = 0;

	/**
     * The feature id for the '<em><b>Motion Arguments</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS = 1;

	/**
     * The number of structural features of the '<em>Motion Profile Settings</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_PROFILE_SETTINGS_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Motion Profile Settings</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_PROFILE_SETTINGS_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link setting.impl.MotionArgumentsMapEntryImpl <em>Motion Arguments Map Entry</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.MotionArgumentsMapEntryImpl
     * @see setting.impl.SettingPackageImpl#getMotionArgumentsMapEntry()
     * @generated
     */
	int MOTION_ARGUMENTS_MAP_ENTRY = 9;

	/**
     * The feature id for the '<em><b>Key</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_ARGUMENTS_MAP_ENTRY__KEY = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_ARGUMENTS_MAP_ENTRY__VALUE = 1;

	/**
     * The feature id for the '<em><b>Bd Value</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_ARGUMENTS_MAP_ENTRY__BD_VALUE = 2;

	/**
     * The number of structural features of the '<em>Motion Arguments Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_ARGUMENTS_MAP_ENTRY_FEATURE_COUNT = 3;

	/**
     * The number of operations of the '<em>Motion Arguments Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOTION_ARGUMENTS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link setting.impl.DistanceSettingsMapEntryImpl <em>Distance Settings Map Entry</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see setting.impl.DistanceSettingsMapEntryImpl
     * @see setting.impl.SettingPackageImpl#getDistanceSettingsMapEntry()
     * @generated
     */
	int DISTANCE_SETTINGS_MAP_ENTRY = 10;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE_SETTINGS_MAP_ENTRY__NAME = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE_SETTINGS_MAP_ENTRY__VALUE = 1;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE_SETTINGS_MAP_ENTRY__KEY = 2;

	/**
     * The feature id for the '<em><b>Bd Value</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE_SETTINGS_MAP_ENTRY__BD_VALUE = 3;

	/**
     * The feature id for the '<em><b>Motion Settings</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS = 4;

	/**
     * The number of structural features of the '<em>Distance Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE_SETTINGS_MAP_ENTRY_FEATURE_COUNT = 5;

	/**
     * The number of operations of the '<em>Distance Settings Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE_SETTINGS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
     * Returns the meta object for class '{@link setting.Settings <em>Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Settings</em>'.
     * @see setting.Settings
     * @generated
     */
	EClass getSettings();

	/**
     * Returns the meta object for the containment reference list '{@link setting.Settings#getDeclarations <em>Declarations</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Declarations</em>'.
     * @see setting.Settings#getDeclarations()
     * @see #getSettings()
     * @generated
     */
	EReference getSettings_Declarations();

	/**
     * Returns the meta object for the containment reference list '{@link setting.Settings#getPhysicalSettings <em>Physical Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Physical Settings</em>'.
     * @see setting.Settings#getPhysicalSettings()
     * @see #getSettings()
     * @generated
     */
	EReference getSettings_PhysicalSettings();

	/**
     * Returns the meta object for the '{@link setting.Settings#getTransitivePhysicalSettings() <em>Get Transitive Physical Settings</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Transitive Physical Settings</em>' operation.
     * @see setting.Settings#getTransitivePhysicalSettings()
     * @generated
     */
	EOperation getSettings__GetTransitivePhysicalSettings();

	/**
     * Returns the meta object for the '{@link setting.Settings#getPhysicalSettings(machine.IResource, machine.Peripheral) <em>Get Physical Settings</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Physical Settings</em>' operation.
     * @see setting.Settings#getPhysicalSettings(machine.IResource, machine.Peripheral)
     * @generated
     */
	EOperation getSettings__GetPhysicalSettings__IResource_Peripheral();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Profile Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Profile Settings Map Entry</em>'.
     * @see java.util.Map.Entry
     * @model features="name key value" 
     *        nameDataType="org.eclipse.emf.ecore.EString" nameTransient="true" nameChangeable="false" nameVolatile="true" nameDerived="true"
     *        keyType="machine.Profile" keyRequired="true"
     *        valueType="setting.MotionProfileSettings" valueContainment="true" valueRequired="true"
     * @generated
     */
	EClass getProfileSettingsMapEntry();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see java.util.Map.Entry
     * @see #getProfileSettingsMapEntry()
     * @generated
     */
	EAttribute getProfileSettingsMapEntry_Name();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getProfileSettingsMapEntry()
     * @generated
     */
	EReference getProfileSettingsMapEntry_Key();

	/**
     * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getProfileSettingsMapEntry()
     * @generated
     */
	EReference getProfileSettingsMapEntry_Value();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Location Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Location Settings Map Entry</em>'.
     * @see java.util.Map.Entry
     * @model features="name value key" 
     *        nameDataType="org.eclipse.emf.ecore.EString" nameTransient="true" nameChangeable="false" nameVolatile="true" nameDerived="true"
     *        valueType="setting.PhysicalLocation" valueContainment="true" valueRequired="true"
     *        keyType="machine.Position" keyRequired="true"
     * @generated
     */
	EClass getLocationSettingsMapEntry();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see java.util.Map.Entry
     * @see #getLocationSettingsMapEntry()
     * @generated
     */
	EAttribute getLocationSettingsMapEntry_Name();

	/**
     * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getLocationSettingsMapEntry()
     * @generated
     */
	EReference getLocationSettingsMapEntry_Value();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getLocationSettingsMapEntry()
     * @generated
     */
	EReference getLocationSettingsMapEntry_Key();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Timing Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Timing Settings Map Entry</em>'.
     * @see java.util.Map.Entry
     * @model features="name value key" 
     *        nameDataType="org.eclipse.emf.ecore.EString" nameTransient="true" nameChangeable="false" nameVolatile="true" nameDerived="true"
     *        valueType="timing.Timing" valueContainment="true" valueRequired="true"
     *        keyType="machine.ActionType" keyRequired="true"
     * @generated
     */
	EClass getTimingSettingsMapEntry();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see java.util.Map.Entry
     * @see #getTimingSettingsMapEntry()
     * @generated
     */
	EAttribute getTimingSettingsMapEntry_Name();

	/**
     * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getTimingSettingsMapEntry()
     * @generated
     */
	EReference getTimingSettingsMapEntry_Value();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getTimingSettingsMapEntry()
     * @generated
     */
	EReference getTimingSettingsMapEntry_Key();

	/**
     * Returns the meta object for class '{@link setting.PhysicalSettings <em>Physical Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Physical Settings</em>'.
     * @see setting.PhysicalSettings
     * @generated
     */
	EClass getPhysicalSettings();

	/**
     * Returns the meta object for the map '{@link setting.PhysicalSettings#getTimingSettings <em>Timing Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Timing Settings</em>'.
     * @see setting.PhysicalSettings#getTimingSettings()
     * @see #getPhysicalSettings()
     * @generated
     */
	EReference getPhysicalSettings_TimingSettings();

	/**
     * Returns the meta object for the map '{@link setting.PhysicalSettings#getMotionSettings <em>Motion Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Motion Settings</em>'.
     * @see setting.PhysicalSettings#getMotionSettings()
     * @see #getPhysicalSettings()
     * @generated
     */
	EReference getPhysicalSettings_MotionSettings();

	/**
     * Returns the meta object for the reference '{@link setting.PhysicalSettings#getResource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Resource</em>'.
     * @see setting.PhysicalSettings#getResource()
     * @see #getPhysicalSettings()
     * @generated
     */
	EReference getPhysicalSettings_Resource();

	/**
     * Returns the meta object for the reference '{@link setting.PhysicalSettings#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Peripheral</em>'.
     * @see setting.PhysicalSettings#getPeripheral()
     * @see #getPhysicalSettings()
     * @generated
     */
	EReference getPhysicalSettings_Peripheral();

	/**
     * Returns the meta object for the reference '{@link setting.PhysicalSettings#getSettings <em>Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Settings</em>'.
     * @see setting.PhysicalSettings#getSettings()
     * @see #getPhysicalSettings()
     * @generated
     */
	EReference getPhysicalSettings_Settings();

	/**
     * Returns the meta object for the attribute '{@link setting.PhysicalSettings#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see setting.PhysicalSettings#getName()
     * @see #getPhysicalSettings()
     * @generated
     */
	EAttribute getPhysicalSettings_Name();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Motion Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Motion Settings Map Entry</em>'.
     * @see java.util.Map.Entry
     * @model features="name key value settings" 
     *        nameDataType="org.eclipse.emf.ecore.EString" nameTransient="true" nameChangeable="false" nameVolatile="true" nameDerived="true"
     *        keyType="machine.Axis" keyRequired="true"
     *        valueType="setting.MotionSettings" valueOpposite="entry" valueContainment="true" valueRequired="true"
     *        settingsType="setting.PhysicalSettings" settingsOpposite="motionSettings" settingsResolveProxies="false" settingsRequired="true"
     * @generated
     */
	EClass getMotionSettingsMapEntry();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see java.util.Map.Entry
     * @see #getMotionSettingsMapEntry()
     * @generated
     */
	EAttribute getMotionSettingsMapEntry_Name();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getMotionSettingsMapEntry()
     * @generated
     */
	EReference getMotionSettingsMapEntry_Key();

	/**
     * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getMotionSettingsMapEntry()
     * @generated
     */
	EReference getMotionSettingsMapEntry_Value();

	/**
     * Returns the meta object for the container reference '{@link java.util.Map.Entry <em>Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Settings</em>'.
     * @see java.util.Map.Entry
     * @see #getMotionSettingsMapEntry()
     * @generated
     */
	EReference getMotionSettingsMapEntry_Settings();

	/**
     * Returns the meta object for class '{@link setting.MotionSettings <em>Motion Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Motion Settings</em>'.
     * @see setting.MotionSettings
     * @generated
     */
	EClass getMotionSettings();

	/**
     * Returns the meta object for the map '{@link setting.MotionSettings#getLocationSettings <em>Location Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Location Settings</em>'.
     * @see setting.MotionSettings#getLocationSettings()
     * @see #getMotionSettings()
     * @generated
     */
	EReference getMotionSettings_LocationSettings();

	/**
     * Returns the meta object for the map '{@link setting.MotionSettings#getProfileSettings <em>Profile Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Profile Settings</em>'.
     * @see setting.MotionSettings#getProfileSettings()
     * @see #getMotionSettings()
     * @generated
     */
	EReference getMotionSettings_ProfileSettings();

	/**
     * Returns the meta object for the map '{@link setting.MotionSettings#getDistanceSettings <em>Distance Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Distance Settings</em>'.
     * @see setting.MotionSettings#getDistanceSettings()
     * @see #getMotionSettings()
     * @generated
     */
	EReference getMotionSettings_DistanceSettings();

	/**
     * Returns the meta object for the container reference '{@link setting.MotionSettings#getEntry <em>Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Entry</em>'.
     * @see setting.MotionSettings#getEntry()
     * @see #getMotionSettings()
     * @generated
     */
	EReference getMotionSettings_Entry();

	/**
     * Returns the meta object for class '{@link setting.PhysicalLocation <em>Physical Location</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Physical Location</em>'.
     * @see setting.PhysicalLocation
     * @generated
     */
	EClass getPhysicalLocation();

	/**
     * Returns the meta object for the containment reference '{@link setting.PhysicalLocation#getDefaultExp <em>Default Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Default Exp</em>'.
     * @see setting.PhysicalLocation#getDefaultExp()
     * @see #getPhysicalLocation()
     * @generated
     */
	EReference getPhysicalLocation_DefaultExp();

	/**
     * Returns the meta object for the containment reference '{@link setting.PhysicalLocation#getMinExp <em>Min Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Min Exp</em>'.
     * @see setting.PhysicalLocation#getMinExp()
     * @see #getPhysicalLocation()
     * @generated
     */
	EReference getPhysicalLocation_MinExp();

	/**
     * Returns the meta object for the containment reference '{@link setting.PhysicalLocation#getMaxExp <em>Max Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Max Exp</em>'.
     * @see setting.PhysicalLocation#getMaxExp()
     * @see #getPhysicalLocation()
     * @generated
     */
	EReference getPhysicalLocation_MaxExp();

	/**
     * Returns the meta object for the attribute '{@link setting.PhysicalLocation#getDefault <em>Default</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Default</em>'.
     * @see setting.PhysicalLocation#getDefault()
     * @see #getPhysicalLocation()
     * @generated
     */
	EAttribute getPhysicalLocation_Default();

	/**
     * Returns the meta object for the attribute '{@link setting.PhysicalLocation#getMin <em>Min</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Min</em>'.
     * @see setting.PhysicalLocation#getMin()
     * @see #getPhysicalLocation()
     * @generated
     */
	EAttribute getPhysicalLocation_Min();

	/**
     * Returns the meta object for the attribute '{@link setting.PhysicalLocation#getMax <em>Max</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Max</em>'.
     * @see setting.PhysicalLocation#getMax()
     * @see #getPhysicalLocation()
     * @generated
     */
	EAttribute getPhysicalLocation_Max();

	/**
     * Returns the meta object for class '{@link setting.MotionProfileSettings <em>Motion Profile Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Motion Profile Settings</em>'.
     * @see setting.MotionProfileSettings
     * @generated
     */
	EClass getMotionProfileSettings();

	/**
     * Returns the meta object for the attribute '{@link setting.MotionProfileSettings#getMotionProfile <em>Motion Profile</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Motion Profile</em>'.
     * @see setting.MotionProfileSettings#getMotionProfile()
     * @see #getMotionProfileSettings()
     * @generated
     */
	EAttribute getMotionProfileSettings_MotionProfile();

	/**
     * Returns the meta object for the map '{@link setting.MotionProfileSettings#getMotionArguments <em>Motion Arguments</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Motion Arguments</em>'.
     * @see setting.MotionProfileSettings#getMotionArguments()
     * @see #getMotionProfileSettings()
     * @generated
     */
	EReference getMotionProfileSettings_MotionArguments();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Motion Arguments Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Motion Arguments Map Entry</em>'.
     * @see java.util.Map.Entry
     * @model features="key value bdValue" 
     *        keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
     *        valueType="expressions.Expression" valueContainment="true" valueRequired="true"
     *        bdValueDataType="org.eclipse.emf.ecore.EBigDecimal" bdValueTransient="true" bdValueChangeable="false" bdValueVolatile="true" bdValueDerived="true"
     * @generated
     */
	EClass getMotionArgumentsMapEntry();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getMotionArgumentsMapEntry()
     * @generated
     */
	EAttribute getMotionArgumentsMapEntry_Key();

	/**
     * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getMotionArgumentsMapEntry()
     * @generated
     */
	EReference getMotionArgumentsMapEntry_Value();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Bd Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Bd Value</em>'.
     * @see java.util.Map.Entry
     * @see #getMotionArgumentsMapEntry()
     * @generated
     */
	EAttribute getMotionArgumentsMapEntry_BdValue();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Distance Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Distance Settings Map Entry</em>'.
     * @see java.util.Map.Entry
     * @model features="name value key bdValue motionSettings" 
     *        nameDataType="org.eclipse.emf.ecore.EString" nameTransient="true" nameChangeable="false" nameVolatile="true" nameDerived="true"
     *        valueType="expressions.Expression" valueContainment="true" valueRequired="true"
     *        keyType="machine.Distance" keyRequired="true"
     *        bdValueDataType="org.eclipse.emf.ecore.EBigDecimal" bdValueTransient="true" bdValueChangeable="false" bdValueVolatile="true" bdValueDerived="true"
     *        motionSettingsType="setting.MotionSettings" motionSettingsOpposite="distanceSettings" motionSettingsResolveProxies="false" motionSettingsRequired="true"
     * @generated
     */
	EClass getDistanceSettingsMapEntry();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see java.util.Map.Entry
     * @see #getDistanceSettingsMapEntry()
     * @generated
     */
	EAttribute getDistanceSettingsMapEntry_Name();

	/**
     * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getDistanceSettingsMapEntry()
     * @generated
     */
	EReference getDistanceSettingsMapEntry_Value();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getDistanceSettingsMapEntry()
     * @generated
     */
	EReference getDistanceSettingsMapEntry_Key();

	/**
     * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Bd Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Bd Value</em>'.
     * @see java.util.Map.Entry
     * @see #getDistanceSettingsMapEntry()
     * @generated
     */
	EAttribute getDistanceSettingsMapEntry_BdValue();

	/**
     * Returns the meta object for the container reference '{@link java.util.Map.Entry <em>Motion Settings</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Motion Settings</em>'.
     * @see java.util.Map.Entry
     * @see #getDistanceSettingsMapEntry()
     * @generated
     */
	EReference getDistanceSettingsMapEntry_MotionSettings();

	/**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	SettingFactory getSettingFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link setting.impl.SettingsImpl <em>Settings</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.SettingsImpl
         * @see setting.impl.SettingPackageImpl#getSettings()
         * @generated
         */
		EClass SETTINGS = eINSTANCE.getSettings();

		/**
         * The meta object literal for the '<em><b>Declarations</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference SETTINGS__DECLARATIONS = eINSTANCE.getSettings_Declarations();

		/**
         * The meta object literal for the '<em><b>Physical Settings</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference SETTINGS__PHYSICAL_SETTINGS = eINSTANCE.getSettings_PhysicalSettings();

		/**
         * The meta object literal for the '<em><b>Get Transitive Physical Settings</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation SETTINGS___GET_TRANSITIVE_PHYSICAL_SETTINGS = eINSTANCE.getSettings__GetTransitivePhysicalSettings();

		/**
         * The meta object literal for the '<em><b>Get Physical Settings</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation SETTINGS___GET_PHYSICAL_SETTINGS__IRESOURCE_PERIPHERAL = eINSTANCE.getSettings__GetPhysicalSettings__IResource_Peripheral();

		/**
         * The meta object literal for the '{@link setting.impl.ProfileSettingsMapEntryImpl <em>Profile Settings Map Entry</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.ProfileSettingsMapEntryImpl
         * @see setting.impl.SettingPackageImpl#getProfileSettingsMapEntry()
         * @generated
         */
		EClass PROFILE_SETTINGS_MAP_ENTRY = eINSTANCE.getProfileSettingsMapEntry();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PROFILE_SETTINGS_MAP_ENTRY__NAME = eINSTANCE.getProfileSettingsMapEntry_Name();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PROFILE_SETTINGS_MAP_ENTRY__KEY = eINSTANCE.getProfileSettingsMapEntry_Key();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PROFILE_SETTINGS_MAP_ENTRY__VALUE = eINSTANCE.getProfileSettingsMapEntry_Value();

		/**
         * The meta object literal for the '{@link setting.impl.LocationSettingsMapEntryImpl <em>Location Settings Map Entry</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.LocationSettingsMapEntryImpl
         * @see setting.impl.SettingPackageImpl#getLocationSettingsMapEntry()
         * @generated
         */
		EClass LOCATION_SETTINGS_MAP_ENTRY = eINSTANCE.getLocationSettingsMapEntry();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute LOCATION_SETTINGS_MAP_ENTRY__NAME = eINSTANCE.getLocationSettingsMapEntry_Name();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LOCATION_SETTINGS_MAP_ENTRY__VALUE = eINSTANCE.getLocationSettingsMapEntry_Value();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LOCATION_SETTINGS_MAP_ENTRY__KEY = eINSTANCE.getLocationSettingsMapEntry_Key();

		/**
         * The meta object literal for the '{@link setting.impl.TimingSettingsMapEntryImpl <em>Timing Settings Map Entry</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.TimingSettingsMapEntryImpl
         * @see setting.impl.SettingPackageImpl#getTimingSettingsMapEntry()
         * @generated
         */
		EClass TIMING_SETTINGS_MAP_ENTRY = eINSTANCE.getTimingSettingsMapEntry();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute TIMING_SETTINGS_MAP_ENTRY__NAME = eINSTANCE.getTimingSettingsMapEntry_Name();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference TIMING_SETTINGS_MAP_ENTRY__VALUE = eINSTANCE.getTimingSettingsMapEntry_Value();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference TIMING_SETTINGS_MAP_ENTRY__KEY = eINSTANCE.getTimingSettingsMapEntry_Key();

		/**
         * The meta object literal for the '{@link setting.impl.PhysicalSettingsImpl <em>Physical Settings</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.PhysicalSettingsImpl
         * @see setting.impl.SettingPackageImpl#getPhysicalSettings()
         * @generated
         */
		EClass PHYSICAL_SETTINGS = eINSTANCE.getPhysicalSettings();

		/**
         * The meta object literal for the '<em><b>Timing Settings</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PHYSICAL_SETTINGS__TIMING_SETTINGS = eINSTANCE.getPhysicalSettings_TimingSettings();

		/**
         * The meta object literal for the '<em><b>Motion Settings</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PHYSICAL_SETTINGS__MOTION_SETTINGS = eINSTANCE.getPhysicalSettings_MotionSettings();

		/**
         * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PHYSICAL_SETTINGS__RESOURCE = eINSTANCE.getPhysicalSettings_Resource();

		/**
         * The meta object literal for the '<em><b>Peripheral</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PHYSICAL_SETTINGS__PERIPHERAL = eINSTANCE.getPhysicalSettings_Peripheral();

		/**
         * The meta object literal for the '<em><b>Settings</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PHYSICAL_SETTINGS__SETTINGS = eINSTANCE.getPhysicalSettings_Settings();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PHYSICAL_SETTINGS__NAME = eINSTANCE.getPhysicalSettings_Name();

		/**
         * The meta object literal for the '{@link setting.impl.MotionSettingsMapEntryImpl <em>Motion Settings Map Entry</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.MotionSettingsMapEntryImpl
         * @see setting.impl.SettingPackageImpl#getMotionSettingsMapEntry()
         * @generated
         */
		EClass MOTION_SETTINGS_MAP_ENTRY = eINSTANCE.getMotionSettingsMapEntry();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MOTION_SETTINGS_MAP_ENTRY__NAME = eINSTANCE.getMotionSettingsMapEntry_Name();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOTION_SETTINGS_MAP_ENTRY__KEY = eINSTANCE.getMotionSettingsMapEntry_Key();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOTION_SETTINGS_MAP_ENTRY__VALUE = eINSTANCE.getMotionSettingsMapEntry_Value();

		/**
         * The meta object literal for the '<em><b>Settings</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOTION_SETTINGS_MAP_ENTRY__SETTINGS = eINSTANCE.getMotionSettingsMapEntry_Settings();

		/**
         * The meta object literal for the '{@link setting.impl.MotionSettingsImpl <em>Motion Settings</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.MotionSettingsImpl
         * @see setting.impl.SettingPackageImpl#getMotionSettings()
         * @generated
         */
		EClass MOTION_SETTINGS = eINSTANCE.getMotionSettings();

		/**
         * The meta object literal for the '<em><b>Location Settings</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOTION_SETTINGS__LOCATION_SETTINGS = eINSTANCE.getMotionSettings_LocationSettings();

		/**
         * The meta object literal for the '<em><b>Profile Settings</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOTION_SETTINGS__PROFILE_SETTINGS = eINSTANCE.getMotionSettings_ProfileSettings();

		/**
         * The meta object literal for the '<em><b>Distance Settings</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOTION_SETTINGS__DISTANCE_SETTINGS = eINSTANCE.getMotionSettings_DistanceSettings();

		/**
         * The meta object literal for the '<em><b>Entry</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOTION_SETTINGS__ENTRY = eINSTANCE.getMotionSettings_Entry();

		/**
         * The meta object literal for the '{@link setting.impl.PhysicalLocationImpl <em>Physical Location</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.PhysicalLocationImpl
         * @see setting.impl.SettingPackageImpl#getPhysicalLocation()
         * @generated
         */
		EClass PHYSICAL_LOCATION = eINSTANCE.getPhysicalLocation();

		/**
         * The meta object literal for the '<em><b>Default Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PHYSICAL_LOCATION__DEFAULT_EXP = eINSTANCE.getPhysicalLocation_DefaultExp();

		/**
         * The meta object literal for the '<em><b>Min Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PHYSICAL_LOCATION__MIN_EXP = eINSTANCE.getPhysicalLocation_MinExp();

		/**
         * The meta object literal for the '<em><b>Max Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PHYSICAL_LOCATION__MAX_EXP = eINSTANCE.getPhysicalLocation_MaxExp();

		/**
         * The meta object literal for the '<em><b>Default</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PHYSICAL_LOCATION__DEFAULT = eINSTANCE.getPhysicalLocation_Default();

		/**
         * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PHYSICAL_LOCATION__MIN = eINSTANCE.getPhysicalLocation_Min();

		/**
         * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PHYSICAL_LOCATION__MAX = eINSTANCE.getPhysicalLocation_Max();

		/**
         * The meta object literal for the '{@link setting.impl.MotionProfileSettingsImpl <em>Motion Profile Settings</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.MotionProfileSettingsImpl
         * @see setting.impl.SettingPackageImpl#getMotionProfileSettings()
         * @generated
         */
		EClass MOTION_PROFILE_SETTINGS = eINSTANCE.getMotionProfileSettings();

		/**
         * The meta object literal for the '<em><b>Motion Profile</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MOTION_PROFILE_SETTINGS__MOTION_PROFILE = eINSTANCE.getMotionProfileSettings_MotionProfile();

		/**
         * The meta object literal for the '<em><b>Motion Arguments</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS = eINSTANCE.getMotionProfileSettings_MotionArguments();

		/**
         * The meta object literal for the '{@link setting.impl.MotionArgumentsMapEntryImpl <em>Motion Arguments Map Entry</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.MotionArgumentsMapEntryImpl
         * @see setting.impl.SettingPackageImpl#getMotionArgumentsMapEntry()
         * @generated
         */
		EClass MOTION_ARGUMENTS_MAP_ENTRY = eINSTANCE.getMotionArgumentsMapEntry();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MOTION_ARGUMENTS_MAP_ENTRY__KEY = eINSTANCE.getMotionArgumentsMapEntry_Key();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOTION_ARGUMENTS_MAP_ENTRY__VALUE = eINSTANCE.getMotionArgumentsMapEntry_Value();

		/**
         * The meta object literal for the '<em><b>Bd Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MOTION_ARGUMENTS_MAP_ENTRY__BD_VALUE = eINSTANCE.getMotionArgumentsMapEntry_BdValue();

		/**
         * The meta object literal for the '{@link setting.impl.DistanceSettingsMapEntryImpl <em>Distance Settings Map Entry</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see setting.impl.DistanceSettingsMapEntryImpl
         * @see setting.impl.SettingPackageImpl#getDistanceSettingsMapEntry()
         * @generated
         */
		EClass DISTANCE_SETTINGS_MAP_ENTRY = eINSTANCE.getDistanceSettingsMapEntry();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute DISTANCE_SETTINGS_MAP_ENTRY__NAME = eINSTANCE.getDistanceSettingsMapEntry_Name();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference DISTANCE_SETTINGS_MAP_ENTRY__VALUE = eINSTANCE.getDistanceSettingsMapEntry_Value();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference DISTANCE_SETTINGS_MAP_ENTRY__KEY = eINSTANCE.getDistanceSettingsMapEntry_Key();

		/**
         * The meta object literal for the '<em><b>Bd Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute DISTANCE_SETTINGS_MAP_ENTRY__BD_VALUE = eINSTANCE.getDistanceSettingsMapEntry_BdValue();

		/**
         * The meta object literal for the '<em><b>Motion Settings</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS = eINSTANCE.getDistanceSettingsMapEntry_MotionSettings();

	}

} //SettingPackage
