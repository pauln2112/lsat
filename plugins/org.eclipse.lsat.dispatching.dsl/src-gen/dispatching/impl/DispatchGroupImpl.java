/**
 */
package dispatching.impl;

import dispatching.Attribute;
import dispatching.Dispatch;
import dispatching.DispatchGroup;
import dispatching.DispatchingPackage;
import dispatching.Repeat;
import java.math.BigDecimal;

import java.util.Collection;
import machine.IResource;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dispatch Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dispatching.impl.DispatchGroupImpl#getUserAttributes <em>User Attributes</em>}</li>
 *   <li>{@link dispatching.impl.DispatchGroupImpl#getDispatches <em>Dispatches</em>}</li>
 *   <li>{@link dispatching.impl.DispatchGroupImpl#getOffset <em>Offset</em>}</li>
 *   <li>{@link dispatching.impl.DispatchGroupImpl#getName <em>Name</em>}</li>
 *   <li>{@link dispatching.impl.DispatchGroupImpl#getIteratorName <em>Iterator Name</em>}</li>
 *   <li>{@link dispatching.impl.DispatchGroupImpl#getRepeats <em>Repeats</em>}</li>
 *   <li>{@link dispatching.impl.DispatchGroupImpl#getYield <em>Yield</em>}</li>
 *   <li>{@link dispatching.impl.DispatchGroupImpl#getResourceYield <em>Resource Yield</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DispatchGroupImpl extends MinimalEObjectImpl.Container implements DispatchGroup {
	/**
	 * The cached value of the '{@link #getUserAttributes() <em>User Attributes</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserAttributes()
	 * @generated
	 * @ordered
	 */
	protected EMap<Attribute, String> userAttributes;

	/**
	 * The cached value of the '{@link #getDispatches() <em>Dispatches</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatches()
	 * @generated
	 * @ordered
	 */
	protected EList<Dispatch> dispatches;

	/**
	 * The default value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected static final BigDecimal OFFSET_EDEFAULT = new BigDecimal("0.0");

	/**
	 * The cached value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected BigDecimal offset = OFFSET_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = "default";

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getIteratorName() <em>Iterator Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIteratorName()
	 * @generated
	 * @ordered
	 */
	protected static final String ITERATOR_NAME_EDEFAULT = "iteration";

	/**
	 * The cached value of the '{@link #getIteratorName() <em>Iterator Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIteratorName()
	 * @generated
	 * @ordered
	 */
	protected String iteratorName = ITERATOR_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRepeats() <em>Repeats</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepeats()
	 * @generated
	 * @ordered
	 */
	protected EList<Repeat> repeats;

	/**
	 * The default value of the '{@link #getYield() <em>Yield</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYield()
	 * @generated
	 * @ordered
	 */
	protected static final int YIELD_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getYield() <em>Yield</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYield()
	 * @generated
	 * @ordered
	 */
	protected int yield = YIELD_EDEFAULT;

	/**
	 * The cached value of the '{@link #getResourceYield() <em>Resource Yield</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceYield()
	 * @generated
	 * @ordered
	 */
	protected EMap<IResource, Integer> resourceYield;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DispatchGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DispatchingPackage.Literals.DISPATCH_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<Attribute, String> getUserAttributes() {
		if (userAttributes == null) {
			userAttributes = new EcoreEMap<Attribute,String>(DispatchingPackage.Literals.ATTRIBUTES_MAP_ENTRY, AttributesMapEntryImpl.class, this, DispatchingPackage.DISPATCH_GROUP__USER_ATTRIBUTES);
		}
		return userAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Dispatch> getDispatches() {
		if (dispatches == null) {
			dispatches = new EObjectContainmentEList<Dispatch>(Dispatch.class, this, DispatchingPackage.DISPATCH_GROUP__DISPATCHES);
		}
		return dispatches;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BigDecimal getOffset() {
		return offset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOffset(BigDecimal newOffset) {
		BigDecimal oldOffset = offset;
		offset = newOffset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.DISPATCH_GROUP__OFFSET, oldOffset, offset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.DISPATCH_GROUP__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIteratorName() {
		return iteratorName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIteratorName(String newIteratorName) {
		String oldIteratorName = iteratorName;
		iteratorName = newIteratorName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.DISPATCH_GROUP__ITERATOR_NAME, oldIteratorName, iteratorName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Repeat> getRepeats() {
		if (repeats == null) {
			repeats = new EObjectContainmentEList<Repeat>(Repeat.class, this, DispatchingPackage.DISPATCH_GROUP__REPEATS);
		}
		return repeats;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getYield() {
		return yield;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setYield(int newYield) {
		int oldYield = yield;
		yield = newYield;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.DISPATCH_GROUP__YIELD, oldYield, yield));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<IResource, Integer> getResourceYield() {
		if (resourceYield == null) {
			resourceYield = new EcoreEMap<IResource,Integer>(DispatchingPackage.Literals.RESOURCE_YIELD_MAP_ENTRY, ResourceYieldMapEntryImpl.class, this, DispatchingPackage.DISPATCH_GROUP__RESOURCE_YIELD);
		}
		return resourceYield;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH_GROUP__USER_ATTRIBUTES:
				return ((InternalEList<?>)getUserAttributes()).basicRemove(otherEnd, msgs);
			case DispatchingPackage.DISPATCH_GROUP__DISPATCHES:
				return ((InternalEList<?>)getDispatches()).basicRemove(otherEnd, msgs);
			case DispatchingPackage.DISPATCH_GROUP__REPEATS:
				return ((InternalEList<?>)getRepeats()).basicRemove(otherEnd, msgs);
			case DispatchingPackage.DISPATCH_GROUP__RESOURCE_YIELD:
				return ((InternalEList<?>)getResourceYield()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH_GROUP__USER_ATTRIBUTES:
				if (coreType) return getUserAttributes();
				else return getUserAttributes().map();
			case DispatchingPackage.DISPATCH_GROUP__DISPATCHES:
				return getDispatches();
			case DispatchingPackage.DISPATCH_GROUP__OFFSET:
				return getOffset();
			case DispatchingPackage.DISPATCH_GROUP__NAME:
				return getName();
			case DispatchingPackage.DISPATCH_GROUP__ITERATOR_NAME:
				return getIteratorName();
			case DispatchingPackage.DISPATCH_GROUP__REPEATS:
				return getRepeats();
			case DispatchingPackage.DISPATCH_GROUP__YIELD:
				return getYield();
			case DispatchingPackage.DISPATCH_GROUP__RESOURCE_YIELD:
				if (coreType) return getResourceYield();
				else return getResourceYield().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH_GROUP__USER_ATTRIBUTES:
				((EStructuralFeature.Setting)getUserAttributes()).set(newValue);
				return;
			case DispatchingPackage.DISPATCH_GROUP__DISPATCHES:
				getDispatches().clear();
				getDispatches().addAll((Collection<? extends Dispatch>)newValue);
				return;
			case DispatchingPackage.DISPATCH_GROUP__OFFSET:
				setOffset((BigDecimal)newValue);
				return;
			case DispatchingPackage.DISPATCH_GROUP__NAME:
				setName((String)newValue);
				return;
			case DispatchingPackage.DISPATCH_GROUP__ITERATOR_NAME:
				setIteratorName((String)newValue);
				return;
			case DispatchingPackage.DISPATCH_GROUP__REPEATS:
				getRepeats().clear();
				getRepeats().addAll((Collection<? extends Repeat>)newValue);
				return;
			case DispatchingPackage.DISPATCH_GROUP__YIELD:
				setYield((Integer)newValue);
				return;
			case DispatchingPackage.DISPATCH_GROUP__RESOURCE_YIELD:
				((EStructuralFeature.Setting)getResourceYield()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH_GROUP__USER_ATTRIBUTES:
				getUserAttributes().clear();
				return;
			case DispatchingPackage.DISPATCH_GROUP__DISPATCHES:
				getDispatches().clear();
				return;
			case DispatchingPackage.DISPATCH_GROUP__OFFSET:
				setOffset(OFFSET_EDEFAULT);
				return;
			case DispatchingPackage.DISPATCH_GROUP__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DispatchingPackage.DISPATCH_GROUP__ITERATOR_NAME:
				setIteratorName(ITERATOR_NAME_EDEFAULT);
				return;
			case DispatchingPackage.DISPATCH_GROUP__REPEATS:
				getRepeats().clear();
				return;
			case DispatchingPackage.DISPATCH_GROUP__YIELD:
				setYield(YIELD_EDEFAULT);
				return;
			case DispatchingPackage.DISPATCH_GROUP__RESOURCE_YIELD:
				getResourceYield().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH_GROUP__USER_ATTRIBUTES:
				return userAttributes != null && !userAttributes.isEmpty();
			case DispatchingPackage.DISPATCH_GROUP__DISPATCHES:
				return dispatches != null && !dispatches.isEmpty();
			case DispatchingPackage.DISPATCH_GROUP__OFFSET:
				return OFFSET_EDEFAULT == null ? offset != null : !OFFSET_EDEFAULT.equals(offset);
			case DispatchingPackage.DISPATCH_GROUP__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DispatchingPackage.DISPATCH_GROUP__ITERATOR_NAME:
				return ITERATOR_NAME_EDEFAULT == null ? iteratorName != null : !ITERATOR_NAME_EDEFAULT.equals(iteratorName);
			case DispatchingPackage.DISPATCH_GROUP__REPEATS:
				return repeats != null && !repeats.isEmpty();
			case DispatchingPackage.DISPATCH_GROUP__YIELD:
				return yield != YIELD_EDEFAULT;
			case DispatchingPackage.DISPATCH_GROUP__RESOURCE_YIELD:
				return resourceYield != null && !resourceYield.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (offset: ");
		result.append(offset);
		result.append(", name: ");
		result.append(name);
		result.append(", iteratorName: ");
		result.append(iteratorName);
		result.append(", yield: ");
		result.append(yield);
		result.append(')');
		return result.toString();
	}

} //DispatchGroupImpl
