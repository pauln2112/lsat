/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.SetPoint#getAxes <em>Axes</em>}</li>
 *   <li>{@link machine.SetPoint#getName <em>Name</em>}</li>
 *   <li>{@link machine.SetPoint#getUnit <em>Unit</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getSetPoint()
 * @model
 * @generated
 */
public interface SetPoint extends EObject {
	/**
     * Returns the value of the '<em><b>Axes</b></em>' reference list.
     * The list contents are of type {@link machine.Axis}.
     * It is bidirectional and its opposite is '{@link machine.Axis#getSetPoints <em>Set Points</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Axes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Axes</em>' reference list.
     * @see machine.MachinePackage#getSetPoint_Axes()
     * @see machine.Axis#getSetPoints
     * @model opposite="setPoints" resolveProxies="false" required="true" transient="true"
     * @generated
     */
	EList<Axis> getAxes();

	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see machine.MachinePackage#getSetPoint_Name()
     * @model required="true"
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link machine.SetPoint#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

	/**
     * Returns the value of the '<em><b>Unit</b></em>' attribute.
     * The default value is <code>"m"</code>.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Unit</em>' attribute.
     * @see #setUnit(String)
     * @see machine.MachinePackage#getSetPoint_Unit()
     * @model default="m" required="true"
     * @generated
     */
	String getUnit();

	/**
     * Sets the value of the '{@link machine.SetPoint#getUnit <em>Unit</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Unit</em>' attribute.
     * @see #getUnit()
     * @generated
     */
	void setUnit(String value);

} // SetPoint
