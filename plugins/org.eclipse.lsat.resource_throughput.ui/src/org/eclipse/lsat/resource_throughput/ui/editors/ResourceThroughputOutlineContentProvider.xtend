/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors

import activity.PeripheralAction
import dispatching.ActivityDispatching
import java.math.BigDecimal
import java.util.Collections
import java.util.Set
import lsat_graph.PeripheralActionTask
import machine.IResource
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.jface.viewers.IStructuredContentProvider
import org.eclipse.lsat.common.graph.directed.editable.Node
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException
import org.eclipse.lsat.common.scheduler.algorithm.AsapScheduler
import org.eclipse.lsat.common.scheduler.algorithm.IScheduler
import org.eclipse.lsat.common.scheduler.algorithm.SchedulerException
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph
import org.eclipse.lsat.common.xtend.annotations.IntermediateProperty
import org.eclipse.lsat.motioncalculator.MotionException
import org.eclipse.lsat.scheduler.AddExecutionTimes
import org.eclipse.lsat.scheduler.CleanupGraph
import org.eclipse.lsat.scheduler.CleanupGraph.RemoveClaimReleaseStrategy
import org.eclipse.lsat.scheduler.CriticalPathAnalysis
import org.eclipse.lsat.scheduler.Dispatching2Graph
import org.eclipse.lsat.timing.util.ITimingCalculator
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.Delegate
import org.slf4j.LoggerFactory

import static extension org.eclipse.lsat.common.xtend.Queries.*
import static extension org.eclipse.lsat.resource_throughput.ui.editors.ResourceThroughputTableContentProvider.*
import static extension org.eclipse.lsat.scheduler.ALAPScheduler.*
import static extension dispatching.util.DispatchingUtil.getYield

class ResourceThroughputOutlineContentProvider implements IStructuredContentProvider {
    static val LOGGER = LoggerFactory.getLogger(ResourceThroughputOutlineContentProvider)
    
    extension val IScheduler<Task> scheduler = new AsapScheduler
    
    @IntermediateProperty(String)
    public val Set<PeripheralAction> criticalActions = Collections.EMPTY_SET;
     
    override Object[] getElements(Object editingDomain) {
        if (editingDomain instanceof ResourceThroughputEditingDomain) {
            LOGGER.debug('Calculating Resource Throughput Outline Content')
            
            val activityDispatching = editingDomain.activityDispatching
            if (activityDispatching === null) return #[]
            
            try {
                // Clear the critical-path cache upon refresh
                _IntermediateProperty_criticalActions.clear
                
                val activities = editingDomain.activities
                val resources = activities
                    .collect[nodes].objectsOfKind(PeripheralAction)
                    .collectOne[resource].toSet
                    .sortedBy([name], String.CASE_INSENSITIVE_ORDER)
            
                val d2g = new Dispatching2Graph<Task>();
                val d2gResult = d2g.transformModel(activityDispatching, new NullProgressMonitor);
                
                val cleanupGraph = new CleanupGraph<Task>(true, RemoveClaimReleaseStrategy.KeepAll);
                val graph = cleanupGraph.transformModel(d2gResult.taskDependencyGraph, new NullProgressMonitor);

                return resources.collectOne[ resource |
                    val timedNodes = activities
                        .getResourceActivities(resource.fqn)
                        .collect[getResourceActions(resource.fqn)]
                        .toSet
                    val timingCalculator = new FilteredTimingCalculator(editingDomain.timingCalculator, timedNodes);
                    val yield = activityDispatching.getYield(resource)
                    return graph.createThroughputOutlineItem(timingCalculator, resource, yield)
                ]
                // Not calculating the total anymore, as it calculated an erroneous throughput value
                // This row might be enabled in the future when calculation is valid
                //.union(graph.createThroughputOutlineItem(editingDomain.timingCalculator, null, activityDispatching.numberOfIterations))
            } catch (Exception e) {
                LOGGER.error("Failed to calculate resource throughput outline content: " + e.message, e)
                return #[]
            } finally {
                LOGGER.debug('DONE: Calculating Resource Throughput Outline Content')
            }
        } else {
            throw new IllegalArgumentException()
        }
    }
    
        /**
     *  @TODO is the sum what we want here.
     */
    private def getYield(ActivityDispatching ad, IResource resource){
        ad.dispatchGroups.map[getYield(resource)].reduce[p1, p2| p1+p2]
    }
    
    def <T extends Task> createThroughputOutlineItem(TaskDependencyGraph<T> graph, ITimingCalculator timingCalculator, IResource resource, int noIterations) throws MotionException, SchedulerException, QvtoTransformationException {
        val timedGraph = new AddExecutionTimes(timingCalculator).transformModel(graph)
        
        val schedule = createSchedule(timedGraph).applyALAPScheduling
        
        val result = new ResourceThroughputOutlineItem => [
            numberOfIterations = noIterations
            totalMakespan = schedule.nodes.collectOne[endTime].max(BigDecimal.ZERO)
            iterationMakespan = totalMakespan.doubleValue / numberOfIterations
            throughput = if (totalMakespan == BigDecimal.ZERO) Double.POSITIVE_INFINITY else (3600 / iterationMakespan)
        ]

        if (resource !== null) {
            result.resource = resource
            
            val criticalSchedule = new CriticalPathAnalysis().transformModel(schedule, new NullProgressMonitor)
            resource.fqn.criticalActions = criticalSchedule.aspects.select[name == 'Critical'].head.nodes
                .collectOne[task].objectsOfKind(PeripheralActionTask).collectOne[action].toSet
        }

        return result
    }
}

@Accessors(PUBLIC_GETTER, PACKAGE_SETTER)
class ResourceThroughputOutlineItem {
    var IResource resource
    var int numberOfIterations
    var BigDecimal totalMakespan
    var Double iterationMakespan
    var Double throughput
}


class FilteredTimingCalculator implements ITimingCalculator {
    @Delegate
    val ITimingCalculator delegate;
    val Set<Node> acceptedNodes;
    
    new (ITimingCalculator delegate, Set<Node> acceptedNodes) {
        this.delegate = delegate
        this.acceptedNodes = acceptedNodes
    }
    
    override calculateDuration(Node node) throws MotionException {
        return if (acceptedNodes.contains(node)) delegate.calculateDuration(node) else BigDecimal.ZERO
    }
}