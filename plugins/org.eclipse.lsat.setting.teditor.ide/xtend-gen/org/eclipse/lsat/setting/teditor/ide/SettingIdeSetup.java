/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.setting.teditor.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.lsat.setting.teditor.SettingRuntimeModule;
import org.eclipse.lsat.setting.teditor.SettingStandaloneSetup;
import org.eclipse.lsat.setting.teditor.ide.SettingIdeModule;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
@SuppressWarnings("all")
public class SettingIdeSetup extends SettingStandaloneSetup {
  @Override
  public Injector createInjector() {
    SettingRuntimeModule _settingRuntimeModule = new SettingRuntimeModule();
    SettingIdeModule _settingIdeModule = new SettingIdeModule();
    return Guice.createInjector(Modules2.mixin(_settingRuntimeModule, _settingIdeModule));
  }
}
