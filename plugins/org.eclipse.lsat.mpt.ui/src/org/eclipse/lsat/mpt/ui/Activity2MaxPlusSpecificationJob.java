/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.ui;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.lsat.common.emf.ecore.resource.Persistor;
import org.eclipse.lsat.common.emf.ecore.resource.PersistorFactory;
import org.eclipse.lsat.common.mpt.MaxPlusSpecification;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class Activity2MaxPlusSpecificationJob extends Job {
    private final ActivityTransformationOptions options;

    private final UISynchronize sync;

    private final Shell shell;

    private final Activity2DispatchingHelper helper;

    public Activity2MaxPlusSpecificationJob(ActivityTransformationOptions options, UISynchronize sync, Shell shell) {
        super("Activity2MaxPlusSpecificationJob");
        this.options = options;
        this.sync = sync;
        this.shell = shell;
        helper = new Activity2DispatchingHelper(options);
    }

    @Override
    protected IStatus run(IProgressMonitor monitor) {
        String outputFileName;
        try {
            // Create max-plus specification
            MaxPlusSpecification maxPlusSpecification = helper.createMaxPlusSpecification(monitor);
            if (monitor.isCanceled())
                return Status.CANCEL_STATUS;

            // Persist the max plus specification as "[activity file name]_[cif file name].mpt".
            Persistor<EObject> savePersistor = new PersistorFactory().getPersistor(EObject.class);
            outputFileName = options.getActivityFileName().concat("_").concat(options.getCifFileName());
            URI saveBaseURI = helper.getResultFolderURI().appendSegment(outputFileName);
            savePersistor.save(saveBaseURI.appendFileExtension("mpt"), maxPlusSpecification);

            if (monitor.isCanceled())
                return Status.CANCEL_STATUS;
            monitor.worked(50);

            sync.asyncExec(new Runnable() {
                public void run() {
                    new Activity2DispatchingHelper.SuccessDialog(shell, "Max-plus specification generation",
                            "Generated max-plus specification " + "\"" + outputFileName + ".mpt" + "\"").open();
                }
            });

            return Status.OK_STATUS;
        } catch (final Exception e) {
            IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
            Activator.getDefault().getLog().log(status);
            Display.getDefault().asyncExec(new Runnable() {
                public void run() {
                    MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
                            "Max-plus specification generation", "Max-plus specification generation failed:\n"
                                    + e.getMessage() + "\n\nSee Error Log for details.");
                }
            });
            // To avoid getting an additional "Problem Occurred" window, we return OK here.
            return Status.OK_STATUS;
        } finally {
            monitor.done();
        }
    }
}
