/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.teditor.ui.contentassist

import javax.inject.Inject
import org.eclipse.lsat.machine.teditor.services.MachineGrammarAccess
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

/**
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#content-assist
 * on how to customize the content assistant.
 */
class MachineProposalProvider extends AbstractMachineProposalProvider {
    
    @Inject
    MachineGrammarAccess grammarAccess

    override completeKeyword(Keyword keyword, ContentAssistContext contentAssistContext,
        ICompletionProposalAcceptor acceptor) {
        val skipKeywords = newLinkedHashSet(
           grammarAccess.resourceTypeAccess.PASSIVEPassiveResourceKeyword_0_0,
           grammarAccess.resourceTypeAccess.EVENTEventResourceKeyword_1_0
        )
        if (!skipKeywords.contains(keyword)) {
            super.completeKeyword(keyword, contentAssistContext, acceptor)
        }
    }

}
