/**
 */
package org.eclipse.lsat.common.scheduler.graph.impl;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;

import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.GraphFactory;
import org.eclipse.lsat.common.scheduler.graph.GraphPackage;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;

import org.eclipse.lsat.common.scheduler.resources.ResourcesPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GraphPackageImpl extends EPackageImpl implements GraphPackage {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass taskDependencyGraphEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass taskEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass dependencyEClass = null;

	/**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.eclipse.lsat.common.scheduler.graph.GraphPackage#eNS_URI
     * @see #init()
     * @generated
     */
	private GraphPackageImpl() {
        super(eNS_URI, GraphFactory.eINSTANCE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static boolean isInited = false;

	/**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link GraphPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
	public static GraphPackage init() {
        if (isInited) return (GraphPackage)EPackage.Registry.INSTANCE.getEPackage(GraphPackage.eNS_URI);

        // Obtain or create and register package
        Object registeredGraphPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        GraphPackageImpl theGraphPackage = registeredGraphPackage instanceof GraphPackageImpl ? (GraphPackageImpl)registeredGraphPackage : new GraphPackageImpl();

        isInited = true;

        // Initialize simple dependencies
        DirectedGraphPackage.eINSTANCE.eClass();
        ResourcesPackage.eINSTANCE.eClass();

        // Create package meta-data objects
        theGraphPackage.createPackageContents();

        // Initialize created meta-data
        theGraphPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theGraphPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(GraphPackage.eNS_URI, theGraphPackage);
        return theGraphPackage;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getTaskDependencyGraph() {
        return taskDependencyGraphEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getTaskDependencyGraph_ResourceModel() {
        return (EReference)taskDependencyGraphEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getTask() {
        return taskEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getTask_ExecutionTime() {
        return (EAttribute)taskEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getTask_Resources() {
        return (EReference)taskEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDependency() {
        return dependencyEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public GraphFactory getGraphFactory() {
        return (GraphFactory)getEFactoryInstance();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isCreated = false;

	/**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        taskDependencyGraphEClass = createEClass(TASK_DEPENDENCY_GRAPH);
        createEReference(taskDependencyGraphEClass, TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL);

        taskEClass = createEClass(TASK);
        createEAttribute(taskEClass, TASK__EXECUTION_TIME);
        createEReference(taskEClass, TASK__RESOURCES);

        dependencyEClass = createEClass(DEPENDENCY);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isInitialized = false;

	/**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        DirectedGraphPackage theDirectedGraphPackage = (DirectedGraphPackage)EPackage.Registry.INSTANCE.getEPackage(DirectedGraphPackage.eNS_URI);
        ResourcesPackage theResourcesPackage = (ResourcesPackage)EPackage.Registry.INSTANCE.getEPackage(ResourcesPackage.eNS_URI);

        // Create type parameters
        ETypeParameter taskDependencyGraphEClass_T = addETypeParameter(taskDependencyGraphEClass, "T");

        // Set bounds for type parameters
        EGenericType g1 = createEGenericType(this.getTask());
        taskDependencyGraphEClass_T.getEBounds().add(g1);

        // Add supertypes to classes
        g1 = createEGenericType(theDirectedGraphPackage.getDirectedGraph());
        EGenericType g2 = createEGenericType(taskDependencyGraphEClass_T);
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(this.getDependency());
        g1.getETypeArguments().add(g2);
        taskDependencyGraphEClass.getEGenericSuperTypes().add(g1);
        taskEClass.getESuperTypes().add(theDirectedGraphPackage.getNode());
        dependencyEClass.getESuperTypes().add(theDirectedGraphPackage.getEdge());

        // Initialize classes, features, and operations; add parameters
        initEClass(taskDependencyGraphEClass, TaskDependencyGraph.class, "TaskDependencyGraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getTaskDependencyGraph_ResourceModel(), theResourcesPackage.getResourceModel(), null, "resourceModel", null, 1, 1, TaskDependencyGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(taskEClass, Task.class, "Task", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTask_ExecutionTime(), ecorePackage.getEBigDecimal(), "executionTime", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTask_Resources(), theResourcesPackage.getResource(), null, "resources", null, 1, -1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(dependencyEClass, Dependency.class, "Dependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        // Create resource
        createResource(eNS_URI);
    }

} //GraphPackageImpl
