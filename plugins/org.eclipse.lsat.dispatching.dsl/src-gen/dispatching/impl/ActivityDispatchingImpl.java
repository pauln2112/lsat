/**
 */
package dispatching.impl;

import dispatching.ActivityDispatching;
import dispatching.DispatchGroup;
import dispatching.DispatchingPackage;
import java.util.Collection;
import machine.IResource;
import machine.impl.ImportContainerImpl;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity Dispatching</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dispatching.impl.ActivityDispatchingImpl#getDispatchGroups <em>Dispatch Groups</em>}</li>
 *   <li>{@link dispatching.impl.ActivityDispatchingImpl#getNumberOfIterations <em>Number Of Iterations</em>}</li>
 *   <li>{@link dispatching.impl.ActivityDispatchingImpl#getResourceIterations <em>Resource Iterations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActivityDispatchingImpl extends ImportContainerImpl implements ActivityDispatching {
	/**
	 * The cached value of the '{@link #getDispatchGroups() <em>Dispatch Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatchGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<DispatchGroup> dispatchGroups;

	/**
	 * The default value of the '{@link #getNumberOfIterations() <em>Number Of Iterations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfIterations()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_ITERATIONS_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getNumberOfIterations() <em>Number Of Iterations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfIterations()
	 * @generated
	 * @ordered
	 */
	protected int numberOfIterations = NUMBER_OF_ITERATIONS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getResourceIterations() <em>Resource Iterations</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceIterations()
	 * @generated
	 * @ordered
	 */
	protected EMap<IResource, Integer> resourceIterations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityDispatchingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DispatchingPackage.Literals.ACTIVITY_DISPATCHING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DispatchGroup> getDispatchGroups() {
		if (dispatchGroups == null) {
			dispatchGroups = new EObjectContainmentEList<DispatchGroup>(DispatchGroup.class, this, DispatchingPackage.ACTIVITY_DISPATCHING__DISPATCH_GROUPS);
		}
		return dispatchGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfIterations() {
		return numberOfIterations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfIterations(int newNumberOfIterations) {
		int oldNumberOfIterations = numberOfIterations;
		numberOfIterations = newNumberOfIterations;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS, oldNumberOfIterations, numberOfIterations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<IResource, Integer> getResourceIterations() {
		if (resourceIterations == null) {
			resourceIterations = new EcoreEMap<IResource,Integer>(DispatchingPackage.Literals.RESOURCE_ITERATIONS_MAP_ENTRY, ResourceIterationsMapEntryImpl.class, this, DispatchingPackage.ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS);
		}
		return resourceIterations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DispatchingPackage.ACTIVITY_DISPATCHING__DISPATCH_GROUPS:
				return ((InternalEList<?>)getDispatchGroups()).basicRemove(otherEnd, msgs);
			case DispatchingPackage.ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS:
				return ((InternalEList<?>)getResourceIterations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DispatchingPackage.ACTIVITY_DISPATCHING__DISPATCH_GROUPS:
				return getDispatchGroups();
			case DispatchingPackage.ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS:
				return getNumberOfIterations();
			case DispatchingPackage.ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS:
				if (coreType) return getResourceIterations();
				else return getResourceIterations().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DispatchingPackage.ACTIVITY_DISPATCHING__DISPATCH_GROUPS:
				getDispatchGroups().clear();
				getDispatchGroups().addAll((Collection<? extends DispatchGroup>)newValue);
				return;
			case DispatchingPackage.ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS:
				setNumberOfIterations((Integer)newValue);
				return;
			case DispatchingPackage.ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS:
				((EStructuralFeature.Setting)getResourceIterations()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DispatchingPackage.ACTIVITY_DISPATCHING__DISPATCH_GROUPS:
				getDispatchGroups().clear();
				return;
			case DispatchingPackage.ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS:
				setNumberOfIterations(NUMBER_OF_ITERATIONS_EDEFAULT);
				return;
			case DispatchingPackage.ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS:
				getResourceIterations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DispatchingPackage.ACTIVITY_DISPATCHING__DISPATCH_GROUPS:
				return dispatchGroups != null && !dispatchGroups.isEmpty();
			case DispatchingPackage.ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS:
				return numberOfIterations != NUMBER_OF_ITERATIONS_EDEFAULT;
			case DispatchingPackage.ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS:
				return resourceIterations != null && !resourceIterations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (numberOfIterations: ");
		result.append(numberOfIterations);
		result.append(')');
		return result.toString();
	}

} //ActivityDispatchingImpl
