/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.function.Consumer;
import lsat_graph.ActionTask;
import lsat_graph.DispatchGroupTask;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.timing.util.ITimingCalculator;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FinalFieldsConstructor
@SuppressWarnings("all")
public class AddExecutionTimes {
  private static final Logger LOGGER = LoggerFactory.getLogger(AddExecutionTimes.class);
  
  @Extension
  private final ITimingCalculator timingCalculator;
  
  public <T extends Task> TaskDependencyGraph<T> transformModel(final TaskDependencyGraph<T> graph) throws SpecificationException, MotionException {
    AddExecutionTimes.LOGGER.debug("Starting transformation");
    this.<T, Dependency>addExecutionTimes(graph);
    AddExecutionTimes.LOGGER.debug("Finished transformation");
    return graph;
  }
  
  private <T extends Task, E extends Edge> void addExecutionTimes(final DirectedGraph<T, E> graph) throws SpecificationException, MotionException {
    final Consumer<DirectedGraph<T, E>> _function = (DirectedGraph<T, E> it) -> {
      try {
        this.<T, E>addExecutionTimes(it);
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    };
    graph.getSubGraphs().forEach(_function);
    final Consumer<T> _function_1 = (T it) -> {
      try {
        this.addExecutionTime(it);
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    };
    graph.getNodes().forEach(_function_1);
  }
  
  private void _addExecutionTime(final DispatchGroupTask task) {
    BigDecimal _executionTime = task.getExecutionTime();
    boolean _tripleEquals = (_executionTime == null);
    if (_tripleEquals) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Expected execution time to be set for task: ");
      _builder.append(task);
      throw new IllegalStateException(_builder.toString());
    }
  }
  
  private void _addExecutionTime(final ActionTask<?> task) throws SpecificationException, MotionException {
    task.setExecutionTime(this.timingCalculator.calculateDuration(task.getAction()));
  }
  
  private void addExecutionTime(final Task task) throws SpecificationException, MotionException {
    if (task instanceof ActionTask) {
      _addExecutionTime((ActionTask<?>)task);
      return;
    } else if (task instanceof DispatchGroupTask) {
      _addExecutionTime((DispatchGroupTask)task);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(task).toString());
    }
  }
  
  public AddExecutionTimes(final ITimingCalculator timingCalculator) {
    super();
    this.timingCalculator = timingCalculator;
  }
}
