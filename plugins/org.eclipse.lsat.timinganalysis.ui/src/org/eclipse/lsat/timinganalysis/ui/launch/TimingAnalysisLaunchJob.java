/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timinganalysis.ui.launch;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.lsat.common.emf.common.util.URIHelper;
import org.eclipse.lsat.timinganalysis.ui.Activator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

public class TimingAnalysisLaunchJob extends Job {
    private IFile dispatchFile;

    private IFile settingFile;

    private final String mode;

    private final ILaunchConfiguration launchConfiguration;

    private final String showTargetAttribute;

    public TimingAnalysisLaunchJob(IFile[] files, String mode, String showTarget) {
        super("Timing Analysis - " + URIHelper.baseName(files[0]));
        this.showTargetAttribute = showTarget;
        this.dispatchFile = getFile(files, "dispatching");
        this.settingFile = getFile(files, "setting");
        this.mode = mode;
        // ui stuff in ui thread
        this.launchConfiguration = selectLaunchConfiguration();
    }

    @Override
    public boolean shouldSchedule() {
        return launchConfiguration != null;
    }

    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            launchConfiguration.launch(mode, monitor);
            return Status.OK_STATUS;
        } catch (CoreException e) {
            return new Status(Status.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
        }
    }

    private ILaunchConfiguration selectLaunchConfiguration() {
        try {
            Map<String, ILaunchConfiguration> items = collectItems();
            if (items == null) {
                return null;
            }
            if (items.isEmpty()) {
                return createNewConfiguration();
            }
            if (items.size() == 1) {
                // nothing to choose return it.
                return items.values().iterator().next();
            }
            // more choices so ask user
            return askUser(items);
        } catch (Exception e) {
            IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
            Activator.getDefault().getLog().log(status);
        }
        return null;
    }

    private static ILaunchConfiguration askUser(Map<String, ILaunchConfiguration> items) {
        return askUser(items, "Run Timing Analysis", "Select launch configuration");
    }

    private static IFile askUser(IFile ref, String extension) {
        Map<String, IFile> names = findAllInProject(ref, extension).stream()
                .collect(Collectors.toMap(f -> getName(f), f -> f));
        return askUser(names, "File selector", "Please select " + extension + " file");
    }

    private static <T> T askUser(Map<String, T> items, String title, String message) {
        ILabelProvider labelProvider = new LabelProvider();
        ElementListSelectionDialog dialog = new ElementListSelectionDialog(
                PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), labelProvider);

        dialog.setTitle(title);
        URL url = TimingAnalysisLaunchJob.class.getResource("/icons/gantt.png");
        ImageDescriptor imageDcr = ImageDescriptor.createFromURL(url);
        Image image = imageDcr.createImage();
        dialog.setImage(image);
        dialog.setElements(items.keySet().toArray());

        dialog.setMultipleSelection(false);
        dialog.setMessage(message);
        dialog.open();
        if (dialog.getReturnCode() == Window.OK) {
            return items.get(dialog.getFirstResult());
        }
        return null;
    }

    private Map<String, ILaunchConfiguration> collectItems() throws CoreException {
        ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType type = launchManager.getLaunchConfigurationType(Activator.LAUNCH_CONFIG_TYPE);
        return Stream.of(launchManager.getLaunchConfigurations(type)).filter(l -> eligibleConfig(l))
                .collect(Collectors.toMap(l -> l.getName(), l -> l));
    }

    private boolean eligibleConfig(ILaunchConfiguration lc) {
        try {
            return eqOrNull(getPath(dispatchFile),
                    lc.getAttribute(TimingAnalysisLaunchAttributes.MODEL_IFILE, (String)null))
                    && eqOrNull(getPath(settingFile),
                            lc.getAttribute(TimingAnalysisLaunchAttributes.SETTING_IFILE, (String)null))
                    && lc.getAttribute(showTargetAttribute, false);
        } catch (CoreException e) {
            e.printStackTrace();
            return false;
        }
    }

    private ILaunchConfiguration createNewConfiguration() throws CoreException {
        if (dispatchFile == null) {
            dispatchFile = askUser(settingFile, "dispatching");
        }
        if (settingFile == null) {
            settingFile = askUser(dispatchFile, "setting");
        }
        if (dispatchFile == null || settingFile == null) {
            return null;
        }
        ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType type = launchManager.getLaunchConfigurationType(Activator.LAUNCH_CONFIG_TYPE);
        ILaunchConfigurationWorkingCopy workingCopy = type.newInstance(null, getLaunchName());
        workingCopy.setAttribute(TimingAnalysisLaunchAttributes.MODEL_IFILE, getPath(dispatchFile));
        workingCopy.setAttribute(TimingAnalysisLaunchAttributes.SETTING_IFILE, getPath(settingFile));
        workingCopy.setAttribute(TimingAnalysisLaunchAttributes.GANTT_CHART, false);
        workingCopy.setAttribute(showTargetAttribute, true);
        workingCopy.setMappedResources(new IResource[] {dispatchFile, settingFile});
        return workingCopy.doSave();
    }

    private String getLaunchName() {
        return TimingAnalysisUtil.getLaunchName(dispatchFile, settingFile, showTargetAttribute);
    }

    private static boolean eqOrNull(Object a, Object b) {
        if (a == null) {
            return true;
        }
        return a.equals(b);
    }

    private static String getPath(IFile file) {
        return file == null ? null : file.getFullPath().toString();
    }

    private static String getName(IFile file) {
        return file == null ? "Unknown" : URIHelper.baseName(file);
    }

    private static IFile getFile(IFile[] files, String extension) {
        return Stream.of(files).filter(f -> Objects.equals(f.getFileExtension(), extension)).findFirst()
                .orElse(findOneInProject(files[0], extension));
    }

    private static Collection<IFile> findAllInProject(IFile ref, String extension) {
        List<IFile> found = new ArrayList<>();
        try {
            IProject project = ref.getProject();
            IResourceVisitor visitor = r -> {
                if (r instanceof IFile && !r.isHidden(org.eclipse.core.resources.IResource.CHECK_ANCESTORS)) {
                    if (Objects.equals(r.getFileExtension(), extension)) {
                        found.add((IFile)r);
                    }
                    return false;
                }
                return true;
            };
            project.accept(visitor);
        } catch (CoreException e) {
            // ignore and return null
        }
        return found;
    }

    private static IFile findOneInProject(IFile ref, String extension) {
        Collection<IFile> matches = findAllInProject(ref, extension);
        if (matches.size() == 1) {
            return matches.iterator().next();
        }
        return null;
    }
}
