/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.ui;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.lsat.common.mpt.MaxPlusSpecification;
import org.eclipse.lsat.common.mpt.api.MaxPlusAlgorithms;
import org.eclipse.lsat.common.mpt.api.MaximumThroughputResult;
import org.eclipse.lsat.mpt.xtend.transformation.Mpt2DispatchingMaxThroughput;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Activity2DispatchingMaxThroughputJob extends Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(Activity2DispatchingMaxThroughputJob.class);

    private final UISynchronize sync;

    private final Shell shell;

    private final Activity2DispatchingHelper helper;

    public Activity2DispatchingMaxThroughputJob(ActivityTransformationOptions options, UISynchronize sync,
            Shell shell)
    {
        super("Activity2DispatchingMaxThroughputJob");
        this.sync = sync;
        this.shell = shell;
        helper = new Activity2DispatchingHelper(options);
    }

    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            // Create max-plus specification
            MaxPlusSpecification maxPlusSpecification = helper.createMaxPlusSpecification(monitor);
            if (monitor.isCanceled()) {
                return Status.CANCEL_STATUS;
            }

            // Calculate the maximum throughput sequence.
            MaximumThroughputResult result = MaxPlusAlgorithms.calculateMaximumThroughput(maxPlusSpecification);
            if (monitor.isCanceled()) {
                return Status.CANCEL_STATUS;
            }

            LOGGER.info("Maximum throughput: " + result.getThroughput());

            // Write the output to a dispatching file.
            Mpt2DispatchingMaxThroughput mpt2DispatchingMatrix = new Mpt2DispatchingMaxThroughput();
            CharSequence dispatchingContent = mpt2DispatchingMatrix.transformModel(helper.getActivitySet(), result);

            helper.createDispatchingFile(dispatchingContent);

            if (monitor.isCanceled()) {
                return Status.CANCEL_STATUS;
            }
            monitor.worked(50);

            sync.asyncExec(new Runnable() {
                @Override
                public void run() {
                    new Activity2DispatchingHelper.SuccessDialog(shell, "Maximum throughput analysis",
                            "Maximum throughput: " + helper.round(result.getThroughput()) + ".\nResult stored in \""
                                    + helper.getDispatchingFile() + "\".").open();
                }
            });

            return Status.OK_STATUS;
        } catch (final Exception e) {
            IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
            Activator.getDefault().getLog().log(status);
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
                            "Convert activity specification to dispatching file",
                            "Converting activity specification to dispatching file failed:\n" + e.getMessage()
                                    + "\n\nSee Error Log for details.");
                }
            });
            // To avoid getting an additional "Problem Occurred" window, we return OK here.
            return Status.OK_STATUS;
        } finally {
            monitor.done();
        }
    }
}
