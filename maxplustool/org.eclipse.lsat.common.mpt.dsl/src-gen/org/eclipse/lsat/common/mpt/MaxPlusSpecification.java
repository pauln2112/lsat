/**
 */
package org.eclipse.lsat.common.mpt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Max Plus Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.MaxPlusSpecification#getMatrices <em>Matrices</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.MaxPlusSpecification#getGraphs <em>Graphs</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.MaxPlusSpecification#getControllable <em>Controllable</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.MaxPlusSpecification#getUncontrollable <em>Uncontrollable</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMaxPlusSpecification()
 * @model
 * @generated
 */
public interface MaxPlusSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Matrices</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.mpt.Matrix}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matrices</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matrices</em>' containment reference list.
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMaxPlusSpecification_Matrices()
	 * @model containment="true"
	 * @generated
	 */
	EList<Matrix> getMatrices();

	/**
	 * Returns the value of the '<em><b>Graphs</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.mpt.Graph}<code>&lt;?, ?&gt;</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graphs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graphs</em>' containment reference list.
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMaxPlusSpecification_Graphs()
	 * @model containment="true"
	 * @generated
	 */
	EList<Graph<?, ?>> getGraphs();

	/**
	 * Returns the value of the '<em><b>Controllable</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.mpt.Event}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controllable</em>' containment reference list.
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMaxPlusSpecification_Controllable()
	 * @model containment="true"
	 * @generated
	 */
	EList<Event> getControllable();

	/**
	 * Returns the value of the '<em><b>Uncontrollable</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.mpt.Event}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uncontrollable</em>' containment reference list.
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMaxPlusSpecification_Uncontrollable()
	 * @model containment="true"
	 * @generated
	 */
	EList<Event> getUncontrollable();

} // MaxPlusSpecification
