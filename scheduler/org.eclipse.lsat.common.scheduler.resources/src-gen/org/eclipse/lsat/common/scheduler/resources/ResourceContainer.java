/**
 */
package org.eclipse.lsat.common.scheduler.resources;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.resources.ResourceContainer#getResources <em>Resources</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.resources.ResourceContainer#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesPackage#getResourceContainer()
 * @model abstract="true"
 * @generated
 */
public interface ResourceContainer extends NamedResource {
	/**
	 * Returns the value of the '<em><b>Resources</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.scheduler.resources.AbstractResource}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.scheduler.resources.AbstractResource#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resources</em>' containment reference list.
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesPackage#getResourceContainer_Resources()
	 * @see org.eclipse.lsat.common.scheduler.resources.AbstractResource#getContainer
	 * @model opposite="container" containment="true" required="true"
	 * @generated
	 */
	EList<AbstractResource> getResources();

	/**
	 * Returns the value of the '<em><b>Offset</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Offset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' attribute.
	 * @see #setOffset(BigDecimal)
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesPackage#getResourceContainer_Offset()
	 * @model default="0" required="true"
	 * @generated
	 */
	BigDecimal getOffset();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.resources.ResourceContainer#getOffset <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' attribute.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(BigDecimal value);

} // ResourceContainer
