/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor;

import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.lsat.activity.teditor.AbstractActivityRuntimeModule;
import org.eclipse.lsat.activity.teditor.ActivityLinkingService;
import org.eclipse.lsat.activity.teditor.ActivityTransientValueService;
import org.eclipse.lsat.activity.teditor.ItemLabelDiagnostician;
import org.eclipse.lsat.machine.teditor.ImportResourceDescriptionStrategy;
import org.eclipse.lsat.machine.teditor.scoping.ImportScopeProvider;
import org.eclipse.xtext.linking.ILinkingService;
import org.eclipse.xtext.parsetree.reconstr.ITransientValueService;
import org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy;
import org.eclipse.xtext.scoping.IGlobalScopeProvider;

/**
 * Use this class to register components to be used at runtime / without the
 * Equinox extension registry.
 */
@SuppressWarnings("all")
public class ActivityRuntimeModule extends AbstractActivityRuntimeModule {
  @Override
  public Class<? extends ILinkingService> bindILinkingService() {
    return ActivityLinkingService.class;
  }
  
  @Override
  public Class<? extends ITransientValueService> bindITransientValueService() {
    return ActivityTransientValueService.class;
  }
  
  @Override
  public Class<? extends Diagnostician> bindDiagnostician() {
    return ItemLabelDiagnostician.class;
  }
  
  @Override
  public Class<? extends IGlobalScopeProvider> bindIGlobalScopeProvider() {
    return ImportScopeProvider.class;
  }
  
  public Class<? extends IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
    return ImportResourceDescriptionStrategy.class;
  }
}
