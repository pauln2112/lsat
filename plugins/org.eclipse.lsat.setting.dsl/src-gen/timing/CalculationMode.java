/**
 */
package timing;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Calculation Mode</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see timing.TimingPackage#getCalculationMode()
 * @model
 * @generated
 */
public enum CalculationMode implements Enumerator {
	/**
     * The '<em><b>Mean</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #MEAN_VALUE
     * @generated
     * @ordered
     */
	MEAN(0, "Mean", "Mean"),

	/**
     * The '<em><b>Distributed</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #DISTRIBUTED_VALUE
     * @generated
     * @ordered
     */
	DISTRIBUTED(1, "Distributed", "Distributed"),

	/**
     * The '<em><b>Lineair</b></em>' literal object.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #LINEAIR_VALUE
     * @generated
     * @ordered
     */
	LINEAIR(2, "Lineair", "Lineair");

	/**
     * The '<em><b>Mean</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mean</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @see #MEAN
     * @model name="Mean"
     * @generated
     * @ordered
     */
	public static final int MEAN_VALUE = 0;

	/**
     * The '<em><b>Distributed</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Distributed</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @see #DISTRIBUTED
     * @model name="Distributed"
     * @generated
     * @ordered
     */
	public static final int DISTRIBUTED_VALUE = 1;

	/**
     * The '<em><b>Lineair</b></em>' literal value.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Lineair</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @see #LINEAIR
     * @model name="Lineair"
     * @generated
     * @ordered
     */
	public static final int LINEAIR_VALUE = 2;

	/**
     * An array of all the '<em><b>Calculation Mode</b></em>' enumerators.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static final CalculationMode[] VALUES_ARRAY =
		new CalculationMode[]
        {
            MEAN,
            DISTRIBUTED,
            LINEAIR,
        };

	/**
     * A public read-only list of all the '<em><b>Calculation Mode</b></em>' enumerators.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static final List<CalculationMode> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
     * Returns the '<em><b>Calculation Mode</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
	public static CalculationMode get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i)
        {
            CalculationMode result = VALUES_ARRAY[i];
            if (result.toString().equals(literal))
            {
                return result;
            }
        }
        return null;
    }

	/**
     * Returns the '<em><b>Calculation Mode</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
	public static CalculationMode getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i)
        {
            CalculationMode result = VALUES_ARRAY[i];
            if (result.getName().equals(name))
            {
                return result;
            }
        }
        return null;
    }

	/**
     * Returns the '<em><b>Calculation Mode</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
	public static CalculationMode get(int value) {
        switch (value)
        {
            case MEAN_VALUE: return MEAN;
            case DISTRIBUTED_VALUE: return DISTRIBUTED;
            case LINEAIR_VALUE: return LINEAIR;
        }
        return null;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private final int value;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private final String name;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private final String literal;

	/**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private CalculationMode(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getValue() {
      return value;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
      return name;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getLiteral() {
      return literal;
    }

	/**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        return literal;
    }
	
} //CalculationMode
