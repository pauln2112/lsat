/**
 */
package expressions.impl;

import expressions.ExpressionsPackage;
import expressions.Subtract;

import java.lang.reflect.InvocationTargetException;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subtract</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SubtractImpl extends BinaryExpressionImpl implements Subtract {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected SubtractImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ExpressionsPackage.Literals.SUBTRACT;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal operation(final BigDecimal left, final BigDecimal right) {
        return left.subtract(right);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case ExpressionsPackage.SUBTRACT___OPERATION__BIGDECIMAL_BIGDECIMAL:
                return operation((BigDecimal)arguments.get(0), (BigDecimal)arguments.get(1));
        }
        return super.eInvoke(operationID, arguments);
    }

} //SubtractImpl
