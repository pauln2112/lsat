/**
 */
package org.eclipse.lsat.petri_net;

import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Petri Net</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.petri_net.PetriNet#getInitialPlaces <em>Initial Places</em>}</li>
 *   <li>{@link org.eclipse.lsat.petri_net.PetriNet#getFinalPlaces <em>Final Places</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getPetriNet()
 * @model
 * @generated
 */
public interface PetriNet extends DirectedGraph<Node, Edge> {
	/**
	 * Returns the value of the '<em><b>Initial Places</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.petri_net.Place}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.petri_net.Place#getInitial <em>Initial</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Places</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Places</em>' reference list.
	 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getPetriNet_InitialPlaces()
	 * @see org.eclipse.lsat.petri_net.Place#getInitial
	 * @model opposite="initial" required="true"
	 * @generated
	 */
	EList<Place> getInitialPlaces();

	/**
	 * Returns the value of the '<em><b>Final Places</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.petri_net.Place}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.petri_net.Place#getFinal <em>Final</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Places</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Places</em>' reference list.
	 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getPetriNet_FinalPlaces()
	 * @see org.eclipse.lsat.petri_net.Place#getFinal
	 * @model opposite="final" required="true"
	 * @generated
	 */
	EList<Place> getFinalPlaces();

} // PetriNet
