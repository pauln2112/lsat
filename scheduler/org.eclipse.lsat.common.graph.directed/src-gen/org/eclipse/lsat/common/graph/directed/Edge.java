/**
 */
package org.eclipse.lsat.common.graph.directed;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Edge#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Edge#getSourceNode <em>Source Node</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Edge#getTargetNode <em>Target Node</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Edge#getGraph <em>Graph</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Edge#getAspects <em>Aspects</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getEdge()
 * @model
 * @generated
 */
public interface Edge extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * If the name of either sourceNode or targetNode is not set the special tag <<no_name>> will be used for that part of this edge name.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getEdge_Name()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getName();

	/**
	 * Returns the value of the '<em><b>Source Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Node#getOutgoingEdges <em>Outgoing Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Node</em>' reference.
	 * @see #setSourceNode(Node)
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getEdge_SourceNode()
	 * @see org.eclipse.lsat.common.graph.directed.Node#getOutgoingEdges
	 * @model opposite="outgoingEdges" required="true"
	 * @generated
	 */
	Node getSourceNode();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.Edge#getSourceNode <em>Source Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Node</em>' reference.
	 * @see #getSourceNode()
	 * @generated
	 */
	void setSourceNode(Node value);

	/**
	 * Returns the value of the '<em><b>Target Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Node#getIncomingEdges <em>Incoming Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Node</em>' reference.
	 * @see #setTargetNode(Node)
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getEdge_TargetNode()
	 * @see org.eclipse.lsat.common.graph.directed.Node#getIncomingEdges
	 * @model opposite="incomingEdges" required="true"
	 * @generated
	 */
	Node getTargetNode();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.Edge#getTargetNode <em>Target Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Node</em>' reference.
	 * @see #getTargetNode()
	 * @generated
	 */
	void setTargetNode(Node value);

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' container reference.
	 * @see #setGraph(DirectedGraph)
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getEdge_Graph()
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraph#getEdges
	 * @model opposite="edges" required="true" transient="false"
	 * @generated
	 */
	DirectedGraph<?, ?> getGraph();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.Edge#getGraph <em>Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' container reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(DirectedGraph<?, ?> value);

	/**
	 * Returns the value of the '<em><b>Aspects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.Aspect}<code>&lt;?, ?&gt;</code>.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Aspect#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspects</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getEdge_Aspects()
	 * @see org.eclipse.lsat.common.graph.directed.Aspect#getEdges
	 * @model opposite="edges"
	 * @generated
	 */
	EList<Aspect<?, ?>> getAspects();

} // Edge
