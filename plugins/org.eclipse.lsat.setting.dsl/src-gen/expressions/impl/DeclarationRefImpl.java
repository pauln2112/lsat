/**
 */
package expressions.impl;

import expressions.Declaration;
import expressions.DeclarationRef;
import expressions.ExpressionsPackage;

import java.lang.reflect.InvocationTargetException;

import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link expressions.impl.DeclarationRefImpl#getDeclaration <em>Declaration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeclarationRefImpl extends ExpressionImpl implements DeclarationRef {
	/**
     * The cached value of the '{@link #getDeclaration() <em>Declaration</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDeclaration()
     * @generated
     * @ordered
     */
	protected Declaration declaration;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected DeclarationRefImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ExpressionsPackage.Literals.DECLARATION_REF;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Declaration getDeclaration() {
        if (declaration != null && declaration.eIsProxy())
        {
            InternalEObject oldDeclaration = (InternalEObject)declaration;
            declaration = (Declaration)eResolveProxy(oldDeclaration);
            if (declaration != oldDeclaration)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExpressionsPackage.DECLARATION_REF__DECLARATION, oldDeclaration, declaration));
            }
        }
        return declaration;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Declaration basicGetDeclaration() {
        return declaration;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setDeclaration(Declaration newDeclaration) {
        Declaration oldDeclaration = declaration;
        declaration = newDeclaration;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.DECLARATION_REF__DECLARATION, oldDeclaration, declaration));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal evaluate() {
        return (getDeclaration() != null && getDeclaration().getExpression() !=null && !hasCircularDependencies(this)) ? getDeclaration().getExpression().evaluate() : null ;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public boolean hasCircularDependencies(final EObject object) {
        for(EObject child: object.eCrossReferences()){
            if(this == child || hasCircularDependencies(child)){
                return true;
            }
        }
        for(EObject child: object.eContents()){
            if(this == child || hasCircularDependencies(child)){
                return true;
            }
        }
        return false;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case ExpressionsPackage.DECLARATION_REF__DECLARATION:
                if (resolve) return getDeclaration();
                return basicGetDeclaration();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case ExpressionsPackage.DECLARATION_REF__DECLARATION:
                setDeclaration((Declaration)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case ExpressionsPackage.DECLARATION_REF__DECLARATION:
                setDeclaration((Declaration)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case ExpressionsPackage.DECLARATION_REF__DECLARATION:
                return declaration != null;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case ExpressionsPackage.DECLARATION_REF___EVALUATE:
                return evaluate();
            case ExpressionsPackage.DECLARATION_REF___HAS_CIRCULAR_DEPENDENCIES__EOBJECT:
                return hasCircularDependencies((EObject)arguments.get(0));
        }
        return super.eInvoke(operationID, arguments);
    }

} //DeclarationRefImpl
