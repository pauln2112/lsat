/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.util;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import activity.Move;
import expressions.BigDecimalConstant;
import expressions.Expression;
import expressions.ExpressionsFactory;
import machine.Axis;
import machine.Distance;
import machine.Position;
import machine.Profile;
import machine.impl.MachineQueries;
import setting.MotionSettings;
import setting.PhysicalLocation;
import setting.PhysicalSettings;
import setting.SettingPackage;
import setting.Settings;

public class MoveHelper {
    private final Move move;

    private final Axis axis;

    private final Settings settings;

    public MoveHelper(Move move, Axis axis, Settings settings) {
        this.move = move;
        this.axis = axis;
        this.settings = settings;
    }

    public boolean canModify() {
        return getEditableObject() != null;
    }

    public Object getEditableObject() {
        Object result = getPhysicalLocation(false);
        if (result == null) {
            result = getDistance();
        }
        return null;
    }

    protected PhysicalLocation getPhysicalLocation(boolean atStartOfMove) {
        if (!(move.isPositionMove())) {
            return null;
        }

        MotionSettings motionSettings = getMotionSettings();
        if (null != motionSettings) {
            Position axisPosition = atStartOfMove ? move.getSourcePosition().getPosition(axis)
                    : move.getTargetPosition().getPosition(axis);
            return motionSettings.getLocationSettings().get(axisPosition);
        }
        return null;
    }

    protected Entry<Distance, Expression> getDistance() {
        if (move.isPositionMove()) {
            return null;
        }
        MotionSettings motionSettings = getMotionSettings();
        if (null != motionSettings) {
            for (Entry<Distance, Expression> entry: motionSettings.getDistanceSettings().entrySet()) {
                if (entry.getKey().equals(move.getDistance())) {
                    return entry;
                }
            }
        }
        return null;
    }

    public MotionSettings getMotionSettings() {
        PhysicalSettings physicalSettings = settings.getPhysicalSettings(move.getResource(), move.getPeripheral());
        if (null != physicalSettings) {
            return physicalSettings.getMotionSettings().get(axis);
        }
        return null;
    }

    public BigDecimal getSourcePosition() {
        return getPosition(true, BigDecimal.ZERO);
    }

    public BigDecimal getTargetPosition() {
        return getPosition(false, BigDecimal.ZERO);
    }

    /**
     * Returns the source or target position of this move. if the move is a distance move then the start of the first
     * move predecessor move is considered 0.
     */
    public BigDecimal getPosition(boolean atStartOfMove, BigDecimal defaultValue) {
        if (move.isPositionMove()) {
            PhysicalLocation physicalLocation = getPhysicalLocation(atStartOfMove);
            return physicalLocation != null ? physicalLocation.getDefault() : defaultValue;
        } else {
            if (atStartOfMove) {
                // distance has no meaning full start position.
                return defaultValue;
            }
            Entry<Distance, Expression> entry = getDistance();
            return entry != null ? entry.getValue().evaluate() : defaultValue;
        }
    }

    /**
     * @return target position or distance depending on move type
     */
    public BigDecimal getValue() {
        if (move.isPositionMove()) {
            PhysicalLocation physicalLocation = getPhysicalLocation(false);
            return physicalLocation != null ? physicalLocation.getDefault() : null;
        } else {
            Entry<Distance, Expression> entry = getDistance();
            return entry != null ? entry.getValue().evaluate() : null;
        }
    }

    public void setValue(BigDecimal value) {
        BigDecimalConstant exp = ExpressionsFactory.eINSTANCE.createBigDecimalConstant();
        exp.setValue(value);
        setValue(exp);
    }

    public Expression toExpression(Object value) {
        if (value instanceof Expression) {
            return (Expression)value;
        }
        if (value instanceof BigDecimal) {
            BigDecimalConstant exp = ExpressionsFactory.eINSTANCE.createBigDecimalConstant();
            exp.setValue((BigDecimal)value);
            return exp;
        }
        return null;
    }

    public void setValue(EditingDomain editingDomain, Object value) {
        Expression exp = toExpression(value);
        if (exp == null) {
            return;
        }

        if (move.isPositionMove()) {
            PhysicalLocation physicalLocation = getPhysicalLocation(false);
            if (physicalLocation != null
                    && exp.evaluate().compareTo(physicalLocation.getDefault()) != 0)
            {
                editingDomain.getCommandStack().execute(new SetCommand(editingDomain, physicalLocation,
                        SettingPackage.Literals.PHYSICAL_LOCATION__DEFAULT_EXP, exp));
            }
        } else {
            Entry<Distance, Expression> entry = getDistance();
            if (entry instanceof EObject) {
                editingDomain.getCommandStack().execute(new SetCommand(editingDomain, (EObject)entry,
                        SettingPackage.Literals.DISTANCE_SETTINGS_MAP_ENTRY__VALUE, exp));
            }
        }
    }

    public void setValue(Expression expression) {
        if (move.isPositionMove()) {
            PhysicalLocation physicalLocation = getPhysicalLocation(false);
            if (physicalLocation != null) {
                physicalLocation.setDefaultExp(expression);
            }
        } else {
            Entry<Distance, Expression> entry = getDistance();
            if (entry != null) {
                entry.setValue(expression);
            }
        }
    }

    public static String getDescription(Move move, boolean verbose) {
        StringBuffer description = new StringBuffer();
        description.append("Move ");
        if (verbose) {
            description.append(move.getResource().fqn()).append('.');
            description.append(move.getPeripheral().getName()).append(' ');
        }
        if (move.isPositionMove()) {
            description.append(move.isStopAtTarget() ? "to " : "passing ");
        } else {
            description.append(move.isStopAtTarget() ? "for " : "continuing ");
        }
        description.append(getName(move));
        if (verbose) {
            description.append(" with speed profile ");
            description.append(move.getProfile().getName());
        }
        return description.toString();
    }

    public static Set<Profile> getAvailabeProfiles(Move move) {
        if (move.isPositionMove()) {
            return MachineQueries.getAvailableProfiles(move.getSourcePosition(), move.getTargetPosition());
        } else {
            return move.getPeripheral().getProfiles().stream().collect(Collectors.toSet());
        }
    }

    public static String getName(Move m) {
        if (m.isPositionMove()) {
            return m.getTargetPosition().getName();
        } else {
            return m.getDistance().getName();
        }
    }

    public static Collection<Axis> getSettlingAxes(Move m) {
        if (m.isPositionMove()) {
            return MachineQueries.findPath(m.getSourcePosition(), m.getTargetPosition(), m.getProfile()).getSettling();
        } else {
            return m.getDistance().getSettling();
        }
    }

    public static boolean isSettling(Move m, Axis axis) {
        return getSettlingAxes(m).stream().anyMatch(a -> EcoreUtil.equals(a, axis));
    }
}
