/**
 */
package expressions;

import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Big Decimal Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link expressions.BigDecimalConstant#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see expressions.ExpressionsPackage#getBigDecimalConstant()
 * @model
 * @generated
 */
public interface BigDecimalConstant extends Expression {
	/**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(BigDecimal)
     * @see expressions.ExpressionsPackage#getBigDecimalConstant_Value()
     * @model
     * @generated
     */
	BigDecimal getValue();

	/**
     * Sets the value of the '{@link expressions.BigDecimalConstant#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
	void setValue(BigDecimal value);

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true"
     * @generated
     */
	BigDecimal evaluate();

} // BigDecimalConstant
