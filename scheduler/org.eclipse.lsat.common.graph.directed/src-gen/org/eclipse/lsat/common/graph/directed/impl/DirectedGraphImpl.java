/**
 */
package org.eclipse.lsat.common.graph.directed.impl;

import org.eclipse.lsat.common.graph.directed.Aspect;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Directed Graph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.DirectedGraphImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.DirectedGraphImpl#getSubGraphs <em>Sub Graphs</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.DirectedGraphImpl#getParentGraph <em>Parent Graph</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.DirectedGraphImpl#getEdges <em>Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.DirectedGraphImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.DirectedGraphImpl#getAspects <em>Aspects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DirectedGraphImpl<N extends Node, E extends Edge> extends MinimalEObjectImpl.Container implements DirectedGraph<N, E> {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubGraphs() <em>Sub Graphs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubGraphs()
	 * @generated
	 * @ordered
	 */
	protected EList<DirectedGraph<N, E>> subGraphs;

	/**
	 * The cached value of the '{@link #getEdges() <em>Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<E> edges;

	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<N> nodes;

	/**
	 * The cached value of the '{@link #getAspects() <em>Aspects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAspects()
	 * @generated
	 * @ordered
	 */
	protected EList<Aspect<N, E>> aspects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DirectedGraphImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DirectedGraphPackage.Literals.DIRECTED_GRAPH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectedGraphPackage.DIRECTED_GRAPH__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DirectedGraph<N, E>> getSubGraphs() {
		if (subGraphs == null) {
			subGraphs = new EObjectContainmentWithInverseEList<DirectedGraph<N, E>>(DirectedGraph.class, this, DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS, DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH);
		}
		return subGraphs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public DirectedGraph<N, E> getParentGraph() {
		if (eContainerFeatureID() != DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH) return null;
		return (DirectedGraph<N, E>)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentGraph(DirectedGraph<N, E> newParentGraph, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentGraph, DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentGraph(DirectedGraph<N, E> newParentGraph) {
		if (newParentGraph != eInternalContainer() || (eContainerFeatureID() != DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH && newParentGraph != null)) {
			if (EcoreUtil.isAncestor(this, newParentGraph))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentGraph != null)
				msgs = ((InternalEObject)newParentGraph).eInverseAdd(this, DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS, DirectedGraph.class, msgs);
			msgs = basicSetParentGraph(newParentGraph, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH, newParentGraph, newParentGraph));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<E> getEdges() {
		if (edges == null) {
			edges = new EObjectContainmentWithInverseEList<E>(Edge.class, this, DirectedGraphPackage.DIRECTED_GRAPH__EDGES, DirectedGraphPackage.EDGE__GRAPH);
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<N> getNodes() {
		if (nodes == null) {
			nodes = new EObjectContainmentWithInverseEList<N>(Node.class, this, DirectedGraphPackage.DIRECTED_GRAPH__NODES, DirectedGraphPackage.NODE__GRAPH);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Aspect<N, E>> getAspects() {
		if (aspects == null) {
			aspects = new EObjectContainmentWithInverseEList<Aspect<N, E>>(Aspect.class, this, DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS, DirectedGraphPackage.ASPECT__GRAPH);
		}
		return aspects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<N> allNodesInTopologicalOrder() {
		if (null != getParentGraph()) return getParentGraph().allNodesInTopologicalOrder();
		return org.eclipse.lsat.common.graph.directed.util.DirectedGraphQueries.topologicalOrdering(
			org.eclipse.lsat.common.graph.directed.util.DirectedGraphQueries.allSubNodes(this));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubGraphs()).basicAdd(otherEnd, msgs);
			case DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentGraph((DirectedGraph<N, E>)otherEnd, msgs);
			case DirectedGraphPackage.DIRECTED_GRAPH__EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEdges()).basicAdd(otherEnd, msgs);
			case DirectedGraphPackage.DIRECTED_GRAPH__NODES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getNodes()).basicAdd(otherEnd, msgs);
			case DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAspects()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS:
				return ((InternalEList<?>)getSubGraphs()).basicRemove(otherEnd, msgs);
			case DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH:
				return basicSetParentGraph(null, msgs);
			case DirectedGraphPackage.DIRECTED_GRAPH__EDGES:
				return ((InternalEList<?>)getEdges()).basicRemove(otherEnd, msgs);
			case DirectedGraphPackage.DIRECTED_GRAPH__NODES:
				return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
			case DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS:
				return ((InternalEList<?>)getAspects()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH:
				return eInternalContainer().eInverseRemove(this, DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS, DirectedGraph.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DirectedGraphPackage.DIRECTED_GRAPH__NAME:
				return getName();
			case DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS:
				return getSubGraphs();
			case DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH:
				return getParentGraph();
			case DirectedGraphPackage.DIRECTED_GRAPH__EDGES:
				return getEdges();
			case DirectedGraphPackage.DIRECTED_GRAPH__NODES:
				return getNodes();
			case DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS:
				return getAspects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DirectedGraphPackage.DIRECTED_GRAPH__NAME:
				setName((String)newValue);
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS:
				getSubGraphs().clear();
				getSubGraphs().addAll((Collection<? extends DirectedGraph<N, E>>)newValue);
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH:
				setParentGraph((DirectedGraph<N, E>)newValue);
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__EDGES:
				getEdges().clear();
				getEdges().addAll((Collection<? extends E>)newValue);
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__NODES:
				getNodes().clear();
				getNodes().addAll((Collection<? extends N>)newValue);
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS:
				getAspects().clear();
				getAspects().addAll((Collection<? extends Aspect<N, E>>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DirectedGraphPackage.DIRECTED_GRAPH__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS:
				getSubGraphs().clear();
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH:
				setParentGraph((DirectedGraph<N, E>)null);
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__EDGES:
				getEdges().clear();
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__NODES:
				getNodes().clear();
				return;
			case DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS:
				getAspects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DirectedGraphPackage.DIRECTED_GRAPH__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS:
				return subGraphs != null && !subGraphs.isEmpty();
			case DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH:
				return getParentGraph() != null;
			case DirectedGraphPackage.DIRECTED_GRAPH__EDGES:
				return edges != null && !edges.isEmpty();
			case DirectedGraphPackage.DIRECTED_GRAPH__NODES:
				return nodes != null && !nodes.isEmpty();
			case DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS:
				return aspects != null && !aspects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DirectedGraphPackage.DIRECTED_GRAPH___ALL_NODES_IN_TOPOLOGICAL_ORDER:
				return allNodesInTopologicalOrder();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DirectedGraphImpl
