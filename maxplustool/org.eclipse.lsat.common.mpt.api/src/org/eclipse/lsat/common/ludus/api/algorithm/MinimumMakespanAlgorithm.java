/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.api.algorithm;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.lsat.common.ludus.api.MaxPlusException;
import org.eclipse.lsat.common.ludus.api.MinimumMakespanResult;
import org.eclipse.lsat.common.ludus.backend.algebra.Matrix;
import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.algorithms.CycleCheck;
import org.eclipse.lsat.common.ludus.backend.algorithms.Dijkstra;
import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.fsm.FSM;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;
import org.eclipse.lsat.common.ludus.backend.statespace.ComputeStateSpace;
import org.eclipse.lsat.common.ludus.backend.statespace.MaxPlusStateSpace;
import org.eclipse.lsat.common.ludus.backend.statespace.Transition;
import org.eclipse.lsat.common.mpt.api.UnconnectedResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MinimumMakespanAlgorithm extends MaxPlusAlgorithm {
    private static final Logger LOGGER = LoggerFactory.getLogger(MinimumMakespanAlgorithm.class);

    public static MinimumMakespanResult run(FSM<Location, Edge> fsm, Map<String, Matrix> matrixMap)
            throws MaxPlusException, UnconnectedResourceException
    {
        // Check if the FSM is acyclic.
        if (CycleCheck.check(fsm)) {
            throw new MaxPlusException("Cannot compute the minimum makespan. Input CIF file contains a cycle.");
        }

        // Check that each matrix has the same size.
        Matrix matrixFirst = matrixMap.values().iterator().next();
        for (Matrix m: matrixMap.values()) {
            if (matrixFirst.getRows() != m.getRows()) {
                throw new MaxPlusException("Cannot compute the minimum makespan. Matrices differ in size.");
            }
        }

        long startTime, endTime;

        // Compute the state space.
        startTime = System.currentTimeMillis();
        MaxPlusStateSpace stateSpace = ComputeStateSpace.computeMaxPlusStateSpace(fsm, matrixFirst.getRows(),
                matrixMap);
        endTime = System.currentTimeMillis();

        LOGGER.info("Max-plus state space constructed: " + stateSpace.getVertices().size() + " states and "
                + stateSpace.getEdges().size() + " edges. Generation took " + (endTime - startTime) + " ms.");

        // Perform the makespan computation.
        startTime = System.currentTimeMillis();
        Tuple<Value, List<Transition>> result = Dijkstra.runDijkstra(stateSpace, stateSpace.getInitialConfiguration());
        endTime = System.currentTimeMillis();

        LOGGER.info("Minimum makespan computed using Dijkstra's algorithm in " + (endTime - startTime) + " ms.");

        List<String> listOfEventNames = result.getRight().stream().map(Transition::getEvent)
                .collect(Collectors.toList());
        return new MinimumMakespanResult(result.getLeft().toDouble(), listOfEventNames);
    }
}
