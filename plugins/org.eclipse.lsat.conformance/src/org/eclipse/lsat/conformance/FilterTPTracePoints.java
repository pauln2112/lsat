/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance;

import java.util.List;

import org.eclipse.lsat.common.qvto.util.AbstractModelTransformer;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.trace.TraceModel;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;

/**
 * @deprecated use {@link FilterTPTracePointsJava}
 */
@Deprecated
public class FilterTPTracePoints extends AbstractModelTransformer<FilterTPTracePointsInput, List<TraceModel>> {
    @Override
    protected String getDefaultTransformation() {
        return "/transforms/filterTPTracePoints.qvto";
    }

    @Override
    protected List<TraceModel> doTransformModel(FilterTPTracePointsInput input) throws QvtoTransformationException {
        // Input/Output
        BasicModelExtent inoutTrace = new BasicModelExtent();
        inoutTrace.setContents(input.getTraceModels());

        // Input
        BasicModelExtent inActivities = new BasicModelExtent();
        inActivities.setContents(input.getActivities());

        execute(inoutTrace, inActivities);

        return validateMany(TraceModel.class, inoutTrace);
    }
}
