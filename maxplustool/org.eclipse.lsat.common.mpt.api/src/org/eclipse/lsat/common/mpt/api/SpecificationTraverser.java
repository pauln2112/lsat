/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.lsat.common.mpt.FSM;
import org.eclipse.lsat.common.mpt.Graph;
import org.eclipse.lsat.common.mpt.MaxPlusSpecification;

/**
 * Functions to retrieve specific data objects from a max-plus specification.
 *
 * @author Bram van der Sanden
 */
public final class SpecificationTraverser {
    private SpecificationTraverser() {
        // Empty
    }

    /**
     * Find all FSMs in the max-plus specification.
     *
     * @param specification input specification
     * @return FSMImpl in the specification if it exists, otherwise empty.
     */
    public static List<FSM> findAllFSMs(MaxPlusSpecification specification) {
        List<FSM> fsmList = new ArrayList<>();
        for (Graph<?, ?> g: specification.getGraphs()) {
            if (g instanceof FSM) {
                // Found an FSM.
                fsmList.add((FSM)g);
            }
        }
        return fsmList;
    }

    /**
     * Find an FSM in the max-plus specification.
     *
     * @param specification input specification
     * @return FSMImpl in the specification if it exists, otherwise empty.
     */
    public static Optional<FSM> findFSM(MaxPlusSpecification specification) {
        for (Graph<?, ?> g: specification.getGraphs()) {
            if (g instanceof FSM) {
                // Found an FSMImpl.
                FSM result = (FSM)g;
                return Optional.of(result);
            }
        }
        // No FSMImpl found.
        return Optional.empty();
    }

    /**
     * Find an FSM with the given name in the max-plus specification.
     *
     * @param specification input specification
     * @param fsmName name of FSMImpl
     * @return FSMImpl with the given name in the specification if it exists, otherwise empty.
     */
    public static Optional<FSM> findFSM(MaxPlusSpecification specification, String fsmName) {
        for (Graph<?, ?> g: specification.getGraphs()) {
            if (g.getName().equals(fsmName) && g instanceof FSM) {
                // Found an FSMImpl with the desired name.
                FSM result = (FSM)g;
                return Optional.of(result);
            }
        }
        // No FSMImpl found with the desired name.
        return Optional.empty();
    }
}
