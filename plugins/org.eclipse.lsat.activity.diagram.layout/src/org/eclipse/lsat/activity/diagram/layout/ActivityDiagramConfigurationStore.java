/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.layout;

import java.util.Map;

import org.eclipse.elk.conn.gmf.GmfLayoutConfigurationStore;
import org.eclipse.elk.conn.gmf.IEditPartFilter;
import org.eclipse.elk.core.options.CoreOptions;
import org.eclipse.elk.core.service.ILayoutConfigurationStore;
import org.eclipse.gef.EditPart;
import org.eclipse.sirius.diagram.ui.edit.api.part.IDDiagramEditPart;
import org.eclipse.ui.IWorkbenchPart;

import com.google.common.collect.Maps;
import com.google.inject.Inject;

public class ActivityDiagramConfigurationStore extends GmfLayoutConfigurationStore {
    /**
     * Provider for ActivityDiagram layout configuration stores.
     */
    public static final class Provider implements ILayoutConfigurationStore.Provider {
        @Inject
        private IEditPartFilter editPartFilter;

        @Override
        public ILayoutConfigurationStore get(final IWorkbenchPart workbenchPart, final Object context) {
            if (context instanceof EditPart) {
                try {
                    return new ActivityDiagramConfigurationStore((EditPart)context, editPartFilter);
                } catch (IllegalArgumentException e) {
                    // Fall back to null
                }
            }
            return null;
        }
    }

    protected final EditPart editPart;

    protected ActivityDiagramConfigurationStore(EditPart theeditPart, IEditPartFilter filter) {
        super(theeditPart, filter);
        this.editPart = theeditPart;
    }

    @Override
    protected Map<String, Object> getDefaultOptions() {
        Map<String, Object> result = super.getDefaultOptions();
        if (editPart instanceof IDDiagramEditPart) {
            if (null == result) {
                result = Maps.newHashMapWithExpectedSize(1);
            }
            result.put(CoreOptions.ALGORITHM.getId(), ActivityDiagramLayoutOptions.ALGORITHM_ID);
        }
        return result;
    }
}
