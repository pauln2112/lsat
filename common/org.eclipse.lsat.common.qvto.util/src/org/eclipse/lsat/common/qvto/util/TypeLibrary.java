/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util;

import static org.eclipse.ocl.ecore.internal.OCLStandardLibraryImpl.INVALID;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.m2m.qvt.oml.blackbox.java.Module;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;

@Module(packageURIs = "http://www.eclipse.org/emf/2002/Ecore")
public class TypeLibrary {
    private static final HashMap<Class<?>, Class<?>> PRIMITIVE_TYPE_WRAPPERS = new HashMap<Class<?>, Class<?>>(8);

    static {
        PRIMITIVE_TYPE_WRAPPERS.put(Byte.TYPE, Byte.class);
        PRIMITIVE_TYPE_WRAPPERS.put(Short.TYPE, Short.class);
        PRIMITIVE_TYPE_WRAPPERS.put(Integer.TYPE, Integer.class);
        PRIMITIVE_TYPE_WRAPPERS.put(Long.TYPE, Long.class);
        PRIMITIVE_TYPE_WRAPPERS.put(Float.TYPE, Float.class);
        PRIMITIVE_TYPE_WRAPPERS.put(Double.TYPE, Double.class);
        PRIMITIVE_TYPE_WRAPPERS.put(Character.TYPE, Character.class);
        PRIMITIVE_TYPE_WRAPPERS.put(Boolean.TYPE, Boolean.class);
        PRIMITIVE_TYPE_WRAPPERS.put(Void.TYPE, void.class);
    }

    /**
     * This method converts the String value to the requested context type, supporting 2 alternatives, either a static
     * 'valueOf(String)' method or a constructor which takes a String as an argument
     *
     * @param self context type
     * @param value String value
     * @return the converted value
     * @throws IllegalArgumentException if conversion fails
     */
    @Operation(contextual = true)
    public Object valueOf(EClassifier self, String value) {
        Class<?> type = self.getInstanceClass();
        if (String.class.equals(type)) {
            // Nothing to do here
            return value;
        }

        // For primitives use its type wrapper
        if (type.isPrimitive()) {
            type = PRIMITIVE_TYPE_WRAPPERS.get(type);
        }

        Exception cause = null;
        try {
            try {
                Method m = type.getMethod("valueOf", String.class);
                return m.invoke(null, value);
            } catch (NoSuchMethodException e) {
                Constructor<?> c = type.getConstructor(String.class);
                return c.newInstance(value);
            }
        } catch (Exception e) {
            cause = e;
        }
        throw new IllegalArgumentException(
                "Failed to convert value " + value + " to type: " + self.getInstanceClassName(), cause);
    }

    /**
     * Removes and returns the first list element. Returns invalid for an empty list.<br>
     * <code>pre: 1 <= size()</code><br>
     * <code>post: size() = size@pre() - 1</code><br>
     * <code>post:
     * Sequence{1..size()}->forAll(i | at(i) = at@pre(i+1))</code><br>
     * <code>post: result = at@pre(1)</code>
     *
     * @param self the list (context)
     * @return the first element or invalid for an empty list
     * @see <a href="http://www.omg.org/spec/QVT/1.2/">http://www.omg.org/spec/QVT/1.2/</a>
     */
    @Operation(contextual = true)
    @SuppressWarnings({"unchecked", "restriction"})
    public <T> T removeFirst(List<T> self) {
        return self.isEmpty() ? (T)INVALID : self.remove(0);
    }

    /**
     * Removes the first occurrence of the specified element from this list, if it is present (optional operation). If
     * this list does not contain the element, it is unchanged. More formally, removes the element with the lowest index
     * <tt>i</tt> such that <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt> (if such an element
     * exists). Returns <tt>true</tt> if this list contained the specified element (or equivalently, if this list
     * changed as a result of the call).
     *
     * @param o element to be removed from this list, if present
     * @return <tt>true</tt> if this list contained the specified element
     * @throws ClassCastException if the type of the specified element is incompatible with this list
     *     (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this list does not permit null elements
     *     (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws UnsupportedOperationException if the <tt>remove</tt> operation is not supported by this list
     *
     * @see List#remove(Object)
     */
    @Operation(contextual = true)
    public <T> boolean remove(List<T> self, Object o) {
        return self.remove(o);
    }

    @Operation(contextual = true)
    public Collection<EObject> loadResource(String self, Resource context) {
        if (null == context || null == context.getResourceSet()) {
            throw new IllegalArgumentException("The context resource is not loaded in a resource set");
        }
        URI uri = URI.createURI(self);
        uri = uri.resolve(context.getURI());
        Resource loadedResource = context.getResourceSet().getResource(uri, true);
        return loadedResource.getContents();
    }
}
