/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.design;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.Wizard;

import activity.EventAction;

public class AddEventActionWizard extends Wizard {
    private final AddEventActionPage eventActionPage;

    private EventAction eventAction;

    public AddEventActionWizard(Collection<EObject> roots, String initialActionName) {
        eventActionPage = new AddEventActionPage(roots, initialActionName);
    }

    @Override
    public void addPages() {
        addPage(eventActionPage);
    }

    @Override
    public boolean performFinish() {
        eventAction = eventActionPage.createEventAction();
        return true;
    }

    public EventAction getEventAction() {
        return eventAction;
    }
}
