/**
 */
package org.eclipse.lsat.common.scheduler.resources;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Set</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesPackage#getResourceSet()
 * @model
 * @generated
 */
public interface ResourceSet extends ResourceContainer, AbstractResource {
} // ResourceSet
