/**
 */
package timing.impl;

import expressions.Expression;
import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import timing.FixedValue;
import timing.TimingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fixed Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link timing.impl.FixedValueImpl#getValue <em>Value</em>}</li>
 *   <li>{@link timing.impl.FixedValueImpl#getValueExp <em>Value Exp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FixedValueImpl extends MinimalEObjectImpl.Container implements FixedValue {
	/**
     * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getValue()
     * @generated
     * @ordered
     */
	protected static final BigDecimal VALUE_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getValueExp() <em>Value Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getValueExp()
     * @generated
     * @ordered
     */
	protected Expression valueExp;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected FixedValueImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return TimingPackage.Literals.FIXED_VALUE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getValue() {
        return valueExp==null ? null:  valueExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getValueExp() {
        return valueExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetValueExp(Expression newValueExp, NotificationChain msgs) {
        Expression oldValueExp = valueExp;
        valueExp = newValueExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimingPackage.FIXED_VALUE__VALUE_EXP, oldValueExp, newValueExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setValueExp(Expression newValueExp) {
        if (newValueExp != valueExp)
        {
            NotificationChain msgs = null;
            if (valueExp != null)
                msgs = ((InternalEObject)valueExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimingPackage.FIXED_VALUE__VALUE_EXP, null, msgs);
            if (newValueExp != null)
                msgs = ((InternalEObject)newValueExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimingPackage.FIXED_VALUE__VALUE_EXP, null, msgs);
            msgs = basicSetValueExp(newValueExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, TimingPackage.FIXED_VALUE__VALUE_EXP, newValueExp, newValueExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case TimingPackage.FIXED_VALUE__VALUE_EXP:
                return basicSetValueExp(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case TimingPackage.FIXED_VALUE__VALUE:
                return getValue();
            case TimingPackage.FIXED_VALUE__VALUE_EXP:
                return getValueExp();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case TimingPackage.FIXED_VALUE__VALUE_EXP:
                setValueExp((Expression)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case TimingPackage.FIXED_VALUE__VALUE_EXP:
                setValueExp((Expression)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case TimingPackage.FIXED_VALUE__VALUE:
                return VALUE_EDEFAULT == null ? getValue() != null : !VALUE_EDEFAULT.equals(getValue());
            case TimingPackage.FIXED_VALUE__VALUE_EXP:
                return valueExp != null;
        }
        return super.eIsSet(featureID);
    }

} //FixedValueImpl
