/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api;

import java.util.List;

public class MaximumMakespanResult {
    private final double makespan;

    private final List<String> activities;

    public MaximumMakespanResult(double makespan, List<String> events) {
        this.makespan = makespan;
        this.activities = events;
    }

    public double getMakespan() {
        return makespan;
    }

    public List<String> getActivities() {
        return activities;
    }

    @Override
    public String toString() {
        return "MaximumMakespanResult [makespan=" + makespan + ", activities=" + activities + "]";
    }
}
