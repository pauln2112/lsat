/**
 */
package lsat_graph.util;

import activity.EventAction;
import activity.ResourceAction;
import lsat_graph.*;
import org.eclipse.lsat.common.graph.directed.Aspect;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;

import org.eclipse.lsat.common.scheduler.resources.AbstractResource;
import org.eclipse.lsat.common.scheduler.resources.NamedResource;
import org.eclipse.lsat.common.scheduler.resources.Resource;

import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see lsat_graph.lsat_graphPackage
 * @generated
 */
public class lsat_graphAdapterFactory extends AdapterFactoryImpl {
	/**
     * The cached model package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static lsat_graphPackage modelPackage;

	/**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public lsat_graphAdapterFactory() {
        if (modelPackage == null)
        {
            modelPackage = lsat_graphPackage.eINSTANCE;
        }
    }

	/**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
	@Override
	public boolean isFactoryForType(Object object) {
        if (object == modelPackage)
        {
            return true;
        }
        if (object instanceof EObject)
        {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

	/**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected lsat_graphSwitch<Adapter> modelSwitch =
		new lsat_graphSwitch<Adapter>()
        {
            @Override
            public Adapter casePeripheralActionTask(PeripheralActionTask object)
            {
                return createPeripheralActionTaskAdapter();
            }
            @Override
            public Adapter caseClaimTask(ClaimTask object)
            {
                return createClaimTaskAdapter();
            }
            @Override
            public Adapter caseReleaseTask(ReleaseTask object)
            {
                return createReleaseTaskAdapter();
            }
            @Override
            public Adapter caseClaimReleaseResource(ClaimReleaseResource object)
            {
                return createClaimReleaseResourceAdapter();
            }
            @Override
            public Adapter caseDispatchGroupTask(DispatchGroupTask object)
            {
                return createDispatchGroupTaskAdapter();
            }
            @Override
            public Adapter caseDispatchGroupResource(DispatchGroupResource object)
            {
                return createDispatchGroupResourceAdapter();
            }
            @Override
            public Adapter casePeripheralResource(PeripheralResource object)
            {
                return createPeripheralResourceAdapter();
            }
            @Override
            public Adapter caseDispatchGraph(DispatchGraph object)
            {
                return createDispatchGraphAdapter();
            }
            @Override
            public Adapter caseClaimedByScheduledTask(ClaimedByScheduledTask object)
            {
                return createClaimedByScheduledTaskAdapter();
            }
            @Override
            public <T extends ResourceAction> Adapter caseActionTask(ActionTask<T> object)
            {
                return createActionTaskAdapter();
            }
            @Override
            public Adapter caseStochasticAnnotation(StochasticAnnotation object)
            {
                return createStochasticAnnotationAdapter();
            }
            @Override
            public Adapter caseEventStatusTask(EventStatusTask object)
            {
                return createEventStatusTaskAdapter();
            }
            @Override
            public <T extends Task> Adapter caseEventAnnotation(EventAnnotation<T> object)
            {
                return createEventAnnotationAdapter();
            }
            @Override
            public <T extends EventAction> Adapter caseEventTask(EventTask<T> object)
            {
                return createEventTaskAdapter();
            }
            @Override
            public Adapter caseRequireTask(RequireTask object)
            {
                return createRequireTaskAdapter();
            }
            @Override
            public Adapter caseRaiseTask(RaiseTask object)
            {
                return createRaiseTaskAdapter();
            }
            @Override
            public Adapter caseNode(Node object)
            {
                return createNodeAdapter();
            }
            @Override
            public Adapter caseTask(Task object)
            {
                return createTaskAdapter();
            }
            @Override
            public Adapter caseNamedResource(NamedResource object)
            {
                return createNamedResourceAdapter();
            }
            @Override
            public Adapter caseAbstractResource(AbstractResource object)
            {
                return createAbstractResourceAdapter();
            }
            @Override
            public Adapter caseResource(Resource object)
            {
                return createResourceAdapter();
            }
            @Override
            public <N extends Node, E extends Edge> Adapter caseDirectedGraph(DirectedGraph<N, E> object)
            {
                return createDirectedGraphAdapter();
            }
            @Override
            public <T extends Task> Adapter caseTaskDependencyGraph(TaskDependencyGraph<T> object)
            {
                return createTaskDependencyGraphAdapter();
            }
            @Override
            public <T extends Task> Adapter caseScheduledTask(ScheduledTask<T> object)
            {
                return createScheduledTaskAdapter();
            }
            @Override
            public <N extends Node, E extends Edge> Adapter caseAspect(Aspect<N, E> object)
            {
                return createAspectAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object)
            {
                return createEObjectAdapter();
            }
        };

	/**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
	@Override
	public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject)target);
    }


	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.PeripheralActionTask <em>Peripheral Action Task</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.PeripheralActionTask
     * @generated
     */
	public Adapter createPeripheralActionTaskAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.ClaimTask <em>Claim Task</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.ClaimTask
     * @generated
     */
	public Adapter createClaimTaskAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.ReleaseTask <em>Release Task</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.ReleaseTask
     * @generated
     */
	public Adapter createReleaseTaskAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.ClaimReleaseResource <em>Claim Release Resource</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.ClaimReleaseResource
     * @generated
     */
	public Adapter createClaimReleaseResourceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.DispatchGroupTask <em>Dispatch Group Task</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.DispatchGroupTask
     * @generated
     */
	public Adapter createDispatchGroupTaskAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.DispatchGroupResource <em>Dispatch Group Resource</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.DispatchGroupResource
     * @generated
     */
	public Adapter createDispatchGroupResourceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.PeripheralResource <em>Peripheral Resource</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.PeripheralResource
     * @generated
     */
	public Adapter createPeripheralResourceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.DispatchGraph <em>Dispatch Graph</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.DispatchGraph
     * @generated
     */
	public Adapter createDispatchGraphAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.ClaimedByScheduledTask <em>Claimed By Scheduled Task</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.ClaimedByScheduledTask
     * @generated
     */
	public Adapter createClaimedByScheduledTaskAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.ActionTask <em>Action Task</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.ActionTask
     * @generated
     */
	public Adapter createActionTaskAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.StochasticAnnotation <em>Stochastic Annotation</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.StochasticAnnotation
     * @generated
     */
	public Adapter createStochasticAnnotationAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.EventStatusTask <em>Event Status Task</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.EventStatusTask
     * @generated
     */
	public Adapter createEventStatusTaskAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link lsat_graph.EventAnnotation <em>Event Annotation</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.EventAnnotation
     * @generated
     */
    public Adapter createEventAnnotationAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link lsat_graph.EventTask <em>Event Task</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.EventTask
     * @generated
     */
    public Adapter createEventTaskAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link lsat_graph.RequireTask <em>Require Task</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.RequireTask
     * @generated
     */
    public Adapter createRequireTaskAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link lsat_graph.RaiseTask <em>Raise Task</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see lsat_graph.RaiseTask
     * @generated
     */
    public Adapter createRaiseTaskAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.graph.directed.Node <em>Node</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.graph.directed.Node
     * @generated
     */
	public Adapter createNodeAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.scheduler.graph.Task <em>Task</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.scheduler.graph.Task
     * @generated
     */
	public Adapter createTaskAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.scheduler.resources.NamedResource <em>Named Resource</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.scheduler.resources.NamedResource
     * @generated
     */
	public Adapter createNamedResourceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.scheduler.resources.AbstractResource <em>Abstract Resource</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.scheduler.resources.AbstractResource
     * @generated
     */
	public Adapter createAbstractResourceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.scheduler.resources.Resource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.scheduler.resources.Resource
     * @generated
     */
	public Adapter createResourceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.graph.directed.DirectedGraph <em>Directed Graph</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.graph.directed.DirectedGraph
     * @generated
     */
	public Adapter createDirectedGraphAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph <em>Task Dependency Graph</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph
     * @generated
     */
	public Adapter createTaskDependencyGraphAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask <em>Scheduled Task</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledTask
     * @generated
     */
	public Adapter createScheduledTaskAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.graph.directed.Aspect <em>Aspect</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.graph.directed.Aspect
     * @generated
     */
	public Adapter createAspectAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
	public Adapter createEObjectAdapter() {
        return null;
    }

} //lsat_graphAdapterFactory
