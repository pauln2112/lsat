/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend

import com.google.common.collect.Iterables
import java.util.Collections
import java.util.Comparator
import java.util.Iterator
import java.util.LinkedHashSet
import java.util.List
import java.util.Map
import java.util.Objects
import org.eclipse.lsat.common.queries.IterableQueries
import org.eclipse.lsat.common.util.BranchIterable
import org.eclipse.lsat.common.util.BranchIterator
import org.eclipse.lsat.common.util.IterableUtil
import org.eclipse.lsat.common.util.LoggableIterable
import org.eclipse.lsat.common.util.ProcessingIterator
import org.eclipse.lsat.common.util.UniqueIterable

import static java.util.Arrays.asList

class Queries {
    /**
     * A helper method that returns the {@code element} as an {@link Iterable}
     * containing the {@code element} if the {@code element} if not {@code null},
     * else an empty {@link Iterable} is returned.
     */
    static def <T> Iterable<T> notNull(T element) {
        return element === null ? Collections::emptyList : Collections::singleton(element)
    }

    /**
     * From a {@link BranchIterator} find the nearest node in each branch that matches the {@code predicate}
     */
    static def <T> Iterable<T> findNearest(BranchIterable<T> source, (T)=>boolean predicate) {
        return IterableQueries.findNearest(source, predicate)
    }

    /**
     * From a {@link BranchIterator} find the nearest node in each branch that is an instance of {@code type}
     */
    static def <T, R> Iterable<R> findNearest(BranchIterable<T> source, Class<R> type) {
        return IterableQueries.findNearest(source, type)
    }

    /**
     * Returns the elements of each branch of a {@link BranchIterable} up to the
     * element that matches the {@code predicate}. The matched element itself is
     * excluded from the result.
     * 
     * @return an iterator containing all elements of {@code iterable} up to (thus
     *         excluding) the first element that matches the {@code predicate} per
     *         branch.
     */
    static def <T> BranchIterable<T> until(BranchIterable<T> source, (T)=>boolean predicate) {
        return IterableQueries.until(source, predicate)
    }

    /**
     * Returns the elements of each branch of a {@link BranchIterable} up to the
     * element that matches the {@code predicate}. The matched element itself is
     * included in the result.
     * 
     * @return an iterator containing all elements of {@code iterable} up to and
     *         including the first element that matches the {@code predicate} per
     *         branch.
     */
    static def <T> BranchIterable<T> upToAndIncluding(BranchIterable<T> source, (T)=>boolean predicate) {
        return IterableQueries.upToAndIncluding(source, predicate)
    }

    /**
     * Collects all children and children's children without the need for creating a temporary Collection instance.
     * Behaves like OCL <tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>)
     */
    static def <T> BranchIterable<T> closure(Iterable<? extends T> source, (T)=>Iterable<? extends T> transformation) {
        return IterableQueries.closure(source, transformation)
    }

    /**
     * Collects all children and children's children without the need for creating a temporary Collection instance.
     * Behaves like OCL: <ul>
     * <li><tt>!includeSource</tt>: <tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>)</li>
     * <li><tt>includeSource</tt>: <tt>source</tt>->union(<tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>))</li>
     * </ul>
     */
    static def <T> BranchIterable<T> closure(Iterable<? extends T> source, boolean includeSource,
        (T)=>Iterable<? extends T> transformation) {
        return IterableQueries.closure(source, includeSource, transformation)
    }

    /**
     * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
     * <code>false</code>. All returned elements will pass the predicate. No need for creating a temporary Collection instance.<br>
     */
    static def <T> BranchIterable<T> closureWhile(Iterable<? extends T> source,
        (T)=>Iterable<? extends T> transformation, (T)=>boolean predicate) {
        return IterableQueries.closureWhile(source, transformation, predicate)
    }

    /**
     * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
     * <code>false</code>. All returned elements will pass the predicate. No need for creating a temporary Collection instance.<br>
     */
    static def <T> BranchIterable<T> closureWhile(Iterable<? extends T> source, boolean includeSource,
        (T)=>Iterable<? extends T> transformation, (T)=>boolean predicate) {
        return IterableQueries.closureWhile(source, includeSource, transformation, predicate)
    }

    /**
     * Collects child, child's child, etc without the need for creating a temporary Collection instance.
     * Behaves like OCL <tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>)
     */
    static def <T> BranchIterable<T> closureOne(Iterable<? extends T> source, (T)=>T transformation) {
        return IterableQueries.closureOne(source, transformation)
    }

    /**
     * Collects child, child's child, etc without the need for creating a temporary Collection instance.
     * Behaves like OCL: <ul>
     * <li><tt>!includeSource</tt>: <tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>)</li>
     * <li><tt>includeSource</tt>: <tt>source</tt>->union(<tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>))</li>
     * </ul>
     */
    static def <T> BranchIterable<T> closureOne(Iterable<? extends T> source, boolean includeSource,
        (T)=>T transformation) {
        return IterableQueries.closureOne(source, includeSource, transformation)
    }

    /**
     * Collects all children and children's children and stops a sub branch as soon as the predicate evaluates to
     * <code>false</code>. All returned elements will pass the predicate. No need for creating a temporary Collection instance.<br>
     */
    static def <T> BranchIterable<T> closureOneWhile(Iterable<? extends T> source, (T)=>T transformation,
        (T)=>boolean predicate) {
        return IterableQueries.closureOneWhile(source, transformation, predicate)
    }

    /**
     * Collects all children and children's children and stops a sub branch as soon as the predicate evaluates to
     * <code>false</code>. All returned elements will pass the predicate. No need for creating a temporary Collection instance.<br>
     */
    static def <T> BranchIterable<T> closureOneWhile(Iterable<? extends T> source, boolean includeSource,
        (T)=>T transformation, (T)=>boolean predicate) {
        return IterableQueries.closureOneWhile(source, includeSource, transformation, predicate)
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterable</tt> without the need for
     * creating a temporary Collection instance. Note that the resulting iterator might contain a tree node multiple
     * times if elements in <tt>iterable</tt> share this tree node as common ancestor.
     */
    static def <T> BranchIterable<T> climbTree(Iterable<? extends T> source, (T)=>T transformation) {
        return IterableQueries.climbTree(source, transformation);
    }

    /**
     * Collects self (only if <tt>includeSelf</tt> is <code>true</code>), parent and
     * parent's parents per element in this iterable without the need for creating a
     * temporary Collection instance. Note that the resulting iterator might contain
     * a tree node multiple times if elements in this iterable share this tree node
     * as common ancestor. You can use {@link #unique()} to filter these duplicates.
     */
    static def <T> BranchIterable<T> climbTree(Iterable<? extends T> source, boolean includeSelf,
        (T)=>T transformation) {
        return IterableQueries.climbTree(source, includeSelf, transformation);
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> and stops each sub branch as
     * soon as the predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance. Note that the
     * resulting iterator might contain a tree node multiple times if elements in <tt>iterator</tt> share this tree node
     * as common ancestor.
     */
    static def <T> BranchIterable<T> climbTreeWhile(Iterable<? extends T> source, (T)=>T transformation,
        (T)=>boolean predicate) {
        return IterableQueries.climbTreeWhile(source, transformation, predicate);
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> and stops each sub branch as
     * soon as the predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance. Note that the
     * resulting iterator might contain a tree node multiple times if elements in <tt>iterator</tt> share this tree node
     * as common ancestor.
     */
    static def <T> BranchIterable<T> climbTreeWhile(Iterable<? extends T> source, boolean includeSelf,
        (T)=>T transformation, (T)=>boolean predicate) {
        return IterableQueries.climbTreeWhile(source, includeSelf, transformation, predicate);
    }

    /**
     * Collects all descendants (children and children's children; depth first) without the need for creating a
     * temporary Collection instance.
     */
    static def <T> BranchIterable<T> walkTree(Iterable<? extends T> source, (T)=>Iterable<? extends T> transformation) {
        return IterableQueries.walkTree(source, transformation);
    }

    /**
     * Collects self (only if <tt>includeSelf</tt> is <code>true</code>), all
     * children and children's children - depth first - without the need for
     * creating a temporary Collection instance.
     */
    static def <T> BranchIterable<T> walkTree(Iterable<? extends T> source, boolean includeSelf,
        (T)=>Iterable<? extends T> transformation) {
        return IterableQueries.walkTree(source, includeSelf, transformation);
    }

    /**
     * Collects all descendants (children and children's children; depth first) and stops each sub branch as soon as the
     * predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     */
    static def <T> BranchIterable<T> walkTreeWhile(Iterable<? extends T> source,
        (T)=>Iterable<? extends T> transformation, (T)=>boolean predicate) {
        return IterableQueries.walkTreeWhile(source, transformation, predicate);
    }

    /**
     * Collects all descendants (children and children's children; depth first) and stops each sub branch as soon as the
     * predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     */
    static def <T> BranchIterable<T> walkTreeWhile(Iterable<? extends T> source, boolean includeSelf,
        (T)=>Iterable<? extends T> transformation, (T)=>boolean predicate) {
        return IterableQueries.walkTreeWhile(source, includeSelf, transformation, predicate);
    }

    /**
     * Selects elements without the need for creating a temporary Collection instance.
     * Behaves like OCL <tt>source</tt>->select(e | predicate.apply(e))
     */
    static def <T> Iterable<T> select(Iterable<T> source, (T)=>boolean predicate) {
        return source.filter(predicate)
    }

    /**
     * Selects elements without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
     * Behaves like OCL <tt>source</tt>->xselect(e | predicate.apply(e))
     */
    static def <T> Iterable<T> xselect(Iterable<T> source, (T)=>boolean predicate) {
        return source.filterNull.filter(predicate)
    }

    /**
     * Collects single attribute values without the need for creating a temporary Collection instance.
     * Behaves like OCL <tt>source</tt>->collect(e | transformation.apply(e))
     */
    static def <T, R> Iterable<R> collectOne(Iterable<T> source, (T)=>R transformation) {
        return source.map(transformation)
    }

    /**
     * Collects single attribute values without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
     * Behaves like OCL <tt>source</tt>->xcollect(e | transformation.apply(e))
     */
    static def <T, R> Iterable<R> xcollectOne(Iterable<T> source, (T)=>R transformation) {
        return source.map(transformation).filterNull
    }

    /**
     * Collects elements without the need for creating a temporary Collection instance.
     * Behaves like OCL <tt>source</tt>->collect(e | transformation.apply(e))
     */
    static def <T, R> Iterable<R> collect(Iterable<T> source, (T)=>Iterable<? extends R> transformation) {
        return source.map(transformation).flatten
    }

    /**
     * Collects elements without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
     * Behaves like OCL <tt>source</tt>->xcollect(e | transformation.apply(e))
     */
    static def <T, R> Iterable<R> xcollect(Iterable<T> source, (T)=>Iterable<? extends R> transformation) {
        return source.map(transformation).flatten.filterNull
    }

    /**
     * Filters the iteration, only keeping items that are instance of the
     * specified type. <br>
     * Behaves like OCL <tt>source</tt>->objectsOfKind(<tt>Output</tt>)
     */
    static def <T> Iterable<T> objectsOfKind(Iterable<?> source, Class<T> type) {
        return source.filter(type)
    }

    /**
     * Casts this iteration to a specific type. <br>
     * Behaves like OCL <tt>source</tt>->oclAsType(<tt>Output</tt>)
     */
    static def <T> Iterable<T> asType(Iterable<?> source, Class<T> type) {
        return source.map[it as T]
    }

    /**
     * Behaves like OCL <tt>source</tt>->union(<tt>other</tt>)
     */
    static def <T> Iterable<T> union(Iterable<? extends T> source, Iterable<? extends T> other) {
        return Iterables.concat(asList(source, other))
    }

    /**
     * Behaves like OCL <tt>source</tt>->union(<tt>other</tt>)
     */
    static def <T> Iterable<T> union(Iterable<? extends T> source, T... other) {
        return Iterables.concat(asList(source, asList(other)))
    }

    /**
     * Behaves like OCL <tt>this</tt>->excluding(<tt>other</tt>)
     */
    static def <T> Iterable<T> excluding(Iterable<T> iterable, Iterable<?> excludes) {
        return iterable.reject[excludes.includes(it)];
    }

    /**
     * Behaves like OCL <tt>this</tt>->excluding(<tt>other</tt>)
     */
    @SafeVarargs
    static def <T> Iterable<T> excluding(Iterable<T> iterable, Object... excludes) {
        return excluding(iterable, IterableUtil.asList(excludes));
    }

    static def <T> Iterable<T> unique(Iterable<T> source) {
        return source.unique(true)
    }

    static def <T> Iterable<T> unique(Iterable<T> source, boolean useEquals) {
        return new UniqueIterable(source, useEquals)
    }

    static def <L, R> Iterable<Pair<L, R>> product(Iterable<L> left, Iterable<R> right) {
        return left.map[l|right.map[r|l -> r]].flatten
    }

    static def <T> Iterable<Pair<T, T>> product(Iterable<T> source) {
        return source.map[l|source.after[it === l].map[r|l -> r]].flatten
    }

    static def <T> boolean includes(Iterable<T> source, Object item) {
        return Iterables::contains(source, item)
    }

    static def <T> boolean excludes(Iterable<T> source, Object item) {
        return !Iterables::contains(source, item)
    }

    /**
     * Checks for duplicates.
     * 
     * @param source
     *            the element to check for duplicates.
     * @return returns true if there are duplicates in the element. 
     */
    static def boolean containsDuplicates(Iterable<?> source) {
        val set = newHashSet();
        return !source.forall[e|set.add(e)]
    }

    /**
     * Returns the maximum element of the given iterable, according to the
     * <i>natural ordering</i> of its elements. All elements in the iterable
     * must implement the <tt>Comparable</tt> interface. Furthermore, all
     * elements in the iterable must be <i>mutually comparable</i> (that is,
     * <tt>e1.compareTo(e2)</tt> must not throw a <tt>ClassCastException</tt>
     * for any elements <tt>e1</tt> and <tt>e2</tt> in the iterable).
     * <p>
     * 
     * This method iterates over the entire iterable, hence it requires time
     * proportional to the size of the iterable.
     * 
     * @param iterable
     *            the iterable whose maximum element is to be determined.
     * @param _default
     *            result if the collection is empty.
     * @return the maximum element of the given iterable, according to the
     *         <i>natural ordering</i> of its elements, or <tt>_default</tt> if
     *         the iterable is empty.
     * @see Comparable
     */
    static def <T extends Comparable<? super T>> T max(Iterable<T> source, T _default) {
        if(source.empty) _default else source.max
    }

    /**
     * Returns the minimum element of the given iterable, according to the
     * <i>natural ordering</i> of its elements. All elements in the iterable
     * must implement the <tt>Comparable</tt> interface. Furthermore, all
     * elements in the iterable must be <i>mutually comparable</i> (that is,
     * <tt>e1.compareTo(e2)</tt> must not throw a <tt>ClassCastException</tt>
     * for any elements <tt>e1</tt> and <tt>e2</tt> in the iterable).
     * <p>
     * 
     * This method iterates over the entire iterable, hence it requires time
     * proportional to the size of the iterable.
     * 
     * @param iterable
     *            the iterable whose minimum element is to be determined.
     * @param _default
     *            result if the collection is empty.
     * @return the minimum element of the given iterable, according to the
     *         <i>natural ordering</i> of its elements, or <tt>_default</tt> if
     *         the iterable is empty.
     * @see Comparable
     */
    static def <T extends Comparable<? super T>> T min(Iterable<T> source, T _default) {
        if(source.empty) _default else source.min
    }

    /**
     * Returns an Iterable containing all elements starting from the first element for which the predicate returned
     * true. The resulting Iterable is a lazily computed view, so any modifications to the underlying Iterables will be
     * reflected on subsequent iterations. The result's Iterator does not support {@link Iterator#remove()}
     * 
     * @param iterable
     *            the elements from which to drop. May not be <code>null</code>.
     * @param predicate
     *            the predicate which decides the start of the resulting iterable. May not be <code>null</code>.
     * @return the remaining elements after start
     */
    static def <T> Iterable<T> after(Iterable<T> source, (T)=>boolean predicate) {
        return source.dropWhile[!predicate.apply(it)].drop(1)
    }

    /**
     * Returns an Iterable containing all elements starting from the head of the source up to and excluding the first
     * element that matches the predicate The resulting Iterable is a lazily computed view, so any modifications to the
     * underlying Iterables will be reflected on subsequent iterations. The result's Iterator does not support
     * {@link Iterator#remove()}
     * 
     * @param source
     *            the elements from which to take. May not be <code>null</code>.
     * @param predicate
     *            the predicate which decides whether to keep taking elements. May not be <code>null</code>.
     * @return the taken elements
     */
    static def <T> Iterable<T> before(Iterable<T> source, (T)=>boolean predicate) {
        return source.takeWhile[!predicate.apply(it)]
    }

    static def <T> Iterable<T> symmetricHead(Iterable<T> left, Iterable<?> right) {
        return new LoggableIterable<T>() {
            override iterator() {
                new ProcessingIterator<T> {
                    val iLeft = left.iterator
                    val iRight = right.iterator

                    override protected toNext() {
                        if (!iLeft.hasNext || !iRight.hasNext) {
                            return done
                        }
                        val lNext = iLeft.next
                        val rNext = iRight.next
                        return if(lNext == rNext) next = lNext else done
                    }
                }
            }
        }
    }

    static def <T> Iterable<T> symmetricHead(Iterable<T> left, Iterable<?> right, boolean useEquals) {
        if(useEquals) return left.symmetricHead(right)

        return new LoggableIterable<T>() {
            override iterator() {
                new ProcessingIterator<T> {
                    val iLeft = left.iterator
                    val iRight = right.iterator

                    override protected toNext() {
                        if (!iLeft.hasNext || !iRight.hasNext) {
                            return done
                        }
                        val lNext = iLeft.next
                        val rNext = iRight.next
                        return if(lNext === rNext) next = lNext else done
                    }
                }
            }
        }
    }

    // ///////////////////////////////////////////////////////////////////
    //
    // NOTE: The next operations are not streaming
    //
    // ///////////////////////////////////////////////////////////////////
    static def <T, U extends Comparable<? super U>> List<T> sortedBy(Iterable<T> input, (T)=>U function) {
        return sortedBy(input, function, [$0.compareTo($1)])
    }

    static def <T, U> List<T> sortedBy(Iterable<T> input, (T)=>U function, Comparator<? super U> comparator) {
        var result = input.toList
        Collections.sort(result, [comparator.compare(function.apply($0), function.apply($1))]);
        return result
    }

    static def <T> List<T> reverse(Iterable<T> source) {
        return source.toList.reverse
    }

    static def <T> LinkedHashSet<T> toOrderedSet(Iterable<T> source) {
        val result = newLinkedHashSet
        result += source
        return result
    }

    static def <T, K, V> Map<K, List<V>> groupBy(Iterable<? extends T> source, (T)=>K computeKeys, (T)=>V computeValues) {
        if (computeKeys === null) {
            throw new NullPointerException("computeKeys");
        }
        if (computeValues === null) {
            throw new NullPointerException("computeValues");
        }

        val Map<K, List<V>> result = newLinkedHashMap()
        for (element : source) {
            val key = computeKeys.apply(element)
            val value = computeValues.apply(element)
            result.computeIfAbsent(key)[newArrayList] += value
        }
        return result
    }

    /**
     * Groups a source of inputs into groups of equivalent inputs.
     * 
     * @param <Input>     type of input
     * @param iterable      source of inputs
     * @param equivalence defines the equivalence relation of two inputs, e.g.
     *                    {@link Objects#equals(Object, Object)}.
     * @return a list of lists where every list contains equivalent inputs.
     * @see #groupBy(Iterable, Comparator, Function2)
     */
    static def <T> List<List<T>> groupBy(Iterable<T> iterable, (T, T)=>boolean equivalence) {
        return IterableQueries.groupBy(iterable, equivalence)
    }

    /**
     * Groups a source of inputs into groups of equivalent inputs.
     * 
     * @param <Input>     type of input
     * @param iterable    source of inputs
     * @param comparator  If not {@code null}, sorts the groups for performance
     *                    reasons, e.g. ascending list size when all groups are
     *                    expected to contain the same amount of elements or
     *                    descending list size when a few groups are expected to
     *                    contain most of the elements.
     * @param equivalence defines the equivalence relation of two inputs, e.g.
     *                    {@link Objects#equals(Object, Object)}.
     * @return a list of lists where every list contains equivalent inputs.
     */
    static def <T> List<List<T>> groupBy(Iterable<T> iterable, Comparator<? super List<T>> comparator,
        (T, T)=>boolean equivalence) {
        return IterableQueries.groupBy(iterable, comparator, equivalence)
    }

    /**
     * Removes and returns the first entry in the given map that fulfills the predicate. 
     * If none is found or the map is empty, null is returned.
     * <b>NOTE:</b> If the map doesn't guarantee an order this method should be considered as removeAny
     */
    static def <K, V> Map.Entry<K, V> removeFirst(Map<K, V> map, (K, V)=>boolean predicate) {
        return map.entrySet.removeFirst[predicate.apply(key, value)]
    }

    /**
     * Removes and returns the first element in the given iterable that fulfills the predicate. 
     * If none is found or the iterable is empty, null is returned.
     * <b>NOTE:</b> If the iterable doesn't guarantee an order (e.g. Set) this method should be considered as removeAny
     */
    static def <T> T removeFirst(Iterable<T> iterable, (T)=>boolean predicate) {
        return iterable.iterator.removeFirst(predicate)
    }

    /**
     * Removes and returns the first element in the given iterator that fulfills the predicate. 
     * If none is found or the iterator is empty, null is returned.
     */
    static def <T> T removeFirst(Iterator<T> iterator, (T)=>boolean predicate) {
        val result = iterator.findFirst(predicate)
        if (result !== null) {
            iterator.remove;
        }
        return result
    }

    /**
     * Segments {@code iterable} into groups, based on the {@code predicate}.
     * 
     * @param <T> type of input
     * @param iterable source of inputs
     * @param predicate defines where to segment, based on two subsequent entries of the {@code iterable}.
     * @return an iterable of lists (segments).
     */
    static def <T> Iterable<List<T>> segment(Iterable<T> iterable, (T, T)=>boolean predicate) {
        return IterableQueries.segment(iterable, predicate)
    }

    def static void main(String[] args) {
        println(#['a', 'b', 'c'].product(#[1, 2, 3]))
        println(#['a', 'b', 'c'].product)
        println(#['a', 'b', 'c'].symmetricHead(#['a', 'b', 'd']))
        println(#['a', 'b', 'c'].symmetricHead(#[1, 2, 3]))
        println(#['a', 'b', 'c', 'd', 5].after[it == 'c'].reverse)
        println(#['a', 'b', 'c', 'd', 5].before[it == 'c'].reverse)
    }
}
