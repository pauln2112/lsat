/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.emf.common.util.URI;

public class ActivityTransformationOptions {
    private final IFile activityFile;

    private final IFile cifFile;

    private final IWorkspace workspace;

    private final String dispatchingFileExtension;

    public ActivityTransformationOptions(IFile activityFile, IFile cifFile, IWorkspace workspace) {
        this(activityFile, cifFile, "", workspace);
    }

    public ActivityTransformationOptions(IFile activityFile, IFile cifFile, String dispatchingFileExtension,
            IWorkspace workspace)
    {
        this.activityFile = activityFile;
        this.cifFile = cifFile;
        this.dispatchingFileExtension = dispatchingFileExtension;
        this.workspace = workspace;
    }

    public IFile getActivityFile() {
        return activityFile;
    }

    public IFile getCifFile() {
        return cifFile;
    }

    public String getActivityFileName() {
        URI activityFileURI = URI.createPlatformResourceURI(activityFile.getFullPath().toString(), true);
        return activityFileURI.trimFileExtension().lastSegment();
    }

    public String getCifFileName() {
        URI cifFileURI = URI.createPlatformResourceURI(cifFile.getFullPath().toString(), true);
        return cifFileURI.trimFileExtension().lastSegment();
    }

    public IWorkspace getWorkspace() {
        return workspace;
    }

    public String getDispatchingFileExtension() {
        return dispatchingFileExtension;
    }
}
