/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package setting;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.CoreException;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.prefs.Preferences;

public class SettingActivator implements BundleActivator {
    public static final String PREFERENCE_SETTINGS = Settings.class.getName();

    // The plug-in ID
    public static final String PLUGIN_ID = "org.eclipse.lsat.setting.dsl";

    // The shared instance
    private static SettingActivator plugin;

    /**
     * The constructor
     */
    public SettingActivator() {
    }

    /*
     * (non-Javadoc)
     *
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework. BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        plugin = this;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework. BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        plugin = null;
    }

    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static SettingActivator getDefault() {
        return plugin;
    }

    public Preferences getProjectPreferences(IProject project) {
        return new ProjectScope(project).getNode(PLUGIN_ID);
    }

    public IResource getSettingIResource(IResource resource) throws CoreException, FileNotFoundException {
        if ("setting".equals(resource.getFileExtension())) {
            return resource;
        }
        if (".intermediate".equals(resource.getParent().getName())) {
            IFile settingIFile = resource.getProject().getFile(
                    resource.getProjectRelativePath().removeFileExtension().addFileExtension("setting"));
            if (settingIFile.exists()) {
                return settingIFile;
            }
        }
        // fallback to preference settings
        IProject project = resource.getProject();
        String physicalSettingsPath = getProjectPreferences(project).get(PREFERENCE_SETTINGS, null);
        if (null != physicalSettingsPath) {
            IResource settingIResource = project.findMember(physicalSettingsPath);
            if (null != settingIResource) {
                return settingIResource;
            }
        }
        // fallback to search within the project root
        return findSettingIResource(project);
    }

    public Collection<IResource> getSettingIResources(IResource resource) throws CoreException, FileNotFoundException {
        return Stream.of(resource.getProject().members()).filter(r -> "setting".equals(r.getFileExtension()))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private IResource findSettingIResource(IProject project) throws CoreException, FileNotFoundException {
        for (IResource resource: project.members()) {
            if ("setting".equals(resource.getFileExtension())) {
                return resource;
            }
        }
        throw new FileNotFoundException("No setting file found in project");
    }
}
