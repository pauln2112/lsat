/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors;

import activity.Move;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;
import machine.SetPoint;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.timing.util.ITimingCalculator;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.xtend.lib.annotations.Delegate;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class FilteredTimingCalculator implements ITimingCalculator {
  @Delegate
  private final ITimingCalculator delegate;
  
  private final Set<Node> acceptedNodes;
  
  public FilteredTimingCalculator(final ITimingCalculator delegate, final Set<Node> acceptedNodes) {
    this.delegate = delegate;
    this.acceptedNodes = acceptedNodes;
  }
  
  @Override
  public BigDecimal calculateDuration(final Node node) throws MotionException {
    try {
      BigDecimal _xifexpression = null;
      boolean _contains = this.acceptedNodes.contains(node);
      if (_contains) {
        _xifexpression = this.delegate.calculateDuration(node);
      } else {
        _xifexpression = BigDecimal.ZERO;
      }
      return _xifexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public Map<SetPoint, BigDecimal> calculateMotionTime(final Move move) throws SpecificationException, MotionException {
    return this.delegate.calculateMotionTime(move);
  }
  
  public void reset() {
    this.delegate.reset();
  }
}
