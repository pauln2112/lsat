/**
 */
package activity.impl;

import activity.Action;
import activity.Activity;
import activity.ActivityFactory;
import activity.ActivityPackage;
import activity.ActivitySet;
import activity.Claim;
import activity.Event;
import activity.EventAction;
import activity.EventItem;
import activity.LocationPrerequisite;
import activity.Move;
import activity.PeripheralAction;
import activity.RaiseEvent;
import activity.Release;
import activity.RequireEvent;
import activity.ResourceAction;
import activity.SchedulingType;
import activity.SimpleAction;
import activity.SyncBar;
import activity.TracePoint;

import machine.MachinePackage;

import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActivityPackageImpl extends EPackageImpl implements ActivityPackage {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass activitySetEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass activityEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass actionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass peripheralActionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass claimEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass releaseEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass locationPrerequisiteEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass tracePointEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass syncBarEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass simpleActionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass moveEClass = null;

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass eventActionEClass = null;

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass eventEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass raiseEventEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass requireEventEClass = null;

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass resourceActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass eventItemEClass = null;

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum schedulingTypeEEnum = null;

	/**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see activity.ActivityPackage#eNS_URI
     * @see #init()
     * @generated
     */
	private ActivityPackageImpl() {
        super(eNS_URI, ActivityFactory.eINSTANCE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static boolean isInited = false;

	/**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link ActivityPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
	public static ActivityPackage init() {
        if (isInited) return (ActivityPackage)EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI);

        // Obtain or create and register package
        Object registeredActivityPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        ActivityPackageImpl theActivityPackage = registeredActivityPackage instanceof ActivityPackageImpl ? (ActivityPackageImpl)registeredActivityPackage : new ActivityPackageImpl();

        isInited = true;

        // Initialize simple dependencies
        EdgPackage.eINSTANCE.eClass();
        MachinePackage.eINSTANCE.eClass();

        // Create package meta-data objects
        theActivityPackage.createPackageContents();

        // Initialize created meta-data
        theActivityPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theActivityPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(ActivityPackage.eNS_URI, theActivityPackage);
        return theActivityPackage;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getActivitySet() {
        return activitySetEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getActivitySet_Activities() {
        return (EReference)activitySetEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getActivitySet_Events()
    {
        return (EReference)activitySetEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getActivity() {
        return activityEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getActivity_Prerequisites() {
        return (EReference)activityEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getActivity_OriginalName() {
        return (EAttribute)activityEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getActivity__IsExpanded() {
        return activityEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getActivity__GetResourcesNeedingItem() {
        return activityEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getAction() {
        return actionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getAction_OuterEntry() {
        return (EReference)actionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getAction_Entry() {
        return (EReference)actionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getAction_Exit() {
        return (EReference)actionEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getAction_OuterExit() {
        return (EReference)actionEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPeripheralAction() {
        return peripheralActionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheralAction_Peripheral() {
        return (EReference)peripheralActionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPeripheralAction_SchedulingType() {
        return (EAttribute)peripheralActionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getClaim() {
        return claimEClass;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getClaim_Passive()
    {
        return (EAttribute)claimEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getRelease() {
        return releaseEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getLocationPrerequisite() {
        return locationPrerequisiteEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLocationPrerequisite_Resource() {
        return (EReference)locationPrerequisiteEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLocationPrerequisite_Peripheral() {
        return (EReference)locationPrerequisiteEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLocationPrerequisite_Position() {
        return (EReference)locationPrerequisiteEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLocationPrerequisite_Activity() {
        return (EReference)locationPrerequisiteEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getTracePoint() {
        return tracePointEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getTracePoint_Value() {
        return (EAttribute)tracePointEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getTracePoint_Regex() {
        return (EAttribute)tracePointEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getSyncBar() {
        return syncBarEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getSimpleAction() {
        return simpleActionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getSimpleAction_Type() {
        return (EReference)simpleActionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getMove() {
        return moveEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMove_Profile() {
        return (EReference)moveEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMove_SuccessorMove() {
        return (EReference)moveEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMove_PredecessorMove() {
        return (EReference)moveEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMove_StopAtTarget() {
        return (EAttribute)moveEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMove_PositionMove() {
        return (EAttribute)moveEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMove_TargetPosition() {
        return (EReference)moveEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMove_SourcePosition() {
        return (EReference)moveEClass.getEStructuralFeatures().get(6);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMove_Distance() {
        return (EReference)moveEClass.getEStructuralFeatures().get(7);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMove_Passing() {
        return (EAttribute)moveEClass.getEStructuralFeatures().get(8);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMove_Continuing() {
        return (EAttribute)moveEClass.getEStructuralFeatures().get(9);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEventAction()
    {
        return eventActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getEvent() {
        return eventEClass;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EOperation getEvent__GetResourceType()
    {
        return eventEClass.getEOperations().get(0);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getRaiseEvent() {
        return raiseEventEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getRequireEvent() {
        return requireEventEClass;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getResourceAction()
    {
        return resourceActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getResourceAction_Resource()
    {
        return (EReference)resourceActionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEventItem()
    {
        return eventItemEClass;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EEnum getSchedulingType() {
        return schedulingTypeEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ActivityFactory getActivityFactory() {
        return (ActivityFactory)getEFactoryInstance();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isCreated = false;

	/**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        activitySetEClass = createEClass(ACTIVITY_SET);
        createEReference(activitySetEClass, ACTIVITY_SET__ACTIVITIES);
        createEReference(activitySetEClass, ACTIVITY_SET__EVENTS);

        activityEClass = createEClass(ACTIVITY);
        createEReference(activityEClass, ACTIVITY__PREREQUISITES);
        createEAttribute(activityEClass, ACTIVITY__ORIGINAL_NAME);
        createEOperation(activityEClass, ACTIVITY___IS_EXPANDED);
        createEOperation(activityEClass, ACTIVITY___GET_RESOURCES_NEEDING_ITEM);

        actionEClass = createEClass(ACTION);
        createEReference(actionEClass, ACTION__OUTER_ENTRY);
        createEReference(actionEClass, ACTION__ENTRY);
        createEReference(actionEClass, ACTION__EXIT);
        createEReference(actionEClass, ACTION__OUTER_EXIT);

        peripheralActionEClass = createEClass(PERIPHERAL_ACTION);
        createEReference(peripheralActionEClass, PERIPHERAL_ACTION__PERIPHERAL);
        createEAttribute(peripheralActionEClass, PERIPHERAL_ACTION__SCHEDULING_TYPE);

        claimEClass = createEClass(CLAIM);
        createEAttribute(claimEClass, CLAIM__PASSIVE);

        releaseEClass = createEClass(RELEASE);

        locationPrerequisiteEClass = createEClass(LOCATION_PREREQUISITE);
        createEReference(locationPrerequisiteEClass, LOCATION_PREREQUISITE__RESOURCE);
        createEReference(locationPrerequisiteEClass, LOCATION_PREREQUISITE__PERIPHERAL);
        createEReference(locationPrerequisiteEClass, LOCATION_PREREQUISITE__POSITION);
        createEReference(locationPrerequisiteEClass, LOCATION_PREREQUISITE__ACTIVITY);

        tracePointEClass = createEClass(TRACE_POINT);
        createEAttribute(tracePointEClass, TRACE_POINT__VALUE);
        createEAttribute(tracePointEClass, TRACE_POINT__REGEX);

        syncBarEClass = createEClass(SYNC_BAR);

        simpleActionEClass = createEClass(SIMPLE_ACTION);
        createEReference(simpleActionEClass, SIMPLE_ACTION__TYPE);

        moveEClass = createEClass(MOVE);
        createEReference(moveEClass, MOVE__PROFILE);
        createEReference(moveEClass, MOVE__SUCCESSOR_MOVE);
        createEReference(moveEClass, MOVE__PREDECESSOR_MOVE);
        createEAttribute(moveEClass, MOVE__STOP_AT_TARGET);
        createEAttribute(moveEClass, MOVE__POSITION_MOVE);
        createEReference(moveEClass, MOVE__TARGET_POSITION);
        createEReference(moveEClass, MOVE__SOURCE_POSITION);
        createEReference(moveEClass, MOVE__DISTANCE);
        createEAttribute(moveEClass, MOVE__PASSING);
        createEAttribute(moveEClass, MOVE__CONTINUING);

        eventActionEClass = createEClass(EVENT_ACTION);

        raiseEventEClass = createEClass(RAISE_EVENT);

        requireEventEClass = createEClass(REQUIRE_EVENT);

        resourceActionEClass = createEClass(RESOURCE_ACTION);
        createEReference(resourceActionEClass, RESOURCE_ACTION__RESOURCE);

        eventEClass = createEClass(EVENT);
        createEOperation(eventEClass, EVENT___GET_RESOURCE_TYPE);

        eventItemEClass = createEClass(EVENT_ITEM);

        // Create enums
        schedulingTypeEEnum = createEEnum(SCHEDULING_TYPE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isInitialized = false;

	/**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        MachinePackage theMachinePackage = (MachinePackage)EPackage.Registry.INSTANCE.getEPackage(MachinePackage.eNS_URI);
        EdgPackage theEdgPackage = (EdgPackage)EPackage.Registry.INSTANCE.getEPackage(EdgPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        activitySetEClass.getESuperTypes().add(theMachinePackage.getImportContainer());
        activityEClass.getESuperTypes().add(theEdgPackage.getEditableDirectedGraph());
        actionEClass.getESuperTypes().add(theEdgPackage.getNode());
        peripheralActionEClass.getESuperTypes().add(theMachinePackage.getHasResourcePeripheral());
        peripheralActionEClass.getESuperTypes().add(this.getResourceAction());
        claimEClass.getESuperTypes().add(this.getResourceAction());
        releaseEClass.getESuperTypes().add(this.getResourceAction());
        locationPrerequisiteEClass.getESuperTypes().add(theMachinePackage.getHasResourcePeripheral());
        syncBarEClass.getESuperTypes().add(theEdgPackage.getNode());
        simpleActionEClass.getESuperTypes().add(this.getPeripheralAction());
        moveEClass.getESuperTypes().add(this.getPeripheralAction());
        eventActionEClass.getESuperTypes().add(this.getResourceAction());
        raiseEventEClass.getESuperTypes().add(this.getEventAction());
        requireEventEClass.getESuperTypes().add(this.getEventAction());
        resourceActionEClass.getESuperTypes().add(this.getAction());
        eventEClass.getESuperTypes().add(theMachinePackage.getResource());
        eventItemEClass.getESuperTypes().add(theMachinePackage.getResourceItem());

        // Initialize classes, features, and operations; add parameters
        initEClass(activitySetEClass, ActivitySet.class, "ActivitySet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getActivitySet_Activities(), this.getActivity(), null, "activities", null, 0, -1, ActivitySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        getActivitySet_Activities().getEKeys().add(theEdgPackage.getEditableDirectedGraph_Name());
        initEReference(getActivitySet_Events(), this.getEvent(), null, "events", null, 0, -1, ActivitySet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        getActivitySet_Events().getEKeys().add(theMachinePackage.getIResource_Name());

        initEClass(activityEClass, Activity.class, "Activity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getActivity_Prerequisites(), this.getLocationPrerequisite(), this.getLocationPrerequisite_Activity(), "prerequisites", null, 0, -1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getActivity_OriginalName(), ecorePackage.getEString(), "originalName", null, 0, 1, Activity.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getActivity__IsExpanded(), ecorePackage.getEBoolean(), "isExpanded", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEOperation(getActivity__GetResourcesNeedingItem(), theMachinePackage.getResource(), "getResourcesNeedingItem", 0, -1, IS_UNIQUE, IS_ORDERED);

        initEClass(actionEClass, Action.class, "Action", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getAction_OuterEntry(), this.getTracePoint(), null, "outerEntry", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAction_Entry(), this.getTracePoint(), null, "entry", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAction_Exit(), this.getTracePoint(), null, "exit", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAction_OuterExit(), this.getTracePoint(), null, "outerExit", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(peripheralActionEClass, PeripheralAction.class, "PeripheralAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getPeripheralAction_Peripheral(), theMachinePackage.getPeripheral(), null, "peripheral", null, 1, 1, PeripheralAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPeripheralAction_SchedulingType(), this.getSchedulingType(), "schedulingType", null, 0, 1, PeripheralAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(claimEClass, Claim.class, "Claim", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getClaim_Passive(), ecorePackage.getEBoolean(), "passive", "false", 0, 1, Claim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(releaseEClass, Release.class, "Release", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(locationPrerequisiteEClass, LocationPrerequisite.class, "LocationPrerequisite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getLocationPrerequisite_Resource(), theMachinePackage.getIResource(), null, "resource", null, 1, 1, LocationPrerequisite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLocationPrerequisite_Peripheral(), theMachinePackage.getPeripheral(), null, "peripheral", null, 1, 1, LocationPrerequisite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLocationPrerequisite_Position(), theMachinePackage.getSymbolicPosition(), null, "position", null, 1, 1, LocationPrerequisite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLocationPrerequisite_Activity(), this.getActivity(), this.getActivity_Prerequisites(), "activity", null, 1, 1, LocationPrerequisite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(tracePointEClass, TracePoint.class, "TracePoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTracePoint_Value(), ecorePackage.getEString(), "value", null, 1, 1, TracePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTracePoint_Regex(), ecorePackage.getEBoolean(), "regex", null, 1, 1, TracePoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(syncBarEClass, SyncBar.class, "SyncBar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(simpleActionEClass, SimpleAction.class, "SimpleAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSimpleAction_Type(), theMachinePackage.getActionType(), null, "type", null, 1, 1, SimpleAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(moveEClass, Move.class, "Move", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getMove_Profile(), theMachinePackage.getProfile(), null, "profile", null, 1, 1, Move.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMove_SuccessorMove(), this.getMove(), null, "successorMove", null, 0, 1, Move.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getMove_PredecessorMove(), this.getMove(), null, "predecessorMove", null, 0, 1, Move.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEAttribute(getMove_StopAtTarget(), ecorePackage.getEBoolean(), "stopAtTarget", null, 1, 1, Move.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getMove_PositionMove(), ecorePackage.getEBoolean(), "positionMove", null, 1, 1, Move.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMove_TargetPosition(), theMachinePackage.getSymbolicPosition(), null, "targetPosition", null, 0, 1, Move.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMove_SourcePosition(), theMachinePackage.getSymbolicPosition(), null, "sourcePosition", null, 0, 1, Move.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getMove_Distance(), theMachinePackage.getDistance(), null, "distance", null, 0, 1, Move.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getMove_Passing(), ecorePackage.getEBoolean(), "passing", null, 1, 1, Move.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getMove_Continuing(), ecorePackage.getEBoolean(), "continuing", null, 1, 1, Move.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(eventActionEClass, EventAction.class, "EventAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(raiseEventEClass, RaiseEvent.class, "RaiseEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(requireEventEClass, RequireEvent.class, "RequireEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(resourceActionEClass, ResourceAction.class, "ResourceAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getResourceAction_Resource(), theMachinePackage.getIResource(), null, "resource", null, 1, 1, ResourceAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(eventEClass, Event.class, "Event", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEOperation(getEvent__GetResourceType(), theMachinePackage.getResourceType(), "getResourceType", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(eventItemEClass, EventItem.class, "EventItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        // Initialize enums and add enum literals
        initEEnum(schedulingTypeEEnum, SchedulingType.class, "SchedulingType");
        addEEnumLiteral(schedulingTypeEEnum, SchedulingType.ASAP);
        addEEnumLiteral(schedulingTypeEEnum, SchedulingType.ALAP);

        // Create resource
        createResource(eNS_URI);
    }

} //ActivityPackageImpl
