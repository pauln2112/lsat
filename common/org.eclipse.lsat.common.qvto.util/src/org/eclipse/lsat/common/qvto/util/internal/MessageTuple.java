/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util.internal;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageTuple {
    private static final String ASSERT = "ASSERT \\[(\\w+)\\] failed at \\([^\\)]+\\) : ";

    private static final String ERRORCODE = "(\\w+)\\|";

    private static final Pattern MSG_PATTERN = Pattern.compile("^(" + ASSERT + ")?(" + ERRORCODE + ")?(.*)$");

    private final String itsAssert;

    private final String itsMessage;

    private final String itsErrorCode;

    public MessageTuple(String aMessage) {
        Matcher matcher = MSG_PATTERN.matcher(aMessage);
        if (matcher.matches()) {
            itsAssert = matcher.group(1);
            itsErrorCode = matcher.group(4);
            itsMessage = matcher.group(5);
        } else {
            // Is this possible??
            itsAssert = null;
            itsErrorCode = null;
            itsMessage = aMessage;
        }
    }

    public String getMessage() {
        return itsMessage;
    }

    public boolean isAssert() {
        return itsAssert != null;
    }

    public String getAssert() {
        return itsAssert;
    }

    public boolean hasErrorCode() {
        return itsErrorCode != null;
    }

    public String getErrorCode() {
        return itsErrorCode;
    }

    public static void main(String[] args) {
        print(MSG_PATTERN.matcher("Failed to determine type for Bar:virtual_in1:virtual_in1"));
        print(MSG_PATTERN.matcher("CODE|Failed to determine type for Bar:virtual_in1:virtual_in1"));
        print(MSG_PATTERN.matcher(
                "ASSERT [warning] failed at (addW2WBlocks.qvto:90) : Failed to determine type for Bar:virtual_in1:virtual_in1"));
        print(MSG_PATTERN.matcher(
                "ASSERT [fatal] failed at (addW2WBlocks.qvto:90) : CODE|Failed to determine type for Bar:virtual_in1:virtual_in1"));
    }

    private static void print(Matcher aMatcher) {
        if (!aMatcher.matches()) {
            System.out.println("NoMatch");
        }
        for (int i = 1; i <= aMatcher.groupCount(); i++) {
            System.out.println("Group " + i + ": " + aMatcher.group(i));
        }
    }
}
