/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games.benchmarking;

import java.io.PrintWriter;
import java.util.Map;

import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.datastructures.weights.DoubleWeightFunctionDouble;
import org.eclipse.lsat.common.ludus.backend.datastructures.weights.DoubleWeightFunctionInt;
import org.eclipse.lsat.common.ludus.backend.games.StrategyVector;
import org.eclipse.lsat.common.ludus.backend.games.benchmarking.generator.Tor;
import org.eclipse.lsat.common.ludus.backend.games.ratio.solvers.policy.PolicyIterationDoubleVars;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.JGraphTEdge;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.JGraphTVertex;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.ratio.RGDoubleImplJGraphT;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.ratio.RGIntImplJGraphT;

/**
 * @author Bram van der Sanden
 */
public class TorEpsDeltaFuzzyBenchmark extends Benchmark {
    private final String name;

    private final Integer sizeMin;

    private final Integer sizeMax;

    private final Integer stepSize;

    private final Integer maxWeight1;

    private final Integer maxWeight2;

    private PrintWriter file;

    public TorEpsDeltaFuzzyBenchmark(String name, Integer sizeMin, Integer sizeMax, Integer stepSize,
            Integer maxWeight1, Integer maxWeight2)
    {
        this.name = name;
        this.sizeMin = sizeMin;
        this.sizeMax = sizeMax;
        this.stepSize = stepSize;
        this.maxWeight1 = maxWeight1;
        this.maxWeight2 = maxWeight2;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void run(Integer numberOfIterations, boolean runPI, boolean runEG, boolean runZP) {
        // Create a new file.
        file = getFile(name);

        file.printf("%s,%s,%s,%s,%s\n", "N", "maxWeight1", "maxWeight2", "PolicyIterationN", "PolicyIterationR");
        System.out.printf("%s,%s,%s,%s,%s\n", "N", "maxWeight1", "maxWeight2", "PolicyIterationN", "PolicyIterationR");

        // Given the settings, run all tests.
        for (int runId = 0; runId < numberOfIterations; runId++) {
            for (int v = sizeMin; v <= sizeMax; v += stepSize) {
                runAlgorithmsTor(v, maxWeight1, maxWeight2);
            }
        }

        // Close the file for the benchmark.
        file.close();
    }

    /**
     * Create a test case corresponding to the settings, and run it using the algorithms that are enabled.
     *
     * @param size number of vertices in the graph is size * size
     * @param maxWeight1 maximum edge weight for weight 1
     * @param maxWeight2 maximum edge weight for weight 2
     */
    private void runAlgorithmsTor(Integer size, Integer maxWeight1, Integer maxWeight2) {
        RGIntImplJGraphT torGraph = Tor.generateRatioGame(size, maxWeight1, maxWeight2);

        // Solve using double algorithms.
        // Take into account maximum reachable precision.
        RGDoubleImplJGraphT torGraphD = toDoubleGameGraph(torGraph);

        Integer vSize = torGraphD.getVertices().size();
        Double epsilon = 10E-10;
        Double delta = (vSize - 1) * torGraphD.getMaxAbsValue() * epsilon;

        long start, end;
        start = System.nanoTime();
        @SuppressWarnings("unused")
        Tuple<Map<JGraphTVertex, Double>, StrategyVector<JGraphTVertex, JGraphTEdge>> doubleResult = PolicyIterationDoubleVars
                .solve(torGraphD, epsilon, delta);
        end = System.nanoTime();
        float piSecD = ((end - start) / 1000000000.0f);

        file.printf("%d,%d,%d,%f\n", size, maxWeight1, maxWeight2, piSecD);
        System.out.printf("%d,%d,%d,%f\n", size, maxWeight1, maxWeight2, piSecD);
    }

    private RGDoubleImplJGraphT toDoubleGameGraph(RGIntImplJGraphT ratioGame) {
        DoubleWeightFunctionInt<JGraphTEdge> f = ratioGame.getEdgeWeights();
        DoubleWeightFunctionDouble<JGraphTEdge> weights = new DoubleWeightFunctionDouble<>();
        for (JGraphTEdge e: ratioGame.getEdges()) {
            weights.addWeight(e, f.getWeight1(e) * 0.62, f.getWeight2(e) * 0.34);
        }
        return new RGDoubleImplJGraphT(ratioGame.getGraph(), weights);
    }
}
