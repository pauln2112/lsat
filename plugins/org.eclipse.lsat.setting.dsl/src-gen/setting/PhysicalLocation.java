/**
 */
package setting;

import expressions.Expression;
import java.math.BigDecimal;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Location</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link setting.PhysicalLocation#getDefaultExp <em>Default Exp</em>}</li>
 *   <li>{@link setting.PhysicalLocation#getMinExp <em>Min Exp</em>}</li>
 *   <li>{@link setting.PhysicalLocation#getMaxExp <em>Max Exp</em>}</li>
 *   <li>{@link setting.PhysicalLocation#getDefault <em>Default</em>}</li>
 *   <li>{@link setting.PhysicalLocation#getMin <em>Min</em>}</li>
 *   <li>{@link setting.PhysicalLocation#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @see setting.SettingPackage#getPhysicalLocation()
 * @model
 * @generated
 */
public interface PhysicalLocation extends EObject {
	/**
     * Returns the value of the '<em><b>Default Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Default Exp</em>' containment reference.
     * @see #setDefaultExp(Expression)
     * @see setting.SettingPackage#getPhysicalLocation_DefaultExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getDefaultExp();

	/**
     * Sets the value of the '{@link setting.PhysicalLocation#getDefaultExp <em>Default Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Default Exp</em>' containment reference.
     * @see #getDefaultExp()
     * @generated
     */
	void setDefaultExp(Expression value);

	/**
     * Returns the value of the '<em><b>Min Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Min Exp</em>' containment reference.
     * @see #setMinExp(Expression)
     * @see setting.SettingPackage#getPhysicalLocation_MinExp()
     * @model containment="true"
     * @generated
     */
	Expression getMinExp();

	/**
     * Sets the value of the '{@link setting.PhysicalLocation#getMinExp <em>Min Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Min Exp</em>' containment reference.
     * @see #getMinExp()
     * @generated
     */
	void setMinExp(Expression value);

	/**
     * Returns the value of the '<em><b>Max Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Max Exp</em>' containment reference.
     * @see #setMaxExp(Expression)
     * @see setting.SettingPackage#getPhysicalLocation_MaxExp()
     * @model containment="true"
     * @generated
     */
	Expression getMaxExp();

	/**
     * Sets the value of the '{@link setting.PhysicalLocation#getMaxExp <em>Max Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Max Exp</em>' containment reference.
     * @see #getMaxExp()
     * @generated
     */
	void setMaxExp(Expression value);

	/**
     * Returns the value of the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Default</em>' attribute.
     * @see setting.SettingPackage#getPhysicalLocation_Default()
     * @model transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getDefault();

	/**
     * Returns the value of the '<em><b>Min</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Min</em>' attribute.
     * @see setting.SettingPackage#getPhysicalLocation_Min()
     * @model transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getMin();

	/**
     * Returns the value of the '<em><b>Max</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Max</em>' attribute.
     * @see setting.SettingPackage#getPhysicalLocation_Max()
     * @model transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getMax();

} // PhysicalLocation
