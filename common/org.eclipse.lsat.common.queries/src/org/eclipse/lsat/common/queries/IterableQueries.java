/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.queries;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToIntBiFunction;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.lsat.common.util.BranchIterable;
import org.eclipse.lsat.common.util.BranchIterator;
import org.eclipse.lsat.common.util.IterableUtil;
import org.eclipse.lsat.common.util.LoggableIterable;
import org.eclipse.lsat.common.util.SuppliedIterable;

/**
 * This class implements OCL like queries in plain java using Iterators<br>
 * (thus no need for creating a temporary Collection instance).
 */
public class IterableQueries {
    private IterableQueries() {
        // Empty
    }

    /**
     * Behaves like OCL <tt>this</tt>->including(<tt>other</tt>)
     *
     * @see IterableUtil#join(Iterable...)
     */
    public static <Input> Iterable<Input> including(Iterable<Input> iterable, Iterable<? extends Input> includes) {
        return IterableUtil.join(iterable, includes);
    }

    /**
     * Behaves like OCL <tt>this</tt>->including(<tt>other</tt>)
     *
     * @see IterableUtil#join(Iterable...)
     */
    @SafeVarargs
    public static <Input> Iterable<Input> including(Iterable<Input> iterable, Input... includes) {
        return including(iterable, IterableUtil.asList(includes));
    }

    /**
     * Behaves like OCL <tt>this</tt>->excluding(<tt>other</tt>)
     */
    public static <Input> Iterable<Input> excluding(Iterable<Input> iterable, Iterable<?> excludes) {
        return reject(iterable, i -> IterableUtil.contains(excludes, i));
    }

    /**
     * Behaves like OCL <tt>this</tt>->excluding(<tt>other</tt>)
     */
    @SafeVarargs
    public static <Input> Iterable<Input> excluding(Iterable<Input> iterable, Object... excludes) {
        return excluding(iterable, IterableUtil.asList(excludes));
    }

    /**
     * From a {@link BranchIterable} find the nearest node in each branch that matches the {@code predicate}
     */
    public static <Input> Iterable<Input> findNearest(BranchIterable<Input> iterable,
            Predicate<? super Input> predicate)
    {
        return new SuppliedIterable<>(() -> IteratorQueries.findNearest(iterable.iterator(), predicate));
    }

    /**
     * From a {@link BranchIterable} find the nearest node in each branch that is an instance of {@code type}
     */
    public static <Input, Output> Iterable<Output> findNearest(BranchIterable<Input> iterable, Class<Output> type) {
        return new SuppliedIterable<>(() -> IteratorQueries.findNearest(iterable.iterator(), type));
    }

    /**
     * Returns the elements of each branch of a {@link BranchIterable} up to the element that matches the
     * {@code predicate}. The matched element itself is excluded from the result.
     *
     * @return an iterator containing all elements of {@code iterable} up to (thus excluding) the first element that
     *     matches the {@code predicate} per branch.
     */
    public static <Input> BranchIterable<Input> until(BranchIterable<Input> iterable,
            Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable, i -> IteratorQueries.until((BranchIterator<Input>)i, predicate));
    }

    /**
     * Returns the elements of each branch of a {@link BranchIterable} up to the element that matches the
     * {@code predicate}. The matched element itself is included in the result.
     *
     * @return an iterator containing all elements of {@code iterable} up to and including the first element that
     *     matches the {@code predicate} per branch.
     */
    public static <Input> BranchIterable<Input> upToAndIncluding(BranchIterable<Input> iterable,
            Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable,
                i -> IteratorQueries.upToAndIncluding((BranchIterator<Input>)i, predicate));
    }

    /**
     * Collects all children and children's children without the need for creating a temporary Collection instance.
     * Behaves like OCL <tt>iterable</tt>->closure(e | <tt>functor.closureOf(e)</tt>)
     */
    public static <Input> BranchIterable<Input> closure(Iterable<? extends Input> iterable,
            Function<Input, Iterable<? extends Input>> functor)
    {
        return IterableUtil.branchWrap(iterable, i -> IteratorQueries.closure(i, e -> emptyIfNull(functor.apply(e))));
    }

    /**
     * Collects all children and children's children without the need for creating a temporary Collection instance.
     * Behaves like OCL <tt>iterable</tt>->closure(e | <tt>functor.closureOf(e)</tt>)
     */
    public static <Input> BranchIterable<Input> closure(Iterable<? extends Input> iterable, boolean includeSelf,
            Function<Input, Iterable<? extends Input>> functor)
    {
        return IterableUtil.branchWrap(iterable,
                i -> IteratorQueries.closure(i, includeSelf, e -> emptyIfNull(functor.apply(e))));
    }

    /**
     * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
     * true. No need for creating a temporary Collection instance.<br>
     *
     * @see #closure(Iterable, Function)
     */
    public static <Input> BranchIterable<Input> closureWhile(Iterable<? extends Input> iterable,
            Function<Input, Iterable<? extends Input>> functor, Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable,
                i -> IteratorQueries.closureWhile(i, e -> emptyIfNull(functor.apply(e)), predicate));
    }

    /**
     * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
     * true. No need for creating a temporary Collection instance.<br>
     *
     * @see #closure(Iterable, Function)
     */
    public static <Input> BranchIterable<Input> closureWhile(Iterable<? extends Input> iterable, boolean includeSelf,
            Function<Input, Iterable<? extends Input>> functor, Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable,
                i -> IteratorQueries.closureWhile(i, includeSelf, e -> emptyIfNull(functor.apply(e)), predicate));
    }

    /**
     * Collects child, child's child, etc without the need for creating a temporary Collection instance. Behaves like
     * OCL <tt>iterable</tt>->closure(e | <tt>functor.getOne(e)</tt>)
     */
    public static <Input> BranchIterable<Input> closureOne(Iterable<? extends Input> iterable,
            Function<Input, ? extends Input> functor)
    {
        return IterableUtil.branchWrap(iterable, i -> IteratorQueries.closureOne(i, functor));
    }

    /**
     * Collects child, child's child, etc without the need for creating a temporary Collection instance. Behaves like
     * OCL <tt>iterable</tt>->closure(e | <tt>functor.getOne(e)</tt>)
     */
    public static <Input> BranchIterable<Input> closureOne(Iterable<? extends Input> iterable, boolean includeSelf,
            Function<Input, ? extends Input> functor)
    {
        return IterableUtil.branchWrap(iterable, i -> IteratorQueries.closureOne(i, includeSelf, functor));
    }

    /**
     * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
     * true. No need for creating a temporary Collection instance.<br>
     *
     * @see #closure(Iterable, Function)
     */
    public static <Input> BranchIterable<Input> closureOneWhile(Iterable<? extends Input> iterable,
            Function<Input, ? extends Input> functor, Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable, i -> IteratorQueries.closureOneWhile(i, functor, predicate));
    }

    /**
     * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
     * true. No need for creating a temporary Collection instance.<br>
     *
     * @see #closure(Iterable, Function)
     */
    public static <Input> BranchIterable<Input> closureOneWhile(Iterable<? extends Input> iterable, boolean includeSelf,
            Function<Input, ? extends Input> functor, Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable,
                i -> IteratorQueries.closureOneWhile(i, includeSelf, functor, predicate));
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterable</tt> without the need for
     * creating a temporary Collection instance. Note that the resulting iterator might contain a tree node multiple
     * times if elements in <tt>iterable</tt> share this tree node as common ancestor.
     */
    public static <Input> BranchIterable<Input> climbTree(Iterable<? extends Input> iterable,
            Function<Input, ? extends Input> functor)
    {
        return IterableUtil.branchWrap(iterable, i -> IteratorQueries.climbTree(i, functor));
    }

    /**
     * Collects self (only if <tt>includeSelf</tt> is <code>true</code>), parent and parent's parents per element in
     * this iterable without the need for creating a temporary Collection instance. Note that the resulting iterator
     * might contain a tree node multiple times if elements in this iterable share this tree node as common ancestor.
     * You can use {@link #unique()} to filter these duplicates.
     */
    public static <Input> BranchIterable<Input> climbTree(Iterable<? extends Input> iterable, boolean includeSelf,
            Function<Input, ? extends Input> functor)
    {
        return IterableUtil.branchWrap(iterable, i -> IteratorQueries.climbTree(i, includeSelf, functor));
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> and stops each sub branch as
     * soon as the predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance. Note
     * that the resulting iterator might contain a tree node multiple times if elements in <tt>iterator</tt> share this
     * tree node as common ancestor.
     */
    public static <Input> BranchIterable<Input> climbTreeWhile(Iterable<? extends Input> iterable,
            Function<Input, ? extends Input> functor, Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable, i -> IteratorQueries.climbTreeWhile(i, functor, predicate));
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> and stops each sub branch as
     * soon as the predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance. Note
     * that the resulting iterator might contain a tree node multiple times if elements in <tt>iterator</tt> share this
     * tree node as common ancestor.
     */
    public static <Input> BranchIterable<Input> climbTreeWhile(Iterable<? extends Input> iterable, boolean includeSelf,
            Function<Input, ? extends Input> functor, Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable,
                i -> IteratorQueries.climbTreeWhile(i, includeSelf, functor, predicate));
    }

    /**
     * Collects all descendants (children and children's children; depth first) without the need for creating a
     * temporary Collection instance.
     */
    public static <Input> BranchIterable<Input> walkTree(Iterable<? extends Input> iterable,
            Function<Input, Iterable<? extends Input>> functor)
    {
        return IterableUtil.branchWrap(iterable, i -> IteratorQueries.walkTree(i, e -> emptyIfNull(functor.apply(e))));
    }

    /**
     * Collects self (only if <tt>includeSelf</tt> is <code>true</code>), all children and children's children - depth
     * first - without the need for creating a temporary Collection instance.
     */
    public static <Input> BranchIterable<Input> walkTree(Iterable<? extends Input> iterable, boolean includeSelf,
            Function<Input, Iterable<? extends Input>> functor)
    {
        return IterableUtil.branchWrap(iterable,
                i -> IteratorQueries.walkTree(i, includeSelf, e -> emptyIfNull(functor.apply(e))));
    }

    /**
     * Collects all descendants (children and children's children; depth first) and stops each sub branch as soon as the
     * predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     */
    public static <Input> BranchIterable<Input> walkTreeWhile(Iterable<? extends Input> iterable,
            Function<Input, Iterable<? extends Input>> functor, Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable,
                i -> IteratorQueries.walkTreeWhile(i, e -> emptyIfNull(functor.apply(e)), predicate));
    }

    /**
     * Collects all descendants (children and children's children; depth first) and stops each sub branch as soon as the
     * predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     */
    public static <Input> BranchIterable<Input> walkTreeWhile(Iterable<? extends Input> iterable, boolean includeSelf,
            Function<Input, Iterable<? extends Input>> functor, Predicate<? super Input> predicate)
    {
        return IterableUtil.branchWrap(iterable,
                i -> IteratorQueries.walkTreeWhile(i, includeSelf, e -> emptyIfNull(functor.apply(e)), predicate));
    }

    /**
     * Selects elements without the need for creating a temporary Collection instance. Behaves like OCL
     * <tt>iterable</tt>->select(e | not functor.accept(e).oclIsInvalid())
     */
    public static <Input> Iterable<Input> select(Iterable<Input> iterable, Predicate<? super Input> functor) {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.select(i, functor));
    }

    /**
     * Selects elements without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
     * Behaves like OCL <tt>iterable</tt>->xselect(e | not functor.accept(e).oclIsInvalid())
     *
     * @see IterableUtil#notNull(Iterable)
     */
    public static <Input> Iterable<Input> xselect(Iterable<Input> iterable, Predicate<? super Input> functor) {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.xselect(i, functor));
    }

    /**
     * Rejects elements without the need for creating a temporary Collection instance.<br>
     * Behaves like OCL <tt>this</tt>->reject(e | functor.test(e))
     */
    public static <Input> Iterable<Input> reject(Iterable<Input> iterable, Predicate<? super Input> functor) {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.reject(i, functor));
    }

    /**
     * Filters the iteration, only keeping items that are instance of the specified type. <br>
     * Behaves like OCL <tt>this</tt>->objectsOfKind(<tt>Output</tt>)
     */
    public static <Input, Output> Iterable<Output> objectsOfKind(Iterable<Input> iterable, Class<Output> outputType) {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.objectsOfKind(i, outputType));
    }

    /**
     * Filters the iteration, only keeping item of a specific type. <br>
     * <b>NOTE:</b> If <tt>outputType</tt> is an interface, only items are selected which directly implement the
     * interface.<br>
     * Behaves like OCL <tt>this</tt>->objectsOfType(<tt>Output</tt>)
     */
    public static <Input, Output> Iterable<Output> objectsOfType(Iterable<Input> iterable, Class<Output> outputType) {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.objectsOfType(i, outputType));
    }

    /**
     * Casts this iteration to a specific type. <br>
     * Behaves like OCL <tt>this</tt>->oclAsType(<tt>Output</tt>)
     */
    public static <Input, Output> Iterable<Output> asType(Iterable<Input> iterable, Class<Output> outputType) {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.asType(i, outputType));
    }

    /**
     * Collects elements without the need for creating a temporary Collection instance. Behaves like OCL
     * <tt>iterable</tt>->collect(e | functor.collectFrom(e))
     */
    public static <Input, Output> Iterable<Output> collect(Iterable<? extends Input> iterable,
            Function<Input, Iterable<? extends Output>> functor)
    {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.collect(i, e -> emptyIfNull(functor.apply(e))));
    }

    /**
     * Collects elements without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
     * Behaves like OCL <tt>iterable</tt>->xcollect(e | functor.collectFrom(e))
     *
     * @see IterableUtil#notNull(Iterable)
     */
    public static <Input, Output> Iterable<Output> xcollect(Iterable<? extends Input> iterable,
            Function<Input, Iterable<? extends Output>> functor)
    {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.xcollect(i, e -> emptyIfNull(functor.apply(e))));
    }

    /**
     * Collects single attribute values without the need for creating a temporary Collection instance. Behaves like OCL
     * <tt>iterable</tt>->collect(e | functor.getOne(e))
     *
     * @see IteratorQueries#collectOne(Iterator, Function)
     */
    public static <Input, Output> Iterable<Output> collectOne(Iterable<? extends Input> iterable,
            Function<Input, Output> functor)
    {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.collectOne(i, functor));
    }

    /**
     * Collects single attribute values without the need for creating a temporary Collection instance, skipping
     * <tt>null</tt> values. Behaves like OCL <tt>iterable</tt>->xcollect(e | functor.getOne(e))
     *
     * @see IteratorQueries#xcollectOne(Iterator, Function)
     */
    public static <Input, Output> Iterable<Output> xcollectOne(Iterable<? extends Input> iterable,
            Function<Input, Output> functor)
    {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.xcollectOne(i, functor));
    }

    /**
     * Behaves like OCL <tt>iterable</tt>->forAll(e | functor.test(e))
     */
    public static <Input> boolean forAll(Iterable<? extends Input> iterable, Predicate<Input> functor) {
        return IteratorQueries.forAll(iterable.iterator(), functor);
    }

    /**
     * Behaves like QVTo <tt>iterable</tt>->exists(e | functor.test(e))
     */
    public static <Input> boolean exists(Iterable<? extends Input> iterable, Predicate<Input> functor) {
        return IteratorQueries.exists(iterable.iterator(), functor);
    }

    /**
     * Behaves like QVTo <tt>iterable</tt>->any(e | functor.test(e))
     */
    public static <Input> Input any(Iterable<? extends Input> iterable, Predicate<? super Input> functor) {
        return IteratorQueries.any(iterable.iterator(), functor);
    }

    /**
     * Returns an iterable which pairs the elements of <tt>iterableA</tt> and <tt>iterableB</tt> such that Pair{n}.a =
     * iterableA{n} and Pair{n}.b = iterableB{n}. The amount of elements in the resulting iterator will be deduced from
     * either <tt>iterableA</tt> or <tt>iterableB</tt> depending on which of both produces the least amount of elements.
     *
     * @see IteratorQueries#zip(Iterator, Iterator)
     */
    public static <InputA, InputB> Iterable<Pair<InputA, InputB>> zip(Iterable<? extends InputA> iterableA,
            Iterable<? extends InputB> iterableB)
    {
        return new LoggableIterable<Pair<InputA, InputB>>() {
            @Override
            public Iterator<Pair<InputA, InputB>> iterator() {
                return IteratorQueries.zip(iterableA.iterator(), iterableB.iterator());
            }
        };
    }

    /**
     * Returns an iterable which pairs the elements of <tt>iteratorA</tt> and <tt>iteratorB</tt> such that Pair{n}.a =
     * iteratorA{n} and Pair{n}.b = iteratorB{n + offset}. The amount of elements in the resulting iterator will be
     * deduced from either <tt>iteratorA</tt> or <tt>iteratorB</tt> depending on <tt>includeOneSideOnly</tt> which of
     * both produces the least/most amount of elements.
     *
     * @see IteratorQueries#zip(Iterator, Iterator, boolean, int)
     */
    public static <InputA, InputB> Iterable<Pair<InputA, InputB>> zip(Iterable<? extends InputA> iterableA,
            Iterable<? extends InputB> iterableB, boolean includeOneSideOnly, int offset)
    {
        return new LoggableIterable<Pair<InputA, InputB>>() {
            @Override
            public Iterator<Pair<InputA, InputB>> iterator() {
                return IteratorQueries.zip(iterableA.iterator(), iterableB.iterator(), includeOneSideOnly, offset);
            }
        };
    }

    /**
     * Returns an iterator which pairs the elements of <tt>iteratorA</tt> and <tt>iteratorB</tt> such that <br>
     * <code>comparator.compare(Pair.a, Pair.b) == 0</code>.<br>
     * For elements which only occur in <tt>iteratorA</tt> or <tt>iteratorB</tt> an incomplete pair will be returned
     * (meaning respectively either a or b will be <tt>null</tt>)<br>
     * <br>
     * <b>NOTE:</b> Both this <tt>iterableA</tt> as <tt>iterableB</tt> should be sorted with respect to the
     * <tt>comparator</tt>
     */
    public static <InputA, InputB> Iterable<Pair<InputA, InputB>> zip(Iterable<? extends InputA> iterableA,
            Iterable<? extends InputB> iterableB, ToIntBiFunction<? super InputA, ? super InputB> comparator)
    {
        return new LoggableIterable<Pair<InputA, InputB>>() {
            @Override
            public Iterator<Pair<InputA, InputB>> iterator() {
                return IteratorQueries.zip(iterableA.iterator(), iterableB.iterator(), comparator);
            }
        };
    }

    /**
     * Behaves like QVTo <tt>iterableA</tt>->product(<tt>iterableB</tt>)
     *
     * @see IteratorQueries#product(Iterator, Iterator)
     */
    public static <InputA, InputB> Iterable<Pair<InputA, InputB>> product(Iterable<? extends InputA> iterableA,
            Iterable<? extends InputB> iterableB)
    {
        return new LoggableIterable<Pair<InputA, InputB>>() {
            @Override
            public Iterator<Pair<InputA, InputB>> iterator() {
                return IteratorQueries.product(iterableA.iterator(), iterableB.iterator());
            }
        };
    }

    /**
     * Returns a map for which the Map.values are the given elements in the given order, and each key is the product of
     * invoking a supplied function computeKeys on its corresponding value. If the function produces the same key for
     * different values, the last one will be contained in the map.
     */
    public static <Input, K> Map<K, Input> toMap(Iterable<Input> iterable, Function<? super Input, K> computeKeys) {
        return IteratorQueries.toMap(iterable.iterator(), computeKeys);
    }

    /**
     * Returns a map for which the Map.values are the product of invoking supplied function computeValues on input
     * iterable elements, and each key is the product of invoking a supplied function computeKeys on same elements. If
     * the function produces the same key for different values, the last one will be contained in the map.
     */
    public static <Input, K, V> Map<K, V> toMap(Iterable<Input> iterable, Function<? super Input, K> computeKeys,
            Function<? super Input, V> computeValues)
    {
        return IteratorQueries.toMap(iterable.iterator(), computeKeys, computeValues);
    }

    /**
     * Returns a map for which the Map.values is a collection of lists, where the elements in the list will appear in
     * the order as they appeared in the iterable. Each key is the product of invoking the supplied function computeKeys
     * on its corresponding value. So a key of that map groups a list of values for which the function produced exactly
     * that key.
     */
    public static <Input, K> Map<K, List<Input>> groupBy(Iterable<Input> iterable,
            Function<? super Input, K> computeKeys)
    {
        return IteratorQueries.groupBy(iterable.iterator(), computeKeys);
    }

    /**
     * Returns a map for which the Map.Entry.value is the reduced value of all values found for a key. Each key is the
     * product of invoking the supplied function computeKeys on its corresponding value.
     */
    public static <Input, K, V> Map<K, V> groupByAndReduce(Iterable<Input> iterable,
            Function<? super Input, K> computeKeys, BiFunction<? super Input, V, ? extends V> computeValues)
    {
        return IteratorQueries.groupByAndReduce(iterable.iterator(), computeKeys, computeValues);
    }

    /**
     * Groups a source of inputs into groups of equivalent inputs.
     *
     * @param <Input> type of input
     * @param iterable source of inputs
     * @param equivalence defines the equivalence relation of two inputs, e.g. {@link Objects#equals(Object, Object)}.
     * @return a list of lists where every list contains equivalent inputs.
     * @see #groupBy(Iterable, Comparator, BiPredicate)
     */
    public static <Input> List<List<Input>> groupBy(Iterable<Input> iterable,
            BiPredicate<? super Input, ? super Input> equivalence)
    {
        return IteratorQueries.groupBy(iterable.iterator(), equivalence);
    }

    /**
     * Groups a source of inputs into groups of equivalent inputs.
     *
     * @param <Input> type of input
     * @param iterable source of inputs
     * @param comparator If not {@code null}, sorts the groups for performance reasons, e.g. ascending list size when
     *     all groups are expected to contain the same amount of elements or descending list size when a few groups are
     *     expected to contain most of the elements.
     * @param equivalence defines the equivalence relation of two inputs, e.g. {@link Objects#equals(Object, Object)}.
     * @return a list of lists where every list contains equivalent inputs.
     */
    public static <Input> List<List<Input>> groupBy(Iterable<Input> iterable,
            Comparator<? super List<Input>> comparator, BiPredicate<? super Input, ? super Input> equivalence)
    {
        return IteratorQueries.groupBy(iterable.iterator(), comparator, equivalence);
    }

    /**
     * Segments {@code iterable} into groups, based on the {@code predicate}.
     *
     * @param <Input> type of input
     * @param iterable source of inputs
     * @param predicate defines where to segment, based on two subsequent entries of the {@code iterable}.
     * @return an iterable of lists (segments).
     */
    public static <Input> Iterable<List<Input>> segment(Iterable<Input> iterable,
            BiPredicate<? super Input, ? super Input> predicate)
    {
        return IterableUtil.wrap(iterable, i -> IteratorQueries.segment(i, predicate));
    }

    private static <T> Iterator<T> emptyIfNull(Iterable<T> iterable) {
        return null == iterable ? Collections.emptyIterator() : iterable.iterator();
    }
}
