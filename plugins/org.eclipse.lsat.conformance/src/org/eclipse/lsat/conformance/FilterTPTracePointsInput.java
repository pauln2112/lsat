/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.lsat.trace.TraceModel;

import activity.Activity;
import dispatching.ActivityDispatching;
import dispatching.Dispatch;
import dispatching.DispatchGroup;

public class FilterTPTracePointsInput {
    private final List<TraceModel> traceModels;

    private final List<Activity> activities = new ArrayList<Activity>();

    public FilterTPTracePointsInput(ActivityDispatching activityDispatching, List<TraceModel> traceModels) {
        this.traceModels = traceModels;
        for (DispatchGroup group: activityDispatching.getDispatchGroups()) {
            for (Dispatch dispatch: group.getDispatches()) {
                this.activities.add(dispatch.getActivity());
            }
        }
    }

    public FilterTPTracePointsInput(Collection<Activity> activities, TraceModel traceModels) {
        this.traceModels = new ArrayList<TraceModel>(1);
        this.traceModels.add(traceModels);
        this.activities.addAll(activities);
    }

    public List<TraceModel> getTraceModels() {
        return traceModels;
    }

    public List<Activity> getActivities() {
        return activities;
    }
}
