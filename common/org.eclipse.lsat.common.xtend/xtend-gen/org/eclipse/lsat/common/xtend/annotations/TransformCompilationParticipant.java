/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend.annotations;

import java.util.Queue;
import org.eclipse.xtend.lib.macro.AbstractMethodProcessor;
import org.eclipse.xtend.lib.macro.TransformationContext;
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableMethodDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableParameterDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration;
import org.eclipse.xtend.lib.macro.declaration.ResolvedMethod;
import org.eclipse.xtend.lib.macro.declaration.TypeReference;
import org.eclipse.xtend.lib.macro.declaration.Visibility;
import org.eclipse.xtend2.lib.StringConcatenationClient;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class TransformCompilationParticipant extends AbstractMethodProcessor {
  @Override
  public void doTransform(final MutableMethodDeclaration annotatedMethod, @Extension final TransformationContext context) {
    annotatedMethod.markAsRead();
    boolean _isStatic = annotatedMethod.isStatic();
    if (_isStatic) {
      context.addError(annotatedMethod, "Transformation methods cannot be static");
      return;
    } else {
      MutableTypeDeclaration _declaringType = annotatedMethod.getDeclaringType();
      boolean _not = (!(_declaringType instanceof MutableClassDeclaration));
      if (_not) {
        context.addError(annotatedMethod, "Transformation methods should be declared on class");
        return;
      }
    }
    final String transformName = annotatedMethod.getSimpleName();
    annotatedMethod.setSimpleName(("_transform_" + transformName));
    annotatedMethod.setVisibility(Visibility.PRIVATE);
    final Procedure1<MutableMethodDeclaration> _function = (MutableMethodDeclaration it) -> {
      it.setVisibility(Visibility.PUBLIC);
      it.setFinal(annotatedMethod.isFinal());
      it.setSynchronized(true);
      it.setReturnType(annotatedMethod.getReturnType());
      it.setExceptions(((TypeReference[])Conversions.unwrapArray(annotatedMethod.getExceptions(), TypeReference.class)));
      Iterable<? extends MutableParameterDeclaration> _parameters = annotatedMethod.getParameters();
      for (final MutableParameterDeclaration parameter : _parameters) {
        it.addParameter(parameter.getSimpleName(), parameter.getType());
      }
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          {
            boolean _isVoid = it.getReturnType().isVoid();
            boolean _not = (!_isVoid);
            if (_not) {
              String _simpleName = it.getReturnType().getSimpleName();
              _builder.append(_simpleName);
              _builder.append(" result = ");
            }
          }
          String _simpleName_1 = annotatedMethod.getSimpleName();
          _builder.append(_simpleName_1);
          _builder.append("(");
          {
            Iterable<? extends MutableParameterDeclaration> _parameters = annotatedMethod.getParameters();
            boolean _hasElements = false;
            for(final MutableParameterDeclaration parameter : _parameters) {
              if (!_hasElements) {
                _hasElements = true;
              } else {
                _builder.appendImmediate(", ", "");
              }
              String _simpleName_2 = parameter.getSimpleName();
              _builder.append(_simpleName_2);
            }
          }
          _builder.append(");");
          _builder.newLineIfNotEmpty();
          _builder.append("_performLateFunctions();");
          _builder.newLine();
          {
            boolean _isVoid_1 = it.getReturnType().isVoid();
            boolean _not_1 = (!_isVoid_1);
            if (_not_1) {
              _builder.append("return result;");
            }
          }
          _builder.newLineIfNotEmpty();
        }
      };
      it.setBody(_client);
    };
    annotatedMethod.getDeclaringType().addMethod(transformName, _function);
    MutableTypeDeclaration _declaringType_1 = annotatedMethod.getDeclaringType();
    this.doTransform(((MutableClassDeclaration) _declaringType_1), context);
  }
  
  public void doTransform(final MutableClassDeclaration annotatedClass, @Extension final TransformationContext context) {
    final Function1<ResolvedMethod, String> _function = (ResolvedMethod it) -> {
      return it.getDeclaration().getSimpleName();
    };
    boolean _contains = IterableExtensions.contains(IterableExtensions.map(context.newTypeReference(annotatedClass).getAllResolvedMethods(), _function), "late");
    if (_contains) {
      return;
    }
    final Procedure1<MutableFieldDeclaration> _function_1 = (MutableFieldDeclaration it) -> {
      it.setVisibility(Visibility.PRIVATE);
      it.setFinal(true);
      it.setType(context.newTypeReference(Queue.class, context.newTypeReference(Runnable.class)));
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("new java.util.LinkedList<>()");
        }
      };
      it.setInitializer(_client);
    };
    annotatedClass.addField("_lateFunctions", _function_1);
    final Procedure1<MutableMethodDeclaration> _function_2 = (MutableMethodDeclaration it) -> {
      it.setVisibility(Visibility.PROTECTED);
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("while (!_lateFunctions.isEmpty()) {");
          _builder.newLine();
          _builder.append("    ");
          _builder.append("_lateFunctions.remove().run();");
          _builder.newLine();
          _builder.append("}");
          _builder.newLine();
        }
      };
      it.setBody(_client);
    };
    annotatedClass.addMethod("_performLateFunctions", _function_2);
    final Procedure1<MutableMethodDeclaration> _function_3 = (MutableMethodDeclaration it) -> {
      it.setDocComment("Late functions are executed at the end of the transformation, just before returning the result.");
      it.setVisibility(Visibility.PROTECTED);
      it.setFinal(true);
      it.addParameter("function", context.newTypeReference(Runnable.class));
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("_lateFunctions.add(function);");
        }
      };
      it.setBody(_client);
    };
    annotatedClass.addMethod("late", _function_3);
  }
}
