/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor.ui.quickfix;

import activity.Activity;
import activity.ActivityFactory;
import activity.ActivitySet;
import activity.Event;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.diagnostics.Diagnostic;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification;
import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;

/**
 * Custom quickfixes.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#quickfixes
 */
@SuppressWarnings("all")
public class ActivityQuickfixProvider extends DefaultQuickfixProvider {
  @Fix(Diagnostic.LINKING_DIAGNOSTIC)
  public void createEvent(final Issue issue, final IssueResolutionAcceptor acceptor) {
    final ISemanticModification _function = (EObject element, IModificationContext context) -> {
      EObject _eContainer = element.eContainer();
      final Activity activity = ((Activity) _eContainer);
      EObject _eContainer_1 = activity.eContainer();
      final ActivitySet aSet = ((ActivitySet) _eContainer_1);
      final Event event = ActivityFactory.eINSTANCE.createEvent();
      final String text = context.getXtextDocument().get((issue.getOffset()).intValue(), (issue.getLength()).intValue());
      event.setName(text);
      EList<Event> _events = aSet.getEvents();
      _events.add(event);
    };
    acceptor.accept(issue, "Create event", "Create event", "", _function);
  }
}
