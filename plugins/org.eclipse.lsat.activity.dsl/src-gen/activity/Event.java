/**
 */
package activity;

import machine.Resource;
import machine.ResourceType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activity.ActivityPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends Resource
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     * @generated
     */
    ResourceType getResourceType();

} // Event
