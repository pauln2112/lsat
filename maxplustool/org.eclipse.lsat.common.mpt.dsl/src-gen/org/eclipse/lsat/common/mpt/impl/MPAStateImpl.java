/**
 */
package org.eclipse.lsat.common.mpt.impl;

import org.eclipse.lsat.common.mpt.FSMState;
import org.eclipse.lsat.common.mpt.MPAState;
import org.eclipse.lsat.common.mpt.MPTPackage;
import org.eclipse.lsat.common.mpt.Matrix;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MPA State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.impl.MPAStateImpl#getState <em>State</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.impl.MPAStateImpl#getMatrixRowIndex <em>Matrix Row Index</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.impl.MPAStateImpl#getMatrixColumnIndex <em>Matrix Column Index</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.impl.MPAStateImpl#getMatrix <em>Matrix</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.impl.MPAStateImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MPAStateImpl extends VertexImpl implements MPAState {
	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected FSMState state;

	/**
	 * The default value of the '{@link #getMatrixRowIndex() <em>Matrix Row Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatrixRowIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int MATRIX_ROW_INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMatrixRowIndex() <em>Matrix Row Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatrixRowIndex()
	 * @generated
	 * @ordered
	 */
	protected int matrixRowIndex = MATRIX_ROW_INDEX_EDEFAULT;

	/**
	 * The default value of the '{@link #getMatrixColumnIndex() <em>Matrix Column Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatrixColumnIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int MATRIX_COLUMN_INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMatrixColumnIndex() <em>Matrix Column Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatrixColumnIndex()
	 * @generated
	 * @ordered
	 */
	protected int matrixColumnIndex = MATRIX_COLUMN_INDEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMatrix() <em>Matrix</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatrix()
	 * @generated
	 * @ordered
	 */
	protected Matrix matrix;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final double VALUE_EDEFAULT = 0.0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPAStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MPTPackage.Literals.MPA_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FSMState getState() {
		if (state != null && state.eIsProxy()) {
			InternalEObject oldState = (InternalEObject)state;
			state = (FSMState)eResolveProxy(oldState);
			if (state != oldState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MPTPackage.MPA_STATE__STATE, oldState, state));
			}
		}
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMState basicGetState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setState(FSMState newState) {
		FSMState oldState = state;
		state = newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MPTPackage.MPA_STATE__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMatrixRowIndex() {
		return matrixRowIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMatrixRowIndex(int newMatrixRowIndex) {
		int oldMatrixRowIndex = matrixRowIndex;
		matrixRowIndex = newMatrixRowIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MPTPackage.MPA_STATE__MATRIX_ROW_INDEX, oldMatrixRowIndex, matrixRowIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMatrixColumnIndex() {
		return matrixColumnIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMatrixColumnIndex(int newMatrixColumnIndex) {
		int oldMatrixColumnIndex = matrixColumnIndex;
		matrixColumnIndex = newMatrixColumnIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MPTPackage.MPA_STATE__MATRIX_COLUMN_INDEX, oldMatrixColumnIndex, matrixColumnIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Matrix getMatrix() {
		if (matrix != null && matrix.eIsProxy()) {
			InternalEObject oldMatrix = (InternalEObject)matrix;
			matrix = (Matrix)eResolveProxy(oldMatrix);
			if (matrix != oldMatrix) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MPTPackage.MPA_STATE__MATRIX, oldMatrix, matrix));
			}
		}
		return matrix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Matrix basicGetMatrix() {
		return matrix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMatrix(Matrix newMatrix) {
		Matrix oldMatrix = matrix;
		matrix = newMatrix;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MPTPackage.MPA_STATE__MATRIX, oldMatrix, matrix));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public double getValue() {
		final Matrix matrix = getMatrix();
		if (null == matrix) return Double.NaN;
		return matrix.getValue(getMatrixRowIndex(), getMatrixColumnIndex());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MPTPackage.MPA_STATE__STATE:
				if (resolve) return getState();
				return basicGetState();
			case MPTPackage.MPA_STATE__MATRIX_ROW_INDEX:
				return getMatrixRowIndex();
			case MPTPackage.MPA_STATE__MATRIX_COLUMN_INDEX:
				return getMatrixColumnIndex();
			case MPTPackage.MPA_STATE__MATRIX:
				if (resolve) return getMatrix();
				return basicGetMatrix();
			case MPTPackage.MPA_STATE__VALUE:
				return getValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MPTPackage.MPA_STATE__STATE:
				setState((FSMState)newValue);
				return;
			case MPTPackage.MPA_STATE__MATRIX_ROW_INDEX:
				setMatrixRowIndex((Integer)newValue);
				return;
			case MPTPackage.MPA_STATE__MATRIX_COLUMN_INDEX:
				setMatrixColumnIndex((Integer)newValue);
				return;
			case MPTPackage.MPA_STATE__MATRIX:
				setMatrix((Matrix)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MPTPackage.MPA_STATE__STATE:
				setState((FSMState)null);
				return;
			case MPTPackage.MPA_STATE__MATRIX_ROW_INDEX:
				setMatrixRowIndex(MATRIX_ROW_INDEX_EDEFAULT);
				return;
			case MPTPackage.MPA_STATE__MATRIX_COLUMN_INDEX:
				setMatrixColumnIndex(MATRIX_COLUMN_INDEX_EDEFAULT);
				return;
			case MPTPackage.MPA_STATE__MATRIX:
				setMatrix((Matrix)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MPTPackage.MPA_STATE__STATE:
				return state != null;
			case MPTPackage.MPA_STATE__MATRIX_ROW_INDEX:
				return matrixRowIndex != MATRIX_ROW_INDEX_EDEFAULT;
			case MPTPackage.MPA_STATE__MATRIX_COLUMN_INDEX:
				return matrixColumnIndex != MATRIX_COLUMN_INDEX_EDEFAULT;
			case MPTPackage.MPA_STATE__MATRIX:
				return matrix != null;
			case MPTPackage.MPA_STATE__VALUE:
				return getValue() != VALUE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (matrixRowIndex: ");
		result.append(matrixRowIndex);
		result.append(", matrixColumnIndex: ");
		result.append(matrixColumnIndex);
		result.append(')');
		return result.toString();
	}

} //MPAStateImpl
