/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
#include "pch.h"
#include "json_server.h"
#include <stdlib.h>
#include <string.h>
#include <string>
int request(char* response, const char* request, size_t response_size)
{
    printf("%s\n", request);

    const char* r =  "{\"requestType\":\"PositionInfo\",\"motionProfiles\":[{\"key\":\"aProfile\",\"name\":\"aName\",\"url\":\"https://aUrl\",\"parameters\":[{\"key\":\"aKey\",\"name\":\"aParName\",\"required\":true},{\"key\":\"rKey\",\"name\":\"rParName\",\"required\":false}]}],\"positionInfo\":[{\"setPointId\":\"X\",\"timePositions\":[[2.0,2.0],[1.0,1.0],[0.0,0.0]]},{\"setPointId\":\"Y\",\"timePositions\":[[2.0,2.0],[1.0,1.0],[0.0,0.0]]}]}";
    if (strlen(r) > response_size - 1) {
        return -1;
    }
    
    strncpy_s(response, response_size,  r,strlen(r));
    return 0;
}
