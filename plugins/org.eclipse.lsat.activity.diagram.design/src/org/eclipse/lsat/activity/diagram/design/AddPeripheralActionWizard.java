/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.design;

import java.util.Collection;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.lsat.common.emf.ui.model.ModelWorkbenchContentProvider;
import org.eclipse.lsat.common.emf.ui.wizards.ElementTreeSelectionWizardPage;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

import activity.PeripheralAction;
import machine.IResource;
import machine.Machine;
import machine.Peripheral;
import machine.Resource;
import machine.provider.MachineItemProviderAdapterFactory;

public class AddPeripheralActionWizard extends Wizard {
    private final ElementTreeSelectionWizardPage peripheralSelectionPage;

    private final AddPeripheralActionPage peripheralActionPage;

    private PeripheralAction peripheralAction;

    public AddPeripheralActionWizard(Collection<EObject> roots, IResource resource, EObject initialSelection,
            String initialActionName)
    {
        ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
                ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
        adapterFactory.addAdapterFactory(new MachineItemProviderAdapterFactory());
        adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());

        peripheralSelectionPage = createPeripheralSelectionPage(roots, initialSelection, adapterFactory);
        peripheralActionPage = new AddPeripheralActionPage(resource, initialActionName, adapterFactory);
    }

    private ElementTreeSelectionWizardPage createPeripheralSelectionPage(Collection<EObject> roots,
            EObject initialSelection, AdapterFactory adapterFactory)
    {
        ElementTreeSelectionWizardPage peripheralSelectionPage = new ElementTreeSelectionWizardPage(
                "PeripheralSelectionPage", new AdapterFactoryLabelProvider(adapterFactory),
                new ModelWorkbenchContentProvider());
        peripheralSelectionPage.setTitle("Select peripheral");
        peripheralSelectionPage.setMessage("Select peripheral to add action");
        peripheralSelectionPage.setInput(roots);
        peripheralSelectionPage.setAllowMultiple(false);
        Resource targetResource = initialSelection instanceof IResource ? ((IResource)initialSelection).getResource()
                : null;
        peripheralSelectionPage.addFilter(new ViewerFilter() {
            public boolean select(Viewer viewer, Object parentElement, Object element) {
                return element instanceof Machine
                        || (targetResource == element || targetResource == null && element instanceof Resource)
                        || element instanceof Peripheral;
            }
        });
        peripheralSelectionPage.setValidator(new ISelectionStatusValidator() {
            public IStatus validate(Object[] selection) {
                if (selection.length <= 0) {
                    return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Please select a peripheral!", null);
                }
                for (Object selected: selection) {
                    if (!(selected instanceof Peripheral)) {
                        return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0,
                                "Only peripheral selection is allowed!", null);
                    }
                }
                return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, "OK", null);
            }
        });
        peripheralSelectionPage.setInitialSelections(initialSelection);
        return peripheralSelectionPage;
    }

    @Override
    public void addPages() {
        addPage(peripheralSelectionPage);
        addPage(peripheralActionPage);
    }

    @Override
    public IWizardPage getStartingPage() {
        if (peripheralSelectionPage.isPageComplete()) {
            return getNextPage(peripheralSelectionPage);
        }
        return super.getStartingPage();
    }

    @Override
    public IWizardPage getNextPage(IWizardPage page) {
        if (peripheralSelectionPage == page) {
            peripheralActionPage.setPeripheral((Peripheral)peripheralSelectionPage.getResult()[0]);
        }
        return super.getNextPage(page);
    }

    @Override
    public boolean performFinish() {
        peripheralAction = peripheralActionPage.createPeripheralAction();
        return true;
    }

    public PeripheralAction getPeripheralAction() {
        return peripheralAction;
    }
}
