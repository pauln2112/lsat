<!--

    Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.eclipse.lsat</groupId>
        <artifactId>org.eclipse.lsat.root</artifactId>
        <version>0.3.0-SNAPSHOT</version>
        <relativePath>../../</relativePath>
    </parent>

    <artifactId>org.eclipse.lsat.website</artifactId>
    <packaging>pom</packaging>
    <name>LSAT Website</name>

    <dependencies>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>org.eclipse.lsat.documentation</artifactId>
            <version>${project.version}</version>
            <classifier>userguide</classifier>
            <type>zip</type>
        </dependency>
        <dependency>
            <groupId>${project.groupId}</groupId>
            <artifactId>org.eclipse.lsat.design</artifactId>
            <version>${project.version}</version>
            <classifier>design</classifier>
            <type>zip</type>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.asciidoctor</groupId>
                <artifactId>asciidoctor-maven-plugin</artifactId>
                <!-- Attributes common to all output formats -->
                <executions>
                    <!-- Rendering the website -->
                    <execution>
                        <id>generate-website</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>process-asciidoc</goal>
                        </goals>
                        <configuration>
                            <sourceDirectory>adoc</sourceDirectory>
                            <outputDirectory>${project.build.directory}/website</outputDirectory>
                            <backend>html5</backend>
                            <attributes>
                                <imagesdir />
                                <imgsdir>images</imgsdir>
                                <doctype>book</doctype>
                                <icons>font</icons>
                                <sectanchors>true</sectanchors>
                                <toc>left</toc>
                                <toclevels>3</toclevels>
                                <lsat-binary-prefix>${lsat.binary.prefix}</lsat-binary-prefix>
                                <lsat-version-enduser>${lsat.version.enduser}</lsat-version-enduser>
                            </attributes>
                            <resources>
                                <resource>
                                    <directory>${basedir}/images</directory>
                                    <targetPath>images</targetPath>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <id>assemble-website</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <finalName>${lsat.binary.prefix}-${lsat.version.enduser}</finalName>
                            <descriptors>
                                <descriptor>${basedir}/assembly.xml</descriptor>
                            </descriptors>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
