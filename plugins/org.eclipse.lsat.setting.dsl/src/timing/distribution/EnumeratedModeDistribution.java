/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package timing.distribution;

import org.apache.commons.math3.distribution.EnumeratedRealDistribution;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.util.Pair;

public class EnumeratedModeDistribution extends EnumeratedRealDistribution implements ModeDistribution {
    private static final long serialVersionUID = -5796357692875297528L;

    private final Double itsDefault;

    public EnumeratedModeDistribution(RandomGenerator rng, double[] data, Double _default) {
        super(rng, data);
        itsDefault = _default;
    }

    /**
     * If no number is repeated, then there is no mode for the list.
     *
     * @see timing.distribution.ModeDistribution#getMode()
     */
    @Override
    public double getMode() throws ModeNotSupportedException {
        Double mode = null;
        double maxProb = Double.NEGATIVE_INFINITY;
        for (Pair<Double, Double> sample: innerDistribution.getPmf()) {
            double prob = sample.getSecond();
            if (prob > maxProb) {
                maxProb = prob;
                mode = sample.getFirst();
            } else if (prob == maxProb) {
                mode = null;
            }
        }
        if (null == mode) {
            throw new ModeNotSupportedException("The distribution has no mode");
        }
        return mode;
    }

    @Override
    public double getDefault() {
        if (null != itsDefault) {
            return itsDefault;
        }
        return getNumericalMean();
    }
}
