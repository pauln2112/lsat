/**
 */
package dispatching.impl;

import activity.Activity;

import dispatching.Attribute;
import dispatching.Dispatch;
import dispatching.DispatchingPackage;
import java.util.Collection;
import machine.ResourceItem;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Dispatch</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dispatching.impl.DispatchImpl#getUserAttributes <em>User Attributes</em>}</li>
 *   <li>{@link dispatching.impl.DispatchImpl#getActivity <em>Activity</em>}</li>
 *   <li>{@link dispatching.impl.DispatchImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link dispatching.impl.DispatchImpl#getResourceItems <em>Resource Items</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DispatchImpl extends MinimalEObjectImpl.Container implements Dispatch {
	/**
	 * The cached value of the '{@link #getUserAttributes() <em>User Attributes</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserAttributes()
	 * @generated
	 * @ordered
	 */
	protected EMap<Attribute, String> userAttributes;

	/**
	 * The cached value of the '{@link #getActivity() <em>Activity</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getActivity()
	 * @generated
	 * @ordered
	 */
	protected Activity activity;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getResourceItems() <em>Resource
	 * Items</em>}' reference list. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getResourceItems()
	 * @generated
	 * @ordered
	 */
	protected EList<ResourceItem> resourceItems;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected DispatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DispatchingPackage.Literals.DISPATCH;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Activity getActivity() {
		if (activity != null && activity.eIsProxy()) {
			InternalEObject oldActivity = (InternalEObject)activity;
			activity = (Activity)eResolveProxy(oldActivity);
			if (activity != oldActivity) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DispatchingPackage.DISPATCH__ACTIVITY, oldActivity, activity));
			}
		}
		return activity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Activity basicGetActivity() {
		return activity;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActivity(Activity newActivity) {
		Activity oldActivity = activity;
		activity = newActivity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.DISPATCH__ACTIVITY, oldActivity, activity));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.DISPATCH__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ResourceItem> getResourceItems() {
		if (resourceItems == null) {
			resourceItems = new EObjectResolvingEList<ResourceItem>(ResourceItem.class, this, DispatchingPackage.DISPATCH__RESOURCE_ITEMS);
		}
		return resourceItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<Attribute, String> getUserAttributes() {
		if (userAttributes == null) {
			userAttributes = new EcoreEMap<Attribute,String>(DispatchingPackage.Literals.ATTRIBUTES_MAP_ENTRY, AttributesMapEntryImpl.class, this, DispatchingPackage.DISPATCH__USER_ATTRIBUTES);
		}
		return userAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH__USER_ATTRIBUTES:
				return ((InternalEList<?>)getUserAttributes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH__USER_ATTRIBUTES:
				if (coreType) return getUserAttributes();
				else return getUserAttributes().map();
			case DispatchingPackage.DISPATCH__ACTIVITY:
				if (resolve) return getActivity();
				return basicGetActivity();
			case DispatchingPackage.DISPATCH__DESCRIPTION:
				return getDescription();
			case DispatchingPackage.DISPATCH__RESOURCE_ITEMS:
				return getResourceItems();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH__USER_ATTRIBUTES:
				((EStructuralFeature.Setting)getUserAttributes()).set(newValue);
				return;
			case DispatchingPackage.DISPATCH__ACTIVITY:
				setActivity((Activity)newValue);
				return;
			case DispatchingPackage.DISPATCH__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case DispatchingPackage.DISPATCH__RESOURCE_ITEMS:
				getResourceItems().clear();
				getResourceItems().addAll((Collection<? extends ResourceItem>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH__USER_ATTRIBUTES:
				getUserAttributes().clear();
				return;
			case DispatchingPackage.DISPATCH__ACTIVITY:
				setActivity((Activity)null);
				return;
			case DispatchingPackage.DISPATCH__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case DispatchingPackage.DISPATCH__RESOURCE_ITEMS:
				getResourceItems().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DispatchingPackage.DISPATCH__USER_ATTRIBUTES:
				return userAttributes != null && !userAttributes.isEmpty();
			case DispatchingPackage.DISPATCH__ACTIVITY:
				return activity != null;
			case DispatchingPackage.DISPATCH__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case DispatchingPackage.DISPATCH__RESOURCE_ITEMS:
				return resourceItems != null && !resourceItems.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(')');
		return result.toString();
	}

} // DispatchImpl
