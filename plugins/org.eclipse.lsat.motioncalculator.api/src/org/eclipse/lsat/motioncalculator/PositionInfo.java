/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Container for timePosition info for a given setPointId
 */
public final class PositionInfo implements Serializable {
    private static final long serialVersionUID = 6966667096730059599L;

    private String setPointId;

    private List<double[]> timePositions = new ArrayList<>();

    public PositionInfo(String setPointId) {
        this.setPointId = setPointId;
    }

    public String getSetPointId() {
        return setPointId;
    }

    public void addTimePosition(double time, double position) {
        timePositions.add(new double[] {time, position});
    }

    public Collection<TimePosition> getTimePositions() {
        return Collections.unmodifiableCollection(new AbstractList<TimePosition>() {
            @Override
            public TimePosition get(int index) {
                double[] d = timePositions.get(index);
                return new TimePosition(d[0], d[1]);
            }

            @Override
            public int size() {
                return timePositions.size();
            }
        });
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((setPointId == null) ? 0 : setPointId.hashCode());
        result = prime * result + ((timePositions == null) ? 0 : timePositions.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PositionInfo other = (PositionInfo)obj;
        if (setPointId == null) {
            if (other.setPointId != null) {
                return false;
            }
        } else if (!setPointId.equals(other.setPointId)) {
            return false;
        }
        if (timePositions == null) {
            if (other.timePositions != null) {
                return false;
            }
        } else if (!timePositions.equals(other.timePositions)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PositionInfo [setPointId=" + setPointId + ", timePositions=" + timePositions + "]";
    }
}
