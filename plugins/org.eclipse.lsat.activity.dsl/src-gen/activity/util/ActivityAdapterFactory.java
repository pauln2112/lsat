/**
 */
package activity.util;

import activity.*;

import machine.HasResourcePeripheral;
import machine.IResource;
import machine.ImportContainer;
import machine.Resource;
import machine.ResourceItem;
import org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph;
import org.eclipse.lsat.common.graph.directed.editable.Node;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see activity.ActivityPackage
 * @generated
 */
public class ActivityAdapterFactory extends AdapterFactoryImpl {
	/**
     * The cached model package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static ActivityPackage modelPackage;

	/**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ActivityAdapterFactory() {
        if (modelPackage == null)
        {
            modelPackage = ActivityPackage.eINSTANCE;
        }
    }

	/**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
	@Override
	public boolean isFactoryForType(Object object) {
        if (object == modelPackage)
        {
            return true;
        }
        if (object instanceof EObject)
        {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

	/**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActivitySwitch<Adapter> modelSwitch =
		new ActivitySwitch<Adapter>()
        {
            @Override
            public Adapter caseActivitySet(ActivitySet object)
            {
                return createActivitySetAdapter();
            }
            @Override
            public Adapter caseActivity(Activity object)
            {
                return createActivityAdapter();
            }
            @Override
            public Adapter caseAction(Action object)
            {
                return createActionAdapter();
            }
            @Override
            public Adapter casePeripheralAction(PeripheralAction object)
            {
                return createPeripheralActionAdapter();
            }
            @Override
            public Adapter caseClaim(Claim object)
            {
                return createClaimAdapter();
            }
            @Override
            public Adapter caseRelease(Release object)
            {
                return createReleaseAdapter();
            }
            @Override
            public Adapter caseLocationPrerequisite(LocationPrerequisite object)
            {
                return createLocationPrerequisiteAdapter();
            }
            @Override
            public Adapter caseTracePoint(TracePoint object)
            {
                return createTracePointAdapter();
            }
            @Override
            public Adapter caseSyncBar(SyncBar object)
            {
                return createSyncBarAdapter();
            }
            @Override
            public Adapter caseSimpleAction(SimpleAction object)
            {
                return createSimpleActionAdapter();
            }
            @Override
            public Adapter caseMove(Move object)
            {
                return createMoveAdapter();
            }
            @Override
            public Adapter caseEventAction(EventAction object)
            {
                return createEventActionAdapter();
            }
            @Override
            public Adapter caseRaiseEvent(RaiseEvent object)
            {
                return createRaiseEventAdapter();
            }
            @Override
            public Adapter caseRequireEvent(RequireEvent object)
            {
                return createRequireEventAdapter();
            }
            @Override
            public Adapter caseResourceAction(ResourceAction object)
            {
                return createResourceActionAdapter();
            }
            @Override
            public Adapter caseEvent(Event object)
            {
                return createEventAdapter();
            }
            @Override
            public Adapter caseEventItem(EventItem object)
            {
                return createEventItemAdapter();
            }
            @Override
            public Adapter caseImportContainer(ImportContainer object)
            {
                return createImportContainerAdapter();
            }
            @Override
            public Adapter caseEditableDirectedGraph(EditableDirectedGraph object)
            {
                return createEditableDirectedGraphAdapter();
            }
            @Override
            public Adapter caseNode(Node object)
            {
                return createNodeAdapter();
            }
            @Override
            public Adapter caseHasResourcePeripheral(HasResourcePeripheral object)
            {
                return createHasResourcePeripheralAdapter();
            }
            @Override
            public Adapter caseIResource(IResource object)
            {
                return createIResourceAdapter();
            }
            @Override
            public Adapter caseResource(Resource object)
            {
                return createResourceAdapter();
            }
            @Override
            public Adapter caseResourceItem(ResourceItem object)
            {
                return createResourceItemAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object)
            {
                return createEObjectAdapter();
            }
        };

	/**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
	@Override
	public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject)target);
    }


	/**
     * Creates a new adapter for an object of class '{@link activity.ActivitySet <em>Set</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.ActivitySet
     * @generated
     */
	public Adapter createActivitySetAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.Activity <em>Activity</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.Activity
     * @generated
     */
	public Adapter createActivityAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.Action <em>Action</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.Action
     * @generated
     */
	public Adapter createActionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.PeripheralAction <em>Peripheral Action</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.PeripheralAction
     * @generated
     */
	public Adapter createPeripheralActionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.Claim <em>Claim</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.Claim
     * @generated
     */
	public Adapter createClaimAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.Release <em>Release</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.Release
     * @generated
     */
	public Adapter createReleaseAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.LocationPrerequisite <em>Location Prerequisite</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.LocationPrerequisite
     * @generated
     */
	public Adapter createLocationPrerequisiteAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.TracePoint <em>Trace Point</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.TracePoint
     * @generated
     */
	public Adapter createTracePointAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.SyncBar <em>Sync Bar</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.SyncBar
     * @generated
     */
	public Adapter createSyncBarAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.SimpleAction <em>Simple Action</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.SimpleAction
     * @generated
     */
	public Adapter createSimpleActionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.Move <em>Move</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.Move
     * @generated
     */
	public Adapter createMoveAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.EventAction <em>Event Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.EventAction
     * @generated
     */
    public Adapter createEventActionAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link activity.Event <em>Event</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.Event
     * @generated
     */
	public Adapter createEventAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.RaiseEvent <em>Raise Event</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.RaiseEvent
     * @generated
     */
	public Adapter createRaiseEventAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.RequireEvent <em>Require Event</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.RequireEvent
     * @generated
     */
	public Adapter createRequireEventAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link activity.ResourceAction <em>Resource Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.ResourceAction
     * @generated
     */
    public Adapter createResourceActionAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link activity.EventItem <em>Event Item</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see activity.EventItem
     * @generated
     */
    public Adapter createEventItemAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link machine.ImportContainer <em>Import Container</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.ImportContainer
     * @generated
     */
	public Adapter createImportContainerAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph <em>Editable Directed Graph</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph
     * @generated
     */
	public Adapter createEditableDirectedGraphAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link org.eclipse.lsat.common.graph.directed.editable.Node <em>Node</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see org.eclipse.lsat.common.graph.directed.editable.Node
     * @generated
     */
	public Adapter createNodeAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.HasResourcePeripheral <em>Has Resource Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.HasResourcePeripheral
     * @generated
     */
	public Adapter createHasResourcePeripheralAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.IResource <em>IResource</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.IResource
     * @generated
     */
    public Adapter createIResourceAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link machine.Resource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Resource
     * @generated
     */
    public Adapter createResourceAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link machine.ResourceItem <em>Resource Item</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.ResourceItem
     * @generated
     */
    public Adapter createResourceItemAdapter()
    {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
	public Adapter createEObjectAdapter() {
        return null;
    }

} //ActivityAdapterFactory
