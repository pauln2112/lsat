/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json.jna;

import org.eclipse.lsat.motioncalculator.json.JsonMotionCalculatorClient;

/**
 * A Motion Calculator implementation that communicates with a third party server.
 * <p>
 * If the third party server is not configured then getSupported Profiles will return an empty list all other call will
 * raise an error.
 * </p>
 */
public class NativeJsonMotionCalculator extends JsonMotionCalculatorClient {
    public NativeJsonMotionCalculator() {
        super(request -> request(request));
    }

    private static final String request(String request) {
        return new NativeJsonServer().request(request);
    }
}
