/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.PathAnnotation#getName <em>Name</em>}</li>
 *   <li>{@link machine.PathAnnotation#getPaths <em>Paths</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getPathAnnotation()
 * @model
 * @generated
 */
public interface PathAnnotation extends EObject {
	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see machine.MachinePackage#getPathAnnotation_Name()
     * @model required="true"
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link machine.PathAnnotation#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

	/**
     * Returns the value of the '<em><b>Paths</b></em>' reference list.
     * The list contents are of type {@link machine.Path}.
     * It is bidirectional and its opposite is '{@link machine.Path#getAnnotations <em>Annotations</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paths</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Paths</em>' reference list.
     * @see machine.MachinePackage#getPathAnnotation_Paths()
     * @see machine.Path#getAnnotations
     * @model opposite="annotations" resolveProxies="false" transient="true"
     * @generated
     */
	EList<Path> getPaths();

} // PathAnnotation
