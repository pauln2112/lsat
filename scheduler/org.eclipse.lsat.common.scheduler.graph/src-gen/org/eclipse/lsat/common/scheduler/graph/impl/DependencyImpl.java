/**
 */
package org.eclipse.lsat.common.scheduler.graph.impl;

import org.eclipse.lsat.common.graph.directed.impl.EdgeImpl;

import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.GraphPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DependencyImpl extends EdgeImpl implements Dependency {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected DependencyImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return GraphPackage.Literals.DEPENDENCY;
    }

} //DependencyImpl
