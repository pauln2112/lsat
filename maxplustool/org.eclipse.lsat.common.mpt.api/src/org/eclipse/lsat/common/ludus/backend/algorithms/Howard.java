/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.algorithms;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.graph.DoubleWeightedGraph;
import org.eclipse.lsat.common.ludus.backend.graph.Graph;

/**
 * Howard's minimum cycle ratio algorithm.
 *
 * @author Bram van der Sanden
 */
public final class Howard {
    private static final double SMALL_CONSTANT_EPSILON = 0.000000001;

    private static final int MAX_ITERATIONS = 1000;

    private Howard() {
        // Empty
    }

    /**
     * Run Howard's minimum cycle ratio algorithm.
     *
     * @param <V> vertex type
     * @param <E> edge type
     * @param graph input graph
     */
    public static <V, E> Tuple<Value, List<E>> runHoward(DoubleWeightedGraph<V, E, Value> graph) {
        // Use a heuristic to determine epsilon. The termination tests in Howard
        // are performed up to an epsilon constant.

        E maxE = Collections.max(graph.getEdges(), Comparator.comparing(e -> graph.getWeight1(e)));
        Value maxWeight1 = graph.getWeight1(maxE);

        E minE = Collections.min(graph.getEdges(), Comparator.comparing(e -> graph.getWeight1(e)));
        Value minWeight1 = graph.getWeight1(minE);

        Value epsilon = maxWeight1.subtract(minWeight1).multiply(new Value(SMALL_CONSTANT_EPSILON));

        return runHoward(graph, epsilon);
    }

    /**
     * Run Howard's minimum cycle ratio algorithm.
     *
     * @param <V> vertex type
     * @param <E> edge type
     * @param graph input graph that must b
     * @param eps absolute threshold for comparing cycle ratios
     */
    public static <V, E> Tuple<Value, List<E>> runHoward(DoubleWeightedGraph<V, E, Value> graph, Value eps) {
        // Upper bound on the cycle ratio.
        E maxE = Collections.max(graph.getEdges(), Comparator.comparing(e -> graph.getWeight1(e)));
        Value maxWeight1 = graph.getWeight1(maxE);
        Value r_max = (new Value(graph.getEdges().size())).multiply(maxWeight1).add(new Value(1.0));

        Map<V, Value> d = new HashMap<>();

        // Successor function p, that induces a successor graph Gp.
        Map<V, V> p = new HashMap<>();

        // Initialize the distance of each node to infinity.
        for (V v: graph.getVertices()) {
            d.put(v, Value.POSITIVE_INFINITY);
        }

        // Set the distance of the nodes, and initialize the successor function.
        for (E e: graph.getEdges()) {
            V source = graph.getEdgeSource(e);
            V target = graph.getEdgeTarget(e);
            if (graph.getWeight1(e).smallerThan(d.get(source))) {
                d.put(source, graph.getWeight1(e));
                p.put(source, target);
            }
        }

        // Result of findRatio(). This contains the ratio value of the critical
        // cycle and a handle. The cycle can be inferred using the p(v) function.
        Tuple<Value, Optional<V>> result;

        // Variables to store currently best known cycle.
        Value r = r_max;
        V r_handle = null;
        List<E> r_cycle = null;

        boolean changed = true;
        int cycleCount = 0;

        // If the algorithm needs more than 1000 cycles, we expect that we have run
        // into floating-point precision issues and will not converge to a fixed point.
        while (changed && cycleCount < MAX_ITERATIONS) {
            result = findRatio(graph, r, p);

            // Check if there exists a path from v to handle in Gp.
            if (result.getLeft().smallerThan(r)) {
                // Update the ratio value.
                r = result.getLeft();

                // Update the handle and cycle.
                r_handle = result.getRight().get();
                r_cycle = getCycle(graph, p, r_handle);

                // Perform a reverse BFS to update the node distances to
                // node handle in graph Gp.
                Set<V> visited = new HashSet<>();
                Queue<V> frontier = new LinkedList<>();
                frontier.add(r_handle);
                visited.add(r_handle);

                while (!frontier.isEmpty()) {
                    V v = frontier.remove();
                    for (E e: graph.incomingEdgesOf(v)) {
                        V incoming = graph.getEdgeSource(e);
                        // Edge (incoming,v).
                        if (!visited.contains(incoming) && p.get(incoming).equals(v)) {
                            frontier.add(incoming);
                            // Distance of v has been set.
                            Value v1 = d.get(v).add(graph.getWeight1(e));
                            Value v2 = r.multiply(graph.getWeight2(e));

                            d.put(incoming, v1.subtract(v2));
                        }
                    }
                }
            }

            // Improve the vertex distances.
            changed = false;
            for (E edge: graph.getEdges()) {
                V u = graph.getEdgeSource(edge);
                V v = graph.getEdgeTarget(edge);
                Value v1 = d.get(v).add(graph.getWeight1(edge));
                Value v2 = r.multiply(graph.getWeight2(edge));
                Value dist = v1.subtract(v2);
                if (d.get(u).biggerThan(dist.add(eps))) {
                    d.put(u, dist);
                    p.put(u, v);
                    changed = true;
                }
            }

            cycleCount++;
        }

        // If there is no cycle or we have exceed the number of cycles allowed,
        // we return a ratio value of -Infinity.
        if (r_handle == null || cycleCount >= MAX_ITERATIONS) {
            return Tuple.of(Value.NEGATIVE_INFINITY, null);
        }

        // Return both the ratio value and the cycle.
        return Tuple.of(r, r_cycle);
    }

    private static <V, E> List<E> getCycle(Graph<V, E> graph, Map<V, V> p, V r_handle) {
        List<E> cycle = new LinkedList<>();
        V v = r_handle;
        V vInit = r_handle;

        while (true) {
            V vNext = p.get(v);
            E edge = graph.getEdge(v, vNext);
            cycle.add(edge);
            if (vNext.equals(vInit)) {
                break;
            } else {
                v = vNext;
            }
        }
        return cycle;
    }

    /**
     * Find the minimum cycle ratio in the successor graph induced by function p. From each vertex, find the cycle that
     * is reached. If the ratio is lower than the currently known lowest ratio, we update the ratio and handle
     * variables. If a cycle is found with a ratio smaller than {@code r}, return a tuple of the ratio value and a
     * handle to the cycle.
     *
     * @param <V> vertex type
     * @param <E> edge type
     * @param graph input graph
     * @param r currently known lowest cycle ratio
     * @param p successor function that induces graph Gp
     * @return a tuple of the minimum cycle ratio and a handle to this cycle if the cycle ratio is smaller than
     *     {@code r}, otherwise return the same ratio and an empty handle.
     */
    private static <V, E> Tuple<Value, Optional<V>> findRatio(DoubleWeightedGraph<V, E, Value> graph, Value r,
            Map<V, V> p)
    {
        Map<V, V> visited = new HashMap<>();

        // Nodes are not visited initially.
        for (V v: graph.getVertices()) {
            visited.put(v, null);
        }

        // Currently known lowest cycle ratio.
        Value r_prime = r;
        Optional<V> handle = Optional.empty();

        for (V v: graph.getVertices()) {
            if (visited.get(v) != null) {
                // Vertex v is visited before.
                continue;
            }

            // Search for a new cycle. Mark nodes with v.
            V u = v;
            do {
                visited.put(u, v);
                u = p.get(u);
            } while (visited.get(u) == null);

            if (!visited.get(u).equals(v)) {
                // u is on an old cycle.
                continue;
            }

            // Vertex u is on a new cycle. Compute the cycle ratio.
            V x = u;
            Value sum = new Value(0.0);
            Value length = new Value(0.0);

            do {
                E edge = graph.getEdge(x, p.get(x));
                assert (edge != null);
                sum = sum.add(graph.getWeight1(edge));
                length = length.add(graph.getWeight2(edge));
                x = p.get(x);
            } while (!x.equals(u));

            Value cycleRatio = sum.divide(length);

            // If we have found a cycle with a smaller ratio,
            // return u which is on the cycle as a handle.
            if (r_prime.biggerThan(cycleRatio)) {
                r_prime = cycleRatio;
                handle = Optional.of(u);
            }
        }
        return Tuple.of(r_prime, handle);
    }
}
