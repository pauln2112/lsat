/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import org.eclipse.lsat.common.qvto.util.AbstractModelTransformer;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;

public class CleanupGraph<T extends Task>
        extends AbstractModelTransformer<TaskDependencyGraph<T>, TaskDependencyGraph<T>>
{
    public enum RemoveClaimReleaseStrategy {
        KeepAll, RemoveInner, RemoveAll
    }

    public CleanupGraph(boolean flatten, RemoveClaimReleaseStrategy strategy) {
        setConfigProperty("FLATTEN", flatten);
        setConfigProperty("REMOVE_CLAIMS_AND_RELEASES", strategy.ordinal());
    }

    @Override
    protected String getDefaultTransformation() {
        return "/transforms/cleanupGraph.qvto";
    }

    @SuppressWarnings("unchecked")
    @Override
    protected TaskDependencyGraph<T> doTransformModel(TaskDependencyGraph<T> input) throws QvtoTransformationException {
        // Inout
        BasicModelExtent inoutGraph = new BasicModelExtent();
        inoutGraph.add(input);

        execute(inoutGraph);

        return validateOneAndOnlyOne(TaskDependencyGraph.class, inoutGraph);
    }
}
