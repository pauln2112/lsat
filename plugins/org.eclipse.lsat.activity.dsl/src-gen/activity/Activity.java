/**
 */
package activity;

import machine.Resource;
import org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.Activity#getPrerequisites <em>Prerequisites</em>}</li>
 *   <li>{@link activity.Activity#getOriginalName <em>Original Name</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getActivity()
 * @model
 * @generated
 */
public interface Activity extends EditableDirectedGraph {
	/**
     * Returns the value of the '<em><b>Prerequisites</b></em>' containment reference list.
     * The list contents are of type {@link activity.LocationPrerequisite}.
     * It is bidirectional and its opposite is '{@link activity.LocationPrerequisite#getActivity <em>Activity</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prerequisites</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Prerequisites</em>' containment reference list.
     * @see activity.ActivityPackage#getActivity_Prerequisites()
     * @see activity.LocationPrerequisite#getActivity
     * @model opposite="activity" containment="true"
     * @generated
     */
	EList<LocationPrerequisite> getPrerequisites();

	/**
     * Returns the value of the '<em><b>Original Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Original Name</em>' attribute.
     * @see #setOriginalName(String)
     * @see activity.ActivityPackage#getActivity_OriginalName()
     * @model unique="false" transient="true"
     * @generated
     */
	String getOriginalName();

	/**
     * Sets the value of the '{@link activity.Activity#getOriginalName <em>Original Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Original Name</em>' attribute.
     * @see #getOriginalName()
     * @generated
     */
	void setOriginalName(String value);

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     * @generated
     */
	boolean isExpanded();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation"
     * @generated
     */
	EList<Resource> getResourcesNeedingItem();

} // Activity
