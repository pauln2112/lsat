/**
 */
package dispatching;

import activity.Activity;

import machine.ResourceItem;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dispatch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dispatching.Dispatch#getActivity <em>Activity</em>}</li>
 *   <li>{@link dispatching.Dispatch#getDescription <em>Description</em>}</li>
 *   <li>{@link dispatching.Dispatch#getResourceItems <em>Resource Items</em>}</li>
 * </ul>
 *
 * @see dispatching.DispatchingPackage#getDispatch()
 * @model
 * @generated
 */
public interface Dispatch extends HasUserAttributes {
	/**
	 * Returns the value of the '<em><b>Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity</em>' reference.
	 * @see #setActivity(Activity)
	 * @see dispatching.DispatchingPackage#getDispatch_Activity()
	 * @model required="true"
	 * @generated
	 */
	Activity getActivity();

	/**
	 * Sets the value of the '{@link dispatching.Dispatch#getActivity <em>Activity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activity</em>' reference.
	 * @see #getActivity()
	 * @generated
	 */
	void setActivity(Activity value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see dispatching.DispatchingPackage#getDispatch_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link dispatching.Dispatch#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Resource Items</b></em>' reference list.
	 * The list contents are of type {@link machine.ResourceItem}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Items</em>' reference list.
	 * @see dispatching.DispatchingPackage#getDispatch_ResourceItems()
	 * @model
	 * @generated
	 */
	EList<ResourceItem> getResourceItems();

} // Dispatch
