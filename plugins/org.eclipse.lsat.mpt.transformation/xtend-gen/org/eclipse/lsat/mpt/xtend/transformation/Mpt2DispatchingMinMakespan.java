/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.xtend.transformation;

import activity.ActivitySet;
import org.eclipse.lsat.common.mpt.api.MinimumMakespanResult;
import org.eclipse.lsat.mpt.xtend.transformation.Mpt2Dispatching;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class Mpt2DispatchingMinMakespan {
  public CharSequence transformModel(final ActivitySet inActivity, final MinimumMakespanResult makespanResult) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// Minimum makespan analysis result");
    _builder.newLine();
    _builder.append("import \"");
    String _string = inActivity.eResource().getURI().toString();
    _builder.append(_string);
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("// Activity sequence achieving the minimum makespan of ");
    String _round = Mpt2Dispatching.round(Double.valueOf(makespanResult.getMakespan()));
    _builder.append(_round);
    _builder.append(":");
    _builder.newLineIfNotEmpty();
    String _createSequence = Mpt2Dispatching.createSequence(makespanResult.getActivities(), "Steady phase");
    _builder.append(_createSequence);
    _builder.newLineIfNotEmpty();
    return _builder;
  }
}
