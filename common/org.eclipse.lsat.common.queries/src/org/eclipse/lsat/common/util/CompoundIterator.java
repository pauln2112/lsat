/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Arrays;
import java.util.Iterator;

public class CompoundIterator<E> extends ProcessingIterator<E> {
    private final Iterator<? extends Iterator<? extends E>> iterators;

    private Iterator<? extends E> current;

    public CompoundIterator(Iterator<? extends Iterator<? extends E>> iterators) {
        this.iterators = iterators;
    }

    @SafeVarargs
    public CompoundIterator(Iterator<? extends E>... iterators) {
        this.iterators = Arrays.asList(iterators).iterator();
    }

    @Override
    protected boolean toNext() {
        while ((null == current || !current.hasNext()) && iterators.hasNext()) {
            current = iterators.next();
        }
        if (null != current && current.hasNext()) {
            return setNext(current.next());
        } else {
            return done();
        }
    }
}
