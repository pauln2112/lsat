/**
 */
package org.eclipse.lsat.common.scheduler.schedule.impl;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;

import org.eclipse.lsat.common.scheduler.graph.GraphPackage;

import org.eclipse.lsat.common.scheduler.resources.ResourcesPackage;

import org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.ScheduleFactory;
import org.eclipse.lsat.common.scheduler.schedule.SchedulePackage;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.scheduler.schedule.Sequence;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SchedulePackageImpl extends EPackageImpl implements SchedulePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduledTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduledDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dependencyBoundaryEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum scheduledDependencyTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SchedulePackageImpl() {
		super(eNS_URI, ScheduleFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link SchedulePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SchedulePackage init() {
		if (isInited) return (SchedulePackage)EPackage.Registry.INSTANCE.getEPackage(SchedulePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredSchedulePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		SchedulePackageImpl theSchedulePackage = registeredSchedulePackage instanceof SchedulePackageImpl ? (SchedulePackageImpl)registeredSchedulePackage : new SchedulePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DirectedGraphPackage.eINSTANCE.eClass();
		GraphPackage.eINSTANCE.eClass();
		ResourcesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSchedulePackage.createPackageContents();

		// Initialize created meta-data
		theSchedulePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSchedulePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SchedulePackage.eNS_URI, theSchedulePackage);
		return theSchedulePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getScheduledTask() {
		return scheduledTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getScheduledTask_StartTime() {
		return (EAttribute)scheduledTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getScheduledTask_EndTime() {
		return (EAttribute)scheduledTaskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getScheduledTask_Task() {
		return (EReference)scheduledTaskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getScheduledTask_Sequence() {
		return (EReference)scheduledTaskEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getScheduledTask_Duration() {
		return (EAttribute)scheduledTaskEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getScheduledDependency() {
		return scheduledDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getScheduledDependency_Boundary() {
		return (EAttribute)scheduledDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getScheduledDependency_Dependency() {
		return (EReference)scheduledDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getScheduledDependency_Type() {
		return (EAttribute)scheduledDependencyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSchedule() {
		return scheduleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSchedule_ResourceModel() {
		return (EReference)scheduleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSchedule_Sequences() {
		return (EReference)scheduleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSchedule_EpochTime() {
		return (EAttribute)scheduleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSequence() {
		return sequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSequence_Name() {
		return (EAttribute)sequenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSequence_Schedule() {
		return (EReference)sequenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSequence_ScheduledTasks() {
		return (EReference)sequenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSequence_Resource() {
		return (EReference)sequenceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDependencyBoundary() {
		return dependencyBoundaryEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getScheduledDependencyType() {
		return scheduledDependencyTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ScheduleFactory getScheduleFactory() {
		return (ScheduleFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		scheduledTaskEClass = createEClass(SCHEDULED_TASK);
		createEAttribute(scheduledTaskEClass, SCHEDULED_TASK__START_TIME);
		createEAttribute(scheduledTaskEClass, SCHEDULED_TASK__END_TIME);
		createEReference(scheduledTaskEClass, SCHEDULED_TASK__TASK);
		createEReference(scheduledTaskEClass, SCHEDULED_TASK__SEQUENCE);
		createEAttribute(scheduledTaskEClass, SCHEDULED_TASK__DURATION);

		scheduledDependencyEClass = createEClass(SCHEDULED_DEPENDENCY);
		createEAttribute(scheduledDependencyEClass, SCHEDULED_DEPENDENCY__BOUNDARY);
		createEReference(scheduledDependencyEClass, SCHEDULED_DEPENDENCY__DEPENDENCY);
		createEAttribute(scheduledDependencyEClass, SCHEDULED_DEPENDENCY__TYPE);

		scheduleEClass = createEClass(SCHEDULE);
		createEReference(scheduleEClass, SCHEDULE__RESOURCE_MODEL);
		createEReference(scheduleEClass, SCHEDULE__SEQUENCES);
		createEAttribute(scheduleEClass, SCHEDULE__EPOCH_TIME);

		sequenceEClass = createEClass(SEQUENCE);
		createEAttribute(sequenceEClass, SEQUENCE__NAME);
		createEReference(sequenceEClass, SEQUENCE__SCHEDULE);
		createEReference(sequenceEClass, SEQUENCE__SCHEDULED_TASKS);
		createEReference(sequenceEClass, SEQUENCE__RESOURCE);

		// Create enums
		dependencyBoundaryEEnum = createEEnum(DEPENDENCY_BOUNDARY);
		scheduledDependencyTypeEEnum = createEEnum(SCHEDULED_DEPENDENCY_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GraphPackage theGraphPackage = (GraphPackage)EPackage.Registry.INSTANCE.getEPackage(GraphPackage.eNS_URI);
		DirectedGraphPackage theDirectedGraphPackage = (DirectedGraphPackage)EPackage.Registry.INSTANCE.getEPackage(DirectedGraphPackage.eNS_URI);
		ResourcesPackage theResourcesPackage = (ResourcesPackage)EPackage.Registry.INSTANCE.getEPackage(ResourcesPackage.eNS_URI);

		// Create type parameters
		ETypeParameter scheduledTaskEClass_T = addETypeParameter(scheduledTaskEClass, "T");
		ETypeParameter scheduleEClass_T = addETypeParameter(scheduleEClass, "T");
		ETypeParameter sequenceEClass_T = addETypeParameter(sequenceEClass, "T");

		// Set bounds for type parameters
		EGenericType g1 = createEGenericType(theGraphPackage.getTask());
		scheduledTaskEClass_T.getEBounds().add(g1);
		g1 = createEGenericType(theGraphPackage.getTask());
		scheduleEClass_T.getEBounds().add(g1);
		g1 = createEGenericType(theGraphPackage.getTask());
		sequenceEClass_T.getEBounds().add(g1);

		// Add supertypes to classes
		scheduledTaskEClass.getESuperTypes().add(theDirectedGraphPackage.getNode());
		scheduledDependencyEClass.getESuperTypes().add(theDirectedGraphPackage.getEdge());
		g1 = createEGenericType(theDirectedGraphPackage.getDirectedGraph());
		EGenericType g2 = createEGenericType(this.getScheduledTask());
		g1.getETypeArguments().add(g2);
		EGenericType g3 = createEGenericType(scheduleEClass_T);
		g2.getETypeArguments().add(g3);
		g2 = createEGenericType(this.getScheduledDependency());
		g1.getETypeArguments().add(g2);
		scheduleEClass.getEGenericSuperTypes().add(g1);

		// Initialize classes, features, and operations; add parameters
		initEClass(scheduledTaskEClass, ScheduledTask.class, "ScheduledTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScheduledTask_StartTime(), ecorePackage.getEBigDecimal(), "startTime", null, 1, 1, ScheduledTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduledTask_EndTime(), ecorePackage.getEBigDecimal(), "endTime", null, 1, 1, ScheduledTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(scheduledTaskEClass_T);
		initEReference(getScheduledTask_Task(), g1, null, "task", null, 0, 1, ScheduledTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getSequence());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEReference(getScheduledTask_Sequence(), g1, this.getSequence_ScheduledTasks(), "sequence", null, 1, 1, ScheduledTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduledTask_Duration(), ecorePackage.getEBigDecimal(), "duration", null, 1, 1, ScheduledTask.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(scheduledDependencyEClass, ScheduledDependency.class, "ScheduledDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScheduledDependency_Boundary(), this.getDependencyBoundary(), "boundary", null, 0, 1, ScheduledDependency.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getScheduledDependency_Dependency(), theGraphPackage.getDependency(), null, "dependency", null, 0, 1, ScheduledDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduledDependency_Type(), this.getScheduledDependencyType(), "type", null, 1, 1, ScheduledDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scheduleEClass, Schedule.class, "Schedule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSchedule_ResourceModel(), theResourcesPackage.getResourceModel(), null, "resourceModel", null, 0, 1, Schedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getSequence());
		g2 = createEGenericType(scheduleEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(getSchedule_Sequences(), g1, this.getSequence_Schedule(), "sequences", null, 0, -1, Schedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSchedule_EpochTime(), ecorePackage.getEBoolean(), "epochTime", null, 1, 1, Schedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sequenceEClass, Sequence.class, "Sequence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSequence_Name(), ecorePackage.getEString(), "name", null, 0, 1, Sequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getSchedule());
		g2 = createEGenericType(sequenceEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(getSequence_Schedule(), g1, this.getSchedule_Sequences(), "schedule", null, 1, 1, Sequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getScheduledTask());
		g2 = createEGenericType(sequenceEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(getSequence_ScheduledTasks(), g1, this.getScheduledTask_Sequence(), "scheduledTasks", null, 0, -1, Sequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSequence_Resource(), theResourcesPackage.getResource(), null, "resource", null, 0, 1, Sequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(dependencyBoundaryEEnum, DependencyBoundary.class, "DependencyBoundary");
		addEEnumLiteral(dependencyBoundaryEEnum, DependencyBoundary.IN_RESOURCE);
		addEEnumLiteral(dependencyBoundaryEEnum, DependencyBoundary.CROSS_RESOURCE);
		addEEnumLiteral(dependencyBoundaryEEnum, DependencyBoundary.CROSS_RESOURCE_CONTAINER);

		initEEnum(scheduledDependencyTypeEEnum, ScheduledDependencyType.class, "ScheduledDependencyType");
		addEEnumLiteral(scheduledDependencyTypeEEnum, ScheduledDependencyType.SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS);
		addEEnumLiteral(scheduledDependencyTypeEEnum, ScheduledDependencyType.SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS);
		addEEnumLiteral(scheduledDependencyTypeEEnum, ScheduledDependencyType.SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS);
		addEEnumLiteral(scheduledDependencyTypeEEnum, ScheduledDependencyType.SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS);

		// Create resource
		createResource(eNS_URI);
	}

} //SchedulePackageImpl
