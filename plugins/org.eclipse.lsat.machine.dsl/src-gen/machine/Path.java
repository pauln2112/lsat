/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.Path#getName <em>Name</em>}</li>
 *   <li>{@link machine.Path#getProfiles <em>Profiles</em>}</li>
 *   <li>{@link machine.Path#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link machine.Path#getAnnotations <em>Annotations</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getPath()
 * @model abstract="true"
 * @generated
 */
public interface Path extends EObject {
	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see machine.MachinePackage#getPath_Name()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	String getName();

	/**
     * Returns the value of the '<em><b>Profiles</b></em>' reference list.
     * The list contents are of type {@link machine.Profile}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profiles</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Profiles</em>' reference list.
     * @see machine.MachinePackage#getPath_Profiles()
     * @model resolveProxies="false" required="true"
     * @generated
     */
	EList<Profile> getProfiles();

	/**
     * Returns the value of the '<em><b>Peripheral</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link machine.Peripheral#getPaths <em>Paths</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Peripheral</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripheral</em>' container reference.
     * @see #setPeripheral(Peripheral)
     * @see machine.MachinePackage#getPath_Peripheral()
     * @see machine.Peripheral#getPaths
     * @model opposite="paths" required="true" transient="false"
     * @generated
     */
	Peripheral getPeripheral();

	/**
     * Sets the value of the '{@link machine.Path#getPeripheral <em>Peripheral</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Peripheral</em>' container reference.
     * @see #getPeripheral()
     * @generated
     */
	void setPeripheral(Peripheral value);

	/**
     * Returns the value of the '<em><b>Annotations</b></em>' reference list.
     * The list contents are of type {@link machine.PathAnnotation}.
     * It is bidirectional and its opposite is '{@link machine.PathAnnotation#getPaths <em>Paths</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Annotations</em>' reference list.
     * @see machine.MachinePackage#getPath_Annotations()
     * @see machine.PathAnnotation#getPaths
     * @model opposite="paths" resolveProxies="false"
     * @generated
     */
	EList<PathAnnotation> getAnnotations();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     * @generated
     */
	EList<SymbolicPosition> getSources();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     * @generated
     */
	EList<PathTargetReference> getTargets();

} // Path
