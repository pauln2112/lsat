/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.statespace;

import org.eclipse.lsat.common.ludus.backend.algebra.Vector;

/**
 * @author Bram van der Sanden
 * @param <T> location type
 */
public class Configuration<T> {
    T location;

    Vector vector;

    public Configuration(T location, Vector vector) {
        this.location = location;
        this.vector = vector;
    }

    public T getLocation() {
        return location;
    }

    public Vector getVector() {
        return vector;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Configuration)) {
            return false;
        }

        Configuration<?> that = (Configuration<?>)o;

        if (!location.equals(that.location)) {
            return false;
        }
        return vector.equals(that.vector);
    }

    @Override
    public int hashCode() {
        int result = location.hashCode();
        result = 31 * result + vector.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "<" + location.toString() + "," + vector.toString() + ">";
    }
}
