/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.simulator.main

import machine.ActionType
import machine.Machine
import machine.Peripheral

import static extension org.eclipse.lsat.common.xtend.Queries.*
import static extension org.eclipse.lsat.scheduler.simulator.common.Common.*

class GenerateMachineTemplate {
    static def generateMachineJavaScript(Machine machine) {
        var index = 1
        
        '''
        «FOR resource : machine.resources.xcollect[itemsOrResource].sortBy[fqn]»
            «FOR peripheral : resource.resource.peripherals.sortBy[name]»
            var «resource.getID(peripheral)» = {
              dot: null,
«««           Using ugly formatting, just to get the commas at the right place
              «peripheral.generateInit(index++)»«
              FOR action : peripheral.type.actions», 
              «action.generateAction»«
              ENDFOR»«
              IF !peripheral.positions.isEmpty || !peripheral.distances.isEmpty», 
              «peripheral.generateMove»«
              ENDIF»
            }
            
            «ENDFOR»
        «ENDFOR»
        // Auto generated, do not modify!
        «FOR action : machine.peripheralTypes.xcollect[actions].sortBy[name]»
            function «action.name»(peripheral, timeFrom, timeTo, time) {
              var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
              if (percentageComplete == undefined) return; // Outside time scope
              peripheral.«action.name»(percentageComplete);
            }
            
        «ENDFOR»
        
        function move(peripheral, from, to, timeFrom, timeTo, time) {
          var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
          if (percentageComplete == undefined) return; // Outside time scope
          peripheral.move(from, to, percentageComplete);
        }
        
        function toPercentageComplete(timeFrom, timeTo, time) {
          if (time < timeFrom) return;
          if (time > timeTo + 0.05) return;
          
          var deltaTime = timeTo - timeFrom;
          var timeIn = time - timeFrom;
          var ratio = timeIn/deltaTime;
          return Math.min(ratio, 1);
        }

        function plusIs(base, augment) {
          for (key in augment) {
            base[key] += augment[key];
          }
          return base;
        }

        function clone(base) {
          return JSON.parse(JSON.stringify(base));
        }
        '''
    }

    static def private generateInit(Peripheral peripheral, int index) '''
        init: function(«IF !peripheral.positions.isEmpty»position«ENDIF») {
          // TODO: Implement this
          this.dot = new Path.Circle(new Point(«index*20», 10), 8);
          this.dot.fillColor = 'SteelBlue';
«««     Terminate directly to avoid newlines
        }'''

    static def private generateAction(ActionType action) '''
        «action.name»: function(percentageComplete) {
          if (percentageComplete < 1) {
            // TODO: Working on it...
            this.dot.fillColor = 'Crimson';
          } else {
            // TODO: Done!
            this.dot.fillColor = 'SteelBlue';
          }
«««     Terminate directly to avoid newlines
        }'''

    static def private generateMove(Peripheral peripheral) '''
        move: function(from, to, percentageComplete) {
          «FOR axis : peripheral.type.axes»
            var new«axis.name» = from.«axis.name» + (to.«axis.name» - from.«axis.name») * percentageComplete;
          «ENDFOR»
          // TODO: Move it
          if (percentageComplete < 1) {
            // TODO: Working on it...
            this.dot.fillColor = 'Gold';
          } else {
            // TODO: Done!
            this.dot.fillColor = 'SteelBlue';
          }
«««     Terminate directly to avoid newlines
        }'''
}