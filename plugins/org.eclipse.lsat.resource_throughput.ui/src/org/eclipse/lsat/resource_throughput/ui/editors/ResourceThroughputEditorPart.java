/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.EventObject;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.ui.viewer.IViewerProvider;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.IPageChangedListener;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.PageChangedEvent;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.lsat.common.emf.common.ui.AbstractEMFMultiPageEditorPart;
import org.eclipse.lsat.common.emf.common.ui.TableContentOutlinePage;
import org.eclipse.lsat.common.emf.common.util.URIHelper;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.queries.QueryableIterable;
import org.eclipse.lsat.resource_throughput.ui.editors.internal.AxisTargetLocationEditingSupport;
import org.eclipse.lsat.resource_throughput.ui.editors.internal.MotionProfileEditingSupport;
import org.eclipse.lsat.resource_throughput.ui.editors.internal.SimpleActionExecutionTimeEditingSupport;
import org.eclipse.lsat.timing.util.MoveHelper;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import activity.Activity;
import activity.Move;
import activity.PeripheralAction;
import activity.SimpleAction;
import machine.Axis;
import machine.IResource;
import machine.Peripheral;
import machine.PeripheralType;
import machine.SetPoint;
import machine.impl.MachineQueries;
import machine.provider.MachineItemProviderAdapterFactory;
import setting.SettingActivator;

public class ResourceThroughputEditorPart extends AbstractEMFMultiPageEditorPart {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceThroughputEditorPart.class);

    private static final int MIN_COL_WIDTH = 60;

    private AdapterFactoryLabelProvider labelProvider;

    private final ResourceThroughputOutlineContentProvider outlineContentProvider = new ResourceThroughputOutlineContentProvider();

    public ResourceThroughputEditorPart() {
        addPageChangedListener(new IPageChangedListener() {
            @Override
            public void pageChanged(PageChangedEvent event) {
                refresh(false);
            }
        });
    }

    @Override
    protected EMFPlugin getPlugin() {
        return ResourceThroughputEditorPlugin.INSTANCE;
    }

    public void refresh(boolean reCalculate) {
        final Viewer viewer = getViewer();
        if (null != viewer) {
            viewer.getControl().setCursor(getSite().getShell().getDisplay().getSystemCursor(SWT.CURSOR_WAIT));
        }
        if (reCalculate) {
            getResourceThroughputEditingDomain().reCalculate();
            if (null != contentOutlinePage) {
                ((IViewerProvider)contentOutlinePage).getViewer().refresh();
            }
        }
        if (null != viewer) {
            viewer.refresh();
            viewer.getControl().setCursor(getSite().getShell().getDisplay().getSystemCursor(SWT.CURSOR_ARROW));
        }
    }

    public void reload() {
        if (isDirty() && !MessageDialog.openQuestion(getSite().getShell(), "Discard changes",
                "There are unsaved changes.  Do you wish to discard this editor's changes?"))
        {
            return;
        }
        handleReload();
    }

    @Override
    public boolean isSaveAsAllowed() {
        return false;
    }

    /** This sets up the editing domain for the model editor. */
    @Override
    protected EditingDomain createEditingDomain(CommandStack commandStack,
            Map<org.eclipse.emf.ecore.resource.Resource, Boolean> resourceToReadOnlyMap)
    {
        // Initialize some other things
        adapterFactory.insertAdapterFactory(new MachineItemProviderAdapterFactory());
        labelProvider = new AdapterFactoryLabelProvider(adapterFactory);
        commandStack.addCommandStackListener(new CommandStackListener() {
            @Override
            public void commandStackChanged(EventObject event) {
                // Refresh whenever an editable command is performed
                refresh(true);
            }
        });

        return new ResourceThroughputEditingDomain(adapterFactory, commandStack);
    }

    protected ResourceThroughputEditingDomain getResourceThroughputEditingDomain() {
        return (ResourceThroughputEditingDomain)editingDomain;
    }

    @Override
    protected org.eclipse.emf.ecore.resource.Resource createModel() {
        org.eclipse.emf.ecore.resource.Resource dispatchingResource = super.createModel();
        if (null == dispatchingResource || !dispatchingResource.isLoaded()) {
            // Something went wrong, thus skip loading settings
            return dispatchingResource;
        }

        // Now also load the applicable settings model
        URI settingsUri = null;
        try {
            URI dispatchingURI = dispatchingResource.getURI();
            org.eclipse.core.resources.IResource settingIResource = SettingActivator.getDefault()
                    .getSettingIResource(URIHelper.asIResource(dispatchingURI));
            settingsUri = URIHelper.asURI(settingIResource);
        } catch (Exception e) {
            // Failed to determine settings file, attach it as error to the dispatching resource
            if (!resourceToDiagnosticMap.containsKey(dispatchingResource)) {
                resourceToDiagnosticMap.put(dispatchingResource, analyzeResourceProblems(dispatchingResource, e));
            }
            return dispatchingResource;
        }

        Exception exception = null;
        org.eclipse.emf.ecore.resource.Resource settingsResource = null;
        try {
            // Load the resource through the editing domain.
            settingsResource = editingDomain.getResourceSet().getResource(settingsUri, true);
        } catch (Exception e) {
            exception = e;
            settingsResource = editingDomain.getResourceSet().getResource(settingsUri, false);
        }

        Diagnostic diagnostic = analyzeResourceProblems(settingsResource, exception);
        if (diagnostic.getSeverity() != Diagnostic.OK) {
            resourceToDiagnosticMap.put(settingsResource, diagnostic);
        }

        return dispatchingResource;
    }

    @Override
    protected IContentOutlinePage createOutlinePage() {
        // The content outline is just a table.
        return new TableContentOutlinePage() {
            @Override
            protected int getTableStyle() {
                return SWT.SINGLE | SWT.FULL_SELECTION | SWT.H_SCROLL | SWT.V_SCROLL;
            }

            @Override
            public void createControl(Composite parent) {
                super.createControl(parent);
                TableViewer viewer = getTableViewer();

                Table table = viewer.getTable();
                TableLayout layout = new TableLayout();
                table.setLayout(layout);
                table.setHeaderVisible(true);
                table.setLinesVisible(true);

                layout.addColumnData(new ColumnWeightData(2, MIN_COL_WIDTH, true));
                TableViewerColumn tvcResource = new TableViewerColumn(viewer, SWT.LEFT);
                tvcResource.setLabelProvider(
                        new OutlineColumnLabelProvider(i -> null == i.getResource() ? "Total" : i.getResource().fqn()));
                TableColumn tcResource = tvcResource.getColumn();
                tcResource.setText("Resource");

                layout.addColumnData(new ColumnWeightData(3, MIN_COL_WIDTH, true));
                TableViewerColumn tvcMakespan = new TableViewerColumn(viewer, SWT.RIGHT);
                tvcMakespan.setLabelProvider(
                        new OutlineColumnLabelProvider(i -> String.format("%,.6f", i.getTotalMakespan())));
                TableColumn tcMakespan = tvcMakespan.getColumn();
                tcMakespan.setText("Makespan [sec]");

                layout.addColumnData(new ColumnWeightData(1, MIN_COL_WIDTH, true));
                TableViewerColumn tvcIterations = new TableViewerColumn(viewer, SWT.RIGHT);
                tvcIterations.setLabelProvider(
                        new OutlineColumnLabelProvider(i -> String.format("%,d", i.getNumberOfIterations())));
                TableColumn tcIterations = tvcIterations.getColumn();
                tcIterations.setText("No. Iterations");

                layout.addColumnData(new ColumnWeightData(3, MIN_COL_WIDTH, true));
                TableViewerColumn tvcIterationMakespan = new TableViewerColumn(viewer, SWT.RIGHT);
                tvcIterationMakespan.setLabelProvider(
                        new OutlineColumnLabelProvider(i -> String.format("%,.6f", i.getIterationMakespan())));
                TableColumn tcIterationMakespan = tvcIterationMakespan.getColumn();
                tcIterationMakespan.setText("Iteration Makespan [sec]");

                layout.addColumnData(new ColumnWeightData(2, MIN_COL_WIDTH, true));
                TableViewerColumn tvcThroughput = new TableViewerColumn(viewer, SWT.RIGHT);
                tvcThroughput.setLabelProvider(
                        new OutlineColumnLabelProvider(i -> String.format("%,.1f", i.getThroughput())));
                TableColumn tcThroughput = tvcThroughput.getColumn();
                tcThroughput.setText("Throughput [per hour]");

                viewer.setContentProvider(outlineContentProvider);
                viewer.setInput(editingDomain);
            }
        };
    }

    @Override
    protected void createPages() {
        createModel();
        ResourceThroughputEditingDomain rtEditingDomain = getResourceThroughputEditingDomain();
        // Initialize the outline already to update the critical paths
        outlineContentProvider.getElements(rtEditingDomain);

        // Inspecting models
        QueryableIterable<Node> activitiesNodes = rtEditingDomain.getActivities().collect(Activity::getNodes);

        Collection<IResource> resources = activitiesNodes.objectsOfKind(PeripheralAction.class)
                .collectOne(PeripheralAction::getResource).sortedBy(IResource::fqn, String.CASE_INSENSITIVE_ORDER)
                .asOrderedSet();

        Collection<String> axisNames = activitiesNodes.objectsOfKind(Move.class).collectOne(Move::getPeripheral)
                .collectOne(Peripheral::getType).collect(PeripheralType::getAxes).collectOne(Axis::getName)
                .asOrderedSet();

        Collection<String> setPointNames = activitiesNodes.objectsOfKind(Move.class).collectOne(Move::getPeripheral)
                .collectOne(Peripheral::getType).collect(PeripheralType::getSetPoints).collectOne(SetPoint::getName)
                .asOrderedSet();

        for (IResource resource: resources) {
            String resourceFqn = resource.fqn();
            ResourceThroughputViewerPane viewerPane = new ResourceThroughputViewerPane(labelProvider.getText(resource),
                    labelProvider.getImage(resource), getSite().getPage(), ResourceThroughputEditorPart.this);
            viewerPane.createControl(getContainer());
            TableViewer tableViewer = (TableViewer)viewerPane.getViewer();
            configureTableViewer(tableViewer, axisNames, setPointNames,
                    // Get the calculated critical actions from the outline to illustrate in the table
                    o -> outlineContentProvider.getCriticalActions(resourceFqn).contains(o));

            ResourceThroughputTableContentProvider contentProvider = new ResourceThroughputTableContentProvider(
                    resourceFqn);
            tableViewer.setContentProvider(contentProvider);
            tableViewer.setInput(editingDomain);
            createContextMenuFor(tableViewer);

            int pageIndex = addPage(viewerPane);
            setPageText(pageIndex, String.format("%s", resourceFqn));
            setPageImage(pageIndex, labelProvider.getImage(resource));
            viewerPane.refreshTitle();
        }

        if (resources.isEmpty()) {
            if (resourceToDiagnosticMap.isEmpty()) {
                resourceToDiagnosticMap.put(rtEditingDomain.getResourceSet().getResources().get(0),
                        new BasicDiagnostic(Diagnostic.ERROR, getPlugin().getSymbolicName(), 0,
                                "No machine resources found in dispatching file", null));
            }
            updateProblemIndication();
        }
    }

    protected void configureTableViewer(TableViewer viewer, Collection<String> axisNames,
            Collection<String> setPointNames, CriticalPath criticalPath)
    {
        Table table = viewer.getTable();
        TableLayout layout = new TableLayout();
        table.setLayout(layout);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        layout.addColumnData(new ColumnWeightData(2, MIN_COL_WIDTH, true));
        TableViewerColumn tvcPeripheral = new TableViewerColumn(viewer, SWT.LEFT);
        tvcPeripheral.setLabelProvider(new ColumnLabelProvider<>(PeripheralAction.class,
                a -> a.getResource().fqn() + '.' + a.getPeripheral().getName()));
        TableColumn tcPeripheral = tvcPeripheral.getColumn();
        tcPeripheral.setText("Peripheral");

        layout.addColumnData(new ColumnWeightData(2, MIN_COL_WIDTH, true));
        TableViewerColumn tvcAction = new TableViewerColumn(viewer, SWT.LEFT);
        tvcAction.setLabelProvider(
                new ColumnLabelProvider<>(PeripheralAction.class, a -> a.getGraph().getName() + "." + a.getName()));
        TableColumn tcAction = tvcAction.getColumn();
        tcAction.setText("Action");

        layout.addColumnData(new ColumnWeightData(4, MIN_COL_WIDTH, true));
        TableViewerColumn tvcFrom = new TableViewerColumn(viewer, SWT.LEFT);
        tvcFrom.setLabelProvider(new ColumnLabelProvider<>(PeripheralAction.class, m -> {
            if (m instanceof Move) {
                Move move = (Move)m;
                if (move.isPositionMove()) {
                    return move.getSourcePosition().getName();
                }
                return MoveHelper.getName(move);
            }
            if (m instanceof SimpleAction) {
                return ((SimpleAction)m).getType().getName();
            }
            return m.getName();
        }));
        TableColumn tcFrom = tvcFrom.getColumn();
        tcFrom.setText("Name / From / Distance");

        layout.addColumnData(new ColumnWeightData(4, MIN_COL_WIDTH, true));
        TableViewerColumn tvcTo = new TableViewerColumn(viewer, SWT.LEFT);
        tvcTo.setLabelProvider(new ColumnLabelProvider<>(Move.class,
                m -> (m.isPositionMove()) ? m.getTargetPosition().getName() : ""));
        TableColumn tcTo = tvcTo.getColumn();
        tcTo.setText("To");

        layout.addColumnData(new ColumnWeightData(1, MIN_COL_WIDTH, true));
        TableViewerColumn tvcSettling = new TableViewerColumn(viewer, SWT.LEFT);
        tvcSettling.setLabelProvider(new ColumnLabelProvider<>(Move.class,
                m -> {
                    return MoveHelper.getSettlingAxes(m).stream().map(Axis::getName).collect(Collectors.joining(", "));
                }));
        TableColumn tcSettling = tvcSettling.getColumn();
        tcSettling.setText("Settling");

        for (String axisName: axisNames) {
            layout.addColumnData(new ColumnWeightData(2, MIN_COL_WIDTH, true));
            TableViewerColumn tvcAxisOld = new TableViewerColumn(viewer, SWT.RIGHT);
            tvcAxisOld.setLabelProvider(
                    new AxisColumnLabelProvider(axisName, (m, x) -> { return toString(x, m, true); }));
            TableColumn tcAxisOld = tvcAxisOld.getColumn();
            tcAxisOld.setText("Old " + axisName);
        }

        for (String axisName: axisNames) {
            layout.addColumnData(new ColumnWeightData(2, MIN_COL_WIDTH, true));
            TableViewerColumn tvcAxisNew = new TableViewerColumn(viewer, SWT.RIGHT);
            tvcAxisNew
                    .setLabelProvider(new AxisColumnLabelProvider(axisName, (m, x) -> { return toString(x, m, false); })
                    {
                        @Override
                        public Image getImage(Object element) {
                            Image image = super.getImage(element);
                            if (null != image || !(element instanceof Move)) {
                                return image;
                            }

                            Move m = (Move)element;
                            Axis x = MachineQueries.getAxis(m.getPeripheral(), axisName);
                            if (null == x) {
                                return image;
                            }
                            try {
                                BigDecimal sourcePos = getValue(x, m, true, BigDecimal.ZERO);
                                BigDecimal targetPos = getValue(x, m, false, BigDecimal.ZERO);
                                int difference = targetPos != null ? targetPos.compareTo(sourcePos)
                                        : sourcePos.intValue();
                                if (difference > 0) {
                                    image = ResourceThroughputEditorPlugin.getPlugin().getImageRegistry()
                                            .get(ResourceThroughputEditorPlugin.IMAGE_UP);
                                } else if (difference < 0) {
                                    image = ResourceThroughputEditorPlugin.getPlugin().getImageRegistry()
                                            .get(ResourceThroughputEditorPlugin.IMAGE_DOWN);
                                }
                            } catch (SpecificationException e) {
                                // Should not happen, already handled by super implementation (getValue will
                                // also lead to exception)
                                LOGGER.debug("Could not determine position", e);
                            }
                            return image;
                        }
                    });
            tvcAxisNew.setEditingSupport(
                    new AxisTargetLocationEditingSupport(viewer, axisName, getResourceThroughputEditingDomain()));
            TableColumn tcAxisNew = tvcAxisNew.getColumn();
            tcAxisNew.setText("New " + axisName);
            tcAxisNew.setImage(ResourceThroughputEditorPlugin.getPlugin().getImageRegistry()
                    .get(ResourceThroughputEditorPlugin.IMAGE_EDIT));
        }

        layout.addColumnData(new ColumnWeightData(2, MIN_COL_WIDTH, true));
        TableViewerColumn tvcProfile = new TableViewerColumn(viewer, SWT.LEFT);
        tvcProfile.setLabelProvider(new ColumnLabelProvider<>(Move.class, m -> m.getProfile().getName()));
        tvcProfile.setEditingSupport(new MotionProfileEditingSupport(viewer, getResourceThroughputEditingDomain()));
        TableColumn tcProfile = tvcProfile.getColumn();
        tcProfile.setText("Profile");
        tcProfile.setImage(ResourceThroughputEditorPlugin.getPlugin().getImageRegistry()
                .get(ResourceThroughputEditorPlugin.IMAGE_EDIT));

        for (String setPointName: setPointNames) {
            layout.addColumnData(new ColumnWeightData(2, MIN_COL_WIDTH, true));
            TableViewerColumn tvcAxisTime = new TableViewerColumn(viewer, SWT.RIGHT);
            tvcAxisTime.setLabelProvider(new SetPointColumnLabelProvider(setPointName, (m, s) -> formatTime(
                    getResourceThroughputEditingDomain().getTimingCalculator().calculateMotionTime(m).get(s))));
            TableColumn tcAxisTime = tvcAxisTime.getColumn();
            tcAxisTime.setText(setPointName + " time [ms]");
        }

        layout.addColumnData(new ColumnWeightData(3, MIN_COL_WIDTH, true));
        TableViewerColumn tvcTime = new TableViewerColumn(viewer, SWT.RIGHT);
        tvcTime.setLabelProvider(new ColumnLabelProvider<PeripheralAction>(PeripheralAction.class,
                a -> formatTime(getResourceThroughputEditingDomain().getTimingCalculator().calculateDuration(a)))
        {
            @Override
            public Image getImage(Object element) {
                return criticalPath.contains(element) ? ResourceThroughputEditorPlugin.getPlugin().getImageRegistry()
                        .get(ResourceThroughputEditorPlugin.IMAGE_CRITICAL) : super.getImage(element);
            }
        });
        tvcTime.setEditingSupport(
                new SimpleActionExecutionTimeEditingSupport(viewer, getResourceThroughputEditingDomain()));
        TableColumn tcTime = tvcTime.getColumn();
        tcTime.setText("Time [ms]");
        tcTime.setImage(ResourceThroughputEditorPlugin.getPlugin().getImageRegistry()
                .get(ResourceThroughputEditorPlugin.IMAGE_EDIT));
    }

    protected String formatTime(BigDecimal timeInSeconds) {
        return String.format("%,.3f", timeInSeconds.movePointRight(3));
    }

    String toString(Axis axis, Move m, boolean atStartOfMove) throws SpecificationException {
        BigDecimal value = getValue(axis, m, atStartOfMove, null);
        return value == null ? "" : String.valueOf(value);
    }

    BigDecimal getValue(Axis axis, Move m, boolean atStartOfMove, BigDecimal defaultValue)
            throws SpecificationException
    {
        MoveHelper helper = new MoveHelper(m, axis, getResourceThroughputEditingDomain().getSettings());
        return helper.getPosition(atStartOfMove, defaultValue);
    }

    protected static class OutlineColumnLabelProvider extends ColumnLabelProvider<ResourceThroughputOutlineItem> {
        public OutlineColumnLabelProvider(ExceptionalFunction<ResourceThroughputOutlineItem, String, ?> valueProvider) {
            super(ResourceThroughputOutlineItem.class, valueProvider);
        }

        @Override
        public Color getBackground(Object element) {
            Color background = super.getBackground(element);
            if (null == background && element instanceof ResourceThroughputOutlineItem
                    && null == ((ResourceThroughputOutlineItem)element).getResource())
            {
                background = Display.getDefault().getSystemColor(SWT.COLOR_INFO_BACKGROUND);
            }
            return background;
        }
    }

    protected static class AxisColumnLabelProvider extends ColumnLabelProvider<Move> {
        private final String axisName;

        private AxisColumnLabelProvider(String axisName, ExceptionalBiFunction<Move, Axis, String, ?> valueProvider) {
            super(Move.class, m -> {
                Axis x = MachineQueries.getAxis(m.getPeripheral(), axisName);
                return null == x ? null : valueProvider.apply(m, x);
            });
            this.axisName = axisName;
        }

        @Override
        public Color getBackground(Object element) {
            Color background = super.getBackground(element);
            if (null == background && MachineQueries.getAxis(((Move)element).getPeripheral(), axisName) == null) {
                // Type matches, but axis is not contained
                background = Display.getDefault().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
            }
            return background;
        }
    }

    protected static class SetPointColumnLabelProvider extends ColumnLabelProvider<Move> {
        private final String setPointName;

        private SetPointColumnLabelProvider(String setPointName,
                ExceptionalBiFunction<Move, SetPoint, String, ?> valueProvider)
        {
            super(Move.class, m -> {
                SetPoint s = MachineQueries.getSetPoint(m.getPeripheral(), setPointName);
                return null == s ? null : valueProvider.apply(m, s);
            });
            this.setPointName = setPointName;
        }

        @Override
        public Color getBackground(Object element) {
            Color background = super.getBackground(element);
            if (null == background
                    && MachineQueries.getSetPoint(((Move)element).getPeripheral(), setPointName) == null)
            {
                // Type matches, but axis is not contained
                background = Display.getDefault().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
            }
            return background;
        }
    }

    protected static class ColumnLabelProvider<T> extends org.eclipse.jface.viewers.ColumnLabelProvider {
        private final Class<T> type;

        private final ExceptionalFunction<T, String, ?> valueProvider;

        public ColumnLabelProvider(Class<T> type, ExceptionalFunction<T, String, ?> valueProvider) {
            this.type = type;
            this.valueProvider = valueProvider;
        }

        @Override
        public String getText(Object element) {
            try {
                return type.isInstance(element) ? valueProvider.apply(type.cast(element)) : null;
            } catch (Exception e) {
                final String msg = e.getLocalizedMessage();
                return msg == null ? e.toString() : msg;
            }
        }

        @Override
        public Image getImage(Object element) {
            if (type.isInstance(element)) {
                try {
                    valueProvider.apply(type.cast(element));
                } catch (Exception e) {
                    return ResourceThroughputEditorPlugin.getPlugin().getImageRegistry()
                            .get(ResourceThroughputEditorPlugin.IMAGE_ERROR);
                }
            }
            return null;
        }

        @Override
        public Color getBackground(Object element) {
            return type.isInstance(element) ? null : Display.getDefault().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
        }
    }

    @FunctionalInterface
    protected interface CriticalPath {
        boolean contains(Object action);
    }
}
