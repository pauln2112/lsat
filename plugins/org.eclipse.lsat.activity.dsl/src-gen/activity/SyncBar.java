/**
 */
package activity;

import org.eclipse.lsat.common.graph.directed.editable.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sync Bar</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activity.ActivityPackage#getSyncBar()
 * @model
 * @generated
 */
public interface SyncBar extends Node {
} // SyncBar
