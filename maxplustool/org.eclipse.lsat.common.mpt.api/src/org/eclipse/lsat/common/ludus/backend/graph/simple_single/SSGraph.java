/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.graph.simple_single;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.graph.SingleWeightedGraph;

/**
 * A graph implementation using an edge set, vertex set. Each vertex stores the incoming and outgoing edges. Each edge
 * stores its weight.
 *
 * @author Bram van der Sanden
 */
public class SSGraph implements SingleWeightedGraph<SSVertex, SSEdge, Double> {
    private static final long serialVersionUID = 1L;

    private final Set<SSVertex> vertexSet;

    private final Set<SSEdge> edgeSet;

    public SSGraph() {
        vertexSet = new HashSet<>();
        edgeSet = new HashSet<>();
    }

    @Override
    public Set<SSEdge> getEdges() {
        return edgeSet;
    }

    public void addVertex(SSVertex vertex) {
        vertexSet.add(vertex);
    }

    public SSEdge addEdge(SSVertex source, SSVertex target, Double weight) {
        SSEdge e = new SSEdge(source, target, weight);
        source.addOutgoing(e);
        target.addIncoming(e);
        edgeSet.add(e);
        return e;
    }

    @Override
    public Set<SSVertex> getVertices() {
        return vertexSet;
    }

    @Override
    public Collection<SSEdge> incomingEdgesOf(SSVertex v) {
        return v.getIncoming();
    }

    @Override
    public Collection<SSEdge> outgoingEdgesOf(SSVertex v) {
        return v.getOutgoing();
    }

    @Override
    public SSVertex getEdgeSource(SSEdge e) {
        return e.getSource();
    }

    @Override
    public SSVertex getEdgeTarget(SSEdge e) {
        return e.getTarget();
    }

    @Override
    public SSEdge getEdge(SSVertex source, SSVertex target) {
        return source.getOutgoing(target);
    }

    @Override
    public Double getWeight(SSEdge edge) {
        return edge.getWeight();
    }
}
