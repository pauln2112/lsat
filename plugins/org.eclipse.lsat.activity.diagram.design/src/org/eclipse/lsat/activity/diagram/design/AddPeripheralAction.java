/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.design;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.lsat.activity.diagram.services.ActivityServices;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.sirius.business.api.action.AbstractExternalJavaAction;
import org.eclipse.sirius.viewpoint.DSemanticDecorator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import activity.Activity;
import activity.ActivitySet;
import activity.Claim;
import activity.PeripheralAction;
import activity.Release;
import activity.ResourceAction;
import activity.util.ActivityUtil;
import machine.IResource;
import machine.Import;

public class AddPeripheralAction extends AbstractExternalJavaAction {
    @Override
    public boolean canExecute(Collection<? extends EObject> selections) {
        return null != selections && selections.size() == 1;
    }

    @Override
    public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
        final DSemanticDecorator view = getParameter(parameters, "view", DSemanticDecorator.class);
        final Activity activity = ActivityServices.getActivity(view);
        final IResource resource = ActivityServices.getResource(view);
        final EObject selection = selections.iterator().next();

        final ArrayList<EObject> roots = new ArrayList<EObject>();
        for (Import _import: ((ActivitySet)activity.eContainer()).getImports()) {
            roots.addAll(_import.load());
        }

        final EObject initialSelection;
        if (selection instanceof PeripheralAction) {
            initialSelection = ((PeripheralAction)selection).getPeripheral();
        } else if (selection instanceof ResourceAction) {
            initialSelection = ((ResourceAction)selection).getResource();
        } else {
            initialSelection = selection;
        }

        final String initialActionName = ActivityUtil.getFirstFreeName("A", activity);

        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                AddPeripheralActionWizard wizard = new AddPeripheralActionWizard(roots, resource, initialSelection,
                        initialActionName);
                WizardDialog dialog = new WizardDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
                        wizard);
                if (Dialog.OK == dialog.open()) {
                    PeripheralAction action = wizard.getPeripheralAction();

                    // For some selections we support automatic connecting
                    if (selection instanceof Claim && ((Claim)selection).getResource() == action.getResource()) {
                        addEdge(activity, (Claim)selection, action);
                    } else if (selection instanceof Release
                            && ((Release)selection).getResource() == action.getResource())
                    {
                        addEdge(activity, action, (Release)selection);
                    } else {
                        IResource resource = action.getResource();
                        if (!ActivityServices.getResources(activity).contains(resource)) {
                            addClaimRelease(activity, action);
                        }
                    }

                    // Do this at the end, the else depends on it
                    activity.getNodes().add(action);
                }
            }
        });
    }

    private static void addClaimRelease(Activity activity, PeripheralAction action) {
        addEdge(activity, ActivityUtil.addClaim(activity, action.getResource()), action);
        addEdge(activity, action, ActivityUtil.addRelease(activity, action.getResource()));
    }

    private static void addEdge(Activity activity, Node source, Node target) {
        ActivityUtil.addEdge(activity, source, target);
    }
}
