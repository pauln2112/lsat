/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.UsageCrossReferencer;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.lsat.common.emf.common.util.FastContainsEList;

/** Functionality that is missing from {@link EcoreUtil}. */
public class EcoreUtility {
    private EcoreUtility() {
        // Empty
    }

    /**
     * Get the root of an object. The container hierarchy is used to find the ancestor that doesn't have a container
     * (this may be the object itself it is {@link Resource#getContents directly contained} in a resource), then the
     * resource that contains the non-contained object, and then the resource set that contains the resource. The
     * resource set is returned (if found), or otherwise the resource is returned (if found), or otherwise the
     * non-contained ancestor is returned.
     *
     * @return The root object.
     */
    public static Notifier getRoot(EObject eObject) {
        Resource resource = eObject.eResource();
        if (resource == null) {
            return EcoreUtil.getRootContainer(eObject);
        }
        ResourceSet resourceSet = resource.getResourceSet();
        return (resourceSet != null) ? resourceSet : resource;
    }

    /**
     * Deletes the objects from the given resource and/or their {@link EObject#eContainer containing} object as well as
     * from any other feature that reference them within the given resource.
     *
     * @param eObjects The objects to delete.
     * @param resource The resource whose content tree should be considered.
     * @see EcoreUtil#delete(EObject)
     */
    public static void deleteAll(Collection<? extends EObject> eObjects) {
        // Implementation based on EcoreUtil.delete, but for multiple objects at
        // once. Unified root handling.

        // Get roots.
        Set<Object> roots = new HashSet<>();
        for (EObject eObject: eObjects) {
            roots.add(getRoot(eObject));
        }

        // Find usages.
        Map<EObject, Collection<EStructuralFeature.Setting>> usages = UsageCrossReferencer.findAll(eObjects, roots);

        // Remove all usages.
        for (Entry<EObject, Collection<EStructuralFeature.Setting>> entry: usages.entrySet()) {
            EObject eObject = entry.getKey();
            for (EStructuralFeature.Setting setting: entry.getValue()) {
                if (setting.getEStructuralFeature().isChangeable()) {
                    EcoreUtil.remove(setting, eObject);
                }
            }
        }

        // Remove all objects.
        removeAll(eObjects);
    }

    /**
     * Deletes the objects from the given resource and/or their {@link EObject#eContainer containing} object as well as
     * from any other feature that reference them within the given resource. If recursive true, contained children of
     * the object that are in the same resource are similarly removed from any features that references them.
     *
     * @param eObjects The objects to delete.
     * @param recursive Whether references to contained children should also be removed.
     * @see EcoreUtil#delete(EObject, boolean)
     */
    public static void deleteAll(Collection<? extends EObject> eObjects, boolean recursive) {
        // Implementation based on EcoreUtil.delete, but for multiple objects at
        // once. Unified root handling.

        // Reuse non-recursive implementation.
        if (!recursive) {
            deleteAll(eObjects);
            return;
        }

        // Get roots.
        Set<Notifier> roots = new HashSet<>();
        for (EObject eObject: eObjects) {
            roots.add(getRoot(eObject));
        }

        // Get objects to remove an all their descendants.
        Set<EObject> eAllObjects = new HashSet<EObject>();
        Set<EObject> crossResourceEObjects = new HashSet<EObject>();
        eAllObjects.addAll(eObjects);
        for (EObject eObject: eObjects) {
            for (TreeIterator<EObject> j = eObject.eAllContents(); j.hasNext();) {
                InternalEObject childEObject = (InternalEObject)j.next();
                if (childEObject.eDirectResource() != null) {
                    crossResourceEObjects.add(childEObject);
                } else {
                    eAllObjects.add(childEObject);
                }
            }
        }

        // Find usages.
        Map<EObject, Collection<EStructuralFeature.Setting>> usages = UsageCrossReferencer.findAll(eAllObjects, roots);

        // Remove all usages.
        for (Map.Entry<EObject, Collection<EStructuralFeature.Setting>> entry: usages.entrySet()) {
            EObject deletedEObject = entry.getKey();
            Collection<EStructuralFeature.Setting> settings = entry.getValue();
            for (EStructuralFeature.Setting setting: settings) {
                if (!eAllObjects.contains(setting.getEObject()) && setting.getEStructuralFeature().isChangeable()) {
                    EcoreUtil.remove(setting, deletedEObject);
                }
            }
        }

        // Remove all objects.
        // TODO: Removing just the original collection should be sufficient right? (better performance)
        removeAll(eObjects); // Instead of: removeAll(eAllObjects);

        // Remove all cross resource objects.
        for (EObject crossResourceEObject: crossResourceEObjects) {
            EcoreUtil.remove(crossResourceEObject.eContainer(), crossResourceEObject.eContainmentFeature(),
                    crossResourceEObject);
        }
    }

    /**
     * Removes the objects from their {@link EObject#eResource containing} resource and/or their
     * {@link EObject#eContainer containing} object.
     *
     * @param eObjects the objects to remove.
     * @see EcoreUtil#remove(EObject)
     */
    public static void removeAll(Collection<? extends EObject> eObjects) {
        // First collect all siblings based on their containing EStructuralFeature.Setting
        // NOTE: Using IdentityHashMap as its performance is much better than plain HashMap
        Map<EStructuralFeature.Setting, Collection<EObject>> buckets = new IdentityHashMap<>();
        for (EObject eObject: eObjects) {
            InternalEObject internalEObject = (InternalEObject)eObject;
            InternalEObject container = internalEObject.eInternalContainer();
            if (null != container) {
                EStructuralFeature.Setting setting = container.eSetting(eObject.eContainingFeature());
                Collection<EObject> values = buckets.get(setting);
                if (null == values) {
                    values = new FastContainsEList.FastCompare<>();
                    buckets.put(setting, values);
                }
                values.add(eObject);
            }

            // TODO: Should we optimize this one as well?
            Resource resource = internalEObject.eDirectResource();
            if (resource != null) {
                resource.getContents().remove(eObject);
            }
        }

        for (Map.Entry<EStructuralFeature.Setting, Collection<EObject>> entry: buckets.entrySet()) {
            removeAll(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Removes the values from the feature of the object.
     *
     * @param eObject the object holding the value.
     * @param eStructuralFeature the feature of the object holding the value.
     * @param value the value to remove.
     * @see EcoreUtil#remove(EObject, EStructuralFeature, Object)
     */
    public static void removeAll(EObject eObject, EStructuralFeature eStructuralFeature, Collection<?> values) {
        if (FeatureMapUtil.isMany(eObject, eStructuralFeature)) {
            ((List<?>)eObject.eGet(eStructuralFeature)).removeAll(values);
        } else {
            // Strange to call this method, but as the original implementation
            // also doesn't check the value at this time we'll also just unset
            // the feature
            eObject.eUnset(eStructuralFeature);
        }
    }

    /**
     * Removes the values from the setting.
     *
     * @param setting the setting holding the value.
     * @param values the values to remove.
     */
    public static void removeAll(EStructuralFeature.Setting setting, Collection<?> values) {
        if (FeatureMapUtil.isMany(setting.getEObject(), setting.getEStructuralFeature())) {
            ((List<?>)setting.get(false)).removeAll(values);
        } else {
            // Strange to call this method, but as the original implementation
            // also doesn't check the value at this time we'll also just unset
            // the feature
            setting.unset();
        }
    }
}
