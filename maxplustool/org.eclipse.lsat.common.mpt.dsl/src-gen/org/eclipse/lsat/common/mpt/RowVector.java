/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Row Vector</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getRowVector()
 * @model
 * @generated
 */
public interface RowVector extends Vector, NamedObject {
} // RowVector
