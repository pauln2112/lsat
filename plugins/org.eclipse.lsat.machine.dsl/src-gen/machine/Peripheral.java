/**
 */
package machine;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Peripheral</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.Peripheral#getName <em>Name</em>}</li>
 *   <li>{@link machine.Peripheral#getType <em>Type</em>}</li>
 *   <li>{@link machine.Peripheral#getAxisPositions <em>Axis Positions</em>}</li>
 *   <li>{@link machine.Peripheral#getPositions <em>Positions</em>}</li>
 *   <li>{@link machine.Peripheral#getResource <em>Resource</em>}</li>
 *   <li>{@link machine.Peripheral#getPaths <em>Paths</em>}</li>
 *   <li>{@link machine.Peripheral#getProfiles <em>Profiles</em>}</li>
 *   <li>{@link machine.Peripheral#getDistances <em>Distances</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getPeripheral()
 * @model
 * @generated
 */
public interface Peripheral extends EObject {
	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see machine.MachinePackage#getPeripheral_Name()
     * @model required="true"
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link machine.Peripheral#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

	/**
     * Returns the value of the '<em><b>Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Type</em>' reference.
     * @see #setType(PeripheralType)
     * @see machine.MachinePackage#getPeripheral_Type()
     * @model required="true"
     * @generated
     */
	PeripheralType getType();

	/**
     * Sets the value of the '{@link machine.Peripheral#getType <em>Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type</em>' reference.
     * @see #getType()
     * @generated
     */
	void setType(PeripheralType value);

	/**
     * Returns the value of the '<em><b>Axis Positions</b></em>' map.
     * The key is of type {@link machine.Axis},
     * and the value is of type list of {@link machine.Position},
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Axis Positions</em>' map.
     * @see machine.MachinePackage#getPeripheral_AxisPositions()
     * @model mapType="machine.AxisPositionsMapEntry&lt;machine.Axis, machine.Position&gt;"
     * @generated
     */
	EMap<Axis, EList<Position>> getAxisPositions();

	/**
     * Returns the value of the '<em><b>Positions</b></em>' containment reference list.
     * The list contents are of type {@link machine.SymbolicPosition}.
     * It is bidirectional and its opposite is '{@link machine.SymbolicPosition#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Positions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Positions</em>' containment reference list.
     * @see machine.MachinePackage#getPeripheral_Positions()
     * @see machine.SymbolicPosition#getPeripheral
     * @model opposite="peripheral" containment="true" keys="name"
     * @generated
     */
	EList<SymbolicPosition> getPositions();

	/**
     * Returns the value of the '<em><b>Resource</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link machine.Resource#getPeripherals <em>Peripherals</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Resource</em>' container reference.
     * @see #setResource(Resource)
     * @see machine.MachinePackage#getPeripheral_Resource()
     * @see machine.Resource#getPeripherals
     * @model opposite="peripherals" resolveProxies="false" required="true"
     * @generated
     */
	Resource getResource();

	/**
     * Sets the value of the '{@link machine.Peripheral#getResource <em>Resource</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resource</em>' container reference.
     * @see #getResource()
     * @generated
     */
	void setResource(Resource value);

	/**
     * Returns the value of the '<em><b>Paths</b></em>' containment reference list.
     * The list contents are of type {@link machine.Path}.
     * It is bidirectional and its opposite is '{@link machine.Path#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paths</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Paths</em>' containment reference list.
     * @see machine.MachinePackage#getPeripheral_Paths()
     * @see machine.Path#getPeripheral
     * @model opposite="peripheral" containment="true"
     * @generated
     */
	EList<Path> getPaths();

	/**
     * Returns the value of the '<em><b>Profiles</b></em>' containment reference list.
     * The list contents are of type {@link machine.Profile}.
     * It is bidirectional and its opposite is '{@link machine.Profile#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profiles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Profiles</em>' containment reference list.
     * @see machine.MachinePackage#getPeripheral_Profiles()
     * @see machine.Profile#getPeripheral
     * @model opposite="peripheral" containment="true" keys="name"
     * @generated
     */
	EList<Profile> getProfiles();

	/**
     * Returns the value of the '<em><b>Distances</b></em>' containment reference list.
     * The list contents are of type {@link machine.Distance}.
     * It is bidirectional and its opposite is '{@link machine.Distance#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Distances</em>' containment reference list.
     * @see machine.MachinePackage#getPeripheral_Distances()
     * @see machine.Distance#getPeripheral
     * @model opposite="peripheral" containment="true"
     * @generated
     */
	EList<Distance> getDistances();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true"
     * @generated
     */
	String fqn();

} // Peripheral
