/**
 */
package setting;

import expressions.Expression;
import java.util.Map;

import machine.Axis;
import machine.Distance;
import machine.Position;
import machine.Profile;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Motion Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link setting.MotionSettings#getLocationSettings <em>Location Settings</em>}</li>
 *   <li>{@link setting.MotionSettings#getProfileSettings <em>Profile Settings</em>}</li>
 *   <li>{@link setting.MotionSettings#getDistanceSettings <em>Distance Settings</em>}</li>
 *   <li>{@link setting.MotionSettings#getEntry <em>Entry</em>}</li>
 * </ul>
 *
 * @see setting.SettingPackage#getMotionSettings()
 * @model
 * @generated
 */
public interface MotionSettings extends EObject {
	/**
     * Returns the value of the '<em><b>Location Settings</b></em>' map.
     * The key is of type {@link machine.Position},
     * and the value is of type {@link setting.PhysicalLocation},
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location Settings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Location Settings</em>' map.
     * @see setting.SettingPackage#getMotionSettings_LocationSettings()
     * @model mapType="setting.LocationSettingsMapEntry&lt;machine.Position, setting.PhysicalLocation&gt;"
     * @generated
     */
	EMap<Position, PhysicalLocation> getLocationSettings();

	/**
     * Returns the value of the '<em><b>Profile Settings</b></em>' map.
     * The key is of type {@link machine.Profile},
     * and the value is of type {@link setting.MotionProfileSettings},
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Settings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Profile Settings</em>' map.
     * @see setting.SettingPackage#getMotionSettings_ProfileSettings()
     * @model mapType="setting.ProfileSettingsMapEntry&lt;machine.Profile, setting.MotionProfileSettings&gt;"
     * @generated
     */
	EMap<Profile, MotionProfileSettings> getProfileSettings();

	/**
     * Returns the value of the '<em><b>Distance Settings</b></em>' map.
     * The key is of type {@link machine.Distance},
     * and the value is of type {@link expressions.Expression},
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Distance Settings</em>' map.
     * @see setting.SettingPackage#getMotionSettings_DistanceSettings()
     * @model mapType="setting.DistanceSettingsMapEntry&lt;machine.Distance, expressions.Expression&gt;"
     * @generated
     */
	EMap<Distance, Expression> getDistanceSettings();

	/**
     * Returns the value of the '<em><b>Entry</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Entry</em>' container reference.
     * @see #setEntry(Map.Entry)
     * @see setting.SettingPackage#getMotionSettings_Entry()
     * @model mapType="setting.MotionSettingsMapEntry&lt;machine.Axis, setting.MotionSettings&gt;" transient="true"
     * @generated
     */
	Map.Entry<Axis, MotionSettings> getEntry();

	/**
     * Sets the value of the '{@link setting.MotionSettings#getEntry <em>Entry</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Entry</em>' container reference.
     * @see #getEntry()
     * @generated
     */
	void setEntry(Map.Entry<Axis, MotionSettings> value);

} // MotionSettings
