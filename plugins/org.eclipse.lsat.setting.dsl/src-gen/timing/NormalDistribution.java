/**
 */
package timing;

import expressions.Expression;
import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Normal Distribution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link timing.NormalDistribution#getMean <em>Mean</em>}</li>
 *   <li>{@link timing.NormalDistribution#getSd <em>Sd</em>}</li>
 *   <li>{@link timing.NormalDistribution#getMeanExp <em>Mean Exp</em>}</li>
 *   <li>{@link timing.NormalDistribution#getSdExp <em>Sd Exp</em>}</li>
 * </ul>
 *
 * @see timing.TimingPackage#getNormalDistribution()
 * @model
 * @generated
 */
public interface NormalDistribution extends Distribution {
	/**
     * Returns the value of the '<em><b>Mean</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mean</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Mean</em>' attribute.
     * @see timing.TimingPackage#getNormalDistribution_Mean()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getMean();

	/**
     * Returns the value of the '<em><b>Sd</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sd</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Sd</em>' attribute.
     * @see timing.TimingPackage#getNormalDistribution_Sd()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getSd();

	/**
     * Returns the value of the '<em><b>Mean Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Mean Exp</em>' containment reference.
     * @see #setMeanExp(Expression)
     * @see timing.TimingPackage#getNormalDistribution_MeanExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getMeanExp();

	/**
     * Sets the value of the '{@link timing.NormalDistribution#getMeanExp <em>Mean Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Mean Exp</em>' containment reference.
     * @see #getMeanExp()
     * @generated
     */
	void setMeanExp(Expression value);

	/**
     * Returns the value of the '<em><b>Sd Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Sd Exp</em>' containment reference.
     * @see #setSdExp(Expression)
     * @see timing.TimingPackage#getNormalDistribution_SdExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getSdExp();

	/**
     * Sets the value of the '{@link timing.NormalDistribution#getSdExp <em>Sd Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Sd Exp</em>' containment reference.
     * @see #getSdExp()
     * @generated
     */
	void setSdExp(Expression value);

} // NormalDistribution
