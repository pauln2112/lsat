/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor.serializer;

import activity.SyncBar;
import com.google.inject.Inject;
import org.eclipse.lsat.activity.teditor.serializer.AbstractActivitySemanticSequencer;
import org.eclipse.lsat.activity.teditor.services.ActivityGrammarAccess;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.graph.directed.editable.SourceReference;
import org.eclipse.lsat.common.graph.directed.editable.TargetReference;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;

@SuppressWarnings("all")
public class ActivitySemanticSequencer extends AbstractActivitySemanticSequencer {
  @Inject
  private ActivityGrammarAccess grammarAccess;
  
  /**
   * Make sure the right reference syntax is used for sync bars and actions
   * I.m.h.o. XText could have derived this from our syntax definition
   */
  @Override
  protected void sequence_SourceReference(final ISerializationContext context, final SourceReference semanticObject) {
    final SequenceFeeder feeder = this.createSequencerFeeder(semanticObject);
    Node _node = semanticObject.getNode();
    if ((_node instanceof SyncBar)) {
      feeder.accept(this.grammarAccess.getSourceReferenceAccess().getNodeSyncBarIIDParserRuleCall_1_1_1_0_1(), semanticObject.getNode());
    } else {
      feeder.accept(this.grammarAccess.getSourceReferenceAccess().getNodeActionIIDParserRuleCall_1_0_0_1(), semanticObject.getNode());
    }
    feeder.finish();
  }
  
  /**
   * Make sure the right reference syntax is used for sync bars and actions
   * I.m.h.o. XText could have derived this from our syntax definition
   */
  @Override
  protected void sequence_TargetReference(final ISerializationContext context, final TargetReference semanticObject) {
    final SequenceFeeder feeder = this.createSequencerFeeder(semanticObject);
    Node _node = semanticObject.getNode();
    if ((_node instanceof SyncBar)) {
      feeder.accept(this.grammarAccess.getTargetReferenceAccess().getNodeSyncBarIIDParserRuleCall_1_1_1_0_1(), semanticObject.getNode());
    } else {
      feeder.accept(this.grammarAccess.getTargetReferenceAccess().getNodeActionIIDParserRuleCall_1_0_0_1(), semanticObject.getNode());
    }
    feeder.finish();
  }
}
