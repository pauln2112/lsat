/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import activity.EventAction;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import lsat_graph.EventTask;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;
import org.eclipse.lsat.common.graph.directed.util.DirectedGraphUtil;
import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.GraphFactory;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Removes the generated event resources and inserts a direct dependency between the actions
 */
@SuppressWarnings("all")
public class RemoveEvents<T extends Task, E extends Edge> {
  private static final Logger LOGGER = LoggerFactory.getLogger(RemoveEvents.class);
  
  private final TaskDependencyGraph<T> root;
  
  private RemoveEvents(final TaskDependencyGraph<T> graph) {
    this.root = graph;
    Resource _eResource = graph.eResource();
    boolean _tripleEquals = (_eResource == null);
    if (_tripleEquals) {
      EList<EObject> _contents = RemoveEvents.createResourceSet().createResource(URI.createURI("graph")).getContents();
      _contents.add(graph);
    }
  }
  
  public static <T extends Task> TaskDependencyGraph<T> transformModel(final TaskDependencyGraph<T> graph) {
    try {
      RemoveEvents.LOGGER.debug("Starting transformation");
      final RemoveEvents<T, Edge> scr = new RemoveEvents<T, Edge>(graph);
      scr.<T, Dependency>processGraph(scr.root);
      RemoveEvents.LOGGER.debug("Finished transformation");
      return graph;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  private <T extends Task, E extends Edge> void processGraph(final DirectedGraph<T, E> graph) throws SpecificationException, MotionException {
    final Consumer<DirectedGraph<T, E>> _function = (DirectedGraph<T, E> it) -> {
      try {
        this.<T, E>processGraph(it);
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    };
    ((List<DirectedGraph<T, E>>)Conversions.doWrapArray(((DirectedGraph<T, E>[])Conversions.unwrapArray(graph.getSubGraphs(), DirectedGraph.class)).clone())).forEach(_function);
    final Consumer<T> _function_1 = (T it) -> {
      try {
        this.process(it);
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    };
    ((List<T>)Conversions.doWrapArray(((T[])Conversions.unwrapArray(graph.getNodes(), Task.class)).clone())).forEach(_function_1);
  }
  
  /**
   * search the original node in the activity an check if it is has type event.
   */
  private void _process(final Node node) {
  }
  
  private void _process(final EventTask<EventAction> task) throws SpecificationException, MotionException {
    final DirectedGraphUtil.EdgeFactory _function = () -> {
      return GraphFactory.eINSTANCE.createDependency();
    };
    DirectedGraphUtil.shortcutAndRemoveNode(task, _function);
  }
  
  public static ResourceSet createResourceSet() {
    final ResourceSetImpl resourceSet = new ResourceSetImpl();
    HashMap<URI, Resource> _hashMap = new HashMap<URI, Resource>();
    resourceSet.setURIResourceMap(_hashMap);
    return resourceSet;
  }
  
  private void process(final Node task) throws SpecificationException, MotionException {
    if (task instanceof EventTask) {
      _process((EventTask<EventAction>)task);
      return;
    } else if (task != null) {
      _process(task);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(task).toString());
    }
  }
}
