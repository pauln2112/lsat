/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.transformation;

import activity.Activity
import activity.ActivitySet
import activity.util.ActivityUtil
import com.google.common.collect.Sets
import java.util.List
import java.util.Set
import machine.ResourceItem

/**
 * Add a named Activity for all combinations of ResourceItems.
 * The original (unusable) activity is removed.
 */
final class ExpandActivities {
    private new (){}

    static def void expand(ActivitySet activitySet) {
        val expandMap = activitySet.activities.expandNames
        if(expandMap.size == activitySet.activities.size){
            //nothing to expand
            return
        }
        val eligibleActivities = activitySet.activities.filter[!resourcesNeedingItem.empty].toList
        eligibleActivities.forEach[expand]
    }
    
    private static def void expand(Activity activity) {
        activity.resourcesNeedingItem.map[items.toSet].toList
        .cartesianProduct.forEach[ActivityUtil.queryCreateExpandedActivity(activity, it)]
        
        // remove original activity from container as it should not be used anymore
        val c = activity.eContainer as ActivitySet
        c.activities -= activity;
    }
    
     /**
     * returns a map with all possible activity names and activity as values
     */
    static def expandNames(List<Activity> activities) {
        val result = newLinkedHashMap
        activities.forEach[ activity |
        result.put(activity.name, activity)
        activity.resourcesNeedingItem.map[items.toSet].
            toList.cartesianProduct.map[ActivityUtil.expandName(activity, it)].forEach[result.put(it, activity)]
        ]
        return result
    }

    private static def cartesianProduct(List<Set<ResourceItem>> r) {
       Sets.cartesianProduct(r)
    }

}
