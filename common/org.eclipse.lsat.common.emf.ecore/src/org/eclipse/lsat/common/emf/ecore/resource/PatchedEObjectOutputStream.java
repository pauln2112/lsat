/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.impl.BinaryResourceImpl.EObjectOutputStream;

public class PatchedEObjectOutputStream extends EObjectOutputStream {
    public PatchedEObjectOutputStream(OutputStream objectOutput, Map<?, ?> options, Version version)
            throws IOException
    {
        super(objectOutput, options, version);
    }

    /**
     * Workaround for bug http://eclip.se/535058
     */
    @Override
    protected EClassData writeEClass(EClass eClass) throws IOException {
        EClassData eClassData = eClassDataMap.get(eClass);
        if (eClassData == null) {
            super.writeEClass(eClass);
            eClassData = eClassDataMap.get(eClass);
            int featureCount = eClass.getFeatureCount();
            for (int i = 0; i < featureCount; ++i) {
                EStructuralFeatureData eStructuralFeatureData = eClassData.eStructuralFeatureData[i];
                EStructuralFeature.Internal eStructuralFeature = (EStructuralFeature.Internal)eClass
                        .getEStructuralFeature(i);

                /*
                 * Mark one-to-many opposite relations as transient too, if the other side is not transient. These
                 * dependencies will be restored by the when loading by the eInverseAdd operations. Note: not for
                 * one-to-one relations, undecidable which side not to persist.
                 */
                eStructuralFeatureData.isTransient |= (eStructuralFeature.getEOpposite() != null
                        && !eStructuralFeature.getEOpposite().isTransient() && !eStructuralFeature.isMany()
                        && eStructuralFeature.getEOpposite().isMany());
                eStructuralFeatureData.kind = FeatureKind.get(eStructuralFeature);
            }
        } else {
            super.writeEClass(eClass);
        }
        return eClassData;
    }
}
