/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;
import java.util.LinkedList;

public class BufferedIterator<E> extends StackedIterator<E> {
    private LinkedList<E> record = null;

    public BufferedIterator(Iterator<? extends E> source) {
        super(source);
    }

    @Override
    public E next() {
        E next = super.next();
        if (null != record) {
            record.add(next);
        }
        return next;
    }

    public void mark() {
        record = new LinkedList<>();
    }

    public void unmark() {
        record = null;
    }

    public void reset() {
        reset(false);
    }

    public void reset(boolean unmark) {
        if (null == record) {
            throw new IllegalStateException("Iterator has not been marked yet!");
        }
        push(record.iterator());
        record = unmark ? null : new LinkedList<E>();
    }

    public E current() {
        if (null == record) {
            throw new IllegalStateException("Iterator has not been marked yet!");
        }
        return record.peekLast();
    }
}
