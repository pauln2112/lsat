/**
 */
package org.eclipse.lsat.trace;

import java.math.BigDecimal;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Line</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.trace.TraceLine#getLineNumber <em>Line Number</em>}</li>
 *   <li>{@link org.eclipse.lsat.trace.TraceLine#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link org.eclipse.lsat.trace.TraceLine#getTracePoint <em>Trace Point</em>}</li>
 *   <li>{@link org.eclipse.lsat.trace.TraceLine#getModel <em>Model</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.trace.TracePackage#getTraceLine()
 * @model
 * @generated
 */
public interface TraceLine extends EObject {
	/**
	 * Returns the value of the '<em><b>Line Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Line Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Line Number</em>' attribute.
	 * @see #setLineNumber(int)
	 * @see org.eclipse.lsat.trace.TracePackage#getTraceLine_LineNumber()
	 * @model required="true"
	 * @generated
	 */
	int getLineNumber();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.trace.TraceLine#getLineNumber <em>Line Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Line Number</em>' attribute.
	 * @see #getLineNumber()
	 * @generated
	 */
	void setLineNumber(int value);

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(BigDecimal)
	 * @see org.eclipse.lsat.trace.TracePackage#getTraceLine_Timestamp()
	 * @model required="true"
	 * @generated
	 */
	BigDecimal getTimestamp();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.trace.TraceLine#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Trace Point</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trace Point</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trace Point</em>' attribute.
	 * @see #setTracePoint(String)
	 * @see org.eclipse.lsat.trace.TracePackage#getTraceLine_TracePoint()
	 * @model required="true"
	 * @generated
	 */
	String getTracePoint();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.trace.TraceLine#getTracePoint <em>Trace Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trace Point</em>' attribute.
	 * @see #getTracePoint()
	 * @generated
	 */
	void setTracePoint(String value);

	/**
	 * Returns the value of the '<em><b>Model</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.trace.TraceModel#getLines <em>Lines</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model</em>' container reference.
	 * @see #setModel(TraceModel)
	 * @see org.eclipse.lsat.trace.TracePackage#getTraceLine_Model()
	 * @see org.eclipse.lsat.trace.TraceModel#getLines
	 * @model opposite="lines" required="true" transient="false"
	 * @generated
	 */
	TraceModel getModel();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.trace.TraceLine#getModel <em>Model</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model</em>' container reference.
	 * @see #getModel()
	 * @generated
	 */
	void setModel(TraceModel value);

} // TraceLine
