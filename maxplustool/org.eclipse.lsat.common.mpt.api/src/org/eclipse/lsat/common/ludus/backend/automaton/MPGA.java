/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.automaton;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.games.ratio.solvers.policy.RatioGamePolicyIteration;

/**
 * Max-plus game automaton. We assume that the nodes for player 1 have a negative index value.
 *
 * @author Bram van der Sanden
 */
public class MPGA<T> extends MaxPlusAutomaton<T>
        implements RatioGamePolicyIteration<MPAState<T>, MPATransition<T>, Value>
{
    private static final long serialVersionUID = 1L;

    private final Set<MPAState<T>> player0states;

    private final Set<MPAState<T>> player1states;

    private final Map<MPAState<T>, Integer> idMap;

    private Integer currentId;

    public MPGA() {
        super();
        player0states = new HashSet<>();
        player1states = new HashSet<>();
        idMap = new HashMap<>();
        currentId = 0;
    }

    @Override
    public void addState(MPAState<T> MPAState) {
        super.addState(MPAState);
        if (MPAState.getIndex() < 0) {
            player1states.add(MPAState);
        } else {
            player0states.add(MPAState);
        }
        idMap.put(MPAState, currentId);
        currentId++;
    }

    @Override
    public Set<MPAState<T>> getV0() {
        return player0states;
    }

    @Override
    public Set<MPAState<T>> getV1() {
        return player1states;
    }

    @Override
    public Value getMaxAbsValue() {
        Value max = Value.NEGATIVE_INFINITY;
        for (MPATransition<T> t: getEdges()) {
            max = max.max(getWeight1(t));
            max = max.max(getWeight2(t));
        }
        return max;
    }

    @Override
    public Integer getId(MPAState<T> vertex) {
        return idMap.get(vertex);
    }
}
