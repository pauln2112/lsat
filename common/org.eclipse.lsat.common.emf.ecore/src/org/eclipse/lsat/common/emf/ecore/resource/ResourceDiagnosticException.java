/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import org.eclipse.emf.common.util.DiagnosticException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * Extending {@link DiagnosticException} to be compatible with {@link EcoreUtil#computeDiagnostic(Resource, boolean)}.
 */
public class ResourceDiagnosticException extends DiagnosticException implements Diagnostic {
    private static final long serialVersionUID = 5590745978712337960L;

    public static final int UNKNOWN_LINE = -1;

    public static final int UNKNOWN_COLUMN = -1;

    public ResourceDiagnosticException(String message) {
        this(message, null);
    }

    public ResourceDiagnosticException(String message, Throwable cause) {
        this(Severity.ERROR, message, cause);
    }

    public ResourceDiagnosticException(Severity severity, String message) {
        this(severity, message, null);
    }

    public ResourceDiagnosticException(Severity severity, String message, Throwable cause) {
        this(severity, null, UNKNOWN_LINE, UNKNOWN_COLUMN, message, cause);
    }

    public ResourceDiagnosticException(Severity severity, URI location, int column, int line, String message,
            Throwable cause)
    {
        this(severity, null, 0, location, column, line, message, cause);
    }

    public ResourceDiagnosticException(Severity severity, String source, int code, URI location, int column, int line,
            String message, Throwable cause)
    {
        this(new ResourceDiagnosticInfo(severity, source, code, message, cause, location, line, column));
    }

    protected ResourceDiagnosticException(ResourceDiagnosticInfo info) {
        super(info);
    }

    protected ResourceDiagnosticInfo getInfo() {
        return (ResourceDiagnosticInfo)super.getDiagnostic();
    }

    public Severity getSeverity() {
        return getInfo().getSeverityEnum();
    }

    @Override
    public String getMessage() {
        return getInfo().toString();
    }

    @Override
    public String getLocation() {
        return getInfo().getLocation();
    }

    public void setLocation(URI location) {
        getInfo().setLocationURI(location);
    }

    @Override
    public int getLine() {
        return getInfo().getLine();
    }

    public void setLine(int line) {
        getInfo().setLine(line);
    }

    @Override
    public int getColumn() {
        return getInfo().getColumn();
    }

    public void setColumn(int column) {
        getInfo().setColumn(column);
    }

    public void addDetail(String key, Object value) {
        getInfo().addDetail(key, value);
    }

    public void getDetail(String key, Object value) {
        getInfo().getDetail(key, value);
    }

    @Override
    public String toString() {
        return getInfo().toString();
    }

    /**
     * Specifies the level of severity for this exception. This is used to attach the
     * {@link ResourceDiagnosticException} to the {@link Resource}.
     *
     * @see Resource#getWarnings()
     * @see Resource#getErrors()
     */
    public static enum Severity {
        ERROR(org.eclipse.emf.common.util.Diagnostic.ERROR), WARNING(org.eclipse.emf.common.util.Diagnostic.WARNING);

        private final int severity;

        private Severity(int severity) {
            this.severity = severity;
        }

        public int getSeverity() {
            return severity;
        }

        public static Severity toSeverity(int severity) {
            switch (severity) {
                case org.eclipse.emf.common.util.Diagnostic.ERROR:
                    return ERROR;
                case org.eclipse.emf.common.util.Diagnostic.WARNING:
                    return WARNING;
                default:
                    throw new IllegalArgumentException("Unknown severity: " + severity);
            }
        }
    }
}
