/**
 */
package lsat_graph.impl;

import activity.RaiseEvent;
import lsat_graph.RaiseTask;
import lsat_graph.lsat_graphPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Raise Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RaiseTaskImpl extends EventTaskImpl<RaiseEvent> implements RaiseTask
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected RaiseTaskImpl()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass()
    {
        return lsat_graphPackage.Literals.RAISE_TASK;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * This is specialized for the more specific type known in this context.
     * @generated
     */
    @Override
    public void setAction(RaiseEvent newAction)
    {
        super.setAction(newAction);
    }

} //RaiseTaskImpl
