/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import "animated.activity"

activities {
    // Some dummy init ;)
    // This does enable location validation for all activities below though
    init
}
    
activities {
    offset: 4
    // First some example of a non-throughput-optimal schedule
    LRBallFromInToCond      ( "Ball 1" )
    Condition               ( "Ball 1" )
    LRBallFromCondToDrill   ( "Ball 1" )
    Drill                   ( "Ball 1" )
    UROutToOutDrill         ( "No ball" )
    URBallFromDrillToOut    ( "Ball 1" )

    LRBallFromInToCond      ( "Ball 2" )
    Condition               ( "Ball 2" )
    LRBallFromCondToDrill   ( "Ball 2" )
    Drill
    UROutToOutDrill         ( "No ball" )
    URBallFromDrillToOut    ( "Ball 2" )

    // Now a more throughput-optimal schedule
    //This section can be looped over and over...
    LRBallFromInToCond      ( "Ball 3" )
    LRFromCondToIn          ( "No ball" )
    Condition               ( "Ball 3" )
    URBallFromCondToDrill   ( "Ball 3" )
    Drill                   ( "Ball 3" )
    URBallFromDrillToOut    ( "Ball 3" )
    
    LRBallFromInToCond      ( "Ball 4" )
    Condition               ( "Ball 4" )
    LRBallFromCondToDrill   ( "Ball 4" )
    Drill                   ( "Ball 4" )
    UROutToOutDrill         ( "No ball" )
    URBallFromDrillToOut    ( "Ball 4" )
    // End of section
    
    LRBallFromInToCond      ( "Ball 5" )
    LRFromCondToIn          ( "No ball" )
    Condition               ( "Ball 5" )
    URBallFromCondToDrill   ( "Ball 5" )
    Drill                   ( "Ball 5" )
    URBallFromDrillToOut    ( "Ball 5" )
    
    LRBallFromInToCond      ( "Ball 6" )
    Condition               ( "Ball 6" )
    LRBallFromCondToDrill   ( "Ball 6" )
    Drill                   ( "Ball 6" )
    UROutToOutDrill         ( "No ball" )
    URBallFromDrillToOut    ( "Ball 6" )
    
    LRBallFromInToCond      ( "Ball 7" )
    LRFromCondToIn          ( "No ball" )
    Condition               ( "Ball 7" )
    URBallFromCondToDrill   ( "Ball 7" )
    Drill                   ( "Ball 7" )
    URBallFromDrillToOut    ( "Ball 7" )
    
    LRBallFromInToCond      ( "Ball 8" )
    Condition               ( "Ball 8" )
    LRBallFromCondToDrill   ( "Ball 8" )
    Drill                   ( "Ball 8" )
    UROutToOutDrill         ( "No ball" )
    URBallFromDrillToOut    ( "Ball 8" )
}