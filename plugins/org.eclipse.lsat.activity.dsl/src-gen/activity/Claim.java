/**
 */
package activity;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Claim</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.Claim#isPassive <em>Passive</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getClaim()
 * @model
 * @generated
 */
public interface Claim extends ResourceAction {

    /**
     * Returns the value of the '<em><b>Passive</b></em>' attribute.
     * The default value is <code>"false"</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Passive</em>' attribute.
     * @see #setPassive(boolean)
     * @see activity.ActivityPackage#getClaim_Passive()
     * @model default="false"
     * @generated
     */
    boolean isPassive();

    /**
     * Sets the value of the '{@link activity.Claim#isPassive <em>Passive</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Passive</em>' attribute.
     * @see #isPassive()
     * @generated
     */
    void setPassive(boolean value);
} // Claim
