/**
 */
package timing;

import expressions.Expression;
import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fixed Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link timing.FixedValue#getValue <em>Value</em>}</li>
 *   <li>{@link timing.FixedValue#getValueExp <em>Value Exp</em>}</li>
 * </ul>
 *
 * @see timing.TimingPackage#getFixedValue()
 * @model
 * @generated
 */
public interface FixedValue extends Timing {
	/**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see timing.TimingPackage#getFixedValue_Value()
     * @model transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getValue();

	/**
     * Returns the value of the '<em><b>Value Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Value Exp</em>' containment reference.
     * @see #setValueExp(Expression)
     * @see timing.TimingPackage#getFixedValue_ValueExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getValueExp();

	/**
     * Sets the value of the '{@link timing.FixedValue#getValueExp <em>Value Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value Exp</em>' containment reference.
     * @see #getValueExp()
     * @generated
     */
	void setValueExp(Expression value);

} // FixedValue
