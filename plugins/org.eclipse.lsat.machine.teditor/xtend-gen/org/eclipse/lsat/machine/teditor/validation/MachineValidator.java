/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.teditor.validation;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import machine.Axis;
import machine.BidirectionalPath;
import machine.FullMeshPath;
import machine.Import;
import machine.Machine;
import machine.MachinePackage;
import machine.Path;
import machine.PathTargetReference;
import machine.Peripheral;
import machine.PeripheralType;
import machine.Position;
import machine.Profile;
import machine.Resource;
import machine.ResourceItem;
import machine.SetPoint;
import machine.SymbolicPosition;
import machine.UnidirectionalPath;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.lsat.common.util.IterableUtil;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.lsat.machine.teditor.validation.AbstractMachineValidator;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

/**
 * Custom validation rules.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
@SuppressWarnings("all")
public class MachineValidator extends AbstractMachineValidator {
  public static final String SETPOINTS_NO_UNIT = "noUnitForSetPoint";
  
  public static final String UNKNOWN_CONVERSION_AXIS_TO_SETPOINT = "unknownConversionAxisToSetPoint";
  
  public static final String SAME_SOURCE_AND_TARGET_UNID_PATH_FOR_POSITION = "sameSourceAndTargetForPositionUNI";
  
  public static final String SAME_SOURCE_AND_TARGET_BID_PATH_FOR_POSITION = "sameSourceAndTargetForPositionBI";
  
  public static final String SAME_SOURCE_AND_TARGET_FUM_PATH_FOR_POSITION = "sameSourceAndTargetForPositionFU";
  
  public static final String PROFILE_NOT_USED = "profileNotUsed";
  
  public static final String CONFLICTING_POSITIONS = "conflictingPositions";
  
  public static final String INVALID_IMPORT = "invalidImport";
  
  public static final String INVALID_MUTUAL_EXCLUSIONS = "invalidMutualExclusions";
  
  @Check
  public void checkImportIsValid(final Import imp) {
    try {
      final boolean isImportUriValid = EcoreUtil2.isValidUri(imp, URI.createURI(imp.getImportURI()));
      if ((!isImportUriValid)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("The import ");
        String _importURI = imp.getImportURI();
        _builder.append(_importURI);
        _builder.append(" cannot be resolved. Make sure that the name is spelled correctly.");
        this.error(_builder.toString(), imp, MachinePackage.Literals.IMPORT__IMPORT_URI, MachineValidator.INVALID_IMPORT);
      }
      final boolean isUnderstood = imp.getImportURI().matches(".*\\.(machine)");
      if ((!isUnderstood)) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Importing ");
        String _importURI_1 = imp.getImportURI();
        _builder_1.append(_importURI_1);
        _builder_1.append(" is not allowed. Only \'machine\' files are allowed");
        this.error(_builder_1.toString(), imp, MachinePackage.Literals.IMPORT__IMPORT_URI, MachineValidator.INVALID_IMPORT);
      }
    } catch (final Throwable _t) {
      if (_t instanceof IllegalArgumentException) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("The import ");
        String _importURI_2 = imp.getImportURI();
        _builder_2.append(_importURI_2);
        _builder_2.append(" is not a valid URI.");
        this.error(_builder_2.toString(), imp, 
          MachinePackage.Literals.IMPORT__IMPORT_URI, MachineValidator.INVALID_IMPORT);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  @Check
  public void checkUnknownConversionAxisToSetPoint(final Axis axis) {
    if (((!axis.getSetPoints().isEmpty()) && (IterableUtil.<String>asSet(Queries.<SetPoint, String>xcollectOne(axis.getSetPoints(), ((Function1<SetPoint, String>) (SetPoint it) -> {
      return it.getUnit();
    }))).size() > 1))) {
      this.error("Setpoints of axes should have same unit", MachinePackage.Literals.AXIS__SET_POINTS, 
        MachineValidator.SETPOINTS_NO_UNIT);
    }
  }
  
  @Check
  public void checkEqualSetPointUnits(final PeripheralType peripheralType) {
    boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(peripheralType.getConversion());
    boolean _not = (!_isNullOrEmpty);
    if (_not) {
      return;
    }
    final Function1<Axis, Boolean> _function = (Axis axis) -> {
      final Function1<SetPoint, Boolean> _function_1 = (SetPoint setPoint) -> {
        String _unit = axis.getUnit();
        String _unit_1 = setPoint.getUnit();
        return Boolean.valueOf(Objects.equal(_unit, _unit_1));
      };
      return Boolean.valueOf(IterableExtensions.<SetPoint>forall(axis.getSetPoints(), _function_1));
    };
    final boolean conditionIsMet = IterableExtensions.<Axis>forall(peripheralType.getAxes(), _function);
    if ((!conditionIsMet)) {
      this.error("Don\'t know how to convert axes to setpoints, please specifiy a conversion.", 
        MachinePackage.Literals.PERIPHERAL_TYPE__SET_POINTS, MachineValidator.UNKNOWN_CONVERSION_AXIS_TO_SETPOINT);
    }
  }
  
  @Check
  public void checkEqualSourceTargetUnidirectional(final UnidirectionalPath undPath) {
    if ((((undPath.getSource() != null) && (undPath.getTarget() != null)) && Objects.equal(undPath.getSource().getName(), undPath.getTarget().getName()))) {
      this.warning("Source and target should not refer to same position", 
        MachinePackage.Literals.UNIDIRECTIONAL_PATH__SOURCE, MachineValidator.SAME_SOURCE_AND_TARGET_UNID_PATH_FOR_POSITION);
    }
  }
  
  @Check
  public void checkEqualEndPointsBidirectional(final BidirectionalPath biPath) {
    if (((!biPath.getEndPoints().isEmpty()) && Queries.containsDuplicates(Queries.<PathTargetReference, SymbolicPosition>xcollectOne(biPath.getEndPoints(), ((Function1<PathTargetReference, SymbolicPosition>) (PathTargetReference it) -> {
      return it.getPosition();
    }))))) {
      this.warning("Source and target should not refer to same position", 
        MachinePackage.Literals.BIDIRECTIONAL_PATH__END_POINTS, MachineValidator.SAME_SOURCE_AND_TARGET_BID_PATH_FOR_POSITION);
    }
  }
  
  @Check
  public void checkEqualEndPointsFullMesh(final FullMeshPath fullPath) {
    if (((!fullPath.getEndPoints().isEmpty()) && Queries.containsDuplicates(Queries.<PathTargetReference, SymbolicPosition>xcollectOne(fullPath.getEndPoints(), ((Function1<PathTargetReference, SymbolicPosition>) (PathTargetReference it) -> {
      return it.getPosition();
    }))))) {
      this.error("Positions in full mesh should be unique", MachinePackage.Literals.FULL_MESH_PATH__END_POINTS, 
        MachineValidator.SAME_SOURCE_AND_TARGET_FUM_PATH_FOR_POSITION);
    }
  }
  
  @Check
  public void checkResourcesUnique(final Machine machine) {
    final Function1<Resource, String> _function = (Resource it) -> {
      return it.getName();
    };
    final Function1<List<Resource>, Boolean> _function_1 = (List<Resource> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<Resource> _function_2 = (Resource it) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Resource names must be unique. Please remove all duplicate instances.");
      this.error(_builder.toString(), machine, MachinePackage.Literals.MACHINE__RESOURCES, machine.getResources().indexOf(it));
    };
    Iterables.<Resource>concat(IterableExtensions.<List<Resource>>filter(IterableExtensions.<String, Resource>groupBy(machine.getResources(), _function).values(), _function_1)).forEach(_function_2);
  }
  
  @Check
  public void checkResourceItemsUnique(final Resource resource) {
    final Function1<ResourceItem, String> _function = (ResourceItem it) -> {
      return it.getName();
    };
    final Function1<List<ResourceItem>, Boolean> _function_1 = (List<ResourceItem> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<ResourceItem> _function_2 = (ResourceItem it) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Items names must be unique. Please remove all duplicate instances.");
      this.error(_builder.toString(), resource, MachinePackage.Literals.RESOURCE__ITEMS, resource.getItems().indexOf(it));
    };
    Iterables.<ResourceItem>concat(IterableExtensions.<List<ResourceItem>>filter(IterableExtensions.<String, ResourceItem>groupBy(resource.getItems(), _function).values(), _function_1)).forEach(_function_2);
  }
  
  @Check
  public void checkPositionHasAxes(final Peripheral peripheral) {
    boolean _isEmpty = peripheral.getType().getAxes().isEmpty();
    if (_isEmpty) {
      final LinkedHashMap<String, EReference> items = CollectionLiterals.<String, EReference>newLinkedHashMap();
      boolean _isEmpty_1 = peripheral.getAxisPositions().isEmpty();
      boolean _not = (!_isEmpty_1);
      if (_not) {
        items.put("AxisPositions", MachinePackage.Literals.PERIPHERAL__AXIS_POSITIONS);
      }
      boolean _isEmpty_2 = peripheral.getPaths().isEmpty();
      boolean _not_1 = (!_isEmpty_2);
      if (_not_1) {
        items.put("Paths", MachinePackage.Literals.PERIPHERAL__PATHS);
      }
      boolean _isEmpty_3 = peripheral.getPositions().isEmpty();
      boolean _not_2 = (!_isEmpty_3);
      if (_not_2) {
        items.put("SymbolicPositions", MachinePackage.Literals.PERIPHERAL__POSITIONS);
      }
      boolean _isEmpty_4 = items.isEmpty();
      boolean _not_3 = (!_isEmpty_4);
      if (_not_3) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Positions can only be used for peripheral types with axes. Remove positions or add axes to PeripheralType ");
        String _name = peripheral.getType().getName();
        _builder.append(_name);
        final String msg = _builder.toString();
        Set<Map.Entry<String, EReference>> _entrySet = items.entrySet();
        for (final Map.Entry<String, EReference> e : _entrySet) {
          this.error(msg, peripheral, e.getValue(), (-1), MachineValidator.INVALID_MUTUAL_EXCLUSIONS);
        }
      }
    }
  }
  
  @Check
  public void checkMutualExclusiveness(final Peripheral peripheral) {
    boolean _isEmpty = peripheral.getDistances().isEmpty();
    final boolean distanceDefined = (!_isEmpty);
    if (distanceDefined) {
      final LinkedHashMap<Object, EReference> items = CollectionLiterals.<Object, EReference>newLinkedHashMap();
      boolean _isEmpty_1 = peripheral.getAxisPositions().isEmpty();
      boolean _not = (!_isEmpty_1);
      if (_not) {
        items.put("AxisPositions", MachinePackage.Literals.PERIPHERAL__AXIS_POSITIONS);
      }
      boolean _isEmpty_2 = peripheral.getPaths().isEmpty();
      boolean _not_1 = (!_isEmpty_2);
      if (_not_1) {
        items.put("Paths", MachinePackage.Literals.PERIPHERAL__PATHS);
      }
      boolean _isEmpty_3 = peripheral.getPositions().isEmpty();
      boolean _not_2 = (!_isEmpty_3);
      if (_not_2) {
        items.put("SymbolicPositions", MachinePackage.Literals.PERIPHERAL__POSITIONS);
      }
      boolean _isEmpty_4 = items.isEmpty();
      boolean _not_3 = (!_isEmpty_4);
      if (_not_3) {
        String _xifexpression = null;
        int _size = items.size();
        boolean _greaterThan = (_size > 1);
        if (_greaterThan) {
          _xifexpression = "Remove Distances or all others";
        } else {
          _xifexpression = "Remove one";
        }
        final String request = _xifexpression;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Distances cannot be mixed with ");
        String _join = IterableExtensions.join(items.keySet(), " and ");
        _builder.append(_join);
        _builder.append(". ");
        _builder.append(request);
        final String msg = _builder.toString();
        Set<Map.Entry<Object, EReference>> _entrySet = items.entrySet();
        for (final Map.Entry<Object, EReference> e : _entrySet) {
          this.error(msg, peripheral, e.getValue(), 0, MachineValidator.INVALID_MUTUAL_EXCLUSIONS);
        }
        this.error(msg, peripheral, MachinePackage.Literals.PERIPHERAL__DISTANCES, 0, MachineValidator.INVALID_MUTUAL_EXCLUSIONS);
      }
    }
  }
  
  @Check
  public void checkNotUsedProfile(final Peripheral peripheral) {
    boolean _isEmpty = peripheral.getDistances().isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      return;
    }
    final Function1<Path, EList<Profile>> _function = (Path it) -> {
      return it.getProfiles();
    };
    final Iterable<Profile> usedProfiles = Queries.<Path, Profile>xcollect(peripheral.getPaths(), _function);
    EList<Profile> _profiles = peripheral.getProfiles();
    for (final Profile profile : _profiles) {
      boolean _contains = IterableUtil.contains(usedProfiles, profile);
      boolean _not_1 = (!_contains);
      if (_not_1) {
        String _name = profile.getName();
        String _plus = ("Profile " + _name);
        String _plus_1 = (_plus + " is not used.");
        this.warning(_plus_1, MachinePackage.Literals.PERIPHERAL__PROFILES, 
          peripheral.getProfiles().indexOf(profile), MachineValidator.PROFILE_NOT_USED);
      }
    }
  }
  
  @Check
  public void checkConflictingPositions(final SymbolicPosition symbolicPosition) {
    if ((symbolicPosition != null)) {
      final Set<Axis> axesWithoutPosition = IterableExtensions.<Axis>toSet(symbolicPosition.getPeripheral().getType().getAxes());
      axesWithoutPosition.removeAll(symbolicPosition.getAxisPosition().keySet());
      final Function1<Axis, Boolean> _function = (Axis axis) -> {
        final Function1<Position, Boolean> _function_1 = (Position axisPos) -> {
          String _name = axisPos.getName();
          String _name_1 = symbolicPosition.getName();
          return Boolean.valueOf((!Objects.equal(_name, _name_1)));
        };
        return Boolean.valueOf(IterableExtensions.<Position>forall(symbolicPosition.getPeripheral().getAxisPositions().get(axis), _function_1));
      };
      final boolean conditionIsMet = IterableExtensions.<Axis>forall(axesWithoutPosition, _function);
      Peripheral peripheral = symbolicPosition.getPeripheral();
      if (((peripheral != null) && (!conditionIsMet))) {
        String _name = symbolicPosition.getName();
        String _plus = ("A position with name " + _name);
        String _plus_1 = (_plus + 
          " already exists for one of the axes, please specify these axis positions explicitly");
        this.error(_plus_1, 
          MachinePackage.Literals.SYMBOLIC_POSITION__AXIS_POSITION, MachineValidator.CONFLICTING_POSITIONS);
      }
    }
  }
}
