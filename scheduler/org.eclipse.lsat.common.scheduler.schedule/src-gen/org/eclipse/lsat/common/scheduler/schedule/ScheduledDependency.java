/**
 */
package org.eclipse.lsat.common.scheduler.schedule;

import org.eclipse.lsat.common.graph.directed.Edge;

import org.eclipse.lsat.common.scheduler.graph.Dependency;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scheduled Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getBoundary <em>Boundary</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getDependency <em>Dependency</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledDependency()
 * @model
 * @generated
 */
public interface ScheduledDependency extends Edge {
	/**
	 * Returns the value of the '<em><b>Boundary</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Returns the dependency boundary, default of inResource is invalid; can be null if model is incomplete.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Boundary</em>' attribute.
	 * @see org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledDependency_Boundary()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	DependencyBoundary getBoundary();

	/**
	 * Returns the value of the '<em><b>Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependency</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependency</em>' reference.
	 * @see #setDependency(Dependency)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledDependency_Dependency()
	 * @model
	 * @generated
	 */
	Dependency getDependency();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getDependency <em>Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dependency</em>' reference.
	 * @see #getDependency()
	 * @generated
	 */
	void setDependency(Dependency value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType
	 * @see #setType(ScheduledDependencyType)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledDependency_Type()
	 * @model required="true"
	 * @generated
	 */
	ScheduledDependencyType getType();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType
	 * @see #getType()
	 * @generated
	 */
	void setType(ScheduledDependencyType value);

} // ScheduledDependency
