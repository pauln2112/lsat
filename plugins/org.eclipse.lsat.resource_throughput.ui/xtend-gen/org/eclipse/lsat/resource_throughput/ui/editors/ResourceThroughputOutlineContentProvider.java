/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors;

import activity.Activity;
import activity.PeripheralAction;
import com.google.common.base.Objects;
import dispatching.ActivityDispatching;
import dispatching.DispatchGroup;
import dispatching.util.DispatchingUtil;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lsat_graph.PeripheralActionTask;
import machine.IResource;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.lsat.common.graph.directed.Aspect;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.queries.QueryableIterable;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.common.scheduler.algorithm.AsapScheduler;
import org.eclipse.lsat.common.scheduler.algorithm.IScheduler;
import org.eclipse.lsat.common.scheduler.algorithm.SchedulerException;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.lsat.common.xtend.annotations.IntermediateProperty;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.resource_throughput.ui.editors.FilteredTimingCalculator;
import org.eclipse.lsat.resource_throughput.ui.editors.ResourceThroughputEditingDomain;
import org.eclipse.lsat.resource_throughput.ui.editors.ResourceThroughputOutlineItem;
import org.eclipse.lsat.resource_throughput.ui.editors.ResourceThroughputTableContentProvider;
import org.eclipse.lsat.scheduler.ALAPScheduler;
import org.eclipse.lsat.scheduler.AddExecutionTimes;
import org.eclipse.lsat.scheduler.CleanupGraph;
import org.eclipse.lsat.scheduler.CriticalPathAnalysis;
import org.eclipse.lsat.scheduler.Dispatching2Graph;
import org.eclipse.lsat.scheduler.Dispatching2GraphOutput;
import org.eclipse.lsat.timing.util.ITimingCalculator;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class ResourceThroughputOutlineContentProvider implements IStructuredContentProvider {
  private static final Logger LOGGER = LoggerFactory.getLogger(ResourceThroughputOutlineContentProvider.class);
  
  @Extension
  private final IScheduler<Task> scheduler = new AsapScheduler<Task>();
  
  @IntermediateProperty(String.class)
  public final Map<String, Set<PeripheralAction>> _IntermediateProperty_criticalActions = new java.util.WeakHashMap<>();
  
  @Override
  public Object[] getElements(final Object editingDomain) {
    if ((editingDomain instanceof ResourceThroughputEditingDomain)) {
      ResourceThroughputOutlineContentProvider.LOGGER.debug("Calculating Resource Throughput Outline Content");
      final ActivityDispatching activityDispatching = ((ResourceThroughputEditingDomain)editingDomain).getActivityDispatching();
      if ((activityDispatching == null)) {
        return new Object[] {};
      }
      try {
        this._IntermediateProperty_criticalActions.clear();
        final QueryableIterable<Activity> activities = ((ResourceThroughputEditingDomain)editingDomain).getActivities();
        final Function1<Activity, EList<Node>> _function = (Activity it) -> {
          return it.getNodes();
        };
        final Function1<PeripheralAction, IResource> _function_1 = (PeripheralAction it) -> {
          return it.getResource();
        };
        final Function1<IResource, String> _function_2 = (IResource it) -> {
          return it.getName();
        };
        final List<IResource> resources = Queries.<IResource, String>sortedBy(IterableExtensions.<IResource>toSet(Queries.<PeripheralAction, IResource>collectOne(Queries.<PeripheralAction>objectsOfKind(Queries.<Activity, Node>collect(activities, _function), PeripheralAction.class), _function_1)), _function_2, String.CASE_INSENSITIVE_ORDER);
        final Dispatching2Graph<Task> d2g = new Dispatching2Graph<Task>();
        NullProgressMonitor _nullProgressMonitor = new NullProgressMonitor();
        final Dispatching2GraphOutput<Task> d2gResult = d2g.transformModel(activityDispatching, _nullProgressMonitor);
        final CleanupGraph<Task> cleanupGraph = new CleanupGraph<Task>(true, CleanupGraph.RemoveClaimReleaseStrategy.KeepAll);
        TaskDependencyGraph<Task> _taskDependencyGraph = d2gResult.getTaskDependencyGraph();
        NullProgressMonitor _nullProgressMonitor_1 = new NullProgressMonitor();
        final TaskDependencyGraph<Task> graph = cleanupGraph.transformModel(_taskDependencyGraph, _nullProgressMonitor_1);
        final Function1<IResource, ResourceThroughputOutlineItem> _function_3 = (IResource resource) -> {
          try {
            final Function1<Activity, Iterable<Node>> _function_4 = (Activity it) -> {
              return ResourceThroughputTableContentProvider.getResourceActions(it, resource.fqn());
            };
            final Set<Node> timedNodes = IterableExtensions.<Node>toSet(Queries.<Activity, Node>collect(ResourceThroughputTableContentProvider.getResourceActivities(activities, resource.fqn()), _function_4));
            ITimingCalculator _timingCalculator = ((ResourceThroughputEditingDomain)editingDomain).getTimingCalculator();
            final FilteredTimingCalculator timingCalculator = new FilteredTimingCalculator(_timingCalculator, timedNodes);
            final Integer yield = this.getYield(activityDispatching, resource);
            return this.<Task>createThroughputOutlineItem(graph, timingCalculator, resource, (yield).intValue());
          } catch (Throwable _e) {
            throw Exceptions.sneakyThrow(_e);
          }
        };
        return ((Object[])Conversions.unwrapArray(Queries.<IResource, ResourceThroughputOutlineItem>collectOne(resources, _function_3), Object.class));
      } catch (final Throwable _t) {
        if (_t instanceof Exception) {
          final Exception e = (Exception)_t;
          String _message = e.getMessage();
          String _plus = ("Failed to calculate resource throughput outline content: " + _message);
          ResourceThroughputOutlineContentProvider.LOGGER.error(_plus, e);
          return new Object[] {};
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      } finally {
        ResourceThroughputOutlineContentProvider.LOGGER.debug("DONE: Calculating Resource Throughput Outline Content");
      }
    } else {
      throw new IllegalArgumentException();
    }
  }
  
  /**
   * @TODO is the sum what we want here.
   */
  private Integer getYield(final ActivityDispatching ad, final IResource resource) {
    final Function1<DispatchGroup, Integer> _function = (DispatchGroup it) -> {
      return Integer.valueOf(DispatchingUtil.getYield(it, resource));
    };
    final Function2<Integer, Integer, Integer> _function_1 = (Integer p1, Integer p2) -> {
      return Integer.valueOf(((p1).intValue() + (p2).intValue()));
    };
    return IterableExtensions.<Integer>reduce(ListExtensions.<DispatchGroup, Integer>map(ad.getDispatchGroups(), _function), _function_1);
  }
  
  public <T extends Task> ResourceThroughputOutlineItem createThroughputOutlineItem(final TaskDependencyGraph<T> graph, final ITimingCalculator timingCalculator, final IResource resource, final int noIterations) throws MotionException, SchedulerException, QvtoTransformationException {
    try {
      final TaskDependencyGraph<T> timedGraph = new AddExecutionTimes(timingCalculator).<T>transformModel(graph);
      final Schedule<Task> schedule = ALAPScheduler.<Task>applyALAPScheduling(this.scheduler.createSchedule(timedGraph));
      ResourceThroughputOutlineItem _resourceThroughputOutlineItem = new ResourceThroughputOutlineItem();
      final Procedure1<ResourceThroughputOutlineItem> _function = (ResourceThroughputOutlineItem it) -> {
        it.setNumberOfIterations(noIterations);
        final Function1<ScheduledTask<Task>, BigDecimal> _function_1 = (ScheduledTask<Task> it_1) -> {
          return it_1.getEndTime();
        };
        it.setTotalMakespan(Queries.<BigDecimal>max(Queries.<ScheduledTask<Task>, BigDecimal>collectOne(schedule.getNodes(), _function_1), BigDecimal.ZERO));
        double _doubleValue = it.getTotalMakespan().doubleValue();
        int _numberOfIterations = it.getNumberOfIterations();
        double _divide = (_doubleValue / _numberOfIterations);
        it.setIterationMakespan(Double.valueOf(_divide));
        double _xifexpression = (double) 0;
        BigDecimal _totalMakespan = it.getTotalMakespan();
        boolean _equals = Objects.equal(_totalMakespan, BigDecimal.ZERO);
        if (_equals) {
          _xifexpression = Double.POSITIVE_INFINITY;
        } else {
          Double _iterationMakespan = it.getIterationMakespan();
          _xifexpression = (3600 / (_iterationMakespan).doubleValue());
        }
        it.setThroughput(Double.valueOf(_xifexpression));
      };
      final ResourceThroughputOutlineItem result = ObjectExtensions.<ResourceThroughputOutlineItem>operator_doubleArrow(_resourceThroughputOutlineItem, _function);
      if ((resource != null)) {
        result.setResource(resource);
        CriticalPathAnalysis<Task> _criticalPathAnalysis = new CriticalPathAnalysis<Task>();
        NullProgressMonitor _nullProgressMonitor = new NullProgressMonitor();
        final Schedule<Task> criticalSchedule = _criticalPathAnalysis.transformModel(schedule, _nullProgressMonitor);
        String _fqn = resource.fqn();
        final Function1<Aspect<ScheduledTask<Task>, ScheduledDependency>, Boolean> _function_1 = (Aspect<ScheduledTask<Task>, ScheduledDependency> it) -> {
          String _name = it.getName();
          return Boolean.valueOf(Objects.equal(_name, "Critical"));
        };
        final Function1<ScheduledTask<Task>, Task> _function_2 = (ScheduledTask<Task> it) -> {
          return it.getTask();
        };
        final Function1<PeripheralActionTask, PeripheralAction> _function_3 = (PeripheralActionTask it) -> {
          return it.getAction();
        };
        this.setCriticalActions(_fqn, IterableExtensions.<PeripheralAction>toSet(Queries.<PeripheralActionTask, PeripheralAction>collectOne(Queries.<PeripheralActionTask>objectsOfKind(Queries.<ScheduledTask<Task>, Task>collectOne(IterableExtensions.<Aspect<ScheduledTask<Task>, ScheduledDependency>>head(Queries.<Aspect<ScheduledTask<Task>, ScheduledDependency>>select(criticalSchedule.getAspects(), _function_1)).getNodes(), _function_2), PeripheralActionTask.class), _function_3)));
      }
      return result;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  private static final Set<PeripheralAction> _DEFAULT_STRING_CRITICALACTIONS = Collections.EMPTY_SET;
  
  public Set<PeripheralAction> getCriticalActions(final String owner) {
    Set<PeripheralAction> value = _IntermediateProperty_criticalActions.get(owner);
    return value == null ? _DEFAULT_STRING_CRITICALACTIONS : value;
  }
  
  public void setCriticalActions(final String owner, final Set<PeripheralAction> value) {
    if (value == _DEFAULT_STRING_CRITICALACTIONS) {
        _IntermediateProperty_criticalActions.remove(owner);
    } else {
        _IntermediateProperty_criticalActions.put(owner, value);
    }
  }
  
  private void disposeCriticalActions() {
    _IntermediateProperty_criticalActions.clear();
  }
}
