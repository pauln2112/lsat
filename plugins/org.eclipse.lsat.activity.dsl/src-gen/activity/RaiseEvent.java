/**
 */
package activity;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Raise EventAction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activity.ActivityPackage#getRaiseEvent()
 * @model
 * @generated
 */
public interface RaiseEvent extends EventAction {

} // RaiseEvent
