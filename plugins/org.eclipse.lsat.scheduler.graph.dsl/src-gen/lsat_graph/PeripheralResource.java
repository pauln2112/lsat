/**
 */
package lsat_graph;

import org.eclipse.lsat.common.scheduler.resources.Resource;

import machine.Peripheral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Peripheral Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.PeripheralResource#getPeripheral <em>Peripheral</em>}</li>
 * </ul>
 *
 * @see lsat_graph.lsat_graphPackage#getPeripheralResource()
 * @model
 * @generated
 */
public interface PeripheralResource extends Resource {
	/**
     * Returns the value of the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Peripheral</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripheral</em>' reference.
     * @see #setPeripheral(Peripheral)
     * @see lsat_graph.lsat_graphPackage#getPeripheralResource_Peripheral()
     * @model required="true"
     * @generated
     */
	Peripheral getPeripheral();

	/**
     * Sets the value of the '{@link lsat_graph.PeripheralResource#getPeripheral <em>Peripheral</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Peripheral</em>' reference.
     * @see #getPeripheral()
     * @generated
     */
	void setPeripheral(Peripheral value);

} // PeripheralResource
