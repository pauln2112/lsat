/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.teditor.ui.contentassist;

import java.util.LinkedHashSet;
import javax.inject.Inject;
import org.eclipse.lsat.machine.teditor.services.MachineGrammarAccess;
import org.eclipse.lsat.machine.teditor.ui.contentassist.AbstractMachineProposalProvider;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

/**
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#content-assist
 * on how to customize the content assistant.
 */
@SuppressWarnings("all")
public class MachineProposalProvider extends AbstractMachineProposalProvider {
  @Inject
  private MachineGrammarAccess grammarAccess;
  
  @Override
  public void completeKeyword(final Keyword keyword, final ContentAssistContext contentAssistContext, final ICompletionProposalAcceptor acceptor) {
    final LinkedHashSet<Keyword> skipKeywords = CollectionLiterals.<Keyword>newLinkedHashSet(
      this.grammarAccess.getResourceTypeAccess().getPASSIVEPassiveResourceKeyword_0_0(), 
      this.grammarAccess.getResourceTypeAccess().getEVENTEventResourceKeyword_1_0());
    boolean _contains = skipKeywords.contains(keyword);
    boolean _not = (!_contains);
    if (_not) {
      super.completeKeyword(keyword, contentAssistContext, acceptor);
    }
  }
}
