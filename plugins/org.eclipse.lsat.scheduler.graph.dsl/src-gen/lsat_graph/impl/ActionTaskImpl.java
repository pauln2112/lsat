/**
 */
package lsat_graph.impl;

import activity.ResourceAction;
import lsat_graph.ActionTask;
import lsat_graph.lsat_graphPackage;
import org.eclipse.lsat.common.scheduler.graph.impl.TaskImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.impl.ActionTaskImpl#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ActionTaskImpl<T extends ResourceAction> extends TaskImpl implements ActionTask<T> {
	/**
     * The cached value of the '{@link #getAction() <em>Action</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getAction()
     * @generated
     * @ordered
     */
	protected T action;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActionTaskImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.ACTION_TASK;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public T getAction() {
        if (action != null && action.eIsProxy())
        {
            InternalEObject oldAction = (InternalEObject)action;
            action = (T)eResolveProxy(oldAction);
            if (action != oldAction)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, lsat_graphPackage.ACTION_TASK__ACTION, oldAction, action));
            }
        }
        return action;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public T basicGetAction() {
        return action;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setAction(T newAction) {
        T oldAction = action;
        action = newAction;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, lsat_graphPackage.ACTION_TASK__ACTION, oldAction, action));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case lsat_graphPackage.ACTION_TASK__ACTION:
                if (resolve) return getAction();
                return basicGetAction();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case lsat_graphPackage.ACTION_TASK__ACTION:
                setAction((T)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.ACTION_TASK__ACTION:
                setAction((T)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.ACTION_TASK__ACTION:
                return action != null;
        }
        return super.eIsSet(featureID);
    }

} //ActionTaskImpl
