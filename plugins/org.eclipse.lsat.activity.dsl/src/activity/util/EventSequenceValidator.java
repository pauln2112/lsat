/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */

package activity.util;

import static org.eclipse.lsat.common.graph.directed.editable.EdgQueries.topologicalOrdering;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import activity.Activity;
import activity.EventAction;
import activity.RaiseEvent;
import activity.RequireEvent;

public class EventSequenceValidator {
    private EventSequenceValidator() {
        // Empty
    }

    public static interface ErrorRaiser {
        public void raise(int activityIndex, String msg);
    }

    /**
     * Checks the number if events raised and required match test rules:
     * <ul>
     * <li>#receives <= #sends at any point in the sequence</li>
     * <li>0 <= #sends - #receives <= 1, such that at most one event can be outstanding</li>
     * <li>the number of sends and receives matches over the full sequence for the event</li>
     * <li>if the first event is a RequireEvent it will be ignored</li>
     * <li>if the last event is a RaiseEvent it will be ignored</li>
     * </ul>
     */
    public static void validate(List<Activity> activitySequence, ErrorRaiser errorRaiser) {
        Map<String, List<EventAction>> eventMaps = new LinkedHashMap<>();

        // raise only one error per event name
        Set<String> alreadyRaised = new LinkedHashSet<>();

        for (int i = 0; i < activitySequence.size(); i++) {
            Activity a = activitySequence.get(i);
            Iterable<EventAction> stream = topologicalOrdering(a.getNodes()).stream().filter(EventAction.class::isInstance)
                    .map(EventAction.class::cast)::iterator;
            for (EventAction eventAction: stream) {
                int numEventPending = updateEvents(eventMaps, eventAction);
                if (numEventPending < 0 && alreadyRaised.add(eventAction.getResource().fqn())) {
                    errorRaiser.raise(i, "EventAction '" + eventAction.getResource().fqn() + "' required for activity '" + a.getName()
                            + "' but not raised before");
                } else if (numEventPending > 1 && alreadyRaised.add(eventAction.getResource().fqn())) {
                    errorRaiser.raise(i, "EventAction '" + eventAction.getResource().fqn() + "' in activity '" + a.getName()
                            + "' raised more than once before consumed");
                }
            }
        }

        for (Map.Entry<String, List<EventAction>> e: eventMaps.entrySet()) {
            if (!alreadyRaised.contains(e.getKey())) {
                List<EventAction> eventList = e.getValue();
                if (eventList != null && eventList.size() > 0) {
                    // remove last raise as it can be ignored
                    if (eventList.get(eventList.size() - 1) instanceof RaiseEvent) {
                        eventList.remove(eventList.size() - 1);
                    }

                    if (countEvents(eventList) > 0) {
                        errorRaiser.raise(-1, "The total number 'raise' and 'requires' for event '" + e.getKey()
                                + "' should be equal in a dispatching sequence");
                    }
                }
            }
        }
    }

    private static int updateEvents(Map<String, List<EventAction>> eventsMap, EventAction eventAction) {
        List<EventAction> eventActions = eventsMap.get(eventAction.getResource().fqn());
        // skip first event
        if (eventActions == null) {
            eventActions = new ArrayList<>();
            eventsMap.put(eventAction.getResource().fqn(), eventActions);
            // skip first RequirEvent
            if (eventAction instanceof RequireEvent) {
                return 0;
            }
        }
        eventActions.add(eventAction);
        return countEvents(eventActions);
    }

    private static int countEvents(List<EventAction> eventActions) {
        return eventActions.stream().mapToInt(e -> e instanceof RequireEvent ? -1 : 1).sum();
    }
}
