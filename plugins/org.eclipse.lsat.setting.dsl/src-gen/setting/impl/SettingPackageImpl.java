/**
 */
package setting.impl;

import expressions.ExpressionsPackage;

import expressions.impl.ExpressionsPackageImpl;

import java.util.Map;

import machine.MachinePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import setting.MotionProfileSettings;
import setting.MotionSettings;
import setting.PhysicalLocation;
import setting.PhysicalSettings;
import setting.SettingFactory;
import setting.SettingPackage;
import setting.Settings;

import timing.TimingPackage;

import timing.impl.TimingPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SettingPackageImpl extends EPackageImpl implements SettingPackage {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass settingsEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass profileSettingsMapEntryEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass locationSettingsMapEntryEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass timingSettingsMapEntryEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass physicalSettingsEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass motionSettingsMapEntryEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass motionSettingsEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass physicalLocationEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass motionProfileSettingsEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass motionArgumentsMapEntryEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass distanceSettingsMapEntryEClass = null;

	/**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see setting.SettingPackage#eNS_URI
     * @see #init()
     * @generated
     */
	private SettingPackageImpl() {
        super(eNS_URI, SettingFactory.eINSTANCE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static boolean isInited = false;

	/**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link SettingPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
	public static SettingPackage init() {
        if (isInited) return (SettingPackage)EPackage.Registry.INSTANCE.getEPackage(SettingPackage.eNS_URI);

        // Obtain or create and register package
        Object registeredSettingPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        SettingPackageImpl theSettingPackage = registeredSettingPackage instanceof SettingPackageImpl ? (SettingPackageImpl)registeredSettingPackage : new SettingPackageImpl();

        isInited = true;

        // Initialize simple dependencies
        MachinePackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI);
        ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl)(registeredPackage instanceof ExpressionsPackageImpl ? registeredPackage : ExpressionsPackage.eINSTANCE);
        registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimingPackage.eNS_URI);
        TimingPackageImpl theTimingPackage = (TimingPackageImpl)(registeredPackage instanceof TimingPackageImpl ? registeredPackage : TimingPackage.eINSTANCE);

        // Create package meta-data objects
        theSettingPackage.createPackageContents();
        theExpressionsPackage.createPackageContents();
        theTimingPackage.createPackageContents();

        // Initialize created meta-data
        theSettingPackage.initializePackageContents();
        theExpressionsPackage.initializePackageContents();
        theTimingPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theSettingPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(SettingPackage.eNS_URI, theSettingPackage);
        return theSettingPackage;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getSettings() {
        return settingsEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getSettings_Declarations() {
        return (EReference)settingsEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getSettings_PhysicalSettings() {
        return (EReference)settingsEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getSettings__GetTransitivePhysicalSettings() {
        return settingsEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getSettings__GetPhysicalSettings__IResource_Peripheral() {
        return settingsEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getProfileSettingsMapEntry() {
        return profileSettingsMapEntryEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getProfileSettingsMapEntry_Name() {
        return (EAttribute)profileSettingsMapEntryEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getProfileSettingsMapEntry_Key() {
        return (EReference)profileSettingsMapEntryEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getProfileSettingsMapEntry_Value() {
        return (EReference)profileSettingsMapEntryEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getLocationSettingsMapEntry() {
        return locationSettingsMapEntryEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getLocationSettingsMapEntry_Name() {
        return (EAttribute)locationSettingsMapEntryEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLocationSettingsMapEntry_Value() {
        return (EReference)locationSettingsMapEntryEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getLocationSettingsMapEntry_Key() {
        return (EReference)locationSettingsMapEntryEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getTimingSettingsMapEntry() {
        return timingSettingsMapEntryEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getTimingSettingsMapEntry_Name() {
        return (EAttribute)timingSettingsMapEntryEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getTimingSettingsMapEntry_Value() {
        return (EReference)timingSettingsMapEntryEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getTimingSettingsMapEntry_Key() {
        return (EReference)timingSettingsMapEntryEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPhysicalSettings() {
        return physicalSettingsEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPhysicalSettings_TimingSettings() {
        return (EReference)physicalSettingsEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPhysicalSettings_MotionSettings() {
        return (EReference)physicalSettingsEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPhysicalSettings_Resource() {
        return (EReference)physicalSettingsEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPhysicalSettings_Peripheral() {
        return (EReference)physicalSettingsEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPhysicalSettings_Settings() {
        return (EReference)physicalSettingsEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPhysicalSettings_Name() {
        return (EAttribute)physicalSettingsEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getMotionSettingsMapEntry() {
        return motionSettingsMapEntryEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMotionSettingsMapEntry_Name() {
        return (EAttribute)motionSettingsMapEntryEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMotionSettingsMapEntry_Key() {
        return (EReference)motionSettingsMapEntryEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMotionSettingsMapEntry_Value() {
        return (EReference)motionSettingsMapEntryEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMotionSettingsMapEntry_Settings() {
        return (EReference)motionSettingsMapEntryEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getMotionSettings() {
        return motionSettingsEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMotionSettings_LocationSettings() {
        return (EReference)motionSettingsEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMotionSettings_ProfileSettings() {
        return (EReference)motionSettingsEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMotionSettings_DistanceSettings() {
        return (EReference)motionSettingsEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMotionSettings_Entry() {
        return (EReference)motionSettingsEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPhysicalLocation() {
        return physicalLocationEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPhysicalLocation_DefaultExp() {
        return (EReference)physicalLocationEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPhysicalLocation_MinExp() {
        return (EReference)physicalLocationEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPhysicalLocation_MaxExp() {
        return (EReference)physicalLocationEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPhysicalLocation_Default() {
        return (EAttribute)physicalLocationEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPhysicalLocation_Min() {
        return (EAttribute)physicalLocationEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPhysicalLocation_Max() {
        return (EAttribute)physicalLocationEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getMotionProfileSettings() {
        return motionProfileSettingsEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMotionProfileSettings_MotionProfile() {
        return (EAttribute)motionProfileSettingsEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMotionProfileSettings_MotionArguments() {
        return (EReference)motionProfileSettingsEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getMotionArgumentsMapEntry() {
        return motionArgumentsMapEntryEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMotionArgumentsMapEntry_Key() {
        return (EAttribute)motionArgumentsMapEntryEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMotionArgumentsMapEntry_Value() {
        return (EReference)motionArgumentsMapEntryEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMotionArgumentsMapEntry_BdValue() {
        return (EAttribute)motionArgumentsMapEntryEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDistanceSettingsMapEntry() {
        return distanceSettingsMapEntryEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getDistanceSettingsMapEntry_Name() {
        return (EAttribute)distanceSettingsMapEntryEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getDistanceSettingsMapEntry_Value() {
        return (EReference)distanceSettingsMapEntryEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getDistanceSettingsMapEntry_Key() {
        return (EReference)distanceSettingsMapEntryEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getDistanceSettingsMapEntry_BdValue() {
        return (EAttribute)distanceSettingsMapEntryEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getDistanceSettingsMapEntry_MotionSettings() {
        return (EReference)distanceSettingsMapEntryEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SettingFactory getSettingFactory() {
        return (SettingFactory)getEFactoryInstance();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isCreated = false;

	/**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        settingsEClass = createEClass(SETTINGS);
        createEReference(settingsEClass, SETTINGS__DECLARATIONS);
        createEReference(settingsEClass, SETTINGS__PHYSICAL_SETTINGS);
        createEOperation(settingsEClass, SETTINGS___GET_TRANSITIVE_PHYSICAL_SETTINGS);
        createEOperation(settingsEClass, SETTINGS___GET_PHYSICAL_SETTINGS__IRESOURCE_PERIPHERAL);

        profileSettingsMapEntryEClass = createEClass(PROFILE_SETTINGS_MAP_ENTRY);
        createEAttribute(profileSettingsMapEntryEClass, PROFILE_SETTINGS_MAP_ENTRY__NAME);
        createEReference(profileSettingsMapEntryEClass, PROFILE_SETTINGS_MAP_ENTRY__KEY);
        createEReference(profileSettingsMapEntryEClass, PROFILE_SETTINGS_MAP_ENTRY__VALUE);

        locationSettingsMapEntryEClass = createEClass(LOCATION_SETTINGS_MAP_ENTRY);
        createEAttribute(locationSettingsMapEntryEClass, LOCATION_SETTINGS_MAP_ENTRY__NAME);
        createEReference(locationSettingsMapEntryEClass, LOCATION_SETTINGS_MAP_ENTRY__VALUE);
        createEReference(locationSettingsMapEntryEClass, LOCATION_SETTINGS_MAP_ENTRY__KEY);

        timingSettingsMapEntryEClass = createEClass(TIMING_SETTINGS_MAP_ENTRY);
        createEAttribute(timingSettingsMapEntryEClass, TIMING_SETTINGS_MAP_ENTRY__NAME);
        createEReference(timingSettingsMapEntryEClass, TIMING_SETTINGS_MAP_ENTRY__VALUE);
        createEReference(timingSettingsMapEntryEClass, TIMING_SETTINGS_MAP_ENTRY__KEY);

        physicalSettingsEClass = createEClass(PHYSICAL_SETTINGS);
        createEReference(physicalSettingsEClass, PHYSICAL_SETTINGS__TIMING_SETTINGS);
        createEReference(physicalSettingsEClass, PHYSICAL_SETTINGS__MOTION_SETTINGS);
        createEReference(physicalSettingsEClass, PHYSICAL_SETTINGS__RESOURCE);
        createEReference(physicalSettingsEClass, PHYSICAL_SETTINGS__PERIPHERAL);
        createEReference(physicalSettingsEClass, PHYSICAL_SETTINGS__SETTINGS);
        createEAttribute(physicalSettingsEClass, PHYSICAL_SETTINGS__NAME);

        motionSettingsMapEntryEClass = createEClass(MOTION_SETTINGS_MAP_ENTRY);
        createEAttribute(motionSettingsMapEntryEClass, MOTION_SETTINGS_MAP_ENTRY__NAME);
        createEReference(motionSettingsMapEntryEClass, MOTION_SETTINGS_MAP_ENTRY__KEY);
        createEReference(motionSettingsMapEntryEClass, MOTION_SETTINGS_MAP_ENTRY__VALUE);
        createEReference(motionSettingsMapEntryEClass, MOTION_SETTINGS_MAP_ENTRY__SETTINGS);

        motionSettingsEClass = createEClass(MOTION_SETTINGS);
        createEReference(motionSettingsEClass, MOTION_SETTINGS__LOCATION_SETTINGS);
        createEReference(motionSettingsEClass, MOTION_SETTINGS__PROFILE_SETTINGS);
        createEReference(motionSettingsEClass, MOTION_SETTINGS__DISTANCE_SETTINGS);
        createEReference(motionSettingsEClass, MOTION_SETTINGS__ENTRY);

        physicalLocationEClass = createEClass(PHYSICAL_LOCATION);
        createEReference(physicalLocationEClass, PHYSICAL_LOCATION__DEFAULT_EXP);
        createEReference(physicalLocationEClass, PHYSICAL_LOCATION__MIN_EXP);
        createEReference(physicalLocationEClass, PHYSICAL_LOCATION__MAX_EXP);
        createEAttribute(physicalLocationEClass, PHYSICAL_LOCATION__DEFAULT);
        createEAttribute(physicalLocationEClass, PHYSICAL_LOCATION__MIN);
        createEAttribute(physicalLocationEClass, PHYSICAL_LOCATION__MAX);

        motionProfileSettingsEClass = createEClass(MOTION_PROFILE_SETTINGS);
        createEAttribute(motionProfileSettingsEClass, MOTION_PROFILE_SETTINGS__MOTION_PROFILE);
        createEReference(motionProfileSettingsEClass, MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS);

        motionArgumentsMapEntryEClass = createEClass(MOTION_ARGUMENTS_MAP_ENTRY);
        createEAttribute(motionArgumentsMapEntryEClass, MOTION_ARGUMENTS_MAP_ENTRY__KEY);
        createEReference(motionArgumentsMapEntryEClass, MOTION_ARGUMENTS_MAP_ENTRY__VALUE);
        createEAttribute(motionArgumentsMapEntryEClass, MOTION_ARGUMENTS_MAP_ENTRY__BD_VALUE);

        distanceSettingsMapEntryEClass = createEClass(DISTANCE_SETTINGS_MAP_ENTRY);
        createEAttribute(distanceSettingsMapEntryEClass, DISTANCE_SETTINGS_MAP_ENTRY__NAME);
        createEReference(distanceSettingsMapEntryEClass, DISTANCE_SETTINGS_MAP_ENTRY__VALUE);
        createEReference(distanceSettingsMapEntryEClass, DISTANCE_SETTINGS_MAP_ENTRY__KEY);
        createEAttribute(distanceSettingsMapEntryEClass, DISTANCE_SETTINGS_MAP_ENTRY__BD_VALUE);
        createEReference(distanceSettingsMapEntryEClass, DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isInitialized = false;

	/**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        MachinePackage theMachinePackage = (MachinePackage)EPackage.Registry.INSTANCE.getEPackage(MachinePackage.eNS_URI);
        ExpressionsPackage theExpressionsPackage = (ExpressionsPackage)EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI);
        TimingPackage theTimingPackage = (TimingPackage)EPackage.Registry.INSTANCE.getEPackage(TimingPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        settingsEClass.getESuperTypes().add(theMachinePackage.getImportContainer());
        physicalSettingsEClass.getESuperTypes().add(theMachinePackage.getHasResourcePeripheral());

        // Initialize classes, features, and operations; add parameters
        initEClass(settingsEClass, Settings.class, "Settings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSettings_Declarations(), theExpressionsPackage.getDeclaration(), null, "declarations", null, 0, -1, Settings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getSettings_PhysicalSettings(), this.getPhysicalSettings(), null, "physicalSettings", null, 0, -1, Settings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getSettings__GetTransitivePhysicalSettings(), this.getPhysicalSettings(), "getTransitivePhysicalSettings", 0, -1, IS_UNIQUE, IS_ORDERED);

        EOperation op = initEOperation(getSettings__GetPhysicalSettings__IResource_Peripheral(), this.getPhysicalSettings(), "getPhysicalSettings", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, theMachinePackage.getIResource(), "resource", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, theMachinePackage.getPeripheral(), "peripheral", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(profileSettingsMapEntryEClass, Map.Entry.class, "ProfileSettingsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getProfileSettingsMapEntry_Name(), ecorePackage.getEString(), "name", null, 0, 1, Map.Entry.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getProfileSettingsMapEntry_Key(), theMachinePackage.getProfile(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getProfileSettingsMapEntry_Value(), this.getMotionProfileSettings(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(locationSettingsMapEntryEClass, Map.Entry.class, "LocationSettingsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLocationSettingsMapEntry_Name(), ecorePackage.getEString(), "name", null, 0, 1, Map.Entry.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getLocationSettingsMapEntry_Value(), this.getPhysicalLocation(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLocationSettingsMapEntry_Key(), theMachinePackage.getPosition(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(timingSettingsMapEntryEClass, Map.Entry.class, "TimingSettingsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTimingSettingsMapEntry_Name(), ecorePackage.getEString(), "name", null, 0, 1, Map.Entry.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getTimingSettingsMapEntry_Value(), theTimingPackage.getTiming(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTimingSettingsMapEntry_Key(), theMachinePackage.getActionType(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(physicalSettingsEClass, PhysicalSettings.class, "PhysicalSettings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getPhysicalSettings_TimingSettings(), this.getTimingSettingsMapEntry(), null, "timingSettings", null, 0, -1, PhysicalSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPhysicalSettings_MotionSettings(), this.getMotionSettingsMapEntry(), this.getMotionSettingsMapEntry_Settings(), "motionSettings", null, 0, -1, PhysicalSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPhysicalSettings_Resource(), theMachinePackage.getIResource(), null, "resource", null, 1, 1, PhysicalSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPhysicalSettings_Peripheral(), theMachinePackage.getPeripheral(), null, "peripheral", null, 1, 1, PhysicalSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPhysicalSettings_Settings(), this.getSettings(), null, "settings", null, 0, 1, PhysicalSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPhysicalSettings_Name(), ecorePackage.getEString(), "name", null, 0, 1, PhysicalSettings.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

        initEClass(motionSettingsMapEntryEClass, Map.Entry.class, "MotionSettingsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getMotionSettingsMapEntry_Name(), ecorePackage.getEString(), "name", null, 0, 1, Map.Entry.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getMotionSettingsMapEntry_Key(), theMachinePackage.getAxis(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMotionSettingsMapEntry_Value(), this.getMotionSettings(), this.getMotionSettings_Entry(), "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMotionSettingsMapEntry_Settings(), this.getPhysicalSettings(), this.getPhysicalSettings_MotionSettings(), "settings", null, 1, 1, Map.Entry.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(motionSettingsEClass, MotionSettings.class, "MotionSettings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getMotionSettings_LocationSettings(), this.getLocationSettingsMapEntry(), null, "locationSettings", null, 0, -1, MotionSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMotionSettings_ProfileSettings(), this.getProfileSettingsMapEntry(), null, "profileSettings", null, 0, -1, MotionSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMotionSettings_DistanceSettings(), this.getDistanceSettingsMapEntry(), this.getDistanceSettingsMapEntry_MotionSettings(), "distanceSettings", null, 0, -1, MotionSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMotionSettings_Entry(), this.getMotionSettingsMapEntry(), this.getMotionSettingsMapEntry_Value(), "entry", null, 1, 1, MotionSettings.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(physicalLocationEClass, PhysicalLocation.class, "PhysicalLocation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getPhysicalLocation_DefaultExp(), theExpressionsPackage.getExpression(), null, "defaultExp", null, 1, 1, PhysicalLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPhysicalLocation_MinExp(), theExpressionsPackage.getExpression(), null, "minExp", null, 0, 1, PhysicalLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPhysicalLocation_MaxExp(), theExpressionsPackage.getExpression(), null, "maxExp", null, 0, 1, PhysicalLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPhysicalLocation_Default(), ecorePackage.getEBigDecimal(), "default", null, 0, 1, PhysicalLocation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEAttribute(getPhysicalLocation_Min(), ecorePackage.getEBigDecimal(), "min", null, 0, 1, PhysicalLocation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEAttribute(getPhysicalLocation_Max(), ecorePackage.getEBigDecimal(), "max", null, 0, 1, PhysicalLocation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

        initEClass(motionProfileSettingsEClass, MotionProfileSettings.class, "MotionProfileSettings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getMotionProfileSettings_MotionProfile(), ecorePackage.getEString(), "motionProfile", null, 0, 1, MotionProfileSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMotionProfileSettings_MotionArguments(), this.getMotionArgumentsMapEntry(), null, "motionArguments", null, 0, -1, MotionProfileSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(motionArgumentsMapEntryEClass, Map.Entry.class, "MotionArgumentsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getMotionArgumentsMapEntry_Key(), ecorePackage.getEString(), "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMotionArgumentsMapEntry_Value(), theExpressionsPackage.getExpression(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getMotionArgumentsMapEntry_BdValue(), ecorePackage.getEBigDecimal(), "bdValue", null, 0, 1, Map.Entry.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

        initEClass(distanceSettingsMapEntryEClass, Map.Entry.class, "DistanceSettingsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDistanceSettingsMapEntry_Name(), ecorePackage.getEString(), "name", null, 0, 1, Map.Entry.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getDistanceSettingsMapEntry_Value(), theExpressionsPackage.getExpression(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getDistanceSettingsMapEntry_Key(), theMachinePackage.getDistance(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDistanceSettingsMapEntry_BdValue(), ecorePackage.getEBigDecimal(), "bdValue", null, 0, 1, Map.Entry.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getDistanceSettingsMapEntry_MotionSettings(), this.getMotionSettings(), this.getMotionSettings_DistanceSettings(), "motionSettings", null, 1, 1, Map.Entry.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        // Create resource
        createResource(eNS_URI);
    }

} //SettingPackageImpl
