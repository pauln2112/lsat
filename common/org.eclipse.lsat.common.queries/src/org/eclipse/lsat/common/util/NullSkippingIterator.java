/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;

public class NullSkippingIterator<E> extends ProcessingIterator<E> {
    private final Iterator<? extends E> source;

    public NullSkippingIterator(Iterator<? extends E> source) {
        this.source = source;
    }

    @Override
    protected boolean toNext() {
        while (source.hasNext()) {
            E next = source.next();
            if (null != next) {
                return setNext(next);
            }
        }
        return done();
    }
}
