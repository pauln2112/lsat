/**
 */
package setting;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see setting.SettingPackage
 * @generated
 */
public interface SettingFactory extends EFactory {
	/**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	SettingFactory eINSTANCE = setting.impl.SettingFactoryImpl.init();

	/**
     * Returns a new object of class '<em>Settings</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Settings</em>'.
     * @generated
     */
	Settings createSettings();

	/**
     * Returns a new object of class '<em>Physical Settings</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Physical Settings</em>'.
     * @generated
     */
	PhysicalSettings createPhysicalSettings();

	/**
     * Returns a new object of class '<em>Motion Settings</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Motion Settings</em>'.
     * @generated
     */
	MotionSettings createMotionSettings();

	/**
     * Returns a new object of class '<em>Physical Location</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Physical Location</em>'.
     * @generated
     */
	PhysicalLocation createPhysicalLocation();

	/**
     * Returns a new object of class '<em>Motion Profile Settings</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Motion Profile Settings</em>'.
     * @generated
     */
	MotionProfileSettings createMotionProfileSettings();

	/**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
	SettingPackage getSettingPackage();

} //SettingFactory
