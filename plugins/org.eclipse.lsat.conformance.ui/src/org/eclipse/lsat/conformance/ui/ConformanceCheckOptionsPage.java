/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance.ui;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.typed.PojoProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.SelectObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.lsat.conformance.ConformanceCheckInput;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

class ConformanceCheckOptionsPage extends WizardPage {
    @SuppressWarnings("unused")
    private DataBindingContext m_bindingContext;

    private final ConformanceCheckOptions options;

    private Button btnCheckButton;

    private Button btnMinimal;

    private Button btnClaim;

    private Button btnDispatching;

    public ConformanceCheckOptionsPage(ConformanceCheckOptions options) {
        super("ConformancePropertiesPage");
        setTitle("Select conformance check options");
        setMessage("Select the options to apply for the conformance test");
        this.options = options;
    }

    @Override
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        setControl(container);
        container.setLayout(new GridLayout(1, false));

        Group grpActivityDispatching = new Group(container, SWT.NONE);
        grpActivityDispatching.setLayout(new GridLayout(1, false));
        grpActivityDispatching.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        grpActivityDispatching.setText("Activity dispatching");

        btnCheckButton = new Button(grpActivityDispatching, SWT.CHECK);
        btnCheckButton.setToolTipText("Select if activity dispatching occurs multiple times in trace");
        btnCheckButton.setText("Looped activities");

        Group grpConformanceLevel = new Group(container, SWT.NONE);
        grpConformanceLevel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        grpConformanceLevel.setText("Conformance level");
        grpConformanceLevel.setLayout(new GridLayout(1, false));

        btnMinimal = new Button(grpConformanceLevel, SWT.RADIO);
        btnMinimal.setText("Minimal");

        btnClaim = new Button(grpConformanceLevel, SWT.RADIO);
        btnClaim.setText("Claim");

        btnDispatching = new Button(grpConformanceLevel, SWT.RADIO);
        btnDispatching.setText("Dispatching");

        m_bindingContext = initDataBindings();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected DataBindingContext initDataBindings() {
        DataBindingContext bindingContext = new DataBindingContext();
        //
        IObservableValue observeSelectionBtnCheckButtonObserveWidget = WidgetProperties.widgetSelection()
                .observe(btnCheckButton);
        IObservableValue dipatchingLoopOptionsObserveValue = PojoProperties.value("dipatchingLoop").observe(options);
        bindingContext.bindValue(observeSelectionBtnCheckButtonObserveWidget, dipatchingLoopOptionsObserveValue, null,
                null);

        // Manual code to bind an enum value to a radio button
        IObservableValue observeSelectionBtnMinimalObserveWidget = WidgetProperties.widgetSelection()
                .observe(btnMinimal);
        IObservableValue observeSelectionBtnClaimObserveWidget = WidgetProperties.widgetSelection().observe(btnClaim);
        IObservableValue observeSelectionBtnDispatchingObserveWidget = WidgetProperties.widgetSelection()
                .observe(btnDispatching);
        IObservableValue conformanceLevelOptionsObserveValue = PojoProperties.value("conformanceLevel")
                .observe(options);
        SelectObservableValue conformanceLevelObservable = new SelectObservableValue(ConformanceCheckInput.Level.class);
        conformanceLevelObservable.addOption(ConformanceCheckInput.Level.Minimal,
                observeSelectionBtnMinimalObserveWidget);
        conformanceLevelObservable.addOption(ConformanceCheckInput.Level.Claim, observeSelectionBtnClaimObserveWidget);
        conformanceLevelObservable.addOption(ConformanceCheckInput.Level.Dispatching,
                observeSelectionBtnDispatchingObserveWidget);
        bindingContext.bindValue(conformanceLevelObservable, conformanceLevelOptionsObserveValue);
        return bindingContext;
    }
}
