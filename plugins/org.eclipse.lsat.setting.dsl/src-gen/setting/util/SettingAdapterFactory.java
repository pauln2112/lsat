/**
 */
package setting.util;

import expressions.Expression;
import java.util.Map;

import machine.ActionType;
import machine.Axis;
import machine.Distance;
import machine.HasResourcePeripheral;
import machine.ImportContainer;
import machine.Position;
import machine.Profile;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import setting.*;

import timing.Timing;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see setting.SettingPackage
 * @generated
 */
public class SettingAdapterFactory extends AdapterFactoryImpl {
	/**
     * The cached model package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static SettingPackage modelPackage;

	/**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public SettingAdapterFactory() {
        if (modelPackage == null)
        {
            modelPackage = SettingPackage.eINSTANCE;
        }
    }

	/**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
	@Override
	public boolean isFactoryForType(Object object) {
        if (object == modelPackage)
        {
            return true;
        }
        if (object instanceof EObject)
        {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

	/**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected SettingSwitch<Adapter> modelSwitch =
		new SettingSwitch<Adapter>()
        {
            @Override
            public Adapter caseSettings(Settings object)
            {
                return createSettingsAdapter();
            }
            @Override
            public Adapter caseProfileSettingsMapEntry(Map.Entry<Profile, MotionProfileSettings> object)
            {
                return createProfileSettingsMapEntryAdapter();
            }
            @Override
            public Adapter caseLocationSettingsMapEntry(Map.Entry<Position, PhysicalLocation> object)
            {
                return createLocationSettingsMapEntryAdapter();
            }
            @Override
            public Adapter caseTimingSettingsMapEntry(Map.Entry<ActionType, Timing> object)
            {
                return createTimingSettingsMapEntryAdapter();
            }
            @Override
            public Adapter casePhysicalSettings(PhysicalSettings object)
            {
                return createPhysicalSettingsAdapter();
            }
            @Override
            public Adapter caseMotionSettingsMapEntry(Map.Entry<Axis, MotionSettings> object)
            {
                return createMotionSettingsMapEntryAdapter();
            }
            @Override
            public Adapter caseMotionSettings(MotionSettings object)
            {
                return createMotionSettingsAdapter();
            }
            @Override
            public Adapter casePhysicalLocation(PhysicalLocation object)
            {
                return createPhysicalLocationAdapter();
            }
            @Override
            public Adapter caseMotionProfileSettings(MotionProfileSettings object)
            {
                return createMotionProfileSettingsAdapter();
            }
            @Override
            public Adapter caseMotionArgumentsMapEntry(Map.Entry<String, Expression> object)
            {
                return createMotionArgumentsMapEntryAdapter();
            }
            @Override
            public Adapter caseDistanceSettingsMapEntry(Map.Entry<Distance, Expression> object)
            {
                return createDistanceSettingsMapEntryAdapter();
            }
            @Override
            public Adapter caseImportContainer(ImportContainer object)
            {
                return createImportContainerAdapter();
            }
            @Override
            public Adapter caseHasResourcePeripheral(HasResourcePeripheral object)
            {
                return createHasResourcePeripheralAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object)
            {
                return createEObjectAdapter();
            }
        };

	/**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
	@Override
	public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject)target);
    }


	/**
     * Creates a new adapter for an object of class '{@link setting.Settings <em>Settings</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see setting.Settings
     * @generated
     */
	public Adapter createSettingsAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Profile Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
	public Adapter createProfileSettingsMapEntryAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Location Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
	public Adapter createLocationSettingsMapEntryAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Timing Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
	public Adapter createTimingSettingsMapEntryAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link setting.PhysicalSettings <em>Physical Settings</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see setting.PhysicalSettings
     * @generated
     */
	public Adapter createPhysicalSettingsAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Motion Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
	public Adapter createMotionSettingsMapEntryAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link setting.MotionSettings <em>Motion Settings</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see setting.MotionSettings
     * @generated
     */
	public Adapter createMotionSettingsAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link setting.PhysicalLocation <em>Physical Location</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see setting.PhysicalLocation
     * @generated
     */
	public Adapter createPhysicalLocationAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link setting.MotionProfileSettings <em>Motion Profile Settings</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see setting.MotionProfileSettings
     * @generated
     */
	public Adapter createMotionProfileSettingsAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Motion Arguments Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
	public Adapter createMotionArgumentsMapEntryAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Distance Settings Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
	public Adapter createDistanceSettingsMapEntryAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.ImportContainer <em>Import Container</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.ImportContainer
     * @generated
     */
	public Adapter createImportContainerAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.HasResourcePeripheral <em>Has Resource Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.HasResourcePeripheral
     * @generated
     */
	public Adapter createHasResourcePeripheralAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
	public Adapter createEObjectAdapter() {
        return null;
    }

} //SettingAdapterFactory
