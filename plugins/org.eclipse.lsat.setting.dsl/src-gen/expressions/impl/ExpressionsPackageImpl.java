/**
 */
package expressions.impl;

import expressions.AbstractElement;
import expressions.Add;
import expressions.BigDecimalConstant;
import expressions.BinaryExpression;
import expressions.Declaration;
import expressions.DeclarationRef;
import expressions.Divide;
import expressions.Expression;
import expressions.ExpressionsFactory;
import expressions.ExpressionsPackage;
import expressions.Multiply;
import expressions.Subtract;

import machine.MachinePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import setting.SettingPackage;

import setting.impl.SettingPackageImpl;

import timing.TimingPackage;

import timing.impl.TimingPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExpressionsPackageImpl extends EPackageImpl implements ExpressionsPackage {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass abstractElementEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass declarationEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass expressionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass addEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass subtractEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass multiplyEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass bigDecimalConstantEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass declarationRefEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass divideEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass binaryExpressionEClass = null;

	/**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see expressions.ExpressionsPackage#eNS_URI
     * @see #init()
     * @generated
     */
	private ExpressionsPackageImpl() {
        super(eNS_URI, ExpressionsFactory.eINSTANCE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static boolean isInited = false;

	/**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link ExpressionsPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
	public static ExpressionsPackage init() {
        if (isInited) return (ExpressionsPackage)EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI);

        // Obtain or create and register package
        Object registeredExpressionsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        ExpressionsPackageImpl theExpressionsPackage = registeredExpressionsPackage instanceof ExpressionsPackageImpl ? (ExpressionsPackageImpl)registeredExpressionsPackage : new ExpressionsPackageImpl();

        isInited = true;

        // Initialize simple dependencies
        MachinePackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SettingPackage.eNS_URI);
        SettingPackageImpl theSettingPackage = (SettingPackageImpl)(registeredPackage instanceof SettingPackageImpl ? registeredPackage : SettingPackage.eINSTANCE);
        registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimingPackage.eNS_URI);
        TimingPackageImpl theTimingPackage = (TimingPackageImpl)(registeredPackage instanceof TimingPackageImpl ? registeredPackage : TimingPackage.eINSTANCE);

        // Create package meta-data objects
        theExpressionsPackage.createPackageContents();
        theSettingPackage.createPackageContents();
        theTimingPackage.createPackageContents();

        // Initialize created meta-data
        theExpressionsPackage.initializePackageContents();
        theSettingPackage.initializePackageContents();
        theTimingPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theExpressionsPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(ExpressionsPackage.eNS_URI, theExpressionsPackage);
        return theExpressionsPackage;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getAbstractElement() {
        return abstractElementEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDeclaration() {
        return declarationEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getDeclaration_Name() {
        return (EAttribute)declarationEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getDeclaration_Expression() {
        return (EReference)declarationEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getDeclaration__Fqn() {
        return declarationEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getExpression() {
        return expressionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getExpression__Evaluate() {
        return expressionEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getAdd() {
        return addEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getAdd__Operation__BigDecimal_BigDecimal() {
        return addEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getSubtract() {
        return subtractEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getSubtract__Operation__BigDecimal_BigDecimal() {
        return subtractEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getMultiply() {
        return multiplyEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getMultiply__Operation__BigDecimal_BigDecimal() {
        return multiplyEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getBigDecimalConstant() {
        return bigDecimalConstantEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getBigDecimalConstant_Value() {
        return (EAttribute)bigDecimalConstantEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getBigDecimalConstant__Evaluate() {
        return bigDecimalConstantEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDeclarationRef() {
        return declarationRefEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getDeclarationRef_Declaration() {
        return (EReference)declarationRefEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getDeclarationRef__Evaluate() {
        return declarationRefEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getDeclarationRef__HasCircularDependencies__EObject() {
        return declarationRefEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDivide() {
        return divideEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getDivide__Operation__BigDecimal_BigDecimal() {
        return divideEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getBinaryExpression() {
        return binaryExpressionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getBinaryExpression_Left() {
        return (EReference)binaryExpressionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getBinaryExpression_Right() {
        return (EReference)binaryExpressionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getBinaryExpression__Evaluate() {
        return binaryExpressionEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getBinaryExpression__Operation__BigDecimal_BigDecimal() {
        return binaryExpressionEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ExpressionsFactory getExpressionsFactory() {
        return (ExpressionsFactory)getEFactoryInstance();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isCreated = false;

	/**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        abstractElementEClass = createEClass(ABSTRACT_ELEMENT);

        declarationEClass = createEClass(DECLARATION);
        createEAttribute(declarationEClass, DECLARATION__NAME);
        createEReference(declarationEClass, DECLARATION__EXPRESSION);
        createEOperation(declarationEClass, DECLARATION___FQN);

        expressionEClass = createEClass(EXPRESSION);
        createEOperation(expressionEClass, EXPRESSION___EVALUATE);

        addEClass = createEClass(ADD);
        createEOperation(addEClass, ADD___OPERATION__BIGDECIMAL_BIGDECIMAL);

        subtractEClass = createEClass(SUBTRACT);
        createEOperation(subtractEClass, SUBTRACT___OPERATION__BIGDECIMAL_BIGDECIMAL);

        multiplyEClass = createEClass(MULTIPLY);
        createEOperation(multiplyEClass, MULTIPLY___OPERATION__BIGDECIMAL_BIGDECIMAL);

        bigDecimalConstantEClass = createEClass(BIG_DECIMAL_CONSTANT);
        createEAttribute(bigDecimalConstantEClass, BIG_DECIMAL_CONSTANT__VALUE);
        createEOperation(bigDecimalConstantEClass, BIG_DECIMAL_CONSTANT___EVALUATE);

        declarationRefEClass = createEClass(DECLARATION_REF);
        createEReference(declarationRefEClass, DECLARATION_REF__DECLARATION);
        createEOperation(declarationRefEClass, DECLARATION_REF___EVALUATE);
        createEOperation(declarationRefEClass, DECLARATION_REF___HAS_CIRCULAR_DEPENDENCIES__EOBJECT);

        divideEClass = createEClass(DIVIDE);
        createEOperation(divideEClass, DIVIDE___OPERATION__BIGDECIMAL_BIGDECIMAL);

        binaryExpressionEClass = createEClass(BINARY_EXPRESSION);
        createEReference(binaryExpressionEClass, BINARY_EXPRESSION__LEFT);
        createEReference(binaryExpressionEClass, BINARY_EXPRESSION__RIGHT);
        createEOperation(binaryExpressionEClass, BINARY_EXPRESSION___EVALUATE);
        createEOperation(binaryExpressionEClass, BINARY_EXPRESSION___OPERATION__BIGDECIMAL_BIGDECIMAL);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isInitialized = false;

	/**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        declarationEClass.getESuperTypes().add(this.getAbstractElement());
        expressionEClass.getESuperTypes().add(this.getAbstractElement());
        addEClass.getESuperTypes().add(this.getBinaryExpression());
        subtractEClass.getESuperTypes().add(this.getBinaryExpression());
        multiplyEClass.getESuperTypes().add(this.getBinaryExpression());
        bigDecimalConstantEClass.getESuperTypes().add(this.getExpression());
        declarationRefEClass.getESuperTypes().add(this.getExpression());
        divideEClass.getESuperTypes().add(this.getBinaryExpression());
        binaryExpressionEClass.getESuperTypes().add(this.getExpression());

        // Initialize classes, features, and operations; add parameters
        initEClass(abstractElementEClass, AbstractElement.class, "AbstractElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(declarationEClass, Declaration.class, "Declaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDeclaration_Name(), ecorePackage.getEString(), "name", null, 0, 1, Declaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getDeclaration_Expression(), this.getExpression(), null, "expression", null, 0, 1, Declaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getDeclaration__Fqn(), ecorePackage.getEString(), "fqn", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(expressionEClass, Expression.class, "Expression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEOperation(getExpression__Evaluate(), ecorePackage.getEBigDecimal(), "evaluate", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(addEClass, Add.class, "Add", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        EOperation op = initEOperation(getAdd__Operation__BigDecimal_BigDecimal(), ecorePackage.getEBigDecimal(), "operation", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "left", 1, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "right", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(subtractEClass, Subtract.class, "Subtract", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        op = initEOperation(getSubtract__Operation__BigDecimal_BigDecimal(), ecorePackage.getEBigDecimal(), "operation", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "left", 1, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "right", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(multiplyEClass, Multiply.class, "Multiply", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        op = initEOperation(getMultiply__Operation__BigDecimal_BigDecimal(), ecorePackage.getEBigDecimal(), "operation", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "left", 1, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "right", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(bigDecimalConstantEClass, BigDecimalConstant.class, "BigDecimalConstant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getBigDecimalConstant_Value(), ecorePackage.getEBigDecimal(), "value", null, 0, 1, BigDecimalConstant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getBigDecimalConstant__Evaluate(), ecorePackage.getEBigDecimal(), "evaluate", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(declarationRefEClass, DeclarationRef.class, "DeclarationRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getDeclarationRef_Declaration(), this.getDeclaration(), null, "declaration", null, 0, 1, DeclarationRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getDeclarationRef__Evaluate(), ecorePackage.getEBigDecimal(), "evaluate", 1, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getDeclarationRef__HasCircularDependencies__EObject(), ecorePackage.getEBoolean(), "hasCircularDependencies", 1, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEObject(), "object", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(divideEClass, Divide.class, "Divide", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        op = initEOperation(getDivide__Operation__BigDecimal_BigDecimal(), ecorePackage.getEBigDecimal(), "operation", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "left", 1, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "right", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(binaryExpressionEClass, BinaryExpression.class, "BinaryExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getBinaryExpression_Left(), this.getExpression(), null, "left", null, 1, 1, BinaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getBinaryExpression_Right(), this.getExpression(), null, "right", null, 1, 1, BinaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getBinaryExpression__Evaluate(), ecorePackage.getEBigDecimal(), "evaluate", 1, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getBinaryExpression__Operation__BigDecimal_BigDecimal(), ecorePackage.getEBigDecimal(), "operation", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "left", 1, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBigDecimal(), "right", 1, 1, IS_UNIQUE, IS_ORDERED);

        // Create resource
        createResource(eNS_URI);
    }

} //ExpressionsPackageImpl
