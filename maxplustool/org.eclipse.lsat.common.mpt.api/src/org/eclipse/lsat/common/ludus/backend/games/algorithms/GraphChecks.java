/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games.algorithms;

import java.util.function.Predicate;

import org.eclipse.lsat.common.ludus.backend.games.GameGraph;

/**
 * @author Bram van der Sanden
 */
public class GraphChecks {
    private GraphChecks() {
        // Empty
    }

    /**
     * Check whether each vertex in the game graph has at least one successor.
     *
     * @param graph game graph
     * @return true if each vertex in the given game graph contains at least one successor.
     */
    public static <V, E> boolean checkEachNodeHasSuccessor(GameGraph<V, E> graph) {
        return graph.getVertices().stream().allMatch(checkHasSuccessors(graph));
    }

    /**
     * Return true if the vertex has at least one successor.
     *
     * @param graph game graph
     * @return true if the vertex has at least one successor
     */
    private static <V, E> Predicate<V> checkHasSuccessors(GameGraph<V, E> graph) {
        return v -> graph.outgoingEdgesOf(v).size() > 0;
    }
}
