package org.eclipse.lsat.activity.diagram.layout;

import java.util.EnumSet;
import org.eclipse.elk.core.data.ILayoutMetaDataProvider;
import org.eclipse.elk.graph.properties.IProperty;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayout;
import org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayoutMetaDataProvider;

@SuppressWarnings("all")
public class ActivityDiagramLayoutOptions implements ILayoutMetaDataProvider {
  /**
   * The id of the Activity Diagram algorithm.
   */
  public static final String ALGORITHM_ID = "org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayout";
  
  /**
   * The eClass of the node
   */
  public static final IProperty<EClass> E_CLASS = ActivityDiagramLayoutMetaDataProvider.E_CLASS;
  
  /**
   * Totally irrelevant for the algorithm, but it makes life a lot easier while debugging
   */
  public static final IProperty<String> NAME = ActivityDiagramLayoutMetaDataProvider.NAME;
  
  /**
   * name of the event
   */
  public static final IProperty<String> EVENT_NAME = ActivityDiagramLayoutMetaDataProvider.EVENT_NAME;
  
  /**
   * Abstract row of the node in the final, global layout, determined by topological order and node type
   */
  public static final IProperty<Integer> LAYOUT_ROW = ActivityDiagramLayoutMetaDataProvider.LAYOUT_ROW;
  
  public void apply(final org.eclipse.elk.core.data.ILayoutMetaDataProvider.Registry registry) {
    registry.register(new org.eclipse.elk.core.data.LayoutAlgorithmData(
        "org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayout",
        "Activity Diagram",
        "Performs a basic layout for activity diagrams. The algorithm does not preserve any existing layout, so run first, tune later. NB Work in progress.",
        new org.eclipse.elk.core.util.AlgorithmFactory(ActivityDiagramLayout.Provider.class, ""),
        "org.eclipse.lsat.activity.diagram.layout.LSAT",
        null,
        null,
        EnumSet.of(org.eclipse.elk.core.options.GraphFeature.COMPOUND, org.eclipse.elk.core.options.GraphFeature.PORTS)
    ));
    registry.addOptionSupport(
        "org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayout",
        "org.eclipse.lsat.activity.diagram.layout.eClass",
        E_CLASS.getDefault()
    );
    registry.addOptionSupport(
        "org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayout",
        "org.eclipse.lsat.activity.diagram.layout.name",
        NAME.getDefault()
    );
    registry.addOptionSupport(
        "org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayout",
        "org.eclipse.lsat.activity.diagram.layout.eventName",
        EVENT_NAME.getDefault()
    );
    registry.addOptionSupport(
        "org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayout",
        "org.eclipse.lsat.activity.diagram.layout.layoutRow",
        LAYOUT_ROW.getDefault()
    );
  }
}
