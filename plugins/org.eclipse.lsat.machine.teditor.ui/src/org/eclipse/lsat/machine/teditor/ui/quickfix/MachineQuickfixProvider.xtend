/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.teditor.ui.quickfix

import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider
import org.eclipse.lsat.machine.teditor.validation.MachineValidator
import org.eclipse.xtext.validation.Issue
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.ui.editor.quickfix.Fix
import machine.Peripheral

//import org.eclipse.xtext.EcoreUtil2

/**
 * Custom quickfixes.
 *
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#quick-fixes
 */
class MachineQuickfixProvider extends DefaultQuickfixProvider {

//	@Fix(MachineValidator.INVALID_NAME)
//	def capitalizeName(Issue issue, IssueResolutionAcceptor acceptor) {
//		acceptor.accept(issue, 'Capitalize name', 'Capitalize the name.', 'upcase.png') [
//			context |
//			val xtextDocument = context.xtextDocument
//			val firstLetter = xtextDocument.get(issue.offset, 1)
//			xtextDocument.replace(issue.offset, 1, firstLetter.toUpperCase)
//		]
//	}


    @Fix(MachineValidator.INVALID_MUTUAL_EXCLUSIONS)
    def CreateMissingEntity(Issue issue, IssueResolutionAcceptor acceptor)
    {
        acceptor.accept(issue,"Remove Distances","Remove Distances", "" ,
            [element, context | 
                val peripheral = element as Peripheral
                peripheral.distances.clear
            ]
        );
        acceptor.accept(issue,"Remove Positions","Remove Positions", "" ,
            [element, context | 
                val peripheral = element as Peripheral
                peripheral.axisPositions.clear
                peripheral.positions.clear
                peripheral.paths.clear
            ]
        );
    }
}
