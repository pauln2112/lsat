/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.queries;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToIntBiFunction;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.lsat.common.util.BranchIterator;
import org.eclipse.lsat.common.util.BranchUpToIterator;
import org.eclipse.lsat.common.util.BufferedIterator;
import org.eclipse.lsat.common.util.ClosureIterator;
import org.eclipse.lsat.common.util.IterableUtil;
import org.eclipse.lsat.common.util.IteratorUtil;
import org.eclipse.lsat.common.util.PeekingIterator;
import org.eclipse.lsat.common.util.ProcessingIterator;
import org.eclipse.lsat.common.util.SegmentIterator;
import org.eclipse.lsat.common.util.TreeWalkerIterator;

/**
 * This class implements OCL like queries in plain java using Iterators<br>
 * (thus no need for creating a temporary Collection instance).
 */
public class IteratorQueries {
    private IteratorQueries() {
        // Empty
    }

    /**
     * From a {@link BranchIterator} find the nearest node in each branch that matches the {@code predicate}
     */
    public static <Input> Iterator<Input> findNearest(BranchIterator<Input> iterator,
            Predicate<? super Input> predicate)
    {
        return new ProcessingIterator<Input>() {
            @Override
            protected boolean toNext() {
                while (iterator.hasNext()) {
                    final Input next = iterator.next();
                    if (predicate.test(next)) {
                        final boolean result = setNext(next);
                        // Found the nearest, so prune this branch
                        iterator.prune();
                        return result;
                    }
                }
                return done();
            }
        };
    }

    /**
     * From a {@link BranchIterator} find the nearest node in each branch that is an instance of {@code type}
     */
    public static <Input, Output> Iterator<Output> findNearest(BranchIterator<Input> iterator, Class<Output> type) {
        return asType(findNearest(iterator, type::isInstance), type);
    }

    /**
     * Returns the elements of each branch of a {@link BranchIterator} up to the element that matches the
     * {@code predicate}. The matched element itself is excluded from the result.
     *
     * @return an iterator containing all elements of {@code iterator} up to (thus excluding) the first element that
     *     matches the {@code predicate} per branch.
     */
    public static <Input> BranchIterator<Input> until(BranchIterator<Input> iterator,
            Predicate<? super Input> predicate)
    {
        return new BranchUpToIterator<Input>(iterator, predicate, false);
    }

    /**
     * Returns the elements of each branch of a {@link BranchIterator} up to the element that matches the
     * {@code predicate}. The matched element itself is included in the result.
     *
     * @return an iterator containing all elements of {@code iterator} up to and including the first element that
     *     matches the {@code predicate} per branch.
     */
    public static <Input> BranchIterator<Input> upToAndIncluding(BranchIterator<Input> iterator,
            Predicate<? super Input> predicate)
    {
        return new BranchUpToIterator<Input>(iterator, predicate, true);
    }

    /**
     * Collects all children and children's children (breadth first) without the need for creating a temporary
     * Collection instance. Behaves like OCL <tt>iterator</tt>->closure(e | <tt>functor.closureOf(e)</tt>)
     */
    public static <Input> BranchIterator<Input> closure(Iterator<? extends Input> iterator,
            Function<Input, Iterator<? extends Input>> functor)
    {
        return closure(iterator, false, functor);
    }

    /**
     * Collects all children and children's children (breadth first) without the need for creating a temporary
     * Collection instance. Behaves like OCL <tt>iterator</tt>->closure(e | <tt>functor.closureOf(e)</tt>)
     */
    public static <Input> BranchIterator<Input> closure(Iterator<? extends Input> iterator, boolean includeSource,
            Function<Input, Iterator<? extends Input>> functor)
    {
        return new ClosureIterator<>(iterator, includeSource, functor);
    }

    /**
     * Collects all children and children's children (breadth first) and stops the sub branch as soon as the predicate
     * evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     *
     * @see #closure(Iterator, Function)
     */
    public static <Input> BranchIterator<Input> closureWhile(Iterator<? extends Input> iterator,
            Function<Input, Iterator<? extends Input>> functor, Predicate<? super Input> predicate)
    {
        return closureWhile(iterator, false, functor, predicate);
    }

    /**
     * Collects all children and children's children (breadth first) and stops the sub branch as soon as the predicate
     * evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     *
     * @see #closure(Iterator, Function)
     */
    public static <Input> BranchIterator<Input> closureWhile(Iterator<? extends Input> iterator, boolean includeSource,
            Function<Input, Iterator<? extends Input>> functor, Predicate<? super Input> predicate)
    {
        return until(closure(iterator, includeSource, functor), predicate.negate());
    }

    /**
     * Collects child, child's child, etc (breadth first) without the need for creating a temporary Collection instance.
     * Behaves like OCL <tt>iterator</tt>->closure(e | <tt>functor.getOne(e)</tt>)
     */
    public static <Input> BranchIterator<Input> closureOne(Iterator<? extends Input> iterator,
            Function<Input, ? extends Input> functor)
    {
        return closureOne(iterator, false, functor);
    }

    /**
     * Collects child, child's child, etc (breadth first) without the need for creating a temporary Collection instance.
     * Behaves like OCL <tt>iterator</tt>->closure(e | <tt>functor.getOne(e)</tt>)
     */
    public static <Input> BranchIterator<Input> closureOne(Iterator<? extends Input> iterator, boolean includeSource,
            Function<Input, ? extends Input> functor)
    {
        // A single valued attribute can be interpreted as a many attribute containing 1
        // element. Performance wise this might not be optimal, but it allows us to
        // reuse a lot of code.
        return new ClosureIterator<>(iterator, includeSource, e -> toMany(functor.apply(e)));
    }

    /**
     * Collects all children and children's children (breadth first) and stops each sub branch as soon as the predicate
     * evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     *
     * @see #closureOne(Iterator, Function)
     */
    public static <Input> BranchIterator<Input> closureOneWhile(Iterator<? extends Input> iterator,
            Function<Input, ? extends Input> functor, Predicate<? super Input> predicate)
    {
        return closureOneWhile(iterator, false, functor, predicate);
    }

    /**
     * Collects all children and children's children (breadth first) and stops each sub branch as soon as the predicate
     * evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     */
    public static <Input> BranchIterator<Input> closureOneWhile(Iterator<? extends Input> iterator,
            boolean includeSource, Function<Input, ? extends Input> functor, Predicate<? super Input> predicate)
    {
        return until(closureOne(iterator, includeSource, functor), predicate.negate());
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> without the need for
     * creating a temporary Collection instance. Note that the resulting iterator might contain a tree node multiple
     * times if elements in <tt>iterator</tt> share this tree node as common ancestor.
     */
    public static <Input> BranchIterator<Input> climbTree(Iterator<? extends Input> iterator,
            Function<Input, ? extends Input> functor)
    {
        return climbTree(iterator, false, functor);
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> without the need for
     * creating a temporary Collection instance. Note that the resulting iterator might contain a tree node multiple
     * times if elements in <tt>iterator</tt> share this tree node as common ancestor.
     */
    public static <Input> BranchIterator<Input> climbTree(Iterator<? extends Input> iterator, boolean includeSource,
            Function<Input, ? extends Input> functor)
    {
        // A single valued attribute can be interpreted as a many attribute containing 1
        // element. Performance wise this might not be optimal, but it allows us to
        // reuse a lot of code.
        return new TreeWalkerIterator<>(iterator, includeSource, e -> toMany(functor.apply(e)));
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> and stops each sub branch as
     * soon as the predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance. Note
     * that the resulting iterator might contain a tree node multiple times if elements in <tt>iterator</tt> share this
     * tree node as common ancestor.
     */
    public static <Input> BranchIterator<Input> climbTreeWhile(Iterator<? extends Input> iterator,
            Function<Input, ? extends Input> functor, Predicate<? super Input> predicate)
    {
        return climbTreeWhile(iterator, false, functor, predicate);
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> and stops each sub branch as
     * soon as the predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance. Note
     * that the resulting iterator might contain a tree node multiple times if elements in <tt>iterator</tt> share this
     * tree node as common ancestor.
     */
    public static <Input> BranchIterator<Input> climbTreeWhile(Iterator<? extends Input> iterator,
            boolean includeSource, Function<Input, ? extends Input> functor, Predicate<? super Input> predicate)
    {
        return until(climbTree(iterator, includeSource, functor), predicate.negate());
    }

    /**
     * Collects all descendants (children and children's children; depth first) without the need for creating a
     * temporary Collection instance.
     */
    public static <Input> BranchIterator<Input> walkTree(Iterator<? extends Input> iterator,
            Function<Input, Iterator<? extends Input>> functor)
    {
        return walkTree(iterator, false, functor);
    }

    /**
     * Collects all descendants (children and children's children; depth first) without the need for creating a
     * temporary Collection instance.
     */
    public static <Input> BranchIterator<Input> walkTree(Iterator<? extends Input> iterator, boolean includeSource,
            Function<Input, Iterator<? extends Input>> functor)
    {
        return new TreeWalkerIterator<Input>(iterator, includeSource, functor);
    }

    /**
     * Collects all descendants (children and children's children; depth first) and stops each sub branch as soon as the
     * predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     */
    public static <Input> BranchIterator<Input> walkTreeWhile(Iterator<? extends Input> iterator,
            Function<Input, Iterator<? extends Input>> functor, Predicate<? super Input> predicate)
    {
        return walkTreeWhile(iterator, false, functor, predicate);
    }

    /**
     * Collects all descendants (children and children's children; depth first) and stops each sub branch as soon as the
     * predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance.
     */
    public static <Input> BranchIterator<Input> walkTreeWhile(Iterator<? extends Input> iterator, boolean includeSource,
            Function<Input, Iterator<? extends Input>> functor, Predicate<? super Input> predicate)
    {
        return until(walkTree(iterator, includeSource, functor), predicate.negate());
    }

    /**
     * Selects elements without the need for creating a temporary Collection instance. Behaves like OCL
     * <tt>iterator</tt>->select(e | not functor.accept(e).oclIsInvalid())
     */
    public static <Input> Iterator<Input> select(Iterator<Input> iterator, Predicate<? super Input> predicate) {
        return IteratorUtil.filter(iterator, predicate);
    }

    /**
     * Selects elements without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
     * Behaves like OCL <tt>iterator</tt>->xselect(e | not functor.accept(e).oclIsInvalid())
     *
     * @see IteratorUtil#notNull(Iterator)
     */
    public static <Input> Iterator<Input> xselect(Iterator<Input> iterator, Predicate<? super Input> functor) {
        return select(IteratorUtil.notNull(iterator), functor);
    }

    /**
     * Rejects elements without the need for creating a temporary Collection instance.<br>
     * Behaves like OCL <tt>this</tt>->reject(e | functor.test(e))
     */
    public static <Input> Iterator<Input> reject(Iterator<Input> iterator, Predicate<? super Input> functor) {
        return select(iterator, functor.negate());
    }

    /**
     * Filters the iterator, only keeping items that are instance of the specified type. <br>
     * Behaves like OCL <tt>this</tt>->objectsOfKind(<tt>Output</tt>)
     */
    public static <Input, Output> Iterator<Output> objectsOfKind(Iterator<Input> iterator, Class<Output> outputType) {
        return xcollectOne(iterator, input -> outputType.isInstance(input) ? outputType.cast(input) : null);
    }

    /**
     * Filters the iterator, only keeping item of a specific type. <br>
     * <b>NOTE:</b> If <tt>outputType</tt> is an interface, only items are selected which directly implement the
     * interface. <br>
     * Behaves like OCL <tt>this</tt>->objectsOfType(<tt>Output</tt>)
     */
    public static <Input, Output> Iterator<Output> objectsOfType(Iterator<Input> iterator, Class<Output> outputType) {
        Function<Input, Output> functor;
        if (outputType.isInterface()) {
            functor = i -> null != i && IterableUtil.asList(i.getClass().getInterfaces()).contains(outputType)
                    ? outputType.cast(i) : null;
        } else {
            functor = i -> null != i && i.getClass().equals(outputType) ? outputType.cast(i) : null;
        }
        return xcollectOne(iterator, functor);
    }

    /**
     * Casts this iteration to a specific type. <br>
     * Behaves like OCL <tt>this</tt>->oclAsType(<tt>Output</tt>)
     */
    public static <Input, Output> Iterator<Output> asType(Iterator<Input> iterator, Class<Output> outputType) {
        return collectOne(iterator, outputType::cast);
    }

    /**
     * Collects elements without the need for creating a temporary Collection instance. Behaves like OCL
     * <tt>iterator</tt>->collect(e | functor.collectFrom(e))
     */
    public static <Input, Output> Iterator<Output> collect(Iterator<? extends Input> iterator,
            Function<Input, Iterator<? extends Output>> functor)
    {
        return IteratorUtil.flatMap(iterator, functor);
    }

    /**
     * Collects elements without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
     * Behaves like OCL <tt>iterator</tt>->xcollect(e | functor.collectFrom(e))
     *
     * @see IteratorUtil#notNull(Iterator)
     */
    public static <Input, Output> Iterator<Output> xcollect(Iterator<? extends Input> iterator,
            Function<Input, Iterator<? extends Output>> functor)
    {
        return IteratorUtil.notNull(collect(iterator, functor));
    }

    /**
     * Collects single attribute values without the need for creating a temporary Collection instance. Behaves like OCL
     * <tt>iterator</tt>->collect(e | functor.convert(e))
     *
     * @see IteratorUtil#map(Iterator, Function)
     */
    public static <Input, Output> Iterator<Output> collectOne(Iterator<? extends Input> iterator,
            Function<Input, Output> functor)
    {
        return IteratorUtil.map(iterator, functor);
    }

    /**
     * Collects single attribute values without the need for creating a temporary Collection instance, skipping
     * <tt>null</tt> values. Behaves like OCL <tt>iterator</tt>->xcollect(e | functor.convert(e))
     *
     * @see IteratorUtil#map(Iterator, Function)
     * @see IteratorUtil#notNull(Iterator)
     */
    public static <Input, Output> Iterator<Output> xcollectOne(Iterator<? extends Input> iterator,
            Function<Input, Output> functor)
    {
        return IteratorUtil.notNull(collectOne(iterator, functor));
    }

    /**
     * Behaves like OCL <tt>iterator</tt>->forAll(e | functor.test(e))
     */
    public static <Input> boolean forAll(Iterator<? extends Input> iterator, Predicate<Input> functor) {
        // Using double negation to exit query asap.
        return !exists(iterator, functor.negate());
    }

    /**
     * Behaves like QVTo <tt>iterator</tt>->exists(e | functor.test(e))
     */
    public static <Input> boolean exists(Iterator<? extends Input> iterator, Predicate<Input> functor) {
        return select(iterator, functor).hasNext();
    }

    /**
     * Behaves like QVTo <tt>iterator</tt>->any(e | functor.test(e))
     */
    public static <Input> Input any(Iterator<Input> iterator, Predicate<? super Input> functor) {
        return IteratorUtil.safeNext(select(iterator, functor));
    }

    /**
     * Returns an iterator which pairs the elements of <tt>iteratorA</tt> and <tt>iteratorB</tt> such that Pair{n}.a =
     * iteratorA{n} and Pair{n}.b = iteratorB{n}. The amount of elements in the resulting iterator will be deduced from
     * either <tt>iteratorA</tt> or <tt>iteratorB</tt> depending on which of both produces the least amount of elements.
     */
    public static <InputA, InputB> Iterator<Pair<InputA, InputB>> zip(Iterator<? extends InputA> iteratorA,
            Iterator<? extends InputB> iteratorB)
    {
        return new ProcessingIterator<Pair<InputA, InputB>>() {
            @Override
            protected boolean toNext() {
                if (iteratorA.hasNext() && iteratorB.hasNext()) {
                    return setNext(Pair.of(iteratorA.next(), iteratorB.next()));
                } else {
                    return done();
                }
            }
        };
    }

    /**
     * Returns an iterator which pairs the elements of <tt>iteratorA</tt> and <tt>iteratorB</tt> such that Pair{n}.a =
     * iteratorA{n} and Pair{n}.b = iteratorB{n + offset}. The amount of elements in the resulting iterator will be
     * deduced from either <tt>iteratorA</tt> or <tt>iteratorB</tt> depending on <tt>includeOneSideOnly</tt> which of
     * both produces the least/most amount of elements.
     */
    public static <InputA, InputB> Iterator<Pair<InputA, InputB>> zip(Iterator<? extends InputA> iteratorA,
            Iterator<? extends InputB> iteratorB, boolean includeOneSideOnly, int offset)
    {
        return new ProcessingIterator<Pair<InputA, InputB>>() {
            int indexA = offset > 0 ? 0 : offset;

            int indexB = offset > 0 ? -offset : 0;

            @Override
            protected boolean toNext() {
                while (iteratorA.hasNext() || iteratorB.hasNext()) {
                    indexA++;
                    indexB++;

                    boolean gotA = false;
                    InputA a = null;
                    if (indexA > 0 && iteratorA.hasNext()) {
                        a = iteratorA.next();
                        gotA = true;
                    }
                    boolean gotB = false;
                    InputB b = null;
                    if (indexB > 0 && iteratorB.hasNext()) {
                        b = iteratorB.next();
                        gotB = true;
                    }
                    if (includeOneSideOnly || (gotA && gotB)) {
                        return setNext(Pair.of(a, b));
                    }
                }
                return done();
            }
        };
    }

    /**
     * Returns an iterator which pairs the elements of <tt>iteratorA</tt> and <tt>iteratorB</tt> such that <br>
     * <code>comparator.compare(Pair.a, Pair.b) == 0</code>.<br>
     * For elements which only occur in <tt>iteratorA</tt> or <tt>iteratorB</tt> an incomplete pair will be returned
     * (meaning respectively either a or b will be <tt>null</tt>)<br>
     * <br>
     * <b>NOTE:</b> Both this <tt>iteratorA</tt> as <tt>iteratorB</tt> should be sorted with respect to the
     * <tt>comparator</tt>
     */
    public static <InputA, InputB> Iterator<Pair<InputA, InputB>> zip(Iterator<? extends InputA> iteratorA,
            Iterator<? extends InputB> iteratorB, ToIntBiFunction<? super InputA, ? super InputB> comparator)
    {
        return new ProcessingIterator<Pair<InputA, InputB>>() {
            private final PeekingIterator<? extends InputA> left = IteratorUtil.toPeeking(iteratorA);

            private final PeekingIterator<? extends InputB> right = IteratorUtil.toPeeking(iteratorB);

            @Override
            protected boolean toNext() {
                if (left.hasNext() && right.hasNext()) {
                    final int compare = comparator.applyAsInt(left.peek(), right.peek());
                    if (compare == 0) {
                        return setNext(Pair.of(left.next(), right.next()));
                    } else if (compare < 0) {
                        return setNext(Pair.of(left.next(), null));
                    } else /* if (compare > 0) */ {
                        return setNext(Pair.of(null, right.next()));
                    }
                } else if (left.hasNext()) {
                    return setNext(Pair.of(left.next(), null));
                } else if (right.hasNext()) {
                    return setNext(Pair.of(null, right.next()));
                } else {
                    return done();
                }
            }
        };
    }

    /**
     * Behaves like QVTo <tt>iteratorA</tt>->product(<tt>iteratorB</tt>)
     */
    public static <InputA, InputB> Iterator<Pair<InputA, InputB>> product(Iterator<? extends InputA> iteratorA,
            Iterator<? extends InputB> iteratorB)
    {
        BufferedIterator<? extends InputB> bufferedInputB = new BufferedIterator<>(iteratorB);
        bufferedInputB.mark();

        return new ProcessingIterator<Pair<InputA, InputB>>() {
            private boolean initialized = false;

            private InputA currentInputA = null;

            @Override
            protected boolean toNext() {
                if (!initialized) {
                    initialized = true;
                    if (iteratorA.hasNext()) {
                        currentInputA = iteratorA.next();
                    } else {
                        return done();
                    }
                }
                if (!bufferedInputB.hasNext()) {
                    if (iteratorA.hasNext()) {
                        currentInputA = iteratorA.next();
                        bufferedInputB.reset();
                        if (!bufferedInputB.hasNext()) {
                            return done();
                        }
                    } else {
                        return done();
                    }
                }
                return setNext(Pair.of(currentInputA, bufferedInputB.next()));
            }
        };
    }

    /**
     * Returns a map for which the Map.values are the given elements in the given order, and each key is the product of
     * invoking a supplied function computeKeys on its corresponding value. If the function produces the same key for
     * different values, the last one will be contained in the map.
     */
    public static <Input, K> Map<K, Input> toMap(Iterator<Input> iterator, Function<? super Input, K> computeKeys) {
        return toMap(iterator, computeKeys, v -> v);
    }

    /**
     * Returns a map for which the Map.values are the product of invoking supplied function computeValues on input
     * iterable elements, and each key is the product of invoking a supplied function computeKeys on same elements. If
     * the function produces the same key for different values, the last one will be contained in the map.
     */
    public static <Input, K, V> Map<K, V> toMap(Iterator<Input> iterator, Function<? super Input, K> computeKeys,
            Function<? super Input, V> computeValues)
    {
        Map<K, V> result = new LinkedHashMap<>();
        while (iterator.hasNext()) {
            Input next = iterator.next();
            result.put(computeKeys.apply(next), computeValues.apply(next));
        }
        return result;
    }

    /**
     * Returns a map for which the Map.values is a collection of lists, where the elements in the list will appear in
     * the order as they appeared in the iterable. Each key is the product of invoking the supplied function computeKeys
     * on its corresponding value. So a key of that map groups a list of values for which the function produced exactly
     * that key.
     */
    public static <Input, K> Map<K, List<Input>> groupBy(Iterator<Input> iterator,
            Function<? super Input, K> computeKeys)
    {
        Map<K, List<Input>> result = new LinkedHashMap<>();
        while (iterator.hasNext()) {
            Input next = iterator.next();
            result.computeIfAbsent(computeKeys.apply(next), k -> new LinkedList<Input>()).add(next);
        }
        return result;
    }

    /**
     * Returns a map for which the Map.Entry.value is the reduced value of all values found for a key. Each key is the
     * product of invoking the supplied function computeKeys on its corresponding value.
     */
    public static <Input, K, V> Map<K, V> groupByAndReduce(Iterator<Input> iterator,
            Function<? super Input, K> computeKeys, BiFunction<? super Input, V, ? extends V> computeValues)
    {
        Map<K, V> result = new LinkedHashMap<>();
        while (iterator.hasNext()) {
            Input next = iterator.next();
            result.compute(computeKeys.apply(next), (A, B) -> computeValues.apply(next, B));
        }
        return result;
    }

    /**
     * Groups a source of inputs into groups of equivalent inputs.
     *
     * @param <Input> type of input
     * @param source source of inputs
     * @param equivalence defines the equivalence relation of two inputs, e.g. {@link Objects#equals(Object, Object)}.
     * @return a list of lists where every list contains equivalent inputs.
     * @see #groupBy(Iterator, Comparator, BiPredicate)
     */
    public static <Input> List<List<Input>> groupBy(Iterator<Input> source,
            BiPredicate<? super Input, ? super Input> equivalence)
    {
        return groupBy(source, null, equivalence);
    }

    /**
     * Groups a source of inputs into groups of equivalent inputs.
     *
     * @param <Input> type of input
     * @param source source of inputs
     * @param comparator If not {@code null}, sorts the groups for performance reasons, e.g. ascending list size when
     *     all groups are expected to contain the same amount of elements or descending list size when a few groups are
     *     expected to contain most of the elements.
     * @param equivalence defines the equivalence relation of two inputs, e.g. {@link Objects#equals(Object, Object)}.
     * @return a list of lists where every list contains equivalent inputs.
     */
    public static <Input> List<List<Input>> groupBy(Iterator<Input> source, Comparator<? super List<Input>> comparator,
            BiPredicate<? super Input, ? super Input> equivalence)
    {
        List<List<Input>> groups = new LinkedList<>();
        while (source.hasNext()) {
            Input element = source.next();
            List<Input> group = any(groups.iterator(),
                    (List<Input> g) -> equivalence.test(element, IterableUtil.first(g)));
            if (group == null) {
                group = new LinkedList<>();
                groups.add(group);
            }
            group.add(element);

            if (comparator != null) {
                groups.sort(comparator);
            }
        }
        return groups;
    }

    /**
     * Segments {@code source} into groups, based on the {@code predicate}.
     *
     * @param <Input> type of input
     * @param source source of inputs
     * @param predicate defines where to segment, based on two subsequent entries of the {@code source}.
     * @return an iterator of lists (segments).
     */
    public static <Input> Iterator<List<Input>> segment(Iterator<Input> source,
            BiPredicate<? super Input, ? super Input> predicate)
    {
        return new SegmentIterator<Input>(source, predicate);
    }

    private static <T> Iterator<T> toMany(final T element) {
        return element == null ? Collections.emptyIterator() : IteratorUtil.singletonIterator(element);
    }
}
