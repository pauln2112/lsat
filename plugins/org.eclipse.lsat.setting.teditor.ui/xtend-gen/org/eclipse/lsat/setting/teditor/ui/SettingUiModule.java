/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.setting.teditor.ui;

import org.eclipse.lsat.setting.teditor.ui.AbstractSettingUiModule;
import org.eclipse.lsat.setting.teditor.ui.hover.SettingHoverProvider;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.editor.hover.IEObjectHoverProvider;

/**
 * Use this class to register components to be used within the IDE.
 */
@SuppressWarnings("all")
public class SettingUiModule extends AbstractSettingUiModule {
  public SettingUiModule(final AbstractUIPlugin plugin) {
    super(plugin);
  }
  
  public Class<? extends IEObjectHoverProvider> bindIEObjectHoverProvider() {
    return SettingHoverProvider.class;
  }
}
