/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.statespace;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.games.ratio.solvers.policy.RatioGamePolicyIteration;

/**
 * @author Bram van der Sanden
 */
@SuppressWarnings("rawtypes")
public class MaxPlusGame implements RatioGamePolicyIteration<Configuration, Transition, Value> {
    private static final long serialVersionUID = 1L;

    private final Set<Configuration> v0;

    private final Set<Configuration> v1;

    private final Set<Configuration> vertices;

    private final Map<Configuration, Integer> vertexIds;

    private final Set<Transition> transitions;

    private final Map<Configuration, Set<Transition>> outgoingMap;

    private final Map<Configuration, Set<Transition>> incomingMap;

    private final Map<Transition, Value> weightMap1;

    private final Map<Transition, Value> weightMap2;

    private final Value maxWeight;

    /**
     * Generate a max-plus game graph given a max-plus state space.
     *
     * @param stateSpace
     * @param controllableEvents set of controllable event names
     * @param uncontrollableEvents set of uncontrollable event names
     */
    public MaxPlusGame(MaxPlusStateSpace stateSpace, Set<String> controllableEvents, Set<String> uncontrollableEvents) {
        v0 = new HashSet<>();
        v1 = new HashSet<>();
        vertexIds = new HashMap<>();

        // Vertices.
        vertices = stateSpace.getConfigurations();

        // Edges.
        transitions = new HashSet<>();
        outgoingMap = new HashMap<>();
        incomingMap = new HashMap<>();
        weightMap1 = new HashMap<>();
        weightMap2 = new HashMap<>();

        for (Configuration c: vertices) {
            for (Transition t: stateSpace.outgoingEdgesOf(c)) {
                addIncoming(t.getTarget(), t);
                addOutgoing(c, t);
                weightMap1.put(t, t.getDuration());
                weightMap2.put(t, t.getReward());
            }
        }

        int id = 0;
        for (Configuration c: vertices) {
            if (allInSet(c, controllableEvents)) {
                v0.add(c);
            } else if (allInSet(c, uncontrollableEvents)) {
                v1.add(c);
            } else {
                System.out.println("Configuration found with two types of outgoing edges! " + c.toString());
            }
            vertexIds.put(c, id);
            id += 1;
        }

        Value max1 = weightMap1.values().stream().reduce((i, j) -> i.max(j)).get();
        Value max2 = weightMap2.values().stream().reduce((i, j) -> i.max(j)).get();

        maxWeight = Value.max(max1.abs(), max2.abs());
    }

    private boolean allInSet(Configuration c, Set<String> eventSet) {
        return outgoingEdgesOf(c).stream().noneMatch((t) -> (!eventSet.contains(t.getEvent())));
    }

    private void addIncoming(Configuration c, Transition t) {
        if (incomingMap.containsKey(c)) {
            incomingMap.get(c).add(t);
        } else {
            Set<Transition> set = new HashSet<>();
            set.add(t);
            incomingMap.put(c, set);
        }
    }

    private void addOutgoing(Configuration c, Transition t) {
        if (outgoingMap.containsKey(c)) {
            outgoingMap.get(c).add(t);
        } else {
            Set<Transition> set = new HashSet<>();
            set.add(t);
            outgoingMap.put(c, set);
        }
    }

    @Override
    public Set<Configuration> getV0() {
        return v0;
    }

    @Override
    public Set<Configuration> getV1() {
        return v1;
    }

    @Override
    public Set<Configuration> getVertices() {
        return vertices;
    }

    @Override
    public Set<Transition> getEdges() {
        return transitions;
    }

    @Override
    public Collection<Transition> incomingEdgesOf(Configuration v) {
        return incomingMap.getOrDefault(v, Collections.emptySet());
    }

    @Override
    public Collection<Transition> outgoingEdgesOf(Configuration v) {
        return outgoingMap.getOrDefault(v, Collections.emptySet());
    }

    @Override
    public Configuration getEdgeSource(Transition e) {
        return e.getSource();
    }

    @Override
    public Configuration getEdgeTarget(Transition e) {
        return e.getTarget();
    }

    @Override
    public Transition getEdge(Configuration source, Configuration target) {
        for (Transition t: outgoingMap.get(source)) {
            if (t.getTarget().equals(target)) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Value getWeight1(Transition edge) {
        return weightMap1.get(edge);
    }

    @Override
    public Value getWeight2(Transition edge) {
        return weightMap2.get(edge);
    }

    @Override
    public Value getMaxAbsValue() {
        return maxWeight;
    }

    @Override
    public Integer getId(Configuration vertex) {
        return vertexIds.get(vertex);
    }
}
