/**
 */
package org.eclipse.lsat.common.mpt.util;

import org.eclipse.lsat.common.mpt.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.mpt.MPTPackage
 * @generated
 */
public class MPTSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MPTPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPTSwitch() {
		if (modelPackage == null) {
			modelPackage = MPTPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MPTPackage.VECTOR: {
				Vector vector = (Vector)theEObject;
				T result = caseVector(vector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.MATRIX: {
				Matrix matrix = (Matrix)theEObject;
				T result = caseMatrix(matrix);
				if (result == null) result = caseNamedObject(matrix);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.MAX_PLUS_SPECIFICATION: {
				MaxPlusSpecification maxPlusSpecification = (MaxPlusSpecification)theEObject;
				T result = caseMaxPlusSpecification(maxPlusSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.GRAPH: {
				Graph<?, ?> graph = (Graph<?, ?>)theEObject;
				T result = caseGraph(graph);
				if (result == null) result = caseNamedObject(graph);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.FSM: {
				FSM fsm = (FSM)theEObject;
				T result = caseFSM(fsm);
				if (result == null) result = caseGraph(fsm);
				if (result == null) result = caseNamedObject(fsm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.FSM_STATE: {
				FSMState fsmState = (FSMState)theEObject;
				T result = caseFSMState(fsmState);
				if (result == null) result = caseVertex(fsmState);
				if (result == null) result = caseNamedObject(fsmState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.FSM_TRANSITION: {
				FSMTransition fsmTransition = (FSMTransition)theEObject;
				T result = caseFSMTransition(fsmTransition);
				if (result == null) result = caseEdge(fsmTransition);
				if (result == null) result = caseNamedObject(fsmTransition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.VERTEX: {
				Vertex vertex = (Vertex)theEObject;
				T result = caseVertex(vertex);
				if (result == null) result = caseNamedObject(vertex);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.EDGE: {
				Edge edge = (Edge)theEObject;
				T result = caseEdge(edge);
				if (result == null) result = caseNamedObject(edge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.MPS: {
				MPS mps = (MPS)theEObject;
				T result = caseMPS(mps);
				if (result == null) result = caseGraph(mps);
				if (result == null) result = caseNamedObject(mps);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.MPS_CONFIGURATION: {
				MPSConfiguration mpsConfiguration = (MPSConfiguration)theEObject;
				T result = caseMPSConfiguration(mpsConfiguration);
				if (result == null) result = caseVertex(mpsConfiguration);
				if (result == null) result = caseNamedObject(mpsConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.MPS_TRANSITION: {
				MPSTransition mpsTransition = (MPSTransition)theEObject;
				T result = caseMPSTransition(mpsTransition);
				if (result == null) result = caseEdge(mpsTransition);
				if (result == null) result = caseNamedObject(mpsTransition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.MPA: {
				MPA mpa = (MPA)theEObject;
				T result = caseMPA(mpa);
				if (result == null) result = caseGraph(mpa);
				if (result == null) result = caseNamedObject(mpa);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.MPA_STATE: {
				MPAState mpaState = (MPAState)theEObject;
				T result = caseMPAState(mpaState);
				if (result == null) result = caseVertex(mpaState);
				if (result == null) result = caseNamedObject(mpaState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.MPA_TRANSITION: {
				MPATransition mpaTransition = (MPATransition)theEObject;
				T result = caseMPATransition(mpaTransition);
				if (result == null) result = caseEdge(mpaTransition);
				if (result == null) result = caseNamedObject(mpaTransition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.NAMED_OBJECT: {
				NamedObject namedObject = (NamedObject)theEObject;
				T result = caseNamedObject(namedObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.ROW_VECTOR: {
				RowVector rowVector = (RowVector)theEObject;
				T result = caseRowVector(rowVector);
				if (result == null) result = caseVector(rowVector);
				if (result == null) result = caseNamedObject(rowVector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.COLUMN_VECTOR: {
				ColumnVector columnVector = (ColumnVector)theEObject;
				T result = caseColumnVector(columnVector);
				if (result == null) result = caseVector(columnVector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MPTPackage.EVENT: {
				Event event = (Event)theEObject;
				T result = caseEvent(event);
				if (result == null) result = caseNamedObject(event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVector(Vector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Matrix</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Matrix</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMatrix(Matrix object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Max Plus Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Max Plus Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaxPlusSpecification(MaxPlusSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <V, E> T caseGraph(Graph<V, E> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSM(FSM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMState(FSMState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSMTransition(FSMTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertex</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertex</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertex(Vertex object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEdge(Edge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MPS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MPS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPS(MPS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MPS Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MPS Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPSConfiguration(MPSConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MPS Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MPS Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPSTransition(MPSTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MPA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MPA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPA(MPA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MPA State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MPA State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPAState(MPAState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MPA Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MPA Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPATransition(MPATransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedObject(NamedObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Row Vector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Row Vector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRowVector(RowVector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Column Vector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Column Vector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseColumnVector(ColumnVector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvent(Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MPTSwitch
