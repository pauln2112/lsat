/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend.annotations

import com.google.common.collect.BiMap
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.Target
import java.util.Map
import org.eclipse.xtend.lib.macro.AbstractFieldProcessor
import org.eclipse.xtend.lib.macro.Active
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.Visibility

import static extension org.eclipse.lsat.common.xtend.annotations.TypeReferenceUtil.getCommonBaseTypes

@Retention(SOURCE)
@Target(ElementType.FIELD)
@Active(IntermediatePropertyCompilationParticipant)
annotation IntermediateProperty {
    Class<?>[] value = #[Object]
    String opposite = ''
    int expectedSize = -1
}

class IntermediatePropertyCompilationParticipant extends AbstractFieldProcessor {
    override doTransform(MutableFieldDeclaration field, extension TransformationContext context) {
        val _annotation = field.findAnnotation(IntermediateProperty.findTypeGlobally)
        val ownerTypes = _annotation.getClassArrayValue('value')
        val oppositePropertyName = _annotation.getStringValue('opposite')
        val expectedSize = _annotation.getIntValue('expectedSize')
        val ownerCommonType = ownerTypes.commonBaseTypes.head;

        field.markAsRead // We processed this field
        if (field.type.inferred) {
            field.addError('Type inference is not supported by @IntermediateProperty')
            return
        } else if (field.type.primitive) {
            field.addError("Fields with primitives are not supported by @IntermediateProperty")
            return
        }
        if (field.static) {
            val swAnn = field.findAnnotation(SuppressWarnings.findTypeGlobally)
            if (swAnn === null || !swAnn.getStringArrayValue('value').contains('static'))
                field.addWarning("Note that this property will be statically available")
        }
        if (!oppositePropertyName.nullOrEmpty && !ownerTypes.contains(ownerCommonType)) {
            field.addWarning('''«field.type».«oppositePropertyName» property type is reduced to «ownerCommonType.name»''')
        }
        
        val propertyType = field.type
        val propertyName = field.simpleName
        val propertyMap = '''_IntermediateProperty_«propertyName»'''
        val propertyDefault = if (field.initializer !== null) {
            field.declaringType.addField('''_DEFAULT«FOR type : ownerTypes»_«type.simpleName.toUpperCase»«ENDFOR»_«propertyName.toUpperCase»''') [
                visibility = Visibility.PRIVATE
                static = true
                final = true
                type = field.type
                initializer = field.initializer
            ]
        }
        
        for (ownerType : ownerTypes) {
            // add a getter method
            field.declaringType.addMethod('get' + propertyName.toFirstUpper) [
                visibility = field.visibility
                static = field.static
                addParameter('owner', ownerType)
                returnType = propertyType
                body = if (propertyDefault === null) '''
                    return «propertyMap».get(owner);
                ''' else '''
                    «propertyType» value = «propertyMap».get(owner);
                    return value == null ? «propertyDefault.simpleName» : value;
                '''
                primarySourceElement = field
            ]
            
            // add a setter method
            field.declaringType.addMethod('set' + propertyName.toFirstUpper) [
                visibility = field.visibility
                static = field.static
                addParameter('owner', ownerType)
                addParameter('value', propertyType)
                body = '''
                    «IF propertyDefault === null»
                    if (value == null) {
                    «ELSE»
                    if (value == «propertyDefault.simpleName») {
                    «ENDIF»
                        «propertyMap».remove(owner);
                    } else {
                        «propertyMap».put(owner, value);
                    }
                '''
                primarySourceElement = field
            ]
        }
        
        if (!oppositePropertyName.nullOrEmpty) {
            // add an opposite getter method
            field.declaringType.addMethod('get' + oppositePropertyName.toFirstUpper) [
                visibility = field.visibility
                static = field.static
                addParameter('owner', propertyType)
                returnType = ownerCommonType
                body = '''
                    return «propertyMap».inverse().get(owner);
                '''
                primarySourceElement = field
            ]
            
            // add an opposite setter method
            field.declaringType.addMethod('set' + oppositePropertyName.toFirstUpper) [
                visibility = field.visibility
                static = field.static
                addParameter('owner', propertyType)
                addParameter('value', ownerCommonType)
                body = '''
                    if (value == null) {
                        «propertyMap».inverse().remove(owner);
                    } else {
                        «propertyMap».inverse().put(owner, value);
                    }
                '''
                primarySourceElement = field
            ]
        }
        
        field.declaringType.addMethod('dispose' + propertyName.toFirstUpper) [
            visibility = Visibility.PRIVATE
            static = field.static
            body = '''
                «propertyMap».clear();
            '''
            primarySourceElement = field
        ]

        // Rewrite the field to a java.util.Map
        field.markAsRead
        field.simpleName = propertyMap;
        val sizeStr = if (expectedSize > 0) expectedSize.toString else ''
        if (!oppositePropertyName.nullOrEmpty) {
            field.type = newTypeReference(BiMap, ownerCommonType, field.type)
            field.initializer = '''com.google.common.collect.HashBiMap.create(«sizeStr»)'''
        } else {
            field.type = newTypeReference(Map, ownerCommonType, field.type)
            field.initializer = '''new java.util.WeakHashMap<>(«sizeStr»)'''
        }
    }
}
