/**
 */
package machine.impl;

import java.util.Collection;

import machine.ActionType;
import machine.Axis;
import machine.MachinePackage;
import machine.PeripheralType;
import machine.SetPoint;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Peripheral Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.PeripheralTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link machine.impl.PeripheralTypeImpl#getConversion <em>Conversion</em>}</li>
 *   <li>{@link machine.impl.PeripheralTypeImpl#getAxes <em>Axes</em>}</li>
 *   <li>{@link machine.impl.PeripheralTypeImpl#getSetPoints <em>Set Points</em>}</li>
 *   <li>{@link machine.impl.PeripheralTypeImpl#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PeripheralTypeImpl extends MinimalEObjectImpl.Container implements PeripheralType {
	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected String name = NAME_EDEFAULT;

	/**
     * The default value of the '{@link #getConversion() <em>Conversion</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getConversion()
     * @generated
     * @ordered
     */
	protected static final String CONVERSION_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getConversion() <em>Conversion</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getConversion()
     * @generated
     * @ordered
     */
	protected String conversion = CONVERSION_EDEFAULT;

	/**
     * The cached value of the '{@link #getAxes() <em>Axes</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getAxes()
     * @generated
     * @ordered
     */
	protected EList<Axis> axes;

	/**
     * The cached value of the '{@link #getSetPoints() <em>Set Points</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSetPoints()
     * @generated
     * @ordered
     */
	protected EList<SetPoint> setPoints;

	/**
     * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActions()
     * @generated
     * @ordered
     */
	protected EList<ActionType> actions;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected PeripheralTypeImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.PERIPHERAL_TYPE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
        return name;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.PERIPHERAL_TYPE__NAME, oldName, name));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getConversion() {
        return conversion;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setConversion(String newConversion) {
        String oldConversion = conversion;
        conversion = newConversion;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.PERIPHERAL_TYPE__CONVERSION, oldConversion, conversion));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Axis> getAxes() {
        if (axes == null)
        {
            axes = new EObjectContainmentEList<Axis>(Axis.class, this, MachinePackage.PERIPHERAL_TYPE__AXES);
        }
        return axes;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<SetPoint> getSetPoints() {
        if (setPoints == null)
        {
            setPoints = new EObjectContainmentEList<SetPoint>(SetPoint.class, this, MachinePackage.PERIPHERAL_TYPE__SET_POINTS);
        }
        return setPoints;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<ActionType> getActions() {
        if (actions == null)
        {
            actions = new EObjectContainmentEList<ActionType>(ActionType.class, this, MachinePackage.PERIPHERAL_TYPE__ACTIONS);
        }
        return actions;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL_TYPE__AXES:
                return ((InternalEList<?>)getAxes()).basicRemove(otherEnd, msgs);
            case MachinePackage.PERIPHERAL_TYPE__SET_POINTS:
                return ((InternalEList<?>)getSetPoints()).basicRemove(otherEnd, msgs);
            case MachinePackage.PERIPHERAL_TYPE__ACTIONS:
                return ((InternalEList<?>)getActions()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL_TYPE__NAME:
                return getName();
            case MachinePackage.PERIPHERAL_TYPE__CONVERSION:
                return getConversion();
            case MachinePackage.PERIPHERAL_TYPE__AXES:
                return getAxes();
            case MachinePackage.PERIPHERAL_TYPE__SET_POINTS:
                return getSetPoints();
            case MachinePackage.PERIPHERAL_TYPE__ACTIONS:
                return getActions();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL_TYPE__NAME:
                setName((String)newValue);
                return;
            case MachinePackage.PERIPHERAL_TYPE__CONVERSION:
                setConversion((String)newValue);
                return;
            case MachinePackage.PERIPHERAL_TYPE__AXES:
                getAxes().clear();
                getAxes().addAll((Collection<? extends Axis>)newValue);
                return;
            case MachinePackage.PERIPHERAL_TYPE__SET_POINTS:
                getSetPoints().clear();
                getSetPoints().addAll((Collection<? extends SetPoint>)newValue);
                return;
            case MachinePackage.PERIPHERAL_TYPE__ACTIONS:
                getActions().clear();
                getActions().addAll((Collection<? extends ActionType>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL_TYPE__NAME:
                setName(NAME_EDEFAULT);
                return;
            case MachinePackage.PERIPHERAL_TYPE__CONVERSION:
                setConversion(CONVERSION_EDEFAULT);
                return;
            case MachinePackage.PERIPHERAL_TYPE__AXES:
                getAxes().clear();
                return;
            case MachinePackage.PERIPHERAL_TYPE__SET_POINTS:
                getSetPoints().clear();
                return;
            case MachinePackage.PERIPHERAL_TYPE__ACTIONS:
                getActions().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL_TYPE__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case MachinePackage.PERIPHERAL_TYPE__CONVERSION:
                return CONVERSION_EDEFAULT == null ? conversion != null : !CONVERSION_EDEFAULT.equals(conversion);
            case MachinePackage.PERIPHERAL_TYPE__AXES:
                return axes != null && !axes.isEmpty();
            case MachinePackage.PERIPHERAL_TYPE__SET_POINTS:
                return setPoints != null && !setPoints.isEmpty();
            case MachinePackage.PERIPHERAL_TYPE__ACTIONS:
                return actions != null && !actions.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", conversion: ");
        result.append(conversion);
        result.append(')');
        return result.toString();
    }

} //PeripheralTypeImpl
