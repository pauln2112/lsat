/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.graph.Graph;

/**
 * Topological sort using a DFS.
 *
 * @author Bram van der Sanden
 */
public final class TopologicalSort {
    private TopologicalSort() {
        // Empty
    }

    /**
     * Return a topological sort on the given input graph. If the graph is not acyclic, an exception is thrown.
     *
     * @param graph input graph
     * @param <V> vertex type
     * @param <E> edge type
     * @return a topological sorted vertex list
     * @throws CycleFoundException thrown if a cycle is found
     */
    public static <V, E> List<V> topologicalSort(Graph<V, E> graph) throws CycleFoundException {
        List<V> l = new LinkedList<>();
        Set<V> unmarked = new HashSet<>(graph.getVertices());
        Set<V> tempMarked = new HashSet<>();
        while (!unmarked.isEmpty()) {
            V v = unmarked.iterator().next();
            visit(graph, l, unmarked, tempMarked, v);
        }
        return l;
    }

    /**
     * Recursively visit the nodes using a DFS.
     *
     * @param graph input graph
     * @param l list of topologically sorted vertices
     * @param unmarked set of unmarked vertices
     * @param tempMarked set of temporarily marked vertices
     * @param v current vertex
     * @param <V> vertex type
     * @param <E> edge type
     * @throws CycleFoundException thrown if a cycle is found
     */
    private static <V, E> void visit(Graph<V, E> graph, List<V> l, Set<V> unmarked, Set<V> tempMarked, V v)
            throws CycleFoundException
    {
        if (tempMarked.contains(v)) {
            throw new CycleFoundException();
        }

        if (unmarked.contains(v)) {
            tempMarked.add(v);
            for (E edge: graph.outgoingEdgesOf(v)) {
                visit(graph, l, unmarked, tempMarked, graph.getEdgeTarget(edge));
            }
            // Mark v permanently.
            unmarked.remove(v);
            // Unmark v temporarily.
            tempMarked.remove(v);
            // Add n to head of L.
            l.add(0, v);
        }
    }
}
