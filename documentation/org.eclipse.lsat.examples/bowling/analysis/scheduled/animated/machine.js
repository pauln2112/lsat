/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
var ballSize = 40;
var ballTemplate;

function createBall() {
  if (ballTemplate == undefined) {
    ballTemplate = new Path.Circle(new Point(0, 0), ballSize);
    ballTemplate.fillColor = {
      gradient : {
        stops : [ [ 'Red', 0.2 ], [ 'Crimson', 0.35 ], [ 'Black', 1 ] ],
        radial : true
      },
      origin : ballTemplate.bounds.topLeft,
      destination : ballTemplate.bounds.rightCenter
    };
    return ballTemplate;
  } else {
    var copy = ballTemplate.clone();
    copy.bringToFront();
    return copy;
  }
}

function rail(anchor1, anchor2) {
  var rail = new Path.Rectangle({
    topLeft : new Point(anchor1.X - 75, anchor1.Y - 42),
    bottomRight : new Point(anchor2.X + 75, anchor1.Y - 32),
    fillColor : 'SteelBlue',
    // Set the shadow color of the circle to RGB black:
    shadowColor : new Color(0, 0, 0),
    // Set the shadow blur radius to 10:
    shadowBlur : 10,
    // Offset the shadow by { x: 2, y: 2 }
    shadowOffset : new Point(2, 2)
  });
}

function container(anchor) {
  var container = new CompoundPath({
    children : [ new Path.Rectangle(new Point(anchor.X - 75, anchor.Y), new Size(150, (ballSize * 6) + 25)),
        new Path.Rectangle(new Point(anchor.X - 65, anchor.Y), new Size(130, 80)) ],
    fillRule : 'evenodd',
    fillColor : 'SteelBlue',
    // Set the shadow color of the circle to RGB black:
    shadowColor : new Color(0, 0, 0),
    // Set the shadow blur radius to 10:
    shadowBlur : 10,
    // Offset the shadow by { x: 2, y: 2 }
    shadowOffset : new Point(2, 2)
  });
  var ball = createBall();
  ball.position = new Point(anchor.X - 20, anchor.Y + ballSize)
  ball = createBall();
  ball.position = new Point(anchor.X + 24, anchor.Y + ballSize)
  ball = createBall();
  ball.position = new Point(anchor.X, anchor.Y + ballSize)
}

function drillOn(drill, percentageComplete) {
  if (percentageComplete < 1) {
    drill.fillColor = 'DimGray';
  } else {
    drill.fillColor = 'Black';
  }
}
  
function drillOff(drill, percentageComplete) {
  if (percentageComplete < 1) {
    drill.fillColor = 'DimGray';
  } else {
    drill.fillColor = {
      gradient : {
        stops : [ 'DimGray', 'Black', 'DimGray', 'Black', 'DimGray', 'Black', 'DimGray', 'Black' ]
      },
      origin : drill.bounds.topLeft,
      destination : drill.bounds.bottomRight
    };
  }
}

var Conditioner_CD = {
  plate : null,
  ducts : null,
  init : function(anchor) {
    var table = new CompoundPath({
      children : [ new Path.Rectangle(new Point(anchor.X - 75, anchor.Y + (ballSize * 2)), new Size(150, 30)),
                   new Path.Rectangle(new Point(anchor.X - 30, anchor.Y + (ballSize * 2) + 30), new Size(60, (ballSize * 4) - 5))],
      fillColor : 'SteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 10:
      shadowBlur : 10,
      // Offset the shadow by { x: 2, y: 2 }
      shadowOffset : new Point(2, 2)
    });
    this.plate = new Path.Rectangle(new Point(anchor.X - 75, anchor.Y + (ballSize * 2)), new Size(150, 30));
    this.plate.fillColor = new Color(0, 1, 1);
    this.ducts = new Path.Circle(new Point(500, 500), 9);
    this.ducts.fillColor = new Color(0, 1, 1);
    var ductSymbol = new Symbol(this.ducts);
    for (var i = 0; i < 6; i++) {
      var x = (anchor.X - 61) + i * 24;
      var y = anchor.Y + (ballSize * 2) + 15;
      ductSymbol.place(new Point(x, y));
    }
  },
  condition : function(percentageComplete, heater) {
    if (heater)
      this.ducts.fillColor = 'red';
    else
      this.ducts.fillColor = new Color(0, 1, 1);
    this.plate.fillColor = new Color(percentageComplete, 1 - percentageComplete, 1 - percentageComplete);
  }
}

var Conditioner_CL = {
  ball : null,
  init : function(anchor) {
    this.ball = createBall();
    this.ball.visible = false;
    this.ball.position = new Point(anchor.X, anchor.Y + ballSize);
  },
  clamp : function(percentageComplete) {
    this.ball.visible = true;
  },
  unclamp : function(percentageComplete) {
    this.ball.visible = false;
  }
}

var Drill_IndexFinger_DR = {
  on: function(percentageComplete) {
    drillOn(DrillTable_CA.drill2, percentageComplete);
  }, 
  off: function(percentageComplete) {
    drillOff(DrillTable_CA.drill2, percentageComplete);
  }, 
  move: function(from, to, percentageComplete) {
    var newZ = from.Z + (to.Z - from.Z) * percentageComplete;
    DrillTable_CA.drill2.position.y = DrillTable_CA.drill2.lockY - newZ;
  }
}

var Drill_MiddleFinger_DR = {
  on: function(percentageComplete) {
    drillOn(DrillTable_CA.drill3, percentageComplete);
  }, 
  off: function(percentageComplete) {
    drillOff(DrillTable_CA.drill3, percentageComplete);
  }, 
  move: function(from, to, percentageComplete) {
    var newZ = from.Z + (to.Z - from.Z) * percentageComplete;
    DrillTable_CA.drill3.position.y = DrillTable_CA.drill3.lockY - newZ;
  }
}

var Drill_Thumb_DR = {
  on: function(percentageComplete) {
    drillOn(DrillTable_CA.drill1, percentageComplete);
  }, 
  off: function(percentageComplete) {
    drillOff(DrillTable_CA.drill1, percentageComplete);
  }, 
  move: function(from, to, percentageComplete) {
    var newZ = from.Z + (to.Z - from.Z) * percentageComplete;
    DrillTable_CA.drill1.position.y = DrillTable_CA.drill1.lockY - newZ;
  }
}

var DrillTable_CA = {
  carousel : null,
  center : null,
  drill1 : null,
  drill2 : null,
  drill3 : null,
  init: function(anchor) {
    const maxR = (ballSize * 2);
    const minR = Math.sqrt(Math.pow(maxR, 2) - Math.pow(maxR/2, 2));
    center = new Point(anchor.X, anchor.Y + (ballSize * 2) + 10 + maxR);
    const shaft1 = new Path.Rectangle(new Point(center.x - 10, center.y - 10), new Size(20, - (minR - 10)));
    const shaft2 = shaft1.clone();
    const shaft3 = shaft1.clone();
    shaft1.rotate(90, center);
    shaft2.rotate(210, center);
    shaft3.rotate(330, center);

    this.carousel = new CompoundPath({
      children : [ new Path.RegularPolygon(center, 6, maxR),
                   shaft1, shaft2, shaft3],
      fillRule : 'evenodd',
      fillColor : 'SteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 10:
      shadowBlur : 10,
      // Offset the shadow by { x: 2, y: 2 }
      shadowOffset : new Point(2, 2),
      rotation : DrillTable_CA__INITIAL_POSITION_Theta,
      angle : DrillTable_CA__INITIAL_POSITION_Theta
    });

    this.drill1 = new Path({
      segments : [[center.x - 6, center.y - 12], [center.x - 6, center.y - minR], [center.x, center.y - maxR], [center.x + 6, center.y - minR], [center.x + 6, center.y - 12]],
      closed : true,
      lockY : null
    });
    this.drill1.lockY = this.drill1.position.y;
    drillOff(this.drill1, 1);
    this.drill2 = new Path({
      segments : [[center.x - 4, center.y - 12], [center.x - 4, center.y - minR], [center.x, center.y - maxR], [center.x + 4, center.y - minR], [center.x + 4, center.y - 12]],
      closed : true,
      lockY : null
    });
    this.drill2.lockY = this.drill2.position.y;
    drillOff(this.drill2, 1);
    this.drill2.rotate(240, center);
    this.drill3 = new Path({
      segments : [[center.x - 5, center.y - 12], [center.x - 5, center.y - minR], [center.x, center.y - maxR], [center.x + 5, center.y - minR], [center.x + 5, center.y - 12]],
      closed : true,
      lockY : null
    });
    this.drill3.lockY = this.drill3.position.y;
    drillOff(this.drill3, 1);
    this.drill3.rotate(120, center);
  }, 
  move: function(from, to, percentageComplete) {
    var newTheta = (from.Theta + (to.Theta - from.Theta) * percentageComplete);
    var deltaTheta = newTheta - this.carousel.angle;
    this.carousel.rotation = deltaTheta;
    this.carousel.angle = newTheta
    
    this.drill1.rotate(deltaTheta, center);
    this.drill2.rotate(deltaTheta, center);
    this.drill3.rotate(deltaTheta, center);
  }
}

var DrillTable_CL = {
  ball : null,
  table : null,
  init : function(anchor) {
    this.table = new CompoundPath({
      children : [ new Path.Rectangle(new Point(anchor.X - (ballSize * 2) - 15, anchor.Y + (ballSize * 2)), new Size((ballSize * 4) + 30, (ballSize * 4) + 25)),
                   new Path.Rectangle(new Point(anchor.X - (ballSize * 2) - 5, anchor.Y + (ballSize * 2) + 10), new Size((ballSize * 4) + 10, (ballSize * 4) + 5)),
                   new Path.Rectangle(new Point(anchor.X - 10, anchor.Y + (ballSize * 2)), new Size(20, 10))],
      fillRule : 'evenodd',
      fillColor : 'SteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 10:
      shadowBlur : 10,
      // Offset the shadow by { x: 2, y: 2 }
      shadowOffset : new Point(2, 2)
    });

    this.ball = createBall();
    this.ball.visible = false;
    this.ball.position = new Point(anchor.X, anchor.Y + ballSize);
  },
  clamp : function(percentageComplete) {
    if (percentageComplete == 1) {
      this.ball.visible = true;
    }
  },
  unclamp : function(percentageComplete) {
    if (percentageComplete == 1) {
      this.ball.visible = false;
    }
  }
}

var DrillTable_XY = {
  move: function(from, to, percentageComplete) {
    var newX = from.X + (to.X - from.X) * percentageComplete;
    var newY = from.Y + (to.Y - from.Y) * percentageComplete;
    var newOrigin = new Point(DrillTable_CL.ball.bounds.topLeft.x + newX, DrillTable_CL.ball.bounds.topLeft.y + newY);
    
    DrillTable_CL.ball.fillColor = {
      gradient : {
        stops : [ [ 'Red', 0.2 ], [ 'Crimson', 0.35 ], [ 'Black', 1 ] ],
        radial : true
      },
      origin : newOrigin,
      destination : DrillTable_CL.ball.bounds.rightCenter
    };
  }
}

var LoadRobot_CL = {
  clamp : function(percentageComplete) {
    if (percentageComplete < 1) {
      LoadRobot_XY.clamp.fillColor = 'LightSteelBlue';
    } else {
      LoadRobot_XY.clamp.fillColor = 'Crimson';
      LoadRobot_XY.ball.visible = true;
    }
  },
  unclamp : function(percentageComplete) {
    if (percentageComplete < 1) {
      LoadRobot_XY.clamp.fillColor = 'LightSteelBlue';
    } else {
      LoadRobot_XY.clamp.fillColor = 'SteelBlue';
      LoadRobot_XY.ball.visible = false;
    }
  }
}

var LoadRobot_XY = {
  ball : null,
  clamp : null,
  shaft : null,
  init : function(position) {
    this.ball = createBall();
    this.ball.visible = false;
    //new Point(0, 0), new Size(ballSize * 2, 40));
    //this.clamp.fillColor = 'SteelBlue';
    this.shaft = new Path.Rectangle({
      topLeft : new Point(0, 0),
      bottomRight : new Point(10,2),
      fillColor : 'LightSteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 10:
      shadowBlur : 10,
      // Offset the shadow by { x: 2, y: 2 }
      shadowOffset : new Point(2, 2)
    });
    //new Path.Rectangle(new Point(0, 0), new Size(10, 10));
    //this.shaft.fillColor = 'LightSteelBlue';
    this.clamp = new Path.Rectangle({
      topLeft : new Point(0, 0),
      bottomRight : new Point(ballSize * 2, 40),
      fillColor : 'SteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 10:
      shadowBlur : 10,
      // Offset the shadow by { x: 2, y: 2 }
      shadowOffset : new Point(2, 2)
    });
    this.move(position, position, 1);
  },
  move : function(from, to, percentageComplete) {
    var newX = from.X + (to.X - from.X) * percentageComplete;
    var newY = from.Y + (to.Y - from.Y) * percentageComplete;
    this.shaft.bounds.height = newY - LoadRobot_XY_ABOVE_Y + 2;
    this.shaft.position = new Point(newX, LoadRobot_XY_ABOVE_Y - 31 + (newY - LoadRobot_XY_ABOVE_Y) / 2);
    this.clamp.position = new Point(newX, newY - 10)
    this.ball.position = new Point(newX, newY + ballSize);
  }
}

var UnloadRobot_CL = {
  clamp : function(percentageComplete) {
    if (percentageComplete < 1) {
      UnloadRobot_XY.clamp.fillColor = 'LightSteelBlue';
    } else {
      UnloadRobot_XY.clamp.fillColor = 'Crimson';
      UnloadRobot_XY.ball.visible = true;
    }
  },
  unclamp : function(percentageComplete) {
    if (percentageComplete < 1) {
      UnloadRobot_XY.clamp.fillColor = 'LightSteelBlue';
    } else {
      UnloadRobot_XY.clamp.fillColor = 'SteelBlue';
      UnloadRobot_XY.ball.visible = false;
    }
  }
}

var UnloadRobot_XY = {
  ball : null,
  clamp : null,
  shaft : null,
  init : function(position) {
    this.ball = createBall();
    this.ball.visible = false;
    //new Point(0, 0), new Size(ballSize * 2, 40));
    //this.clamp.fillColor = 'SteelBlue';
    this.shaft = new Path.Rectangle({
      topLeft : new Point(0, 0),
      bottomRight : new Point(10,2),
      fillColor : 'LightSteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 10:
      shadowBlur : 10,
      // Offset the shadow by { x: 2, y: 2 }
      shadowOffset : new Point(2, 2)
    });
    //new Path.Rectangle(new Point(0, 0), new Size(10, 10));
    //this.shaft.fillColor = 'LightSteelBlue';
    this.clamp = new Path.Rectangle({
      topLeft : new Point(0, 0),
      bottomRight : new Point(ballSize * 2, 40),
      fillColor : 'SteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 10:
      shadowBlur : 10,
      // Offset the shadow by { x: 2, y: 2 }
      shadowOffset : new Point(2, 2)
    });
    this.move(position, position, 1);
  },
  move : function(from, to, percentageComplete) {
    var newX = from.X + (to.X - from.X) * percentageComplete;
    var newY = from.Y + (to.Y - from.Y) * percentageComplete;
    this.shaft.bounds.height = newY - LoadRobot_XY_ABOVE_Y + 2;
    this.shaft.position = new Point(newX, LoadRobot_XY_ABOVE_Y - 31 + (newY - LoadRobot_XY_ABOVE_Y) / 2);
    this.clamp.position = new Point(newX, newY - 10)
    this.ball.position = new Point(newX, newY + ballSize);
  }
}

// Auto generated, do not modify!
function clamp(peripheral, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined) return; // Outside time scope
  peripheral.clamp(percentageComplete);
}

// Manually adapted as conditioner needs to cool down ;)
function condition(peripheral, timeFrom, timeTo, time) {
  var deltaTime = timeTo - timeFrom;
  if (time < timeFrom)
    return; // Outside time scope
  if (time > (timeTo + deltaTime))
    return; // Outside time scope

  var percentageComplete = (time - timeFrom) / deltaTime;

  if (percentageComplete < 1) {
    peripheral.condition(percentageComplete, true);
  } else {
    // It takes 40% of the time to cool down
    percentageComplete = Math.max((1.4 - percentageComplete) * 2.5, 0);
    peripheral.condition(percentageComplete, false);
  }
}

function off(peripheral, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined) return; // Outside time scope
  peripheral.off(percentageComplete);
}

function on(peripheral, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined) return; // Outside time scope
  peripheral.on(percentageComplete);
}

function unclamp(peripheral, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined) return; // Outside time scope
  peripheral.unclamp(percentageComplete);
}

function move(peripheral, from, to, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined) return; // Outside time scope
  peripheral.move(from, to, percentageComplete);
}

function toPercentageComplete(timeFrom, timeTo, time) {
  if (time < timeFrom) return;
  if (time > timeTo + 0.1) return;
  
  var deltaTime = timeTo - timeFrom;
  var timeIn = time - timeFrom;
  var ratio = timeIn/deltaTime;
  return Math.min(ratio, 1);
}

function plusIs(base, augment) {
  for (key in augment) {
    base[key] += augment[key];
  }
  return base;
}

function clone(base) {
  return JSON.parse(JSON.stringify(base));
}
