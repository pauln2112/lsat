/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler

import lsat_graph.ClaimTask
import lsat_graph.ReleaseTask
import lsat_graph.lsat_graphFactory
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.lsat.common.graph.directed.DirectedGraph
import org.eclipse.lsat.common.graph.directed.Edge
import org.eclipse.lsat.common.graph.directed.Node
import org.eclipse.lsat.common.scheduler.graph.GraphFactory
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph
import org.eclipse.lsat.motioncalculator.MotionException
import org.eclipse.lsat.timing.util.SpecificationException
import org.slf4j.LoggerFactory

/**
 * Removes the generated event resources and inserts a direct dependency between the actions
 */
class ShortcutClaimRelease<T extends Task> {
    static val LOGGER = LoggerFactory.getLogger(ShortcutClaimRelease)

    private new(){
    }
    
    static def <T extends Task> transformModel(TaskDependencyGraph<T> graph){
        LOGGER.debug('Starting transformation')
        graph.processGraph
        LOGGER.debug('Finished transformation')
        return graph
    }

    private static def <T extends Task, E extends Edge> void processGraph(
        DirectedGraph<T, E> graph) throws SpecificationException, MotionException {
        // creating a  copy first because the graph is modified
        graph.subGraphs.clone.forEach[processGraph]
        graph.nodes.clone.forEach[shortcutClaimRelease]
    }

    private static def dispatch void shortcutClaimRelease(Node node) {
    }

    private static def dispatch void shortcutClaimRelease(ClaimTask claimTask) throws SpecificationException, MotionException {
        val releaseTask = claimTask.correspondingReleaseTask;
        if (releaseTask!==null) {
            claimTask.incomingEdges.map[sourceNode].forEach [ s |
                releaseTask.outgoingEdges.map[targetNode].forEach [ t |
                    addDependency(s, t, claimTask.action.resource.name);
                ]
            ]
            claimTask.incomingEdges.clone.forEach[ delete ]
            claimTask.outgoingEdges.clone.forEach[delete]
            releaseTask.outgoingEdges.clone.forEach[delete]
            claimTask.delete
            releaseTask.delete
        }
    }
    
    private static def getCorrespondingReleaseTask(ClaimTask claimTask){
        if (claimTask.outgoingEdges.size != 1) return null
        return  claimTask.outgoingEdges.map[targetNode].filter(ReleaseTask).findFirst[action.resource===claimTask.action.resource]
    }
    
    private static def void delete(Edge edge){
        edge.targetNode=null 
        edge.sourceNode = null 
        edge.graph = null
        EcoreUtil.delete(edge)
    }

    private static  def void delete(Node node){
        node.graph = null
        EcoreUtil.delete(node)
    }

    private static def  <T extends Task, E extends Edge> addDependency(Node src,  Node target, String eventName) {
        var dep = GraphFactory.eINSTANCE.createDependency()
        var eventAnnotation = lsat_graphFactory.eINSTANCE.createEventAnnotation();
        eventAnnotation.name = eventName
        dep.aspects += eventAnnotation;
        dep.graph = src.graph
        dep.sourceNode = src
        dep.targetNode = target
    }
}
