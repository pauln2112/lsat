/**
 */
package lsat_graph.impl;

import java.util.Collection;
import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;

import org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledTaskImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import lsat_graph.ClaimedByScheduledTask;
import lsat_graph.lsat_graphPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Claimed By Scheduled Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.impl.ClaimedByScheduledTaskImpl#getReleases <em>Releases</em>}</li>
 *   <li>{@link lsat_graph.impl.ClaimedByScheduledTaskImpl#getClaims <em>Claims</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClaimedByScheduledTaskImpl extends ScheduledTaskImpl<Task> implements ClaimedByScheduledTask {
	/**
     * The cached value of the '{@link #getReleases() <em>Releases</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReleases()
     * @generated
     * @ordered
     */
    protected EList<ScheduledTask<Task>> releases;

    /**
     * The cached value of the '{@link #getClaims() <em>Claims</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getClaims()
     * @generated
     * @ordered
     */
    protected EList<ScheduledTask<Task>> claims;

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ClaimedByScheduledTaskImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.CLAIMED_BY_SCHEDULED_TASK;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<ScheduledTask<Task>> getReleases()
    {
        if (releases == null)
        {
            releases = new EObjectResolvingEList<ScheduledTask<Task>>(ScheduledTask.class, this, lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__RELEASES);
        }
        return releases;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<ScheduledTask<Task>> getClaims()
    {
        if (claims == null)
        {
            claims = new EObjectResolvingEList<ScheduledTask<Task>>(ScheduledTask.class, this, lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__CLAIMS);
        }
        return claims;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__RELEASES:
                return getReleases();
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__CLAIMS:
                return getClaims();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__RELEASES:
                getReleases().clear();
                getReleases().addAll((Collection<? extends ScheduledTask<Task>>)newValue);
                return;
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__CLAIMS:
                getClaims().clear();
                getClaims().addAll((Collection<? extends ScheduledTask<Task>>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__RELEASES:
                getReleases().clear();
                return;
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__CLAIMS:
                getClaims().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__RELEASES:
                return releases != null && !releases.isEmpty();
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK__CLAIMS:
                return claims != null && !claims.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //ClaimedByScheduledTaskImpl
