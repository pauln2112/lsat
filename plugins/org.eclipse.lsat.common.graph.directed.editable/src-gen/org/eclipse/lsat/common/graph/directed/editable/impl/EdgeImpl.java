/**
 */
package org.eclipse.lsat.common.graph.directed.editable.impl;

import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.EdgeTarget;
import org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.graph.directed.editable.SourceReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl#getEdge <em>Edge</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl#getSourceNode <em>Source Node</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl#getTargetNode <em>Target Node</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl#getGraph <em>Graph</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EdgeImpl extends MinimalEObjectImpl.Container implements Edge {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected SourceReference source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected EdgeTarget target;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EdgPackage.Literals.EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Edge getEdge() {
		if (eContainerFeatureID() != EdgPackage.EDGE__EDGE) return null;
		return (Edge)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEdge(Edge newEdge, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newEdge, EdgPackage.EDGE__EDGE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEdge(Edge newEdge) {
		if (newEdge != eInternalContainer() || (eContainerFeatureID() != EdgPackage.EDGE__EDGE && newEdge != null)) {
			if (EcoreUtil.isAncestor(this, newEdge))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newEdge != null)
				msgs = ((InternalEObject)newEdge).eInverseAdd(this, EdgPackage.EDGE__TARGET, Edge.class, msgs);
			msgs = basicSetEdge(newEdge, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EdgPackage.EDGE__EDGE, newEdge, newEdge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		// We need this trick as we can only put the genmodel annotation on the DependencyTarget interface.
		// This causes this code to be generated in all implementers of this interface and therefore the code should compile 
		if (EdgeImpl.class.isInstance(this)) {
			// Dependency.name = sourceTask.name -> targetTask.name
			return (null == EdgeImpl.class.cast(this).source ? null : EdgeImpl.class.cast(this).source.getName()) + 
					" -> " + 
					(null == EdgeImpl.class.cast(this).target ? null : EdgeImpl.class.cast(this).target.getName());
		} else if (TargetReferenceImpl.class.isInstance(this)) {
			// TaskRef.name = task.name 
			return null == TargetReferenceImpl.class.cast(this).node ? null : TargetReferenceImpl.class.cast(this).node.getName();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SourceReference getSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSource(SourceReference newSource, NotificationChain msgs) {
		SourceReference oldSource = source;
		source = newSource;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EdgPackage.EDGE__SOURCE, oldSource, newSource);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSource(SourceReference newSource) {
		if (newSource != source) {
			NotificationChain msgs = null;
			if (source != null)
				msgs = ((InternalEObject)source).eInverseRemove(this, EdgPackage.SOURCE_REFERENCE__EDGE, SourceReference.class, msgs);
			if (newSource != null)
				msgs = ((InternalEObject)newSource).eInverseAdd(this, EdgPackage.SOURCE_REFERENCE__EDGE, SourceReference.class, msgs);
			msgs = basicSetSource(newSource, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EdgPackage.EDGE__SOURCE, newSource, newSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Node getSourceNode() {
		return null == source ? null : source.getNode();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EdgeTarget getTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTarget(EdgeTarget newTarget, NotificationChain msgs) {
		EdgeTarget oldTarget = target;
		target = newTarget;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EdgPackage.EDGE__TARGET, oldTarget, newTarget);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTarget(EdgeTarget newTarget) {
		if (newTarget != target) {
			NotificationChain msgs = null;
			if (target != null)
				msgs = ((InternalEObject)target).eInverseRemove(this, EdgPackage.EDGE_TARGET__EDGE, EdgeTarget.class, msgs);
			if (newTarget != null)
				msgs = ((InternalEObject)newTarget).eInverseAdd(this, EdgPackage.EDGE_TARGET__EDGE, EdgeTarget.class, msgs);
			msgs = basicSetTarget(newTarget, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EdgPackage.EDGE__TARGET, newTarget, newTarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Node getTargetNode() {
		if (target instanceof TargetReferenceImpl) {
			return TargetReferenceImpl.class.cast(target).node;
		}
		if (target instanceof EdgeImpl) {
			return EdgeImpl.class.cast(target).getSourceNode();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EditableDirectedGraph getGraph() {
		if (null != getEdge()) {
			return getEdge().getGraph();
		}
		return EditableDirectedGraph.class.cast(eContainer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EdgPackage.EDGE__EDGE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetEdge((Edge)otherEnd, msgs);
			case EdgPackage.EDGE__SOURCE:
				if (source != null)
					msgs = ((InternalEObject)source).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EdgPackage.EDGE__SOURCE, null, msgs);
				return basicSetSource((SourceReference)otherEnd, msgs);
			case EdgPackage.EDGE__TARGET:
				if (target != null)
					msgs = ((InternalEObject)target).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EdgPackage.EDGE__TARGET, null, msgs);
				return basicSetTarget((EdgeTarget)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EdgPackage.EDGE__EDGE:
				return basicSetEdge(null, msgs);
			case EdgPackage.EDGE__SOURCE:
				return basicSetSource(null, msgs);
			case EdgPackage.EDGE__TARGET:
				return basicSetTarget(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case EdgPackage.EDGE__EDGE:
				return eInternalContainer().eInverseRemove(this, EdgPackage.EDGE__TARGET, Edge.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EdgPackage.EDGE__EDGE:
				return getEdge();
			case EdgPackage.EDGE__NAME:
				return getName();
			case EdgPackage.EDGE__SOURCE:
				return getSource();
			case EdgPackage.EDGE__SOURCE_NODE:
				return getSourceNode();
			case EdgPackage.EDGE__TARGET:
				return getTarget();
			case EdgPackage.EDGE__TARGET_NODE:
				return getTargetNode();
			case EdgPackage.EDGE__GRAPH:
				return getGraph();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EdgPackage.EDGE__EDGE:
				setEdge((Edge)newValue);
				return;
			case EdgPackage.EDGE__SOURCE:
				setSource((SourceReference)newValue);
				return;
			case EdgPackage.EDGE__TARGET:
				setTarget((EdgeTarget)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EdgPackage.EDGE__EDGE:
				setEdge((Edge)null);
				return;
			case EdgPackage.EDGE__SOURCE:
				setSource((SourceReference)null);
				return;
			case EdgPackage.EDGE__TARGET:
				setTarget((EdgeTarget)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EdgPackage.EDGE__EDGE:
				return getEdge() != null;
			case EdgPackage.EDGE__NAME:
				return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
			case EdgPackage.EDGE__SOURCE:
				return source != null;
			case EdgPackage.EDGE__SOURCE_NODE:
				return getSourceNode() != null;
			case EdgPackage.EDGE__TARGET:
				return target != null;
			case EdgPackage.EDGE__TARGET_NODE:
				return getTargetNode() != null;
			case EdgPackage.EDGE__GRAPH:
				return getGraph() != null;
		}
		return super.eIsSet(featureID);
	}

} //EdgeImpl
