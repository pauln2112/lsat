/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.util;

import activity.Move;
import activity.SimpleAction;
import com.google.common.base.Objects;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import machine.ActionType;
import machine.Axis;
import machine.IResource;
import machine.Peripheral;
import machine.SetPoint;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomGenerator;
import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.util.IterableUtil;
import org.eclipse.lsat.common.xtend.annotations.IntermediateProperty;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.lsat.timing.util.ITimingCalculator;
import org.eclipse.lsat.timing.util.MotionCalculatorHelper;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pair;
import setting.PhysicalSettings;
import setting.Settings;
import timing.Array;
import timing.CalculationMode;
import timing.Distribution;
import timing.DistributionsFactory;
import timing.FixedValue;
import timing.Timing;
import timing.distribution.ModeDistribution;

@SuppressWarnings("all")
public class TimingCalculator implements ITimingCalculator {
  private static final MathContext MICRO_SECONDS = new MathContext(6, RoundingMode.HALF_UP);
  
  @IntermediateProperty(Array.class)
  private final Map<Array, Integer> _IntermediateProperty_pointer = new java.util.WeakHashMap<>();
  
  private final RandomGenerator distributionRandom = new JDKRandomGenerator(1618033989);
  
  private final Map<Distribution, ModeDistribution> distributionSession = new IdentityHashMap<Distribution, ModeDistribution>();
  
  private final MotionCalculatorHelper motionCalculatorHelper;
  
  private final CalculationMode mode;
  
  private final boolean synchronizeAxes;
  
  private final Map<Node, BigDecimal> nodeTimes;
  
  private final Map<Move, Map<SetPoint, BigDecimal>> motionTimes;
  
  public TimingCalculator(final Settings settings, final MotionCalculatorExtension motionCalculator) {
    this(settings, motionCalculator, CalculationMode.MEAN, true, true);
  }
  
  public TimingCalculator(final Settings settings, final MotionCalculatorExtension motionCalculator, final CalculationMode mode) {
    this(settings, motionCalculator, mode, true, true);
  }
  
  public TimingCalculator(final Settings settings, final MotionCalculatorExtension motionCalculator, final CalculationMode mode, final boolean synchronizeAxes, final boolean useCache) {
    MotionCalculatorHelper _motionCalculatorHelper = new MotionCalculatorHelper(settings, motionCalculator);
    this.motionCalculatorHelper = _motionCalculatorHelper;
    this.mode = mode;
    this.synchronizeAxes = synchronizeAxes;
    IdentityHashMap<Node, BigDecimal> _xifexpression = null;
    if ((useCache && Objects.equal(mode, CalculationMode.MEAN))) {
      _xifexpression = new IdentityHashMap<Node, BigDecimal>();
    }
    this.nodeTimes = _xifexpression;
    IdentityHashMap<Move, Map<SetPoint, BigDecimal>> _xifexpression_1 = null;
    if (useCache) {
      _xifexpression_1 = new IdentityHashMap<Move, Map<SetPoint, BigDecimal>>();
    }
    this.motionTimes = _xifexpression_1;
  }
  
  public Settings getSettings() {
    return this.motionCalculatorHelper.getSettings();
  }
  
  public MotionCalculatorExtension getMotionCalculator() {
    return this.motionCalculatorHelper.getMotionCalculator();
  }
  
  @Override
  public void reset() {
    this.distributionSession.clear();
    this._IntermediateProperty_pointer.clear();
    if ((this.nodeTimes != null)) {
      this.nodeTimes.clear();
    }
    if ((this.motionTimes != null)) {
      this.motionTimes.clear();
    }
  }
  
  @Override
  public BigDecimal calculateDuration(final Node node) throws MotionException {
    BigDecimal _xifexpression = null;
    if ((null == this.nodeTimes)) {
      _xifexpression = this.doCalculateDuration(node);
    } else {
      final Function<Node, BigDecimal> _function = (Node it) -> {
        return this.doCalculateDuration(it);
      };
      _xifexpression = this.nodeTimes.computeIfAbsent(node, _function);
    }
    return _xifexpression;
  }
  
  protected BigDecimal _doCalculateDuration(final Node node) {
    return BigDecimal.ZERO;
  }
  
  protected BigDecimal _doCalculateDuration(final SimpleAction action) {
    return this.doCalculateValue(action.getType(), action.getResource(), action.getPeripheral());
  }
  
  protected BigDecimal doCalculateValue(final ActionType actionType, final IResource resource, final Peripheral peripheral) {
    try {
      return this.doCalculateValue(this.getPhysicalSettings(resource, peripheral).getTimingSettings().get(actionType));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected BigDecimal _doCalculateValue(final FixedValue fixedValue) {
    return fixedValue.getValue();
  }
  
  protected BigDecimal _doCalculateValue(final Array array) {
    BigDecimal _switchResult = null;
    final CalculationMode mode = this.mode;
    if (mode != null) {
      switch (mode) {
        case LINEAIR:
          BigDecimal _xblockexpression = null;
          {
            Integer _pointer = this.getPointer(array);
            int _plus = ((_pointer).intValue() + 1);
            int _size = array.getValues().size();
            int _modulo = (_plus % _size);
            this.setPointer(array, Integer.valueOf(_modulo));
            _xblockexpression = array.getValues().get((this.getPointer(array)).intValue());
          }
          _switchResult = _xblockexpression;
          break;
        default:
          _switchResult = TimingCalculator.getAverage(array.getValues());
          break;
      }
    } else {
      _switchResult = TimingCalculator.getAverage(array.getValues());
    }
    return _switchResult;
  }
  
  protected BigDecimal _doCalculateValue(final Distribution distribution) {
    BigDecimal _xblockexpression = null;
    {
      final Function<Distribution, ModeDistribution> _function = (Distribution it) -> {
        return DistributionsFactory.createRealDistribution(it, this.distributionRandom);
      };
      final ModeDistribution realDistribution = this.distributionSession.computeIfAbsent(distribution, _function);
      BigDecimal _switchResult = null;
      final CalculationMode mode = this.mode;
      if (mode != null) {
        switch (mode) {
          case DISTRIBUTED:
            _switchResult = TimingCalculator.normalize(Double.valueOf(realDistribution.sample()));
            break;
          default:
            _switchResult = TimingCalculator.normalize(Double.valueOf(realDistribution.getDefault()));
            break;
        }
      } else {
        _switchResult = TimingCalculator.normalize(Double.valueOf(realDistribution.getDefault()));
      }
      _xblockexpression = _switchResult;
    }
    return _xblockexpression;
  }
  
  protected BigDecimal _doCalculateDuration(final Move move) {
    try {
      return IterableUtil.<BigDecimal>max(this.calculateMotionTime(move).values(), BigDecimal.ZERO);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Override
  public Map<SetPoint, BigDecimal> calculateMotionTime(final Move move) throws MotionException {
    if ((null == this.motionTimes)) {
      return this.doCalculateMotionTimes(move).get(move);
    }
    boolean _containsKey = this.motionTimes.containsKey(move);
    boolean _not = (!_containsKey);
    if (_not) {
      this.motionTimes.putAll(this.doCalculateMotionTimes(move));
    }
    return this.motionTimes.get(move);
  }
  
  protected Map<Move, ? extends Map<SetPoint, BigDecimal>> doCalculateMotionTimes(final Move move) {
    try {
      Map<EList<Axis>, ? extends List<SetPoint>> _xifexpression = null;
      if (this.synchronizeAxes) {
        EList<Axis> _axes = move.getPeripheral().getType().getAxes();
        EList<SetPoint> _setPoints = move.getPeripheral().getType().getSetPoints();
        Pair<EList<Axis>, EList<SetPoint>> _mappedTo = Pair.<EList<Axis>, EList<SetPoint>>of(_axes, _setPoints);
        _xifexpression = CollectionLiterals.<EList<Axis>, EList<SetPoint>>newHashMap(_mappedTo);
      } else {
        final Function1<SetPoint, EList<Axis>> _function = (SetPoint it) -> {
          return it.getAxes();
        };
        _xifexpression = IterableExtensions.<EList<Axis>, SetPoint>groupBy(move.getPeripheral().getType().getSetPoints(), _function);
      }
      final Map<EList<Axis>, ? extends List<SetPoint>> axesAndSetPoints = _xifexpression;
      final List<Move> concatenatedMove = this.motionCalculatorHelper.getConcatenatedMove(move);
      int _size = concatenatedMove.size();
      final IdentityHashMap<Move, IdentityHashMap<SetPoint, BigDecimal>> result = new IdentityHashMap<Move, IdentityHashMap<SetPoint, BigDecimal>>(_size);
      Set<? extends Map.Entry<EList<Axis>, ? extends List<SetPoint>>> _entrySet = axesAndSetPoints.entrySet();
      for (final Map.Entry<EList<Axis>, ? extends List<SetPoint>> e : _entrySet) {
        {
          final List<MotionSegment> motionSegments = this.motionCalculatorHelper.createMotionSegments(concatenatedMove, e.getKey());
          final List<Double> motionTimes = this.motionCalculatorHelper.getMotionCalculator().calculateTimes(motionSegments);
          BigDecimal startTime = BigDecimal.ZERO;
          for (int i = 0; (i < motionTimes.size()); i++) {
            {
              final BigDecimal endTime = TimingCalculator.normalize(motionTimes.get(i));
              final Function<Move, IdentityHashMap<SetPoint, BigDecimal>> _function_1 = (Move it) -> {
                int _size_1 = move.getPeripheral().getType().getSetPoints().size();
                return new IdentityHashMap<SetPoint, BigDecimal>(_size_1);
              };
              TimingCalculator.<SetPoint, BigDecimal>fill(result.computeIfAbsent(concatenatedMove.get(i), _function_1), e.getValue(), endTime.subtract(startTime));
              startTime = endTime;
            }
          }
        }
      }
      return result;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected PhysicalSettings getPhysicalSettings(final IResource resource, final Peripheral peripheral) throws SpecificationException {
    final PhysicalSettings physicalSettings = this.getSettings().getPhysicalSettings(resource, peripheral);
    if ((null == physicalSettings)) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Physical settings not specified for peripheral: ");
      String _fqn = peripheral.fqn();
      _builder.append(_fqn);
      Settings _settings = this.getSettings();
      throw new SpecificationException(_builder.toString(), _settings);
    }
    return physicalSettings;
  }
  
  /**
   * Fills the map with the specified value for all specified keys.
   */
  public static <K extends Object, V extends Object> void fill(final Map<K, V> map, final Collection<? extends K> keys, final V value) {
    final Consumer<K> _function = (K it) -> {
      map.put(it, value);
    };
    keys.forEach(_function);
  }
  
  public static BigDecimal getAverage(final Collection<BigDecimal> values) {
    BigDecimal sum = IterableExtensions.<BigDecimal>head(values);
    Iterable<BigDecimal> _tail = IterableExtensions.<BigDecimal>tail(values);
    for (final BigDecimal value : _tail) {
      BigDecimal _sum = sum;
      sum = _sum.add(value);
    }
    int _size = values.size();
    BigDecimal _bigDecimal = new BigDecimal(_size);
    return sum.divide(_bigDecimal, TimingCalculator.MICRO_SECONDS);
  }
  
  public static BigDecimal normalize(final Number number) {
    final BigDecimal result = BigDecimal.valueOf(number.doubleValue());
    return result.max(BigDecimal.ZERO).round(TimingCalculator.MICRO_SECONDS);
  }
  
  protected BigDecimal doCalculateDuration(final Node move) {
    if (move instanceof Move) {
      return _doCalculateDuration((Move)move);
    } else if (move instanceof SimpleAction) {
      return _doCalculateDuration((SimpleAction)move);
    } else if (move != null) {
      return _doCalculateDuration(move);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(move).toString());
    }
  }
  
  protected BigDecimal doCalculateValue(final Timing array) {
    if (array instanceof Array) {
      return _doCalculateValue((Array)array);
    } else if (array instanceof Distribution) {
      return _doCalculateValue((Distribution)array);
    } else if (array instanceof FixedValue) {
      return _doCalculateValue((FixedValue)array);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(array).toString());
    }
  }
  
  private static final Integer _DEFAULT_ARRAY_POINTER = Integer.valueOf((-1));
  
  private Integer getPointer(final Array owner) {
    Integer value = _IntermediateProperty_pointer.get(owner);
    return value == null ? _DEFAULT_ARRAY_POINTER : value;
  }
  
  private void setPointer(final Array owner, final Integer value) {
    if (value == _DEFAULT_ARRAY_POINTER) {
        _IntermediateProperty_pointer.remove(owner);
    } else {
        _IntermediateProperty_pointer.put(owner, value);
    }
  }
  
  private void disposePointer() {
    _IntermediateProperty_pointer.clear();
  }
}
