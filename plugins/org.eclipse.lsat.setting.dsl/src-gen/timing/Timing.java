/**
 */
package timing;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timing</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see timing.TimingPackage#getTiming()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Timing extends EObject {
} // Timing
