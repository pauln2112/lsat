/**
 */
package org.eclipse.lsat.common.mpt.impl;

import org.eclipse.lsat.common.mpt.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MPTFactoryImpl extends EFactoryImpl implements MPTFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPTFactory init() {
		try {
			MPTFactory theMPTFactory = (MPTFactory)EPackage.Registry.INSTANCE.getEFactory(MPTPackage.eNS_URI);
			if (theMPTFactory != null) {
				return theMPTFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MPTFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPTFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MPTPackage.MATRIX: return createMatrix();
			case MPTPackage.MAX_PLUS_SPECIFICATION: return createMaxPlusSpecification();
			case MPTPackage.FSM: return createFSM();
			case MPTPackage.FSM_STATE: return createFSMState();
			case MPTPackage.FSM_TRANSITION: return createFSMTransition();
			case MPTPackage.MPS: return createMPS();
			case MPTPackage.MPS_CONFIGURATION: return createMPSConfiguration();
			case MPTPackage.MPS_TRANSITION: return createMPSTransition();
			case MPTPackage.MPA: return createMPA();
			case MPTPackage.MPA_STATE: return createMPAState();
			case MPTPackage.MPA_TRANSITION: return createMPATransition();
			case MPTPackage.ROW_VECTOR: return createRowVector();
			case MPTPackage.COLUMN_VECTOR: return createColumnVector();
			case MPTPackage.EVENT: return createEvent();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MPTPackage.FSM_TYPE:
				return createFSMTypeFromString(eDataType, initialValue);
			case MPTPackage.VALUE:
				return createValueFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MPTPackage.FSM_TYPE:
				return convertFSMTypeToString(eDataType, instanceValue);
			case MPTPackage.VALUE:
				return convertValueToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Matrix createMatrix() {
		MatrixImpl matrix = new MatrixImpl();
		return matrix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MaxPlusSpecification createMaxPlusSpecification() {
		MaxPlusSpecificationImpl maxPlusSpecification = new MaxPlusSpecificationImpl();
		return maxPlusSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FSM createFSM() {
		FSMImpl fsm = new FSMImpl();
		return fsm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FSMState createFSMState() {
		FSMStateImpl fsmState = new FSMStateImpl();
		return fsmState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FSMTransition createFSMTransition() {
		FSMTransitionImpl fsmTransition = new FSMTransitionImpl();
		return fsmTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MPS createMPS() {
		MPSImpl mps = new MPSImpl();
		return mps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MPSConfiguration createMPSConfiguration() {
		MPSConfigurationImpl mpsConfiguration = new MPSConfigurationImpl();
		return mpsConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MPSTransition createMPSTransition() {
		MPSTransitionImpl mpsTransition = new MPSTransitionImpl();
		return mpsTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MPA createMPA() {
		MPAImpl mpa = new MPAImpl();
		return mpa;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MPAState createMPAState() {
		MPAStateImpl mpaState = new MPAStateImpl();
		return mpaState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MPATransition createMPATransition() {
		MPATransitionImpl mpaTransition = new MPATransitionImpl();
		return mpaTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RowVector createRowVector() {
		RowVectorImpl rowVector = new RowVectorImpl();
		return rowVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ColumnVector createColumnVector() {
		ColumnVectorImpl columnVector = new ColumnVectorImpl();
		return columnVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Event createEvent() {
		EventImpl event = new EventImpl();
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSMType createFSMTypeFromString(EDataType eDataType, String initialValue) {
		FSMType result = FSMType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFSMTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createValueFromString(EDataType eDataType, String initialValue) {
		return (Double)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertValueToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MPTPackage getMPTPackage() {
		return (MPTPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MPTPackage getPackage() {
		return MPTPackage.eINSTANCE;
	}

} //MPTFactoryImpl
