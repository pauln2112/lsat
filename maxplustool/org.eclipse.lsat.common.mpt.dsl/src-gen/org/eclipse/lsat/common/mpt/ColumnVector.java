/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Column Vector</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getColumnVector()
 * @model
 * @generated
 */
public interface ColumnVector extends Vector {
} // ColumnVector
