/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
modeltype m_petrinet "strict" uses petri_net('http://www.eclipse.org/lsat/petri_net');
modeltype m_dg "strict" uses directed('http://www.eclipse.org/lsat/directed_graph');

library petriNetFix;

property counter: Integer;

/**
* Fix all the illegal scenarios in the petri net (and its sub nets) 
*/
helper fixPetriNet(inout petriNet: PetriNet) {
	var nodeFixes := petriNet.allSubobjectsOfKind(Node)[Node]->map fixNode();
	// We could also add all fixes to the petrinet, but this confuses Sirius' layout algorithm 
	//petriNet.nodes += nodeFixes.n;
	//petriNet.edges += nodeFixes.e;
	var edgeFixes := petriNet.allSubobjectsOfKind(Edge)[Edge]->map fixEdge();
    // We could also add all fixes to the petrinet, but this confuses Sirius' layout algorithm 
	//petriNet.nodes += edgeFixes.n;
	//petriNet.edges += edgeFixes.e;
}

mapping inout Node::fixNode(): Tuple(n: Node, e: Edge) 
disjuncts Transition::fixInitialTransition, Transition::fixFinalTransition, Place::fixMutipleIncomingEdges, Place::fixMutipleOutgoingEdges {}

mapping inout Transition::fixInitialTransition(): Tuple(n: Place, e: Edge) 
when {self.incomingEdges->isEmpty()} {
    init {
        result := Tuple {n = object Place {}, e = object Edge{}};
    }
    n.name := 'p'.appendCounter();
    e.sourceNode := n;
    e.targetNode := self;
    
    // Add the new elements to the correct graph container
    self.graph.nodes += n;
    self.graph.edges += e;
}

mapping inout Transition::fixFinalTransition(): Tuple(n: Place, e: Edge) 
when {self.outgoingEdges->isEmpty()} {
    init {
        result := Tuple {n = object Place {}, e = object Edge{}};
    }
    n.name := 'p'.appendCounter();
    e.sourceNode := self;
    e.targetNode := n;
    
    // Add the new elements to the correct graph container
    self.graph.nodes += n;
    self.graph.edges += e;
}

mapping inout Place::fixMutipleIncomingEdges(): Tuple(n: Transition, e: Edge) 
when {self.incomingEdges->size() > 1} {
    init {
        result := Tuple {n = object Transition {}, e = object Edge{}};
    }
    //n.name := 't'.appendCounter();
    n.name := 'tau';
	n.incomingEdges := self.incomingEdges;
	e.sourceNode := n;
	e.targetNode := self;
	
	// Add the new elements to the correct graph container
	self.graph.nodes += n;
	self.graph.edges += e;
}

mapping inout Place::fixMutipleOutgoingEdges(): Tuple(n: Transition, e: Edge) 
when {self.outgoingEdges->size() > 1} {
    init {
        result := Tuple {n = object Transition {}, e = object Edge{}};
    }
    //n.name := 't'.appendCounter();
    n.name := 'tau';
	n.outgoingEdges := self.outgoingEdges;
	e.sourceNode := self;
	e.targetNode := n;
	
	// Add the new elements to the correct graph container
	self.graph.nodes += n;
	self.graph.edges += e;
}

mapping inout Edge::fixEdge(): Tuple(n: Node, e: Edge) 
disjuncts Edge::addPlace, Edge::addTransition {}

mapping inout Edge::addPlace(): Tuple(n: Place, e: Edge) 
when {self.sourceNode.oclIsKindOf(Transition) and self.targetNode.oclIsKindOf(Transition)} {
    init {
    	result := Tuple {n = object Place {}, e = object Edge{}};
    }
    n.name := 'p'.appendCounter();
	e.sourceNode := n;
	e.targetNode := self.targetNode;
	self.targetNode := n;
	
	// Add the new elements to the correct graph container
	e.targetNode.graph.nodes += n;
	self.graph.edges += e;
}

mapping inout Edge::addTransition(): Tuple(n: Transition, e: Edge) 
when {self.sourceNode.oclIsKindOf(Place) and self.targetNode.oclIsKindOf(Place)} {
    init {
        result := Tuple {n = object Transition {}, e = object Edge{}};
    }
    //n.name := 't'.appendCounter();
    n.name := 'tau';
	e.sourceNode := n;
	e.targetNode := self.targetNode;
	self.targetNode := n;
	
	// Add the new elements to the correct graph container
	e.targetNode.graph.nodes += n;
	self.graph.edges += e;
}

query String::appendCounter(): String {
    counter := counter + 1;
	return self + counter.toString();
}
