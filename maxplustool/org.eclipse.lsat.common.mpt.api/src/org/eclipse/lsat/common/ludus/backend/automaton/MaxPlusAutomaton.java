/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.automaton;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.graph.DoubleWeightedGraph;
import org.eclipse.lsat.common.ludus.backend.graph.SingleWeightedGraph;

/**
 * Max-plus automaton.
 *
 * @author Bram van der Sanden
 */
public class MaxPlusAutomaton<T> implements SingleWeightedGraph<MPAState<T>, MPATransition<T>, Value>,
        DoubleWeightedGraph<MPAState<T>, MPATransition<T>, Value>
{
    private static final long serialVersionUID = 1L;

    private final Set<MPAState<T>> states;

    private final Set<MPATransition<T>> mpaTransitions;

    private final Map<MPAState<T>, Set<MPATransition<T>>> outgoingMap;

    private final Map<MPAState<T>, Set<MPATransition<T>>> incomingMap;

    private final Map<Tuple<T, Integer>, MPAState<T>> stateMap;

    public MaxPlusAutomaton() {
        states = new HashSet<>();
        mpaTransitions = new HashSet<>();
        outgoingMap = new HashMap<>();
        incomingMap = new HashMap<>();
        stateMap = new HashMap<>();
    }

    public void addState(MPAState<T> MPAState) {
        states.add(MPAState);
        stateMap.put(Tuple.of(MPAState.getLocation(), MPAState.getIndex()), MPAState);
    }

    public void addTransition(MPATransition<T> MPATransition) {
        MPAState<T> src = MPATransition.getSource();
        MPAState<T> tgt = MPATransition.getTarget();

        outgoingMap.putIfAbsent(src, new HashSet<>());
        Set<MPATransition<T>> out = outgoingMap.get(src);
        out.add(MPATransition);

        incomingMap.putIfAbsent(tgt, new HashSet<>());
        Set<MPATransition<T>> in = incomingMap.get(tgt);
        in.add(MPATransition);

        mpaTransitions.add(MPATransition);
    }

    @Override
    public Value getWeight1(MPATransition<T> edge) {
        return edge.getReward();
    }

    @Override
    public Value getWeight2(MPATransition<T> edge) {
        return edge.getDuration();
    }

    @Override
    public Value getWeight(MPATransition<T> edge) {
        return edge.getDuration();
    }

    public MPAState<T> getState(T fsmState, Integer index) {
        return stateMap.get(Tuple.of(fsmState, index));
    }

    @Override
    public Set<MPAState<T>> getVertices() {
        return states;
    }

    @Override
    public Set<MPATransition<T>> getEdges() {
        return mpaTransitions;
    }

    @Override
    public Collection<MPATransition<T>> incomingEdgesOf(MPAState<T> MPAState) {
        return incomingMap.getOrDefault(MPAState, Collections.emptySet());
    }

    @Override
    public Collection<MPATransition<T>> outgoingEdgesOf(MPAState<T> MPAState) {
        return outgoingMap.getOrDefault(MPAState, Collections.emptySet());
    }

    @Override
    public MPAState<T> getEdgeSource(MPATransition<T> MPATransition) {
        return MPATransition.getSource();
    }

    @Override
    public MPAState<T> getEdgeTarget(MPATransition<T> MPATransition) {
        return MPATransition.getTarget();
    }

    @Override
    public MPATransition<T> getEdge(MPAState<T> source, MPAState<T> target) {
        for (MPATransition<T> t: outgoingEdgesOf(source)) {
            if (t.getTarget().equals(target)) {
                return t;
            }
        }
        return null;
    }
}
