/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator;

import static java.lang.String.format;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A movement along one axis containing the <i>optional</i> from position and distance and the profile parameter values.
 * A {@link MotionSetPoint} has an id which typically represents an axis.<br>
 * <br>
 * If a motion segment contains more than one {@link MotionSetPoint} they are all moving at the same moment in time
 */
public final class MotionSetPoint implements Serializable {
    private static final long serialVersionUID = 2406048417109387877L;

    private final String id;

    private BigDecimal from;

    private BigDecimal to;

    private BigDecimal distance;

    private boolean settling;

    private String motionProfileId; // only used for (de)serialising

    private transient MotionProfile motionProfile; // motionProfile itself is not (de)serialised

    private final Map<String, BigDecimal> arguments = new LinkedHashMap<>();

    public MotionSetPoint(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getFrom() {
        return from;
    }

    public void setFrom(BigDecimal from) {
        this.from = from;
    }

    public BigDecimal getTo() {
        return to;
    }

    public void setTo(BigDecimal to) {
        this.to = to;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public boolean doesMove() {
        return distance.compareTo(BigDecimal.ZERO) != 0;
    }

    public void setSettling(boolean settling) {
        this.settling = settling;
    }

    public boolean isSettling() {
        return settling;
    }

    public MotionProfile getMotionProfile() {
        return motionProfile;
    }

    public void setMotionProfile(MotionProfile motionProfile) {
        this.motionProfile = motionProfile;
        this.motionProfileId = this.motionProfile != null ? this.motionProfile.getKey() : null;
    }

    public Map<String, BigDecimal> getMotionProfileArguments() {
        return Collections.unmodifiableMap(arguments);
    }

    public void setMotionProfileArgument(String key, BigDecimal value) {
        arguments.put(key, value);
    }

    public BigDecimal getMotionProfileArgument(String key) {
        return getMotionProfileArgument(key, null);
    }

    public BigDecimal getMotionProfileArgument(String key, BigDecimal defaultValue) throws IllegalArgumentException {
        if (motionProfile != null) {
            final MotionProfileParameter parameter = motionProfile.getParameter(key);
            if (null == parameter) {
                throw new IllegalArgumentException(
                        format("Programming error: Motion profile %s does not define parameter %s",
                                motionProfile.getName(), key));
            }
        }
        return arguments.getOrDefault(key, defaultValue);
    }

    @Override
    public String toString() {
        return String.format("Distance=%f, From=%f, To=%f, %s", distance, from, to, motionProfileId);
    }
}
