/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor.ui.contentassist;

import activity.ActivitySet;
import activity.Event;
import activity.Move;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import java.util.Set;
import machine.IResource;
import machine.Path;
import machine.PathTargetReference;
import machine.Profile;
import machine.ResourceItem;
import machine.SymbolicPosition;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.activity.teditor.ui.contentassist.AbstractActivityProposalProvider;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
@SuppressWarnings("all")
public class ActivityProposalProvider extends AbstractActivityProposalProvider {
  @Override
  public void completeMove_TargetPosition(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    final Move move = ((Move) model);
    final SymbolicPosition sourcePosition = move.getSourcePosition();
    if ((((!move.isPositionMove()) || move.getIncomingEdges().isEmpty()) || (null == sourcePosition))) {
      super.completeMove_TargetPosition(move, assignment, context, acceptor);
    } else {
      final Function1<Path, Iterable<? extends PathTargetReference>> _function = (Path it) -> {
        return it.getTargets();
      };
      Iterable<PathTargetReference> targetReferences = Queries.<Path, PathTargetReference>xcollect(sourcePosition.getOutgoingPaths(), _function);
      boolean _isStopAtTarget = move.isStopAtTarget();
      boolean _not = (!_isStopAtTarget);
      if (_not) {
        final Function1<PathTargetReference, Boolean> _function_1 = (PathTargetReference it) -> {
          return Boolean.valueOf(it.getSettling().isEmpty());
        };
        targetReferences = Queries.<PathTargetReference>select(targetReferences, _function_1);
      }
      final Function1<PathTargetReference, SymbolicPosition> _function_2 = (PathTargetReference it) -> {
        return it.getPosition();
      };
      final Set<SymbolicPosition> allowedPositions = IterableExtensions.<SymbolicPosition>toSet(Queries.<SymbolicPosition>excluding(Queries.<PathTargetReference, SymbolicPosition>xcollectOne(targetReferences, _function_2), sourcePosition));
      AbstractElement _terminal = assignment.getTerminal();
      final Predicate<IEObjectDescription> _function_3 = (IEObjectDescription it) -> {
        return allowedPositions.contains(it.getEObjectOrProxy());
      };
      this.lookupCrossReference(((CrossReference) _terminal), context, acceptor, _function_3);
    }
  }
  
  @Override
  public void completeMove_Profile(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    final Move move = ((Move) model);
    final SymbolicPosition sourcePosition = move.getSourcePosition();
    final SymbolicPosition targetPosition = move.getTargetPosition();
    if (((((!move.isPositionMove()) || move.getIncomingEdges().isEmpty()) || (null == sourcePosition)) || (null == targetPosition))) {
      super.completeMove_Profile(move, assignment, context, acceptor);
    } else {
      final Function1<Path, Boolean> _function = (Path it) -> {
        final Function1<PathTargetReference, SymbolicPosition> _function_1 = (PathTargetReference it_1) -> {
          return it_1.getPosition();
        };
        return Boolean.valueOf(Queries.<SymbolicPosition>includes(Queries.<PathTargetReference, SymbolicPosition>xcollectOne(it.getTargets(), _function_1), targetPosition));
      };
      final Function1<Path, EList<Profile>> _function_1 = (Path it) -> {
        return it.getProfiles();
      };
      final Set<Profile> allowedProfiles = IterableExtensions.<Profile>toSet(Queries.<Path, Profile>xcollect(Queries.<Path>select(sourcePosition.getOutgoingPaths(), _function), _function_1));
      AbstractElement _terminal = assignment.getTerminal();
      final Predicate<IEObjectDescription> _function_2 = (IEObjectDescription it) -> {
        return allowedProfiles.contains(it.getEObjectOrProxy());
      };
      this.lookupCrossReference(((CrossReference) _terminal), context, acceptor, _function_2);
    }
  }
  
  @Override
  public void completeActivity_Edges(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completeActivity_Edges(model, assignment, context, acceptor);
  }
  
  @Override
  public void completeRequireEvent_Resource(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    final Iterable<IResource> events = ActivityProposalProvider.events(model);
    AbstractElement _terminal = assignment.getTerminal();
    final Predicate<IEObjectDescription> _function = (IEObjectDescription it) -> {
      return IterableExtensions.contains(events, it.getEObjectOrProxy());
    };
    this.lookupCrossReference(((CrossReference) _terminal), context, acceptor, _function);
  }
  
  @Override
  public void completeRaiseEvent_Resource(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    final Iterable<IResource> events = ActivityProposalProvider.events(model);
    AbstractElement _terminal = assignment.getTerminal();
    final Predicate<IEObjectDescription> _function = (IEObjectDescription it) -> {
      return IterableExtensions.contains(events, it.getEObjectOrProxy());
    };
    this.lookupCrossReference(((CrossReference) _terminal), context, acceptor, _function);
  }
  
  @Override
  public void completeClaim_Resource(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    final Iterable<IResource> events = ActivityProposalProvider.events(model);
    AbstractElement _terminal = assignment.getTerminal();
    final Predicate<IEObjectDescription> _function = (IEObjectDescription it) -> {
      boolean _contains = IterableExtensions.contains(events, it.getEObjectOrProxy());
      return (!_contains);
    };
    this.lookupCrossReference(((CrossReference) _terminal), context, acceptor, _function);
  }
  
  @Override
  public void completeRelease_Resource(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    final Iterable<IResource> events = ActivityProposalProvider.events(model);
    AbstractElement _terminal = assignment.getTerminal();
    final Predicate<IEObjectDescription> _function = (IEObjectDescription it) -> {
      boolean _contains = IterableExtensions.contains(events, it.getEObjectOrProxy());
      return (!_contains);
    };
    this.lookupCrossReference(((CrossReference) _terminal), context, acceptor, _function);
  }
  
  public static Iterable<IResource> events(final EObject model) {
    Iterable<IResource> _xblockexpression = null;
    {
      EObject _eContainer = model.eContainer().eContainer();
      final ActivitySet sSet = ((ActivitySet) _eContainer);
      final Function1<Event, EList<ResourceItem>> _function = (Event it) -> {
        return it.getItems();
      };
      Iterable<ResourceItem> _flatMap = IterableExtensions.<Event, ResourceItem>flatMap(sSet.getEvents(), _function);
      EList<Event> _events = sSet.getEvents();
      _xblockexpression = Iterables.<IResource>concat(_flatMap, _events);
    }
    return _xblockexpression;
  }
}
