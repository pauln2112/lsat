/**
 */
package org.eclipse.lsat.common.graph.directed.editable;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getNode <em>Node</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getEdge <em>Edge</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getSourceReference()
 * @model
 * @generated
 */
public interface SourceReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getSourceReferences <em>Source References</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' reference.
	 * @see #setNode(Node)
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getSourceReference_Node()
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getSourceReferences
	 * @model opposite="sourceReferences" resolveProxies="false" required="true"
	 * @generated
	 */
	Node getNode();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getNode <em>Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node</em>' reference.
	 * @see #getNode()
	 * @generated
	 */
	void setNode(Node value);

	/**
	 * Returns the value of the '<em><b>Edge</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge</em>' container reference.
	 * @see #setEdge(Edge)
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getSourceReference_Edge()
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge#getSource
	 * @model opposite="source" resolveProxies="false" required="true" transient="false"
	 * @generated
	 */
	Edge getEdge();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getEdge <em>Edge</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edge</em>' container reference.
	 * @see #getEdge()
	 * @generated
	 */
	void setEdge(Edge value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getSourceReference_Name()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getName();

} // SourceReference
