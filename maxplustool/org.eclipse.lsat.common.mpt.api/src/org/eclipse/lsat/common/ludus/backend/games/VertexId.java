/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games;

/**
 * @author Bram van der Sanden
 * @param <V> vertex type
 */
public interface VertexId<V> {
    /**
     * Get the unique vertex id.
     *
     * @param vertex vertex whose unique id is to be returned.
     * @return the unique vertex id of the specified vertex.
     */
    Integer getId(V vertex);
}
