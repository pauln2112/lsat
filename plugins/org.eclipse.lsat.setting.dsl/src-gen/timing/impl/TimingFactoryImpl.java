/**
 */
package timing.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import timing.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimingFactoryImpl extends EFactoryImpl implements TimingFactory {
	/**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static TimingFactory init() {
        try
        {
            TimingFactory theTimingFactory = (TimingFactory)EPackage.Registry.INSTANCE.getEFactory(TimingPackage.eNS_URI);
            if (theTimingFactory != null)
            {
                return theTimingFactory;
            }
        }
        catch (Exception exception)
        {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new TimingFactoryImpl();
    }

	/**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public TimingFactoryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject create(EClass eClass) {
        switch (eClass.getClassifierID())
        {
            case TimingPackage.FIXED_VALUE: return createFixedValue();
            case TimingPackage.ARRAY: return createArray();
            case TimingPackage.TRIANGULAR_DISTRIBUTION: return createTriangularDistribution();
            case TimingPackage.PERT_DISTRIBUTION: return createPertDistribution();
            case TimingPackage.NORMAL_DISTRIBUTION: return createNormalDistribution();
            case TimingPackage.ENUMERATED_DISTRIBUTION: return createEnumeratedDistribution();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID())
        {
            case TimingPackage.CALCULATION_MODE:
                return createCalculationModeFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID())
        {
            case TimingPackage.CALCULATION_MODE:
                return convertCalculationModeToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public FixedValue createFixedValue() {
        FixedValueImpl fixedValue = new FixedValueImpl();
        return fixedValue;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Array createArray() {
        ArrayImpl array = new ArrayImpl();
        return array;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public TriangularDistribution createTriangularDistribution() {
        TriangularDistributionImpl triangularDistribution = new TriangularDistributionImpl();
        return triangularDistribution;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PertDistribution createPertDistribution() {
        PertDistributionImpl pertDistribution = new PertDistributionImpl();
        return pertDistribution;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NormalDistribution createNormalDistribution() {
        NormalDistributionImpl normalDistribution = new NormalDistributionImpl();
        return normalDistribution;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EnumeratedDistribution createEnumeratedDistribution() {
        EnumeratedDistributionImpl enumeratedDistribution = new EnumeratedDistributionImpl();
        return enumeratedDistribution;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public CalculationMode createCalculationModeFromString(EDataType eDataType, String initialValue) {
        CalculationMode result = CalculationMode.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertCalculationModeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public TimingPackage getTimingPackage() {
        return (TimingPackage)getEPackage();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
	@Deprecated
	public static TimingPackage getPackage() {
        return TimingPackage.eINSTANCE;
    }

} //TimingFactoryImpl
