/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.xtend.transformation

import java.text.DecimalFormat
import java.util.List

class Mpt2Dispatching {
    private new(){}

    def static createSequence(List<String> activitySequence, String phase) {
        return if (!activitySequence.empty)
            '''
            activities «phase.quote» {
                «FOR activity : activitySequence»
                    «activity»
                «ENDFOR»
            }
            '''
            else 
            '''
            // «phase» not available
            '''
    }

    def static round(Double d) {
        val df = new DecimalFormat("###.######");
        return df.format(d);
    }
    
    private def static quote( String str){
        str.matches("\\p{Alpha}\\w*") ? str : '"'+str+'"' 
    }
}
