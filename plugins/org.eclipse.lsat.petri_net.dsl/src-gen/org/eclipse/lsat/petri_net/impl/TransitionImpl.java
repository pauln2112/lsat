/**
 */
package org.eclipse.lsat.petri_net.impl;

import activity.SyncBar;
import activity.TracePoint;

import org.eclipse.lsat.common.graph.directed.impl.NodeImpl;
import org.eclipse.lsat.petri_net.PetriNetPackage;
import org.eclipse.lsat.petri_net.Transition;

import org.eclipse.lsat.trace.TraceLine;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.petri_net.impl.TransitionImpl#getTracePoint <em>Trace Point</em>}</li>
 *   <li>{@link org.eclipse.lsat.petri_net.impl.TransitionImpl#getTraceLines <em>Trace Lines</em>}</li>
 *   <li>{@link org.eclipse.lsat.petri_net.impl.TransitionImpl#getSyncBar <em>Sync Bar</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionImpl extends NodeImpl implements Transition {
	/**
	 * The cached value of the '{@link #getTracePoint() <em>Trace Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTracePoint()
	 * @generated
	 * @ordered
	 */
	protected TracePoint tracePoint;

	/**
	 * The cached value of the '{@link #getTraceLines() <em>Trace Lines</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTraceLines()
	 * @generated
	 * @ordered
	 */
	protected EList<TraceLine> traceLines;

	/**
	 * The cached value of the '{@link #getSyncBar() <em>Sync Bar</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSyncBar()
	 * @generated
	 * @ordered
	 */
	protected SyncBar syncBar;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PetriNetPackage.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TracePoint getTracePoint() {
		if (tracePoint != null && tracePoint.eIsProxy()) {
			InternalEObject oldTracePoint = (InternalEObject)tracePoint;
			tracePoint = (TracePoint)eResolveProxy(oldTracePoint);
			if (tracePoint != oldTracePoint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PetriNetPackage.TRANSITION__TRACE_POINT, oldTracePoint, tracePoint));
			}
		}
		return tracePoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TracePoint basicGetTracePoint() {
		return tracePoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTracePoint(TracePoint newTracePoint) {
		TracePoint oldTracePoint = tracePoint;
		tracePoint = newTracePoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PetriNetPackage.TRANSITION__TRACE_POINT, oldTracePoint, tracePoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TraceLine> getTraceLines() {
		if (traceLines == null) {
			traceLines = new EObjectResolvingEList<TraceLine>(TraceLine.class, this, PetriNetPackage.TRANSITION__TRACE_LINES);
		}
		return traceLines;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SyncBar getSyncBar() {
		if (syncBar != null && syncBar.eIsProxy()) {
			InternalEObject oldSyncBar = (InternalEObject)syncBar;
			syncBar = (SyncBar)eResolveProxy(oldSyncBar);
			if (syncBar != oldSyncBar) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PetriNetPackage.TRANSITION__SYNC_BAR, oldSyncBar, syncBar));
			}
		}
		return syncBar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncBar basicGetSyncBar() {
		return syncBar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSyncBar(SyncBar newSyncBar) {
		SyncBar oldSyncBar = syncBar;
		syncBar = newSyncBar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PetriNetPackage.TRANSITION__SYNC_BAR, oldSyncBar, syncBar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__TRACE_POINT:
				if (resolve) return getTracePoint();
				return basicGetTracePoint();
			case PetriNetPackage.TRANSITION__TRACE_LINES:
				return getTraceLines();
			case PetriNetPackage.TRANSITION__SYNC_BAR:
				if (resolve) return getSyncBar();
				return basicGetSyncBar();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__TRACE_POINT:
				setTracePoint((TracePoint)newValue);
				return;
			case PetriNetPackage.TRANSITION__TRACE_LINES:
				getTraceLines().clear();
				getTraceLines().addAll((Collection<? extends TraceLine>)newValue);
				return;
			case PetriNetPackage.TRANSITION__SYNC_BAR:
				setSyncBar((SyncBar)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__TRACE_POINT:
				setTracePoint((TracePoint)null);
				return;
			case PetriNetPackage.TRANSITION__TRACE_LINES:
				getTraceLines().clear();
				return;
			case PetriNetPackage.TRANSITION__SYNC_BAR:
				setSyncBar((SyncBar)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PetriNetPackage.TRANSITION__TRACE_POINT:
				return tracePoint != null;
			case PetriNetPackage.TRANSITION__TRACE_LINES:
				return traceLines != null && !traceLines.isEmpty();
			case PetriNetPackage.TRANSITION__SYNC_BAR:
				return syncBar != null;
		}
		return super.eIsSet(featureID);
	}

} //TransitionImpl
