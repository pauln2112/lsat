/**
 */
package lsat_graph.impl;

import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.graph.impl.TaskDependencyGraphImpl;

import dispatching.Dispatch;
import lsat_graph.DispatchGraph;
import lsat_graph.lsat_graphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dispatch Graph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.impl.DispatchGraphImpl#getDispatch <em>Dispatch</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DispatchGraphImpl extends TaskDependencyGraphImpl<Task> implements DispatchGraph {
	/**
     * The cached value of the '{@link #getDispatch() <em>Dispatch</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDispatch()
     * @generated
     * @ordered
     */
	protected Dispatch dispatch;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected DispatchGraphImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.DISPATCH_GRAPH;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Dispatch getDispatch() {
        if (dispatch != null && dispatch.eIsProxy())
        {
            InternalEObject oldDispatch = (InternalEObject)dispatch;
            dispatch = (Dispatch)eResolveProxy(oldDispatch);
            if (dispatch != oldDispatch)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, lsat_graphPackage.DISPATCH_GRAPH__DISPATCH, oldDispatch, dispatch));
            }
        }
        return dispatch;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Dispatch basicGetDispatch() {
        return dispatch;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setDispatch(Dispatch newDispatch) {
        Dispatch oldDispatch = dispatch;
        dispatch = newDispatch;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, lsat_graphPackage.DISPATCH_GRAPH__DISPATCH, oldDispatch, dispatch));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case lsat_graphPackage.DISPATCH_GRAPH__DISPATCH:
                if (resolve) return getDispatch();
                return basicGetDispatch();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case lsat_graphPackage.DISPATCH_GRAPH__DISPATCH:
                setDispatch((Dispatch)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.DISPATCH_GRAPH__DISPATCH:
                setDispatch((Dispatch)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.DISPATCH_GRAPH__DISPATCH:
                return dispatch != null;
        }
        return super.eIsSet(featureID);
    }

} //DispatchGraphImpl
