/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance.internal;

import org.eclipse.lsat.trace.TraceLine;

import activity.TracePoint;

/**
 * As each trace line in a log should be matched with all tracepoints it saves a lot of time to filter out the
 * duplicates. This wrapper is created as it is not allowed to override the equals/hashcode methods in an EObject.
 */
public class UniqueTracePoint {
    /**
     * Safely creates an {@link UniqueTracePoint}, returning <tt>null</tt> if <tt>tracePoint</tt> is <tt>null</tt>.
     */
    public static UniqueTracePoint create(TracePoint tracePoint) {
        if (null == tracePoint) {
            return null;
        }
        return new UniqueTracePoint(tracePoint);
    }

    public static boolean matchesOneOf(TraceLine traceLine, Iterable<UniqueTracePoint> tracePoints) {
        for (UniqueTracePoint uTracePoint: tracePoints) {
            if (matches(traceLine, uTracePoint)) {
                return true;
            }
        }
        return false;
    }

    public static boolean matches(TraceLine traceLine, UniqueTracePoint tracePoint) {
        return matches(traceLine, null == tracePoint ? null : tracePoint.delegate);
    }

    public static boolean matches(TraceLine traceLine, TracePoint tracePoint) {
        if (null == tracePoint && null == traceLine) {
            // It's a match when no trace line is required and none is provided
            return true;
        }
        if (null != tracePoint && null != traceLine) {
            // Required and provided, look for a match
            return tracePoint.isRegex() ? traceLine.getTracePoint().matches(tracePoint.getValue())
                    : traceLine.getTracePoint().endsWith(tracePoint.getValue());
        }
        // Either required or provided, no match
        return false;
    }

    private final TracePoint delegate;

    private UniqueTracePoint(TracePoint delegate) {
        if (null == delegate)
            throw new IllegalArgumentException("Null not allowed!");
        this.delegate = delegate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (delegate.isRegex() ? 1231 : 1237);
        result = prime * result + ((delegate.getValue() == null) ? 0 : delegate.getValue().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UniqueTracePoint other = (UniqueTracePoint)obj;
        if (delegate.isRegex() != other.delegate.isRegex())
            return false;
        if (delegate.getValue() == null) {
            if (other.delegate.getValue() != null)
                return false;
        } else if (!delegate.getValue().equals(other.delegate.getValue()))
            return false;
        return true;
    }
}
