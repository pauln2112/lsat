/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import activity.util.ActivityUtil
import dispatching.ActivityDispatching
import org.eclipse.emf.ecore.util.EcoreUtil
import activity.Activity
import activity.ActivitySet
import dispatching.Dispatch

class ExpandActivities {

     private new(){}
     
   /**
   *  If {@link Dispatch} instances contain {@link ResourceItem}s then {@link Activity} are expanded.
   * 
   * @return A copy with expanded activities or the original if there is nothing to do.
   */
    static def ActivityDispatching transformModel(ActivityDispatching dispatching) {
       val out = EcoreUtil.copy(dispatching);
       val remove = newLinkedHashSet

       out.dispatchGroups.flatMap[dispatches].filter[!resourceItems.isEmpty].forEach[
           remove+=activity
           activity = ActivityUtil.queryCreateExpandedActivity(activity, resourceItems)
           resourceItems.clear
       ]   
       if(remove.isEmpty){
           //return original dispatch as there has nothing changed
           return dispatching;
       }
       remove.forEach[(eContainer as ActivitySet).activities -= it] 
       return out
    }
}
