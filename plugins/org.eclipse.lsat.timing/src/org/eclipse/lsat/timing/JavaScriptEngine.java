/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing;

import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * An Engine that executes JavaScript.
 */
public class JavaScriptEngine {

    private final ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
    private final ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("JavaScript");

    public Object eval(String script, Map<String, Object> args) throws ScriptException {
        Bindings scriptEngineBindings = scriptEngine.createBindings();
        scriptEngineBindings.putAll(args);
        try {
            return scriptEngine.eval(script, scriptEngineBindings);
        } finally {
            args.putAll(scriptEngineBindings);
        }
    }

}
