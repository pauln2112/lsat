/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ui;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.ui.action.ValidateAction.EclipseResourcesUtil;

public class AdapterFactoryDiagnostician extends Diagnostician {
    private final AdapterFactory adapterFactory;

    public AdapterFactoryDiagnostician(AdapterFactory adapterFactory) {
        super();
        this.adapterFactory = adapterFactory;
    }

    public AdapterFactoryDiagnostician(Registry eValidatorRegistry, AdapterFactory adapterFactory) {
        super(eValidatorRegistry);
        this.adapterFactory = adapterFactory;
    }

    @Override
    public String getObjectLabel(EObject eObject) {
        IItemLabelProvider itemLabelProvider = (IItemLabelProvider)adapterFactory.adapt(eObject,
                IItemLabelProvider.class);
        if (itemLabelProvider != null) {
            return itemLabelProvider.getText(eObject);
        }
        return super.getObjectLabel(eObject);
    }

    public Diagnostic validate(Resource resource, boolean createMarkers) {
        BasicDiagnostic resourceDiagnostic = new BasicDiagnostic(EObjectValidator.DIAGNOSTIC_SOURCE, 0,
                "Diagnosis of " + resource.getURI(), new Object[]
                {resource});
        for (EObject eObject: resource.getContents()) {
            Diagnostic eObjectDiagnostic = validate(eObject);
            resourceDiagnostic.addAll(eObjectDiagnostic);
        }
        if (createMarkers) {
            EclipseResourcesUtil util = new EclipseResourcesUtil();
            util.deleteMarkers(resource);
            for (Diagnostic diagnostic: resourceDiagnostic.getChildren()) {
                util.createMarkers(resource, diagnostic);
            }
        }
        return resourceDiagnostic;
    }
}
