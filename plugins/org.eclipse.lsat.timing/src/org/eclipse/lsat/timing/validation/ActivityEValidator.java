/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.validation;

import static org.eclipse.emf.common.util.Diagnostic.WARNING;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.lsat.common.emf.common.util.BufferedDiagnosticChain;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.lsat.timing.util.MotionCalculatorHelper;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.CheckType;

import activity.Activity;
import activity.ActivitySet;
import activity.Move;
import machine.Activator;
import machine.Path;
import setting.SettingUtil;
import setting.Settings;

/**
 * This validator validates whether:
 * <ul>
 * <li>Peripheral contains incompatible paths, see {@link #getIncompatibleTargetReferences(Path, Path)}</li>
 * </ul>
 */
public class ActivityEValidator implements EValidator {
    public static final ActivityEValidator INSTANCE = new ActivityEValidator();

    @Override
    public boolean validate(EDataType eDataType, Object value, DiagnosticChain diagnostics,
            Map<Object, Object> context)
    {
        return true;
    }

    @Override
    public boolean validate(EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return null == eObject ? true : validate(eObject.eClass(), eObject, diagnostics, context);
    }

    @Override
    public boolean validate(EClass eClass, EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        CheckMode checkMode = CheckMode.getCheckMode(context);
        // do not check in fast mode as this validation uses the motion calculator which might be an expensive operation
        if (!(checkMode.shouldCheck(CheckType.NORMAL)) || checkMode.shouldCheck(CheckType.EXPENSIVE)) {
            return true;
        }
        if (!(EcoreUtil.getRootContainer(eObject) instanceof ActivitySet)) {
            return true;
        }
        BufferedDiagnosticChain bufferedDiagnostics = new BufferedDiagnosticChain(diagnostics);
        validateActivitySet((ActivitySet)EcoreUtil.getRootContainer(eObject), bufferedDiagnostics);
        return bufferedDiagnostics.getMaxSeverity() < WARNING;
    }

    private void validateActivitySet(ActivitySet activitySet, DiagnosticChain diagnostics) {
        try {
            Settings settings = SettingUtil.getSettings(activitySet);
            MotionCalculatorExtension motionCalculator = MotionCalculatorExtension.getSelectedMotionCalculator();
            MotionCalculatorHelper helper = new MotionCalculatorHelper(settings, motionCalculator);
            for (Activity activity: activitySet.getActivities()) {
                validateActivity(activity, helper, diagnostics);
            }
        } catch (IOException | MotionException e) {
            writeDiagnostic(diagnostics, Diagnostic.INFO, "Motion calculator validation disabled: " + e.getMessage(),
                    activitySet);
        }
    }

    private void validateActivity(Activity activity, MotionCalculatorHelper helper, DiagnosticChain diagnostics) {
        Set<Move> seen = new LinkedHashSet<>();
        try {
            activity.allNodesInTopologicalOrder().stream().filter(Move.class::isInstance).map(Move.class::cast)
                    .forEach(move -> {
                        if (seen.add(move)) {
                            List<Move> concatenatedMove = helper.getConcatenatedMove(move);
                            seen.addAll(concatenatedMove);
                            try {
                                helper.validate(concatenatedMove);
                            } catch (SpecificationException e) {
                                for (EObject eObject: e.getEObjects()) {
                                    writeDiagnostic(diagnostics, Diagnostic.ERROR, e.getMessage(), eObject);
                                }
                            }
                        }
                    });
        } catch (Exception e) {
            // Most probably cause by an invalid syntax while editing. So skip it
        }
    }

    private void writeDiagnostic(DiagnosticChain diagnostics, int severity, String message, EObject... data) {
        diagnostics.add(new BasicDiagnostic(severity, Activator.BUNDLE_NAME, 0, message, data));
    }
}
