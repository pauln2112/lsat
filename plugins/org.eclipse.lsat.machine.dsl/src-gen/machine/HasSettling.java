/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Settling</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.HasSettling#getSettling <em>Settling</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getHasSettling()
 * @model abstract="true"
 * @generated
 */
public interface HasSettling extends EObject {
	/**
     * Returns the value of the '<em><b>Settling</b></em>' reference list.
     * The list contents are of type {@link machine.Axis}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Settling</em>' reference list.
     * @see machine.MachinePackage#getHasSettling_Settling()
     * @model
     * @generated
     */
	EList<Axis> getSettling();

} // HasSettling
