/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package activity.util;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.lsat.common.graph.directed.editable.EdgFactory;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.graph.directed.editable.SourceReference;
import org.eclipse.lsat.common.graph.directed.editable.TargetReference;

import activity.Action;
import activity.Activity;
import activity.ActivityFactory;
import activity.ActivitySet;
import activity.Claim;
import activity.Release;
import activity.ResourceAction;
import activity.SyncBar;
import activity.impl.ActivityQueries;
import machine.IResource;
import machine.Resource;
import machine.ResourceItem;
import machine.ResourceType;

public final class ActivityUtil {
    private static final String ITEM_DELIMITER = "_";

    public static final String EXPAND_DELIMITER = "_";

    private ActivityUtil() {
        /* Empty */
    }

    /**
     * @return an activityName that is postFix with the resource names splitted with "__"
     */
    public static String expandName(Activity activity, Collection<ResourceItem> resources) {
        String name = activity.getName();

        if (!resources.isEmpty()) {
            // make sure name is sorted in resources order.
            return activity.getResourcesNeedingItem().stream().map(
                    r -> resources.stream().filter(i -> i.getResource() == r).map(IResource::getName).findFirst().get())
                    .collect(Collectors.joining(ITEM_DELIMITER, name + ITEM_DELIMITER, ""));
        }
        return name;
    }

    /**
     * @return an activityName with possible items between brackets
     *
     */
    public static String displayName(Activity activity) {
        if (activity == null) {
            return null;
        }
        if (activity.getOriginalName() == null) {
            return activity.getName();
        }
        return getItemNames(activity.getOriginalName(), activity.getName()).stream()
                .collect(Collectors.joining(",", activity.getOriginalName() + "[", "]"));
    }

    /**
     * @return the resource item names in an expandedName of
     */
    public static Collection<String> getItemNames(String orgActivityName, String expandedActivityName) {
        if (expandedActivityName.startsWith(orgActivityName + EXPAND_DELIMITER)) {
            return Arrays.asList(expandedActivityName.substring(orgActivityName.length() + EXPAND_DELIMITER.length())
                    .split(ITEM_DELIMITER));
        }
        return Collections.emptyList();
    }

    /**
     * Replaces {@link Resource}s with {@link ResourceItem}s and adds the activity to the ActivitySet if not already
     * added.
     *
     * @param orgActivity the non expanded activity
     * @param items concrete {@link ResourceItem}s
     * @return the expanded activity
     */
    public static Activity queryCreateExpandedActivity(final Activity orgActivity,
            final Collection<ResourceItem> items)
    {
        EObject eContainer = orgActivity.eContainer();
        if (eContainer == null) {
            throw new RuntimeException("Activity is not part of a container");
        }
        if (!(eContainer instanceof ActivitySet)) {
            throw new RuntimeException("Activity must be part of an ActivitySet");
        }
        final ActivitySet set = ((ActivitySet)eContainer);
        final EList<Activity> activities = set.getActivities();
        final String activityName = ActivityUtil.expandName(orgActivity, items);
        Activity result = from(set.getActivities()).select(a -> a.getName().equals(activityName)).first();
        if (result == null) {
            result = expand(orgActivity, items);
            activities.add(activities.indexOf(orgActivity), result);
        }
        return result;
    }

    private static Activity expand(final Activity activity, final Collection<ResourceItem> resources) {
        final Activity result = EcoreUtil.copy(activity);
        result.setOriginalName(activity.getName());
        result.setName(expandName(activity, resources));
        String expansion = result.getName().substring(activity.getName().length());
        from(resources).forEach(item -> {
            from(result.getNodes()).objectsOfKind(ResourceAction.class)
                    .select(a -> a.getResource().equals(item.getResource())).forEach(a -> a.setResource(item));
            from(result.getPrerequisites()).select(p -> p.getResource().equals(item.getResource()))
                    .forEach(p -> p.setResource(item));
        });
        return result;
    }

    public static void addEdge(Activity activity, Node source, Node target) {
        if (!edgeExists(activity, source, target)) {
            Edge edge = EdgFactory.eINSTANCE.createEdge();
            SourceReference sourceRef = EdgFactory.eINSTANCE.createSourceReference();
            sourceRef.setNode(source);
            edge.setSource(sourceRef);
            TargetReference targetRef = EdgFactory.eINSTANCE.createTargetReference();
            targetRef.setNode(target);
            edge.setTarget(targetRef);
            activity.getEdges().add(edge);
        }
    }

    public static <T extends Node> void process(ActivitySet activitySet, Class<T> clazz, Consumer<? super T> predicate ) {
        activitySet.getActivities().forEach(a->process(a, clazz, predicate));
    }

    public static <T extends Node> void process(Activity activity, Class<T> clazz, Consumer<? super T> predicate ) {
        new ArrayList<>(activity.getNodes()).stream()
        .filter(clazz::isInstance)
        .map(clazz::cast)
        .distinct()
        .forEach(predicate);
    }

    private static boolean edgeExists(Activity activity, Node source, Node target) {
        return activity.getEdges().stream().anyMatch(e -> (e.getSource() == source && e.getTarget() == target));
    }

    public static SyncBar addSyncBar(Activity activity) {
        SyncBar syncBar = ActivityFactory.eINSTANCE.createSyncBar();
        syncBar.setName(getFirstFreeName("S", activity));
        activity.getNodes().add(syncBar);
        return syncBar;
    }

    public static Release addRelease(Activity activity, IResource resource) {
        if (getReleases(activity, resource).isEmpty()) {
            Release release = ActivityFactory.eINSTANCE.createRelease();
            release.setName(getFirstFreeName("R", activity));
            release.setResource(resource);
            activity.getNodes().add(release);
        }
        return getReleases(activity, resource).iterator().next();
    }

    public static Claim addClaim(Activity activity, IResource resource) {
        if (getClaims(activity, resource).isEmpty()) {
            Claim claim = ActivityFactory.eINSTANCE.createClaim();
            claim.setName(getFirstFreeName("C", activity));
            claim.setResource(resource);
            activity.getNodes().add(claim);
        }
        return getClaims(activity, resource).iterator().next();
    }

    /**
     * Inserts a SyncBar if there are multiple incoming or outgoing edges.
     * The edges are rerouted
     */
    public static void insertSyncBars(Action action) {
        Activity activity = (Activity)action.getGraph();
        if (action.getIncomingEdges().size() > 1) {
            SyncBar syncBar = ActivityUtil.addSyncBar(activity);
            action.getIncomingEdges().stream().map(Edge::getSourceNode).forEach(s->addEdge(activity, s, syncBar));
            new ArrayList<>(action.getIncomingEdges()).forEach(ActivityUtil::delete);
            addEdge(activity, syncBar, action);
        }
        if (action.getOutgoingEdges().size() > 1) {
            SyncBar syncBar = ActivityUtil.addSyncBar(activity);
            action.getOutgoingEdges().stream().map(Edge::getTargetNode).forEach(s->addEdge(activity, syncBar, s));
            new ArrayList<>(action.getOutgoingEdges()).forEach(ActivityUtil::delete);
            addEdge(activity, action, syncBar);
        }
    }

    public static void delete(Node node) {
        for (Edge edge: node.getIncomingEdges()) {
            delete(edge);
        }
        for (Edge edge: node.getOutgoingEdges()) {
            delete(edge);
        }
        EcoreUtil.delete(node);
    }

    public static void delete(Edge edge) {
        if (edge.getTarget() instanceof Edge) {
            edge.getGraph().getEdges().add((Edge)edge.getTarget());
        }
        if (null != edge.getEdge()) {
            TargetReference target = EdgFactory.eINSTANCE.createTargetReference();
            target.setNode(edge.getSource().getNode());
            edge.getEdge().setTarget(target);
        }
        // this is really needed to clean up all relations:
        edge.setTarget(null);
        edge.setSource(null);
        EcoreUtil.delete(edge);
    }

    public static String getFirstFreeName(String prefix, final Activity activity) {
        Collection<String> names = activity.getNodes().stream().map(n -> n.getName()).collect(Collectors.toSet());
        int i = 1;
        while (names.contains(prefix + i)) {
            i++;
        }
        return prefix + i;
    }

    public static String getPassiveName(IResource resource) {
        if ( resource.getResource().getResourceType() == ResourceType.PASSIVE) {
            return resource.getName();
        }
        return ResourceType.PASSIVE.getName() + '_' + resource.fqn().replace('.', '_');
    }

    /**
     * Get claims for a certain resource (typically 1)
     */
    public static Collection<Claim> getClaims(Activity activity, IResource resource) {
        return ActivityQueries.getActionsFor(resource, Claim.class, activity.getNodes()).asList();
    }

    /**
     * Get releases for a certain resource (typically 1)
     */
    public static Collection<Release> getReleases(Activity activity, IResource resource) {
        return ActivityQueries.getActionsFor(resource, Release.class, activity.getNodes()).asList();
    }
}
