/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.contributions.ui.project;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;

public class LogisticsProjectNature implements IProjectNature {
    public static final String NATURE_ID = "org.eclipse.lsat.LogisticsProjectNature";

    private IProject project = null;

    @Override
    public void configure() throws CoreException {
        // only called once the nature has been set

        // configure the project...
    }

    @Override
    public void deconfigure() throws CoreException {
        // only called once the nature has been set

        // reset the project configuration...
    }

    @Override
    public IProject getProject() {
        return this.project;
    }

    @Override
    public void setProject(IProject project) {
        this.project = project;
    }
}
