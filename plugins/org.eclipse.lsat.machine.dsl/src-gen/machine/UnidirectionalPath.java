/**
 */
package machine;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unidirectional Path</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.UnidirectionalPath#getSource <em>Source</em>}</li>
 *   <li>{@link machine.UnidirectionalPath#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getUnidirectionalPath()
 * @model
 * @generated
 */
public interface UnidirectionalPath extends Path {
	/**
     * Returns the value of the '<em><b>Source</b></em>' reference.
     * It is bidirectional and its opposite is '{@link machine.SymbolicPosition#getSourceReferences <em>Source References</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Source</em>' reference.
     * @see #setSource(SymbolicPosition)
     * @see machine.MachinePackage#getUnidirectionalPath_Source()
     * @see machine.SymbolicPosition#getSourceReferences
     * @model opposite="sourceReferences" resolveProxies="false" required="true"
     * @generated
     */
	SymbolicPosition getSource();

	/**
     * Sets the value of the '{@link machine.UnidirectionalPath#getSource <em>Source</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Source</em>' reference.
     * @see #getSource()
     * @generated
     */
	void setSource(SymbolicPosition value);

	/**
     * Returns the value of the '<em><b>Target</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Target</em>' containment reference.
     * @see #setTarget(PathTargetReference)
     * @see machine.MachinePackage#getUnidirectionalPath_Target()
     * @model containment="true" required="true"
     * @generated
     */
	PathTargetReference getTarget();

	/**
     * Sets the value of the '{@link machine.UnidirectionalPath#getTarget <em>Target</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Target</em>' containment reference.
     * @see #getTarget()
     * @generated
     */
	void setTarget(PathTargetReference value);

} // UnidirectionalPath
