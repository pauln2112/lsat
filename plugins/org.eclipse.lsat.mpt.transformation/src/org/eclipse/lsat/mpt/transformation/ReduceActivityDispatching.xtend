/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.transformation;

import activity.Activity
import activity.ActivitySet
import activity.util.ActivityUtil
import dispatching.ActivityDispatching
import java.util.List
import machine.MachineFactory

/**
 * Add a named Activity for all combinations of ResourceItems.
 * The original (unusable) activity is removed.
 */
final class ReduceActivityDispatching {

    private new(){}
    /**
     * Reduces expanded activities back to their original format stored on disk.
     * Activities refer to the supplied activitySet
     */
    static def void reduce(ActivityDispatching activityDispatching, ActivitySet activitySet) {
        //collect activities
        val allActivities = newHashSet
        allActivities += activitySet.activities
        allActivities += activitySet.loadAll.filter(ActivitySet).flatMap[activities];
        val expandMap = allActivities.toList.expandNames
        val importActivitySet = MachineFactory.eINSTANCE.createImport();
        // make sure activitySet resource can be resolved.
        importActivitySet.importURI = activitySet.eResource.URI.toString
        activityDispatching.imports += importActivitySet
        activityDispatching.dispatchGroups.flatMap[dispatches].forEach [ dis |
            val activity = expandMap.get(dis.activity.name);
            val itemNames = ActivityUtil.getItemNames(activity.name, dis.activity.name);
            val resourceNeedingItem = activity.resourcesNeedingItem
            if (itemNames.size != resourceNeedingItem.size) {
                throw new RuntimeException("Cannot convert expanded activity")
            }
            val iter = resourceNeedingItem.iterator
            for (itemName : itemNames) {
                val resourceItem = iter.next().items.findFirst[name == itemName]
                if (resourceItem === null) {
                    throw new RuntimeException("Cannot find item with name " + itemName)
                }
                dis.resourceItems += resourceItem
            }
            dis.activity = activity
        ]
        activityDispatching.imports -= importActivitySet
        // replace activity import with reduced version.
        // this construction is used to keep comment around it at the right place
         activityDispatching.imports.findFirst[importURI.contains(".activity")].importURI = activitySet.eResource.URI.toString
    }

    /**
     * returns a map with key all possible activity names and activity as values
     */
    private static def expandNames(List<Activity> activities) {
        ExpandActivities.expandNames(activities)
    }

}
