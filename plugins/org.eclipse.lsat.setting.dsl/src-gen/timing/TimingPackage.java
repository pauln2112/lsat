/**
 */
package timing;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see timing.TimingFactory
 * @model kind="package"
 * @generated
 */
public interface TimingPackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "timing";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://www.eclipse.org/lsat/timing";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "timing";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	TimingPackage eINSTANCE = timing.impl.TimingPackageImpl.init();

	/**
     * The meta object id for the '{@link timing.Timing <em>Timing</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see timing.Timing
     * @see timing.impl.TimingPackageImpl#getTiming()
     * @generated
     */
	int TIMING = 0;

	/**
     * The number of structural features of the '<em>Timing</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIMING_FEATURE_COUNT = 0;

	/**
     * The number of operations of the '<em>Timing</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TIMING_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link timing.impl.FixedValueImpl <em>Fixed Value</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see timing.impl.FixedValueImpl
     * @see timing.impl.TimingPackageImpl#getFixedValue()
     * @generated
     */
	int FIXED_VALUE = 1;

	/**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FIXED_VALUE__VALUE = TIMING_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Value Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FIXED_VALUE__VALUE_EXP = TIMING_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Fixed Value</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FIXED_VALUE_FEATURE_COUNT = TIMING_FEATURE_COUNT + 2;

	/**
     * The number of operations of the '<em>Fixed Value</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FIXED_VALUE_OPERATION_COUNT = TIMING_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link timing.impl.ArrayImpl <em>Array</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see timing.impl.ArrayImpl
     * @see timing.impl.TimingPackageImpl#getArray()
     * @generated
     */
	int ARRAY = 2;

	/**
     * The feature id for the '<em><b>Values</b></em>' attribute list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ARRAY__VALUES = TIMING_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Values Exp</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ARRAY__VALUES_EXP = TIMING_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Array</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ARRAY_FEATURE_COUNT = TIMING_FEATURE_COUNT + 2;

	/**
     * The number of operations of the '<em>Array</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ARRAY_OPERATION_COUNT = TIMING_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link timing.impl.DistributionImpl <em>Distribution</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see timing.impl.DistributionImpl
     * @see timing.impl.TimingPackageImpl#getDistribution()
     * @generated
     */
	int DISTRIBUTION = 4;

	/**
     * The feature id for the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTRIBUTION__DEFAULT = TIMING_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Default Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTRIBUTION__DEFAULT_EXP = TIMING_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTRIBUTION_FEATURE_COUNT = TIMING_FEATURE_COUNT + 2;

	/**
     * The number of operations of the '<em>Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTRIBUTION_OPERATION_COUNT = TIMING_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link timing.impl.TriangularDistributionImpl <em>Triangular Distribution</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see timing.impl.TriangularDistributionImpl
     * @see timing.impl.TimingPackageImpl#getTriangularDistribution()
     * @generated
     */
	int TRIANGULAR_DISTRIBUTION = 3;

	/**
     * The feature id for the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION__DEFAULT = DISTRIBUTION__DEFAULT;

	/**
     * The feature id for the '<em><b>Default Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION__DEFAULT_EXP = DISTRIBUTION__DEFAULT_EXP;

	/**
     * The feature id for the '<em><b>Mode</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION__MODE = DISTRIBUTION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Min</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION__MIN = DISTRIBUTION_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Max</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION__MAX = DISTRIBUTION_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Mode Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION__MODE_EXP = DISTRIBUTION_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Min Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION__MIN_EXP = DISTRIBUTION_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Max Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION__MAX_EXP = DISTRIBUTION_FEATURE_COUNT + 5;

	/**
     * The number of structural features of the '<em>Triangular Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION_FEATURE_COUNT = DISTRIBUTION_FEATURE_COUNT + 6;

	/**
     * The number of operations of the '<em>Triangular Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRIANGULAR_DISTRIBUTION_OPERATION_COUNT = DISTRIBUTION_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link timing.impl.PertDistributionImpl <em>Pert Distribution</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see timing.impl.PertDistributionImpl
     * @see timing.impl.TimingPackageImpl#getPertDistribution()
     * @generated
     */
	int PERT_DISTRIBUTION = 5;

	/**
     * The feature id for the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__DEFAULT = DISTRIBUTION__DEFAULT;

	/**
     * The feature id for the '<em><b>Default Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__DEFAULT_EXP = DISTRIBUTION__DEFAULT_EXP;

	/**
     * The feature id for the '<em><b>Mode</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__MODE = DISTRIBUTION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Min</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__MIN = DISTRIBUTION_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Max</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__MAX = DISTRIBUTION_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Mode Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__MODE_EXP = DISTRIBUTION_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Min Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__MIN_EXP = DISTRIBUTION_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Max Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__MAX_EXP = DISTRIBUTION_FEATURE_COUNT + 5;

	/**
     * The feature id for the '<em><b>Gamma</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__GAMMA = DISTRIBUTION_FEATURE_COUNT + 6;

	/**
     * The feature id for the '<em><b>Gamma Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION__GAMMA_EXP = DISTRIBUTION_FEATURE_COUNT + 7;

	/**
     * The number of structural features of the '<em>Pert Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION_FEATURE_COUNT = DISTRIBUTION_FEATURE_COUNT + 8;

	/**
     * The number of operations of the '<em>Pert Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERT_DISTRIBUTION_OPERATION_COUNT = DISTRIBUTION_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link timing.impl.NormalDistributionImpl <em>Normal Distribution</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see timing.impl.NormalDistributionImpl
     * @see timing.impl.TimingPackageImpl#getNormalDistribution()
     * @generated
     */
	int NORMAL_DISTRIBUTION = 6;

	/**
     * The feature id for the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NORMAL_DISTRIBUTION__DEFAULT = DISTRIBUTION__DEFAULT;

	/**
     * The feature id for the '<em><b>Default Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NORMAL_DISTRIBUTION__DEFAULT_EXP = DISTRIBUTION__DEFAULT_EXP;

	/**
     * The feature id for the '<em><b>Mean</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NORMAL_DISTRIBUTION__MEAN = DISTRIBUTION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Sd</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NORMAL_DISTRIBUTION__SD = DISTRIBUTION_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Mean Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NORMAL_DISTRIBUTION__MEAN_EXP = DISTRIBUTION_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Sd Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NORMAL_DISTRIBUTION__SD_EXP = DISTRIBUTION_FEATURE_COUNT + 3;

	/**
     * The number of structural features of the '<em>Normal Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NORMAL_DISTRIBUTION_FEATURE_COUNT = DISTRIBUTION_FEATURE_COUNT + 4;

	/**
     * The number of operations of the '<em>Normal Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int NORMAL_DISTRIBUTION_OPERATION_COUNT = DISTRIBUTION_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link timing.impl.EnumeratedDistributionImpl <em>Enumerated Distribution</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see timing.impl.EnumeratedDistributionImpl
     * @see timing.impl.TimingPackageImpl#getEnumeratedDistribution()
     * @generated
     */
	int ENUMERATED_DISTRIBUTION = 7;

	/**
     * The feature id for the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENUMERATED_DISTRIBUTION__DEFAULT = DISTRIBUTION__DEFAULT;

	/**
     * The feature id for the '<em><b>Default Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENUMERATED_DISTRIBUTION__DEFAULT_EXP = DISTRIBUTION__DEFAULT_EXP;

	/**
     * The feature id for the '<em><b>Values</b></em>' attribute list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENUMERATED_DISTRIBUTION__VALUES = DISTRIBUTION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Values Exp</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENUMERATED_DISTRIBUTION__VALUES_EXP = DISTRIBUTION_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Enumerated Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENUMERATED_DISTRIBUTION_FEATURE_COUNT = DISTRIBUTION_FEATURE_COUNT + 2;

	/**
     * The number of operations of the '<em>Enumerated Distribution</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ENUMERATED_DISTRIBUTION_OPERATION_COUNT = DISTRIBUTION_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link timing.CalculationMode <em>Calculation Mode</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see timing.CalculationMode
     * @see timing.impl.TimingPackageImpl#getCalculationMode()
     * @generated
     */
	int CALCULATION_MODE = 8;


	/**
     * Returns the meta object for class '{@link timing.Timing <em>Timing</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Timing</em>'.
     * @see timing.Timing
     * @generated
     */
	EClass getTiming();

	/**
     * Returns the meta object for class '{@link timing.FixedValue <em>Fixed Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Fixed Value</em>'.
     * @see timing.FixedValue
     * @generated
     */
	EClass getFixedValue();

	/**
     * Returns the meta object for the attribute '{@link timing.FixedValue#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see timing.FixedValue#getValue()
     * @see #getFixedValue()
     * @generated
     */
	EAttribute getFixedValue_Value();

	/**
     * Returns the meta object for the containment reference '{@link timing.FixedValue#getValueExp <em>Value Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Value Exp</em>'.
     * @see timing.FixedValue#getValueExp()
     * @see #getFixedValue()
     * @generated
     */
	EReference getFixedValue_ValueExp();

	/**
     * Returns the meta object for class '{@link timing.Array <em>Array</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Array</em>'.
     * @see timing.Array
     * @generated
     */
	EClass getArray();

	/**
     * Returns the meta object for the attribute list '{@link timing.Array#getValues <em>Values</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute list '<em>Values</em>'.
     * @see timing.Array#getValues()
     * @see #getArray()
     * @generated
     */
	EAttribute getArray_Values();

	/**
     * Returns the meta object for the containment reference list '{@link timing.Array#getValuesExp <em>Values Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Values Exp</em>'.
     * @see timing.Array#getValuesExp()
     * @see #getArray()
     * @generated
     */
	EReference getArray_ValuesExp();

	/**
     * Returns the meta object for class '{@link timing.TriangularDistribution <em>Triangular Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Triangular Distribution</em>'.
     * @see timing.TriangularDistribution
     * @generated
     */
	EClass getTriangularDistribution();

	/**
     * Returns the meta object for the attribute '{@link timing.TriangularDistribution#getMode <em>Mode</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Mode</em>'.
     * @see timing.TriangularDistribution#getMode()
     * @see #getTriangularDistribution()
     * @generated
     */
	EAttribute getTriangularDistribution_Mode();

	/**
     * Returns the meta object for the attribute '{@link timing.TriangularDistribution#getMin <em>Min</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Min</em>'.
     * @see timing.TriangularDistribution#getMin()
     * @see #getTriangularDistribution()
     * @generated
     */
	EAttribute getTriangularDistribution_Min();

	/**
     * Returns the meta object for the attribute '{@link timing.TriangularDistribution#getMax <em>Max</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Max</em>'.
     * @see timing.TriangularDistribution#getMax()
     * @see #getTriangularDistribution()
     * @generated
     */
	EAttribute getTriangularDistribution_Max();

	/**
     * Returns the meta object for the containment reference '{@link timing.TriangularDistribution#getModeExp <em>Mode Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Mode Exp</em>'.
     * @see timing.TriangularDistribution#getModeExp()
     * @see #getTriangularDistribution()
     * @generated
     */
	EReference getTriangularDistribution_ModeExp();

	/**
     * Returns the meta object for the containment reference '{@link timing.TriangularDistribution#getMinExp <em>Min Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Min Exp</em>'.
     * @see timing.TriangularDistribution#getMinExp()
     * @see #getTriangularDistribution()
     * @generated
     */
	EReference getTriangularDistribution_MinExp();

	/**
     * Returns the meta object for the containment reference '{@link timing.TriangularDistribution#getMaxExp <em>Max Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Max Exp</em>'.
     * @see timing.TriangularDistribution#getMaxExp()
     * @see #getTriangularDistribution()
     * @generated
     */
	EReference getTriangularDistribution_MaxExp();

	/**
     * Returns the meta object for class '{@link timing.Distribution <em>Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Distribution</em>'.
     * @see timing.Distribution
     * @generated
     */
	EClass getDistribution();

	/**
     * Returns the meta object for the attribute '{@link timing.Distribution#getDefault <em>Default</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Default</em>'.
     * @see timing.Distribution#getDefault()
     * @see #getDistribution()
     * @generated
     */
	EAttribute getDistribution_Default();

	/**
     * Returns the meta object for the containment reference '{@link timing.Distribution#getDefaultExp <em>Default Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Default Exp</em>'.
     * @see timing.Distribution#getDefaultExp()
     * @see #getDistribution()
     * @generated
     */
	EReference getDistribution_DefaultExp();

	/**
     * Returns the meta object for class '{@link timing.PertDistribution <em>Pert Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Pert Distribution</em>'.
     * @see timing.PertDistribution
     * @generated
     */
	EClass getPertDistribution();

	/**
     * Returns the meta object for the attribute '{@link timing.PertDistribution#getMode <em>Mode</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Mode</em>'.
     * @see timing.PertDistribution#getMode()
     * @see #getPertDistribution()
     * @generated
     */
	EAttribute getPertDistribution_Mode();

	/**
     * Returns the meta object for the attribute '{@link timing.PertDistribution#getMin <em>Min</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Min</em>'.
     * @see timing.PertDistribution#getMin()
     * @see #getPertDistribution()
     * @generated
     */
	EAttribute getPertDistribution_Min();

	/**
     * Returns the meta object for the attribute '{@link timing.PertDistribution#getMax <em>Max</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Max</em>'.
     * @see timing.PertDistribution#getMax()
     * @see #getPertDistribution()
     * @generated
     */
	EAttribute getPertDistribution_Max();

	/**
     * Returns the meta object for the containment reference '{@link timing.PertDistribution#getModeExp <em>Mode Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Mode Exp</em>'.
     * @see timing.PertDistribution#getModeExp()
     * @see #getPertDistribution()
     * @generated
     */
	EReference getPertDistribution_ModeExp();

	/**
     * Returns the meta object for the containment reference '{@link timing.PertDistribution#getMinExp <em>Min Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Min Exp</em>'.
     * @see timing.PertDistribution#getMinExp()
     * @see #getPertDistribution()
     * @generated
     */
	EReference getPertDistribution_MinExp();

	/**
     * Returns the meta object for the containment reference '{@link timing.PertDistribution#getMaxExp <em>Max Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Max Exp</em>'.
     * @see timing.PertDistribution#getMaxExp()
     * @see #getPertDistribution()
     * @generated
     */
	EReference getPertDistribution_MaxExp();

	/**
     * Returns the meta object for the attribute '{@link timing.PertDistribution#getGamma <em>Gamma</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Gamma</em>'.
     * @see timing.PertDistribution#getGamma()
     * @see #getPertDistribution()
     * @generated
     */
	EAttribute getPertDistribution_Gamma();

	/**
     * Returns the meta object for the containment reference '{@link timing.PertDistribution#getGammaExp <em>Gamma Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Gamma Exp</em>'.
     * @see timing.PertDistribution#getGammaExp()
     * @see #getPertDistribution()
     * @generated
     */
	EReference getPertDistribution_GammaExp();

	/**
     * Returns the meta object for class '{@link timing.NormalDistribution <em>Normal Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Normal Distribution</em>'.
     * @see timing.NormalDistribution
     * @generated
     */
	EClass getNormalDistribution();

	/**
     * Returns the meta object for the attribute '{@link timing.NormalDistribution#getMean <em>Mean</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Mean</em>'.
     * @see timing.NormalDistribution#getMean()
     * @see #getNormalDistribution()
     * @generated
     */
	EAttribute getNormalDistribution_Mean();

	/**
     * Returns the meta object for the attribute '{@link timing.NormalDistribution#getSd <em>Sd</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Sd</em>'.
     * @see timing.NormalDistribution#getSd()
     * @see #getNormalDistribution()
     * @generated
     */
	EAttribute getNormalDistribution_Sd();

	/**
     * Returns the meta object for the containment reference '{@link timing.NormalDistribution#getMeanExp <em>Mean Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Mean Exp</em>'.
     * @see timing.NormalDistribution#getMeanExp()
     * @see #getNormalDistribution()
     * @generated
     */
	EReference getNormalDistribution_MeanExp();

	/**
     * Returns the meta object for the containment reference '{@link timing.NormalDistribution#getSdExp <em>Sd Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Sd Exp</em>'.
     * @see timing.NormalDistribution#getSdExp()
     * @see #getNormalDistribution()
     * @generated
     */
	EReference getNormalDistribution_SdExp();

	/**
     * Returns the meta object for class '{@link timing.EnumeratedDistribution <em>Enumerated Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Enumerated Distribution</em>'.
     * @see timing.EnumeratedDistribution
     * @generated
     */
	EClass getEnumeratedDistribution();

	/**
     * Returns the meta object for the attribute list '{@link timing.EnumeratedDistribution#getValues <em>Values</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute list '<em>Values</em>'.
     * @see timing.EnumeratedDistribution#getValues()
     * @see #getEnumeratedDistribution()
     * @generated
     */
	EAttribute getEnumeratedDistribution_Values();

	/**
     * Returns the meta object for the containment reference list '{@link timing.EnumeratedDistribution#getValuesExp <em>Values Exp</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Values Exp</em>'.
     * @see timing.EnumeratedDistribution#getValuesExp()
     * @see #getEnumeratedDistribution()
     * @generated
     */
	EReference getEnumeratedDistribution_ValuesExp();

	/**
     * Returns the meta object for enum '{@link timing.CalculationMode <em>Calculation Mode</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Calculation Mode</em>'.
     * @see timing.CalculationMode
     * @generated
     */
	EEnum getCalculationMode();

	/**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	TimingFactory getTimingFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link timing.Timing <em>Timing</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see timing.Timing
         * @see timing.impl.TimingPackageImpl#getTiming()
         * @generated
         */
		EClass TIMING = eINSTANCE.getTiming();

		/**
         * The meta object literal for the '{@link timing.impl.FixedValueImpl <em>Fixed Value</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see timing.impl.FixedValueImpl
         * @see timing.impl.TimingPackageImpl#getFixedValue()
         * @generated
         */
		EClass FIXED_VALUE = eINSTANCE.getFixedValue();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute FIXED_VALUE__VALUE = eINSTANCE.getFixedValue_Value();

		/**
         * The meta object literal for the '<em><b>Value Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference FIXED_VALUE__VALUE_EXP = eINSTANCE.getFixedValue_ValueExp();

		/**
         * The meta object literal for the '{@link timing.impl.ArrayImpl <em>Array</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see timing.impl.ArrayImpl
         * @see timing.impl.TimingPackageImpl#getArray()
         * @generated
         */
		EClass ARRAY = eINSTANCE.getArray();

		/**
         * The meta object literal for the '<em><b>Values</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ARRAY__VALUES = eINSTANCE.getArray_Values();

		/**
         * The meta object literal for the '<em><b>Values Exp</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ARRAY__VALUES_EXP = eINSTANCE.getArray_ValuesExp();

		/**
         * The meta object literal for the '{@link timing.impl.TriangularDistributionImpl <em>Triangular Distribution</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see timing.impl.TriangularDistributionImpl
         * @see timing.impl.TimingPackageImpl#getTriangularDistribution()
         * @generated
         */
		EClass TRIANGULAR_DISTRIBUTION = eINSTANCE.getTriangularDistribution();

		/**
         * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute TRIANGULAR_DISTRIBUTION__MODE = eINSTANCE.getTriangularDistribution_Mode();

		/**
         * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute TRIANGULAR_DISTRIBUTION__MIN = eINSTANCE.getTriangularDistribution_Min();

		/**
         * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute TRIANGULAR_DISTRIBUTION__MAX = eINSTANCE.getTriangularDistribution_Max();

		/**
         * The meta object literal for the '<em><b>Mode Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference TRIANGULAR_DISTRIBUTION__MODE_EXP = eINSTANCE.getTriangularDistribution_ModeExp();

		/**
         * The meta object literal for the '<em><b>Min Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference TRIANGULAR_DISTRIBUTION__MIN_EXP = eINSTANCE.getTriangularDistribution_MinExp();

		/**
         * The meta object literal for the '<em><b>Max Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference TRIANGULAR_DISTRIBUTION__MAX_EXP = eINSTANCE.getTriangularDistribution_MaxExp();

		/**
         * The meta object literal for the '{@link timing.impl.DistributionImpl <em>Distribution</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see timing.impl.DistributionImpl
         * @see timing.impl.TimingPackageImpl#getDistribution()
         * @generated
         */
		EClass DISTRIBUTION = eINSTANCE.getDistribution();

		/**
         * The meta object literal for the '<em><b>Default</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute DISTRIBUTION__DEFAULT = eINSTANCE.getDistribution_Default();

		/**
         * The meta object literal for the '<em><b>Default Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference DISTRIBUTION__DEFAULT_EXP = eINSTANCE.getDistribution_DefaultExp();

		/**
         * The meta object literal for the '{@link timing.impl.PertDistributionImpl <em>Pert Distribution</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see timing.impl.PertDistributionImpl
         * @see timing.impl.TimingPackageImpl#getPertDistribution()
         * @generated
         */
		EClass PERT_DISTRIBUTION = eINSTANCE.getPertDistribution();

		/**
         * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PERT_DISTRIBUTION__MODE = eINSTANCE.getPertDistribution_Mode();

		/**
         * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PERT_DISTRIBUTION__MIN = eINSTANCE.getPertDistribution_Min();

		/**
         * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PERT_DISTRIBUTION__MAX = eINSTANCE.getPertDistribution_Max();

		/**
         * The meta object literal for the '<em><b>Mode Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERT_DISTRIBUTION__MODE_EXP = eINSTANCE.getPertDistribution_ModeExp();

		/**
         * The meta object literal for the '<em><b>Min Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERT_DISTRIBUTION__MIN_EXP = eINSTANCE.getPertDistribution_MinExp();

		/**
         * The meta object literal for the '<em><b>Max Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERT_DISTRIBUTION__MAX_EXP = eINSTANCE.getPertDistribution_MaxExp();

		/**
         * The meta object literal for the '<em><b>Gamma</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PERT_DISTRIBUTION__GAMMA = eINSTANCE.getPertDistribution_Gamma();

		/**
         * The meta object literal for the '<em><b>Gamma Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERT_DISTRIBUTION__GAMMA_EXP = eINSTANCE.getPertDistribution_GammaExp();

		/**
         * The meta object literal for the '{@link timing.impl.NormalDistributionImpl <em>Normal Distribution</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see timing.impl.NormalDistributionImpl
         * @see timing.impl.TimingPackageImpl#getNormalDistribution()
         * @generated
         */
		EClass NORMAL_DISTRIBUTION = eINSTANCE.getNormalDistribution();

		/**
         * The meta object literal for the '<em><b>Mean</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute NORMAL_DISTRIBUTION__MEAN = eINSTANCE.getNormalDistribution_Mean();

		/**
         * The meta object literal for the '<em><b>Sd</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute NORMAL_DISTRIBUTION__SD = eINSTANCE.getNormalDistribution_Sd();

		/**
         * The meta object literal for the '<em><b>Mean Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference NORMAL_DISTRIBUTION__MEAN_EXP = eINSTANCE.getNormalDistribution_MeanExp();

		/**
         * The meta object literal for the '<em><b>Sd Exp</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference NORMAL_DISTRIBUTION__SD_EXP = eINSTANCE.getNormalDistribution_SdExp();

		/**
         * The meta object literal for the '{@link timing.impl.EnumeratedDistributionImpl <em>Enumerated Distribution</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see timing.impl.EnumeratedDistributionImpl
         * @see timing.impl.TimingPackageImpl#getEnumeratedDistribution()
         * @generated
         */
		EClass ENUMERATED_DISTRIBUTION = eINSTANCE.getEnumeratedDistribution();

		/**
         * The meta object literal for the '<em><b>Values</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ENUMERATED_DISTRIBUTION__VALUES = eINSTANCE.getEnumeratedDistribution_Values();

		/**
         * The meta object literal for the '<em><b>Values Exp</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ENUMERATED_DISTRIBUTION__VALUES_EXP = eINSTANCE.getEnumeratedDistribution_ValuesExp();

		/**
         * The meta object literal for the '{@link timing.CalculationMode <em>Calculation Mode</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see timing.CalculationMode
         * @see timing.impl.TimingPackageImpl#getCalculationMode()
         * @generated
         */
		EEnum CALCULATION_MODE = eINSTANCE.getCalculationMode();

	}

} //TimingPackage
