/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;

/**
 * A mechanism for iterating over all the branches of a graph or tree; it provides the capability to {@link #prune
 * prune} the iteration so that all results of a particular branch are skipped.
 */
public interface BranchIterator<E> extends Iterator<E> {
    /**
     * Prunes the iterator so that it skips over the current branch, i.e. all the nodes after the most recent result of
     * calling {@link #next() next()}.
     *
     * @throws IllegalStateException If {@link #prune()} or {@link #hasNext()} already has been called after calling
     *     {@link #next()}.
     */
    void prune();
}
