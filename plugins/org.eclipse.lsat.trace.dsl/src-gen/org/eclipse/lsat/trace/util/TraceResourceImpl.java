/**
 */
package org.eclipse.lsat.trace.util;

import static java.lang.String.format;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;

import org.eclipse.lsat.trace.TraceFactory;
import org.eclipse.lsat.trace.TraceLine;
import org.eclipse.lsat.trace.TraceModel;


/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.trace.util.TraceResourceFactoryImpl
 * @generated
 */
public class TraceResourceImpl extends ResourceImpl {
	private static final Charset ENCODING = StandardCharsets.UTF_8;  

	public TraceResourceImpl(URI uri) {
		super(uri);
	}
	
	@Override
	protected void doLoad(InputStream inputStream, Map<?, ?> options) throws IOException {
		LineNumberReader reader = new LineNumberReader(new InputStreamReader(inputStream, ENCODING));
		TraceModel model = TraceFactory.eINSTANCE.createTraceModel();
		model.setName(getURI().trimFileExtension().lastSegment());		
		getContents().add(model);
		String line = null;
		while ((line = reader.readLine()) != null) {
			try {
			
				TraceLine traceLine = parseTraceLine(line);
				traceLine.setLineNumber(reader.getLineNumber());
				
				model.getLines().add(traceLine);
				
			} catch (ParseException e) {
				throw new IOException(format("Parse failure at line %s: %s", reader.getLineNumber(), e.getMessage()), e);
			}
		}
	}

	static final TraceLine parseTraceLine(String aLine) throws ParseException {
		String[] segments = aLine.split(", ", 2);
		Instant instant = Instant.parse(segments[0]);
		BigDecimal timestamp = new BigDecimal(instant.getEpochSecond()).add(
				new BigDecimal(instant.getNano()).movePointLeft(9).stripTrailingZeros());

		TraceLine tp_line = TraceFactory.eINSTANCE.createTraceLine();
		tp_line.setTimestamp(timestamp);
		tp_line.setTracePoint(segments[1]);
		return tp_line;
	}
	
	@Override
	protected void doSave(OutputStream outputStream, Map<?, ?> options) throws IOException {
		Writer writer = new OutputStreamWriter(outputStream);
		for(EObject model : getContents()) {
			if (!(model instanceof TraceModel)) {
				continue;
			}
			for (TraceLine line : ((TraceModel)model).getLines()) {
				String traceLine = formatTraceLine(line);
				writer.write(traceLine);
				writer.write(System.lineSeparator());
				writer.flush();
			}
		}
	}

	public static final String formatTraceLine(TraceLine aLine) {
		BigDecimal timestamp = aLine.getTimestamp();
		BigDecimal epochSecond = timestamp.setScale(0, RoundingMode.FLOOR);
		BigDecimal nanoAdjustment = timestamp.subtract(epochSecond).movePointRight(9);
		Instant instant = Instant.ofEpochSecond(epochSecond.longValue(), nanoAdjustment.longValue());
		return DateTimeFormatter.ISO_INSTANT.format(instant) + ", " + aLine.getTracePoint();
	}
}
