/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Axis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.Axis#getName <em>Name</em>}</li>
 *   <li>{@link machine.Axis#getSetPoints <em>Set Points</em>}</li>
 *   <li>{@link machine.Axis#getUnit <em>Unit</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getAxis()
 * @model
 * @generated
 */
public interface Axis extends EObject {
	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see machine.MachinePackage#getAxis_Name()
     * @model required="true"
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link machine.Axis#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

	/**
     * Returns the value of the '<em><b>Set Points</b></em>' reference list.
     * The list contents are of type {@link machine.SetPoint}.
     * It is bidirectional and its opposite is '{@link machine.SetPoint#getAxes <em>Axes</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Points</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Set Points</em>' reference list.
     * @see machine.MachinePackage#getAxis_SetPoints()
     * @see machine.SetPoint#getAxes
     * @model opposite="axes" resolveProxies="false" required="true"
     * @generated
     */
	EList<SetPoint> getSetPoints();

	/**
     * Returns the value of the '<em><b>Unit</b></em>' attribute.
     * The default value is <code>"m"</code>.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Unit</em>' attribute.
     * @see #setUnit(String)
     * @see machine.MachinePackage#getAxis_Unit()
     * @model default="m" required="true"
     * @generated
     */
	String getUnit();

	/**
     * Sets the value of the '{@link machine.Axis#getUnit <em>Unit</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Unit</em>' attribute.
     * @see #getUnit()
     * @generated
     */
	void setUnit(String value);

} // Axis
