/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.layout

import org.eclipse.emf.ecore.EClass

bundle {  
    metadataClass ActivityDiagramLayoutMetaDataProvider
    idPrefix org.eclipse.lsat.activity.diagram.layout
}

category LSAT {
    description "Layout algorithms dedicated to the LSAT tooling"
}

algorithm ActivityDiagramLayout(ActivityDiagramLayout.Provider) {
    label "Activity Diagram"
    metadataClass ActivityDiagramLayoutOptions
    description "Performs a basic layout for activity diagrams. The algorithm does not preserve any existing layout, so run first, tune later. NB Work in progress."
    category LSAT
    features compound, ports
 
    supports eClass
    supports name
    supports eventName
    supports layoutRow
} 

programmatic option eClass : EClass {
    label "eClass"
    description "The eClass of the node"
    default = null
    targets nodes
}

programmatic option eventName : String {
    label "Event Name"
    description "name of the event "
    default = ""
    targets nodes
}

programmatic option name : String {
    label "Node Name"
    description "Totally irrelevant for the algorithm, but it makes life a lot easier while debugging"
    default = ""
    targets nodes
}

programmatic option layoutRow : Integer {
    label "Row in the layout"
    description "Abstract row of the node in the final, global layout, determined by topological order and node type"
    default = 1
    targets nodes
}

