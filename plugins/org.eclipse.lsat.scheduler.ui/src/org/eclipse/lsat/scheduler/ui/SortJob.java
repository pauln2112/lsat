/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.ui;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.lsat.common.emf.common.util.URIHelper;
import org.eclipse.lsat.common.emf.ecore.resource.Persistor;
import org.eclipse.lsat.common.emf.ecore.resource.PersistorFactory;
import org.eclipse.lsat.scheduler.SortMachine;
import org.eclipse.lsat.scheduler.SortSettings;
import org.eclipse.xtext.resource.SaveOptions;

import machine.Machine;
import setting.Settings;

public class SortJob extends Job {
    private final IFile modelIFile;

    public SortJob(IFile modelIFile) {
        super("Sort specification");
        this.modelIFile = modelIFile;
    }

    private void refresEclipsehWorkspaces() throws CoreException {
        for (IProject project: ResourcesPlugin.getWorkspace().getRoot().getProjects()) {
            project.refreshLocal(IResource.DEPTH_INFINITE, null);
        }
    }

    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            monitor.beginTask("Sorting...", 100);
            URI modelURI = URIHelper.asURI(modelIFile);
            monitor.subTask("Sorting " + modelURI.lastSegment());

            PersistorFactory factory = new PersistorFactory();

            switch (modelURI.fileExtension()) {
                case "machine":
                    Persistor<Machine> machinePersistor = factory.getPersistor(Machine.class);
                    Machine machine = new SortMachine().transformModel(machinePersistor.loadOne(modelURI), monitor);
                    machine.eResource().save(SaveOptions.newBuilder().format().getOptions().toOptionsMap());
                    break;
                case "setting":
                    Persistor<Settings> settingsPersistor = factory.getPersistor(Settings.class);
                    Settings settings = new SortSettings().transformModel(settingsPersistor.loadOne(modelURI), monitor);
                    settings.eResource().save(SaveOptions.newBuilder().format().getOptions().toOptionsMap());
                    break;
                case "etf":
                    /*
                     * This functionality was not requested by the client. It is used only for testing purposes in the
                     * RCPTT test cases.
                     */
                    // Read the file into a list of strings
                    List<String> lines = Files.readAllLines(Paths.get(modelIFile.getRawLocation().toOSString()));
                    // Sort the list of strings
                    Collections.sort(lines);
                    // Set output filename
                    String sortedOutputFilenamePath = modelIFile.getRawLocation().removeFileExtension().toOSString()
                            + ".sorted";
                    // Save sorted file to output folder.
                    Files.write(Paths.get(sortedOutputFilenamePath), lines);
                    // Refresh workspace to show the newly sorted file
                    refresEclipsehWorkspaces();
                    break;

                default:
                    throw new Exception("Unknown file extension to sort: " + modelURI.fileExtension());
            }

            return Status.OK_STATUS;
        } catch (final Exception e) {
            return new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
        } finally {
            monitor.done();
        }
    }
}
