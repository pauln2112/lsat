/**
 */
package org.eclipse.lsat.common.mpt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertex</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.Vertex#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.Vertex#getIncoming <em>Incoming</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getVertex()
 * @model abstract="true"
 * @generated
 */
public interface Vertex extends NamedObject {
	/**
	 * Returns the value of the '<em><b>Outgoing</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.mpt.Edge}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.mpt.Edge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing</em>' reference list.
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getVertex_Outgoing()
	 * @see org.eclipse.lsat.common.mpt.Edge#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<Edge> getOutgoing();

	/**
	 * Returns the value of the '<em><b>Incoming</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.mpt.Edge}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.mpt.Edge#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming</em>' reference list.
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getVertex_Incoming()
	 * @see org.eclipse.lsat.common.mpt.Edge#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<Edge> getIncoming();

} // Vertex
