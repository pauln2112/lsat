/**
 */
package org.eclipse.lsat.common.scheduler.schedule;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduleFactory
 * @model kind="package"
 * @generated
 */
public interface SchedulePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "schedule";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/lsat/scheduler/schedule";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "schedule";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SchedulePackage eINSTANCE = org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledTaskImpl <em>Scheduled Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledTaskImpl
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getScheduledTask()
	 * @generated
	 */
	int SCHEDULED_TASK = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__NAME = DirectedGraphPackage.NODE__NAME;

	/**
	 * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__OUTGOING_EDGES = DirectedGraphPackage.NODE__OUTGOING_EDGES;

	/**
	 * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__INCOMING_EDGES = DirectedGraphPackage.NODE__INCOMING_EDGES;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__GRAPH = DirectedGraphPackage.NODE__GRAPH;

	/**
	 * The feature id for the '<em><b>Aspects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__ASPECTS = DirectedGraphPackage.NODE__ASPECTS;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__START_TIME = DirectedGraphPackage.NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__END_TIME = DirectedGraphPackage.NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__TASK = DirectedGraphPackage.NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__SEQUENCE = DirectedGraphPackage.NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK__DURATION = DirectedGraphPackage.NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Scheduled Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK_FEATURE_COUNT = DirectedGraphPackage.NODE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Scheduled Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_TASK_OPERATION_COUNT = DirectedGraphPackage.NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledDependencyImpl <em>Scheduled Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledDependencyImpl
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getScheduledDependency()
	 * @generated
	 */
	int SCHEDULED_DEPENDENCY = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY__NAME = DirectedGraphPackage.EDGE__NAME;

	/**
	 * The feature id for the '<em><b>Source Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY__SOURCE_NODE = DirectedGraphPackage.EDGE__SOURCE_NODE;

	/**
	 * The feature id for the '<em><b>Target Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY__TARGET_NODE = DirectedGraphPackage.EDGE__TARGET_NODE;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY__GRAPH = DirectedGraphPackage.EDGE__GRAPH;

	/**
	 * The feature id for the '<em><b>Aspects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY__ASPECTS = DirectedGraphPackage.EDGE__ASPECTS;

	/**
	 * The feature id for the '<em><b>Boundary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY__BOUNDARY = DirectedGraphPackage.EDGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dependency</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY__DEPENDENCY = DirectedGraphPackage.EDGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY__TYPE = DirectedGraphPackage.EDGE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Scheduled Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY_FEATURE_COUNT = DirectedGraphPackage.EDGE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Scheduled Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULED_DEPENDENCY_OPERATION_COUNT = DirectedGraphPackage.EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduleImpl <em>Schedule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.ScheduleImpl
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getSchedule()
	 * @generated
	 */
	int SCHEDULE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__NAME = DirectedGraphPackage.DIRECTED_GRAPH__NAME;

	/**
	 * The feature id for the '<em><b>Sub Graphs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__SUB_GRAPHS = DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS;

	/**
	 * The feature id for the '<em><b>Parent Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__PARENT_GRAPH = DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__EDGES = DirectedGraphPackage.DIRECTED_GRAPH__EDGES;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__NODES = DirectedGraphPackage.DIRECTED_GRAPH__NODES;

	/**
	 * The feature id for the '<em><b>Aspects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__ASPECTS = DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS;

	/**
	 * The feature id for the '<em><b>Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__RESOURCE_MODEL = DirectedGraphPackage.DIRECTED_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sequences</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__SEQUENCES = DirectedGraphPackage.DIRECTED_GRAPH_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Epoch Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__EPOCH_TIME = DirectedGraphPackage.DIRECTED_GRAPH_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_FEATURE_COUNT = DirectedGraphPackage.DIRECTED_GRAPH_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>All Nodes In Topological Order</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE___ALL_NODES_IN_TOPOLOGICAL_ORDER = DirectedGraphPackage.DIRECTED_GRAPH___ALL_NODES_IN_TOPOLOGICAL_ORDER;

	/**
	 * The number of operations of the '<em>Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_OPERATION_COUNT = DirectedGraphPackage.DIRECTED_GRAPH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.schedule.impl.SequenceImpl <em>Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SequenceImpl
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getSequence()
	 * @generated
	 */
	int SEQUENCE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__SCHEDULE = 1;

	/**
	 * The feature id for the '<em><b>Scheduled Tasks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__SCHEDULED_TASKS = 2;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__RESOURCE = 3;

	/**
	 * The number of structural features of the '<em>Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary <em>Dependency Boundary</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getDependencyBoundary()
	 * @generated
	 */
	int DEPENDENCY_BOUNDARY = 4;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType <em>Scheduled Dependency Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType
	 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getScheduledDependencyType()
	 * @generated
	 */
	int SCHEDULED_DEPENDENCY_TYPE = 5;


	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask <em>Scheduled Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scheduled Task</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledTask
	 * @generated
	 */
	EClass getScheduledTask();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getStartTime()
	 * @see #getScheduledTask()
	 * @generated
	 */
	EAttribute getScheduledTask_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getEndTime()
	 * @see #getScheduledTask()
	 * @generated
	 */
	EAttribute getScheduledTask_EndTime();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getTask()
	 * @see #getScheduledTask()
	 * @generated
	 */
	EReference getScheduledTask_Task();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sequence</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getSequence()
	 * @see #getScheduledTask()
	 * @generated
	 */
	EReference getScheduledTask_Sequence();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getDuration()
	 * @see #getScheduledTask()
	 * @generated
	 */
	EAttribute getScheduledTask_Duration();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency <em>Scheduled Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scheduled Dependency</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency
	 * @generated
	 */
	EClass getScheduledDependency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getBoundary <em>Boundary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Boundary</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getBoundary()
	 * @see #getScheduledDependency()
	 * @generated
	 */
	EAttribute getScheduledDependency_Boundary();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getDependency <em>Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dependency</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getDependency()
	 * @see #getScheduledDependency()
	 * @generated
	 */
	EReference getScheduledDependency_Dependency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency#getType()
	 * @see #getScheduledDependency()
	 * @generated
	 */
	EAttribute getScheduledDependency_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.schedule.Schedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.Schedule
	 * @generated
	 */
	EClass getSchedule();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.scheduler.schedule.Schedule#getResourceModel <em>Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource Model</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.Schedule#getResourceModel()
	 * @see #getSchedule()
	 * @generated
	 */
	EReference getSchedule_ResourceModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.scheduler.schedule.Schedule#getSequences <em>Sequences</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sequences</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.Schedule#getSequences()
	 * @see #getSchedule()
	 * @generated
	 */
	EReference getSchedule_Sequences();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.schedule.Schedule#isEpochTime <em>Epoch Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Epoch Time</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.Schedule#isEpochTime()
	 * @see #getSchedule()
	 * @generated
	 */
	EAttribute getSchedule_EpochTime();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.Sequence
	 * @generated
	 */
	EClass getSequence();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.Sequence#getName()
	 * @see #getSequence()
	 * @generated
	 */
	EAttribute getSequence_Name();

	/**
	 * Returns the meta object for the container reference '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Schedule</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.Sequence#getSchedule()
	 * @see #getSequence()
	 * @generated
	 */
	EReference getSequence_Schedule();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getScheduledTasks <em>Scheduled Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Scheduled Tasks</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.Sequence#getScheduledTasks()
	 * @see #getSequence()
	 * @generated
	 */
	EReference getSequence_ScheduledTasks();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.Sequence#getResource()
	 * @see #getSequence()
	 * @generated
	 */
	EReference getSequence_Resource();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary <em>Dependency Boundary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Dependency Boundary</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary
	 * @generated
	 */
	EEnum getDependencyBoundary();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType <em>Scheduled Dependency Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Scheduled Dependency Type</em>'.
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType
	 * @generated
	 */
	EEnum getScheduledDependencyType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScheduleFactory getScheduleFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledTaskImpl <em>Scheduled Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledTaskImpl
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getScheduledTask()
		 * @generated
		 */
		EClass SCHEDULED_TASK = eINSTANCE.getScheduledTask();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_TASK__START_TIME = eINSTANCE.getScheduledTask_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_TASK__END_TIME = eINSTANCE.getScheduledTask_EndTime();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULED_TASK__TASK = eINSTANCE.getScheduledTask_Task();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULED_TASK__SEQUENCE = eINSTANCE.getScheduledTask_Sequence();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_TASK__DURATION = eINSTANCE.getScheduledTask_Duration();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledDependencyImpl <em>Scheduled Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledDependencyImpl
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getScheduledDependency()
		 * @generated
		 */
		EClass SCHEDULED_DEPENDENCY = eINSTANCE.getScheduledDependency();

		/**
		 * The meta object literal for the '<em><b>Boundary</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_DEPENDENCY__BOUNDARY = eINSTANCE.getScheduledDependency_Boundary();

		/**
		 * The meta object literal for the '<em><b>Dependency</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULED_DEPENDENCY__DEPENDENCY = eINSTANCE.getScheduledDependency_Dependency();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULED_DEPENDENCY__TYPE = eINSTANCE.getScheduledDependency_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduleImpl <em>Schedule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.ScheduleImpl
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getSchedule()
		 * @generated
		 */
		EClass SCHEDULE = eINSTANCE.getSchedule();

		/**
		 * The meta object literal for the '<em><b>Resource Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE__RESOURCE_MODEL = eINSTANCE.getSchedule_ResourceModel();

		/**
		 * The meta object literal for the '<em><b>Sequences</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE__SEQUENCES = eINSTANCE.getSchedule_Sequences();

		/**
		 * The meta object literal for the '<em><b>Epoch Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE__EPOCH_TIME = eINSTANCE.getSchedule_EpochTime();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.schedule.impl.SequenceImpl <em>Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SequenceImpl
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getSequence()
		 * @generated
		 */
		EClass SEQUENCE = eINSTANCE.getSequence();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEQUENCE__NAME = eINSTANCE.getSequence_Name();

		/**
		 * The meta object literal for the '<em><b>Schedule</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE__SCHEDULE = eINSTANCE.getSequence_Schedule();

		/**
		 * The meta object literal for the '<em><b>Scheduled Tasks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE__SCHEDULED_TASKS = eINSTANCE.getSequence_ScheduledTasks();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE__RESOURCE = eINSTANCE.getSequence_Resource();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary <em>Dependency Boundary</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getDependencyBoundary()
		 * @generated
		 */
		EEnum DEPENDENCY_BOUNDARY = eINSTANCE.getDependencyBoundary();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType <em>Scheduled Dependency Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType
		 * @see org.eclipse.lsat.common.scheduler.schedule.impl.SchedulePackageImpl#getScheduledDependencyType()
		 * @generated
		 */
		EEnum SCHEDULED_DEPENDENCY_TYPE = eINSTANCE.getScheduledDependencyType();

	}

} //SchedulePackage
