/**
 */
package setting;

import expressions.Expression;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Motion Profile Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link setting.MotionProfileSettings#getMotionProfile <em>Motion Profile</em>}</li>
 *   <li>{@link setting.MotionProfileSettings#getMotionArguments <em>Motion Arguments</em>}</li>
 * </ul>
 *
 * @see setting.SettingPackage#getMotionProfileSettings()
 * @model
 * @generated
 */
public interface MotionProfileSettings extends EObject {
	/**
     * Returns the value of the '<em><b>Motion Profile</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Motion Profile</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Motion Profile</em>' attribute.
     * @see #setMotionProfile(String)
     * @see setting.SettingPackage#getMotionProfileSettings_MotionProfile()
     * @model
     * @generated
     */
	String getMotionProfile();

	/**
     * Sets the value of the '{@link setting.MotionProfileSettings#getMotionProfile <em>Motion Profile</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Motion Profile</em>' attribute.
     * @see #getMotionProfile()
     * @generated
     */
	void setMotionProfile(String value);

	/**
     * Returns the value of the '<em><b>Motion Arguments</b></em>' map.
     * The key is of type {@link java.lang.String},
     * and the value is of type {@link expressions.Expression},
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Motion Arguments</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Motion Arguments</em>' map.
     * @see setting.SettingPackage#getMotionProfileSettings_MotionArguments()
     * @model mapType="setting.MotionArgumentsMapEntry&lt;org.eclipse.emf.ecore.EString, expressions.Expression&gt;"
     * @generated
     */
	EMap<String, Expression> getMotionArguments();

} // MotionProfileSettings
