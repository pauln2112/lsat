/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util.internal;

import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public class Log4Slf4jLog extends AbstractQvtLog {
    private final Logger itsLogger;

    private MessageTuple itsLastMessage = null;

    public Log4Slf4jLog(Logger aLogger) {
        this.itsLogger = aLogger;
    }

    /**
     * @return last warning/error/fatal message written to this log
     */
    public MessageTuple getLastMessage() {
        return itsLastMessage;
    }

    public boolean hasLastMessage() {
        return itsLastMessage != null;
    }

    @Override
    protected synchronized void doLog(LogLevel aLevel, String aFormat, Object... aArgs) {
        switch (aLevel) {
            case Trace:
                if (!itsLogger.isTraceEnabled())
                    return;
                break;
            case Debug:
                if (!itsLogger.isDebugEnabled())
                    return;
                break;
            case Info:
                if (!itsLogger.isInfoEnabled())
                    return;
                break;
            default:
                // Always handle Errors and Fatals
        }

        final String message = createMessage(aFormat, aArgs);

        switch (aLevel) {
            case Trace:
                itsLogger.trace(message);
                break;
            case Debug:
                itsLogger.debug(message);
                break;
            case Info:
                itsLogger.info(message);
                break;
            case Warning:
                itsLogger.warn(message);
                itsLastMessage = new MessageTuple(message);
                if (itsLastMessage.hasErrorCode()) {
                    Marker marker = MarkerFactory.getMarker(itsLastMessage.getErrorCode());
                    itsLogger.warn(marker, itsLastMessage.getMessage());
                }
                break;
            case Error:
            case Fatal:
                itsLogger.error(message);
                itsLastMessage = new MessageTuple(message);
                break;
        }
    }
}
