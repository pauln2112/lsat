//
// Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
//
// This program and the accompanying materials are made
// available under the terms of the Eclipse Public License 2.0
// which is available at https://www.eclipse.org/legal/epl-2.0/
//
// SPDX-License-Identifier: EPL-2.0
//

pipeline {
    agent {
        // The 'ubuntu-2404' pod template allows UI tests.
        kubernetes {
            inheritFrom 'ubuntu-2404'
        }
    }

    tools {
        jdk 'temurin-jdk8-latest'
        maven 'apache-maven-3.8.3'
    }

    options {
        buildDiscarder(logRotator(
            // Number of builds to keep.
            numToKeepStr: '5',

            // Number of builds for which to keep the artifacts.
            artifactNumToKeepStr: '5',

            // Number of days to keep builds.
            daysToKeepStr: env.BRANCH_NAME ==~ /master/ ? '7' :             // master
                           env.BRANCH_NAME ==~ /develop/ ? '1000' :         // develop
                           env.TAG_NAME ==~ /v[0-9]+\\.[0-9]+.*/ ?  '120' : // release tags
                           '30',                                            // other branches and merge requests

            // Number of days to keep artifacts of builds.
            artifactDaysToKeepStr: env.BRANCH_NAME ==~ /master/ ? '7' :            // master
                                   env.BRANCH_NAME ==~ /develop/ ? '1000' :        // develop
                                   env.TAG_NAME ==~ /v[0-9]+\\.[0-9]+.*/ ?  '30' : // release tags
                                   '30',                                           // other branches and merge requests
        ))
        timeout(time: 1, unit: 'HOURS')
        timestamps()
    }

    stages {
        stage('Build & Test') {
            steps {
                wrap([$class: 'Xvnc', takeScreenshot: false, useXauthority: true]) {
                    sh '''
                        # Print versions.
                        java -version
                        mvn -version
                        git --version

                        # Print environment.
                        printenv

                        # Get Git last commit date.
                        GIT_DATE_EPOCH=$(git log -1 --format=%cd --date=raw | cut -d ' ' -f 1)
                        GIT_DATE=$(date -d @$GIT_DATE_EPOCH -u +%Y%m%d-%H%M%S)

                        # Configure update snapshots and fail-at-end for build.
                        BUILD_ARGS="-U -fae"

                        # Configure 'sign' profile for build.
                        # Sign 'develop' branch, to allow release signing for 'nightly' deployment.
                        # Sign releases. Determined based on branch or release version tag name.
                        if [[ "$GIT_BRANCH" == "develop" || "$TAG_NAME" =~ ^v[0-9]+\\.[0-9]+.*$ ]]; then
                            BUILD_ARGS="$BUILD_ARGS -Psign"
                        fi

                        # Override the 'lsat.version.enduser' property for releases,
                        # 'nightly' for the develop branch or 'dev' otherwise.
                        if [[ "$TAG_NAME" =~ ^v[0-9]+\\.[0-9]+.*$ ]]; then
                            BUILD_ARGS="$BUILD_ARGS -Dlsat.version.enduser=$TAG_NAME"
                        elif [[ "$GIT_BRANCH" == "develop" ]]; then
                            BUILD_ARGS="$BUILD_ARGS -Dlsat.version.enduser=nightly"
                        fi

                        # Override the 'lsat.version.qualifier' property for Jenkins builds.
                        # It starts with 'v' and the Git date, followed by a qualifier postfix.
                        # For releases, the qualifier postfix is the postfix of the version tag (if any).
                        # For non-releases, the qualifier postfix is 'nightly'.
                        if [[ "$TAG_NAME" =~ ^v[0-9]+\\.[0-9]+.*$ ]]; then
                            QUALIFIER_POSTFIX=$(echo "$TAG_NAME" | sed -e 's/^[^-]*//g')
                        elif [[ "$GIT_BRANCH" == "develop" ]]; then
                            QUALIFIER_POSTFIX=-nightly
                        else
                            QUALIFIER_POSTFIX=-dev
                        fi
                        BUILD_ARGS="$BUILD_ARGS -Dlsat.version.qualifier=v$GIT_DATE$QUALIFIER_POSTFIX"

                        # Perform build.
                        ./build.sh $BUILD_ARGS
                    '''
                }
            }

            post {
                always {
                    // Website including documentation
                    archiveArtifacts artifacts: 'releng/org.eclipse.lsat.website/target/*-website.zip', allowEmptyArchive: true

                    // Test results
                    junit '**/target/surefire-reports/*.xml'
                }
                success {
                    // Update site.
                    archiveArtifacts 'product/org.eclipse.lsat.product/target/*-updatesite.zip'

                    // Product.
                    archiveArtifacts 'product/org.eclipse.lsat.product/target/products/*-linux*.tar.gz'
                    archiveArtifacts 'product/org.eclipse.lsat.product/target/products/*-win*.zip'
                }
            }
        }

        stage('Deploy Downloads') {
            when {
                anyOf {
                    branch 'develop'
                    tag pattern: "v\\d+\\.\\d+.*", comparator: "REGEXP"
                }
            }
            environment {
                DOWNLOADS_PATH = "/home/data/httpd/download.eclipse.org/lsat"
                DOWNLOADS_URL = "genie.lsat@projects-storage.eclipse.org:${DOWNLOADS_PATH}"
                RELEASE_VERSION = getReleaseVersionFromTag(env.TAG_NAME)
            }
            steps {
                // Deploy downloads.
                sh '''
                    mkdir -p deploy/update-site/
                    unzip -q product/org.eclipse.lsat.product/target/*-updatesite.zip -d deploy/update-site/
                '''
                sshagent (['projects-storage.eclipse.org-bot-ssh']) {
                    // Remove any existing directory for this release.
                    sh 'ssh genie.lsat@projects-storage.eclipse.org rm -rf ${DOWNLOADS_PATH}/${RELEASE_VERSION}/'

                    // Create directory for this release.
                    sh 'ssh genie.lsat@projects-storage.eclipse.org mkdir -p ${DOWNLOADS_PATH}/${RELEASE_VERSION}/'

                    // Website.
                    sh 'scp -r releng/org.eclipse.lsat.website/target/*-website.zip ${DOWNLOADS_URL}/${RELEASE_VERSION}/'

                    // Update site (archive).
                    sh 'scp -r product/org.eclipse.lsat.product/target/*-updatesite.zip ${DOWNLOADS_URL}/${RELEASE_VERSION}/'

                    // Update site (extracted).
                    sh 'ssh genie.lsat@projects-storage.eclipse.org mkdir -p ${DOWNLOADS_PATH}/${RELEASE_VERSION}/update-site/'
                    sh 'scp -r deploy/update-site/* ${DOWNLOADS_URL}/${RELEASE_VERSION}/update-site/'

                    // Product.
                    sh 'scp -r product/org.eclipse.lsat.product/target/products/*-linux*.tar.gz ${DOWNLOADS_URL}/${RELEASE_VERSION}/'
                    sh 'scp -r product/org.eclipse.lsat.product/target/products/*-win*.zip ${DOWNLOADS_URL}/${RELEASE_VERSION}/'
                }
            }
        }

        stage('Deploy Website') {
            when {
                anyOf {
                    branch 'develop'
                    tag pattern: "v\\d+\\.\\d+.*", comparator: "REGEXP"
                }
            }
            environment {
                WEBSITE_GIT_URL = "git@gitlab.eclipse.org:eclipse/lsat/lsat-website.git"
                RELEASE_VERSION = getReleaseVersionFromTag(env.TAG_NAME)
            }
            steps {
                // Deploy website.
                sshagent(['gitlab-bot-ssh']) {
                sh '''
                    mkdir -p deploy/www
                    git clone ${WEBSITE_GIT_URL} deploy/www

                    rm -rf deploy/www/${RELEASE_VERSION}
                    mkdir -p deploy/www/${RELEASE_VERSION}
                    unzip -q releng/org.eclipse.lsat.website/target/*-website.zip -d deploy/www/${RELEASE_VERSION}/
                '''
                dir('deploy/www') {
                    sh '''
                        git config user.email "lsat-bot@eclipse.org"
                        git config user.name "genie.lsat"
                        git config push.default simple # Required to silence Git push warning.
                        git add -A
                        git commit -q -m "Website release ${RELEASE_VERSION}." -m "Generated from commit ${GIT_COMMIT}"
                        git push
                    '''
                }
            }
        }
    }
    }

    post {
        // Send an e-mail on unsuccessful builds (unstable, failure, aborted).
        unsuccessful {
            emailext subject: 'Build $BUILD_STATUS $PROJECT_NAME #$BUILD_NUMBER!',
            body: '''Check console output at $BUILD_URL to view the results.''',
            recipientProviders: [culprits(), requestor()]
        }

        // Send an e-mail on fixed builds (back to normal).
        fixed {
            emailext subject: 'Build $BUILD_STATUS $PROJECT_NAME #$BUILD_NUMBER!',
            body: '''Check console output at $BUILD_URL to view the results.''',
            recipientProviders: [culprits(), requestor()]
        }
    }
}

def getReleaseVersionFromTag(tag) {
    if (tag == null) {
        return 'nightly'
    } else {
        return tag
    }
}
