/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.contributions.ui.project;

import static org.eclipse.lsat.contributions.ui.project.LogisticsProjectNature.NATURE_ID;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.lsat.contributions.ui.Activator;
import org.eclipse.ui.IActionDelegate;

public class LogisticsNatureHandler implements IActionDelegate {
    private IStructuredSelection selection;

    @Override
    public void selectionChanged(IAction action, ISelection selection) {
        this.selection = selection instanceof IStructuredSelection ? (IStructuredSelection)selection : null;
    }

    @Override
    public void run(IAction action) {
        IProject project = getProject();
        if (null == project) {
            return;
        }
        try {
            if (hasNatures(project, NATURE_ID)) {
                removeNatures(project, NATURE_ID);
            } else {
                addNatures(project, NATURE_ID);
            }
        } catch (CoreException e) {
            Activator.getDefault().getLog().log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
        }
    }

    private IProject getProject() {
        Object firstElement = selection.getFirstElement();
        IAdapterManager adapterManager = Platform.getAdapterManager();
        IResource resource = (IResource)adapterManager.getAdapter(firstElement, IResource.class);
        return (null == resource) ? null : resource.getProject();
    }

    public static boolean hasNatures(IProject project, String... natureIds) throws CoreException {
        IProjectDescription projectDescription = project.getDescription();
        Set<String> newNatureIds = getNatureIds(projectDescription);
        return newNatureIds.containsAll(Arrays.asList(natureIds));
    }

    public static void addNatures(IProject project, String... natureIds) throws CoreException {
        IProjectDescription projectDescription = project.getDescription();
        Set<String> newNatureIds = getNatureIds(projectDescription);
        newNatureIds.addAll(Arrays.asList(natureIds));
        setNatureIds(projectDescription, newNatureIds);
        project.setDescription(projectDescription, null);
    }

    public static void removeNatures(IProject project, String... natureIds) throws CoreException {
        IProjectDescription projectDescription = project.getDescription();
        Set<String> newNatureIds = getNatureIds(projectDescription);
        newNatureIds.removeAll(Arrays.asList(natureIds));
        setNatureIds(projectDescription, newNatureIds);
        project.setDescription(projectDescription, null);
    }

    private static Set<String> getNatureIds(IProjectDescription projectDescription) {
        return new LinkedHashSet<String>(Arrays.asList(projectDescription.getNatureIds()));
    }

    private static void setNatureIds(IProjectDescription projectDescription, Set<String> natureIds) {
        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        final String[] newNatures = workspace.sortNatureSet(natureIds.toArray(new String[natureIds.size()]));
        projectDescription.setNatureIds(newNatures);
    }
}
