/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Peripheral Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.PeripheralType#getName <em>Name</em>}</li>
 *   <li>{@link machine.PeripheralType#getConversion <em>Conversion</em>}</li>
 *   <li>{@link machine.PeripheralType#getAxes <em>Axes</em>}</li>
 *   <li>{@link machine.PeripheralType#getSetPoints <em>Set Points</em>}</li>
 *   <li>{@link machine.PeripheralType#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getPeripheralType()
 * @model
 * @generated
 */
public interface PeripheralType extends EObject {
	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see machine.MachinePackage#getPeripheralType_Name()
     * @model required="true"
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link machine.PeripheralType#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

	/**
     * Returns the value of the '<em><b>Conversion</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conversion</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Conversion</em>' attribute.
     * @see #setConversion(String)
     * @see machine.MachinePackage#getPeripheralType_Conversion()
     * @model
     * @generated
     */
	String getConversion();

	/**
     * Sets the value of the '{@link machine.PeripheralType#getConversion <em>Conversion</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Conversion</em>' attribute.
     * @see #getConversion()
     * @generated
     */
	void setConversion(String value);

	/**
     * Returns the value of the '<em><b>Axes</b></em>' containment reference list.
     * The list contents are of type {@link machine.Axis}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Axes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Axes</em>' containment reference list.
     * @see machine.MachinePackage#getPeripheralType_Axes()
     * @model containment="true"
     * @generated
     */
	EList<Axis> getAxes();

	/**
     * Returns the value of the '<em><b>Set Points</b></em>' containment reference list.
     * The list contents are of type {@link machine.SetPoint}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Points</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Set Points</em>' containment reference list.
     * @see machine.MachinePackage#getPeripheralType_SetPoints()
     * @model containment="true"
     * @generated
     */
	EList<SetPoint> getSetPoints();

	/**
     * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
     * The list contents are of type {@link machine.ActionType}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Actions</em>' containment reference list.
     * @see machine.MachinePackage#getPeripheralType_Actions()
     * @model containment="true"
     * @generated
     */
	EList<ActionType> getActions();

} // PeripheralType
