/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.qvto;

import java.math.BigDecimal;

import org.eclipse.lsat.activity.teditor.validation.ActivityValidator;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.m2m.internal.qvt.oml.evaluator.QvtRuntimeException;
import org.eclipse.m2m.qvt.oml.blackbox.java.Module;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;

import activity.Action;
import activity.ActivityPackage;
import setting.SettingPackage;
import setting.Settings;
import timing.CalculationMode;
import timing.TimingPackage;

/**
 * Just a QVTo black-box wrapper for the {@link org.eclipse.lsat.timing.util.TimingCalculator}.
 */
@SuppressWarnings("restriction")
@Module(packageURIs = {ActivityPackage.eNS_URI, SettingPackage.eNS_URI, TimingPackage.eNS_URI})
public class TimingCalculator {
    private org.eclipse.lsat.timing.util.TimingCalculator timingCalculator;

    @Operation
    public void initializeTimingCalculator(Settings settings, CalculationMode mode) throws QvtRuntimeException {
        if (null != timingCalculator) {
            timingCalculator.reset();
            return;
        }
        try {
            MotionCalculatorExtension motionCalculator = MotionCalculatorExtension.getSelectedMotionCalculator();
            timingCalculator = new org.eclipse.lsat.timing.util.TimingCalculator(settings, motionCalculator, mode);
        } catch (MotionException e) {
            throw new QvtRuntimeException("Failed to initialize motion calculator: " + e.getMessage(), e);
        }
    }

    @Operation(contextual = true)
    public BigDecimal calculateDuration(Action self) throws QvtRuntimeException {
        try {
            return timingCalculator.calculateDuration(self);
        } catch (MotionException e) {
            throw new QvtRuntimeException("Failed to calculate " + toDebugString(self), e);
        }
    }

    @Operation(contextual = true)
    public String toDebugString(Action self) {
        return ActivityValidator.id(self) + " in activity " + self.getGraph().getName();
    }
}
