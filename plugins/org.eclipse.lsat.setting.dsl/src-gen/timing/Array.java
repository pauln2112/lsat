/**
 */
package timing;

import expressions.Expression;
import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link timing.Array#getValues <em>Values</em>}</li>
 *   <li>{@link timing.Array#getValuesExp <em>Values Exp</em>}</li>
 * </ul>
 *
 * @see timing.TimingPackage#getArray()
 * @model
 * @generated
 */
public interface Array extends Timing {
	/**
     * Returns the value of the '<em><b>Values</b></em>' attribute list.
     * The list contents are of type {@link java.math.BigDecimal}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Values</em>' attribute list.
     * @see timing.TimingPackage#getArray_Values()
     * @model unique="false" required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	EList<BigDecimal> getValues();

	/**
     * Returns the value of the '<em><b>Values Exp</b></em>' containment reference list.
     * The list contents are of type {@link expressions.Expression}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Values Exp</em>' containment reference list.
     * @see timing.TimingPackage#getArray_ValuesExp()
     * @model containment="true" required="true"
     * @generated
     */
	EList<Expression> getValuesExp();

} // Array
