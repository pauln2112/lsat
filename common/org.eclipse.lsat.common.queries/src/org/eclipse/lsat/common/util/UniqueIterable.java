/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;

public class UniqueIterable<E> extends LoggableIterable<E> {
    private final Iterable<? extends E> source;

    private final boolean useEquals;

    public UniqueIterable(Iterable<? extends E> source) {
        this(source, true);
    }

    public UniqueIterable(Iterable<? extends E> source, boolean useEquals) {
        this.source = source;
        this.useEquals = useEquals;
    }

    @Override
    public Iterator<E> iterator() {
        return new UniqueIterator<E>(source.iterator(), useEquals);
    }
}
