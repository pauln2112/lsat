/**
 */
package activity;

import machine.MachinePackage;
import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see activity.ActivityFactory
 * @model kind="package"
 * @generated
 */
public interface ActivityPackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "activity";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://www.eclipse.org/lsat/activity";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "activity";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	ActivityPackage eINSTANCE = activity.impl.ActivityPackageImpl.init();

	/**
     * The meta object id for the '{@link activity.impl.ActivitySetImpl <em>Set</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.ActivitySetImpl
     * @see activity.impl.ActivityPackageImpl#getActivitySet()
     * @generated
     */
	int ACTIVITY_SET = 0;

	/**
     * The feature id for the '<em><b>Imports</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY_SET__IMPORTS = MachinePackage.IMPORT_CONTAINER__IMPORTS;

	/**
     * The feature id for the '<em><b>Activities</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY_SET__ACTIVITIES = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Events</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ACTIVITY_SET__EVENTS = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Set</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY_SET_FEATURE_COUNT = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 2;

	/**
     * The operation id for the '<em>Load All</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY_SET___LOAD_ALL = MachinePackage.IMPORT_CONTAINER___LOAD_ALL;

	/**
     * The number of operations of the '<em>Set</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY_SET_OPERATION_COUNT = MachinePackage.IMPORT_CONTAINER_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.ActivityImpl <em>Activity</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.ActivityImpl
     * @see activity.impl.ActivityPackageImpl#getActivity()
     * @generated
     */
	int ACTIVITY = 1;

	/**
     * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY__NODES = EdgPackage.EDITABLE_DIRECTED_GRAPH__NODES;

	/**
     * The feature id for the '<em><b>Edges</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY__EDGES = EdgPackage.EDITABLE_DIRECTED_GRAPH__EDGES;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY__NAME = EdgPackage.EDITABLE_DIRECTED_GRAPH__NAME;

	/**
     * The feature id for the '<em><b>Prerequisites</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY__PREREQUISITES = EdgPackage.EDITABLE_DIRECTED_GRAPH_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Original Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY__ORIGINAL_NAME = EdgPackage.EDITABLE_DIRECTED_GRAPH_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Activity</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY_FEATURE_COUNT = EdgPackage.EDITABLE_DIRECTED_GRAPH_FEATURE_COUNT + 2;

	/**
     * The operation id for the '<em>All Nodes In Topological Order</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY___ALL_NODES_IN_TOPOLOGICAL_ORDER = EdgPackage.EDITABLE_DIRECTED_GRAPH___ALL_NODES_IN_TOPOLOGICAL_ORDER;

	/**
     * The operation id for the '<em>Is Expanded</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY___IS_EXPANDED = EdgPackage.EDITABLE_DIRECTED_GRAPH_OPERATION_COUNT + 0;

	/**
     * The operation id for the '<em>Get Resources Needing Item</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY___GET_RESOURCES_NEEDING_ITEM = EdgPackage.EDITABLE_DIRECTED_GRAPH_OPERATION_COUNT + 1;

	/**
     * The number of operations of the '<em>Activity</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTIVITY_OPERATION_COUNT = EdgPackage.EDITABLE_DIRECTED_GRAPH_OPERATION_COUNT + 2;

	/**
     * The meta object id for the '{@link activity.impl.ActionImpl <em>Action</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.ActionImpl
     * @see activity.impl.ActivityPackageImpl#getAction()
     * @generated
     */
	int ACTION = 2;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__NAME = EdgPackage.NODE__NAME;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__GRAPH = EdgPackage.NODE__GRAPH;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__INCOMING_EDGES = EdgPackage.NODE__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__OUTGOING_EDGES = EdgPackage.NODE__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__TARGET_REFERENCES = EdgPackage.NODE__TARGET_REFERENCES;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__SOURCE_REFERENCES = EdgPackage.NODE__SOURCE_REFERENCES;

	/**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__OUTER_ENTRY = EdgPackage.NODE_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__ENTRY = EdgPackage.NODE_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__EXIT = EdgPackage.NODE_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION__OUTER_EXIT = EdgPackage.NODE_FEATURE_COUNT + 3;

	/**
     * The number of structural features of the '<em>Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_FEATURE_COUNT = EdgPackage.NODE_FEATURE_COUNT + 4;

	/**
     * The number of operations of the '<em>Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_OPERATION_COUNT = EdgPackage.NODE_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.PeripheralActionImpl <em>Peripheral Action</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.PeripheralActionImpl
     * @see activity.impl.ActivityPackageImpl#getPeripheralAction()
     * @generated
     */
	int PERIPHERAL_ACTION = 3;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__NAME = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__GRAPH = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__INCOMING_EDGES = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__OUTGOING_EDGES = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__TARGET_REFERENCES = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__SOURCE_REFERENCES = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 5;

	/**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__OUTER_ENTRY = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 6;

	/**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__ENTRY = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 7;

	/**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__EXIT = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 8;

	/**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__OUTER_EXIT = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 9;

    /**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__RESOURCE = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 10;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__PERIPHERAL = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 11;

	/**
     * The feature id for the '<em><b>Scheduling Type</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION__SCHEDULING_TYPE = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 12;

	/**
     * The number of structural features of the '<em>Peripheral Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_FEATURE_COUNT = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 13;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION___FQN = MachinePackage.HAS_RESOURCE_PERIPHERAL___FQN;

	/**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION___GET_RESOURCE = MachinePackage.HAS_RESOURCE_PERIPHERAL___GET_RESOURCE;

	/**
     * The operation id for the '<em>Get Peripheral</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION___GET_PERIPHERAL = MachinePackage.HAS_RESOURCE_PERIPHERAL___GET_PERIPHERAL;

	/**
     * The operation id for the '<em>Rp Equals</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION___RP_EQUALS__HASRESOURCEPERIPHERAL = MachinePackage.HAS_RESOURCE_PERIPHERAL___RP_EQUALS__HASRESOURCEPERIPHERAL;

	/**
     * The number of operations of the '<em>Peripheral Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_OPERATION_COUNT = MachinePackage.HAS_RESOURCE_PERIPHERAL_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.ResourceActionImpl <em>Resource Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see activity.impl.ResourceActionImpl
     * @see activity.impl.ActivityPackageImpl#getResourceAction()
     * @generated
     */
    int RESOURCE_ACTION = 14;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__NAME = ACTION__NAME;

    /**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__GRAPH = ACTION__GRAPH;

    /**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__INCOMING_EDGES = ACTION__INCOMING_EDGES;

    /**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__OUTGOING_EDGES = ACTION__OUTGOING_EDGES;

    /**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__TARGET_REFERENCES = ACTION__TARGET_REFERENCES;

    /**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__SOURCE_REFERENCES = ACTION__SOURCE_REFERENCES;

    /**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__OUTER_ENTRY = ACTION__OUTER_ENTRY;

    /**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__ENTRY = ACTION__ENTRY;

    /**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__EXIT = ACTION__EXIT;

    /**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__OUTER_EXIT = ACTION__OUTER_EXIT;

    /**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION__RESOURCE = ACTION_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Resource Action</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Resource Action</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link activity.impl.ClaimImpl <em>Claim</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.ClaimImpl
     * @see activity.impl.ActivityPackageImpl#getClaim()
     * @generated
     */
	int CLAIM = 4;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__NAME = RESOURCE_ACTION__NAME;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__GRAPH = RESOURCE_ACTION__GRAPH;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__INCOMING_EDGES = RESOURCE_ACTION__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__OUTGOING_EDGES = RESOURCE_ACTION__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__TARGET_REFERENCES = RESOURCE_ACTION__TARGET_REFERENCES;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__SOURCE_REFERENCES = RESOURCE_ACTION__SOURCE_REFERENCES;

	/**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__OUTER_ENTRY = RESOURCE_ACTION__OUTER_ENTRY;

	/**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__ENTRY = RESOURCE_ACTION__ENTRY;

	/**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__EXIT = RESOURCE_ACTION__EXIT;

	/**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__OUTER_EXIT = RESOURCE_ACTION__OUTER_EXIT;

    /**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM__RESOURCE = RESOURCE_ACTION__RESOURCE;

	/**
     * The feature id for the '<em><b>Passive</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CLAIM__PASSIVE = RESOURCE_ACTION_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Claim</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_FEATURE_COUNT = RESOURCE_ACTION_FEATURE_COUNT + 1;

	/**
     * The number of operations of the '<em>Claim</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_OPERATION_COUNT = RESOURCE_ACTION_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.ReleaseImpl <em>Release</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.ReleaseImpl
     * @see activity.impl.ActivityPackageImpl#getRelease()
     * @generated
     */
	int RELEASE = 5;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__NAME = RESOURCE_ACTION__NAME;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__GRAPH = RESOURCE_ACTION__GRAPH;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__INCOMING_EDGES = RESOURCE_ACTION__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__OUTGOING_EDGES = RESOURCE_ACTION__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__TARGET_REFERENCES = RESOURCE_ACTION__TARGET_REFERENCES;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__SOURCE_REFERENCES = RESOURCE_ACTION__SOURCE_REFERENCES;

	/**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__OUTER_ENTRY = RESOURCE_ACTION__OUTER_ENTRY;

	/**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__ENTRY = RESOURCE_ACTION__ENTRY;

	/**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__EXIT = RESOURCE_ACTION__EXIT;

	/**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__OUTER_EXIT = RESOURCE_ACTION__OUTER_EXIT;

    /**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE__RESOURCE = RESOURCE_ACTION__RESOURCE;

	/**
     * The number of structural features of the '<em>Release</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_FEATURE_COUNT = RESOURCE_ACTION_FEATURE_COUNT + 0;

	/**
     * The number of operations of the '<em>Release</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_OPERATION_COUNT = RESOURCE_ACTION_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.LocationPrerequisiteImpl <em>Location Prerequisite</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.LocationPrerequisiteImpl
     * @see activity.impl.ActivityPackageImpl#getLocationPrerequisite()
     * @generated
     */
	int LOCATION_PREREQUISITE = 6;

	/**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE__RESOURCE = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE__PERIPHERAL = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Position</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE__POSITION = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Activity</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE__ACTIVITY = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 3;

	/**
     * The number of structural features of the '<em>Location Prerequisite</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE_FEATURE_COUNT = MachinePackage.HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT + 4;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE___FQN = MachinePackage.HAS_RESOURCE_PERIPHERAL___FQN;

	/**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE___GET_RESOURCE = MachinePackage.HAS_RESOURCE_PERIPHERAL___GET_RESOURCE;

	/**
     * The operation id for the '<em>Get Peripheral</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE___GET_PERIPHERAL = MachinePackage.HAS_RESOURCE_PERIPHERAL___GET_PERIPHERAL;

	/**
     * The operation id for the '<em>Rp Equals</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE___RP_EQUALS__HASRESOURCEPERIPHERAL = MachinePackage.HAS_RESOURCE_PERIPHERAL___RP_EQUALS__HASRESOURCEPERIPHERAL;

	/**
     * The number of operations of the '<em>Location Prerequisite</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int LOCATION_PREREQUISITE_OPERATION_COUNT = MachinePackage.HAS_RESOURCE_PERIPHERAL_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.TracePointImpl <em>Trace Point</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.TracePointImpl
     * @see activity.impl.ActivityPackageImpl#getTracePoint()
     * @generated
     */
	int TRACE_POINT = 7;

	/**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRACE_POINT__VALUE = 0;

	/**
     * The feature id for the '<em><b>Regex</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRACE_POINT__REGEX = 1;

	/**
     * The number of structural features of the '<em>Trace Point</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRACE_POINT_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Trace Point</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TRACE_POINT_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link activity.impl.SyncBarImpl <em>Sync Bar</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.SyncBarImpl
     * @see activity.impl.ActivityPackageImpl#getSyncBar()
     * @generated
     */
	int SYNC_BAR = 8;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYNC_BAR__NAME = EdgPackage.NODE__NAME;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYNC_BAR__GRAPH = EdgPackage.NODE__GRAPH;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYNC_BAR__INCOMING_EDGES = EdgPackage.NODE__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYNC_BAR__OUTGOING_EDGES = EdgPackage.NODE__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYNC_BAR__TARGET_REFERENCES = EdgPackage.NODE__TARGET_REFERENCES;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYNC_BAR__SOURCE_REFERENCES = EdgPackage.NODE__SOURCE_REFERENCES;

	/**
     * The number of structural features of the '<em>Sync Bar</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYNC_BAR_FEATURE_COUNT = EdgPackage.NODE_FEATURE_COUNT + 0;

	/**
     * The number of operations of the '<em>Sync Bar</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYNC_BAR_OPERATION_COUNT = EdgPackage.NODE_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.SimpleActionImpl <em>Simple Action</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.SimpleActionImpl
     * @see activity.impl.ActivityPackageImpl#getSimpleAction()
     * @generated
     */
	int SIMPLE_ACTION = 9;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__NAME = PERIPHERAL_ACTION__NAME;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__GRAPH = PERIPHERAL_ACTION__GRAPH;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__INCOMING_EDGES = PERIPHERAL_ACTION__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__OUTGOING_EDGES = PERIPHERAL_ACTION__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__TARGET_REFERENCES = PERIPHERAL_ACTION__TARGET_REFERENCES;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__SOURCE_REFERENCES = PERIPHERAL_ACTION__SOURCE_REFERENCES;

	/**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__OUTER_ENTRY = PERIPHERAL_ACTION__OUTER_ENTRY;

	/**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__ENTRY = PERIPHERAL_ACTION__ENTRY;

	/**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__EXIT = PERIPHERAL_ACTION__EXIT;

	/**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__OUTER_EXIT = PERIPHERAL_ACTION__OUTER_EXIT;

    /**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__RESOURCE = PERIPHERAL_ACTION__RESOURCE;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__PERIPHERAL = PERIPHERAL_ACTION__PERIPHERAL;

	/**
     * The feature id for the '<em><b>Scheduling Type</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__SCHEDULING_TYPE = PERIPHERAL_ACTION__SCHEDULING_TYPE;

	/**
     * The feature id for the '<em><b>Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION__TYPE = PERIPHERAL_ACTION_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Simple Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION_FEATURE_COUNT = PERIPHERAL_ACTION_FEATURE_COUNT + 1;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION___FQN = PERIPHERAL_ACTION___FQN;

	/**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION___GET_RESOURCE = PERIPHERAL_ACTION___GET_RESOURCE;

	/**
     * The operation id for the '<em>Get Peripheral</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION___GET_PERIPHERAL = PERIPHERAL_ACTION___GET_PERIPHERAL;

	/**
     * The operation id for the '<em>Rp Equals</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION___RP_EQUALS__HASRESOURCEPERIPHERAL = PERIPHERAL_ACTION___RP_EQUALS__HASRESOURCEPERIPHERAL;

	/**
     * The number of operations of the '<em>Simple Action</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SIMPLE_ACTION_OPERATION_COUNT = PERIPHERAL_ACTION_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.MoveImpl <em>Move</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.MoveImpl
     * @see activity.impl.ActivityPackageImpl#getMove()
     * @generated
     */
	int MOVE = 10;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__NAME = PERIPHERAL_ACTION__NAME;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__GRAPH = PERIPHERAL_ACTION__GRAPH;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__INCOMING_EDGES = PERIPHERAL_ACTION__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__OUTGOING_EDGES = PERIPHERAL_ACTION__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__TARGET_REFERENCES = PERIPHERAL_ACTION__TARGET_REFERENCES;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__SOURCE_REFERENCES = PERIPHERAL_ACTION__SOURCE_REFERENCES;

	/**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__OUTER_ENTRY = PERIPHERAL_ACTION__OUTER_ENTRY;

	/**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__ENTRY = PERIPHERAL_ACTION__ENTRY;

	/**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__EXIT = PERIPHERAL_ACTION__EXIT;

	/**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__OUTER_EXIT = PERIPHERAL_ACTION__OUTER_EXIT;

    /**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__RESOURCE = PERIPHERAL_ACTION__RESOURCE;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__PERIPHERAL = PERIPHERAL_ACTION__PERIPHERAL;

	/**
     * The feature id for the '<em><b>Scheduling Type</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__SCHEDULING_TYPE = PERIPHERAL_ACTION__SCHEDULING_TYPE;

	/**
     * The feature id for the '<em><b>Profile</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__PROFILE = PERIPHERAL_ACTION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Successor Move</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__SUCCESSOR_MOVE = PERIPHERAL_ACTION_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Predecessor Move</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__PREDECESSOR_MOVE = PERIPHERAL_ACTION_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Stop At Target</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__STOP_AT_TARGET = PERIPHERAL_ACTION_FEATURE_COUNT + 3;

	/**
     * The feature id for the '<em><b>Position Move</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__POSITION_MOVE = PERIPHERAL_ACTION_FEATURE_COUNT + 4;

	/**
     * The feature id for the '<em><b>Target Position</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__TARGET_POSITION = PERIPHERAL_ACTION_FEATURE_COUNT + 5;

	/**
     * The feature id for the '<em><b>Source Position</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__SOURCE_POSITION = PERIPHERAL_ACTION_FEATURE_COUNT + 6;

	/**
     * The feature id for the '<em><b>Distance</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__DISTANCE = PERIPHERAL_ACTION_FEATURE_COUNT + 7;

	/**
     * The feature id for the '<em><b>Passing</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__PASSING = PERIPHERAL_ACTION_FEATURE_COUNT + 8;

	/**
     * The feature id for the '<em><b>Continuing</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE__CONTINUING = PERIPHERAL_ACTION_FEATURE_COUNT + 9;

	/**
     * The number of structural features of the '<em>Move</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE_FEATURE_COUNT = PERIPHERAL_ACTION_FEATURE_COUNT + 10;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE___FQN = PERIPHERAL_ACTION___FQN;

	/**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE___GET_RESOURCE = PERIPHERAL_ACTION___GET_RESOURCE;

	/**
     * The operation id for the '<em>Get Peripheral</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE___GET_PERIPHERAL = PERIPHERAL_ACTION___GET_PERIPHERAL;

	/**
     * The operation id for the '<em>Rp Equals</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE___RP_EQUALS__HASRESOURCEPERIPHERAL = PERIPHERAL_ACTION___RP_EQUALS__HASRESOURCEPERIPHERAL;

	/**
     * The number of operations of the '<em>Move</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MOVE_OPERATION_COUNT = PERIPHERAL_ACTION_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.EventActionImpl <em>Event Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see activity.impl.EventActionImpl
     * @see activity.impl.ActivityPackageImpl#getEventAction()
     * @generated
     */
    int EVENT_ACTION = 11;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__NAME = RESOURCE_ACTION__NAME;

    /**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__GRAPH = RESOURCE_ACTION__GRAPH;

    /**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__INCOMING_EDGES = RESOURCE_ACTION__INCOMING_EDGES;

    /**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__OUTGOING_EDGES = RESOURCE_ACTION__OUTGOING_EDGES;

    /**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__TARGET_REFERENCES = RESOURCE_ACTION__TARGET_REFERENCES;

    /**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__SOURCE_REFERENCES = RESOURCE_ACTION__SOURCE_REFERENCES;

    /**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__OUTER_ENTRY = RESOURCE_ACTION__OUTER_ENTRY;

    /**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__ENTRY = RESOURCE_ACTION__ENTRY;

    /**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__EXIT = RESOURCE_ACTION__EXIT;

    /**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__OUTER_EXIT = RESOURCE_ACTION__OUTER_EXIT;

    /**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION__RESOURCE = RESOURCE_ACTION__RESOURCE;

    /**
     * The number of structural features of the '<em>Event Action</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION_FEATURE_COUNT = RESOURCE_ACTION_FEATURE_COUNT + 0;

    /**
     * The number of operations of the '<em>Event Action</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ACTION_OPERATION_COUNT = RESOURCE_ACTION_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link activity.impl.EventImpl <em>Event</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.EventImpl
     * @see activity.impl.ActivityPackageImpl#getEvent()
     * @generated
     */
	int EVENT = 15;

	/**
     * The meta object id for the '{@link activity.impl.RaiseEventImpl <em>Raise Event</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.RaiseEventImpl
     * @see activity.impl.ActivityPackageImpl#getRaiseEvent()
     * @generated
     */
	int RAISE_EVENT = 12;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RAISE_EVENT__NAME = EVENT_ACTION__NAME;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RAISE_EVENT__GRAPH = EVENT_ACTION__GRAPH;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RAISE_EVENT__INCOMING_EDGES = EVENT_ACTION__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RAISE_EVENT__OUTGOING_EDGES = EVENT_ACTION__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RAISE_EVENT__TARGET_REFERENCES = EVENT_ACTION__TARGET_REFERENCES;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RAISE_EVENT__SOURCE_REFERENCES = EVENT_ACTION__SOURCE_REFERENCES;

	/**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_EVENT__OUTER_ENTRY = EVENT_ACTION__OUTER_ENTRY;

    /**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_EVENT__ENTRY = EVENT_ACTION__ENTRY;

    /**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_EVENT__EXIT = EVENT_ACTION__EXIT;

    /**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_EVENT__OUTER_EXIT = EVENT_ACTION__OUTER_EXIT;

    /**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_EVENT__RESOURCE = EVENT_ACTION__RESOURCE;

    /**
     * The number of structural features of the '<em>Raise Event</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RAISE_EVENT_FEATURE_COUNT = EVENT_ACTION_FEATURE_COUNT + 0;

	/**
     * The number of operations of the '<em>Raise Event</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RAISE_EVENT_OPERATION_COUNT = EVENT_ACTION_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link activity.impl.RequireEventImpl <em>Require Event</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.impl.RequireEventImpl
     * @see activity.impl.ActivityPackageImpl#getRequireEvent()
     * @generated
     */
	int REQUIRE_EVENT = 13;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REQUIRE_EVENT__NAME = EVENT_ACTION__NAME;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REQUIRE_EVENT__GRAPH = EVENT_ACTION__GRAPH;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REQUIRE_EVENT__INCOMING_EDGES = EVENT_ACTION__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REQUIRE_EVENT__OUTGOING_EDGES = EVENT_ACTION__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REQUIRE_EVENT__TARGET_REFERENCES = EVENT_ACTION__TARGET_REFERENCES;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REQUIRE_EVENT__SOURCE_REFERENCES = EVENT_ACTION__SOURCE_REFERENCES;

	/**
     * The feature id for the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_EVENT__OUTER_ENTRY = EVENT_ACTION__OUTER_ENTRY;

    /**
     * The feature id for the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_EVENT__ENTRY = EVENT_ACTION__ENTRY;

    /**
     * The feature id for the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_EVENT__EXIT = EVENT_ACTION__EXIT;

    /**
     * The feature id for the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_EVENT__OUTER_EXIT = EVENT_ACTION__OUTER_EXIT;

    /**
     * The feature id for the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_EVENT__RESOURCE = EVENT_ACTION__RESOURCE;

    /**
     * The number of structural features of the '<em>Require Event</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REQUIRE_EVENT_FEATURE_COUNT = EVENT_ACTION_FEATURE_COUNT + 0;

	/**
     * The number of operations of the '<em>Require Event</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REQUIRE_EVENT_OPERATION_COUNT = EVENT_ACTION_OPERATION_COUNT + 0;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT__NAME = MachinePackage.RESOURCE__NAME;

    /**
     * The feature id for the '<em><b>Peripherals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT__PERIPHERALS = MachinePackage.RESOURCE__PERIPHERALS;

    /**
     * The feature id for the '<em><b>Items</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT__ITEMS = MachinePackage.RESOURCE__ITEMS;

    /**
     * The feature id for the '<em><b>Resource Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT__RESOURCE_TYPE = MachinePackage.RESOURCE__RESOURCE_TYPE;

    /**
     * The number of structural features of the '<em>Event</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_FEATURE_COUNT = MachinePackage.RESOURCE_FEATURE_COUNT + 0;

    /**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT___GET_RESOURCE = MachinePackage.RESOURCE___GET_RESOURCE;

    /**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT___FQN = MachinePackage.RESOURCE___FQN;

    /**
     * The operation id for the '<em>Get Resource Type</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT___GET_RESOURCE_TYPE = MachinePackage.RESOURCE_OPERATION_COUNT + 0;

    /**
     * The number of operations of the '<em>Event</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_OPERATION_COUNT = MachinePackage.RESOURCE_OPERATION_COUNT + 1;

	/**
     * The meta object id for the '{@link activity.impl.EventItemImpl <em>Event Item</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see activity.impl.EventItemImpl
     * @see activity.impl.ActivityPackageImpl#getEventItem()
     * @generated
     */
    int EVENT_ITEM = 16;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ITEM__NAME = MachinePackage.RESOURCE_ITEM__NAME;

    /**
     * The feature id for the '<em><b>Resource</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ITEM__RESOURCE = MachinePackage.RESOURCE_ITEM__RESOURCE;

    /**
     * The number of structural features of the '<em>Event Item</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ITEM_FEATURE_COUNT = MachinePackage.RESOURCE_ITEM_FEATURE_COUNT + 0;

    /**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ITEM___GET_RESOURCE = MachinePackage.RESOURCE_ITEM___GET_RESOURCE;

    /**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ITEM___FQN = MachinePackage.RESOURCE_ITEM___FQN;

    /**
     * The number of operations of the '<em>Event Item</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ITEM_OPERATION_COUNT = MachinePackage.RESOURCE_ITEM_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link activity.SchedulingType <em>Scheduling Type</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see activity.SchedulingType
     * @see activity.impl.ActivityPackageImpl#getSchedulingType()
     * @generated
     */
	int SCHEDULING_TYPE = 17;


	/**
     * Returns the meta object for class '{@link activity.ActivitySet <em>Set</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Set</em>'.
     * @see activity.ActivitySet
     * @generated
     */
	EClass getActivitySet();

	/**
     * Returns the meta object for the containment reference list '{@link activity.ActivitySet#getActivities <em>Activities</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Activities</em>'.
     * @see activity.ActivitySet#getActivities()
     * @see #getActivitySet()
     * @generated
     */
	EReference getActivitySet_Activities();

	/**
     * Returns the meta object for the containment reference list '{@link activity.ActivitySet#getEvents <em>Events</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Events</em>'.
     * @see activity.ActivitySet#getEvents()
     * @see #getActivitySet()
     * @generated
     */
    EReference getActivitySet_Events();

    /**
     * Returns the meta object for class '{@link activity.Activity <em>Activity</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Activity</em>'.
     * @see activity.Activity
     * @generated
     */
	EClass getActivity();

	/**
     * Returns the meta object for the containment reference list '{@link activity.Activity#getPrerequisites <em>Prerequisites</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Prerequisites</em>'.
     * @see activity.Activity#getPrerequisites()
     * @see #getActivity()
     * @generated
     */
	EReference getActivity_Prerequisites();

	/**
     * Returns the meta object for the attribute '{@link activity.Activity#getOriginalName <em>Original Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Original Name</em>'.
     * @see activity.Activity#getOriginalName()
     * @see #getActivity()
     * @generated
     */
	EAttribute getActivity_OriginalName();

	/**
     * Returns the meta object for the '{@link activity.Activity#isExpanded() <em>Is Expanded</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Is Expanded</em>' operation.
     * @see activity.Activity#isExpanded()
     * @generated
     */
	EOperation getActivity__IsExpanded();

	/**
     * Returns the meta object for the '{@link activity.Activity#getResourcesNeedingItem() <em>Get Resources Needing Item</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Resources Needing Item</em>' operation.
     * @see activity.Activity#getResourcesNeedingItem()
     * @generated
     */
	EOperation getActivity__GetResourcesNeedingItem();

	/**
     * Returns the meta object for class '{@link activity.Action <em>Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Action</em>'.
     * @see activity.Action
     * @generated
     */
	EClass getAction();

	/**
     * Returns the meta object for the containment reference '{@link activity.Action#getOuterEntry <em>Outer Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Outer Entry</em>'.
     * @see activity.Action#getOuterEntry()
     * @see #getAction()
     * @generated
     */
	EReference getAction_OuterEntry();

	/**
     * Returns the meta object for the containment reference '{@link activity.Action#getEntry <em>Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Entry</em>'.
     * @see activity.Action#getEntry()
     * @see #getAction()
     * @generated
     */
	EReference getAction_Entry();

	/**
     * Returns the meta object for the containment reference '{@link activity.Action#getExit <em>Exit</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Exit</em>'.
     * @see activity.Action#getExit()
     * @see #getAction()
     * @generated
     */
	EReference getAction_Exit();

	/**
     * Returns the meta object for the containment reference '{@link activity.Action#getOuterExit <em>Outer Exit</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Outer Exit</em>'.
     * @see activity.Action#getOuterExit()
     * @see #getAction()
     * @generated
     */
	EReference getAction_OuterExit();

	/**
     * Returns the meta object for class '{@link activity.PeripheralAction <em>Peripheral Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Peripheral Action</em>'.
     * @see activity.PeripheralAction
     * @generated
     */
	EClass getPeripheralAction();

	/**
     * Returns the meta object for the reference '{@link activity.PeripheralAction#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Peripheral</em>'.
     * @see activity.PeripheralAction#getPeripheral()
     * @see #getPeripheralAction()
     * @generated
     */
	EReference getPeripheralAction_Peripheral();

	/**
     * Returns the meta object for the attribute '{@link activity.PeripheralAction#getSchedulingType <em>Scheduling Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Scheduling Type</em>'.
     * @see activity.PeripheralAction#getSchedulingType()
     * @see #getPeripheralAction()
     * @generated
     */
	EAttribute getPeripheralAction_SchedulingType();

	/**
     * Returns the meta object for class '{@link activity.Claim <em>Claim</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Claim</em>'.
     * @see activity.Claim
     * @generated
     */
	EClass getClaim();

	/**
     * Returns the meta object for the attribute '{@link activity.Claim#isPassive <em>Passive</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Passive</em>'.
     * @see activity.Claim#isPassive()
     * @see #getClaim()
     * @generated
     */
    EAttribute getClaim_Passive();

    /**
     * Returns the meta object for class '{@link activity.Release <em>Release</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Release</em>'.
     * @see activity.Release
     * @generated
     */
	EClass getRelease();

	/**
     * Returns the meta object for class '{@link activity.LocationPrerequisite <em>Location Prerequisite</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Location Prerequisite</em>'.
     * @see activity.LocationPrerequisite
     * @generated
     */
	EClass getLocationPrerequisite();

	/**
     * Returns the meta object for the reference '{@link activity.LocationPrerequisite#getResource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Resource</em>'.
     * @see activity.LocationPrerequisite#getResource()
     * @see #getLocationPrerequisite()
     * @generated
     */
	EReference getLocationPrerequisite_Resource();

	/**
     * Returns the meta object for the reference '{@link activity.LocationPrerequisite#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Peripheral</em>'.
     * @see activity.LocationPrerequisite#getPeripheral()
     * @see #getLocationPrerequisite()
     * @generated
     */
	EReference getLocationPrerequisite_Peripheral();

	/**
     * Returns the meta object for the reference '{@link activity.LocationPrerequisite#getPosition <em>Position</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Position</em>'.
     * @see activity.LocationPrerequisite#getPosition()
     * @see #getLocationPrerequisite()
     * @generated
     */
	EReference getLocationPrerequisite_Position();

	/**
     * Returns the meta object for the container reference '{@link activity.LocationPrerequisite#getActivity <em>Activity</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Activity</em>'.
     * @see activity.LocationPrerequisite#getActivity()
     * @see #getLocationPrerequisite()
     * @generated
     */
	EReference getLocationPrerequisite_Activity();

	/**
     * Returns the meta object for class '{@link activity.TracePoint <em>Trace Point</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Trace Point</em>'.
     * @see activity.TracePoint
     * @generated
     */
	EClass getTracePoint();

	/**
     * Returns the meta object for the attribute '{@link activity.TracePoint#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see activity.TracePoint#getValue()
     * @see #getTracePoint()
     * @generated
     */
	EAttribute getTracePoint_Value();

	/**
     * Returns the meta object for the attribute '{@link activity.TracePoint#isRegex <em>Regex</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Regex</em>'.
     * @see activity.TracePoint#isRegex()
     * @see #getTracePoint()
     * @generated
     */
	EAttribute getTracePoint_Regex();

	/**
     * Returns the meta object for class '{@link activity.SyncBar <em>Sync Bar</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Sync Bar</em>'.
     * @see activity.SyncBar
     * @generated
     */
	EClass getSyncBar();

	/**
     * Returns the meta object for class '{@link activity.SimpleAction <em>Simple Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Simple Action</em>'.
     * @see activity.SimpleAction
     * @generated
     */
	EClass getSimpleAction();

	/**
     * Returns the meta object for the reference '{@link activity.SimpleAction#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Type</em>'.
     * @see activity.SimpleAction#getType()
     * @see #getSimpleAction()
     * @generated
     */
	EReference getSimpleAction_Type();

	/**
     * Returns the meta object for class '{@link activity.Move <em>Move</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Move</em>'.
     * @see activity.Move
     * @generated
     */
	EClass getMove();

	/**
     * Returns the meta object for the reference '{@link activity.Move#getProfile <em>Profile</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Profile</em>'.
     * @see activity.Move#getProfile()
     * @see #getMove()
     * @generated
     */
	EReference getMove_Profile();

	/**
     * Returns the meta object for the reference '{@link activity.Move#getSuccessorMove <em>Successor Move</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Successor Move</em>'.
     * @see activity.Move#getSuccessorMove()
     * @see #getMove()
     * @generated
     */
	EReference getMove_SuccessorMove();

	/**
     * Returns the meta object for the reference '{@link activity.Move#getPredecessorMove <em>Predecessor Move</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Predecessor Move</em>'.
     * @see activity.Move#getPredecessorMove()
     * @see #getMove()
     * @generated
     */
	EReference getMove_PredecessorMove();

	/**
     * Returns the meta object for the attribute '{@link activity.Move#isStopAtTarget <em>Stop At Target</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Stop At Target</em>'.
     * @see activity.Move#isStopAtTarget()
     * @see #getMove()
     * @generated
     */
	EAttribute getMove_StopAtTarget();

	/**
     * Returns the meta object for the attribute '{@link activity.Move#isPositionMove <em>Position Move</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Position Move</em>'.
     * @see activity.Move#isPositionMove()
     * @see #getMove()
     * @generated
     */
	EAttribute getMove_PositionMove();

	/**
     * Returns the meta object for the reference '{@link activity.Move#getTargetPosition <em>Target Position</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Target Position</em>'.
     * @see activity.Move#getTargetPosition()
     * @see #getMove()
     * @generated
     */
	EReference getMove_TargetPosition();

	/**
     * Returns the meta object for the reference '{@link activity.Move#getSourcePosition <em>Source Position</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Source Position</em>'.
     * @see activity.Move#getSourcePosition()
     * @see #getMove()
     * @generated
     */
	EReference getMove_SourcePosition();

	/**
     * Returns the meta object for the reference '{@link activity.Move#getDistance <em>Distance</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Distance</em>'.
     * @see activity.Move#getDistance()
     * @see #getMove()
     * @generated
     */
	EReference getMove_Distance();

	/**
     * Returns the meta object for the attribute '{@link activity.Move#isPassing <em>Passing</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Passing</em>'.
     * @see activity.Move#isPassing()
     * @see #getMove()
     * @generated
     */
	EAttribute getMove_Passing();

	/**
     * Returns the meta object for the attribute '{@link activity.Move#isContinuing <em>Continuing</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Continuing</em>'.
     * @see activity.Move#isContinuing()
     * @see #getMove()
     * @generated
     */
	EAttribute getMove_Continuing();

	/**
     * Returns the meta object for class '{@link activity.EventAction <em>Event Action</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Event Action</em>'.
     * @see activity.EventAction
     * @generated
     */
    EClass getEventAction();

    /**
     * Returns the meta object for class '{@link activity.Event <em>Event</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Event</em>'.
     * @see activity.Event
     * @generated
     */
	EClass getEvent();

	/**
     * Returns the meta object for the '{@link activity.Event#getResourceType() <em>Get Resource Type</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Resource Type</em>' operation.
     * @see activity.Event#getResourceType()
     * @generated
     */
    EOperation getEvent__GetResourceType();

    /**
     * Returns the meta object for class '{@link activity.RaiseEvent <em>Raise Event</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Raise Event</em>'.
     * @see activity.RaiseEvent
     * @generated
     */
	EClass getRaiseEvent();

	/**
     * Returns the meta object for class '{@link activity.RequireEvent <em>Require Event</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Require Event</em>'.
     * @see activity.RequireEvent
     * @generated
     */
	EClass getRequireEvent();

	/**
     * Returns the meta object for class '{@link activity.ResourceAction <em>Resource Action</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Resource Action</em>'.
     * @see activity.ResourceAction
     * @generated
     */
    EClass getResourceAction();

    /**
     * Returns the meta object for the reference '{@link activity.ResourceAction#getResource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Resource</em>'.
     * @see activity.ResourceAction#getResource()
     * @see #getResourceAction()
     * @generated
     */
    EReference getResourceAction_Resource();

    /**
     * Returns the meta object for class '{@link activity.EventItem <em>Event Item</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Event Item</em>'.
     * @see activity.EventItem
     * @generated
     */
    EClass getEventItem();

    /**
     * Returns the meta object for enum '{@link activity.SchedulingType <em>Scheduling Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Scheduling Type</em>'.
     * @see activity.SchedulingType
     * @generated
     */
	EEnum getSchedulingType();

	/**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	ActivityFactory getActivityFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link activity.impl.ActivitySetImpl <em>Set</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.ActivitySetImpl
         * @see activity.impl.ActivityPackageImpl#getActivitySet()
         * @generated
         */
		EClass ACTIVITY_SET = eINSTANCE.getActivitySet();

		/**
         * The meta object literal for the '<em><b>Activities</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTIVITY_SET__ACTIVITIES = eINSTANCE.getActivitySet_Activities();

		/**
         * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ACTIVITY_SET__EVENTS = eINSTANCE.getActivitySet_Events();

        /**
         * The meta object literal for the '{@link activity.impl.ActivityImpl <em>Activity</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.ActivityImpl
         * @see activity.impl.ActivityPackageImpl#getActivity()
         * @generated
         */
		EClass ACTIVITY = eINSTANCE.getActivity();

		/**
         * The meta object literal for the '<em><b>Prerequisites</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTIVITY__PREREQUISITES = eINSTANCE.getActivity_Prerequisites();

		/**
         * The meta object literal for the '<em><b>Original Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ACTIVITY__ORIGINAL_NAME = eINSTANCE.getActivity_OriginalName();

		/**
         * The meta object literal for the '<em><b>Is Expanded</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation ACTIVITY___IS_EXPANDED = eINSTANCE.getActivity__IsExpanded();

		/**
         * The meta object literal for the '<em><b>Get Resources Needing Item</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation ACTIVITY___GET_RESOURCES_NEEDING_ITEM = eINSTANCE.getActivity__GetResourcesNeedingItem();

		/**
         * The meta object literal for the '{@link activity.impl.ActionImpl <em>Action</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.ActionImpl
         * @see activity.impl.ActivityPackageImpl#getAction()
         * @generated
         */
		EClass ACTION = eINSTANCE.getAction();

		/**
         * The meta object literal for the '<em><b>Outer Entry</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTION__OUTER_ENTRY = eINSTANCE.getAction_OuterEntry();

		/**
         * The meta object literal for the '<em><b>Entry</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTION__ENTRY = eINSTANCE.getAction_Entry();

		/**
         * The meta object literal for the '<em><b>Exit</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTION__EXIT = eINSTANCE.getAction_Exit();

		/**
         * The meta object literal for the '<em><b>Outer Exit</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTION__OUTER_EXIT = eINSTANCE.getAction_OuterExit();

		/**
         * The meta object literal for the '{@link activity.impl.PeripheralActionImpl <em>Peripheral Action</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.PeripheralActionImpl
         * @see activity.impl.ActivityPackageImpl#getPeripheralAction()
         * @generated
         */
		EClass PERIPHERAL_ACTION = eINSTANCE.getPeripheralAction();

		/**
         * The meta object literal for the '<em><b>Peripheral</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL_ACTION__PERIPHERAL = eINSTANCE.getPeripheralAction_Peripheral();

		/**
         * The meta object literal for the '<em><b>Scheduling Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PERIPHERAL_ACTION__SCHEDULING_TYPE = eINSTANCE.getPeripheralAction_SchedulingType();

		/**
         * The meta object literal for the '{@link activity.impl.ClaimImpl <em>Claim</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.ClaimImpl
         * @see activity.impl.ActivityPackageImpl#getClaim()
         * @generated
         */
		EClass CLAIM = eINSTANCE.getClaim();

		/**
         * The meta object literal for the '<em><b>Passive</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute CLAIM__PASSIVE = eINSTANCE.getClaim_Passive();

        /**
         * The meta object literal for the '{@link activity.impl.ReleaseImpl <em>Release</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.ReleaseImpl
         * @see activity.impl.ActivityPackageImpl#getRelease()
         * @generated
         */
		EClass RELEASE = eINSTANCE.getRelease();

		/**
         * The meta object literal for the '{@link activity.impl.LocationPrerequisiteImpl <em>Location Prerequisite</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.LocationPrerequisiteImpl
         * @see activity.impl.ActivityPackageImpl#getLocationPrerequisite()
         * @generated
         */
		EClass LOCATION_PREREQUISITE = eINSTANCE.getLocationPrerequisite();

		/**
         * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LOCATION_PREREQUISITE__RESOURCE = eINSTANCE.getLocationPrerequisite_Resource();

		/**
         * The meta object literal for the '<em><b>Peripheral</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LOCATION_PREREQUISITE__PERIPHERAL = eINSTANCE.getLocationPrerequisite_Peripheral();

		/**
         * The meta object literal for the '<em><b>Position</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LOCATION_PREREQUISITE__POSITION = eINSTANCE.getLocationPrerequisite_Position();

		/**
         * The meta object literal for the '<em><b>Activity</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference LOCATION_PREREQUISITE__ACTIVITY = eINSTANCE.getLocationPrerequisite_Activity();

		/**
         * The meta object literal for the '{@link activity.impl.TracePointImpl <em>Trace Point</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.TracePointImpl
         * @see activity.impl.ActivityPackageImpl#getTracePoint()
         * @generated
         */
		EClass TRACE_POINT = eINSTANCE.getTracePoint();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute TRACE_POINT__VALUE = eINSTANCE.getTracePoint_Value();

		/**
         * The meta object literal for the '<em><b>Regex</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute TRACE_POINT__REGEX = eINSTANCE.getTracePoint_Regex();

		/**
         * The meta object literal for the '{@link activity.impl.SyncBarImpl <em>Sync Bar</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.SyncBarImpl
         * @see activity.impl.ActivityPackageImpl#getSyncBar()
         * @generated
         */
		EClass SYNC_BAR = eINSTANCE.getSyncBar();

		/**
         * The meta object literal for the '{@link activity.impl.SimpleActionImpl <em>Simple Action</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.SimpleActionImpl
         * @see activity.impl.ActivityPackageImpl#getSimpleAction()
         * @generated
         */
		EClass SIMPLE_ACTION = eINSTANCE.getSimpleAction();

		/**
         * The meta object literal for the '<em><b>Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference SIMPLE_ACTION__TYPE = eINSTANCE.getSimpleAction_Type();

		/**
         * The meta object literal for the '{@link activity.impl.MoveImpl <em>Move</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.MoveImpl
         * @see activity.impl.ActivityPackageImpl#getMove()
         * @generated
         */
		EClass MOVE = eINSTANCE.getMove();

		/**
         * The meta object literal for the '<em><b>Profile</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOVE__PROFILE = eINSTANCE.getMove_Profile();

		/**
         * The meta object literal for the '<em><b>Successor Move</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOVE__SUCCESSOR_MOVE = eINSTANCE.getMove_SuccessorMove();

		/**
         * The meta object literal for the '<em><b>Predecessor Move</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOVE__PREDECESSOR_MOVE = eINSTANCE.getMove_PredecessorMove();

		/**
         * The meta object literal for the '<em><b>Stop At Target</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MOVE__STOP_AT_TARGET = eINSTANCE.getMove_StopAtTarget();

		/**
         * The meta object literal for the '<em><b>Position Move</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MOVE__POSITION_MOVE = eINSTANCE.getMove_PositionMove();

		/**
         * The meta object literal for the '<em><b>Target Position</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOVE__TARGET_POSITION = eINSTANCE.getMove_TargetPosition();

		/**
         * The meta object literal for the '<em><b>Source Position</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOVE__SOURCE_POSITION = eINSTANCE.getMove_SourcePosition();

		/**
         * The meta object literal for the '<em><b>Distance</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MOVE__DISTANCE = eINSTANCE.getMove_Distance();

		/**
         * The meta object literal for the '<em><b>Passing</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MOVE__PASSING = eINSTANCE.getMove_Passing();

		/**
         * The meta object literal for the '<em><b>Continuing</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MOVE__CONTINUING = eINSTANCE.getMove_Continuing();

		/**
         * The meta object literal for the '{@link activity.impl.EventActionImpl <em>Event Action</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see activity.impl.EventActionImpl
         * @see activity.impl.ActivityPackageImpl#getEventAction()
         * @generated
         */
        EClass EVENT_ACTION = eINSTANCE.getEventAction();

        /**
         * The meta object literal for the '{@link activity.impl.EventImpl <em>Event</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.EventImpl
         * @see activity.impl.ActivityPackageImpl#getEvent()
         * @generated
         */
		EClass EVENT = eINSTANCE.getEvent();

		/**
         * The meta object literal for the '<em><b>Get Resource Type</b></em>' operation.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EOperation EVENT___GET_RESOURCE_TYPE = eINSTANCE.getEvent__GetResourceType();

        /**
         * The meta object literal for the '{@link activity.impl.RaiseEventImpl <em>Raise Event</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.RaiseEventImpl
         * @see activity.impl.ActivityPackageImpl#getRaiseEvent()
         * @generated
         */
		EClass RAISE_EVENT = eINSTANCE.getRaiseEvent();

		/**
         * The meta object literal for the '{@link activity.impl.RequireEventImpl <em>Require Event</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.impl.RequireEventImpl
         * @see activity.impl.ActivityPackageImpl#getRequireEvent()
         * @generated
         */
		EClass REQUIRE_EVENT = eINSTANCE.getRequireEvent();

		/**
         * The meta object literal for the '{@link activity.impl.ResourceActionImpl <em>Resource Action</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see activity.impl.ResourceActionImpl
         * @see activity.impl.ActivityPackageImpl#getResourceAction()
         * @generated
         */
        EClass RESOURCE_ACTION = eINSTANCE.getResourceAction();

        /**
         * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference RESOURCE_ACTION__RESOURCE = eINSTANCE.getResourceAction_Resource();

        /**
         * The meta object literal for the '{@link activity.impl.EventItemImpl <em>Event Item</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see activity.impl.EventItemImpl
         * @see activity.impl.ActivityPackageImpl#getEventItem()
         * @generated
         */
        EClass EVENT_ITEM = eINSTANCE.getEventItem();

        /**
         * The meta object literal for the '{@link activity.SchedulingType <em>Scheduling Type</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see activity.SchedulingType
         * @see activity.impl.ActivityPackageImpl#getSchedulingType()
         * @generated
         */
		EEnum SCHEDULING_TYPE = eINSTANCE.getSchedulingType();

	}

} //ActivityPackage
