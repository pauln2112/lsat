/**
 */
package org.eclipse.lsat.common.scheduler.schedule;

import org.eclipse.lsat.common.graph.directed.DirectedGraph;

import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.resources.ResourceModel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schedule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.Schedule#getResourceModel <em>Resource Model</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.Schedule#getSequences <em>Sequences</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.Schedule#isEpochTime <em>Epoch Time</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getSchedule()
 * @model
 * @generated
 */
public interface Schedule<T extends Task> extends DirectedGraph<ScheduledTask<T>, ScheduledDependency> {
	/**
	 * Returns the value of the '<em><b>Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Model</em>' reference.
	 * @see #setResourceModel(ResourceModel)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getSchedule_ResourceModel()
	 * @model
	 * @generated
	 */
	ResourceModel getResourceModel();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.Schedule#getResourceModel <em>Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Model</em>' reference.
	 * @see #getResourceModel()
	 * @generated
	 */
	void setResourceModel(ResourceModel value);

	/**
	 * Returns the value of the '<em><b>Sequences</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.scheduler.schedule.Sequence}<code>&lt;T&gt;</code>.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sequences</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequences</em>' containment reference list.
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getSchedule_Sequences()
	 * @see org.eclipse.lsat.common.scheduler.schedule.Sequence#getSchedule
	 * @model opposite="schedule" containment="true"
	 * @generated
	 */
	EList<Sequence<T>> getSequences();

	/**
	 * Returns the value of the '<em><b>Epoch Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Epoch Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Epoch Time</em>' attribute.
	 * @see #setEpochTime(boolean)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getSchedule_EpochTime()
	 * @model required="true"
	 * @generated
	 */
	boolean isEpochTime();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.Schedule#isEpochTime <em>Epoch Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Epoch Time</em>' attribute.
	 * @see #isEpochTime()
	 * @generated
	 */
	void setEpochTime(boolean value);

} // Schedule
