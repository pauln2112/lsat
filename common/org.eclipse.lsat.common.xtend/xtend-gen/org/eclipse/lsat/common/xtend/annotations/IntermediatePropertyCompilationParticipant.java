/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend.annotations;

import com.google.common.collect.BiMap;
import java.util.Map;
import org.eclipse.lsat.common.xtend.annotations.IntermediateProperty;
import org.eclipse.lsat.common.xtend.annotations.TypeReferenceUtil;
import org.eclipse.xtend.lib.macro.AbstractFieldProcessor;
import org.eclipse.xtend.lib.macro.TransformationContext;
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference;
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableMethodDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration;
import org.eclipse.xtend.lib.macro.declaration.TypeReference;
import org.eclipse.xtend.lib.macro.declaration.Visibility;
import org.eclipse.xtend.lib.macro.expression.Expression;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtend2.lib.StringConcatenationClient;
import org.eclipse.xtext.xbase.lib.ArrayExtensions;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class IntermediatePropertyCompilationParticipant extends AbstractFieldProcessor {
  @Override
  public void doTransform(final MutableFieldDeclaration field, @Extension final TransformationContext context) {
    final AnnotationReference _annotation = field.findAnnotation(context.findTypeGlobally(IntermediateProperty.class));
    final TypeReference[] ownerTypes = _annotation.getClassArrayValue("value");
    final String oppositePropertyName = _annotation.getStringValue("opposite");
    final int expectedSize = _annotation.getIntValue("expectedSize");
    final TypeReference ownerCommonType = IterableExtensions.<TypeReference>head(TypeReferenceUtil.getCommonBaseTypes(((Iterable<TypeReference>)Conversions.doWrapArray(ownerTypes))));
    field.markAsRead();
    boolean _isInferred = field.getType().isInferred();
    if (_isInferred) {
      context.addError(field, "Type inference is not supported by @IntermediateProperty");
      return;
    } else {
      boolean _isPrimitive = field.getType().isPrimitive();
      if (_isPrimitive) {
        context.addError(field, "Fields with primitives are not supported by @IntermediateProperty");
        return;
      }
    }
    boolean _isStatic = field.isStatic();
    if (_isStatic) {
      final AnnotationReference swAnn = field.findAnnotation(context.findTypeGlobally(SuppressWarnings.class));
      if (((swAnn == null) || (!ArrayExtensions.contains(swAnn.getStringArrayValue("value"), "static")))) {
        context.addWarning(field, "Note that this property will be statically available");
      }
    }
    if (((!StringExtensions.isNullOrEmpty(oppositePropertyName)) && (!ArrayExtensions.contains(ownerTypes, ownerCommonType)))) {
      StringConcatenation _builder = new StringConcatenation();
      TypeReference _type = field.getType();
      _builder.append(_type);
      _builder.append(".");
      _builder.append(oppositePropertyName);
      _builder.append(" property type is reduced to ");
      String _name = ownerCommonType.getName();
      _builder.append(_name);
      context.addWarning(field, _builder.toString());
    }
    final TypeReference propertyType = field.getType();
    final String propertyName = field.getSimpleName();
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append("_IntermediateProperty_");
    _builder_1.append(propertyName);
    final String propertyMap = _builder_1.toString();
    MutableFieldDeclaration _xifexpression = null;
    Expression _initializer = field.getInitializer();
    boolean _tripleNotEquals = (_initializer != null);
    if (_tripleNotEquals) {
      MutableTypeDeclaration _declaringType = field.getDeclaringType();
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("_DEFAULT");
      {
        for(final TypeReference type : ownerTypes) {
          _builder_2.append("_");
          String _upperCase = type.getSimpleName().toUpperCase();
          _builder_2.append(_upperCase);
        }
      }
      _builder_2.append("_");
      String _upperCase_1 = propertyName.toUpperCase();
      _builder_2.append(_upperCase_1);
      final Procedure1<MutableFieldDeclaration> _function = (MutableFieldDeclaration it) -> {
        it.setVisibility(Visibility.PRIVATE);
        it.setStatic(true);
        it.setFinal(true);
        it.setType(field.getType());
        it.setInitializer(field.getInitializer());
      };
      _xifexpression = _declaringType.addField(_builder_2.toString(), _function);
    }
    final MutableFieldDeclaration propertyDefault = _xifexpression;
    for (final TypeReference ownerType : ownerTypes) {
      {
        MutableTypeDeclaration _declaringType_1 = field.getDeclaringType();
        String _firstUpper = StringExtensions.toFirstUpper(propertyName);
        String _plus = ("get" + _firstUpper);
        final Procedure1<MutableMethodDeclaration> _function_1 = (MutableMethodDeclaration it) -> {
          it.setVisibility(field.getVisibility());
          it.setStatic(field.isStatic());
          it.addParameter("owner", ownerType);
          it.setReturnType(propertyType);
          StringConcatenationClient _xifexpression_1 = null;
          if ((propertyDefault == null)) {
            StringConcatenationClient _client = new StringConcatenationClient() {
              @Override
              protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                _builder.append("return ");
                _builder.append(propertyMap);
                _builder.append(".get(owner);");
                _builder.newLineIfNotEmpty();
              }
            };
            _xifexpression_1 = _client;
          } else {
            StringConcatenationClient _client_1 = new StringConcatenationClient() {
              @Override
              protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                _builder.append(propertyType);
                _builder.append(" value = ");
                _builder.append(propertyMap);
                _builder.append(".get(owner);");
                _builder.newLineIfNotEmpty();
                _builder.append("return value == null ? ");
                String _simpleName = propertyDefault.getSimpleName();
                _builder.append(_simpleName);
                _builder.append(" : value;");
                _builder.newLineIfNotEmpty();
              }
            };
            _xifexpression_1 = _client_1;
          }
          it.setBody(_xifexpression_1);
          context.setPrimarySourceElement(it, field);
        };
        _declaringType_1.addMethod(_plus, _function_1);
        MutableTypeDeclaration _declaringType_2 = field.getDeclaringType();
        String _firstUpper_1 = StringExtensions.toFirstUpper(propertyName);
        String _plus_1 = ("set" + _firstUpper_1);
        final Procedure1<MutableMethodDeclaration> _function_2 = (MutableMethodDeclaration it) -> {
          it.setVisibility(field.getVisibility());
          it.setStatic(field.isStatic());
          it.addParameter("owner", ownerType);
          it.addParameter("value", propertyType);
          StringConcatenationClient _client = new StringConcatenationClient() {
            @Override
            protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
              {
                if ((propertyDefault == null)) {
                  _builder.append("if (value == null) {");
                  _builder.newLine();
                } else {
                  _builder.append("if (value == ");
                  String _simpleName = propertyDefault.getSimpleName();
                  _builder.append(_simpleName);
                  _builder.append(") {");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("    ");
              _builder.append(propertyMap, "    ");
              _builder.append(".remove(owner);");
              _builder.newLineIfNotEmpty();
              _builder.append("} else {");
              _builder.newLine();
              _builder.append("    ");
              _builder.append(propertyMap, "    ");
              _builder.append(".put(owner, value);");
              _builder.newLineIfNotEmpty();
              _builder.append("}");
              _builder.newLine();
            }
          };
          it.setBody(_client);
          context.setPrimarySourceElement(it, field);
        };
        _declaringType_2.addMethod(_plus_1, _function_2);
      }
    }
    boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(oppositePropertyName);
    boolean _not = (!_isNullOrEmpty);
    if (_not) {
      MutableTypeDeclaration _declaringType_1 = field.getDeclaringType();
      String _firstUpper = StringExtensions.toFirstUpper(oppositePropertyName);
      String _plus = ("get" + _firstUpper);
      final Procedure1<MutableMethodDeclaration> _function_1 = (MutableMethodDeclaration it) -> {
        it.setVisibility(field.getVisibility());
        it.setStatic(field.isStatic());
        it.addParameter("owner", propertyType);
        it.setReturnType(ownerCommonType);
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("return ");
            _builder.append(propertyMap);
            _builder.append(".inverse().get(owner);");
            _builder.newLineIfNotEmpty();
          }
        };
        it.setBody(_client);
        context.setPrimarySourceElement(it, field);
      };
      _declaringType_1.addMethod(_plus, _function_1);
      MutableTypeDeclaration _declaringType_2 = field.getDeclaringType();
      String _firstUpper_1 = StringExtensions.toFirstUpper(oppositePropertyName);
      String _plus_1 = ("set" + _firstUpper_1);
      final Procedure1<MutableMethodDeclaration> _function_2 = (MutableMethodDeclaration it) -> {
        it.setVisibility(field.getVisibility());
        it.setStatic(field.isStatic());
        it.addParameter("owner", propertyType);
        it.addParameter("value", ownerCommonType);
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("if (value == null) {");
            _builder.newLine();
            _builder.append("    ");
            _builder.append(propertyMap, "    ");
            _builder.append(".inverse().remove(owner);");
            _builder.newLineIfNotEmpty();
            _builder.append("} else {");
            _builder.newLine();
            _builder.append("    ");
            _builder.append(propertyMap, "    ");
            _builder.append(".inverse().put(owner, value);");
            _builder.newLineIfNotEmpty();
            _builder.append("}");
            _builder.newLine();
          }
        };
        it.setBody(_client);
        context.setPrimarySourceElement(it, field);
      };
      _declaringType_2.addMethod(_plus_1, _function_2);
    }
    MutableTypeDeclaration _declaringType_3 = field.getDeclaringType();
    String _firstUpper_2 = StringExtensions.toFirstUpper(propertyName);
    String _plus_2 = ("dispose" + _firstUpper_2);
    final Procedure1<MutableMethodDeclaration> _function_3 = (MutableMethodDeclaration it) -> {
      it.setVisibility(Visibility.PRIVATE);
      it.setStatic(field.isStatic());
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append(propertyMap);
          _builder.append(".clear();");
          _builder.newLineIfNotEmpty();
        }
      };
      it.setBody(_client);
      context.setPrimarySourceElement(it, field);
    };
    _declaringType_3.addMethod(_plus_2, _function_3);
    field.markAsRead();
    field.setSimpleName(propertyMap);
    String _xifexpression_1 = null;
    if ((expectedSize > 0)) {
      _xifexpression_1 = Integer.valueOf(expectedSize).toString();
    } else {
      _xifexpression_1 = "";
    }
    final String sizeStr = _xifexpression_1;
    boolean _isNullOrEmpty_1 = StringExtensions.isNullOrEmpty(oppositePropertyName);
    boolean _not_1 = (!_isNullOrEmpty_1);
    if (_not_1) {
      field.setType(context.newTypeReference(BiMap.class, ownerCommonType, field.getType()));
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("com.google.common.collect.HashBiMap.create(");
          _builder.append(sizeStr);
          _builder.append(")");
        }
      };
      field.setInitializer(_client);
    } else {
      field.setType(context.newTypeReference(Map.class, ownerCommonType, field.getType()));
      StringConcatenationClient _client_1 = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("new java.util.WeakHashMap<>(");
          _builder.append(sizeStr);
          _builder.append(")");
        }
      };
      field.setInitializer(_client_1);
    }
  }
}
