/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json.test;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.lsat.motioncalculator.MotionCalculator;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileProvider;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.motioncalculator.PositionInfo;

public final class ErrorMotionCalculator implements MotionCalculator, MotionProfileProvider {
    @Override
    public Set<MotionProfile> getSupportedProfiles() throws MotionException {
        throw new MotionException("error");
    }

    @Override
    public void validate(List<MotionSegment> segments) throws MotionValidationException {
        throw new MotionValidationException("error", segments);
    }

    @Override
    public List<Double> calculateTimes(List<MotionSegment> segments) throws MotionException {
        throw new MotionValidationException("error", segments);
    }

    @Override
    public Collection<PositionInfo> getPositionInfo(List<MotionSegment> segments) throws MotionException {
        throw new MotionValidationException("error", segments);
    }
}
