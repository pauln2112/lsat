/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json.test;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileParameter;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionSetPoint;
import org.eclipse.lsat.motioncalculator.PositionInfo;
import org.eclipse.lsat.motioncalculator.json.JsonRequest;
import org.eclipse.lsat.motioncalculator.json.JsonRequestType;
import org.eclipse.lsat.motioncalculator.json.JsonResponse;

class MotionDataFactory {
    private MotionDataFactory() {
        // Empty
    }

    public static final MotionProfile createMotionProfile() {
        Collection<MotionProfileParameter> parameters = new ArrayList<>();
        parameters.add(new MotionProfileParameter("aKey", "aParName", true));
        parameters.add(new MotionProfileParameter("rKey", "rParName", false));
        try {
            MotionProfile motionProfile = new MotionProfile("aProfile", "aName", new URL("https://aUrl"), parameters);
            motionProfile.setDefaultProfile(true);
            return motionProfile;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static final MotionSetPoint createMotionSetPoint(String name) {
        MotionSetPoint sp = new MotionSetPoint(name);
        sp.setDistance(BigDecimal.valueOf(1.0));
        sp.setFrom(BigDecimal.valueOf(2.0));
        sp.setMotionProfile(createMotionProfile());
        sp.setSettling(true);
        return sp;
    }

    public static final MotionSegment createMotionSegment(String name, String... axes) {
        MotionSegment s = new MotionSegment(name);
        for (String axis: axes) {
            s.addMotionSetpoint(createMotionSetPoint(axis));
        }
        return s;
    }

    public static final JsonRequest createRequestWithSegments(JsonRequestType type, String... segmentNames) {
        if (segmentNames.length == 0) {
            segmentNames = new String[] {"a"};
        }
        return new JsonRequest(type,
                Stream.of(segmentNames).map(n -> createMotionSegment(n, "x", "y")).collect(Collectors.toList()));
    }

    public static final Collection<PositionInfo> createPositionInfo(int number, String... setPoints) {
        if (setPoints.length == 0) {
            setPoints = new String[] {"a"};
        }
        List<PositionInfo> result = new ArrayList<>();
        for (String sp: setPoints) {
            PositionInfo positionInfo = new PositionInfo(sp);
            Double.valueOf(1.0 * number);
            IntStream.range(0, number).asDoubleStream().boxed().forEach(d -> positionInfo.addTimePosition(d, d));
            result.add(positionInfo);
        }
        return result;
    }

    public static final JsonResponse createResponse(JsonRequestType type, String... segmentNames) {
        if (segmentNames.length == 0) {
            segmentNames = new String[] {"a"};
        }
        JsonResponse response = new JsonResponse();
        response.setRequestType(type);
        response.setTimes(Arrays.asList(1.0, 2.00, 3.000));
        response.setMotionProfiles(Collections.singleton(createMotionProfile()));
        response.setErrorMessage("anErrorMessage");
        response.setErrorSegments(Arrays.asList("a", "b").stream().collect(Collectors.toSet()));
        response.setPositions(createPositionInfo(3, "x", "y"));

        return response;
    }
}
