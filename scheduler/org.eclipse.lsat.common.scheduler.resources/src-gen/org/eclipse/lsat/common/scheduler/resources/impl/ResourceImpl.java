/**
 */
package org.eclipse.lsat.common.scheduler.resources.impl;

import org.eclipse.lsat.common.scheduler.resources.Resource;
import org.eclipse.lsat.common.scheduler.resources.ResourcesPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ResourceImpl extends AbstractResourceImpl implements Resource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResourcesPackage.Literals.RESOURCE;
	}

} //ResourceImpl
