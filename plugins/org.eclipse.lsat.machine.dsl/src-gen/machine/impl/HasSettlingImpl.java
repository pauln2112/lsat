/**
 */
package machine.impl;

import java.util.Collection;

import machine.Axis;
import machine.HasSettling;
import machine.MachinePackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Has Settling</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.HasSettlingImpl#getSettling <em>Settling</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class HasSettlingImpl extends MinimalEObjectImpl.Container implements HasSettling {
	/**
     * The cached value of the '{@link #getSettling() <em>Settling</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSettling()
     * @generated
     * @ordered
     */
	protected EList<Axis> settling;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected HasSettlingImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.HAS_SETTLING;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Axis> getSettling() {
        if (settling == null)
        {
            settling = new EObjectResolvingEList<Axis>(Axis.class, this, MachinePackage.HAS_SETTLING__SETTLING);
        }
        return settling;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.HAS_SETTLING__SETTLING:
                return getSettling();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.HAS_SETTLING__SETTLING:
                getSettling().clear();
                getSettling().addAll((Collection<? extends Axis>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.HAS_SETTLING__SETTLING:
                getSettling().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.HAS_SETTLING__SETTLING:
                return settling != null && !settling.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //HasSettlingImpl
