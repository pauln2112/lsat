/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */

package activity.util;

import static activity.util.ActivityUtil.addClaim;
import static activity.util.ActivityUtil.addEdge;
import static activity.util.ActivityUtil.addRelease;
import static activity.util.ActivityUtil.delete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.lsat.common.graph.directed.editable.Edge;

import activity.Activity;
import activity.ActivitySet;
import activity.Claim;
import activity.EventAction;
import activity.RaiseEvent;
import activity.Release;
import activity.RequireEvent;

public class Event2Resource {
    private Event2Resource() {
        // Empty
    }

    /**
     * Converts events in {@link Activity}s to resources. The resources are added to the first machine that is found.
     * Around each {@link RequireEvent} or {@link RaiseEvent} a claim and release are inserted. The event itself is
     * removed.
     */
    public static void replaceEventsWithClaimRelease(ActivitySet activitySet) {
        insertClaimReleaseForEvents(activitySet, false);
    }

    /**
     * Convert event in {@link Activity}s to resources. The resources are added to the first machine that is found.
     * Arround each {@link RequireEvent} or {@link RaiseEvent} a claim and release are inserted. The event itself is
     * kept.
     */
    public static void surroundEventsWithClaimRelease(ActivitySet activitySet) {
        insertClaimReleaseForEvents(activitySet, true);
    }

    /**
     * Convert event in {@link Activity}s to resources. The resources are added to the first machine that is found. On
     * each {@link RequireEvent} or {@link RaiseEvent} a claim and release are inserted.
     */
    private static void insertClaimReleaseForEvents(ActivitySet activitySet, boolean keepEvents) {
        getEvents(activitySet).forEach(event -> {
            insertClaimReleaseForEvents(event, keepEvents);
        });
    }

    private static Collection<EventAction> getEvents(ActivitySet activitySet) {
        return activitySet.getActivities().stream().flatMap(a -> a.getNodes().stream()).filter(EventAction.class::isInstance)
                .map(EventAction.class::cast).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private static void insertClaimReleaseForEvents(EventAction eventAction, boolean keepEvent) {
        Activity activity = (Activity)eventAction.eContainer();
        List<EventAction> eventActions = activity.allNodesInTopologicalOrder().stream()
                .filter(EventAction.class::isInstance)
                .map(EventAction.class::cast)
                .filter(e -> e.getResource().fqn().equals(eventAction.getResource().fqn())).collect(Collectors.toList());
        if (eventActions.indexOf(eventAction) == 0) { //claim before first
            insertClaim(eventAction);
        }
        if (eventActions.indexOf(eventAction) == eventActions.size() - 1) {  //release after last
            insertRelease(eventAction);
        }
        if (!keepEvent) {
            shortcutAndRemoveNode(eventAction);
        }
    }

    private static void insertClaim(EventAction eventAction) {
        Activity activity = (Activity)eventAction.eContainer();
        Claim cl = addClaim(activity, eventAction.getResource());
        eventAction.getIncomingEdges().forEach(e -> addEdge(activity, e.getSourceNode(), cl));
        // delete the original edges
        new ArrayList<>(eventAction.getIncomingEdges()).forEach(e -> delete(e));
        addEdge(activity, cl, eventAction);
    }

    private static void insertRelease(EventAction eventAction) {
        Activity activity = (Activity)eventAction.eContainer();
        Release rl = addRelease(activity, eventAction.getResource());
        eventAction.getOutgoingEdges().forEach(e -> addEdge(activity, rl, e.getTargetNode()));
        // delete the original edges
        new ArrayList<>(eventAction.getOutgoingEdges()).forEach(e -> delete(e));
        addEdge(activity, eventAction, rl);
    }

    private static void shortcutAndRemoveNode(EventAction node) {
        Activity activity = (Activity)node.eContainer();
        for (Edge in: node.getIncomingEdges()) {
            for (Edge out: node.getOutgoingEdges()) {
                addEdge(activity, in.getSourceNode(), out.getTargetNode());
            }
        }
        new ArrayList<>(node.getIncomingEdges()).forEach(e -> delete(e));
        new ArrayList<>(node.getOutgoingEdges()).forEach(e -> delete(e));
        delete(node);
    }
}
