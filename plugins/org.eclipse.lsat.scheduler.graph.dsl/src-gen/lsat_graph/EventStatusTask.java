/**
 */
package lsat_graph;

import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EventAction Status Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lsat_graph.lsat_graphPackage#getEventStatusTask()
 * @model
 * @generated
 */
public interface EventStatusTask extends ScheduledTask<Task> {
} // EventStatusTask
