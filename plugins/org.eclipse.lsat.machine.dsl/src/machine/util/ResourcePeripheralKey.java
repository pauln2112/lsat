/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package machine.util;

import machine.HasResourcePeripheral;
import machine.IResource;
import machine.Peripheral;

public class ResourcePeripheralKey {
    private final IResource resource;

    private final Peripheral peripheral;

    public ResourcePeripheralKey(HasResourcePeripheral target) {
        this.resource = target.getResource();
        this.peripheral = target.getPeripheral();
    }

    public ResourcePeripheralKey(IResource resource, Peripheral peripheral) {
        this.resource = resource;
        this.peripheral = peripheral;
    }

    public static ResourcePeripheralKey createKey(HasResourcePeripheral target) {
        return new ResourcePeripheralKey(target);
    }

    public IResource getResource() {
        return resource;
    }

    public Peripheral getPeripheral() {
        return peripheral;
    }

    public String fqn() {
        return resource.fqn() + "." + peripheral.getName();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((peripheral == null) ? 0 : peripheral.hashCode());
        result = prime * result + ((resource == null) ? 0 : resource.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ResourcePeripheralKey other = (ResourcePeripheralKey)obj;
        if (peripheral == null) {
            if (other.peripheral != null)
                return false;
        } else if (!peripheral.equals(other.peripheral))
            return false;
        if (resource == null) {
            if (other.resource != null)
                return false;
        } else if (!resource.equals(other.resource))
            return false;
        return true;
    }
}
