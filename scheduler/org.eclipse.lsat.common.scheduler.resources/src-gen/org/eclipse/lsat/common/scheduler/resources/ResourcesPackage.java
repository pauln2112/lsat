/**
 */
package org.eclipse.lsat.common.scheduler.resources;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesFactory
 * @model kind="package"
 * @generated
 */
public interface ResourcesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "resources";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/lsat/scheduler/resources";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "resources";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ResourcesPackage eINSTANCE = org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.NamedResourceImpl <em>Named Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.NamedResourceImpl
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getNamedResource()
	 * @generated
	 */
	int NAMED_RESOURCE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_RESOURCE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_RESOURCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_RESOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.ResourceContainerImpl <em>Resource Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourceContainerImpl
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getResourceContainer()
	 * @generated
	 */
	int RESOURCE_CONTAINER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER__NAME = NAMED_RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER__RESOURCES = NAMED_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER__OFFSET = NAMED_RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Resource Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER_FEATURE_COUNT = NAMED_RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Resource Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_CONTAINER_OPERATION_COUNT = NAMED_RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.AbstractResourceImpl <em>Abstract Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.AbstractResourceImpl
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getAbstractResource()
	 * @generated
	 */
	int ABSTRACT_RESOURCE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_RESOURCE__NAME = NAMED_RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_RESOURCE__CONTAINER = NAMED_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_RESOURCE__START = NAMED_RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Abstract Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_RESOURCE_FEATURE_COUNT = NAMED_RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Abstract Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_RESOURCE_OPERATION_COUNT = NAMED_RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.ResourceModelImpl <em>Resource Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourceModelImpl
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getResourceModel()
	 * @generated
	 */
	int RESOURCE_MODEL = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_MODEL__NAME = RESOURCE_CONTAINER__NAME;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_MODEL__RESOURCES = RESOURCE_CONTAINER__RESOURCES;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_MODEL__OFFSET = RESOURCE_CONTAINER__OFFSET;

	/**
	 * The number of structural features of the '<em>Resource Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_MODEL_FEATURE_COUNT = RESOURCE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Resource Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_MODEL_OPERATION_COUNT = RESOURCE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.ResourceSetImpl <em>Resource Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourceSetImpl
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getResourceSet()
	 * @generated
	 */
	int RESOURCE_SET = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_SET__NAME = RESOURCE_CONTAINER__NAME;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_SET__RESOURCES = RESOURCE_CONTAINER__RESOURCES;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_SET__OFFSET = RESOURCE_CONTAINER__OFFSET;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_SET__CONTAINER = RESOURCE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_SET__START = RESOURCE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Resource Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_SET_FEATURE_COUNT = RESOURCE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Resource Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_SET_OPERATION_COUNT = RESOURCE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.ResourceImpl <em>Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourceImpl
	 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getResource()
	 * @generated
	 */
	int RESOURCE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__NAME = ABSTRACT_RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__CONTAINER = ABSTRACT_RESOURCE__CONTAINER;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__START = ABSTRACT_RESOURCE__START;

	/**
	 * The number of structural features of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FEATURE_COUNT = ABSTRACT_RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_OPERATION_COUNT = ABSTRACT_RESOURCE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.resources.NamedResource <em>Named Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Resource</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.NamedResource
	 * @generated
	 */
	EClass getNamedResource();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.resources.NamedResource#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.NamedResource#getName()
	 * @see #getNamedResource()
	 * @generated
	 */
	EAttribute getNamedResource_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.resources.ResourceContainer <em>Resource Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Container</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourceContainer
	 * @generated
	 */
	EClass getResourceContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.scheduler.resources.ResourceContainer#getResources <em>Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resources</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourceContainer#getResources()
	 * @see #getResourceContainer()
	 * @generated
	 */
	EReference getResourceContainer_Resources();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.resources.ResourceContainer#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourceContainer#getOffset()
	 * @see #getResourceContainer()
	 * @generated
	 */
	EAttribute getResourceContainer_Offset();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.resources.AbstractResource <em>Abstract Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Resource</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.AbstractResource
	 * @generated
	 */
	EClass getAbstractResource();

	/**
	 * Returns the meta object for the container reference '{@link org.eclipse.lsat.common.scheduler.resources.AbstractResource#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.AbstractResource#getContainer()
	 * @see #getAbstractResource()
	 * @generated
	 */
	EReference getAbstractResource_Container();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.resources.AbstractResource#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.AbstractResource#getStart()
	 * @see #getAbstractResource()
	 * @generated
	 */
	EAttribute getAbstractResource_Start();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.resources.ResourceModel <em>Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Model</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourceModel
	 * @generated
	 */
	EClass getResourceModel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.resources.ResourceSet <em>Resource Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Set</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourceSet
	 * @generated
	 */
	EClass getResourceSet();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.resources.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource</em>'.
	 * @see org.eclipse.lsat.common.scheduler.resources.Resource
	 * @generated
	 */
	EClass getResource();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ResourcesFactory getResourcesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.NamedResourceImpl <em>Named Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.NamedResourceImpl
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getNamedResource()
		 * @generated
		 */
		EClass NAMED_RESOURCE = eINSTANCE.getNamedResource();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_RESOURCE__NAME = eINSTANCE.getNamedResource_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.ResourceContainerImpl <em>Resource Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourceContainerImpl
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getResourceContainer()
		 * @generated
		 */
		EClass RESOURCE_CONTAINER = eINSTANCE.getResourceContainer();

		/**
		 * The meta object literal for the '<em><b>Resources</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_CONTAINER__RESOURCES = eINSTANCE.getResourceContainer_Resources();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_CONTAINER__OFFSET = eINSTANCE.getResourceContainer_Offset();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.AbstractResourceImpl <em>Abstract Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.AbstractResourceImpl
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getAbstractResource()
		 * @generated
		 */
		EClass ABSTRACT_RESOURCE = eINSTANCE.getAbstractResource();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_RESOURCE__CONTAINER = eINSTANCE.getAbstractResource_Container();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_RESOURCE__START = eINSTANCE.getAbstractResource_Start();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.ResourceModelImpl <em>Resource Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourceModelImpl
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getResourceModel()
		 * @generated
		 */
		EClass RESOURCE_MODEL = eINSTANCE.getResourceModel();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.ResourceSetImpl <em>Resource Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourceSetImpl
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getResourceSet()
		 * @generated
		 */
		EClass RESOURCE_SET = eINSTANCE.getResourceSet();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.resources.impl.ResourceImpl <em>Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourceImpl
		 * @see org.eclipse.lsat.common.scheduler.resources.impl.ResourcesPackageImpl#getResource()
		 * @generated
		 */
		EClass RESOURCE = eINSTANCE.getResource();

	}

} //ResourcesPackage
