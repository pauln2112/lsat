/**
 */
package lsat_graph.impl;

import org.eclipse.lsat.common.scheduler.graph.impl.TaskImpl;

import dispatching.DispatchGroup;
import lsat_graph.DispatchGroupTask;
import lsat_graph.lsat_graphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dispatch Group Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.impl.DispatchGroupTaskImpl#getDispatchGroup <em>Dispatch Group</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DispatchGroupTaskImpl extends TaskImpl implements DispatchGroupTask {
	/**
     * The cached value of the '{@link #getDispatchGroup() <em>Dispatch Group</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDispatchGroup()
     * @generated
     * @ordered
     */
	protected DispatchGroup dispatchGroup;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected DispatchGroupTaskImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.DISPATCH_GROUP_TASK;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public DispatchGroup getDispatchGroup() {
        if (dispatchGroup != null && dispatchGroup.eIsProxy())
        {
            InternalEObject oldDispatchGroup = (InternalEObject)dispatchGroup;
            dispatchGroup = (DispatchGroup)eResolveProxy(oldDispatchGroup);
            if (dispatchGroup != oldDispatchGroup)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, lsat_graphPackage.DISPATCH_GROUP_TASK__DISPATCH_GROUP, oldDispatchGroup, dispatchGroup));
            }
        }
        return dispatchGroup;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public DispatchGroup basicGetDispatchGroup() {
        return dispatchGroup;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setDispatchGroup(DispatchGroup newDispatchGroup) {
        DispatchGroup oldDispatchGroup = dispatchGroup;
        dispatchGroup = newDispatchGroup;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, lsat_graphPackage.DISPATCH_GROUP_TASK__DISPATCH_GROUP, oldDispatchGroup, dispatchGroup));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case lsat_graphPackage.DISPATCH_GROUP_TASK__DISPATCH_GROUP:
                if (resolve) return getDispatchGroup();
                return basicGetDispatchGroup();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case lsat_graphPackage.DISPATCH_GROUP_TASK__DISPATCH_GROUP:
                setDispatchGroup((DispatchGroup)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.DISPATCH_GROUP_TASK__DISPATCH_GROUP:
                setDispatchGroup((DispatchGroup)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.DISPATCH_GROUP_TASK__DISPATCH_GROUP:
                return dispatchGroup != null;
        }
        return super.eIsSet(featureID);
    }

} //DispatchGroupTaskImpl
