/**
 */
package expressions.util;

import expressions.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see expressions.ExpressionsPackage
 * @generated
 */
public class ExpressionsSwitch<T> extends Switch<T> {
	/**
     * The cached model package
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static ExpressionsPackage modelPackage;

	/**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ExpressionsSwitch() {
        if (modelPackage == null)
        {
            modelPackage = ExpressionsPackage.eINSTANCE;
        }
    }

	/**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

	/**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID)
        {
            case ExpressionsPackage.ABSTRACT_ELEMENT:
            {
                AbstractElement abstractElement = (AbstractElement)theEObject;
                T result = caseAbstractElement(abstractElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ExpressionsPackage.DECLARATION:
            {
                Declaration declaration = (Declaration)theEObject;
                T result = caseDeclaration(declaration);
                if (result == null) result = caseAbstractElement(declaration);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ExpressionsPackage.EXPRESSION:
            {
                Expression expression = (Expression)theEObject;
                T result = caseExpression(expression);
                if (result == null) result = caseAbstractElement(expression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ExpressionsPackage.ADD:
            {
                Add add = (Add)theEObject;
                T result = caseAdd(add);
                if (result == null) result = caseBinaryExpression(add);
                if (result == null) result = caseExpression(add);
                if (result == null) result = caseAbstractElement(add);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ExpressionsPackage.SUBTRACT:
            {
                Subtract subtract = (Subtract)theEObject;
                T result = caseSubtract(subtract);
                if (result == null) result = caseBinaryExpression(subtract);
                if (result == null) result = caseExpression(subtract);
                if (result == null) result = caseAbstractElement(subtract);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ExpressionsPackage.MULTIPLY:
            {
                Multiply multiply = (Multiply)theEObject;
                T result = caseMultiply(multiply);
                if (result == null) result = caseBinaryExpression(multiply);
                if (result == null) result = caseExpression(multiply);
                if (result == null) result = caseAbstractElement(multiply);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ExpressionsPackage.BIG_DECIMAL_CONSTANT:
            {
                BigDecimalConstant bigDecimalConstant = (BigDecimalConstant)theEObject;
                T result = caseBigDecimalConstant(bigDecimalConstant);
                if (result == null) result = caseExpression(bigDecimalConstant);
                if (result == null) result = caseAbstractElement(bigDecimalConstant);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ExpressionsPackage.DECLARATION_REF:
            {
                DeclarationRef declarationRef = (DeclarationRef)theEObject;
                T result = caseDeclarationRef(declarationRef);
                if (result == null) result = caseExpression(declarationRef);
                if (result == null) result = caseAbstractElement(declarationRef);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ExpressionsPackage.DIVIDE:
            {
                Divide divide = (Divide)theEObject;
                T result = caseDivide(divide);
                if (result == null) result = caseBinaryExpression(divide);
                if (result == null) result = caseExpression(divide);
                if (result == null) result = caseAbstractElement(divide);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ExpressionsPackage.BINARY_EXPRESSION:
            {
                BinaryExpression binaryExpression = (BinaryExpression)theEObject;
                T result = caseBinaryExpression(binaryExpression);
                if (result == null) result = caseExpression(binaryExpression);
                if (result == null) result = caseAbstractElement(binaryExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Abstract Element</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseAbstractElement(AbstractElement object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Declaration</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Declaration</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseDeclaration(Declaration object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseExpression(Expression object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Add</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Add</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseAdd(Add object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Subtract</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Subtract</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseSubtract(Subtract object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Multiply</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Multiply</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseMultiply(Multiply object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Big Decimal Constant</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Big Decimal Constant</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseBigDecimalConstant(BigDecimalConstant object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Declaration Ref</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Declaration Ref</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseDeclarationRef(DeclarationRef object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Divide</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Divide</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseDivide(Divide object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Binary Expression</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Binary Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseBinaryExpression(BinaryExpression object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
	@Override
	public T defaultCase(EObject object) {
        return null;
    }

} //ExpressionsSwitch
