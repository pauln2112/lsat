////
  // Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
  //
  // This program and the accompanying materials are made
  // available under the terms of the Eclipse Public License 2.0
  // which is available at https://www.eclipse.org/legal/epl-2.0/
  //
  // SPDX-License-Identifier: EPL-2.0
////

[[plugins]]
== Overview of Plugins

This section presents the overview of plugins in {lsat}.

=== Machine

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.machine.dsl| Contains the metamodel of machine.
|org.eclipse.lsat.machine.dsl.edit| Contains generic reusable classes for building editors for EMF models of machine.
|org.eclipse.lsat.machine.teditor| Contains the Xtext grammar definition of machine and all related components such as parser, lexer, linker. validation, etc.
|org.eclipse.lsat.machine.teditor.ide|Contains the Xtext language-specific text editor.
|org.eclipse.lsat.machine.teditor.ui|Contains advanced functionalities such as content assist, outline tree and quick fix.
|org.eclipse.lsat.machine.design| Contains Sirius modeling projects and representations.
|======================

=== Settings

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.setting.dsl| Contains the metamodel of settings.
|org.eclipse.lsat.setting.teditor| Contains the Xtext grammar definition of settings and all related components such as parser, lexer, linker. validation, etc.
|org.eclipse.lsat.setting.teditor.ide|Contains the Xtext language-specific text editor.
|org.eclipse.lsat.setting.teditor.ui|Contains advanced functionalities such as content assist, outline tree and quick fix.
|org.eclipse.lsat.setting.design| Contains Sirius modeling projects and representations.
|======================

=== Activities

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.activity.dsl| Contains the metamodel of activities.
|org.eclipse.lsat.activity.teditor|Contains the Xtext grammar definition of activities and all related components such as parser, lexer, linker. validation, etc.
|org.eclipse.lsat.activity.teditor.ide|Contains the Xtext language-specific text editor.
|org.eclipse.lsat.activity.teditor.ui|Contains advanced functionalities such as content assist, outline tree and quick fix.
|org.eclipse.lsat.activity.diagram.design| Contains Sirius modeling projects and representations.
|org.eclipse.lsat.activity.diagram.layout| Contains Eclipse Layout Kernel (ELK) definitions and algorithms.
|======================

=== Logistics

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.dispatching.dsl| Contains the metamodel of logistics.
|org.eclipse.lsat.dispatching.teditor|Contains the Xtext grammar definition of activities and all related components such as parser, lexer, linker. validation, etc.
|org.eclipse.lsat.dispatching.teditor.ide|Contains the Xtext language-specific text editor.
|org.eclipse.lsat.dispatching.teditor.ui|Contains advanced functionalities such as content assist, outline tree and quick fix.
|======================

=== Directed Graph

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.common.graph.directed| Contains the metamodel of directed graph.
|org.eclipse.lsat.common.graph.directed.editable| Contains the metamodel of editable directed graph.
|======================

=== MaxPlus

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.common.mpt.dsl| Contains the metamodel of maxplus.
|org.eclipse.lsat.common.mpt.dsl.edit|Contains generic reusable classes for building editors for EMF models of maxplus.
|org.eclipse.lsat.common.mpt.dsl.editor|Contains a maxplus editor.
|org.eclipse.lsat.mpt.api|Contains algorithms for computing optimal makespan/throughput.
|org.eclipse.lsat.mpt.feature|
|org.eclipse.lsat.mpt.ui|Contains ui features for computing optimal makespan/throughput.
|org.eclipse.lsat.mpt.transformation|Contains transformations for computing optimal makespan/throughput explained in Section <<Optimal Makespan/Throughput Scheduling>>.
|======================

=== Scheduling Activities including Gantt Chart Visualization

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.scheduler.graph.dsl| Contains the metamodel of scheduler graph.
|org.eclipse.lsat.scheduler.ui| Contains ui features for scheduling.
|org.eclipse.lsat.scheduler| Contains transformations for scheduling explained in Section <<Scheduling activities including Gantt chart visualization>>.
|org.eclipse.lsat.scheduler.etf| Contains transformations from a schedule to Eclipse TRACE4CPS.
|org.eclipse.lsat.motioncalculator.api| Contains the API for computing timings for different motion types, e.g., point-to-point and distance moves, and motion profiles settings.
|org.eclipse.lsat.motioncalculator.json(.native)| Contains support classes to easily implement a native motion calculator.
|org.eclipse.lsat.timing| Contains algorithms for computing timings for different motion types, e.g., point-to-point and distance moves, and motion profiles settings, e.g., normal distribution, triangular distribution, and Pert distribution.
|org.eclipse.lsat.timinganalysis.ui| Contains ui features for computing timings e.g., playing and exporting  animations.
|======================

=== Conformance Checking

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.petri_net.dsl| Contains the metamodel of Petri Net.
|org.eclipse.lsat.petri_net.design| Contains Sirius modeling projects and representations.
|org.eclipse.lsat.conformance| Contains transformations for conformance checking of traces explained in Section <<Conformance Checking>>. 
|org.eclipse.lsat.conformance.ui| Contains ui functions for conformance checking of traces.
|======================

=== Throughput per Resource

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.resource_throughput.ui| Returns ui functions and algorithms for calculating throughput per resource.
|======================

=== Documentation

[width="100%",frame="topbot",options="header,footer"]
|======================
|Name|Description
|org.eclipse.lsat.documentation| Contains AsciiDoc files of the {lsat} user guide.
|org.eclipse.lsat.design| Contains AsciiDoc files of this {lsat} design documentation.
|org.eclipse.lsat.intro| Contains help files of {lsat}.
|======================