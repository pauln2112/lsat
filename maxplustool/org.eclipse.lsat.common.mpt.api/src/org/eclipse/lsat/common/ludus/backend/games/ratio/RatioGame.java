/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games.ratio;

import org.eclipse.lsat.common.ludus.backend.datastructures.weights.DoubleWeightFunction;
import org.eclipse.lsat.common.ludus.backend.games.GameGraph;

/**
 * Interface to access ratio games. A ratio game consists of a game graph and a function that assigns two weights to
 * each edge in the game.
 *
 * @author Bram van der Sanden
 * @param <V> vertex type
 * @param <E> edge type
 * @param <T> edge weight type
 */
public interface RatioGame<V, E, T> extends GameGraph<V, E>, DoubleWeightFunction<T, E> {

}
