/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import org.eclipse.emf.common.util.URI;

public interface ResourceRenameFilter {
    static final ResourceRenameFilter SAVE_ALL = new ResourceRenameFilter() {
        public URI renameAccept(URI aNormalizedURI) {
            return aNormalizedURI;
        }
    };

    /**
     * @param aNormalizedURI the URI to be accepted and optionally renamed.
     * @return <code>null</code> to filter, the renamed URI or <code>aNormalizedURI</code> to leave it unchanged.
     */
    URI renameAccept(URI aNormalizedURI);
}
