/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Full Mesh Path</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.FullMeshPath#getEndPoints <em>End Points</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getFullMeshPath()
 * @model
 * @generated
 */
public interface FullMeshPath extends Path {
	/**
     * Returns the value of the '<em><b>End Points</b></em>' containment reference list.
     * The list contents are of type {@link machine.PathTargetReference}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Points</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>End Points</em>' containment reference list.
     * @see machine.MachinePackage#getFullMeshPath_EndPoints()
     * @model containment="true" lower="3"
     * @generated
     */
	EList<PathTargetReference> getEndPoints();

} // FullMeshPath
