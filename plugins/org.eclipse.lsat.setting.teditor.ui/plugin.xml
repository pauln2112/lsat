<?xml version="1.0" encoding="UTF-8"?>
<!--

    Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<?eclipse version="3.0"?>

<plugin>

    <extension
            point="org.eclipse.ui.editors">
        <editor
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.XtextEditor"
            contributorClass="org.eclipse.ui.editors.text.TextEditorActionContributor"
            default="true"
            extensions="setting"
            id="org.eclipse.lsat.setting.teditor.Setting"
            name="Setting Editor">
        </editor>
    </extension>
    <extension
        point="org.eclipse.ui.handlers">
        <handler
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclarationHandler"
            commandId="org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclaration">
            <activeWhen>
                <reference
                    definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
        <handler
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.handler.ValidateActionHandler"
            commandId="org.eclipse.lsat.setting.teditor.Setting.validate">
         <activeWhen>
            <reference
                    definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened">
            </reference>
         </activeWhen>
      	</handler>
      	<!-- copy qualified name -->
        <handler
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName">
            <activeWhen>
				<reference definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened" />
            </activeWhen>
        </handler>
        <handler
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedNameHandler"
            commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName">
            <activeWhen>
            	<and>
            		<reference definitionId="org.eclipse.lsat.setting.teditor.Setting.XtextEditor.opened" />
	                <iterate>
						<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
					</iterate>
				</and>
            </activeWhen>
        </handler>
    </extension>
    <extension point="org.eclipse.core.expressions.definitions">
        <definition id="org.eclipse.lsat.setting.teditor.Setting.Editor.opened">
            <and>
                <reference definitionId="isActiveEditorAnInstanceOfXtextEditor"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="org.eclipse.lsat.setting.teditor.Setting" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
        <definition id="org.eclipse.lsat.setting.teditor.Setting.XtextEditor.opened">
            <and>
                <reference definitionId="isXtextEditorActive"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="org.eclipse.lsat.setting.teditor.Setting" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
    </extension>
    <extension
            point="org.eclipse.ui.preferencePages">
        <page
              category="org.eclipse.lsat.preferences"
              class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
              id="org.eclipse.lsat.setting.teditor.Setting"
              name="Setting">
            <keywordReference id="org.eclipse.lsat.setting.teditor.ui.keyword_Setting"/>
        </page>
        <page
            category="org.eclipse.lsat.setting.teditor.Setting"
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.syntaxcoloring.SyntaxColoringPreferencePage"
            id="org.eclipse.lsat.setting.teditor.Setting.coloring"
            name="Syntax Coloring">
            <keywordReference id="org.eclipse.lsat.setting.teditor.ui.keyword_Setting"/>
        </page>
        <page
            category="org.eclipse.lsat.setting.teditor.Setting"
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.templates.XtextTemplatePreferencePage"
            id="org.eclipse.lsat.setting.teditor.Setting.templates"
            name="Templates">
            <keywordReference id="org.eclipse.lsat.setting.teditor.ui.keyword_Setting"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="org.eclipse.lsat.setting.teditor.Setting"
            name="Setting">
            <keywordReference id="org.eclipse.lsat.setting.teditor.ui.keyword_Setting"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>
    <extension
        point="org.eclipse.ui.keywords">
        <keyword
            id="org.eclipse.lsat.setting.teditor.ui.keyword_Setting"
            label="Setting"/>
    </extension>
    <extension
         point="org.eclipse.ui.commands">
      <command
            description="Trigger expensive validation"
            id="org.eclipse.lsat.setting.teditor.Setting.validate"
            name="Validate">
      </command>
      <!-- copy qualified name -->
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
      <command
            id="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName"
            categoryId="org.eclipse.ui.category.edit"
            description="Copy the qualified name for the selected element"
            name="Copy Qualified Name">
      </command>
    </extension>
    <extension point="org.eclipse.ui.menus">
        <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
             <command
                 commandId="org.eclipse.lsat.setting.teditor.Setting.validate"
                 style="push"
                 tooltip="Trigger expensive validation">
            <visibleWhen checkEnabled="false">
                <reference
                    definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened">
                </reference>
            </visibleWhen>
         </command>  
         </menuContribution>
         <!-- copy qualified name -->
         <menuContribution locationURI="popup:#TextEditorContext?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName" 
         		style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="menu:edit?after=copy">
         	<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
            	style="push" tooltip="Copy Qualified Name">
            	<visibleWhen checkEnabled="false">
                	<reference definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened" />
            	</visibleWhen>
         	</command>  
         </menuContribution>
         <menuContribution locationURI="popup:org.eclipse.xtext.ui.outline?after=additions">
			<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName" 
				style="push" tooltip="Copy Qualified Name">
         		<visibleWhen checkEnabled="false">
	            	<and>
	            		<reference definitionId="org.eclipse.lsat.setting.teditor.Setting.XtextEditor.opened" />
						<iterate>
							<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
						</iterate>
					</and>
				</visibleWhen>
			</command>
         </menuContribution>
    </extension>
    <extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?endof=group.find">
			<command commandId="org.eclipse.xtext.ui.editor.FindReferences">
				<visibleWhen checkEnabled="false">
                	<reference definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened">
                	</reference>
            	</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
	    <handler
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.findrefs.FindReferencesHandler"
            commandId="org.eclipse.xtext.ui.editor.FindReferences">
            <activeWhen>
                <reference
                    definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
    </extension>   

<!-- adding resource factories -->

	<extension
		point="org.eclipse.emf.ecore.extension_parser">
		<parser
			class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.resource.IResourceFactory"
			type="setting">
		</parser>
	</extension>
	<extension point="org.eclipse.xtext.extension_resourceServiceProvider">
        <resourceServiceProvider
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.resource.IResourceUIServiceProvider"
            uriExtension="setting">
        </resourceServiceProvider>
    </extension>


	<!-- marker definitions for org.eclipse.lsat.setting.teditor.Setting -->
	<extension
	        id="setting.check.fast"
	        name="Setting Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.fast"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="setting.check.normal"
	        name="Setting Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.normal"/>
	    <persistent value="true"/>
	</extension>
	<extension
	        id="setting.check.expensive"
	        name="Setting Problem"
	        point="org.eclipse.core.resources.markers">
	    <super type="org.eclipse.xtext.ui.check.expensive"/>
	    <persistent value="true"/>
	</extension>

   <extension
         point="org.eclipse.xtext.builder.participant">
      <participant
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.builder.IXtextBuilderParticipant"
            fileExtensions="setting"
            >
      </participant>
   </extension>
   <extension
            point="org.eclipse.ui.preferencePages">
        <page
            category="org.eclipse.lsat.setting.teditor.Setting"
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
            id="org.eclipse.lsat.setting.teditor.Setting.compiler.preferencePage"
            name="Compiler">
            <keywordReference id="org.eclipse.lsat.setting.teditor.ui.keyword_Setting"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            category="org.eclipse.lsat.setting.teditor.Setting"
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
            id="org.eclipse.lsat.setting.teditor.Setting.compiler.propertyPage"
            name="Compiler">
            <keywordReference id="org.eclipse.lsat.setting.teditor.ui.keyword_Setting"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>

	<!-- Quick Outline -->
	<extension
		point="org.eclipse.ui.handlers">
		<handler 
			class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.outline.quickoutline.ShowQuickOutlineActionHandler"
			commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline">
			<activeWhen>
				<reference
					definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
	<extension
		point="org.eclipse.ui.commands">
		<command
			description="Open the quick outline."
			id="org.eclipse.xtext.ui.editor.outline.QuickOutline"
			name="Quick Outline">
		</command>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution
			locationURI="popup:#TextEditorContext?after=group.open">
			<command commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline"
				style="push"
				tooltip="Open Quick Outline">
				<visibleWhen checkEnabled="false">
					<reference definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened"/>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
    <!-- quickfix marker resolution generator for org.eclipse.lsat.setting.teditor.Setting -->
    <extension
            point="org.eclipse.ui.ide.markerResolution">
        <markerResolutionGenerator
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="org.eclipse.lsat.setting.teditor.ui.setting.check.fast">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
        <markerResolutionGenerator
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="org.eclipse.lsat.setting.teditor.ui.setting.check.normal">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
        <markerResolutionGenerator
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
            markerType="org.eclipse.lsat.setting.teditor.ui.setting.check.expensive">
            <attribute
                name="FIXABLE_KEY"
                value="true">
            </attribute>
        </markerResolutionGenerator>
    </extension>
   	<!-- Rename Refactoring -->
	<extension point="org.eclipse.ui.handlers">
		<handler 
			class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.DefaultRenameElementHandler"
			commandId="org.eclipse.xtext.ui.refactoring.RenameElement">
			<activeWhen>
				<reference
					definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
    <extension point="org.eclipse.ui.menus">
         <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
         <command commandId="org.eclipse.xtext.ui.refactoring.RenameElement"
               style="push">
            <visibleWhen checkEnabled="false">
               <reference
                     definitionId="org.eclipse.lsat.setting.teditor.Setting.Editor.opened">
               </reference>
            </visibleWhen>
         </command>
      </menuContribution>
   </extension>
   <extension point="org.eclipse.ui.preferencePages">
	    <page
	        category="org.eclipse.lsat.setting.teditor.Setting"
	        class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.RefactoringPreferencePage"
	        id="org.eclipse.lsat.setting.teditor.Setting.refactoring"
	        name="Refactoring">
	        <keywordReference id="org.eclipse.lsat.setting.teditor.ui.keyword_Setting"/>
	    </page>
	</extension>

  <extension point="org.eclipse.compare.contentViewers">
    <viewer id="org.eclipse.lsat.setting.teditor.Setting.compare.contentViewers"
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
            extensions="setting">
    </viewer>
  </extension>
  <extension point="org.eclipse.compare.contentMergeViewers">
    <viewer id="org.eclipse.lsat.setting.teditor.Setting.compare.contentMergeViewers"
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
            extensions="setting" label="Setting Compare">
     </viewer>
  </extension>
  <extension point="org.eclipse.ui.editors.documentProviders">
    <provider id="org.eclipse.lsat.setting.teditor.Setting.editors.documentProviders"
            class="org.eclipse.lsat.setting.teditor.ui.SettingExecutableExtensionFactory:org.eclipse.xtext.ui.editor.model.XtextDocumentProvider"
            extensions="setting">
    </provider>
  </extension>

</plugin>
