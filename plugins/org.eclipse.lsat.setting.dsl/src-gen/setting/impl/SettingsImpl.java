/**
 */
package setting.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import expressions.Declaration;
import machine.IResource;
import machine.Peripheral;
import machine.impl.ImportContainerImpl;
import setting.PhysicalSettings;
import setting.SettingPackage;
import setting.Settings;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link setting.impl.SettingsImpl#getDeclarations <em>Declarations</em>}</li>
 *   <li>{@link setting.impl.SettingsImpl#getPhysicalSettings <em>Physical Settings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SettingsImpl extends ImportContainerImpl implements Settings {
	/**
     * The cached value of the '{@link #getDeclarations() <em>Declarations</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDeclarations()
     * @generated
     * @ordered
     */
	protected EList<Declaration> declarations;
	/**
     * The cached value of the '{@link #getPhysicalSettings() <em>Physical Settings</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPhysicalSettings()
     * @generated
     * @ordered
     */
	protected EList<PhysicalSettings> physicalSettings;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected SettingsImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return SettingPackage.Literals.SETTINGS;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Declaration> getDeclarations() {
        if (declarations == null)
        {
            declarations = new EObjectContainmentEList<Declaration>(Declaration.class, this, SettingPackage.SETTINGS__DECLARATIONS);
        }
        return declarations;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<PhysicalSettings> getPhysicalSettings() {
        if (physicalSettings == null)
        {
            physicalSettings = new EObjectContainmentEList<PhysicalSettings>(PhysicalSettings.class, this, SettingPackage.SETTINGS__PHYSICAL_SETTINGS);
        }
        return physicalSettings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<PhysicalSettings> getTransitivePhysicalSettings() {
        java.util.Set<PhysicalSettings> allPhysicalSettings = new java.util.LinkedHashSet<>();
        allPhysicalSettings.addAll(getPhysicalSettings());
        loadAll().stream().filter(i-> i instanceof Settings).map(i -> (Settings)i).forEach(child -> {
            allPhysicalSettings.addAll(child.getPhysicalSettings());
        });
        return new org.eclipse.emf.common.util.BasicEList<PhysicalSettings>(allPhysicalSettings);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PhysicalSettings getPhysicalSettings(final IResource resource, final Peripheral peripheral) {
        IResource r = (resource != null) ? resource : peripheral.getResource();
        PhysicalSettings result = getTransitivePhysicalSettings().stream()
                .filter(e -> r.equals(e.getResource()) && peripheral.equals(e.getPeripheral())).findFirst()
                .orElse(null);
        if (result == null && r.getResource() != r)
            return getPhysicalSettings(r.getResource(), peripheral);
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.SETTINGS__DECLARATIONS:
                return ((InternalEList<?>)getDeclarations()).basicRemove(otherEnd, msgs);
            case SettingPackage.SETTINGS__PHYSICAL_SETTINGS:
                return ((InternalEList<?>)getPhysicalSettings()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case SettingPackage.SETTINGS__DECLARATIONS:
                return getDeclarations();
            case SettingPackage.SETTINGS__PHYSICAL_SETTINGS:
                return getPhysicalSettings();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case SettingPackage.SETTINGS__DECLARATIONS:
                getDeclarations().clear();
                getDeclarations().addAll((Collection<? extends Declaration>)newValue);
                return;
            case SettingPackage.SETTINGS__PHYSICAL_SETTINGS:
                getPhysicalSettings().clear();
                getPhysicalSettings().addAll((Collection<? extends PhysicalSettings>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case SettingPackage.SETTINGS__DECLARATIONS:
                getDeclarations().clear();
                return;
            case SettingPackage.SETTINGS__PHYSICAL_SETTINGS:
                getPhysicalSettings().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case SettingPackage.SETTINGS__DECLARATIONS:
                return declarations != null && !declarations.isEmpty();
            case SettingPackage.SETTINGS__PHYSICAL_SETTINGS:
                return physicalSettings != null && !physicalSettings.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case SettingPackage.SETTINGS___GET_TRANSITIVE_PHYSICAL_SETTINGS:
                return getTransitivePhysicalSettings();
            case SettingPackage.SETTINGS___GET_PHYSICAL_SETTINGS__IRESOURCE_PERIPHERAL:
                return getPhysicalSettings((IResource)arguments.get(0), (Peripheral)arguments.get(1));
        }
        return super.eInvoke(operationID, arguments);
    }

} //SettingsImpl
