/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend.annotations

import org.eclipse.xtend.lib.macro.declaration.TypeReference

import static extension org.eclipse.lsat.common.xtend.Queries.*

class TypeReferenceUtil {
    static val TYPE_HIERARCHY_COMPARATOR = [ TypeReference t1, TypeReference t2 |
        switch null {
            case (t1 == t2): {
                return 0
            }
            case (t1.isAssignableFrom(t2)): {
                return 1
            }
            case (t2.isAssignableFrom(t1)): {
                return -1
            }
            default: {
                return 0
            }
        }
    ]
    
    static def getCommonBaseTypes(Iterable<TypeReference> typeReferences) {
        val commonTypeReferences = newTreeSet(TYPE_HIERARCHY_COMPARATOR)
        if (!typeReferences.empty) {
            commonTypeReferences += typeReferences.head.assignableTypeReferences
            typeReferences.drop(1).forEach[commonTypeReferences.retainAll(it.assignableTypeReferences.toSet)]
        }
        return commonTypeReferences
    }
    
    static def getAssignableTypeReferences(TypeReference typeReference) {
        #[typeReference].closure(true) [declaredSuperTypes]
    }
    
    static def findResolvedMethod(TypeReference typeReference, String name, TypeReference... parameterTypes) {
        return typeReference.allResolvedMethods.map[declaration].filter[simpleName == name].findFirst[parameters.map[type] == parameterTypes]
    }
}