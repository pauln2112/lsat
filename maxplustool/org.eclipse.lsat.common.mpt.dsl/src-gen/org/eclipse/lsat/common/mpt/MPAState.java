/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MPA State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.MPAState#getState <em>State</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.MPAState#getMatrixRowIndex <em>Matrix Row Index</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.MPAState#getMatrixColumnIndex <em>Matrix Column Index</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.MPAState#getMatrix <em>Matrix</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.MPAState#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPAState()
 * @model
 * @generated
 */
public interface MPAState extends Vertex {
	/**
	 * Returns the value of the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference.
	 * @see #setState(FSMState)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPAState_State()
	 * @model required="true"
	 * @generated
	 */
	FSMState getState();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.MPAState#getState <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(FSMState value);

	/**
	 * Returns the value of the '<em><b>Matrix Row Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matrix Row Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matrix Row Index</em>' attribute.
	 * @see #setMatrixRowIndex(int)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPAState_MatrixRowIndex()
	 * @model required="true"
	 * @generated
	 */
	int getMatrixRowIndex();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.MPAState#getMatrixRowIndex <em>Matrix Row Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Matrix Row Index</em>' attribute.
	 * @see #getMatrixRowIndex()
	 * @generated
	 */
	void setMatrixRowIndex(int value);

	/**
	 * Returns the value of the '<em><b>Matrix Column Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matrix Column Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matrix Column Index</em>' attribute.
	 * @see #setMatrixColumnIndex(int)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPAState_MatrixColumnIndex()
	 * @model required="true"
	 * @generated
	 */
	int getMatrixColumnIndex();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.MPAState#getMatrixColumnIndex <em>Matrix Column Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Matrix Column Index</em>' attribute.
	 * @see #getMatrixColumnIndex()
	 * @generated
	 */
	void setMatrixColumnIndex(int value);

	/**
	 * Returns the value of the '<em><b>Matrix</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matrix</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matrix</em>' reference.
	 * @see #setMatrix(Matrix)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPAState_Matrix()
	 * @model required="true"
	 * @generated
	 */
	Matrix getMatrix();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.MPAState#getMatrix <em>Matrix</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Matrix</em>' reference.
	 * @see #getMatrix()
	 * @generated
	 */
	void setMatrix(Matrix value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPAState_Value()
	 * @model dataType="org.eclipse.lsat.common.mpt.Value" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	double getValue();

} // MPAState
