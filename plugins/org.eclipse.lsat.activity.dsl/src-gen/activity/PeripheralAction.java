/**
 */
package activity;

import machine.HasResourcePeripheral;
import machine.Peripheral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Peripheral Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.PeripheralAction#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link activity.PeripheralAction#getSchedulingType <em>Scheduling Type</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getPeripheralAction()
 * @model abstract="true"
 * @generated
 */
public interface PeripheralAction extends HasResourcePeripheral, ResourceAction {
	/**
     * Returns the value of the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Peripheral</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripheral</em>' reference.
     * @see #setPeripheral(Peripheral)
     * @see activity.ActivityPackage#getPeripheralAction_Peripheral()
     * @model required="true"
     * @generated
     */
	Peripheral getPeripheral();

	/**
     * Sets the value of the '{@link activity.PeripheralAction#getPeripheral <em>Peripheral</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Peripheral</em>' reference.
     * @see #getPeripheral()
     * @generated
     */
	void setPeripheral(Peripheral value);

	/**
     * Returns the value of the '<em><b>Scheduling Type</b></em>' attribute.
     * The literals are from the enumeration {@link activity.SchedulingType}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduling Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Scheduling Type</em>' attribute.
     * @see activity.SchedulingType
     * @see #setSchedulingType(SchedulingType)
     * @see activity.ActivityPackage#getPeripheralAction_SchedulingType()
     * @model
     * @generated
     */
	SchedulingType getSchedulingType();

	/**
     * Sets the value of the '{@link activity.PeripheralAction#getSchedulingType <em>Scheduling Type</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Scheduling Type</em>' attribute.
     * @see activity.SchedulingType
     * @see #getSchedulingType()
     * @generated
     */
	void setSchedulingType(SchedulingType value);

} // PeripheralAction
