/**
 */
package lsat_graph.impl;

import lsat_graph.EventAnnotation;
import lsat_graph.lsat_graphPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.common.graph.directed.impl.AspectImpl;

import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.Task;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EventAction Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.impl.EventAnnotationImpl#isRequireEvent <em>Require Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventAnnotationImpl<T extends Task> extends AspectImpl<T, Dependency> implements EventAnnotation<T>
{
    /**
     * The default value of the '{@link #isRequireEvent() <em>Require Event</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isRequireEvent()
     * @generated
     * @ordered
     */
    protected static final boolean REQUIRE_EVENT_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isRequireEvent() <em>Require Event</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isRequireEvent()
     * @generated
     * @ordered
     */
    protected boolean requireEvent = REQUIRE_EVENT_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected EventAnnotationImpl()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass()
    {
        return lsat_graphPackage.Literals.EVENT_ANNOTATION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * This is specialized for the more specific element type known in this context.
     * @generated
     */
    @Override
    public EList<Dependency> getEdges()
    {
        if (edges == null)
        {
            edges = new EObjectWithInverseResolvingEList.ManyInverse<Dependency>(Dependency.class, this, lsat_graphPackage.EVENT_ANNOTATION__EDGES, DirectedGraphPackage.EDGE__ASPECTS) { private static final long serialVersionUID = 1L; @Override public Class<?> getInverseFeatureClass() { return Edge.class; } };
        }
        return edges;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * This is specialized for the more specific element type known in this context.
     * @generated
     */
    @Override
    public EList<T> getNodes()
    {
        if (nodes == null)
        {
            nodes = new EObjectWithInverseResolvingEList.ManyInverse<T>(Task.class, this, lsat_graphPackage.EVENT_ANNOTATION__NODES, DirectedGraphPackage.NODE__ASPECTS) { private static final long serialVersionUID = 1L; @Override public Class<?> getInverseFeatureClass() { return Node.class; } };
        }
        return nodes;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isRequireEvent()
    {
        return requireEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setRequireEvent(boolean newRequireEvent)
    {
        boolean oldRequireEvent = requireEvent;
        requireEvent = newRequireEvent;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, lsat_graphPackage.EVENT_ANNOTATION__REQUIRE_EVENT, oldRequireEvent, requireEvent));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType)
    {
        switch (featureID)
        {
            case lsat_graphPackage.EVENT_ANNOTATION__REQUIRE_EVENT:
                return isRequireEvent();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue)
    {
        switch (featureID)
        {
            case lsat_graphPackage.EVENT_ANNOTATION__REQUIRE_EVENT:
                setRequireEvent((Boolean)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID)
    {
        switch (featureID)
        {
            case lsat_graphPackage.EVENT_ANNOTATION__REQUIRE_EVENT:
                setRequireEvent(REQUIRE_EVENT_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID)
    {
        switch (featureID)
        {
            case lsat_graphPackage.EVENT_ANNOTATION__REQUIRE_EVENT:
                return requireEvent != REQUIRE_EVENT_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString()
    {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (requireEvent: ");
        result.append(requireEvent);
        result.append(')');
        return result.toString();
    }

} //EventAnnotationImpl
