/**
 */
package org.eclipse.lsat.common.scheduler.schedule.impl;

import org.eclipse.lsat.common.graph.directed.impl.EdgeImpl;

import org.eclipse.lsat.common.scheduler.graph.Dependency;

import org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary;
import org.eclipse.lsat.common.scheduler.schedule.SchedulePackage;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scheduled Dependency</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledDependencyImpl#getBoundary <em>Boundary</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledDependencyImpl#getDependency <em>Dependency</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledDependencyImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScheduledDependencyImpl extends EdgeImpl implements ScheduledDependency {
	/**
	 * The default value of the '{@link #getBoundary() <em>Boundary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBoundary()
	 * @generated
	 * @ordered
	 */
	protected static final DependencyBoundary BOUNDARY_EDEFAULT = DependencyBoundary.IN_RESOURCE;

	/**
	 * The cached value of the '{@link #getDependency() <em>Dependency</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependency()
	 * @generated
	 * @ordered
	 */
	protected Dependency dependency;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final ScheduledDependencyType TYPE_EDEFAULT = ScheduledDependencyType.SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ScheduledDependencyType type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScheduledDependencyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SchedulePackage.Literals.SCHEDULED_DEPENDENCY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DependencyBoundary getBoundary() {
		final org.eclipse.lsat.common.scheduler.resources.Resource sourceResource = ScheduleHelper.safeGetResource(getSourceNode());
		final org.eclipse.lsat.common.scheduler.resources.Resource targetResource = ScheduleHelper.safeGetResource(getTargetNode());
		if (null == sourceResource || null == targetResource) {
			return null;
		}
		if (sourceResource == targetResource) {
			return DependencyBoundary.IN_RESOURCE;
		}
		if (sourceResource.getContainer() == targetResource.getContainer()) {
			return DependencyBoundary.CROSS_RESOURCE;
		}
		return DependencyBoundary.CROSS_RESOURCE_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Dependency getDependency() {
		if (dependency != null && dependency.eIsProxy()) {
			InternalEObject oldDependency = (InternalEObject)dependency;
			dependency = (Dependency)eResolveProxy(oldDependency);
			if (dependency != oldDependency) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SchedulePackage.SCHEDULED_DEPENDENCY__DEPENDENCY, oldDependency, dependency));
			}
		}
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency basicGetDependency() {
		return dependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDependency(Dependency newDependency) {
		Dependency oldDependency = dependency;
		dependency = newDependency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SchedulePackage.SCHEDULED_DEPENDENCY__DEPENDENCY, oldDependency, dependency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ScheduledDependencyType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(ScheduledDependencyType newType) {
		ScheduledDependencyType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SchedulePackage.SCHEDULED_DEPENDENCY__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SchedulePackage.SCHEDULED_DEPENDENCY__BOUNDARY:
				return getBoundary();
			case SchedulePackage.SCHEDULED_DEPENDENCY__DEPENDENCY:
				if (resolve) return getDependency();
				return basicGetDependency();
			case SchedulePackage.SCHEDULED_DEPENDENCY__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SchedulePackage.SCHEDULED_DEPENDENCY__DEPENDENCY:
				setDependency((Dependency)newValue);
				return;
			case SchedulePackage.SCHEDULED_DEPENDENCY__TYPE:
				setType((ScheduledDependencyType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SchedulePackage.SCHEDULED_DEPENDENCY__DEPENDENCY:
				setDependency((Dependency)null);
				return;
			case SchedulePackage.SCHEDULED_DEPENDENCY__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SchedulePackage.SCHEDULED_DEPENDENCY__BOUNDARY:
				return getBoundary() != BOUNDARY_EDEFAULT;
			case SchedulePackage.SCHEDULED_DEPENDENCY__DEPENDENCY:
				return dependency != null;
			case SchedulePackage.SCHEDULED_DEPENDENCY__TYPE:
				return type != TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //ScheduledDependencyImpl
