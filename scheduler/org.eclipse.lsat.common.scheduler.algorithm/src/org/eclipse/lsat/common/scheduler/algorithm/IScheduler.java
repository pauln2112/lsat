/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.scheduler.algorithm;

import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;

public interface IScheduler<T extends Task> {
    /**
     * Creates a schedule from a graph.
     *
     * @param aTaskGraph a graph
     * @return a schedule
     * @throws SchedulerException When scheduling fails
     */
    Schedule<T> createSchedule(TaskDependencyGraph<? extends T> taskDependencyGraph) throws SchedulerException;
}
