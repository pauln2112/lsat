/**
 */
package activity.impl;

import activity.ActivityPackage;
import activity.Move;
import machine.Distance;
import machine.Profile;
import machine.SymbolicPosition;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Move</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link activity.impl.MoveImpl#getProfile <em>Profile</em>}</li>
 *   <li>{@link activity.impl.MoveImpl#getSuccessorMove <em>Successor Move</em>}</li>
 *   <li>{@link activity.impl.MoveImpl#getPredecessorMove <em>Predecessor Move</em>}</li>
 *   <li>{@link activity.impl.MoveImpl#isStopAtTarget <em>Stop At Target</em>}</li>
 *   <li>{@link activity.impl.MoveImpl#isPositionMove <em>Position Move</em>}</li>
 *   <li>{@link activity.impl.MoveImpl#getTargetPosition <em>Target Position</em>}</li>
 *   <li>{@link activity.impl.MoveImpl#getSourcePosition <em>Source Position</em>}</li>
 *   <li>{@link activity.impl.MoveImpl#getDistance <em>Distance</em>}</li>
 *   <li>{@link activity.impl.MoveImpl#isPassing <em>Passing</em>}</li>
 *   <li>{@link activity.impl.MoveImpl#isContinuing <em>Continuing</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MoveImpl extends PeripheralActionImpl implements Move {
	/**
     * The cached value of the '{@link #getProfile() <em>Profile</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getProfile()
     * @generated
     * @ordered
     */
	protected Profile profile;

	/**
     * The default value of the '{@link #isStopAtTarget() <em>Stop At Target</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isStopAtTarget()
     * @generated
     * @ordered
     */
	protected static final boolean STOP_AT_TARGET_EDEFAULT = false;

	/**
     * The default value of the '{@link #isPositionMove() <em>Position Move</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isPositionMove()
     * @generated
     * @ordered
     */
	protected static final boolean POSITION_MOVE_EDEFAULT = false;

	/**
     * The cached value of the '{@link #getTargetPosition() <em>Target Position</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTargetPosition()
     * @generated
     * @ordered
     */
	protected SymbolicPosition targetPosition;

	/**
     * The cached value of the '{@link #getDistance() <em>Distance</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDistance()
     * @generated
     * @ordered
     */
	protected Distance distance;

	/**
     * The default value of the '{@link #isPassing() <em>Passing</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isPassing()
     * @generated
     * @ordered
     */
	protected static final boolean PASSING_EDEFAULT = false;

	/**
     * The cached value of the '{@link #isPassing() <em>Passing</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isPassing()
     * @generated
     * @ordered
     */
	protected boolean passing = PASSING_EDEFAULT;

	/**
     * The default value of the '{@link #isContinuing() <em>Continuing</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isContinuing()
     * @generated
     * @ordered
     */
	protected static final boolean CONTINUING_EDEFAULT = false;

	/**
     * The cached value of the '{@link #isContinuing() <em>Continuing</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #isContinuing()
     * @generated
     * @ordered
     */
	protected boolean continuing = CONTINUING_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected MoveImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ActivityPackage.Literals.MOVE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Profile getProfile() {
        if (profile != null && profile.eIsProxy())
        {
            InternalEObject oldProfile = (InternalEObject)profile;
            profile = (Profile)eResolveProxy(oldProfile);
            if (profile != oldProfile)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ActivityPackage.MOVE__PROFILE, oldProfile, profile));
            }
        }
        return profile;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Profile basicGetProfile() {
        return profile;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setProfile(Profile newProfile) {
        Profile oldProfile = profile;
        profile = newProfile;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.MOVE__PROFILE, oldProfile, profile));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Move getSuccessorMove() {
        Move successorMove = basicGetSuccessorMove();
        return successorMove != null && successorMove.eIsProxy() ? (Move)eResolveProxy((InternalEObject)successorMove) : successorMove;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Move basicGetSuccessorMove() {
        org.eclipse.lsat.common.queries.QueryableIterable<org.eclipse.lsat.common.graph.directed.editable.Node> successorNodes = 
                org.eclipse.lsat.common.graph.directed.editable.EdgQueries.allSuccessors(this);
        org.eclipse.lsat.common.queries.QueryableIterable<?  extends Move> successorMoves = ActivityQueries.getActionsFor(
                getResource(), getPeripheral(), getClass(), successorNodes);
        return successorMoves.first();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Move getPredecessorMove() {
        Move predecessorMove = basicGetPredecessorMove();
        return predecessorMove != null && predecessorMove.eIsProxy() ? (Move)eResolveProxy((InternalEObject)predecessorMove) : predecessorMove;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Move basicGetPredecessorMove() {
        org.eclipse.lsat.common.queries.QueryableIterable<org.eclipse.lsat.common.graph.directed.editable.Node> predecessorNodes = 
                org.eclipse.lsat.common.graph.directed.editable.EdgQueries.allPredecessors(this);
        org.eclipse.lsat.common.queries.QueryableIterable<? extends Move> predecessorMoves = ActivityQueries.getActionsFor(
                getResource(), getPeripheral(), getClass(), predecessorNodes);
        return predecessorMoves.first();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isPositionMove() {
        return getPeripheral() != null && getPeripheral().getDistances().isEmpty();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SymbolicPosition getTargetPosition() {
        if (targetPosition != null && targetPosition.eIsProxy())
        {
            InternalEObject oldTargetPosition = (InternalEObject)targetPosition;
            targetPosition = (SymbolicPosition)eResolveProxy(oldTargetPosition);
            if (targetPosition != oldTargetPosition)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ActivityPackage.MOVE__TARGET_POSITION, oldTargetPosition, targetPosition));
            }
        }
        return targetPosition;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public SymbolicPosition basicGetTargetPosition() {
        return targetPosition;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setTargetPosition(SymbolicPosition newTargetPosition) {
        SymbolicPosition oldTargetPosition = targetPosition;
        targetPosition = newTargetPosition;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.MOVE__TARGET_POSITION, oldTargetPosition, targetPosition));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SymbolicPosition getSourcePosition() {
        SymbolicPosition sourcePosition = basicGetSourcePosition();
        return sourcePosition != null && sourcePosition.eIsProxy() ? (SymbolicPosition)eResolveProxy((InternalEObject)sourcePosition) : sourcePosition;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public SymbolicPosition basicGetSourcePosition() {
        Object previousMove = getPredecessorMove();
        if (previousMove instanceof Move) {
            return ((Move)previousMove).getTargetPosition();
        }
        if (getGraph() instanceof activity.Activity) {
            for (activity.LocationPrerequisite locationPrerequisite : ((activity.Activity)getGraph()).getPrerequisites()) {
                if (rpEquals(locationPrerequisite)) {
                    return locationPrerequisite.getPosition();
                }
            }
        }
        return null;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Distance getDistance() {
        if (distance != null && distance.eIsProxy())
        {
            InternalEObject oldDistance = (InternalEObject)distance;
            distance = (Distance)eResolveProxy(oldDistance);
            if (distance != oldDistance)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ActivityPackage.MOVE__DISTANCE, oldDistance, distance));
            }
        }
        return distance;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Distance basicGetDistance() {
        return distance;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setDistance(Distance newDistance) {
        Distance oldDistance = distance;
        distance = newDistance;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.MOVE__DISTANCE, oldDistance, distance));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isPassing() {
        return passing;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPassing(boolean newPassing) {
        boolean oldPassing = passing;
        passing = newPassing;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.MOVE__PASSING, oldPassing, passing));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isContinuing() {
        return continuing;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setContinuing(boolean newContinuing) {
        boolean oldContinuing = continuing;
        continuing = newContinuing;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.MOVE__CONTINUING, oldContinuing, continuing));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isStopAtTarget() {
        boolean result =  isPositionMove() ? isPassing(): isContinuing();
        return !result;
    }

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void setStopAtTarget(boolean newStopAtTarget) {
		if (isPositionMove())
			setPassing(!newStopAtTarget);
		else
			setContinuing(!newStopAtTarget);
	}

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case ActivityPackage.MOVE__PROFILE:
                if (resolve) return getProfile();
                return basicGetProfile();
            case ActivityPackage.MOVE__SUCCESSOR_MOVE:
                if (resolve) return getSuccessorMove();
                return basicGetSuccessorMove();
            case ActivityPackage.MOVE__PREDECESSOR_MOVE:
                if (resolve) return getPredecessorMove();
                return basicGetPredecessorMove();
            case ActivityPackage.MOVE__STOP_AT_TARGET:
                return isStopAtTarget();
            case ActivityPackage.MOVE__POSITION_MOVE:
                return isPositionMove();
            case ActivityPackage.MOVE__TARGET_POSITION:
                if (resolve) return getTargetPosition();
                return basicGetTargetPosition();
            case ActivityPackage.MOVE__SOURCE_POSITION:
                if (resolve) return getSourcePosition();
                return basicGetSourcePosition();
            case ActivityPackage.MOVE__DISTANCE:
                if (resolve) return getDistance();
                return basicGetDistance();
            case ActivityPackage.MOVE__PASSING:
                return isPassing();
            case ActivityPackage.MOVE__CONTINUING:
                return isContinuing();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case ActivityPackage.MOVE__PROFILE:
                setProfile((Profile)newValue);
                return;
            case ActivityPackage.MOVE__STOP_AT_TARGET:
                setStopAtTarget((Boolean)newValue);
                return;
            case ActivityPackage.MOVE__TARGET_POSITION:
                setTargetPosition((SymbolicPosition)newValue);
                return;
            case ActivityPackage.MOVE__DISTANCE:
                setDistance((Distance)newValue);
                return;
            case ActivityPackage.MOVE__PASSING:
                setPassing((Boolean)newValue);
                return;
            case ActivityPackage.MOVE__CONTINUING:
                setContinuing((Boolean)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.MOVE__PROFILE:
                setProfile((Profile)null);
                return;
            case ActivityPackage.MOVE__STOP_AT_TARGET:
                setStopAtTarget(STOP_AT_TARGET_EDEFAULT);
                return;
            case ActivityPackage.MOVE__TARGET_POSITION:
                setTargetPosition((SymbolicPosition)null);
                return;
            case ActivityPackage.MOVE__DISTANCE:
                setDistance((Distance)null);
                return;
            case ActivityPackage.MOVE__PASSING:
                setPassing(PASSING_EDEFAULT);
                return;
            case ActivityPackage.MOVE__CONTINUING:
                setContinuing(CONTINUING_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.MOVE__PROFILE:
                return profile != null;
            case ActivityPackage.MOVE__SUCCESSOR_MOVE:
                return basicGetSuccessorMove() != null;
            case ActivityPackage.MOVE__PREDECESSOR_MOVE:
                return basicGetPredecessorMove() != null;
            case ActivityPackage.MOVE__STOP_AT_TARGET:
                return isStopAtTarget() != STOP_AT_TARGET_EDEFAULT;
            case ActivityPackage.MOVE__POSITION_MOVE:
                return isPositionMove() != POSITION_MOVE_EDEFAULT;
            case ActivityPackage.MOVE__TARGET_POSITION:
                return targetPosition != null;
            case ActivityPackage.MOVE__SOURCE_POSITION:
                return basicGetSourcePosition() != null;
            case ActivityPackage.MOVE__DISTANCE:
                return distance != null;
            case ActivityPackage.MOVE__PASSING:
                return passing != PASSING_EDEFAULT;
            case ActivityPackage.MOVE__CONTINUING:
                return continuing != CONTINUING_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (passing: ");
        result.append(passing);
        result.append(", continuing: ");
        result.append(continuing);
        result.append(')');
        return result.toString();
    }

} //MoveImpl
