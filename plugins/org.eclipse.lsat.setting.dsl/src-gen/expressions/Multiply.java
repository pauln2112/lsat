/**
 */
package expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiply</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see expressions.ExpressionsPackage#getMultiply()
 * @model
 * @generated
 */
public interface Multiply extends BinaryExpression {

} // Multiply
