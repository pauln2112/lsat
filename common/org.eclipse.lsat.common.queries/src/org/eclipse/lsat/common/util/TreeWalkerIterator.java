/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import static org.eclipse.lsat.common.util.IteratorUtil.flatMap;

import java.util.Collections;
import java.util.Iterator;
import java.util.function.Function;

public class TreeWalkerIterator<E> implements BranchIterator<E> {
    private final Function<E, Iterator<? extends E>> childrenFunctor;

    private final StackedIterator<E> stackedIterator;

    private boolean canPrune = false;

    public TreeWalkerIterator(Iterator<? extends E> source, boolean includeSource,
            Function<E, Iterator<? extends E>> childrenFunctor)
    {
        this.childrenFunctor = childrenFunctor;
        if (includeSource) {
            this.stackedIterator = new StackedIterator<E>(source);
        } else {
            this.stackedIterator = new StackedIterator<E>(flatMap(source, childrenFunctor));
        }
    }

    @Override
    public boolean hasNext() {
        canPrune = false;
        return stackedIterator.hasNext();
    }

    @Override
    public E next() {
        final E next = stackedIterator.next();
        final Iterator<? extends E> children = childrenFunctor.apply(next);
        // Protect against NullPointerException, do push though to allow prune
        stackedIterator.push(children == null ? Collections.emptyIterator() : children);
        canPrune = true;
        return next;
    }

    @Override
    public void prune() {
        if (canPrune) {
            stackedIterator.pop();
            canPrune = false;
        } else {
            throw new IllegalStateException("Prune is not allowed");
        }
    }
}
