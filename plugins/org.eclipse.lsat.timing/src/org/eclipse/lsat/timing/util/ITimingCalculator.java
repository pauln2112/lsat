/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.util;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.motioncalculator.MotionException;

import activity.Move;
import machine.SetPoint;

public interface ITimingCalculator {
    static ITimingCalculator ZERO_CALCULATOR = new ITimingCalculator() {
        @Override
        public void reset() {
            // Empty
        }

        @Override
        public Map<SetPoint, BigDecimal> calculateMotionTime(Move move) {
            final List<SetPoint> setPoints = move.getPeripheral().getType().getSetPoints();
            final Map<SetPoint, BigDecimal> result = new LinkedHashMap<SetPoint, BigDecimal>(setPoints.size());
            for (SetPoint setPoint: setPoints) {
                result.put(setPoint, BigDecimal.ZERO);
            }
            return result;
        }

        @Override
        public BigDecimal calculateDuration(Node node) {
            return BigDecimal.ZERO;
        }
    };

    void reset();

    BigDecimal calculateDuration(Node node) throws SpecificationException, MotionException;

    Map<SetPoint, BigDecimal> calculateMotionTime(Move move) throws SpecificationException, MotionException;
}
