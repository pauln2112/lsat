/**
 */
package activity.impl;

import activity.ActivityPackage;
import activity.EventItem;

import machine.impl.ResourceItemImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EventAction Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EventItemImpl extends ResourceItemImpl implements EventItem
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected EventItemImpl()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass()
    {
        return ActivityPackage.Literals.EVENT_ITEM;
    }

} //EventItemImpl
