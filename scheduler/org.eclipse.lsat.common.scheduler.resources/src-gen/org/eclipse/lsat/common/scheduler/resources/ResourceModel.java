/**
 */
package org.eclipse.lsat.common.scheduler.resources;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesPackage#getResourceModel()
 * @model
 * @generated
 */
public interface ResourceModel extends ResourceContainer {
} // ResourceModel
