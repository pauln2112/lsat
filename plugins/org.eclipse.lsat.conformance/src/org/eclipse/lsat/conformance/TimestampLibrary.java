/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.ecore.xml.type.internal.XMLCalendar;
import org.eclipse.m2m.qvt.oml.blackbox.java.Module;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;

/** Time stamp QVTo blackbox library. */
@Module(packageURIs = "http://www.eclipse.org/emf/2003/XMLType")
public class TimestampLibrary {
    private static final BigDecimal THOUSAND = new BigDecimal(1000);

    @Operation
    public XMLGregorianCalendar timestampToXMLCalendar(BigDecimal timestamp) {
        final long date = timestamp.multiply(THOUSAND).longValue();
        return new XMLCalendar(new Date(date), XMLCalendar.DATETIME);
    }
}
