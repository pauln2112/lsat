/**
 */
package machine.impl;

import java.util.Collection;
import machine.Machine;
import machine.MachinePackage;
import machine.PathAnnotation;
import machine.PeripheralType;
import machine.Resource;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Machine</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.MachineImpl#getType <em>Type</em>}</li>
 *   <li>{@link machine.impl.MachineImpl#getPathAnnotations <em>Path Annotations</em>}</li>
 *   <li>{@link machine.impl.MachineImpl#getResources <em>Resources</em>}</li>
 *   <li>{@link machine.impl.MachineImpl#getPeripheralTypes <em>Peripheral Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MachineImpl extends ImportContainerImpl implements Machine {
	/**
     * The default value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
	protected static final String TYPE_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
	protected String type = TYPE_EDEFAULT;

	/**
     * The cached value of the '{@link #getPathAnnotations() <em>Path Annotations</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPathAnnotations()
     * @generated
     * @ordered
     */
	protected EList<PathAnnotation> pathAnnotations;

	/**
     * The cached value of the '{@link #getResources() <em>Resources</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getResources()
     * @generated
     * @ordered
     */
	protected EList<Resource> resources;

	/**
     * The cached value of the '{@link #getPeripheralTypes() <em>Peripheral Types</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPeripheralTypes()
     * @generated
     * @ordered
     */
	protected EList<PeripheralType> peripheralTypes;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected MachineImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.MACHINE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getType() {
        return type;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setType(String newType) {
        String oldType = type;
        type = newType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.MACHINE__TYPE, oldType, type));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<PathAnnotation> getPathAnnotations() {
        if (pathAnnotations == null)
        {
            pathAnnotations = new EObjectContainmentEList<PathAnnotation>(PathAnnotation.class, this, MachinePackage.MACHINE__PATH_ANNOTATIONS);
        }
        return pathAnnotations;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Resource> getResources() {
        if (resources == null)
        {
            resources = new EObjectContainmentEList<Resource>(Resource.class, this, MachinePackage.MACHINE__RESOURCES);
        }
        return resources;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<PeripheralType> getPeripheralTypes() {
        if (peripheralTypes == null)
        {
            peripheralTypes = new EObjectContainmentEList<PeripheralType>(PeripheralType.class, this, MachinePackage.MACHINE__PERIPHERAL_TYPES);
        }
        return peripheralTypes;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.MACHINE__PATH_ANNOTATIONS:
                return ((InternalEList<?>)getPathAnnotations()).basicRemove(otherEnd, msgs);
            case MachinePackage.MACHINE__RESOURCES:
                return ((InternalEList<?>)getResources()).basicRemove(otherEnd, msgs);
            case MachinePackage.MACHINE__PERIPHERAL_TYPES:
                return ((InternalEList<?>)getPeripheralTypes()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.MACHINE__TYPE:
                return getType();
            case MachinePackage.MACHINE__PATH_ANNOTATIONS:
                return getPathAnnotations();
            case MachinePackage.MACHINE__RESOURCES:
                return getResources();
            case MachinePackage.MACHINE__PERIPHERAL_TYPES:
                return getPeripheralTypes();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.MACHINE__TYPE:
                setType((String)newValue);
                return;
            case MachinePackage.MACHINE__PATH_ANNOTATIONS:
                getPathAnnotations().clear();
                getPathAnnotations().addAll((Collection<? extends PathAnnotation>)newValue);
                return;
            case MachinePackage.MACHINE__RESOURCES:
                getResources().clear();
                getResources().addAll((Collection<? extends Resource>)newValue);
                return;
            case MachinePackage.MACHINE__PERIPHERAL_TYPES:
                getPeripheralTypes().clear();
                getPeripheralTypes().addAll((Collection<? extends PeripheralType>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.MACHINE__TYPE:
                setType(TYPE_EDEFAULT);
                return;
            case MachinePackage.MACHINE__PATH_ANNOTATIONS:
                getPathAnnotations().clear();
                return;
            case MachinePackage.MACHINE__RESOURCES:
                getResources().clear();
                return;
            case MachinePackage.MACHINE__PERIPHERAL_TYPES:
                getPeripheralTypes().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.MACHINE__TYPE:
                return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
            case MachinePackage.MACHINE__PATH_ANNOTATIONS:
                return pathAnnotations != null && !pathAnnotations.isEmpty();
            case MachinePackage.MACHINE__RESOURCES:
                return resources != null && !resources.isEmpty();
            case MachinePackage.MACHINE__PERIPHERAL_TYPES:
                return peripheralTypes != null && !peripheralTypes.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (type: ");
        result.append(type);
        result.append(')');
        return result.toString();
    }

} //MachineImpl
