/**
 */
package timing.impl;

import expressions.ExpressionsPackage;
import expressions.impl.ExpressionsPackageImpl;
import machine.MachinePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import setting.SettingPackage;

import setting.impl.SettingPackageImpl;

import timing.Array;
import timing.CalculationMode;
import timing.Distribution;
import timing.EnumeratedDistribution;
import timing.FixedValue;
import timing.NormalDistribution;
import timing.PertDistribution;
import timing.Timing;
import timing.TimingFactory;
import timing.TimingPackage;
import timing.TriangularDistribution;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimingPackageImpl extends EPackageImpl implements TimingPackage {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass timingEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass fixedValueEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass arrayEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass triangularDistributionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass distributionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass pertDistributionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass normalDistributionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass enumeratedDistributionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum calculationModeEEnum = null;

	/**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see timing.TimingPackage#eNS_URI
     * @see #init()
     * @generated
     */
	private TimingPackageImpl() {
        super(eNS_URI, TimingFactory.eINSTANCE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static boolean isInited = false;

	/**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link TimingPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
	public static TimingPackage init() {
        if (isInited) return (TimingPackage)EPackage.Registry.INSTANCE.getEPackage(TimingPackage.eNS_URI);

        // Obtain or create and register package
        Object registeredTimingPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        TimingPackageImpl theTimingPackage = registeredTimingPackage instanceof TimingPackageImpl ? (TimingPackageImpl)registeredTimingPackage : new TimingPackageImpl();

        isInited = true;

        // Initialize simple dependencies
        MachinePackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SettingPackage.eNS_URI);
        SettingPackageImpl theSettingPackage = (SettingPackageImpl)(registeredPackage instanceof SettingPackageImpl ? registeredPackage : SettingPackage.eINSTANCE);
        registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI);
        ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl)(registeredPackage instanceof ExpressionsPackageImpl ? registeredPackage : ExpressionsPackage.eINSTANCE);

        // Create package meta-data objects
        theTimingPackage.createPackageContents();
        theSettingPackage.createPackageContents();
        theExpressionsPackage.createPackageContents();

        // Initialize created meta-data
        theTimingPackage.initializePackageContents();
        theSettingPackage.initializePackageContents();
        theExpressionsPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theTimingPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(TimingPackage.eNS_URI, theTimingPackage);
        return theTimingPackage;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getTiming() {
        return timingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getFixedValue() {
        return fixedValueEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getFixedValue_Value() {
        return (EAttribute)fixedValueEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getFixedValue_ValueExp() {
        return (EReference)fixedValueEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getArray() {
        return arrayEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getArray_Values() {
        return (EAttribute)arrayEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getArray_ValuesExp() {
        return (EReference)arrayEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getTriangularDistribution() {
        return triangularDistributionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getTriangularDistribution_Mode() {
        return (EAttribute)triangularDistributionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getTriangularDistribution_Min() {
        return (EAttribute)triangularDistributionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getTriangularDistribution_Max() {
        return (EAttribute)triangularDistributionEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getTriangularDistribution_ModeExp() {
        return (EReference)triangularDistributionEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getTriangularDistribution_MinExp() {
        return (EReference)triangularDistributionEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getTriangularDistribution_MaxExp() {
        return (EReference)triangularDistributionEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDistribution() {
        return distributionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getDistribution_Default() {
        return (EAttribute)distributionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getDistribution_DefaultExp() {
        return (EReference)distributionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPertDistribution() {
        return pertDistributionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPertDistribution_Mode() {
        return (EAttribute)pertDistributionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPertDistribution_Min() {
        return (EAttribute)pertDistributionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPertDistribution_Max() {
        return (EAttribute)pertDistributionEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPertDistribution_ModeExp() {
        return (EReference)pertDistributionEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPertDistribution_MinExp() {
        return (EReference)pertDistributionEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPertDistribution_MaxExp() {
        return (EReference)pertDistributionEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPertDistribution_Gamma() {
        return (EAttribute)pertDistributionEClass.getEStructuralFeatures().get(6);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPertDistribution_GammaExp() {
        return (EReference)pertDistributionEClass.getEStructuralFeatures().get(7);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getNormalDistribution() {
        return normalDistributionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getNormalDistribution_Mean() {
        return (EAttribute)normalDistributionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getNormalDistribution_Sd() {
        return (EAttribute)normalDistributionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getNormalDistribution_MeanExp() {
        return (EReference)normalDistributionEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getNormalDistribution_SdExp() {
        return (EReference)normalDistributionEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getEnumeratedDistribution() {
        return enumeratedDistributionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getEnumeratedDistribution_Values() {
        return (EAttribute)enumeratedDistributionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getEnumeratedDistribution_ValuesExp() {
        return (EReference)enumeratedDistributionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EEnum getCalculationMode() {
        return calculationModeEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public TimingFactory getTimingFactory() {
        return (TimingFactory)getEFactoryInstance();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isCreated = false;

	/**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        timingEClass = createEClass(TIMING);

        fixedValueEClass = createEClass(FIXED_VALUE);
        createEAttribute(fixedValueEClass, FIXED_VALUE__VALUE);
        createEReference(fixedValueEClass, FIXED_VALUE__VALUE_EXP);

        arrayEClass = createEClass(ARRAY);
        createEAttribute(arrayEClass, ARRAY__VALUES);
        createEReference(arrayEClass, ARRAY__VALUES_EXP);

        triangularDistributionEClass = createEClass(TRIANGULAR_DISTRIBUTION);
        createEAttribute(triangularDistributionEClass, TRIANGULAR_DISTRIBUTION__MODE);
        createEAttribute(triangularDistributionEClass, TRIANGULAR_DISTRIBUTION__MIN);
        createEAttribute(triangularDistributionEClass, TRIANGULAR_DISTRIBUTION__MAX);
        createEReference(triangularDistributionEClass, TRIANGULAR_DISTRIBUTION__MODE_EXP);
        createEReference(triangularDistributionEClass, TRIANGULAR_DISTRIBUTION__MIN_EXP);
        createEReference(triangularDistributionEClass, TRIANGULAR_DISTRIBUTION__MAX_EXP);

        distributionEClass = createEClass(DISTRIBUTION);
        createEAttribute(distributionEClass, DISTRIBUTION__DEFAULT);
        createEReference(distributionEClass, DISTRIBUTION__DEFAULT_EXP);

        pertDistributionEClass = createEClass(PERT_DISTRIBUTION);
        createEAttribute(pertDistributionEClass, PERT_DISTRIBUTION__MODE);
        createEAttribute(pertDistributionEClass, PERT_DISTRIBUTION__MIN);
        createEAttribute(pertDistributionEClass, PERT_DISTRIBUTION__MAX);
        createEReference(pertDistributionEClass, PERT_DISTRIBUTION__MODE_EXP);
        createEReference(pertDistributionEClass, PERT_DISTRIBUTION__MIN_EXP);
        createEReference(pertDistributionEClass, PERT_DISTRIBUTION__MAX_EXP);
        createEAttribute(pertDistributionEClass, PERT_DISTRIBUTION__GAMMA);
        createEReference(pertDistributionEClass, PERT_DISTRIBUTION__GAMMA_EXP);

        normalDistributionEClass = createEClass(NORMAL_DISTRIBUTION);
        createEAttribute(normalDistributionEClass, NORMAL_DISTRIBUTION__MEAN);
        createEAttribute(normalDistributionEClass, NORMAL_DISTRIBUTION__SD);
        createEReference(normalDistributionEClass, NORMAL_DISTRIBUTION__MEAN_EXP);
        createEReference(normalDistributionEClass, NORMAL_DISTRIBUTION__SD_EXP);

        enumeratedDistributionEClass = createEClass(ENUMERATED_DISTRIBUTION);
        createEAttribute(enumeratedDistributionEClass, ENUMERATED_DISTRIBUTION__VALUES);
        createEReference(enumeratedDistributionEClass, ENUMERATED_DISTRIBUTION__VALUES_EXP);

        // Create enums
        calculationModeEEnum = createEEnum(CALCULATION_MODE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isInitialized = false;

	/**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        ExpressionsPackage theExpressionsPackage = (ExpressionsPackage)EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        fixedValueEClass.getESuperTypes().add(this.getTiming());
        arrayEClass.getESuperTypes().add(this.getTiming());
        triangularDistributionEClass.getESuperTypes().add(this.getDistribution());
        distributionEClass.getESuperTypes().add(this.getTiming());
        pertDistributionEClass.getESuperTypes().add(this.getDistribution());
        normalDistributionEClass.getESuperTypes().add(this.getDistribution());
        enumeratedDistributionEClass.getESuperTypes().add(this.getDistribution());

        // Initialize classes, features, and operations; add parameters
        initEClass(timingEClass, Timing.class, "Timing", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(fixedValueEClass, FixedValue.class, "FixedValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getFixedValue_Value(), ecorePackage.getEBigDecimal(), "value", null, 0, 1, FixedValue.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getFixedValue_ValueExp(), theExpressionsPackage.getExpression(), null, "valueExp", null, 1, 1, FixedValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(arrayEClass, Array.class, "Array", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getArray_Values(), ecorePackage.getEBigDecimal(), "values", null, 1, -1, Array.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getArray_ValuesExp(), theExpressionsPackage.getExpression(), null, "valuesExp", null, 1, -1, Array.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(triangularDistributionEClass, TriangularDistribution.class, "TriangularDistribution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTriangularDistribution_Mode(), ecorePackage.getEBigDecimal(), "mode", null, 1, 1, TriangularDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEAttribute(getTriangularDistribution_Min(), ecorePackage.getEBigDecimal(), "min", null, 1, 1, TriangularDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEAttribute(getTriangularDistribution_Max(), ecorePackage.getEBigDecimal(), "max", null, 1, 1, TriangularDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getTriangularDistribution_ModeExp(), theExpressionsPackage.getExpression(), null, "modeExp", null, 1, 1, TriangularDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTriangularDistribution_MinExp(), theExpressionsPackage.getExpression(), null, "minExp", null, 1, 1, TriangularDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTriangularDistribution_MaxExp(), theExpressionsPackage.getExpression(), null, "maxExp", null, 1, 1, TriangularDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(distributionEClass, Distribution.class, "Distribution", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDistribution_Default(), ecorePackage.getEBigDecimal(), "default", null, 0, 1, Distribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getDistribution_DefaultExp(), theExpressionsPackage.getExpression(), null, "defaultExp", null, 0, 1, Distribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(pertDistributionEClass, PertDistribution.class, "PertDistribution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPertDistribution_Mode(), ecorePackage.getEBigDecimal(), "mode", null, 1, 1, PertDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEAttribute(getPertDistribution_Min(), ecorePackage.getEBigDecimal(), "min", null, 1, 1, PertDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEAttribute(getPertDistribution_Max(), ecorePackage.getEBigDecimal(), "max", null, 1, 1, PertDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getPertDistribution_ModeExp(), theExpressionsPackage.getExpression(), null, "modeExp", null, 1, 1, PertDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPertDistribution_MinExp(), theExpressionsPackage.getExpression(), null, "minExp", null, 1, 1, PertDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPertDistribution_MaxExp(), theExpressionsPackage.getExpression(), null, "maxExp", null, 1, 1, PertDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPertDistribution_Gamma(), ecorePackage.getEBigDecimal(), "gamma", null, 1, 1, PertDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getPertDistribution_GammaExp(), theExpressionsPackage.getExpression(), null, "gammaExp", null, 1, 1, PertDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(normalDistributionEClass, NormalDistribution.class, "NormalDistribution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getNormalDistribution_Mean(), ecorePackage.getEBigDecimal(), "mean", null, 1, 1, NormalDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEAttribute(getNormalDistribution_Sd(), ecorePackage.getEBigDecimal(), "sd", null, 1, 1, NormalDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getNormalDistribution_MeanExp(), theExpressionsPackage.getExpression(), null, "meanExp", null, 1, 1, NormalDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getNormalDistribution_SdExp(), theExpressionsPackage.getExpression(), null, "sdExp", null, 1, 1, NormalDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(enumeratedDistributionEClass, EnumeratedDistribution.class, "EnumeratedDistribution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getEnumeratedDistribution_Values(), ecorePackage.getEBigDecimal(), "values", null, 1, -1, EnumeratedDistribution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getEnumeratedDistribution_ValuesExp(), theExpressionsPackage.getExpression(), null, "valuesExp", null, 1, -1, EnumeratedDistribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        // Initialize enums and add enum literals
        initEEnum(calculationModeEEnum, CalculationMode.class, "CalculationMode");
        addEEnumLiteral(calculationModeEEnum, CalculationMode.MEAN);
        addEEnumLiteral(calculationModeEEnum, CalculationMode.DISTRIBUTED);
        addEEnumLiteral(calculationModeEEnum, CalculationMode.LINEAIR);

        // Create resource
        createResource(eNS_URI);
    }

} //TimingPackageImpl
