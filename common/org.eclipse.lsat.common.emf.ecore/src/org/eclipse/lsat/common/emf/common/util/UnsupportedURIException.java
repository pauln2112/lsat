/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.util;

public class UnsupportedURIException extends RuntimeException {
    private static final long serialVersionUID = -143988347909754139L;

    public UnsupportedURIException() {
        super();
    }

    public UnsupportedURIException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedURIException(String message) {
        super(message);
    }

    public UnsupportedURIException(Throwable cause) {
        super(cause);
    }
}
