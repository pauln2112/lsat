/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.layout;

import java.util.EnumSet;
import org.eclipse.elk.core.data.ILayoutMetaDataProvider;
import org.eclipse.elk.graph.properties.IProperty;
import org.eclipse.elk.graph.properties.Property;
import org.eclipse.emf.ecore.EClass;

@SuppressWarnings("all")
public class ActivityDiagramLayoutMetaDataProvider implements ILayoutMetaDataProvider {
  /**
   * Default value for {@link #E_CLASS}.
   */
  private static final EClass E_CLASS_DEFAULT = null;
  
  /**
   * The eClass of the node
   */
  public static final IProperty<EClass> E_CLASS = new Property<EClass>(
            "org.eclipse.lsat.activity.diagram.layout.eClass",
            E_CLASS_DEFAULT,
            null,
            null);
  
  /**
   * Default value for {@link #EVENT_NAME}.
   */
  private static final String EVENT_NAME_DEFAULT = "";
  
  /**
   * name of the event
   */
  public static final IProperty<String> EVENT_NAME = new Property<String>(
            "org.eclipse.lsat.activity.diagram.layout.eventName",
            EVENT_NAME_DEFAULT,
            null,
            null);
  
  /**
   * Default value for {@link #NAME}.
   */
  private static final String NAME_DEFAULT = "";
  
  /**
   * Totally irrelevant for the algorithm, but it makes life a lot easier while debugging
   */
  public static final IProperty<String> NAME = new Property<String>(
            "org.eclipse.lsat.activity.diagram.layout.name",
            NAME_DEFAULT,
            null,
            null);
  
  /**
   * Default value for {@link #LAYOUT_ROW}.
   */
  private static final Integer LAYOUT_ROW_DEFAULT = Integer.valueOf(1);
  
  /**
   * Abstract row of the node in the final, global layout, determined by topological order and node type
   */
  public static final IProperty<Integer> LAYOUT_ROW = new Property<Integer>(
            "org.eclipse.lsat.activity.diagram.layout.layoutRow",
            LAYOUT_ROW_DEFAULT,
            null,
            null);
  
  public void apply(final org.eclipse.elk.core.data.ILayoutMetaDataProvider.Registry registry) {
    registry.register(new org.eclipse.elk.core.data.LayoutOptionData(
        "org.eclipse.lsat.activity.diagram.layout.eClass",
        "",
        "eClass",
        "The eClass of the node",
        E_CLASS_DEFAULT,
        null,
        null,
        org.eclipse.elk.core.data.LayoutOptionData.Type.OBJECT,
        EClass.class,
        EnumSet.of(org.eclipse.elk.core.data.LayoutOptionData.Target.NODES),
        org.eclipse.elk.core.data.LayoutOptionData.Visibility.HIDDEN
    ));
    registry.register(new org.eclipse.elk.core.data.LayoutOptionData(
        "org.eclipse.lsat.activity.diagram.layout.eventName",
        "",
        "Event Name",
        "name of the event",
        EVENT_NAME_DEFAULT,
        null,
        null,
        org.eclipse.elk.core.data.LayoutOptionData.Type.STRING,
        String.class,
        EnumSet.of(org.eclipse.elk.core.data.LayoutOptionData.Target.NODES),
        org.eclipse.elk.core.data.LayoutOptionData.Visibility.HIDDEN
    ));
    registry.register(new org.eclipse.elk.core.data.LayoutOptionData(
        "org.eclipse.lsat.activity.diagram.layout.name",
        "",
        "Node Name",
        "Totally irrelevant for the algorithm, but it makes life a lot easier while debugging",
        NAME_DEFAULT,
        null,
        null,
        org.eclipse.elk.core.data.LayoutOptionData.Type.STRING,
        String.class,
        EnumSet.of(org.eclipse.elk.core.data.LayoutOptionData.Target.NODES),
        org.eclipse.elk.core.data.LayoutOptionData.Visibility.HIDDEN
    ));
    registry.register(new org.eclipse.elk.core.data.LayoutOptionData(
        "org.eclipse.lsat.activity.diagram.layout.layoutRow",
        "",
        "Row in the layout",
        "Abstract row of the node in the final, global layout, determined by topological order and node type",
        LAYOUT_ROW_DEFAULT,
        null,
        null,
        org.eclipse.elk.core.data.LayoutOptionData.Type.INT,
        Integer.class,
        EnumSet.of(org.eclipse.elk.core.data.LayoutOptionData.Target.NODES),
        org.eclipse.elk.core.data.LayoutOptionData.Visibility.HIDDEN
    ));
    registry.register(new org.eclipse.elk.core.data.LayoutCategoryData(
        "org.eclipse.lsat.activity.diagram.layout.LSAT",
        "LSAT",
        "Layout algorithms dedicated to the LSAT tooling"
    ));
    new org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayoutOptions().apply(registry);
  }
}
