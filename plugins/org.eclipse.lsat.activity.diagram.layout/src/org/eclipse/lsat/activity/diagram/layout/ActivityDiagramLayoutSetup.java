/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.layout;

import org.eclipse.elk.core.service.IDiagramLayoutConnector;
import org.eclipse.elk.core.service.ILayoutConfigurationStore;
import org.eclipse.elk.core.service.ILayoutSetup;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.util.Modules;

public class ActivityDiagramLayoutSetup implements ILayoutSetup {
    @Override
    public boolean supports(Object object) {
        return true;
    }

    @Override
    public Injector createInjector(Module defaultModule) {
        // Modules basically provide a mapping between types and implementations
        // to instantiate whenever an instance of the type is requested. We use
        // the default module supplied by ELK and override that with custom
        // overrides to get our IDiagramLayoutConnector to enter the picture.
        return Guice.createInjector(Modules.override(defaultModule).with(new ActivityDiagramLayoutModule()));
    }

    public static class ActivityDiagramLayoutModule implements Module {
        @Override
        public void configure(final Binder binder) {
            // This is the most important binding
            binder.bind(IDiagramLayoutConnector.class).to(ActivityDiagramLayoutConnector.class);
            binder.bind(ILayoutConfigurationStore.Provider.class).to(ActivityDiagramConfigurationStore.Provider.class);
        }
    }
}
