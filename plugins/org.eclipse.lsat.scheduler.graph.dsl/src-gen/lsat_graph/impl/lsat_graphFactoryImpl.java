/**
 */
package lsat_graph.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.lsat.common.scheduler.graph.Task;
import lsat_graph.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class lsat_graphFactoryImpl extends EFactoryImpl implements lsat_graphFactory {
	/**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static lsat_graphFactory init() {
        try
        {
            lsat_graphFactory thelsat_graphFactory = (lsat_graphFactory)EPackage.Registry.INSTANCE.getEFactory(lsat_graphPackage.eNS_URI);
            if (thelsat_graphFactory != null)
            {
                return thelsat_graphFactory;
            }
        }
        catch (Exception exception)
        {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new lsat_graphFactoryImpl();
    }

	/**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public lsat_graphFactoryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject create(EClass eClass) {
        switch (eClass.getClassifierID())
        {
            case lsat_graphPackage.PERIPHERAL_ACTION_TASK: return createPeripheralActionTask();
            case lsat_graphPackage.CLAIM_TASK: return createClaimTask();
            case lsat_graphPackage.RELEASE_TASK: return createReleaseTask();
            case lsat_graphPackage.CLAIM_RELEASE_RESOURCE: return createClaimReleaseResource();
            case lsat_graphPackage.DISPATCH_GROUP_TASK: return createDispatchGroupTask();
            case lsat_graphPackage.DISPATCH_GROUP_RESOURCE: return createDispatchGroupResource();
            case lsat_graphPackage.PERIPHERAL_RESOURCE: return createPeripheralResource();
            case lsat_graphPackage.DISPATCH_GRAPH: return createDispatchGraph();
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK: return createClaimedByScheduledTask();
            case lsat_graphPackage.STOCHASTIC_ANNOTATION: return createStochasticAnnotation();
            case lsat_graphPackage.EVENT_STATUS_TASK: return createEventStatusTask();
            case lsat_graphPackage.EVENT_ANNOTATION: return createEventAnnotation();
            case lsat_graphPackage.REQUIRE_TASK: return createRequireTask();
            case lsat_graphPackage.RAISE_TASK: return createRaiseTask();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PeripheralActionTask createPeripheralActionTask() {
        PeripheralActionTaskImpl peripheralActionTask = new PeripheralActionTaskImpl();
        return peripheralActionTask;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ClaimTask createClaimTask() {
        ClaimTaskImpl claimTask = new ClaimTaskImpl();
        return claimTask;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ReleaseTask createReleaseTask() {
        ReleaseTaskImpl releaseTask = new ReleaseTaskImpl();
        return releaseTask;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ClaimReleaseResource createClaimReleaseResource() {
        ClaimReleaseResourceImpl claimReleaseResource = new ClaimReleaseResourceImpl();
        return claimReleaseResource;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public DispatchGroupTask createDispatchGroupTask() {
        DispatchGroupTaskImpl dispatchGroupTask = new DispatchGroupTaskImpl();
        return dispatchGroupTask;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public DispatchGroupResource createDispatchGroupResource() {
        DispatchGroupResourceImpl dispatchGroupResource = new DispatchGroupResourceImpl();
        return dispatchGroupResource;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PeripheralResource createPeripheralResource() {
        PeripheralResourceImpl peripheralResource = new PeripheralResourceImpl();
        return peripheralResource;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public DispatchGraph createDispatchGraph() {
        DispatchGraphImpl dispatchGraph = new DispatchGraphImpl();
        return dispatchGraph;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ClaimedByScheduledTask createClaimedByScheduledTask() {
        ClaimedByScheduledTaskImpl claimedByScheduledTask = new ClaimedByScheduledTaskImpl();
        return claimedByScheduledTask;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public StochasticAnnotation createStochasticAnnotation() {
        StochasticAnnotationImpl stochasticAnnotation = new StochasticAnnotationImpl();
        return stochasticAnnotation;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EventStatusTask createEventStatusTask() {
        EventStatusTaskImpl eventStatusTask = new EventStatusTaskImpl();
        return eventStatusTask;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public <T extends Task> EventAnnotation<T> createEventAnnotation()
    {
        EventAnnotationImpl<T> eventAnnotation = new EventAnnotationImpl<T>();
        return eventAnnotation;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RequireTask createRequireTask()
    {
        RequireTaskImpl requireTask = new RequireTaskImpl();
        return requireTask;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RaiseTask createRaiseTask()
    {
        RaiseTaskImpl raiseTask = new RaiseTaskImpl();
        return raiseTask;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public lsat_graphPackage getlsat_graphPackage() {
        return (lsat_graphPackage)getEPackage();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
	@Deprecated
	public static lsat_graphPackage getPackage() {
        return lsat_graphPackage.eINSTANCE;
    }

} //lsat_graphFactoryImpl
