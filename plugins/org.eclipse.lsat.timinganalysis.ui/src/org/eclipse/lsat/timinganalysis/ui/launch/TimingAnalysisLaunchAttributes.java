/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timinganalysis.ui.launch;

public interface TimingAnalysisLaunchAttributes {
    public static final String MODEL_IFILE = TimingAnalysisLaunchAttributes.class.getName() + ".model";

    public static final String MODEL_IFILE_DEFAULT = "";

    public static final String MACHINE_IFILE = TimingAnalysisLaunchAttributes.class.getName() + ".machine";

    public static final String MACHINE_IFILE_DEFAULT = "";

    public static final String SETTING_IFILE = TimingAnalysisLaunchAttributes.class.getName() + ".setting";

    public static final String SETTING_IFILE_DEFAULT = "";

    public static final String NO_GANTT_CHART = TimingAnalysisLaunchAttributes.class.getName() + ".no_gantt_chart";

    public static final boolean NO_GANTT_CHART_DEFAULT = false;

    public static final String GANTT_CHART = TimingAnalysisLaunchAttributes.class.getName() + ".plain_gantt_chart";

    public static final boolean GANTT_CHART_DEFAULT = true;

    public static final String CRITICAL_PATH = TimingAnalysisLaunchAttributes.class.getName()
            + ".critical_path_no_claims";

    public static final boolean CRITICAL_PATH_DEFAULT = false;

    public static final String STOCHASTIC_IMPACT = TimingAnalysisLaunchAttributes.class.getName()
            + ".stochastic_impact";

    public static final boolean STOCHASTIC_IMPACT_DEFAULT = false;

    public static final String REMOVE_CLAIMS_RELEASES_DEPENDENCIES = TimingAnalysisLaunchAttributes.class.getName()
            + ".remove_claims_releases_dependencies";

    public static final boolean REMOVE_CLAIMS_RELEASES_DEPENDENCIES_DEFAULT = false;

    public static final String PAPERSCRIPT_ANIMATION = TimingAnalysisLaunchAttributes.class.getName()
            + ".paperscript_animation";

    public static final boolean PAPERSCRIPT_ANIMATION_DEFAULT = false;
}
