/**
 */
package org.eclipse.lsat.common.mpt.impl;

import org.eclipse.lsat.common.mpt.MPS;
import org.eclipse.lsat.common.mpt.MPSConfiguration;
import org.eclipse.lsat.common.mpt.MPSTransition;
import org.eclipse.lsat.common.mpt.MPTPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MPS</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MPSImpl extends GraphImpl<MPSConfiguration, MPSTransition> implements MPS {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MPTPackage.Literals.MPS;
	}

} //MPSImpl
