/**
 */
package activity.impl;

import activity.ActivityPackage;
import activity.SyncBar;

import org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sync Bar</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SyncBarImpl extends NodeImpl implements SyncBar {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected SyncBarImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ActivityPackage.Literals.SYNC_BAR;
    }

} //SyncBarImpl
