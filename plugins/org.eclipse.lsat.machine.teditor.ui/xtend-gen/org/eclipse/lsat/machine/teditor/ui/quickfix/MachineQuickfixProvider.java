/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.teditor.ui.quickfix;

import machine.Peripheral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.machine.teditor.validation.MachineValidator;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification;
import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;

/**
 * Custom quickfixes.
 * 
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#quick-fixes
 */
@SuppressWarnings("all")
public class MachineQuickfixProvider extends DefaultQuickfixProvider {
  @Fix(MachineValidator.INVALID_MUTUAL_EXCLUSIONS)
  public void CreateMissingEntity(final Issue issue, final IssueResolutionAcceptor acceptor) {
    final ISemanticModification _function = (EObject element, IModificationContext context) -> {
      final Peripheral peripheral = ((Peripheral) element);
      peripheral.getDistances().clear();
    };
    acceptor.accept(issue, "Remove Distances", "Remove Distances", "", _function);
    final ISemanticModification _function_1 = (EObject element, IModificationContext context) -> {
      final Peripheral peripheral = ((Peripheral) element);
      peripheral.getAxisPositions().clear();
      peripheral.getPositions().clear();
      peripheral.getPaths().clear();
    };
    acceptor.accept(issue, "Remove Positions", "Remove Positions", "", _function_1);
  }
}
