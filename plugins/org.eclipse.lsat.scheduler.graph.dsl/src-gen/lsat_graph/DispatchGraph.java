/**
 */
package lsat_graph;

import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;

import dispatching.Dispatch;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dispatch Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.DispatchGraph#getDispatch <em>Dispatch</em>}</li>
 * </ul>
 *
 * @see lsat_graph.lsat_graphPackage#getDispatchGraph()
 * @model
 * @generated
 */
public interface DispatchGraph extends TaskDependencyGraph<Task> {
	/**
     * Returns the value of the '<em><b>Dispatch</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatch</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Dispatch</em>' reference.
     * @see #setDispatch(Dispatch)
     * @see lsat_graph.lsat_graphPackage#getDispatchGraph_Dispatch()
     * @model required="true"
     * @generated
     */
	Dispatch getDispatch();

	/**
     * Sets the value of the '{@link lsat_graph.DispatchGraph#getDispatch <em>Dispatch</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Dispatch</em>' reference.
     * @see #getDispatch()
     * @generated
     */
	void setDispatch(Dispatch value);

} // DispatchGraph
