/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.xtend.transformation;

import java.text.DecimalFormat;
import java.util.List;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class Mpt2Dispatching {
  private Mpt2Dispatching() {
  }
  
  public static String createSequence(final List<String> activitySequence, final String phase) {
    String _xifexpression = null;
    boolean _isEmpty = activitySequence.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("activities ");
      String _quote = Mpt2Dispatching.quote(phase);
      _builder.append(_quote);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      {
        for(final String activity : activitySequence) {
          _builder.append("    ");
          _builder.append(activity, "    ");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("}");
      _builder.newLine();
      _xifexpression = _builder.toString();
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("// ");
      _builder_1.append(phase);
      _builder_1.append(" not available");
      _builder_1.newLineIfNotEmpty();
      _xifexpression = _builder_1.toString();
    }
    return _xifexpression;
  }
  
  public static String round(final Double d) {
    final DecimalFormat df = new DecimalFormat("###.######");
    return df.format(d);
  }
  
  private static String quote(final String str) {
    String _xifexpression = null;
    boolean _matches = str.matches("\\p{Alpha}\\w*");
    if (_matches) {
      _xifexpression = str;
    } else {
      _xifexpression = (("\"" + str) + "\"");
    }
    return _xifexpression;
  }
}
