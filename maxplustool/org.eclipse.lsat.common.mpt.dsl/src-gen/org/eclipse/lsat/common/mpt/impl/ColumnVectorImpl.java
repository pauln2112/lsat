/**
 */
package org.eclipse.lsat.common.mpt.impl;

import org.eclipse.lsat.common.mpt.ColumnVector;
import org.eclipse.lsat.common.mpt.MPTPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Column Vector</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ColumnVectorImpl extends VectorImpl implements ColumnVector {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ColumnVectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MPTPackage.Literals.COLUMN_VECTOR;
	}

} //ColumnVectorImpl
