/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor.scoping;

import activity.LocationPrerequisite;
import activity.PeripheralAction;
import activity.ResourceAction;
import machine.IResource;
import machine.Peripheral;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it
 */
@SuppressWarnings("all")
public class ActivityScopeProvider extends AbstractDeclarativeScopeProvider {
  /**
   * Scoping for location prerequisites
   */
  public IScope scope_Peripheral(final LocationPrerequisite prerequisite, final EReference ref) {
    IResource _resource = prerequisite.getResource();
    boolean _tripleEquals = (null == _resource);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    final Function1<Peripheral, Boolean> _function = (Peripheral it) -> {
      boolean _isEmpty = it.getPositions().isEmpty();
      return Boolean.valueOf((!_isEmpty));
    };
    return Scopes.scopeFor(IterableExtensions.<Peripheral>filter(prerequisite.getResource().getResource().getPeripherals(), _function));
  }
  
  public IScope scope_SymbolicPosition(final LocationPrerequisite prerequisite, final EReference ref) {
    Peripheral _peripheral = prerequisite.getPeripheral();
    boolean _tripleEquals = (null == _peripheral);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(prerequisite.getPeripheral().getPositions());
  }
  
  /**
   * Scoping for node tree
   */
  public IScope scope_Node(final EditableDirectedGraph graph, final EReference ref) {
    EList<Node> _nodes = graph.getNodes();
    boolean _tripleEquals = (null == _nodes);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(graph.getNodes());
  }
  
  public IScope scope_Peripheral(final ResourceAction action, final EReference ref) {
    IResource _resource = action.getResource();
    boolean _tripleEquals = (null == _resource);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(action.getResource().getResource().getPeripherals());
  }
  
  public IScope scope_ActionType(final PeripheralAction action, final EReference ref) {
    Peripheral _peripheral = action.getPeripheral();
    boolean _tripleEquals = (null == _peripheral);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(action.getPeripheral().getType().getActions());
  }
  
  public IScope scope_SymbolicPosition(final PeripheralAction action, final EReference ref) {
    Peripheral _peripheral = action.getPeripheral();
    boolean _tripleEquals = (null == _peripheral);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(action.getPeripheral().getPositions());
  }
  
  public IScope scope_Distance(final PeripheralAction action, final EReference ref) {
    Peripheral _peripheral = action.getPeripheral();
    boolean _tripleEquals = (null == _peripheral);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(action.getPeripheral().getDistances());
  }
  
  public IScope scope_Profile(final PeripheralAction action, final EReference ref) {
    Peripheral _peripheral = action.getPeripheral();
    boolean _tripleEquals = (null == _peripheral);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(action.getPeripheral().getProfiles());
  }
}
