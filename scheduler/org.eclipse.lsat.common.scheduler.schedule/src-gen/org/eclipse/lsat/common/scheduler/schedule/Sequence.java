/**
 */
package org.eclipse.lsat.common.scheduler.schedule;

import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.resources.Resource;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getSchedule <em>Schedule</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getScheduledTasks <em>Scheduled Tasks</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getResource <em>Resource</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getSequence()
 * @model
 * @generated
 */
public interface Sequence<T extends Task> extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getSequence_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Schedule</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.scheduler.schedule.Schedule#getSequences <em>Sequences</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedule</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedule</em>' container reference.
	 * @see #setSchedule(Schedule)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getSequence_Schedule()
	 * @see org.eclipse.lsat.common.scheduler.schedule.Schedule#getSequences
	 * @model opposite="sequences" required="true" transient="false"
	 * @generated
	 */
	Schedule<T> getSchedule();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getSchedule <em>Schedule</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schedule</em>' container reference.
	 * @see #getSchedule()
	 * @generated
	 */
	void setSchedule(Schedule<T> value);

	/**
	 * Returns the value of the '<em><b>Scheduled Tasks</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask}<code>&lt;T&gt;</code>.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduled Tasks</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduled Tasks</em>' reference list.
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getSequence_ScheduledTasks()
	 * @see org.eclipse.lsat.common.scheduler.schedule.ScheduledTask#getSequence
	 * @model opposite="sequence"
	 * @generated
	 */
	EList<ScheduledTask<T>> getScheduledTasks();

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference.
	 * @see #setResource(Resource)
	 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getSequence_Resource()
	 * @model
	 * @generated
	 */
	Resource getResource();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.schedule.Sequence#getResource <em>Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource</em>' reference.
	 * @see #getResource()
	 * @generated
	 */
	void setResource(Resource value);

} // Sequence
