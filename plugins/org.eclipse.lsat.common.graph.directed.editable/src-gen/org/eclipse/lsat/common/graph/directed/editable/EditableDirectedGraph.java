/**
 */
package org.eclipse.lsat.common.graph.directed.editable;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Editable Directed Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getNodes <em>Nodes</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getEdges <em>Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEditableDirectedGraph()
 * @model
 * @generated
 */
public interface EditableDirectedGraph extends EObject {
	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.editable.Node}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEditableDirectedGraph_Nodes()
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getGraph
	 * @model opposite="graph" containment="true" keys="name"
	 * @generated
	 */
	EList<Node> getNodes();

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.editable.Edge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' containment reference list.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEditableDirectedGraph_Edges()
	 * @model containment="true"
	 * @generated
	 */
	EList<Edge> getEdges();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEditableDirectedGraph_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Returns all nodes of the graph in a topological order.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	EList<Node> allNodesInTopologicalOrder();

} // EditableDirectedGraph
