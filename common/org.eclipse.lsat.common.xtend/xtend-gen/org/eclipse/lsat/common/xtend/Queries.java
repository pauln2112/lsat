/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import org.eclipse.lsat.common.queries.IterableQueries;
import org.eclipse.lsat.common.util.BranchIterable;
import org.eclipse.lsat.common.util.BranchIterator;
import org.eclipse.lsat.common.util.IterableUtil;
import org.eclipse.lsat.common.util.LoggableIterable;
import org.eclipse.lsat.common.util.ProcessingIterator;
import org.eclipse.lsat.common.util.UniqueIterable;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Pair;

@SuppressWarnings("all")
public class Queries {
  /**
   * A helper method that returns the {@code element} as an {@link Iterable}
   * containing the {@code element} if the {@code element} if not {@code null},
   * else an empty {@link Iterable} is returned.
   */
  public static <T extends Object> Iterable<T> notNull(final T element) {
    Collection<T> _xifexpression = null;
    if ((element == null)) {
      _xifexpression = Collections.<T>emptyList();
    } else {
      _xifexpression = Collections.<T>singleton(element);
    }
    return _xifexpression;
  }
  
  /**
   * From a {@link BranchIterator} find the nearest node in each branch that matches the {@code predicate}
   */
  public static <T extends Object> Iterable<T> findNearest(final BranchIterable<T> source, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>findNearest(source, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * From a {@link BranchIterator} find the nearest node in each branch that is an instance of {@code type}
   */
  public static <T extends Object, R extends Object> Iterable<R> findNearest(final BranchIterable<T> source, final Class<R> type) {
    return IterableQueries.<T, R>findNearest(source, type);
  }
  
  /**
   * Returns the elements of each branch of a {@link BranchIterable} up to the
   * element that matches the {@code predicate}. The matched element itself is
   * excluded from the result.
   * 
   * @return an iterator containing all elements of {@code iterable} up to (thus
   *         excluding) the first element that matches the {@code predicate} per
   *         branch.
   */
  public static <T extends Object> BranchIterable<T> until(final BranchIterable<T> source, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>until(source, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Returns the elements of each branch of a {@link BranchIterable} up to the
   * element that matches the {@code predicate}. The matched element itself is
   * included in the result.
   * 
   * @return an iterator containing all elements of {@code iterable} up to and
   *         including the first element that matches the {@code predicate} per
   *         branch.
   */
  public static <T extends Object> BranchIterable<T> upToAndIncluding(final BranchIterable<T> source, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>upToAndIncluding(source, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Collects all children and children's children without the need for creating a temporary Collection instance.
   * Behaves like OCL <tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>)
   */
  public static <T extends Object> BranchIterable<T> closure(final Iterable<? extends T> source, final Function1<? super T, ? extends Iterable<? extends T>> transformation) {
    return IterableQueries.<T>closure(source, new Function<T, Iterable<? extends T>>() {
        public Iterable<? extends T> apply(T t) {
          return transformation.apply(t);
        }
    });
  }
  
  /**
   * Collects all children and children's children without the need for creating a temporary Collection instance.
   * Behaves like OCL: <ul>
   * <li><tt>!includeSource</tt>: <tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>)</li>
   * <li><tt>includeSource</tt>: <tt>source</tt>->union(<tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>))</li>
   * </ul>
   */
  public static <T extends Object> BranchIterable<T> closure(final Iterable<? extends T> source, final boolean includeSource, final Function1<? super T, ? extends Iterable<? extends T>> transformation) {
    return IterableQueries.<T>closure(source, includeSource, new Function<T, Iterable<? extends T>>() {
        public Iterable<? extends T> apply(T t) {
          return transformation.apply(t);
        }
    });
  }
  
  /**
   * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
   * <code>false</code>. All returned elements will pass the predicate. No need for creating a temporary Collection instance.<br>
   */
  public static <T extends Object> BranchIterable<T> closureWhile(final Iterable<? extends T> source, final Function1<? super T, ? extends Iterable<? extends T>> transformation, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>closureWhile(source, new Function<T, Iterable<? extends T>>() {
        public Iterable<? extends T> apply(T t) {
          return transformation.apply(t);
        }
    }, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
   * <code>false</code>. All returned elements will pass the predicate. No need for creating a temporary Collection instance.<br>
   */
  public static <T extends Object> BranchIterable<T> closureWhile(final Iterable<? extends T> source, final boolean includeSource, final Function1<? super T, ? extends Iterable<? extends T>> transformation, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>closureWhile(source, includeSource, new Function<T, Iterable<? extends T>>() {
        public Iterable<? extends T> apply(T t) {
          return transformation.apply(t);
        }
    }, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Collects child, child's child, etc without the need for creating a temporary Collection instance.
   * Behaves like OCL <tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>)
   */
  public static <T extends Object> BranchIterable<T> closureOne(final Iterable<? extends T> source, final Function1<? super T, ? extends T> transformation) {
    return IterableQueries.<T>closureOne(source, new Function<T, T>() {
        public T apply(T t) {
          return transformation.apply(t);
        }
    });
  }
  
  /**
   * Collects child, child's child, etc without the need for creating a temporary Collection instance.
   * Behaves like OCL: <ul>
   * <li><tt>!includeSource</tt>: <tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>)</li>
   * <li><tt>includeSource</tt>: <tt>source</tt>->union(<tt>source</tt>->closure(e | <tt>transformation.apply(e)</tt>))</li>
   * </ul>
   */
  public static <T extends Object> BranchIterable<T> closureOne(final Iterable<? extends T> source, final boolean includeSource, final Function1<? super T, ? extends T> transformation) {
    return IterableQueries.<T>closureOne(source, includeSource, new Function<T, T>() {
        public T apply(T t) {
          return transformation.apply(t);
        }
    });
  }
  
  /**
   * Collects all children and children's children and stops a sub branch as soon as the predicate evaluates to
   * <code>false</code>. All returned elements will pass the predicate. No need for creating a temporary Collection instance.<br>
   */
  public static <T extends Object> BranchIterable<T> closureOneWhile(final Iterable<? extends T> source, final Function1<? super T, ? extends T> transformation, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>closureOneWhile(source, new Function<T, T>() {
        public T apply(T t) {
          return transformation.apply(t);
        }
    }, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Collects all children and children's children and stops a sub branch as soon as the predicate evaluates to
   * <code>false</code>. All returned elements will pass the predicate. No need for creating a temporary Collection instance.<br>
   */
  public static <T extends Object> BranchIterable<T> closureOneWhile(final Iterable<? extends T> source, final boolean includeSource, final Function1<? super T, ? extends T> transformation, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>closureOneWhile(source, includeSource, new Function<T, T>() {
        public T apply(T t) {
          return transformation.apply(t);
        }
    }, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Collects all ancestors (parent and parent's parent) per element in <tt>iterable</tt> without the need for
   * creating a temporary Collection instance. Note that the resulting iterator might contain a tree node multiple
   * times if elements in <tt>iterable</tt> share this tree node as common ancestor.
   */
  public static <T extends Object> BranchIterable<T> climbTree(final Iterable<? extends T> source, final Function1<? super T, ? extends T> transformation) {
    return IterableQueries.<T>climbTree(source, new Function<T, T>() {
        public T apply(T t) {
          return transformation.apply(t);
        }
    });
  }
  
  /**
   * Collects self (only if <tt>includeSelf</tt> is <code>true</code>), parent and
   * parent's parents per element in this iterable without the need for creating a
   * temporary Collection instance. Note that the resulting iterator might contain
   * a tree node multiple times if elements in this iterable share this tree node
   * as common ancestor. You can use {@link #unique()} to filter these duplicates.
   */
  public static <T extends Object> BranchIterable<T> climbTree(final Iterable<? extends T> source, final boolean includeSelf, final Function1<? super T, ? extends T> transformation) {
    return IterableQueries.<T>climbTree(source, includeSelf, new Function<T, T>() {
        public T apply(T t) {
          return transformation.apply(t);
        }
    });
  }
  
  /**
   * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> and stops each sub branch as
   * soon as the predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance. Note that the
   * resulting iterator might contain a tree node multiple times if elements in <tt>iterator</tt> share this tree node
   * as common ancestor.
   */
  public static <T extends Object> BranchIterable<T> climbTreeWhile(final Iterable<? extends T> source, final Function1<? super T, ? extends T> transformation, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>climbTreeWhile(source, new Function<T, T>() {
        public T apply(T t) {
          return transformation.apply(t);
        }
    }, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Collects all ancestors (parent and parent's parent) per element in <tt>iterator</tt> and stops each sub branch as
   * soon as the predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance. Note that the
   * resulting iterator might contain a tree node multiple times if elements in <tt>iterator</tt> share this tree node
   * as common ancestor.
   */
  public static <T extends Object> BranchIterable<T> climbTreeWhile(final Iterable<? extends T> source, final boolean includeSelf, final Function1<? super T, ? extends T> transformation, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>climbTreeWhile(source, includeSelf, new Function<T, T>() {
        public T apply(T t) {
          return transformation.apply(t);
        }
    }, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Collects all descendants (children and children's children; depth first) without the need for creating a
   * temporary Collection instance.
   */
  public static <T extends Object> BranchIterable<T> walkTree(final Iterable<? extends T> source, final Function1<? super T, ? extends Iterable<? extends T>> transformation) {
    return IterableQueries.<T>walkTree(source, new Function<T, Iterable<? extends T>>() {
        public Iterable<? extends T> apply(T t) {
          return transformation.apply(t);
        }
    });
  }
  
  /**
   * Collects self (only if <tt>includeSelf</tt> is <code>true</code>), all
   * children and children's children - depth first - without the need for
   * creating a temporary Collection instance.
   */
  public static <T extends Object> BranchIterable<T> walkTree(final Iterable<? extends T> source, final boolean includeSelf, final Function1<? super T, ? extends Iterable<? extends T>> transformation) {
    return IterableQueries.<T>walkTree(source, includeSelf, new Function<T, Iterable<? extends T>>() {
        public Iterable<? extends T> apply(T t) {
          return transformation.apply(t);
        }
    });
  }
  
  /**
   * Collects all descendants (children and children's children; depth first) and stops each sub branch as soon as the
   * predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance.
   */
  public static <T extends Object> BranchIterable<T> walkTreeWhile(final Iterable<? extends T> source, final Function1<? super T, ? extends Iterable<? extends T>> transformation, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>walkTreeWhile(source, new Function<T, Iterable<? extends T>>() {
        public Iterable<? extends T> apply(T t) {
          return transformation.apply(t);
        }
    }, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Collects all descendants (children and children's children; depth first) and stops each sub branch as soon as the
   * predicate evaluates to <code>false</code>. No need for creating a temporary Collection instance.
   */
  public static <T extends Object> BranchIterable<T> walkTreeWhile(final Iterable<? extends T> source, final boolean includeSelf, final Function1<? super T, ? extends Iterable<? extends T>> transformation, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>walkTreeWhile(source, includeSelf, new Function<T, Iterable<? extends T>>() {
        public Iterable<? extends T> apply(T t) {
          return transformation.apply(t);
        }
    }, new Predicate<T>() {
        public boolean test(T t) {
          return predicate.apply(t);
        }
    });
  }
  
  /**
   * Selects elements without the need for creating a temporary Collection instance.
   * Behaves like OCL <tt>source</tt>->select(e | predicate.apply(e))
   */
  public static <T extends Object> Iterable<T> select(final Iterable<T> source, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableExtensions.<T>filter(source, ((Function1<? super T, Boolean>)predicate));
  }
  
  /**
   * Selects elements without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
   * Behaves like OCL <tt>source</tt>->xselect(e | predicate.apply(e))
   */
  public static <T extends Object> Iterable<T> xselect(final Iterable<T> source, final Function1<? super T, ? extends Boolean> predicate) {
    return IterableExtensions.<T>filter(IterableExtensions.<T>filterNull(source), ((Function1<? super T, Boolean>)predicate));
  }
  
  /**
   * Collects single attribute values without the need for creating a temporary Collection instance.
   * Behaves like OCL <tt>source</tt>->collect(e | transformation.apply(e))
   */
  public static <T extends Object, R extends Object> Iterable<R> collectOne(final Iterable<T> source, final Function1<? super T, ? extends R> transformation) {
    return IterableExtensions.<T, R>map(source, transformation);
  }
  
  /**
   * Collects single attribute values without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
   * Behaves like OCL <tt>source</tt>->xcollect(e | transformation.apply(e))
   */
  public static <T extends Object, R extends Object> Iterable<R> xcollectOne(final Iterable<T> source, final Function1<? super T, ? extends R> transformation) {
    return IterableExtensions.<R>filterNull(IterableExtensions.<T, R>map(source, transformation));
  }
  
  /**
   * Collects elements without the need for creating a temporary Collection instance.
   * Behaves like OCL <tt>source</tt>->collect(e | transformation.apply(e))
   */
  public static <T extends Object, R extends Object> Iterable<R> collect(final Iterable<T> source, final Function1<? super T, ? extends Iterable<? extends R>> transformation) {
    return Iterables.<R>concat(IterableExtensions.<T, Iterable<? extends R>>map(source, transformation));
  }
  
  /**
   * Collects elements without the need for creating a temporary Collection instance, skipping <tt>null</tt> values.
   * Behaves like OCL <tt>source</tt>->xcollect(e | transformation.apply(e))
   */
  public static <T extends Object, R extends Object> Iterable<R> xcollect(final Iterable<T> source, final Function1<? super T, ? extends Iterable<? extends R>> transformation) {
    return IterableExtensions.<R>filterNull(Iterables.<R>concat(IterableExtensions.<T, Iterable<? extends R>>map(source, transformation)));
  }
  
  /**
   * Filters the iteration, only keeping items that are instance of the
   * specified type. <br>
   * Behaves like OCL <tt>source</tt>->objectsOfKind(<tt>Output</tt>)
   */
  public static <T extends Object> Iterable<T> objectsOfKind(final Iterable<?> source, final Class<T> type) {
    return Iterables.<T>filter(source, type);
  }
  
  /**
   * Casts this iteration to a specific type. <br>
   * Behaves like OCL <tt>source</tt>->oclAsType(<tt>Output</tt>)
   */
  public static <T extends Object> Iterable<T> asType(final Iterable<?> source, final Class<T> type) {
    final Function1<Object, T> _function = (Object it) -> {
      return ((T) it);
    };
    return IterableExtensions.map(source, _function);
  }
  
  /**
   * Behaves like OCL <tt>source</tt>->union(<tt>other</tt>)
   */
  public static <T extends Object> Iterable<T> union(final Iterable<? extends T> source, final Iterable<? extends T> other) {
    return Iterables.<T>concat(Arrays.<Iterable<? extends T>>asList(source, other));
  }
  
  /**
   * Behaves like OCL <tt>source</tt>->union(<tt>other</tt>)
   */
  public static <T extends Object> Iterable<T> union(final Iterable<? extends T> source, final T... other) {
    return Iterables.<T>concat(Arrays.<Iterable<? extends T>>asList(source, Arrays.<T>asList(other)));
  }
  
  /**
   * Behaves like OCL <tt>this</tt>->excluding(<tt>other</tt>)
   */
  public static <T extends Object> Iterable<T> excluding(final Iterable<T> iterable, final Iterable<?> excludes) {
    final Function1<T, Boolean> _function = (T it) -> {
      return Boolean.valueOf(Queries.includes(excludes, it));
    };
    return IterableExtensions.<T>reject(iterable, _function);
  }
  
  /**
   * Behaves like OCL <tt>this</tt>->excluding(<tt>other</tt>)
   */
  @SafeVarargs
  public static <T extends Object> Iterable<T> excluding(final Iterable<T> iterable, final Object... excludes) {
    return Queries.<T>excluding(iterable, IterableUtil.<Object>asList(excludes));
  }
  
  public static <T extends Object> Iterable<T> unique(final Iterable<T> source) {
    return Queries.<T>unique(source, true);
  }
  
  public static <T extends Object> Iterable<T> unique(final Iterable<T> source, final boolean useEquals) {
    return new UniqueIterable<T>(source, useEquals);
  }
  
  public static <L extends Object, R extends Object> Iterable<Pair<L, R>> product(final Iterable<L> left, final Iterable<R> right) {
    final Function1<L, Iterable<Pair<L, R>>> _function = (L l) -> {
      final Function1<R, Pair<L, R>> _function_1 = (R r) -> {
        return Pair.<L, R>of(l, r);
      };
      return IterableExtensions.<R, Pair<L, R>>map(right, _function_1);
    };
    return Iterables.<Pair<L, R>>concat(IterableExtensions.<L, Iterable<Pair<L, R>>>map(left, _function));
  }
  
  public static <T extends Object> Iterable<Pair<T, T>> product(final Iterable<T> source) {
    final Function1<T, Iterable<Pair<T, T>>> _function = (T l) -> {
      final Function1<T, Boolean> _function_1 = (T it) -> {
        return Boolean.valueOf((it == l));
      };
      final Function1<T, Pair<T, T>> _function_2 = (T r) -> {
        return Pair.<T, T>of(l, r);
      };
      return IterableExtensions.<T, Pair<T, T>>map(Queries.<T>after(source, _function_1), _function_2);
    };
    return Iterables.<Pair<T, T>>concat(IterableExtensions.<T, Iterable<Pair<T, T>>>map(source, _function));
  }
  
  public static <T extends Object> boolean includes(final Iterable<T> source, final Object item) {
    return Iterables.contains(source, item);
  }
  
  public static <T extends Object> boolean excludes(final Iterable<T> source, final Object item) {
    boolean _contains = Iterables.contains(source, item);
    return (!_contains);
  }
  
  /**
   * Checks for duplicates.
   * 
   * @param source
   *            the element to check for duplicates.
   * @return returns true if there are duplicates in the element.
   */
  public static boolean containsDuplicates(final Iterable<?> source) {
    final HashSet<Object> set = CollectionLiterals.<Object>newHashSet();
    final Function1<Object, Boolean> _function = (Object e) -> {
      return Boolean.valueOf(set.add(e));
    };
    boolean _forall = IterableExtensions.forall(source, _function);
    return (!_forall);
  }
  
  /**
   * Returns the maximum element of the given iterable, according to the
   * <i>natural ordering</i> of its elements. All elements in the iterable
   * must implement the <tt>Comparable</tt> interface. Furthermore, all
   * elements in the iterable must be <i>mutually comparable</i> (that is,
   * <tt>e1.compareTo(e2)</tt> must not throw a <tt>ClassCastException</tt>
   * for any elements <tt>e1</tt> and <tt>e2</tt> in the iterable).
   * <p>
   * 
   * This method iterates over the entire iterable, hence it requires time
   * proportional to the size of the iterable.
   * 
   * @param iterable
   *            the iterable whose maximum element is to be determined.
   * @param _default
   *            result if the collection is empty.
   * @return the maximum element of the given iterable, according to the
   *         <i>natural ordering</i> of its elements, or <tt>_default</tt> if
   *         the iterable is empty.
   * @see Comparable
   */
  public static <T extends Comparable<? super T>> T max(final Iterable<T> source, final T _default) {
    T _xifexpression = null;
    boolean _isEmpty = IterableExtensions.isEmpty(source);
    if (_isEmpty) {
      _xifexpression = _default;
    } else {
      _xifexpression = IterableExtensions.<T>max(source);
    }
    return _xifexpression;
  }
  
  /**
   * Returns the minimum element of the given iterable, according to the
   * <i>natural ordering</i> of its elements. All elements in the iterable
   * must implement the <tt>Comparable</tt> interface. Furthermore, all
   * elements in the iterable must be <i>mutually comparable</i> (that is,
   * <tt>e1.compareTo(e2)</tt> must not throw a <tt>ClassCastException</tt>
   * for any elements <tt>e1</tt> and <tt>e2</tt> in the iterable).
   * <p>
   * 
   * This method iterates over the entire iterable, hence it requires time
   * proportional to the size of the iterable.
   * 
   * @param iterable
   *            the iterable whose minimum element is to be determined.
   * @param _default
   *            result if the collection is empty.
   * @return the minimum element of the given iterable, according to the
   *         <i>natural ordering</i> of its elements, or <tt>_default</tt> if
   *         the iterable is empty.
   * @see Comparable
   */
  public static <T extends Comparable<? super T>> T min(final Iterable<T> source, final T _default) {
    T _xifexpression = null;
    boolean _isEmpty = IterableExtensions.isEmpty(source);
    if (_isEmpty) {
      _xifexpression = _default;
    } else {
      _xifexpression = IterableExtensions.<T>min(source);
    }
    return _xifexpression;
  }
  
  /**
   * Returns an Iterable containing all elements starting from the first element for which the predicate returned
   * true. The resulting Iterable is a lazily computed view, so any modifications to the underlying Iterables will be
   * reflected on subsequent iterations. The result's Iterator does not support {@link Iterator#remove()}
   * 
   * @param iterable
   *            the elements from which to drop. May not be <code>null</code>.
   * @param predicate
   *            the predicate which decides the start of the resulting iterable. May not be <code>null</code>.
   * @return the remaining elements after start
   */
  public static <T extends Object> Iterable<T> after(final Iterable<T> source, final Function1<? super T, ? extends Boolean> predicate) {
    final Function1<T, Boolean> _function = (T it) -> {
      Boolean _apply = predicate.apply(it);
      return Boolean.valueOf((!(_apply).booleanValue()));
    };
    return IterableExtensions.<T>drop(IterableExtensions.<T>dropWhile(source, _function), 1);
  }
  
  /**
   * Returns an Iterable containing all elements starting from the head of the source up to and excluding the first
   * element that matches the predicate The resulting Iterable is a lazily computed view, so any modifications to the
   * underlying Iterables will be reflected on subsequent iterations. The result's Iterator does not support
   * {@link Iterator#remove()}
   * 
   * @param source
   *            the elements from which to take. May not be <code>null</code>.
   * @param predicate
   *            the predicate which decides whether to keep taking elements. May not be <code>null</code>.
   * @return the taken elements
   */
  public static <T extends Object> Iterable<T> before(final Iterable<T> source, final Function1<? super T, ? extends Boolean> predicate) {
    final Function1<T, Boolean> _function = (T it) -> {
      Boolean _apply = predicate.apply(it);
      return Boolean.valueOf((!(_apply).booleanValue()));
    };
    return IterableExtensions.<T>takeWhile(source, _function);
  }
  
  public static <T extends Object> Iterable<T> symmetricHead(final Iterable<T> left, final Iterable<?> right) {
    return new LoggableIterable<T>() {
      @Override
      public Iterator<T> iterator() {
        abstract class ____Queries_1 extends ProcessingIterator<T> {
          Iterator<T> iLeft;
          
          Iterator<?> iRight;
        }
        
        return new ____Queries_1() {
          {
            iLeft = left.iterator();
            
            iRight = right.iterator();
          }
          @Override
          protected boolean toNext() {
            if (((!this.iLeft.hasNext()) || (!this.iRight.hasNext()))) {
              return this.done();
            }
            final T lNext = this.iLeft.next();
            final Object rNext = this.iRight.next();
            boolean _xifexpression = false;
            boolean _equals = Objects.equal(lNext, rNext);
            if (_equals) {
              _xifexpression = this.setNext(lNext);
            } else {
              _xifexpression = this.done();
            }
            return _xifexpression;
          }
        };
      }
    };
  }
  
  public static <T extends Object> Iterable<T> symmetricHead(final Iterable<T> left, final Iterable<?> right, final boolean useEquals) {
    if (useEquals) {
      return Queries.<T>symmetricHead(left, right);
    }
    return new LoggableIterable<T>() {
      @Override
      public Iterator<T> iterator() {
        abstract class ____Queries_1 extends ProcessingIterator<T> {
          Iterator<T> iLeft;
          
          Iterator<?> iRight;
        }
        
        return new ____Queries_1() {
          {
            iLeft = left.iterator();
            
            iRight = right.iterator();
          }
          @Override
          protected boolean toNext() {
            if (((!this.iLeft.hasNext()) || (!this.iRight.hasNext()))) {
              return this.done();
            }
            final T lNext = this.iLeft.next();
            final Object rNext = this.iRight.next();
            boolean _xifexpression = false;
            if ((lNext == rNext)) {
              _xifexpression = this.setNext(lNext);
            } else {
              _xifexpression = this.done();
            }
            return _xifexpression;
          }
        };
      }
    };
  }
  
  public static <T extends Object, U extends Comparable<? super U>> List<T> sortedBy(final Iterable<T> input, final Function1<? super T, ? extends U> function) {
    final Comparator<U> _function = (U $0, U $1) -> {
      return $0.compareTo($1);
    };
    return Queries.<T, U>sortedBy(input, function, _function);
  }
  
  public static <T extends Object, U extends Object> List<T> sortedBy(final Iterable<T> input, final Function1<? super T, ? extends U> function, final Comparator<? super U> comparator) {
    List<T> result = IterableExtensions.<T>toList(input);
    final Comparator<T> _function = (T $0, T $1) -> {
      return comparator.compare(function.apply($0), function.apply($1));
    };
    Collections.<T>sort(result, _function);
    return result;
  }
  
  public static <T extends Object> List<T> reverse(final Iterable<T> source) {
    return ListExtensions.<T>reverse(IterableExtensions.<T>toList(source));
  }
  
  public static <T extends Object> LinkedHashSet<T> toOrderedSet(final Iterable<T> source) {
    final LinkedHashSet<T> result = CollectionLiterals.<T>newLinkedHashSet();
    Iterables.<T>addAll(result, source);
    return result;
  }
  
  public static <T extends Object, K extends Object, V extends Object> Map<K, List<V>> groupBy(final Iterable<? extends T> source, final Function1<? super T, ? extends K> computeKeys, final Function1<? super T, ? extends V> computeValues) {
    if ((computeKeys == null)) {
      throw new NullPointerException("computeKeys");
    }
    if ((computeValues == null)) {
      throw new NullPointerException("computeValues");
    }
    final Map<K, List<V>> result = CollectionLiterals.<K, List<V>>newLinkedHashMap();
    for (final T element : source) {
      {
        final K key = computeKeys.apply(element);
        final V value = computeValues.apply(element);
        final Function<K, List<V>> _function = (K it) -> {
          return CollectionLiterals.<V>newArrayList();
        };
        List<V> _computeIfAbsent = result.computeIfAbsent(key, _function);
        _computeIfAbsent.add(value);
      }
    }
    return result;
  }
  
  /**
   * Groups a source of inputs into groups of equivalent inputs.
   * 
   * @param <Input>     type of input
   * @param iterable      source of inputs
   * @param equivalence defines the equivalence relation of two inputs, e.g.
   *                    {@link Objects#equals(Object, Object)}.
   * @return a list of lists where every list contains equivalent inputs.
   * @see #groupBy(Iterable, Comparator, Function2)
   */
  public static <T extends Object> List<List<T>> groupBy(final Iterable<T> iterable, final Function2<? super T, ? super T, ? extends Boolean> equivalence) {
    return IterableQueries.<T>groupBy(iterable, new BiPredicate<T, T>() {
        public boolean test(T t, T u) {
          return equivalence.apply(t, u);
        }
    });
  }
  
  /**
   * Groups a source of inputs into groups of equivalent inputs.
   * 
   * @param <Input>     type of input
   * @param iterable    source of inputs
   * @param comparator  If not {@code null}, sorts the groups for performance
   *                    reasons, e.g. ascending list size when all groups are
   *                    expected to contain the same amount of elements or
   *                    descending list size when a few groups are expected to
   *                    contain most of the elements.
   * @param equivalence defines the equivalence relation of two inputs, e.g.
   *                    {@link Objects#equals(Object, Object)}.
   * @return a list of lists where every list contains equivalent inputs.
   */
  public static <T extends Object> List<List<T>> groupBy(final Iterable<T> iterable, final Comparator<? super List<T>> comparator, final Function2<? super T, ? super T, ? extends Boolean> equivalence) {
    return IterableQueries.<T>groupBy(iterable, comparator, new BiPredicate<T, T>() {
        public boolean test(T t, T u) {
          return equivalence.apply(t, u);
        }
    });
  }
  
  /**
   * Removes and returns the first entry in the given map that fulfills the predicate.
   * If none is found or the map is empty, null is returned.
   * <b>NOTE:</b> If the map doesn't guarantee an order this method should be considered as removeAny
   */
  public static <K extends Object, V extends Object> Map.Entry<K, V> removeFirst(final Map<K, V> map, final Function2<? super K, ? super V, ? extends Boolean> predicate) {
    final Function1<Map.Entry<K, V>, Boolean> _function = (Map.Entry<K, V> it) -> {
      return predicate.apply(it.getKey(), it.getValue());
    };
    return Queries.<Map.Entry<K, V>>removeFirst(map.entrySet(), _function);
  }
  
  /**
   * Removes and returns the first element in the given iterable that fulfills the predicate.
   * If none is found or the iterable is empty, null is returned.
   * <b>NOTE:</b> If the iterable doesn't guarantee an order (e.g. Set) this method should be considered as removeAny
   */
  public static <T extends Object> T removeFirst(final Iterable<T> iterable, final Function1<? super T, ? extends Boolean> predicate) {
    return Queries.<T>removeFirst(iterable.iterator(), predicate);
  }
  
  /**
   * Removes and returns the first element in the given iterator that fulfills the predicate.
   * If none is found or the iterator is empty, null is returned.
   */
  public static <T extends Object> T removeFirst(final Iterator<T> iterator, final Function1<? super T, ? extends Boolean> predicate) {
    final T result = IteratorExtensions.<T>findFirst(iterator, ((Function1<? super T, Boolean>)predicate));
    if ((result != null)) {
      iterator.remove();
    }
    return result;
  }
  
  /**
   * Segments {@code iterable} into groups, based on the {@code predicate}.
   * 
   * @param <T> type of input
   * @param iterable source of inputs
   * @param predicate defines where to segment, based on two subsequent entries of the {@code iterable}.
   * @return an iterable of lists (segments).
   */
  public static <T extends Object> Iterable<List<T>> segment(final Iterable<T> iterable, final Function2<? super T, ? super T, ? extends Boolean> predicate) {
    return IterableQueries.<T>segment(iterable, new BiPredicate<T, T>() {
        public boolean test(T t, T u) {
          return predicate.apply(t, u);
        }
    });
  }
  
  public static void main(final String[] args) {
    InputOutput.<Iterable<Pair<String, Integer>>>println(Queries.<String, Integer>product(Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("a", "b", "c")), Collections.<Integer>unmodifiableList(CollectionLiterals.<Integer>newArrayList(Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3)))));
    InputOutput.<Iterable<Pair<String, String>>>println(Queries.<String>product(Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("a", "b", "c"))));
    InputOutput.<Iterable<String>>println(Queries.<String>symmetricHead(Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("a", "b", "c")), Collections.<Object>unmodifiableList(CollectionLiterals.<Object>newArrayList("a", "b", "d"))));
    InputOutput.<Iterable<String>>println(Queries.<String>symmetricHead(Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("a", "b", "c")), Collections.<Object>unmodifiableList(CollectionLiterals.<Object>newArrayList(Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3)))));
    final Function1<Object, Boolean> _function = (Object it) -> {
      return Boolean.valueOf(Objects.equal(it, "c"));
    };
    InputOutput.<List<? extends Object>>println(Queries.reverse(Queries.after(Collections.<Object>unmodifiableList(CollectionLiterals.<Object>newArrayList("a", "b", "c", "d", Integer.valueOf(5))), _function)));
    final Function1<Object, Boolean> _function_1 = (Object it) -> {
      return Boolean.valueOf(Objects.equal(it, "c"));
    };
    InputOutput.<List<? extends Object>>println(Queries.reverse(Queries.before(Collections.<Object>unmodifiableList(CollectionLiterals.<Object>newArrayList("a", "b", "c", "d", Integer.valueOf(5))), _function_1)));
  }
}
