/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ContentHandler;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.URIHandler;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;

/**
 * A {@link URIConverter} which delegates all methods, but the returned {@link InputStream} of:
 * <ul>
 * <li>{@link #createInputStream(URI)}</li>
 * <li>{@link #createInputStream(URI, Map)}</li>
 * </ul>
 * is checked to see if it can be uncompressed using a {@link GZIPInputStream}.
 *
 */
public class GZIPAwareURIConverter implements URIConverter {
    /** Unsigned short in Intel byte order. */
    protected static final byte[] GZIP_MAGIC = {(byte)(GZIPInputStream.GZIP_MAGIC),
            (byte)(GZIPInputStream.GZIP_MAGIC >> 8)};

    private final URIConverter delegate;

    public GZIPAwareURIConverter() {
        this(new ExtensibleURIConverterImpl());
    }

    public GZIPAwareURIConverter(URIConverter delegate) {
        this.delegate = delegate;
    }

    public static InputStream tryGZIPInputStream(InputStream inputStream) throws IOException {
        if (inputStream instanceof GZIPInputStream) {
            return inputStream;
        }

        InputStream result = inputStream;
        if (!result.markSupported()) {
            result = new BufferedInputStream(result);
        }
        byte[] signature = new byte[2];
        result.mark(signature.length);
        int read = result.read(signature);
        result.reset();

        if (read == signature.length && Arrays.equals(signature, GZIP_MAGIC)) {
            result = new GZIPInputStream(result);
        }
        return result;
    }

    @Override
    public URI normalize(URI uri) {
        return delegate.normalize(uri);
    }

    @Override
    public Map<URI, URI> getURIMap() {
        return delegate.getURIMap();
    }

    @Override
    public EList<URIHandler> getURIHandlers() {
        return delegate.getURIHandlers();
    }

    @Override
    public URIHandler getURIHandler(URI uri) {
        return delegate.getURIHandler(uri);
    }

    @Override
    public EList<ContentHandler> getContentHandlers() {
        return delegate.getContentHandlers();
    }

    @Override
    public InputStream createInputStream(URI uri) throws IOException {
        return tryGZIPInputStream(delegate.createInputStream(uri));
    }

    @Override
    public InputStream createInputStream(URI uri, Map<?, ?> options) throws IOException {
        return tryGZIPInputStream(delegate.createInputStream(uri, options));
    }

    @Override
    public OutputStream createOutputStream(URI uri) throws IOException {
        return delegate.createOutputStream(uri);
    }

    @Override
    public OutputStream createOutputStream(URI uri, Map<?, ?> options) throws IOException {
        return delegate.createOutputStream(uri, options);
    }

    @Override
    public void delete(URI uri, Map<?, ?> options) throws IOException {
        delegate.delete(uri, options);
    }

    @Override
    public Map<String, ?> contentDescription(URI uri, Map<?, ?> options) throws IOException {
        return delegate.contentDescription(uri, options);
    }

    @Override
    public boolean exists(URI uri, Map<?, ?> options) {
        return delegate.exists(uri, options);
    }

    @Override
    public Map<String, ?> getAttributes(URI uri, Map<?, ?> options) {
        return delegate.getAttributes(uri, options);
    }

    @Override
    public void setAttributes(URI uri, Map<String, ?> attributes, Map<?, ?> options) throws IOException {
        delegate.setAttributes(uri, attributes, options);
    }
}
