/**
 */
package dispatching;

import machine.MachinePackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dispatching.DispatchingFactory
 * @model kind="package"
 * @generated
 */
public interface DispatchingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "dispatching";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/lsat/dispatching";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dispatching";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DispatchingPackage eINSTANCE = dispatching.impl.DispatchingPackageImpl.init();

	/**
	 * The meta object id for the '{@link dispatching.impl.ActivityDispatchingImpl <em>Activity Dispatching</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dispatching.impl.ActivityDispatchingImpl
	 * @see dispatching.impl.DispatchingPackageImpl#getActivityDispatching()
	 * @generated
	 */
	int ACTIVITY_DISPATCHING = 0;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DISPATCHING__IMPORTS = MachinePackage.IMPORT_CONTAINER__IMPORTS;

	/**
	 * The feature id for the '<em><b>Dispatch Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DISPATCHING__DISPATCH_GROUPS = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number Of Iterations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Resource Iterations</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Activity Dispatching</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DISPATCHING_FEATURE_COUNT = MachinePackage.IMPORT_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Load All</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DISPATCHING___LOAD_ALL = MachinePackage.IMPORT_CONTAINER___LOAD_ALL;

	/**
	 * The number of operations of the '<em>Activity Dispatching</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_DISPATCHING_OPERATION_COUNT = MachinePackage.IMPORT_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dispatching.HasUserAttributes <em>Has User Attributes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dispatching.HasUserAttributes
	 * @see dispatching.impl.DispatchingPackageImpl#getHasUserAttributes()
	 * @generated
	 */
	int HAS_USER_ATTRIBUTES = 6;

	/**
	 * The feature id for the '<em><b>User Attributes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_USER_ATTRIBUTES__USER_ATTRIBUTES = 0;

	/**
	 * The number of structural features of the '<em>Has User Attributes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_USER_ATTRIBUTES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Has User Attributes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_USER_ATTRIBUTES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dispatching.impl.DispatchImpl <em>Dispatch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dispatching.impl.DispatchImpl
	 * @see dispatching.impl.DispatchingPackageImpl#getDispatch()
	 * @generated
	 */
	int DISPATCH = 1;

	/**
	 * The feature id for the '<em><b>User Attributes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH__USER_ATTRIBUTES = HAS_USER_ATTRIBUTES__USER_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH__ACTIVITY = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH__DESCRIPTION = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Resource Items</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH__RESOURCE_ITEMS = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Dispatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_FEATURE_COUNT = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Dispatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_OPERATION_COUNT = HAS_USER_ATTRIBUTES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dispatching.impl.DispatchGroupImpl <em>Dispatch Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dispatching.impl.DispatchGroupImpl
	 * @see dispatching.impl.DispatchingPackageImpl#getDispatchGroup()
	 * @generated
	 */
	int DISPATCH_GROUP = 2;

	/**
	 * The feature id for the '<em><b>User Attributes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP__USER_ATTRIBUTES = HAS_USER_ATTRIBUTES__USER_ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Dispatches</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP__DISPATCHES = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP__OFFSET = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP__NAME = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Iterator Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP__ITERATOR_NAME = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Repeats</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP__REPEATS = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Yield</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP__YIELD = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Resource Yield</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP__RESOURCE_YIELD = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Dispatch Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP_FEATURE_COUNT = HAS_USER_ATTRIBUTES_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Dispatch Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPATCH_GROUP_OPERATION_COUNT = HAS_USER_ATTRIBUTES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dispatching.impl.ResourceIterationsMapEntryImpl <em>Resource Iterations Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dispatching.impl.ResourceIterationsMapEntryImpl
	 * @see dispatching.impl.DispatchingPackageImpl#getResourceIterationsMapEntry()
	 * @generated
	 */
	int RESOURCE_ITERATIONS_MAP_ENTRY = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ITERATIONS_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ITERATIONS_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Resource Iterations Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ITERATIONS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Resource Iterations Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_ITERATIONS_MAP_ENTRY_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link dispatching.impl.AttributesMapEntryImpl <em>Attributes Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dispatching.impl.AttributesMapEntryImpl
	 * @see dispatching.impl.DispatchingPackageImpl#getAttributesMapEntry()
	 * @generated
	 */
	int ATTRIBUTES_MAP_ENTRY = 4;

	/**
	 * The feature id for the '<em><b>Key</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Attributes Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Attributes Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTES_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dispatching.impl.AttributeImpl <em>Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dispatching.impl.AttributeImpl
	 * @see dispatching.impl.DispatchingPackageImpl#getAttribute()
	 * @generated
	 */
	int ATTRIBUTE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link dispatching.impl.RepeatImpl <em>Repeat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dispatching.impl.RepeatImpl
	 * @see dispatching.impl.DispatchingPackageImpl#getRepeat()
	 * @generated
	 */
	int REPEAT = 7;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__START = 0;

	/**
	 * The feature id for the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__COUNT = 1;

	/**
	 * The feature id for the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__END = 2;

	/**
	 * The feature id for the '<em><b>Num Repeats</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__NUM_REPEATS = 3;

	/**
	 * The number of structural features of the '<em>Repeat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Repeat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dispatching.impl.ResourceYieldMapEntryImpl <em>Resource Yield Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dispatching.impl.ResourceYieldMapEntryImpl
	 * @see dispatching.impl.DispatchingPackageImpl#getResourceYieldMapEntry()
	 * @generated
	 */
	int RESOURCE_YIELD_MAP_ENTRY = 8;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_YIELD_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_YIELD_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Resource Yield Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_YIELD_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Resource Yield Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_YIELD_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link dispatching.ActivityDispatching <em>Activity Dispatching</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity Dispatching</em>'.
	 * @see dispatching.ActivityDispatching
	 * @generated
	 */
	EClass getActivityDispatching();

	/**
	 * Returns the meta object for the containment reference list '{@link dispatching.ActivityDispatching#getDispatchGroups <em>Dispatch Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dispatch Groups</em>'.
	 * @see dispatching.ActivityDispatching#getDispatchGroups()
	 * @see #getActivityDispatching()
	 * @generated
	 */
	EReference getActivityDispatching_DispatchGroups();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.ActivityDispatching#getNumberOfIterations <em>Number Of Iterations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Iterations</em>'.
	 * @see dispatching.ActivityDispatching#getNumberOfIterations()
	 * @see #getActivityDispatching()
	 * @generated
	 */
	EAttribute getActivityDispatching_NumberOfIterations();

	/**
	 * Returns the meta object for the map '{@link dispatching.ActivityDispatching#getResourceIterations <em>Resource Iterations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Resource Iterations</em>'.
	 * @see dispatching.ActivityDispatching#getResourceIterations()
	 * @see #getActivityDispatching()
	 * @generated
	 */
	EReference getActivityDispatching_ResourceIterations();

	/**
	 * Returns the meta object for class '{@link dispatching.Dispatch <em>Dispatch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dispatch</em>'.
	 * @see dispatching.Dispatch
	 * @generated
	 */
	EClass getDispatch();

	/**
	 * Returns the meta object for the reference '{@link dispatching.Dispatch#getActivity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Activity</em>'.
	 * @see dispatching.Dispatch#getActivity()
	 * @see #getDispatch()
	 * @generated
	 */
	EReference getDispatch_Activity();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.Dispatch#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see dispatching.Dispatch#getDescription()
	 * @see #getDispatch()
	 * @generated
	 */
	EAttribute getDispatch_Description();

	/**
	 * Returns the meta object for the reference list '{@link dispatching.Dispatch#getResourceItems <em>Resource Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Resource Items</em>'.
	 * @see dispatching.Dispatch#getResourceItems()
	 * @see #getDispatch()
	 * @generated
	 */
	EReference getDispatch_ResourceItems();

	/**
	 * Returns the meta object for class '{@link dispatching.DispatchGroup <em>Dispatch Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dispatch Group</em>'.
	 * @see dispatching.DispatchGroup
	 * @generated
	 */
	EClass getDispatchGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link dispatching.DispatchGroup#getDispatches <em>Dispatches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dispatches</em>'.
	 * @see dispatching.DispatchGroup#getDispatches()
	 * @see #getDispatchGroup()
	 * @generated
	 */
	EReference getDispatchGroup_Dispatches();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.DispatchGroup#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see dispatching.DispatchGroup#getOffset()
	 * @see #getDispatchGroup()
	 * @generated
	 */
	EAttribute getDispatchGroup_Offset();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.DispatchGroup#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dispatching.DispatchGroup#getName()
	 * @see #getDispatchGroup()
	 * @generated
	 */
	EAttribute getDispatchGroup_Name();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.DispatchGroup#getIteratorName <em>Iterator Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Iterator Name</em>'.
	 * @see dispatching.DispatchGroup#getIteratorName()
	 * @see #getDispatchGroup()
	 * @generated
	 */
	EAttribute getDispatchGroup_IteratorName();

	/**
	 * Returns the meta object for the containment reference list '{@link dispatching.DispatchGroup#getRepeats <em>Repeats</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Repeats</em>'.
	 * @see dispatching.DispatchGroup#getRepeats()
	 * @see #getDispatchGroup()
	 * @generated
	 */
	EReference getDispatchGroup_Repeats();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.DispatchGroup#getYield <em>Yield</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Yield</em>'.
	 * @see dispatching.DispatchGroup#getYield()
	 * @see #getDispatchGroup()
	 * @generated
	 */
	EAttribute getDispatchGroup_Yield();

	/**
	 * Returns the meta object for the map '{@link dispatching.DispatchGroup#getResourceYield <em>Resource Yield</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Resource Yield</em>'.
	 * @see dispatching.DispatchGroup#getResourceYield()
	 * @see #getDispatchGroup()
	 * @generated
	 */
	EReference getDispatchGroup_ResourceYield();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Resource Iterations Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Iterations Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="machine.IResource" keyRequired="true"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject" valueRequired="true"
	 * @generated
	 */
	EClass getResourceIterationsMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getResourceIterationsMapEntry()
	 * @generated
	 */
	EReference getResourceIterationsMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getResourceIterationsMapEntry()
	 * @generated
	 */
	EAttribute getResourceIterationsMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Attributes Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attributes Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="dispatching.Attribute" keyContainment="true" keyRequired="true"
	 *        valueDataType="org.eclipse.emf.ecore.EString" valueRequired="true"
	 * @generated
	 */
	EClass getAttributesMapEntry();

	/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getAttributesMapEntry()
	 * @generated
	 */
	EReference getAttributesMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getAttributesMapEntry()
	 * @generated
	 */
	EAttribute getAttributesMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link dispatching.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute</em>'.
	 * @see dispatching.Attribute
	 * @generated
	 */
	EClass getAttribute();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.Attribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dispatching.Attribute#getName()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Name();

	/**
	 * Returns the meta object for class '{@link dispatching.HasUserAttributes <em>Has User Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has User Attributes</em>'.
	 * @see dispatching.HasUserAttributes
	 * @generated
	 */
	EClass getHasUserAttributes();

	/**
	 * Returns the meta object for the map '{@link dispatching.HasUserAttributes#getUserAttributes <em>User Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>User Attributes</em>'.
	 * @see dispatching.HasUserAttributes#getUserAttributes()
	 * @see #getHasUserAttributes()
	 * @generated
	 */
	EReference getHasUserAttributes_UserAttributes();

	/**
	 * Returns the meta object for class '{@link dispatching.Repeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repeat</em>'.
	 * @see dispatching.Repeat
	 * @generated
	 */
	EClass getRepeat();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.Repeat#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start</em>'.
	 * @see dispatching.Repeat#getStart()
	 * @see #getRepeat()
	 * @generated
	 */
	EAttribute getRepeat_Start();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.Repeat#getCount <em>Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count</em>'.
	 * @see dispatching.Repeat#getCount()
	 * @see #getRepeat()
	 * @generated
	 */
	EAttribute getRepeat_Count();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.Repeat#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End</em>'.
	 * @see dispatching.Repeat#getEnd()
	 * @see #getRepeat()
	 * @generated
	 */
	EAttribute getRepeat_End();

	/**
	 * Returns the meta object for the attribute '{@link dispatching.Repeat#getNumRepeats <em>Num Repeats</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Repeats</em>'.
	 * @see dispatching.Repeat#getNumRepeats()
	 * @see #getRepeat()
	 * @generated
	 */
	EAttribute getRepeat_NumRepeats();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Resource Yield Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Yield Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="machine.IResource" keyRequired="true"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject" valueRequired="true"
	 * @generated
	 */
	EClass getResourceYieldMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getResourceYieldMapEntry()
	 * @generated
	 */
	EReference getResourceYieldMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getResourceYieldMapEntry()
	 * @generated
	 */
	EAttribute getResourceYieldMapEntry_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DispatchingFactory getDispatchingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dispatching.impl.ActivityDispatchingImpl <em>Activity Dispatching</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dispatching.impl.ActivityDispatchingImpl
		 * @see dispatching.impl.DispatchingPackageImpl#getActivityDispatching()
		 * @generated
		 */
		EClass ACTIVITY_DISPATCHING = eINSTANCE.getActivityDispatching();

		/**
		 * The meta object literal for the '<em><b>Dispatch Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_DISPATCHING__DISPATCH_GROUPS = eINSTANCE.getActivityDispatching_DispatchGroups();

		/**
		 * The meta object literal for the '<em><b>Number Of Iterations</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS = eINSTANCE.getActivityDispatching_NumberOfIterations();

		/**
		 * The meta object literal for the '<em><b>Resource Iterations</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS = eINSTANCE.getActivityDispatching_ResourceIterations();

		/**
		 * The meta object literal for the '{@link dispatching.impl.DispatchImpl <em>Dispatch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dispatching.impl.DispatchImpl
		 * @see dispatching.impl.DispatchingPackageImpl#getDispatch()
		 * @generated
		 */
		EClass DISPATCH = eINSTANCE.getDispatch();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISPATCH__ACTIVITY = eINSTANCE.getDispatch_Activity();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISPATCH__DESCRIPTION = eINSTANCE.getDispatch_Description();

		/**
		 * The meta object literal for the '<em><b>Resource Items</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISPATCH__RESOURCE_ITEMS = eINSTANCE.getDispatch_ResourceItems();

		/**
		 * The meta object literal for the '{@link dispatching.impl.DispatchGroupImpl <em>Dispatch Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dispatching.impl.DispatchGroupImpl
		 * @see dispatching.impl.DispatchingPackageImpl#getDispatchGroup()
		 * @generated
		 */
		EClass DISPATCH_GROUP = eINSTANCE.getDispatchGroup();

		/**
		 * The meta object literal for the '<em><b>Dispatches</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISPATCH_GROUP__DISPATCHES = eINSTANCE.getDispatchGroup_Dispatches();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISPATCH_GROUP__OFFSET = eINSTANCE.getDispatchGroup_Offset();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISPATCH_GROUP__NAME = eINSTANCE.getDispatchGroup_Name();

		/**
		 * The meta object literal for the '<em><b>Iterator Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISPATCH_GROUP__ITERATOR_NAME = eINSTANCE.getDispatchGroup_IteratorName();

		/**
		 * The meta object literal for the '<em><b>Repeats</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISPATCH_GROUP__REPEATS = eINSTANCE.getDispatchGroup_Repeats();

		/**
		 * The meta object literal for the '<em><b>Yield</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISPATCH_GROUP__YIELD = eINSTANCE.getDispatchGroup_Yield();

		/**
		 * The meta object literal for the '<em><b>Resource Yield</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISPATCH_GROUP__RESOURCE_YIELD = eINSTANCE.getDispatchGroup_ResourceYield();

		/**
		 * The meta object literal for the '{@link dispatching.impl.ResourceIterationsMapEntryImpl <em>Resource Iterations Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dispatching.impl.ResourceIterationsMapEntryImpl
		 * @see dispatching.impl.DispatchingPackageImpl#getResourceIterationsMapEntry()
		 * @generated
		 */
		EClass RESOURCE_ITERATIONS_MAP_ENTRY = eINSTANCE.getResourceIterationsMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_ITERATIONS_MAP_ENTRY__KEY = eINSTANCE.getResourceIterationsMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_ITERATIONS_MAP_ENTRY__VALUE = eINSTANCE.getResourceIterationsMapEntry_Value();

		/**
		 * The meta object literal for the '{@link dispatching.impl.AttributesMapEntryImpl <em>Attributes Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dispatching.impl.AttributesMapEntryImpl
		 * @see dispatching.impl.DispatchingPackageImpl#getAttributesMapEntry()
		 * @generated
		 */
		EClass ATTRIBUTES_MAP_ENTRY = eINSTANCE.getAttributesMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTES_MAP_ENTRY__KEY = eINSTANCE.getAttributesMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTES_MAP_ENTRY__VALUE = eINSTANCE.getAttributesMapEntry_Value();

		/**
		 * The meta object literal for the '{@link dispatching.impl.AttributeImpl <em>Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dispatching.impl.AttributeImpl
		 * @see dispatching.impl.DispatchingPackageImpl#getAttribute()
		 * @generated
		 */
		EClass ATTRIBUTE = eINSTANCE.getAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__NAME = eINSTANCE.getAttribute_Name();

		/**
		 * The meta object literal for the '{@link dispatching.HasUserAttributes <em>Has User Attributes</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dispatching.HasUserAttributes
		 * @see dispatching.impl.DispatchingPackageImpl#getHasUserAttributes()
		 * @generated
		 */
		EClass HAS_USER_ATTRIBUTES = eINSTANCE.getHasUserAttributes();

		/**
		 * The meta object literal for the '<em><b>User Attributes</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAS_USER_ATTRIBUTES__USER_ATTRIBUTES = eINSTANCE.getHasUserAttributes_UserAttributes();

		/**
		 * The meta object literal for the '{@link dispatching.impl.RepeatImpl <em>Repeat</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dispatching.impl.RepeatImpl
		 * @see dispatching.impl.DispatchingPackageImpl#getRepeat()
		 * @generated
		 */
		EClass REPEAT = eINSTANCE.getRepeat();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPEAT__START = eINSTANCE.getRepeat_Start();

		/**
		 * The meta object literal for the '<em><b>Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPEAT__COUNT = eINSTANCE.getRepeat_Count();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPEAT__END = eINSTANCE.getRepeat_End();

		/**
		 * The meta object literal for the '<em><b>Num Repeats</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPEAT__NUM_REPEATS = eINSTANCE.getRepeat_NumRepeats();

		/**
		 * The meta object literal for the '{@link dispatching.impl.ResourceYieldMapEntryImpl <em>Resource Yield Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dispatching.impl.ResourceYieldMapEntryImpl
		 * @see dispatching.impl.DispatchingPackageImpl#getResourceYieldMapEntry()
		 * @generated
		 */
		EClass RESOURCE_YIELD_MAP_ENTRY = eINSTANCE.getResourceYieldMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_YIELD_MAP_ENTRY__KEY = eINSTANCE.getResourceYieldMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_YIELD_MAP_ENTRY__VALUE = eINSTANCE.getResourceYieldMapEntry_Value();

	}

} //DispatchingPackage
