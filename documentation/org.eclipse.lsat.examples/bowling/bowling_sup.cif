/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
controllable Condition;
controllable DrillStep_Thumb;
controllable DrillStep_IndexFinger;
controllable DrillStep_MiddleFinger;
controllable DrillTableIndexFinger;
controllable DrillTableMiddleFinger;
controllable DrillTableThumb;
controllable LRFromCondToIn;
controllable LRBallFromInToCond;
controllable LRBallFromCondToDrill;
controllable URBallFromCondToDrill;
controllable URBallFromDrillToOut;
controllable UROutToOutDrill;
supervisor automaton sup:
  alphabet Condition, DrillStep_Thumb, DrillStep_IndexFinger, DrillStep_MiddleFinger, DrillTableIndexFinger, DrillTableMiddleFinger, DrillTableThumb, LRFromCondToIn, LRBallFromInToCond, LRBallFromCondToDrill, URBallFromCondToDrill, URBallFromDrillToOut, UROutToOutDrill;
  location s1:
    initial;
    marked;
    edge UROutToOutDrill goto s2;
    edge LRBallFromInToCond goto s3;
  location s2:
    edge LRBallFromInToCond goto s4;
  location s3:
    edge UROutToOutDrill goto s4;
    edge LRFromCondToIn goto s5;
    edge Condition goto s6;
  location s4:
    edge Condition goto s7;
  location s5:
    edge Condition goto s10;
  location s6:
    edge UROutToOutDrill goto s7;
    edge URBallFromCondToDrill goto s8;
    edge LRBallFromCondToDrill goto s9;
    edge LRFromCondToIn goto s10;
  location s7:
    edge LRBallFromCondToDrill goto s11;
  location s8:
    edge LRFromCondToIn goto s11;
    edge DrillStep_Thumb goto s14;
  location s9:
    edge UROutToOutDrill goto s11;
    edge LRBallFromInToCond goto s12;
    edge DrillStep_Thumb goto s13;
  location s10:
    edge URBallFromCondToDrill goto s11;
  location s11:
    edge LRBallFromInToCond goto s15;
    edge DrillStep_Thumb goto s16;
  location s12:
    edge UROutToOutDrill goto s15;
    edge LRFromCondToIn goto s19;
    edge DrillStep_Thumb goto s17;
    edge Condition goto s20;
  location s13:
    edge UROutToOutDrill goto s16;
    edge LRBallFromInToCond goto s17;
    edge DrillTableIndexFinger goto s18;
  location s14:
    edge LRFromCondToIn goto s16;
    edge DrillTableIndexFinger goto s21;
  location s15:
    edge LRFromCondToIn goto s24;
    edge DrillStep_Thumb goto s22;
    edge Condition goto s25;
  location s16:
    edge LRBallFromInToCond goto s22;
    edge DrillTableIndexFinger goto s23;
  location s17:
    edge UROutToOutDrill goto s22;
    edge LRFromCondToIn goto s28;
    edge DrillTableIndexFinger goto s26;
    edge Condition goto s29;
  location s18:
    edge UROutToOutDrill goto s23;
    edge LRBallFromInToCond goto s26;
    edge DrillStep_IndexFinger goto s27;
  location s19:
    edge UROutToOutDrill goto s24;
    edge DrillStep_Thumb goto s28;
    edge Condition goto s30;
  location s20:
    edge UROutToOutDrill goto s25;
    edge LRFromCondToIn goto s30;
    edge DrillStep_Thumb goto s29;
  location s21:
    edge LRFromCondToIn goto s23;
    edge DrillStep_IndexFinger goto s31;
  location s22:
    edge LRFromCondToIn goto s34;
    edge DrillTableIndexFinger goto s32;
    edge Condition goto s35;
  location s23:
    edge LRBallFromInToCond goto s32;
    edge DrillStep_IndexFinger goto s33;
  location s24:
    edge DrillStep_Thumb goto s34;
    edge Condition goto s36;
  location s25:
    edge LRFromCondToIn goto s36;
    edge DrillStep_Thumb goto s35;
  location s26:
    edge UROutToOutDrill goto s32;
    edge LRFromCondToIn goto s39;
    edge DrillStep_IndexFinger goto s37;
    edge Condition goto s40;
  location s27:
    edge UROutToOutDrill goto s33;
    edge LRBallFromInToCond goto s37;
    edge DrillTableMiddleFinger goto s38;
  location s28:
    edge UROutToOutDrill goto s34;
    edge DrillTableIndexFinger goto s39;
    edge Condition goto s41;
  location s29:
    edge UROutToOutDrill goto s35;
    edge LRFromCondToIn goto s41;
    edge DrillTableIndexFinger goto s40;
  location s30:
    edge UROutToOutDrill goto s36;
    edge DrillStep_Thumb goto s41;
  location s31:
    edge LRFromCondToIn goto s33;
    edge DrillTableMiddleFinger goto s42;
  location s32:
    edge LRFromCondToIn goto s45;
    edge DrillStep_IndexFinger goto s43;
    edge Condition goto s46;
  location s33:
    edge LRBallFromInToCond goto s43;
    edge DrillTableMiddleFinger goto s44;
  location s34:
    edge DrillTableIndexFinger goto s45;
    edge Condition goto s47;
  location s35:
    edge LRFromCondToIn goto s47;
    edge DrillTableIndexFinger goto s46;
  location s36:
    edge DrillStep_Thumb goto s47;
  location s37:
    edge UROutToOutDrill goto s43;
    edge LRFromCondToIn goto s50;
    edge DrillTableMiddleFinger goto s48;
    edge Condition goto s51;
  location s38:
    edge UROutToOutDrill goto s44;
    edge LRBallFromInToCond goto s48;
    edge DrillStep_MiddleFinger goto s49;
  location s39:
    edge UROutToOutDrill goto s45;
    edge DrillStep_IndexFinger goto s50;
    edge Condition goto s52;
  location s40:
    edge UROutToOutDrill goto s46;
    edge LRFromCondToIn goto s52;
    edge DrillStep_IndexFinger goto s51;
  location s41:
    edge UROutToOutDrill goto s47;
    edge DrillTableIndexFinger goto s52;
  location s42:
    edge LRFromCondToIn goto s44;
    edge DrillStep_MiddleFinger goto s53;
  location s43:
    edge LRFromCondToIn goto s56;
    edge DrillTableMiddleFinger goto s54;
    edge Condition goto s57;
  location s44:
    edge LRBallFromInToCond goto s54;
    edge DrillStep_MiddleFinger goto s55;
  location s45:
    edge DrillStep_IndexFinger goto s56;
    edge Condition goto s58;
  location s46:
    edge LRFromCondToIn goto s58;
    edge DrillStep_IndexFinger goto s57;
  location s47:
    edge DrillTableIndexFinger goto s58;
  location s48:
    edge UROutToOutDrill goto s54;
    edge LRFromCondToIn goto s61;
    edge DrillStep_MiddleFinger goto s59;
    edge Condition goto s62;
  location s49:
    edge UROutToOutDrill goto s55;
    edge LRBallFromInToCond goto s59;
    edge DrillTableThumb goto s60;
  location s50:
    edge UROutToOutDrill goto s56;
    edge DrillTableMiddleFinger goto s61;
    edge Condition goto s63;
  location s51:
    edge UROutToOutDrill goto s57;
    edge LRFromCondToIn goto s63;
    edge DrillTableMiddleFinger goto s62;
  location s52:
    edge UROutToOutDrill goto s58;
    edge DrillStep_IndexFinger goto s63;
  location s53:
    edge LRFromCondToIn goto s55;
    edge DrillTableThumb goto s64;
  location s54:
    edge LRFromCondToIn goto s67;
    edge DrillStep_MiddleFinger goto s65;
    edge Condition goto s68;
  location s55:
    edge LRBallFromInToCond goto s65;
    edge DrillTableThumb goto s66;
  location s56:
    edge DrillTableMiddleFinger goto s67;
    edge Condition goto s69;
  location s57:
    edge LRFromCondToIn goto s69;
    edge DrillTableMiddleFinger goto s68;
  location s58:
    edge DrillStep_IndexFinger goto s69;
  location s59:
    edge UROutToOutDrill goto s65;
    edge LRFromCondToIn goto s71;
    edge DrillTableThumb goto s70;
    edge Condition goto s72;
  location s60:
    edge UROutToOutDrill goto s66;
    edge LRBallFromInToCond goto s70;
  location s61:
    edge UROutToOutDrill goto s67;
    edge DrillStep_MiddleFinger goto s71;
    edge Condition goto s73;
  location s62:
    edge UROutToOutDrill goto s68;
    edge LRFromCondToIn goto s73;
    edge DrillStep_MiddleFinger goto s72;
  location s63:
    edge UROutToOutDrill goto s69;
    edge DrillTableMiddleFinger goto s73;
  location s64:
    edge URBallFromDrillToOut goto s74;
    edge LRFromCondToIn goto s66;
  location s65:
    edge LRFromCondToIn goto s76;
    edge DrillTableThumb goto s75;
    edge Condition goto s77;
  location s66:
    edge URBallFromDrillToOut goto s1;
    edge LRBallFromInToCond goto s75;
  location s67:
    edge DrillStep_MiddleFinger goto s76;
    edge Condition goto s78;
  location s68:
    edge LRFromCondToIn goto s78;
    edge DrillStep_MiddleFinger goto s77;
  location s69:
    edge DrillTableMiddleFinger goto s78;
  location s70:
    edge UROutToOutDrill goto s75;
    edge LRFromCondToIn goto s79;
    edge Condition goto s80;
  location s71:
    edge UROutToOutDrill goto s76;
    edge DrillTableThumb goto s79;
    edge Condition goto s81;
  location s72:
    edge UROutToOutDrill goto s77;
    edge LRFromCondToIn goto s81;
    edge DrillTableThumb goto s80;
  location s73:
    edge UROutToOutDrill goto s78;
    edge DrillStep_MiddleFinger goto s81;
  location s74:
    edge UROutToOutDrill goto s82;
    edge LRFromCondToIn goto s1;
  location s75:
    edge URBallFromDrillToOut goto s3;
    edge LRFromCondToIn goto s83;
    edge Condition goto s84;
  location s76:
    edge DrillTableThumb goto s83;
    edge Condition goto s85;
  location s77:
    edge LRFromCondToIn goto s85;
    edge DrillTableThumb goto s84;
  location s78:
    edge DrillStep_MiddleFinger goto s85;
  location s79:
    edge UROutToOutDrill goto s83;
    edge Condition goto s86;
  location s80:
    edge UROutToOutDrill goto s84;
    edge LRFromCondToIn goto s86;
  location s81:
    edge UROutToOutDrill goto s85;
    edge DrillTableThumb goto s86;
  location s82:
    edge LRFromCondToIn goto s2;
  location s83:
    edge URBallFromDrillToOut goto s5;
    edge Condition goto s87;
  location s84:
    edge URBallFromDrillToOut goto s6;
    edge LRFromCondToIn goto s87;
  location s85:
    edge DrillTableThumb goto s87;
  location s86:
    edge UROutToOutDrill goto s87;
  location s87:
    edge URBallFromDrillToOut goto s10;
end
