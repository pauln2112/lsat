/**
 */
package machine.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import machine.MachinePackage;
import machine.Peripheral;
import machine.Resource;

import machine.ResourceItem;
import machine.ResourceType;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.ResourceImpl#getName <em>Name</em>}</li>
 *   <li>{@link machine.impl.ResourceImpl#getPeripherals <em>Peripherals</em>}</li>
 *   <li>{@link machine.impl.ResourceImpl#getItems <em>Items</em>}</li>
 *   <li>{@link machine.impl.ResourceImpl#getResourceType <em>Resource Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceImpl extends MinimalEObjectImpl.Container implements Resource {
	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected String name = NAME_EDEFAULT;

	/**
     * The cached value of the '{@link #getPeripherals() <em>Peripherals</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPeripherals()
     * @generated
     * @ordered
     */
	protected EList<Peripheral> peripherals;

	/**
     * The cached value of the '{@link #getItems() <em>Items</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getItems()
     * @generated
     * @ordered
     */
	protected EList<ResourceItem> items;

	/**
     * The default value of the '{@link #getResourceType() <em>Resource Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getResourceType()
     * @generated
     * @ordered
     */
    protected static final ResourceType RESOURCE_TYPE_EDEFAULT = ResourceType.REGULAR;

    /**
     * The cached value of the '{@link #getResourceType() <em>Resource Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getResourceType()
     * @generated
     * @ordered
     */
    protected ResourceType resourceType = RESOURCE_TYPE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ResourceImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.RESOURCE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
        return name;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.RESOURCE__NAME, oldName, name));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Peripheral> getPeripherals() {
        if (peripherals == null)
        {
            peripherals = new EObjectContainmentWithInverseEList<Peripheral>(Peripheral.class, this, MachinePackage.RESOURCE__PERIPHERALS, MachinePackage.PERIPHERAL__RESOURCE);
        }
        return peripherals;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<ResourceItem> getItems() {
        if (items == null)
        {
            items = new EObjectContainmentWithInverseEList<ResourceItem>(ResourceItem.class, this, MachinePackage.RESOURCE__ITEMS, MachinePackage.RESOURCE_ITEM__RESOURCE);
        }
        return items;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ResourceType getResourceType()
    {
        return resourceType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setResourceType(ResourceType newResourceType)
    {
        ResourceType oldResourceType = resourceType;
        resourceType = newResourceType == null ? RESOURCE_TYPE_EDEFAULT : newResourceType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.RESOURCE__RESOURCE_TYPE, oldResourceType, resourceType));
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Resource getResource() {
        return this;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String fqn() {
        if(name !=null)
          return name;
        return null;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.RESOURCE__PERIPHERALS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getPeripherals()).basicAdd(otherEnd, msgs);
            case MachinePackage.RESOURCE__ITEMS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getItems()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.RESOURCE__PERIPHERALS:
                return ((InternalEList<?>)getPeripherals()).basicRemove(otherEnd, msgs);
            case MachinePackage.RESOURCE__ITEMS:
                return ((InternalEList<?>)getItems()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.RESOURCE__NAME:
                return getName();
            case MachinePackage.RESOURCE__PERIPHERALS:
                return getPeripherals();
            case MachinePackage.RESOURCE__ITEMS:
                return getItems();
            case MachinePackage.RESOURCE__RESOURCE_TYPE:
                return getResourceType();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.RESOURCE__NAME:
                setName((String)newValue);
                return;
            case MachinePackage.RESOURCE__PERIPHERALS:
                getPeripherals().clear();
                getPeripherals().addAll((Collection<? extends Peripheral>)newValue);
                return;
            case MachinePackage.RESOURCE__ITEMS:
                getItems().clear();
                getItems().addAll((Collection<? extends ResourceItem>)newValue);
                return;
            case MachinePackage.RESOURCE__RESOURCE_TYPE:
                setResourceType((ResourceType)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.RESOURCE__NAME:
                setName(NAME_EDEFAULT);
                return;
            case MachinePackage.RESOURCE__PERIPHERALS:
                getPeripherals().clear();
                return;
            case MachinePackage.RESOURCE__ITEMS:
                getItems().clear();
                return;
            case MachinePackage.RESOURCE__RESOURCE_TYPE:
                setResourceType(RESOURCE_TYPE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.RESOURCE__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case MachinePackage.RESOURCE__PERIPHERALS:
                return peripherals != null && !peripherals.isEmpty();
            case MachinePackage.RESOURCE__ITEMS:
                return items != null && !items.isEmpty();
            case MachinePackage.RESOURCE__RESOURCE_TYPE:
                return resourceType != RESOURCE_TYPE_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case MachinePackage.RESOURCE___GET_RESOURCE:
                return getResource();
            case MachinePackage.RESOURCE___FQN:
                return fqn();
        }
        return super.eInvoke(operationID, arguments);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", resourceType: ");
        result.append(resourceType);
        result.append(')');
        return result.toString();
    }

} //ResourceImpl
