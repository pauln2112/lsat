/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import lsat_graph.ClaimReleaseResource;
import lsat_graph.ClaimTask;
import lsat_graph.ClaimedByScheduledTask;
import lsat_graph.ReleaseTask;
import lsat_graph.lsat_graphFactory;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.resources.Resource;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.scheduler.schedule.Sequence;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class VisualizeClaimedBy<T extends Task> {
  @Extension
  private final lsat_graphFactory m_lsat_graph = lsat_graphFactory.eINSTANCE;
  
  public void transformModel(final Schedule<T> schedule, final IProgressMonitor monitor) {
    final Function1<Sequence<T>, Boolean> _function = (Sequence<T> it) -> {
      Resource _resource = it.getResource();
      return Boolean.valueOf((_resource instanceof ClaimReleaseResource));
    };
    final Consumer<Sequence<T>> _function_1 = (Sequence<T> it) -> {
      this.createClaimedByTasks(it);
    };
    IterableExtensions.<Sequence<T>>filter(schedule.getSequences(), _function).forEach(_function_1);
  }
  
  private void createClaimedByTasks(final Sequence<T> claimReleaseSequence) {
    final Function2<ScheduledTask<T>, ScheduledTask<T>, Boolean> _function = (ScheduledTask<T> l, ScheduledTask<T> r) -> {
      return Boolean.valueOf((Objects.equal(l.getTask().getClass(), r.getTask().getClass()) && Objects.equal(l.getStartTime(), r.getStartTime())));
    };
    final List<List<ScheduledTask<T>>> claimReleaseGroups = IterableExtensions.<List<ScheduledTask<T>>>toList(Queries.<ScheduledTask<T>>segment(claimReleaseSequence.getScheduledTasks(), _function));
    ClaimedByScheduledTask currentClaimedBy = null;
    for (final List<ScheduledTask<T>> group : claimReleaseGroups) {
      final Function1<ScheduledTask<T>, Boolean> _function_1 = (ScheduledTask<T> it) -> {
        T _task = it.getTask();
        return Boolean.valueOf((_task instanceof ClaimTask));
      };
      boolean _forall = IterableExtensions.<ScheduledTask<T>>forall(group, _function_1);
      if (_forall) {
        ScheduledTask<T> _head = IterableExtensions.<ScheduledTask<T>>head(group);
        final ScheduledTask<ClaimTask> firstClaim = ((ScheduledTask<ClaimTask>) _head);
        final ClaimedByScheduledTask previousClaimedBy = currentClaimedBy;
        ClaimedByScheduledTask _createClaimedByScheduledTask = this.m_lsat_graph.createClaimedByScheduledTask();
        final Procedure1<ClaimedByScheduledTask> _function_2 = (ClaimedByScheduledTask it) -> {
          it.setStartTime(firstClaim.getStartTime());
          it.setTask(firstClaim.getTask());
          it.setGraph(firstClaim.getGraph());
          it.setSequence(claimReleaseSequence);
        };
        ClaimedByScheduledTask _doubleArrow = ObjectExtensions.<ClaimedByScheduledTask>operator_doubleArrow(_createClaimedByScheduledTask, _function_2);
        currentClaimedBy = _doubleArrow;
        if ((previousClaimedBy != null)) {
          previousClaimedBy.setEndTime(currentClaimedBy.getStartTime());
          EList<ScheduledTask<Task>> _claims = currentClaimedBy.getClaims();
          EList<ScheduledTask<Task>> _claims_1 = previousClaimedBy.getClaims();
          Iterables.<ScheduledTask<Task>>addAll(_claims, _claims_1);
        }
        EList<ScheduledTask<Task>> _claims_2 = currentClaimedBy.getClaims();
        Iterables.<ScheduledTask<Task>>addAll(_claims_2, ((Iterable<? extends ScheduledTask<Task>>) group));
        this.deriveNameFromClaims(currentClaimedBy);
      } else {
        if (((currentClaimedBy != null) && IterableExtensions.<ScheduledTask<T>>forall(group, ((Function1<ScheduledTask<T>, Boolean>) (ScheduledTask<T> it) -> {
          T _task = it.getTask();
          return Boolean.valueOf((_task instanceof ReleaseTask));
        })))) {
          ScheduledTask<T> _last = IterableExtensions.<ScheduledTask<T>>last(group);
          final ScheduledTask<ReleaseTask> lastRelease = ((ScheduledTask<ReleaseTask>) _last);
          currentClaimedBy.setEndTime(lastRelease.getEndTime());
          EList<ScheduledTask<Task>> _releases = currentClaimedBy.getReleases();
          Iterables.<ScheduledTask<Task>>addAll(_releases, ((Iterable<? extends ScheduledTask<Task>>) group));
          int _size = currentClaimedBy.getClaims().size();
          int _size_1 = currentClaimedBy.getReleases().size();
          boolean _equals = (_size == _size_1);
          if (_equals) {
            currentClaimedBy = null;
          } else {
            final Set<ScheduledTask<Task>> remainingClaims = IterableExtensions.<ScheduledTask<Task>>toSet(currentClaimedBy.getClaims());
            final Function1<ScheduledTask<Task>, ScheduledTask<Task>> _function_3 = (ScheduledTask<Task> it) -> {
              return this.getClaim(it);
            };
            Set<ScheduledTask<Task>> _set = IterableExtensions.<ScheduledTask<Task>>toSet(ListExtensions.<ScheduledTask<Task>, ScheduledTask<Task>>map(currentClaimedBy.getReleases(), _function_3));
            Iterables.removeAll(remainingClaims, _set);
            ClaimedByScheduledTask _createClaimedByScheduledTask_1 = this.m_lsat_graph.createClaimedByScheduledTask();
            final Procedure1<ClaimedByScheduledTask> _function_4 = (ClaimedByScheduledTask it) -> {
              it.setStartTime(lastRelease.getEndTime());
              it.setTask(lastRelease.getTask());
              it.setGraph(lastRelease.getGraph());
              it.setSequence(claimReleaseSequence);
              EList<ScheduledTask<Task>> _claims_3 = it.getClaims();
              Iterables.<ScheduledTask<Task>>addAll(_claims_3, remainingClaims);
            };
            ClaimedByScheduledTask _doubleArrow_1 = ObjectExtensions.<ClaimedByScheduledTask>operator_doubleArrow(_createClaimedByScheduledTask_1, _function_4);
            currentClaimedBy = _doubleArrow_1;
            this.deriveNameFromClaims(currentClaimedBy);
          }
        }
      }
    }
  }
  
  private ScheduledTask<Task> getClaim(final ScheduledTask<Task> scheduledTask) {
    Sequence<?> _sequence = scheduledTask.getSequence();
    final Sequence<Task> sequence = ((Sequence<Task>) _sequence);
    final Function1<ScheduledTask<Task>, Boolean> _function = (ScheduledTask<Task> it) -> {
      Task _task = it.getTask();
      return Boolean.valueOf((_task instanceof ClaimTask));
    };
    final Function1<ScheduledTask<Task>, Boolean> _function_1 = (ScheduledTask<Task> it) -> {
      DirectedGraph<?, ?> _graph = it.getTask().getGraph();
      DirectedGraph<?, ?> _graph_1 = scheduledTask.getTask().getGraph();
      return Boolean.valueOf(Objects.equal(_graph, _graph_1));
    };
    return IterableExtensions.<ScheduledTask<Task>>findFirst(IterableExtensions.<ScheduledTask<Task>>filter(sequence.getScheduledTasks(), _function), _function_1);
  }
  
  private void deriveNameFromClaims(final ClaimedByScheduledTask claimedBy) {
    final Function1<ScheduledTask<Task>, Task> _function = (ScheduledTask<Task> it) -> {
      return it.getTask();
    };
    final Function1<ClaimTask, Boolean> _function_1 = (ClaimTask it) -> {
      return Boolean.valueOf(it.getAction().isPassive());
    };
    final boolean isPassive = IterableExtensions.<ClaimTask>forall(Iterables.<ClaimTask>filter(ListExtensions.<ScheduledTask<Task>, Task>map(claimedBy.getClaims(), _function), ClaimTask.class), _function_1);
    if (isPassive) {
      int _size = claimedBy.getClaims().size();
      boolean _greaterThan = (_size > 1);
      if (_greaterThan) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Claimed passively by ");
        int _size_1 = claimedBy.getClaims().size();
        _builder.append(_size_1);
        _builder.append(" activities");
        claimedBy.setName(_builder.toString());
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Claimed passively by ");
        String _name = IterableExtensions.<ScheduledTask<Task>>head(claimedBy.getClaims()).getTask().getGraph().getName();
        _builder_1.append(_name);
        claimedBy.setName(_builder_1.toString());
      }
    } else {
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("Claimed by ");
      String _name_1 = IterableExtensions.<ScheduledTask<Task>>head(claimedBy.getClaims()).getTask().getGraph().getName();
      _builder_2.append(_name_1);
      claimedBy.setName(_builder_2.toString());
    }
  }
}
