/**
 */
package lsat_graph;

import org.eclipse.lsat.common.scheduler.resources.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dispatch Group Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lsat_graph.lsat_graphPackage#getDispatchGroupResource()
 * @model
 * @generated
 */
public interface DispatchGroupResource extends Resource {
} // DispatchGroupResource
