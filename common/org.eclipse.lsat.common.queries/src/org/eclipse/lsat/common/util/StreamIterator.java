/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Spliterator;
import java.util.stream.Stream;

public class StreamIterator<T> extends ProcessingIterator<T> {
    private final Stream<T> stream;

    private Spliterator<T> spliterator;

    public StreamIterator(Stream<T> stream) {
        this.stream = stream;
    }

    @Override
    protected boolean toNext() {
        synchronized (stream) {
            if (null == spliterator) {
                spliterator = stream.spliterator();
            }
        }
        boolean hasNext = spliterator.tryAdvance(this::setNext);
        return hasNext ? hasNext : done();
    }
}
