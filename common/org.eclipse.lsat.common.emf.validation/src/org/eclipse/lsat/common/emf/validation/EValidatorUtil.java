/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.validation;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.EValidator.Registry;
import org.eclipse.emf.ecore.impl.EValidatorRegistryImpl;
import org.eclipse.ocl.pivot.validation.ComposedEValidator;

public class EValidatorUtil {
    private EValidatorUtil() {
        /* Empty */
    }

    /**
     * Adds an {@link EValidator} to the global validation registry.
     *
     * @see Registry#INSTANCE
     */
    public static void registerValidations(EPackage ePackage, EValidator eValidator) {
        registerValidations(ePackage, eValidator, EValidator.Registry.INSTANCE);
    }

    /**
     * Adds an {@link EValidator} to the specified {@link Registry}.
     *
     * @see EValidatorRegistryImpl
     */
    public static void registerValidations(EPackage ePackage, EValidator eValidator, EValidator.Registry registry) {
        synchronized (registry) {
            final ComposedEValidator composedEValidator;
            if (EValidator.Registry.INSTANCE == registry) {
                // Root registry, we can use the pivot code
                composedEValidator = ComposedEValidator.install(ePackage);
            } else {
                EValidator oldEValidator = registry.getEValidator(ePackage);
                if (oldEValidator instanceof ComposedEValidator) {
                    // We cannot add to this directly as it could come from
                    // a delegate registry and we do not want to modify its
                    // content.
                    // Therefore we create a copy
                    composedEValidator = new ComposedEValidator(null);
                    for (EValidator oldChild: ((ComposedEValidator)oldEValidator).getChildren()) {
                        composedEValidator.addChild(oldChild);
                    }
                } else {
                    composedEValidator = new ComposedEValidator(oldEValidator);
                }
                registry.put(ePackage, composedEValidator);
            }
            composedEValidator.addChild(eValidator);
        }
    }
}
