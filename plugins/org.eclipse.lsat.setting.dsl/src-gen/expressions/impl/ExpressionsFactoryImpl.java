/**
 */
package expressions.impl;

import expressions.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExpressionsFactoryImpl extends EFactoryImpl implements ExpressionsFactory {
	/**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static ExpressionsFactory init() {
        try
        {
            ExpressionsFactory theExpressionsFactory = (ExpressionsFactory)EPackage.Registry.INSTANCE.getEFactory(ExpressionsPackage.eNS_URI);
            if (theExpressionsFactory != null)
            {
                return theExpressionsFactory;
            }
        }
        catch (Exception exception)
        {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new ExpressionsFactoryImpl();
    }

	/**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ExpressionsFactoryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject create(EClass eClass) {
        switch (eClass.getClassifierID())
        {
            case ExpressionsPackage.DECLARATION: return createDeclaration();
            case ExpressionsPackage.ADD: return createAdd();
            case ExpressionsPackage.SUBTRACT: return createSubtract();
            case ExpressionsPackage.MULTIPLY: return createMultiply();
            case ExpressionsPackage.BIG_DECIMAL_CONSTANT: return createBigDecimalConstant();
            case ExpressionsPackage.DECLARATION_REF: return createDeclarationRef();
            case ExpressionsPackage.DIVIDE: return createDivide();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Declaration createDeclaration() {
        DeclarationImpl declaration = new DeclarationImpl();
        return declaration;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Add createAdd() {
        AddImpl add = new AddImpl();
        return add;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Subtract createSubtract() {
        SubtractImpl subtract = new SubtractImpl();
        return subtract;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Multiply createMultiply() {
        MultiplyImpl multiply = new MultiplyImpl();
        return multiply;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimalConstant createBigDecimalConstant() {
        BigDecimalConstantImpl bigDecimalConstant = new BigDecimalConstantImpl();
        return bigDecimalConstant;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public DeclarationRef createDeclarationRef() {
        DeclarationRefImpl declarationRef = new DeclarationRefImpl();
        return declarationRef;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Divide createDivide() {
        DivideImpl divide = new DivideImpl();
        return divide;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ExpressionsPackage getExpressionsPackage() {
        return (ExpressionsPackage)getEPackage();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
	@Deprecated
	public static ExpressionsPackage getPackage() {
        return ExpressionsPackage.eINSTANCE;
    }

} //ExpressionsFactoryImpl
