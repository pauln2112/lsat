/**
 */
package activity;

import machine.ResourceItem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EventAction Item</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activity.ActivityPackage#getEventItem()
 * @model
 * @generated
 */
public interface EventItem extends ResourceItem
{
} // EventItem
