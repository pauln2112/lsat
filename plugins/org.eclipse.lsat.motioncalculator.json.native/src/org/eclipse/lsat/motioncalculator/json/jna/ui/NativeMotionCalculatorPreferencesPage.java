/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json.jna.ui;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

public class NativeMotionCalculatorPreferencesPage extends FieldEditorPreferencePage
        implements IWorkbenchPreferencePage
{
    public NativeMotionCalculatorPreferencesPage() {
        super(GRID);
    }

    public void createFieldEditors() {
        addField(new StringFieldEditor("LIBRARY", "&Library name (without extension) :", getFieldEditorParent()));
        addField(new BooleanFieldEditor("WRITE_MESSAGES", "Write &Messages to console :",
                BooleanFieldEditor.SEPARATE_LABEL, getFieldEditorParent()));
    }

    @Override
    public void init(IWorkbench workbench) {
        // second parameter is typically the plug-in id
        setPreferenceStore(
                new ScopedPreferenceStore(InstanceScope.INSTANCE, "org.eclipse.lsat.motioncalculator.json.jna"));
        setDescription("Configures the name of the third party shared library.\n\n");
    }
}
