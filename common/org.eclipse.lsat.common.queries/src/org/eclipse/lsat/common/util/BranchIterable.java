/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

/**
 * An {@link Iterable} that return instances of {@link BranchIterator} when calling {@link #iterator()}.
 *
 * @param <T> the type of elements returned by the iterator
 * @see BranchIterator
 * @see Iterable
 */
public interface BranchIterable<T> extends Iterable<T> {
    @Override
    BranchIterator<T> iterator();
}
