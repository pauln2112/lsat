/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend.annotations

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.Target
import java.util.List
import java.util.Optional
import java.util.stream.Stream
import org.eclipse.xtend.lib.macro.AbstractMethodProcessor
import org.eclipse.xtend.lib.macro.Active
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableMethodDeclaration
import org.eclipse.xtend.lib.macro.declaration.Visibility

import static extension org.eclipse.lsat.common.xtend.annotations.TypeReferenceUtil.*

@Retention(SOURCE)
@Target(ElementType.METHOD)
@Active(ResolvableCompilationParticipant)
annotation Resolvable {
    // Empty
}

class ResolvableCompilationParticipant extends AbstractMethodProcessor {
    override doTransform(MutableMethodDeclaration annotatedMethod, extension TransformationContext context) {
        val createCache = annotatedMethod.declaringType.
            findDeclaredField('''_createCache_«annotatedMethod.simpleName»''')
        if (createCache === null) {
            annotatedMethod.addError('''@Resolvable can only be used on create methods.''')
            return
        } else if (annotatedMethod.returnType.isInferred) {
            annotatedMethod.addError('''Please specify an explicit return type for @Resolvable methods''')
            return
        } else if (annotatedMethod.parameters.isEmpty) {
            annotatedMethod.addError('''At least 1 parameter is required for @Resolvable methods.''')
            return
        } else if (!(annotatedMethod.declaringType instanceof MutableClassDeclaration)) {
            annotatedMethod.addError('''@Resolvable methods should be declared on class.''')
            return
        }
        val declaringClass = annotatedMethod.declaringType as MutableClassDeclaration

        if (annotatedMethod.parameters.length == 1) {
            val sourceParameter = annotatedMethod.parameters.head
            declaringClass.addMethod('''_resolveOne_«annotatedMethod.simpleName»''') [
                visibility = Visibility::PRIVATE
                returnType = newTypeReference(Optional, annotatedMethod.returnType)
                addParameter(sourceParameter.simpleName, sourceParameter.type)
                body = '''
                    final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(«sourceParameter.simpleName»);
                    synchronized («createCache.simpleName») {
                        if («createCache.simpleName».containsKey(_cacheKey)) {
                            return Optional.ofNullable(«createCache.simpleName».get(_cacheKey));
                        } else {
                            return Optional.empty();
                        }
                    }
                '''
            ]

            declaringClass.addMethod('''_resolveAll_«annotatedMethod.simpleName»''') [
                visibility = Visibility::PRIVATE
                returnType = newTypeReference(Stream, annotatedMethod.returnType)
                addParameter(sourceParameter.simpleName, sourceParameter.type)
                body = '''
                    final Optional<«annotatedMethod.returnType»> resolved = _resolveOne_«annotatedMethod.simpleName»(«sourceParameter.simpleName»);
                    return Stream.of(resolved).filter(Optional::isPresent).map(Optional::get);
                '''
            ]
        } else {
            for (sourceParameterType : annotatedMethod.parameters.map[type].toSet) {
                declaringClass.addMethod('''_resolveOne_«annotatedMethod.simpleName»''') [
                    visibility = Visibility::PRIVATE
                    returnType = newTypeReference(Optional, annotatedMethod.returnType)
                    addParameter('source', sourceParameterType)
                    body = '''return _resolveAll_events2Dependency(source).findAny();'''
                ]

                declaringClass.addMethod('''_resolveAll_«annotatedMethod.simpleName»''') [
                    visibility = Visibility::PRIVATE
                    returnType = newTypeReference(Stream, annotatedMethod.returnType)
                    addParameter('source', sourceParameterType)
                    body = '''
                        // TODO what about synchronization here?
                        return «createCache.simpleName».entrySet().stream().filter(e -> e.getKey().contains(source)).map(java.util.Map.Entry::getValue);
                    '''
                ]
            }
        }
        
        declaringClass.addMethod('''_invResolveOne_«annotatedMethod.simpleName»''') [
            val genericType = newTypeReference(addTypeParameter('T'))
            val resolveType = newTypeReference(Class, genericType)
            
            visibility = Visibility::PRIVATE
            returnType = newTypeReference(Optional, genericType)
            addParameter('target', annotatedMethod.returnType)
            addParameter('typeToResolve', resolveType)
            body = '''return _invResolveAll_«annotatedMethod.simpleName»(target, typeToResolve).findFirst();'''
        ]

        declaringClass.addMethod('''_invResolveAll_«annotatedMethod.simpleName»''') [
            val genericType = newTypeReference(addTypeParameter('T'))
            val resolveType = newTypeReference(Class, genericType)

            visibility = Visibility::PRIVATE
            returnType = newTypeReference(Stream, genericType)
            addParameter('target', annotatedMethod.returnType)
            addParameter('typeToResolve', resolveType)
            body = '''
                // TODO what about synchronization here?
                return «createCache.simpleName».entrySet().stream().filter(e -> e.getValue() == target)
                        .flatMap(e -> e.getKey().stream()).filter(typeToResolve::isInstance).map(typeToResolve::cast);
            '''
        ]

        doTransform(declaringClass, context)
    }

    def void doTransform(MutableClassDeclaration declaringClass, extension TransformationContext context) {
        if (declaringClass.declaredMethods.exists[simpleName == 'resolveOne']) {
            // Already processed
            return
        }

        val annotationType = Resolvable.findTypeGlobally
        val annotatedMethods = declaringClass.declaredMethods.filter[findAnnotation(annotationType) !== null]
        val sourceTypes = newLinkedHashMap
        val targetTypes = newLinkedHashMap
        annotatedMethods.forEach [ method |
            method.parameters.map[type].forEach [ type |
                sourceTypes.computeIfAbsent(type)[newLinkedHashSet] += method
            ]
            targetTypes.computeIfAbsent(method.returnType)[newLinkedHashSet] += method
        ]
        
        sourceTypes.forEach [ sourceType, annotatedMethodsForSourceType |
            declaringClass.addMethod('resolveOne') [
                val genericType = newTypeReference(addTypeParameter('T'))
                val resolveType = newTypeReference(Class, genericType)
                val superResolve = newTypeReference(declaringClass).findResolvedMethod('resolveOne', sourceType, resolveType)

                visibility = Visibility::PROTECTED
                returnType = genericType
                addParameter('source', sourceType)
                addParameter('typeToResolve', resolveType)
                body = '''
                    «FOR annotatedMethod : annotatedMethodsForSourceType»
                    if (typeToResolve.isAssignableFrom(«annotatedMethod.returnType.type.simpleName».class) || «annotatedMethod.returnType.type.simpleName».class.isAssignableFrom(typeToResolve)) {
                        final Optional<«annotatedMethod.returnType.simpleName»> resolved = _resolveOne_«annotatedMethod.simpleName»(source);
                        if (resolved.isPresent() && typeToResolve.isInstance(resolved.get())) {
                            return typeToResolve.cast(resolved.get());
                        }
                    }
                    «ENDFOR»
                    «IF superResolve === null»
                    return null;
                    «ELSE»
                    return super.resolveOne(source);
                    «ENDIF»
                '''
            ]

            declaringClass.addMethod('resolveAll') [
                val genericType = newTypeReference(addTypeParameter('T'))
                val resolveType = newTypeReference(Class, genericType)
                val superResolve = newTypeReference(declaringClass).findResolvedMethod('resolveAll', sourceType, resolveType)
                
                visibility = Visibility::PROTECTED
                returnType = newTypeReference(List, genericType)
                addParameter('source', sourceType)
                addParameter('typeToResolve', resolveType)
                body = '''
                    List<T> resolved = CollectionLiterals.newLinkedList();
                    «FOR annotatedMethod : annotatedMethodsForSourceType»
                    if (typeToResolve.isAssignableFrom(«annotatedMethod.returnType.type.simpleName».class) || «annotatedMethod.returnType.type.simpleName».class.isAssignableFrom(typeToResolve)) {
                        final Stream<«annotatedMethod.returnType.simpleName»> result = _resolveAll_«annotatedMethod.simpleName»(source);
                        result.filter(typeToResolve::isInstance).map(typeToResolve::cast).forEachOrdered(resolved::add);
                    }
                    «ENDFOR»
                    «IF superResolve !== null»
                    resolved.addAll(super.resolveAll(source));
                    «ENDIF»
                    return resolved;
                '''
            ]
        ]
        
        targetTypes.forEach [ targetType, annotatedMethodsForTargetType |
            declaringClass.addMethod('invResolveOne') [
                val genericType = newTypeReference(addTypeParameter('T'))
                val resolveType = newTypeReference(Class, genericType)
                val superResolve = newTypeReference(declaringClass).findResolvedMethod('invResolveOne', targetType, resolveType)

                returnType = genericType
                visibility = Visibility::PROTECTED
                addParameter('target', targetType)
                addParameter('typeToResolve', resolveType)
                body = '''
                    «FOR annotatedMethod : annotatedMethodsForTargetType»
                    if («FOR param : annotatedMethod.parameters SEPARATOR '||'»«param.type.type.simpleName».class.isAssignableFrom(typeToResolve)«ENDFOR») {
                        final Optional<«genericType.simpleName»> resolved = _invResolveOne_«annotatedMethod.simpleName»(target, typeToResolve);
                        if (resolved.isPresent()) {
                            return resolved.get();
                        }
                    }
                    «ENDFOR»
                    «IF superResolve === null»
                    return null;
                    «ELSE»
                    return super.resolveOne(source);
                    «ENDIF»
                '''
            ]

            declaringClass.addMethod('invResolveAll') [
                val genericType = newTypeReference(addTypeParameter('T'))
                val resolveType = newTypeReference(Class, genericType)
                val superResolve = newTypeReference(declaringClass).findResolvedMethod('invResolveAll', targetType, resolveType)
                
                returnType = newTypeReference(List, genericType)
                visibility = Visibility::PROTECTED
                addParameter('target', targetType)
                addParameter('typeToResolve', resolveType)
                body = '''
                    List<T> resolved = CollectionLiterals.newLinkedList();
                    «FOR annotatedMethod : annotatedMethodsForTargetType»
                    if («FOR param : annotatedMethod.parameters SEPARATOR '||'»«param.type.type.simpleName».class.isAssignableFrom(typeToResolve)«ENDFOR») {
                        _invResolveAll_«annotatedMethod.simpleName»(target, typeToResolve).forEachOrdered(resolved::add);
                    }
                    «ENDFOR»
                    «IF superResolve !== null»
                    resolved.addAll(super.resolveAll(source));
                    «ENDIF»
                    return resolved;
                '''
            ]
        ]        
    }
}
