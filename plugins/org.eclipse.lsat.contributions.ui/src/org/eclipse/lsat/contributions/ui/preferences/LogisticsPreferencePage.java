/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.contributions.ui.preferences;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class LogisticsPreferencePage extends PreferencePage implements IWorkbenchPreferencePage {
    @Override
    public void init(IWorkbench workbench) {
        setDescription("Expand the tree to edit preferences for a specific feature.");
    }

    @Override
    protected Control createContents(Composite parent) {
        return new Composite(parent, SWT.NONE);
    }
}
