/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.simulator.main;

import java.util.Collection;
import java.util.List;
import machine.ActionType;
import machine.Axis;
import machine.IResource;
import machine.Machine;
import machine.Peripheral;
import machine.PeripheralType;
import machine.Resource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.lsat.scheduler.simulator.common.Common;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class GenerateMachineTemplate {
  public static CharSequence generateMachineJavaScript(final Machine machine) {
    CharSequence _xblockexpression = null;
    {
      int index = 1;
      StringConcatenation _builder = new StringConcatenation();
      {
        final Function1<Resource, Collection<? extends IResource>> _function = (Resource it) -> {
          return Common.getItemsOrResource(it);
        };
        final Function1<IResource, String> _function_1 = (IResource it) -> {
          return it.fqn();
        };
        List<IResource> _sortBy = IterableExtensions.<IResource, String>sortBy(Queries.<Resource, IResource>xcollect(machine.getResources(), _function), _function_1);
        for(final IResource resource : _sortBy) {
          {
            final Function1<Peripheral, String> _function_2 = (Peripheral it) -> {
              return it.getName();
            };
            List<Peripheral> _sortBy_1 = IterableExtensions.<Peripheral, String>sortBy(resource.getResource().getPeripherals(), _function_2);
            for(final Peripheral peripheral : _sortBy_1) {
              _builder.append("var ");
              String _iD = Common.getID(resource, peripheral);
              _builder.append(_iD);
              _builder.append(" = {");
              _builder.newLineIfNotEmpty();
              _builder.append("  ");
              _builder.append("dot: null,");
              _builder.newLine();
              _builder.append("  ");
              int _plusPlus = index++;
              CharSequence _generateInit = GenerateMachineTemplate.generateInit(peripheral, _plusPlus);
              _builder.append(_generateInit, "  ");
              {
                EList<ActionType> _actions = peripheral.getType().getActions();
                for(final ActionType action : _actions) {
                  _builder.append(", ");
                  _builder.newLineIfNotEmpty();
                  _builder.append("  ");
                  CharSequence _generateAction = GenerateMachineTemplate.generateAction(action);
                  _builder.append(_generateAction, "  ");
                }
              }
              {
                if (((!peripheral.getPositions().isEmpty()) || (!peripheral.getDistances().isEmpty()))) {
                  _builder.append(", ");
                  _builder.newLineIfNotEmpty();
                  _builder.append("  ");
                  CharSequence _generateMove = GenerateMachineTemplate.generateMove(peripheral);
                  _builder.append(_generateMove, "  ");
                }
              }
              _builder.newLineIfNotEmpty();
              _builder.append("}");
              _builder.newLine();
              _builder.newLine();
            }
          }
        }
      }
      _builder.append("// Auto generated, do not modify!");
      _builder.newLine();
      {
        final Function1<PeripheralType, EList<ActionType>> _function_3 = (PeripheralType it) -> {
          return it.getActions();
        };
        final Function1<ActionType, String> _function_4 = (ActionType it) -> {
          return it.getName();
        };
        List<ActionType> _sortBy_2 = IterableExtensions.<ActionType, String>sortBy(Queries.<PeripheralType, ActionType>xcollect(machine.getPeripheralTypes(), _function_3), _function_4);
        for(final ActionType action_1 : _sortBy_2) {
          _builder.append("function ");
          String _name = action_1.getName();
          _builder.append(_name);
          _builder.append("(peripheral, timeFrom, timeTo, time) {");
          _builder.newLineIfNotEmpty();
          _builder.append("  ");
          _builder.append("var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);");
          _builder.newLine();
          _builder.append("  ");
          _builder.append("if (percentageComplete == undefined) return; // Outside time scope");
          _builder.newLine();
          _builder.append("  ");
          _builder.append("peripheral.");
          String _name_1 = action_1.getName();
          _builder.append(_name_1, "  ");
          _builder.append("(percentageComplete);");
          _builder.newLineIfNotEmpty();
          _builder.append("}");
          _builder.newLine();
          _builder.newLine();
        }
      }
      _builder.newLine();
      _builder.append("function move(peripheral, from, to, timeFrom, timeTo, time) {");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("if (percentageComplete == undefined) return; // Outside time scope");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("peripheral.move(from, to, percentageComplete);");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("function toPercentageComplete(timeFrom, timeTo, time) {");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("if (time < timeFrom) return;");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("if (time > timeTo + 0.05) return;");
      _builder.newLine();
      _builder.append("  ");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("var deltaTime = timeTo - timeFrom;");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("var timeIn = time - timeFrom;");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("var ratio = timeIn/deltaTime;");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("return Math.min(ratio, 1);");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("function plusIs(base, augment) {");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("for (key in augment) {");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("base[key] += augment[key];");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("}");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("return base;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("function clone(base) {");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("return JSON.parse(JSON.stringify(base));");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  private static CharSequence generateInit(final Peripheral peripheral, final int index) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("init: function(");
    {
      boolean _isEmpty = peripheral.getPositions().isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        _builder.append("position");
      }
    }
    _builder.append(") {");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("// TODO: Implement this");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("this.dot = new Path.Circle(new Point(");
    _builder.append((index * 20), "  ");
    _builder.append(", 10), 8);");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("this.dot.fillColor = \'SteelBlue\';");
    _builder.newLine();
    _builder.append("}");
    return _builder;
  }
  
  private static CharSequence generateAction(final ActionType action) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = action.getName();
    _builder.append(_name);
    _builder.append(": function(percentageComplete) {");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.append("if (percentageComplete < 1) {");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("// TODO: Working on it...");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("this.dot.fillColor = \'Crimson\';");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("} else {");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("// TODO: Done!");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("this.dot.fillColor = \'SteelBlue\';");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    return _builder;
  }
  
  private static CharSequence generateMove(final Peripheral peripheral) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("move: function(from, to, percentageComplete) {");
    _builder.newLine();
    {
      EList<Axis> _axes = peripheral.getType().getAxes();
      for(final Axis axis : _axes) {
        _builder.append("  ");
        _builder.append("var new");
        String _name = axis.getName();
        _builder.append(_name, "  ");
        _builder.append(" = from.");
        String _name_1 = axis.getName();
        _builder.append(_name_1, "  ");
        _builder.append(" + (to.");
        String _name_2 = axis.getName();
        _builder.append(_name_2, "  ");
        _builder.append(" - from.");
        String _name_3 = axis.getName();
        _builder.append(_name_3, "  ");
        _builder.append(") * percentageComplete;");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("  ");
    _builder.append("// TODO: Move it");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("if (percentageComplete < 1) {");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("// TODO: Working on it...");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("this.dot.fillColor = \'Gold\';");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("} else {");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("// TODO: Done!");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("this.dot.fillColor = \'SteelBlue\';");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    return _builder;
  }
}
