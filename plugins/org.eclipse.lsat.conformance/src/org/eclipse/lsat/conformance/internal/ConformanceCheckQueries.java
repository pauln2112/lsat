/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance.internal;

import static java.util.Arrays.asList;
import static org.eclipse.lsat.conformance.internal.UniqueTracePoint.create;

import java.util.ArrayList;
import java.util.List;

import activity.Action;
import activity.Claim;
import activity.Release;

public final class ConformanceCheckQueries {
    private ConformanceCheckQueries() {
        /* Empty */
    }

    /**
     * <tt>action</tt>->xcollect(a | new Sequence(UniqueTracePoint) { new UniqueTracePoint(a.outerEntry), new
     * UniqueTracePoint(a.entry), new UniqueTracePoint(a.exit), new UniqueTracePoint(a.outerExit) })->flatten()
     */
    public static final List<UniqueTracePoint> getTracePoints(Action action) {
        return asList(create(action.getOuterEntry()), create(action.getEntry()), create(action.getExit()),
                create(action.getOuterExit()));
    }

    public static final List<UniqueTracePoint> getClaimedTracePoints(Action action) {
        ArrayList<UniqueTracePoint> result = new ArrayList<UniqueTracePoint>(4);
        if (!(action instanceof Claim)) {
            result.add(create(action.getOuterEntry()));
            result.add(create(action.getEntry()));
        }
        if (!(action instanceof Release)) {
            result.add(create(action.getExit()));
            result.add(create(action.getOuterExit()));
        }
        return result;
    }
}
