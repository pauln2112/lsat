/**
 */
package machine.util;

import java.util.Map;

import machine.*;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see machine.MachinePackage
 * @generated
 */
public class MachineSwitch<T> extends Switch<T> {
	/**
     * The cached model package
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static MachinePackage modelPackage;

	/**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MachineSwitch() {
        if (modelPackage == null)
        {
            modelPackage = MachinePackage.eINSTANCE;
        }
    }

	/**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

	/**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID)
        {
            case MachinePackage.PERIPHERAL_TYPE:
            {
                PeripheralType peripheralType = (PeripheralType)theEObject;
                T result = casePeripheralType(peripheralType);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.PATH:
            {
                Path path = (Path)theEObject;
                T result = casePath(path);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.SYMBOLIC_POSITION:
            {
                SymbolicPosition symbolicPosition = (SymbolicPosition)theEObject;
                T result = caseSymbolicPosition(symbolicPosition);
                if (result == null) result = casePosition(symbolicPosition);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.RESOURCE:
            {
                Resource resource = (Resource)theEObject;
                T result = caseResource(resource);
                if (result == null) result = caseIResource(resource);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.ACTION_TYPE:
            {
                ActionType actionType = (ActionType)theEObject;
                T result = caseActionType(actionType);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.PERIPHERAL:
            {
                Peripheral peripheral = (Peripheral)theEObject;
                T result = casePeripheral(peripheral);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.MACHINE:
            {
                Machine machine = (Machine)theEObject;
                T result = caseMachine(machine);
                if (result == null) result = caseImportContainer(machine);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.IMPORT_CONTAINER:
            {
                ImportContainer importContainer = (ImportContainer)theEObject;
                T result = caseImportContainer(importContainer);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.IMPORT:
            {
                Import import_ = (Import)theEObject;
                T result = caseImport(import_);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.PROFILE:
            {
                Profile profile = (Profile)theEObject;
                T result = caseProfile(profile);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.AXIS:
            {
                Axis axis = (Axis)theEObject;
                T result = caseAxis(axis);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.POSITION:
            {
                Position position = (Position)theEObject;
                T result = casePosition(position);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.SET_POINT:
            {
                SetPoint setPoint = (SetPoint)theEObject;
                T result = caseSetPoint(setPoint);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.AXIS_POSITION_MAP_ENTRY:
            {
                @SuppressWarnings("unchecked") Map.Entry<Axis, Position> axisPositionMapEntry = (Map.Entry<Axis, Position>)theEObject;
                T result = caseAxisPositionMapEntry(axisPositionMapEntry);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.AXIS_POSITIONS_MAP_ENTRY:
            {
                @SuppressWarnings("unchecked") Map.Entry<Axis, EList<Position>> axisPositionsMapEntry = (Map.Entry<Axis, EList<Position>>)theEObject;
                T result = caseAxisPositionsMapEntry(axisPositionsMapEntry);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.PATH_TARGET_REFERENCE:
            {
                PathTargetReference pathTargetReference = (PathTargetReference)theEObject;
                T result = casePathTargetReference(pathTargetReference);
                if (result == null) result = caseHasSettling(pathTargetReference);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.UNIDIRECTIONAL_PATH:
            {
                UnidirectionalPath unidirectionalPath = (UnidirectionalPath)theEObject;
                T result = caseUnidirectionalPath(unidirectionalPath);
                if (result == null) result = casePath(unidirectionalPath);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.BIDIRECTIONAL_PATH:
            {
                BidirectionalPath bidirectionalPath = (BidirectionalPath)theEObject;
                T result = caseBidirectionalPath(bidirectionalPath);
                if (result == null) result = casePath(bidirectionalPath);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.FULL_MESH_PATH:
            {
                FullMeshPath fullMeshPath = (FullMeshPath)theEObject;
                T result = caseFullMeshPath(fullMeshPath);
                if (result == null) result = casePath(fullMeshPath);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.PATH_ANNOTATION:
            {
                PathAnnotation pathAnnotation = (PathAnnotation)theEObject;
                T result = casePathAnnotation(pathAnnotation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.RESOURCE_ITEM:
            {
                ResourceItem resourceItem = (ResourceItem)theEObject;
                T result = caseResourceItem(resourceItem);
                if (result == null) result = caseIResource(resourceItem);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.IRESOURCE:
            {
                IResource iResource = (IResource)theEObject;
                T result = caseIResource(iResource);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.DISTANCE:
            {
                Distance distance = (Distance)theEObject;
                T result = caseDistance(distance);
                if (result == null) result = caseHasSettling(distance);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.HAS_RESOURCE_PERIPHERAL:
            {
                HasResourcePeripheral hasResourcePeripheral = (HasResourcePeripheral)theEObject;
                T result = caseHasResourcePeripheral(hasResourcePeripheral);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case MachinePackage.HAS_SETTLING:
            {
                HasSettling hasSettling = (HasSettling)theEObject;
                T result = caseHasSettling(hasSettling);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Peripheral Type</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Peripheral Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePeripheralType(PeripheralType object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Path</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Path</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePath(Path object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Symbolic Position</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Symbolic Position</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseSymbolicPosition(SymbolicPosition object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Resource</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseResource(Resource object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Action Type</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Action Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseActionType(ActionType object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Peripheral</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Peripheral</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePeripheral(Peripheral object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Machine</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Machine</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseMachine(Machine object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Import Container</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Import Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseImportContainer(ImportContainer object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Import</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Import</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseImport(Import object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Profile</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Profile</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseProfile(Profile object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Axis</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Axis</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseAxis(Axis object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Position</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Position</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePosition(Position object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Set Point</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Set Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseSetPoint(SetPoint object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Axis Position Map Entry</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Axis Position Map Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseAxisPositionMapEntry(Map.Entry<Axis, Position> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Axis Positions Map Entry</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Axis Positions Map Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseAxisPositionsMapEntry(Map.Entry<Axis, EList<Position>> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Path Target Reference</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Path Target Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePathTargetReference(PathTargetReference object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Unidirectional Path</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Unidirectional Path</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseUnidirectionalPath(UnidirectionalPath object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Bidirectional Path</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Bidirectional Path</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseBidirectionalPath(BidirectionalPath object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Full Mesh Path</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Full Mesh Path</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseFullMeshPath(FullMeshPath object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Path Annotation</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Path Annotation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePathAnnotation(PathAnnotation object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Resource Item</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Resource Item</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseResourceItem(ResourceItem object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>IResource</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IResource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseIResource(IResource object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Distance</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Distance</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseDistance(Distance object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Has Resource Peripheral</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Has Resource Peripheral</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseHasResourcePeripheral(HasResourcePeripheral object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Has Settling</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Has Settling</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseHasSettling(HasSettling object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
	@Override
	public T defaultCase(EObject object) {
        return null;
    }

} //MachineSwitch
