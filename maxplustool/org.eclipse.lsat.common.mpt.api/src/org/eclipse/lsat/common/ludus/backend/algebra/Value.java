/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.algebra;

/**
 * Max-plus value.
 *
 * @author Bram van der Sanden
 */
public class Value implements Comparable<Value> {
    public static final Value NEGATIVE_INFINITY = new Value(Double.NEGATIVE_INFINITY);

    public static final Value POSITIVE_INFINITY = new Value(Double.POSITIVE_INFINITY);

    private Double value;

    public Value(Double value) {
        this.value = new Double(value);
    }

    public Value(int value) {
        this.value = new Double(value);
    }

    public Value abs() {
        return new Value(Math.abs(value));
    }

    public Value max(Value other) {
        return new Value(Math.max(value, other.value));
    }

    public Value min(Value other) {
        return new Value(Math.min(value, other.value));
    }

    public Value add(Value other) {
        return new Value(value + other.value);
    }

    public Value multiply(Value other) {
        return new Value(value * other.value);
    }

    public Value divide(Value other) {
        return new Value(value / other.value);
    }

    public Value subtract(Value other) {
        assert (!other.equals(NEGATIVE_INFINITY));
        return new Value(value - other.value);
    }

    public static Value max(Value a, Value b) {
        return a.max(b);
    }

    public int signum() {
        Double signum = Math.signum(value);
        return signum.intValue();
    }

    public boolean smallerThan(Value other) {
        return value.compareTo(other.value) < 0;
    }

    public boolean biggerThan(Value other) {
        return value.compareTo(other.value) > 0;
    }

    public Double toDouble() {
        return value.doubleValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Value)) {
            return false;
        }

        Value value1 = (Value)o;

        return value.equals(value1.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public int compareTo(Value o) {
        return value.compareTo(o.value);
    }

    public Double getValue() {
        return value;
    }
}
