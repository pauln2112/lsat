/**
 */
package lsat_graph.impl;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.common.graph.directed.impl.AspectImpl;

import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;

import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;

import lsat_graph.StochasticAnnotation;
import lsat_graph.lsat_graphPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stochastic Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.impl.StochasticAnnotationImpl#getWeight <em>Weight</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StochasticAnnotationImpl extends AspectImpl<ScheduledTask<Task>, ScheduledDependency> implements StochasticAnnotation {
	/**
     * The default value of the '{@link #getWeight() <em>Weight</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getWeight()
     * @generated
     * @ordered
     */
	protected static final BigDecimal WEIGHT_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getWeight() <em>Weight</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getWeight()
     * @generated
     * @ordered
     */
	protected BigDecimal weight = WEIGHT_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected StochasticAnnotationImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.STOCHASTIC_ANNOTATION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * This is specialized for the more specific element type known in this context.
     * @generated
     */
	@Override
	public EList<ScheduledDependency> getEdges() {
        if (edges == null)
        {
            edges = new EObjectWithInverseResolvingEList.ManyInverse<ScheduledDependency>(ScheduledDependency.class, this, lsat_graphPackage.STOCHASTIC_ANNOTATION__EDGES, DirectedGraphPackage.EDGE__ASPECTS) { private static final long serialVersionUID = 1L; @Override public Class<?> getInverseFeatureClass() { return Edge.class; } };
        }
        return edges;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * This is specialized for the more specific element type known in this context.
     * @generated
     */
	@Override
	public EList<ScheduledTask<Task>> getNodes() {
        if (nodes == null)
        {
            nodes = new EObjectWithInverseResolvingEList.ManyInverse<ScheduledTask<Task>>(ScheduledTask.class, this, lsat_graphPackage.STOCHASTIC_ANNOTATION__NODES, DirectedGraphPackage.NODE__ASPECTS) { private static final long serialVersionUID = 1L; @Override public Class<?> getInverseFeatureClass() { return Node.class; } };
        }
        return nodes;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getWeight() {
        return weight;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setWeight(BigDecimal newWeight) {
        BigDecimal oldWeight = weight;
        weight = newWeight;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, lsat_graphPackage.STOCHASTIC_ANNOTATION__WEIGHT, oldWeight, weight));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case lsat_graphPackage.STOCHASTIC_ANNOTATION__WEIGHT:
                return getWeight();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case lsat_graphPackage.STOCHASTIC_ANNOTATION__WEIGHT:
                setWeight((BigDecimal)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.STOCHASTIC_ANNOTATION__WEIGHT:
                setWeight(WEIGHT_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.STOCHASTIC_ANNOTATION__WEIGHT:
                return WEIGHT_EDEFAULT == null ? weight != null : !WEIGHT_EDEFAULT.equals(weight);
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (weight: ");
        result.append(weight);
        result.append(')');
        return result.toString();
    }

} //StochasticAnnotationImpl
