/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import static org.eclipse.lsat.common.emf.ecore.resource.ResourceDiagnosticException.UNKNOWN_COLUMN;
import static org.eclipse.lsat.common.emf.ecore.resource.ResourceDiagnosticException.UNKNOWN_LINE;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.eclipse.lsat.common.emf.ecore.resource.ResourceDiagnosticException.Severity;

class ResourceDiagnosticInfo implements org.eclipse.emf.common.util.Diagnostic, Diagnostic {
    /**
     * Default source. If diagnostic logged by a job to the Eclipse error log, then 'source' is used as plug-in ID, and
     * it must never be 'null'. If it is 'null', opening the event details of that log event crashes with a
     * {@link NullPointerException}.
     */
    private static final String DEFAULT_SOURCE = ResourceDiagnosticInfo.class.getPackage().getName();

    private final Severity severity;

    private final String source;

    private final int code;

    private final String message;

    private final Throwable exception;

    private URI location;

    private int line;

    private int column;

    // LinkedHashMap ensures that insertion order is maintained, providing control over the output.
    private final Map<String, Object> details = new LinkedHashMap<>(3);

    public ResourceDiagnosticInfo(Severity severity, String source, int code, String message, Throwable exception,
            URI location, int line, int column)
    {
        this.severity = severity;
        this.source = source == null ? DEFAULT_SOURCE : source;
        this.code = code;
        this.message = message;
        this.exception = exception;
        this.location = location;
        this.line = line;
        this.column = column;
    }

    @Override
    public int getSeverity() {
        return severity.getSeverity();
    }

    public Severity getSeverityEnum() {
        return severity;
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public int getCode() {
        return code;
    }

    public String getOriginalMessage() {
        return message;
    }

    @Override
    public String getMessage() {
        return toString();
    }

    @Override
    public Throwable getException() {
        return exception;
    }

    @Override
    public String getLocation() {
        return location == null ? null : location.toString();
    }

    public URI getLocationURI() {
        return location;
    }

    public void setLocationURI(URI location) {
        this.location = location;
    }

    @Override
    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    @Override
    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public void addDetail(String key, Object value) {
        if (details.containsKey(key)) {
            throw new IllegalArgumentException("Detail for key '" + key + "' already present.");
        }
        details.put(key, value);
    }

    public void getDetail(String key, Object value) {
        if (!details.containsKey(key)) {
            throw new IllegalArgumentException("Detail for key '" + key + "' not present.");
        }
        details.put(key, value);
    }

    @Override
    public List<?> getData() {
        // Add resource diagnostic (via 'info') to the data for marker generation, see
        // org.eclipse.emf.edit.ui.util.EditUIMarkerHelper#adjustMarker(IMarker, Diagnostic).
        return (exception == null) ? Arrays.asList(this) : Arrays.asList(exception, this);
    }

    @Override
    public List<org.eclipse.emf.common.util.Diagnostic> getChildren() {
        return Collections.emptyList();
    }

    @Override
    public String toString() {
        StringBuilder detailsText = new StringBuilder();
        if (location != null) {
            detailsText.append(location);
        }
        if (line != UNKNOWN_LINE) {
            if (detailsText.length() > 0) {
                detailsText.append(" ");
            }
            detailsText.append("at line ").append(line);
            if (column != UNKNOWN_COLUMN) {
                detailsText.append(" : ").append(column);
            }
        }
        for (Entry<String, Object> entry: details.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (detailsText.length() > 0) {
                detailsText.append(", ");
            }
            detailsText.append(key).append(": ").append(value);
        }

        StringBuilder result = new StringBuilder();
        result.append(message == null ? "<no message>" : message);
        if (detailsText.length() > 0) {
            result.append(" (").append(detailsText).append(")");
        }

        return result.toString();
    }
}
