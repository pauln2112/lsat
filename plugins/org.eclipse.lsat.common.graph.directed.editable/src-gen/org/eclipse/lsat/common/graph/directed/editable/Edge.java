/**
 */
package org.eclipse.lsat.common.graph.directed.editable;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getSource <em>Source</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getSourceNode <em>Source Node</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getTarget <em>Target</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getTargetNode <em>Target Node</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getGraph <em>Graph</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEdge()
 * @model
 * @generated
 */
public interface Edge extends EdgeTarget {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getEdge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' containment reference.
	 * @see #setSource(SourceReference)
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEdge_Source()
	 * @see org.eclipse.lsat.common.graph.directed.editable.SourceReference#getEdge
	 * @model opposite="edge" containment="true" required="true"
	 * @generated
	 */
	SourceReference getSource();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getSource <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' containment reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(SourceReference value);

	/**
	 * Returns the value of the '<em><b>Source Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getOutgoingEdges <em>Outgoing Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Node</em>' reference.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEdge_SourceNode()
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getOutgoingEdges
	 * @model opposite="outgoingEdges" resolveProxies="false" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Node getSourceNode();

	/**
	 * Returns the value of the '<em><b>Target</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.EdgeTarget#getEdge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' containment reference.
	 * @see #setTarget(EdgeTarget)
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEdge_Target()
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgeTarget#getEdge
	 * @model opposite="edge" containment="true" required="true"
	 * @generated
	 */
	EdgeTarget getTarget();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getTarget <em>Target</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' containment reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(EdgeTarget value);

	/**
	 * Returns the value of the '<em><b>Target Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getIncomingEdges <em>Incoming Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Node</em>' reference.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEdge_TargetNode()
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getIncomingEdges
	 * @model opposite="incomingEdges" resolveProxies="false" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	Node getTargetNode();

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' reference.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEdge_Graph()
	 * @model resolveProxies="false" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EditableDirectedGraph getGraph();

} // Edge
