/**
 */
package lsat_graph.impl;

import activity.RequireEvent;
import lsat_graph.RequireTask;
import lsat_graph.lsat_graphPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Require Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequireTaskImpl extends EventTaskImpl<RequireEvent> implements RequireTask
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected RequireTaskImpl()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass()
    {
        return lsat_graphPackage.Literals.REQUIRE_TASK;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * This is specialized for the more specific type known in this context.
     * @generated
     */
    @Override
    public void setAction(RequireEvent newAction)
    {
        super.setAction(newAction);
    }

} //RequireTaskImpl
