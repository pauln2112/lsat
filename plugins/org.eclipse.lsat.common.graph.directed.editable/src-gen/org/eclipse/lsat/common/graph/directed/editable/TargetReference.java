/**
 */
package org.eclipse.lsat.common.graph.directed.editable;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Target Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.TargetReference#getNode <em>Node</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getTargetReference()
 * @model
 * @generated
 */
public interface TargetReference extends EdgeTarget {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getTargetReferences <em>Target References</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' reference.
	 * @see #setNode(Node)
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getTargetReference_Node()
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getTargetReferences
	 * @model opposite="targetReferences" resolveProxies="false" required="true"
	 * @generated
	 */
	Node getNode();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.editable.TargetReference#getNode <em>Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node</em>' reference.
	 * @see #getNode()
	 * @generated
	 */
	void setNode(Node value);

} // TargetReference
