/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.ui.viewer.IViewerProvider;
import org.eclipse.emf.edit.ui.action.EditingDomainActionBarContributor;
import org.eclipse.emf.edit.ui.action.RedoAction;
import org.eclipse.emf.edit.ui.action.UndoAction;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.lsat.timing.Activator;
import org.eclipse.lsat.timing.view.MotionViewJob;

import activity.Move;

/**
 * Manages the installation/deinstallation of global actions for multi-page editors. Responsible for the redirection of
 * global actions to the active editor. Multi-page contributor replaces the contributors for the individual editors in
 * the multi-page editor.
 */
public class ResourceThroughputEditorContributor extends EditingDomainActionBarContributor {
    private final XYPlotViewAction xyPlotViewAction = new XYPlotViewAction();

    private Viewer getActiveViewer() {
        return getActiveEditor() instanceof IViewerProvider ? ((IViewerProvider)getActiveEditor()).getViewer() : null;
    }

    private Object getActiveSelection() {
        final Viewer activeViewer = getActiveViewer();
        if (null == activeViewer)
            return null;
        final ISelection activeSelection = getActiveViewer().getSelection();
        if (activeSelection.isEmpty())
            return null;
        return ((StructuredSelection)activeSelection).getFirstElement();
    }

    @Override
    protected UndoAction createUndoAction() {
        return new UndoAction() {
            @Override
            public void run() {
                super.run();
                ((ResourceThroughputEditorPart)getActiveEditor()).refresh(true);
            }
        };
    }

    @Override
    protected RedoAction createRedoAction() {
        return new RedoAction() {
            @Override
            public void run() {
                super.run();
                ((ResourceThroughputEditorPart)getActiveEditor()).refresh(true);
            }
        };
    }

    @Override
    public void menuAboutToShow(IMenuManager menuManager) {
        super.menuAboutToShow(menuManager);

        xyPlotViewAction.update();
        if (xyPlotViewAction.isEnabled()) {
            menuManager.insertAfter("additions", xyPlotViewAction);
        }
    }

    private class XYPlotViewAction extends Action {
        public XYPlotViewAction() {
            super("Plot in Chart View", Activator.getDefault().getImageRegistry().getDescriptor(Activator.IMAGE_CHART));
        }

        private void update() {
            setEnabled(getActiveSelection() instanceof Move);
        }

        @Override
        public void run() {
            final Object selection = getActiveSelection();
            if (selection instanceof Move) {
                final Job job = new MotionViewJob((Move)selection);
                job.setUser(true);
                job.schedule();
            }
        }
    }
}
