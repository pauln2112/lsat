/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

public final class IterableUtil {
    private IterableUtil() {
        // Empty for utility classes
    }

    /**
     * @return all values of {@code getter}, applied on {@code owner}, from 0 until {@code size}
     */
    public static <E, T> Iterable<E> iterate(T owner, ObjIntFunction<? super T, E> getter,
            ToIntFunction<? super T> size)
    {
        return new SuppliedIterable<E>(() -> IteratorUtil.iterate(owner, getter, size));
    }

    public static <E> Iterable<E> unique(Iterable<E> source) {
        return new UniqueIterable<E>(source);
    }

    public static <E> Iterable<E> notNull(Iterable<E> source) {
        return new NullSkippingIterable<E>(source);
    }

    /**
     * Joins the iterables into one iterable while iterating.
     *
     * @param iterables input iterables.
     * @return single joined/merged output iterable.
     */
    @SafeVarargs
    public static final <T> Iterable<T> join(final Iterable<? extends T>... iterables) {
        return wrap(asList(iterables), i -> {
            // Somehow the Oracle JDK needs this explicit variable as the inlined version
            // below doesn't compile
            Iterator<Iterator<? extends T>> mapped = IteratorUtil.map(i, Iterable::iterator);
            return new CompoundIterator<T>(mapped);
        });
        // Inlined version of query
        // return wrap(asList(iterables), i -> new CompoundIterator<T>(IteratorUtil.map(i, Iterable::iterator)));
    }

    /**
     * Creates a {@link String} representation of an {@link Iterable} using the {@link Object#toString()} method and
     * <code>separator</code> to separate the elements. The resulting {@link String} is wrapped with <code>begin</code>
     * and <code>end</code>.
     *
     * @param source The source iterable
     * @param separator The spearator
     * @param begin The begin
     * @param end The end
     * @return The String representation of the source iterable
     */
    public static <E> String joinfields(Iterable<E> source, CharSequence separator, CharSequence begin,
            CharSequence end)
    {
        StringBuilder result = new StringBuilder(begin);
        for (Iterator<E> i = source.iterator(); i.hasNext();) {
            result.append(i.next());
            if (i.hasNext()) {
                result.append(separator);
            }
        }
        result.append(end);
        return result.toString();
    }

    /**
     * Returns <tt>true</tt> if the iterable contains the specified element. More formally, returns <tt>true</tt> if and
     * only if this iterable contains at least one element <tt>e</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     *
     * @param iterable iterable
     * @param o element whose presence in this iterable is to be tested
     * @return <tt>true</tt> if this iterable contains the specified element
     * @throws ClassCastException if the type of the specified element is incompatible with this iterator
     *     (<a href="#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this collection does not permit null elements
     *     (<a href="#optional-restrictions">optional</a>)
     * @see Collection#contains(Object)
     */
    public static final boolean contains(Iterable<?> iterable, Object o) {
        return iterable instanceof Collection ? ((Collection<?>)iterable).contains(o)
                : IteratorUtil.contains(iterable.iterator(), o);
    }

    /**
     * Returns the minimum element of the given iterable, according to the <i>natural ordering</i> of its elements. All
     * elements in the iterable must implement the <tt>Comparable</tt> interface. Furthermore, all elements in the
     * iterable must be <i>mutually comparable</i> (that is, <tt>e1.compareTo(e2)</tt> must not throw a
     * <tt>ClassCastException</tt> for any elements <tt>e1</tt> and <tt>e2</tt> in the iterable).
     *
     * <p>
     * This method iterates over the entire iterable, hence it requires time proportional to the size of the iterable.
     * </p>
     *
     * @param iterable the iterable whose minimum element is to be determined.
     * @param _default result if the collection is empty.
     * @return the minimum element of the given iterable, according to the <i>natural ordering</i> of its elements, or
     *     <tt>_default</tt> if the iterable is empty.
     * @see Comparable
     * @see Collections#min(Collection)
     */
    public static final <T extends Comparable<? super T>> T min(Iterable<? extends T> iterable, T _default) {
        return (null == iterable) ? _default : IteratorUtil.min(iterable.iterator(), _default);
    }

    /**
     * Returns the maximum element of the given iterable, according to the <i>natural ordering</i> of its elements. All
     * elements in the iterable must implement the <tt>Comparable</tt> interface. Furthermore, all elements in the
     * iterable must be <i>mutually comparable</i> (that is, <tt>e1.compareTo(e2)</tt> must not throw a
     * <tt>ClassCastException</tt> for any elements <tt>e1</tt> and <tt>e2</tt> in the iterable).
     *
     * <p>
     * This method iterates over the entire iterable, hence it requires time proportional to the size of the iterable.
     * </p>
     *
     * @param iterable the iterable whose maximum element is to be determined.
     * @param _default result if the collection is empty.
     * @return the maximum element of the given iterable, according to the <i>natural ordering</i> of its elements, or
     *     <tt>_default</tt> if the iterable is empty.
     * @see Comparable
     * @see Collections#max(Collection)
     */
    public static final <T extends Comparable<? super T>> T max(Iterable<? extends T> iterable, T _default) {
        return (null == iterable) ? _default : IteratorUtil.max(iterable.iterator(), _default);
    }

    /**
     * Returns the size of the given iterable.
     *
     * <p>
     * This method iterates over the entire iterable, hence it requires time proportional to the size of the iterable.
     * </p>
     *
     * @param iterable the iterable whose size is to be determined.
     * @return the size of the iterable
     * @see Collection#size()
     */
    public static int size(Iterable<?> iterable) {
        if (iterable instanceof Collection) {
            return ((Collection<?>)iterable).size();
        }
        int size = 0;
        for (@SuppressWarnings("unused") Object o: iterable) {
            size++;
        }
        return size;
    }

    /**
     * This implementation returns <tt>size() == 0</tt>.
     *
     * @param iterable source for size
     * @return {@code true} if the iterable is empty, {@code false} otherwise.
     */
    public static boolean isEmpty(Iterable<?> iterable) {
        if (iterable instanceof Collection) {
            return ((Collection<?>)iterable).isEmpty();
        }
        return !iterable.iterator().hasNext();
    }

    /**
     * Return first element or null if iterable is empty.
     *
     * @param iterable source for element
     * @return the first element, or {@code null}.
     */
    public static <T> T first(Iterable<T> iterable) {
        if (iterable instanceof Queue) {
            return ((Queue<T>)iterable).peek();
        }
        return IteratorUtil.safeNext(iterable.iterator());
    }

    /**
     * Return last element or null if iterable is empty.<br>
     * <b>NOTE:</b> This method will consume the whole iterable, but is memory efficient
     *
     * @param iterable source for element
     * @return the last element, or {@code null}.
     */
    public static <T> T last(Iterable<T> iterable) {
        if (iterable instanceof Deque) {
            return ((Deque<T>)iterable).peekLast();
        }
        T last = null;
        for (Iterator<T> i = iterable.iterator(); i.hasNext();) {
            last = i.next();
        }
        return last;
    }

    public static <T> Iterable<T> drop(Iterable<T> iterable, int amount) {
        return wrap(iterable, i -> IteratorUtil.drop(i, amount));
    }

    /**
     * Sorts the {@link Iterable} by the output of the functor.<br>
     * <b>NOTE:</b> This method will consume the whole {@link Iterable} on invocation as it needs an intermediate
     * {@link List} for the sort.
     */
    public static <T, C extends Comparable<? super C>> List<T> sortedBy(Iterable<T> source,
            Function<? super T, C> functor)
    {
        return sortedBy(source, functor, new Comparator<C>() {
            @Override
            public int compare(C o1, C o2) {
                return o1.compareTo(o2);
            }
        });
    }

    /**
     * Sorts the {@link Iterable} by the output of the functor.<br>
     * <b>NOTE:</b> This method will consume the whole {@link Iterable} on invocation as it needs an intermediate
     * {@link List} for the sort.
     */
    public static <T, C> List<T> sortedBy(Iterable<T> source, final Function<? super T, C> functor,
            final Comparator<? super C> comparator)
    {
        List<T> result = asList(source);
        Collections.sort(result, new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return Objects.compare(functor.apply(o1), functor.apply(o2), comparator);
            }
        });
        return result;
    }

    /**
     * Creates an reversed view on the list without having to modify the original list and without the need for a
     * temporary list.
     *
     * @return the list as an Iterable of which the elements will be in reversed order (compared to the original list)
     */
    public static <Input> Iterable<Input> inReversedOrder(final List<Input> list) {
        return new LoggableIterable<Input>() {
            @Override
            public Iterator<Input> iterator() {
                return new Iterator<Input>() {
                    private final ListIterator<Input> iterator = list.listIterator(list.size());

                    @Override
                    public boolean hasNext() {
                        return iterator.hasPrevious();
                    }

                    @Override
                    public Input next() {
                        return iterator.previous();
                    }

                    @Override
                    public void remove() {
                        iterator.remove();
                    }
                };
            }
        };
    }

    /**
     * Returns an array containing all of the elements in this iterable in proper sequence (from first to last element).
     *
     * <p>
     * The returned array will be "safe" in that no references to it are maintained by this iterable. (In other words,
     * this method must allocate a new array). The caller is thus free to modify the returned array.
     * </p>
     * <p>
     * This method acts as bridge between array-based and collection-based APIs.
     * </p>
     *
     * @param iterable the elements of the iterable are to be stored
     * @return an array containing the elements of the iterable
     * @throws NullPointerException if the specified array is null
     * @see Collection#toArray()
     */
    public static Object[] toArray(Iterable<?> iterable) {
        if (iterable instanceof Collection) {
            return ((Collection<?>)iterable).toArray();
        }
        return IteratorUtil.toArray(iterable.iterator());
    }

    /**
     * Returns an array containing all of the elements in this iterable in proper sequence (from first to last element);
     * the runtime type of the returned array is that of the specified type.
     *
     * <p>
     * Like the {@link #toArray(Iterable)} method, this method acts as bridge between array-based and collection-based
     * APIs. Further, this method allows precise control over the runtime type of the output array, and may, under
     * certain circumstances, be used to save allocation costs.
     * </p>
     *
     * @param type type of the returning array
     * @param iterable the elements of the iterable are to be stored
     * @return an array containing the elements of the iterable
     * @throws ArrayStoreException if the runtime type of the specified array is not a supertype of the runtime type of
     *     every element in this list
     * @throws NullPointerException if the specified array is null
     * @see Collection#toArray(Object[])
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] toArray(Iterable<?> iterable, Class<T> type) {
        if (iterable instanceof Collection) {
            Collection<?> collection = (Collection<?>)iterable;
            return collection.toArray((T[])Array.newInstance(type, collection.size()));
        }
        return IteratorUtil.toArray(iterable.iterator(), type);
    }

    /**
     * Returns a {@link LinkedList} filled with the content of <tt>iterable</tt> or <tt>iterable</tt> itself if it
     * already is an instance of {@link LinkedList}.
     *
     * @param iterable content
     * @return the {@link LinkedList}
     */
    public static <T> LinkedList<T> asList(Iterable<T> iterable) {
        if (iterable instanceof LinkedList) {
            return (LinkedList<T>)iterable;
        }
        return IteratorUtil.asList(iterable.iterator());
    }

    /**
     * Returns a {@link ArrayList} with the specified initial capacity, filled with the content of <tt>iterable</tt> or
     * <tt>iterable</tt> itself if it already is an instance of {@link ArrayList}.
     *
     * @param iterable content
     * @param initialCapacity the initial capacity of the resulting list.
     * @return the {@link ArrayList}
     */
    public static <T> ArrayList<T> asList(Iterable<T> iterable, int initialCapacity) {
        if (iterable instanceof ArrayList) {
            return (ArrayList<T>)iterable;
        }
        return IteratorUtil.asList(iterable.iterator(), initialCapacity);
    }

    /**
     * A <tt>null</tt> safe implementation of {@link Arrays#asList(Object...)}.
     *
     * @param <T> the class of the objects in the array
     * @param array the array by which the list will be backed
     * @return a list view of the specified array
     */
    @SafeVarargs
    public static <T> List<T> asList(T... array) {
        return null == array ? Collections.emptyList() : Arrays.asList(array);
    }

    /**
     * Returns a {@link Set} filled with the content of <tt>iterable</tt> or <tt>iterable</tt> itself if it already is
     * an instance of {@link Set}.
     *
     * @param iterable content
     * @return the {@link Set}
     */
    public static <T> Set<T> asSet(Iterable<T> iterable) {
        if (iterable instanceof Set) {
            return (Set<T>)iterable;
        }
        return IteratorUtil.asSet(iterable.iterator());
    }

    /**
     * Returns a {@link LinkedHashSet} filled with the content of iterable.
     *
     * @param iterable content
     * @return the {@link LinkedHashSet}
     */
    public static <T> LinkedHashSet<T> asOrderedSet(Iterable<T> iterable) {
        return IteratorUtil.asOrderedSet(iterable.iterator());
    }

    /**
     * Wraps the specified <tt>iterable</tt> returning a converted {@link Iterator} (using
     * {@link Function#apply(Object)} on <tt>functor</tt>) on every invocation of its {@link Iterable#iterator()}.
     *
     * @param iterable {@link Iterable} to wrap
     * @param functor functor to apply
     * @return a wrapped iterable
     */
    public static <Input, Output> Iterable<Output> wrap(Iterable<Input> iterable,
            Function<Iterator<Input>, Iterator<Output>> functor)
    {
        return new SuppliedIterable<>(() -> functor.apply(iterable.iterator()));
    }

    /**
     * Wraps the specified <tt>iterable</tt> returning a converted {@link BranchIterator} (using
     * {@link Function#apply(Object)} on <tt>functor</tt>) on every invocation of its {@link Iterable#iterator()}.
     *
     * @param iterable {@link Iterable} to wrap
     * @param functor functor to apply
     * @return a wrapped iterable
     */
    public static <Input, Output> BranchIterable<Output> branchWrap(Iterable<Input> iterable,
            Function<Iterator<Input>, BranchIterator<Output>> functor)
    {
        return new BranchIterableImpl<>(() -> functor.apply(iterable.iterator()));
    }

    private static class BranchIterableImpl<T> extends SuppliedIterable<T> implements BranchIterable<T> {
        public BranchIterableImpl(Supplier<? extends BranchIterator<T>> supplier) {
            super(supplier);
        }

        @Override
        public BranchIterator<T> iterator() {
            return (BranchIterator<T>)super.iterator();
        }
    }
}
