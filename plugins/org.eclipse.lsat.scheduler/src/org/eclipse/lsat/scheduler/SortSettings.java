/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import org.eclipse.lsat.common.qvto.util.AbstractModelTransformer;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;

import setting.Settings;

public class SortSettings extends AbstractModelTransformer<Settings, Settings> {
    @Override
    protected String getDefaultTransformation() {
        return "/transforms/sortSettings.qvto";
    }

    @Override
    protected Settings doTransformModel(Settings input) throws QvtoTransformationException {
        BasicModelExtent inoutModel = new BasicModelExtent();
        inoutModel.add(input);

        execute(inoutModel);

        return validateOneAndOnlyOne(Settings.class, inoutModel);
    }
}
