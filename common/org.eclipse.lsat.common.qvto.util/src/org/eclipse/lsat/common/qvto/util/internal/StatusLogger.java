/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util.internal;

import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;

public class StatusLogger extends AbstractQvtLog {
    private final MultiStatus rootStatus;

    public StatusLogger(MultiStatus rootStatus) {
        this.rootStatus = rootStatus;
    }

    @Override
    protected synchronized void doLog(LogLevel aLevel, String aFormat, Object... aArgs) {
        rootStatus.add(new LogStatus(aLevel, rootStatus.getPlugin(), createMessage(aFormat, aArgs)));
    }

    public static final class LogStatus extends Status {
        public static final int LEVEL_FATAL = LogLevel.Fatal.ordinal();

        public static final int LEVEL_ERROR = LogLevel.Error.ordinal();

        public static final int LEVEL_WARNING = LogLevel.Warning.ordinal();

        public static final int LEVEL_INFO = LogLevel.Info.ordinal();

        public static final int LEVEL_DEBUG = LogLevel.Debug.ordinal();

        public LogStatus(LogLevel aLevel, String pluginId, String message) {
            super(aLevel.toSeverity(), pluginId, aLevel.ordinal(), message, null);
        }

        @Override
        public void setException(Throwable exception) {
            super.setException(exception);
        }
    }
}
