/**
 */
package timing.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import timing.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see timing.TimingPackage
 * @generated
 */
public class TimingAdapterFactory extends AdapterFactoryImpl {
	/**
     * The cached model package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static TimingPackage modelPackage;

	/**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public TimingAdapterFactory() {
        if (modelPackage == null)
        {
            modelPackage = TimingPackage.eINSTANCE;
        }
    }

	/**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
	@Override
	public boolean isFactoryForType(Object object) {
        if (object == modelPackage)
        {
            return true;
        }
        if (object instanceof EObject)
        {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

	/**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected TimingSwitch<Adapter> modelSwitch =
		new TimingSwitch<Adapter>()
        {
            @Override
            public Adapter caseTiming(Timing object)
            {
                return createTimingAdapter();
            }
            @Override
            public Adapter caseFixedValue(FixedValue object)
            {
                return createFixedValueAdapter();
            }
            @Override
            public Adapter caseArray(Array object)
            {
                return createArrayAdapter();
            }
            @Override
            public Adapter caseTriangularDistribution(TriangularDistribution object)
            {
                return createTriangularDistributionAdapter();
            }
            @Override
            public Adapter caseDistribution(Distribution object)
            {
                return createDistributionAdapter();
            }
            @Override
            public Adapter casePertDistribution(PertDistribution object)
            {
                return createPertDistributionAdapter();
            }
            @Override
            public Adapter caseNormalDistribution(NormalDistribution object)
            {
                return createNormalDistributionAdapter();
            }
            @Override
            public Adapter caseEnumeratedDistribution(EnumeratedDistribution object)
            {
                return createEnumeratedDistributionAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object)
            {
                return createEObjectAdapter();
            }
        };

	/**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
	@Override
	public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject)target);
    }


	/**
     * Creates a new adapter for an object of class '{@link timing.Timing <em>Timing</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see timing.Timing
     * @generated
     */
	public Adapter createTimingAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link timing.FixedValue <em>Fixed Value</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see timing.FixedValue
     * @generated
     */
	public Adapter createFixedValueAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link timing.Array <em>Array</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see timing.Array
     * @generated
     */
	public Adapter createArrayAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link timing.TriangularDistribution <em>Triangular Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see timing.TriangularDistribution
     * @generated
     */
	public Adapter createTriangularDistributionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link timing.Distribution <em>Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see timing.Distribution
     * @generated
     */
	public Adapter createDistributionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link timing.PertDistribution <em>Pert Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see timing.PertDistribution
     * @generated
     */
	public Adapter createPertDistributionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link timing.NormalDistribution <em>Normal Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see timing.NormalDistribution
     * @generated
     */
	public Adapter createNormalDistributionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link timing.EnumeratedDistribution <em>Enumerated Distribution</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see timing.EnumeratedDistribution
     * @generated
     */
	public Adapter createEnumeratedDistributionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
	public Adapter createEObjectAdapter() {
        return null;
    }

} //TimingAdapterFactory
