/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.design;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.lsat.activity.diagram.services.ActivityServices;
import org.eclipse.sirius.business.api.action.AbstractExternalJavaAction;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import activity.Activity;
import activity.ActivitySet;
import machine.IResource;
import machine.Import;

public class AddResource extends AbstractExternalJavaAction {
    @Override
    public boolean canExecute(Collection<? extends EObject> selections) {
        return true;
    }

    @Override
    public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
        final Activity activity = getParameter(parameters, "activity", Activity.class);
        final EObject selection = selections.iterator().next();

        final ArrayList<EObject> roots = new ArrayList<EObject>();
        for (Import _import: ((ActivitySet)activity.eContainer()).getImports()) {
            roots.addAll(_import.load());
        }

        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                AddResourceWizard wizard = new AddResourceWizard(roots, activity, selection);
                WizardDialog dialog = new WizardDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
                        wizard);
                if (Dialog.OK == dialog.open()) {
                    IResource resource = wizard.getResource();
                    if (!ActivityServices.getResources(activity).contains(resource)) {
                        ActivityServices.addClaimRelease(activity, resource);
                    }
                }
            }
        });
    }
}
