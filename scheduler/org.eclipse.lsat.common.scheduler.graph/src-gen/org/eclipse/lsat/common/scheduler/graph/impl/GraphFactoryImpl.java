/**
 */
package org.eclipse.lsat.common.scheduler.graph.impl;

import org.eclipse.lsat.common.scheduler.graph.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GraphFactoryImpl extends EFactoryImpl implements GraphFactory {
	/**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static GraphFactory init() {
        try
        {
            GraphFactory theGraphFactory = (GraphFactory)EPackage.Registry.INSTANCE.getEFactory(GraphPackage.eNS_URI);
            if (theGraphFactory != null)
            {
                return theGraphFactory;
            }
        }
        catch (Exception exception)
        {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new GraphFactoryImpl();
    }

	/**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public GraphFactoryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject create(EClass eClass) {
        switch (eClass.getClassifierID())
        {
            case GraphPackage.TASK_DEPENDENCY_GRAPH: return createTaskDependencyGraph();
            case GraphPackage.TASK: return createTask();
            case GraphPackage.DEPENDENCY: return createDependency();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public <T extends Task> TaskDependencyGraph<T> createTaskDependencyGraph() {
        TaskDependencyGraphImpl<T> taskDependencyGraph = new TaskDependencyGraphImpl<T>();
        return taskDependencyGraph;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Task createTask() {
        TaskImpl task = new TaskImpl();
        return task;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Dependency createDependency() {
        DependencyImpl dependency = new DependencyImpl();
        return dependency;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public GraphPackage getGraphPackage() {
        return (GraphPackage)getEPackage();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
	@Deprecated
	public static GraphPackage getPackage() {
        return GraphPackage.eINSTANCE;
    }

} //GraphFactoryImpl
