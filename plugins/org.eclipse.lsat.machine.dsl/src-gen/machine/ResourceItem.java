/**
 */
package machine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.ResourceItem#getResource <em>Resource</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getResourceItem()
 * @model
 * @generated
 */
public interface ResourceItem extends IResource {
	/**
     * Returns the value of the '<em><b>Resource</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link machine.Resource#getItems <em>Items</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Resource</em>' container reference.
     * @see #setResource(Resource)
     * @see machine.MachinePackage#getResourceItem_Resource()
     * @see machine.Resource#getItems
     * @model opposite="items" required="true" transient="false"
     * @generated
     */
	Resource getResource();

	/**
     * Sets the value of the '{@link machine.ResourceItem#getResource <em>Resource</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resource</em>' container reference.
     * @see #getResource()
     * @generated
     */
	void setResource(Resource value);

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true"
     * @generated
     */
	String fqn();

} // ResourceItem
