/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import org.eclipse.lsat.common.qvto.util.AbstractModelTransformer;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.common.scheduler.resources.ResourceModel;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;

import dispatching.ActivityDispatching;

public class Dispatching2Graph<T extends Task>
        extends AbstractModelTransformer<ActivityDispatching, Dispatching2GraphOutput<T>>
{
    @Override
    protected String getDefaultTransformation() {
        return "/transforms/dispatching2graph.qvto";
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Dispatching2GraphOutput<T> doTransformModel(ActivityDispatching input)
            throws QvtoTransformationException
    {
        // Input
        BasicModelExtent inDispatching = new BasicModelExtent();
        inDispatching.add(input);

        // Output
        BasicModelExtent outResource = new BasicModelExtent();
        BasicModelExtent outGraph = new BasicModelExtent();

        execute(inDispatching, outResource, outGraph);

        return new Dispatching2GraphOutput<T>(validateOneAndOnlyOne(ResourceModel.class, outResource),
                validateOneAndOnlyOne(TaskDependencyGraph.class, outGraph));
    }
}
