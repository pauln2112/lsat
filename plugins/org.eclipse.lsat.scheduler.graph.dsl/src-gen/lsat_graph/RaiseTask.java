/**
 */
package lsat_graph;

import activity.RaiseEvent;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Raise Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lsat_graph.lsat_graphPackage#getRaiseTask()
 * @model
 * @generated
 */
public interface RaiseTask extends EventTask<RaiseEvent>
{
} // RaiseTask
