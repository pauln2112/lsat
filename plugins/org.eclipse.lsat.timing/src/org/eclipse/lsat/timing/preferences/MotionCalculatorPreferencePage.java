/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.preferences;

import java.util.List;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.lsat.timing.Activator;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.lsat.timing.view.MotionViewJob;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class MotionCalculatorPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
    public MotionCalculatorPreferencePage() {
        super(GRID);
    }

    @Override
    protected void createFieldEditors() {
        addField(new RadioGroupFieldEditor(MotionCalculatorExtension.EXTENSION_POINT, "Motion calculator", 1,
                getLabelsAndValues(), getFieldEditorParent(), true));

        addField(new BooleanFieldEditor(MotionViewJob.SHOW_DEBUG_INFO, "Show debug information in chart view",
                getFieldEditorParent()));
    }

    @Override
    public void init(IWorkbench workbench) {
        setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("Configures the motion calculator to use for this workspace.");
    }

    private String[][] getLabelsAndValues() {
        List<MotionCalculatorExtension> motionCalculators = MotionCalculatorExtension.getAvailableMotionCalculators();
        String[][] labelsAndValues = new String[motionCalculators.size()][2];
        for (int i = 0; i < labelsAndValues.length; i++) {
            labelsAndValues[i][0] = motionCalculators.get(i).getName();
            labelsAndValues[i][1] = motionCalculators.get(i).getId();
        }
        return labelsAndValues;
    }
}
