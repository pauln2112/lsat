/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.contributions.ui.wizard;

import java.util.Arrays;
import java.util.Collection;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.resource.XtextResourceSet;

import dispatching.ActivityDispatching;
import dispatching.DispatchingFactory;
import machine.Import;

public class WizardDispatching extends AbstractNewWizard<ActivityDispatching> {
    public static final String ID = "org.eclipse.lsat.contributions.ui.wizardDispatching";

    public WizardDispatching() {
        super("Activity Dispatching");
    }

    @Override
    protected ResourceSet createResourceSet() {
        return new XtextResourceSet();
    }

    @Override
    protected Collection<String> getFileExtensions() {
        return Arrays.asList("dispatching");
    }

    @Override
    protected ActivityDispatching createModel(IFile modelFile, Collection<Import> imports) {
        ActivityDispatching activityDispatching = DispatchingFactory.eINSTANCE.createActivityDispatching();
        activityDispatching.getImports().addAll(imports);
        activityDispatching.getDispatchGroups().add(DispatchingFactory.eINSTANCE.createDispatchGroup());
        return activityDispatching;
    }
}
