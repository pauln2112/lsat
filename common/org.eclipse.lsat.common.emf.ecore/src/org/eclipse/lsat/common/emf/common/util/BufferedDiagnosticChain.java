/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.util;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

public class BufferedDiagnosticChain implements DiagnosticChain {
    private final Collection<Diagnostic> buffer;

    private final boolean unique;

    private DiagnosticChain delegate = null;

    private int maxSeverity = Diagnostic.OK;

    public BufferedDiagnosticChain() {
        this(null, false);
    }

    public BufferedDiagnosticChain(DiagnosticChain delegate) {
        this(delegate, false);
    }

    public BufferedDiagnosticChain(DiagnosticChain delegate, boolean unique) {
        this.delegate = delegate;
        this.unique = unique;
        this.buffer = unique ? new LinkedHashSet<Diagnostic>() : new LinkedList<Diagnostic>();
    }

    public int getMaxSeverity() {
        return maxSeverity;
    }

    public Collection<Diagnostic> getDiagnostics() {
        return Collections.unmodifiableCollection(buffer);
    }

    public void add(Diagnostic diagnostic) {
        if (buffer.add(wrap(diagnostic)) && null != delegate) {
            maxSeverity = Math.max(maxSeverity, diagnostic.getSeverity());
            delegate.add(diagnostic);
        }
    }

    public void addAll(Diagnostic diagnostic) {
        for (Diagnostic child: diagnostic.getChildren()) {
            add(child);
        }
    }

    public void merge(Diagnostic diagnostic) {
        if (diagnostic.getChildren().isEmpty()) {
            add(diagnostic);
        } else {
            addAll(diagnostic);
        }
    }

    /**
     * Only need to wrap if put in Set (unique)
     */
    private Diagnostic wrap(Diagnostic diagnostic) {
        return unique ? new UniqueDiagnostic(diagnostic) : diagnostic;
    }

    private static class UniqueDiagnostic implements Diagnostic {
        private final Diagnostic delegate;

        private UniqueDiagnostic(Diagnostic delegate) {
            this.delegate = delegate;
        }

        public int getSeverity() {
            return delegate.getSeverity();
        }

        public String getMessage() {
            return delegate.getMessage();
        }

        public String getSource() {
            return delegate.getSource();
        }

        public int getCode() {
            return delegate.getCode();
        }

        public Throwable getException() {
            return delegate.getException();
        }

        public List<?> getData() {
            return delegate.getData();
        }

        public List<Diagnostic> getChildren() {
            return delegate.getChildren();
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((getChildren() == null) ? 0 : getChildren().hashCode());
            result = prime * result + getCode();
            result = prime * result + ((getData() == null) ? 0 : getData().hashCode());
            result = prime * result + ((getMessage() == null) ? 0 : getMessage().hashCode());
            result = prime * result + getSeverity();
            result = prime * result + ((getSource() == null) ? 0 : getSource().hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Diagnostic other = (Diagnostic)obj;
            if (getChildren() == null) {
                if (other.getChildren() != null)
                    return false;
            } else if (!getChildren().equals(other.getChildren()))
                return false;
            if (getCode() != other.getCode())
                return false;
            if (getData() == null) {
                if (other.getData() != null)
                    return false;
            } else if (!getData().equals(other.getData()))
                return false;
            if (getMessage() == null) {
                if (other.getMessage() != null)
                    return false;
            } else if (!getMessage().equals(other.getMessage()))
                return false;
            if (getSeverity() != other.getSeverity())
                return false;
            if (getSource() == null) {
                if (other.getSource() != null)
                    return false;
            } else if (!getSource().equals(other.getSource()))
                return false;
            return true;
        }
    }
}
