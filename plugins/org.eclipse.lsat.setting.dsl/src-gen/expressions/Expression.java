/**
 */
package expressions;

import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see expressions.ExpressionsPackage#getExpression()
 * @model abstract="true"
 * @generated
 */
public interface Expression extends AbstractElement {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true"
     * @generated
     */
	BigDecimal evaluate();

} // Expression
