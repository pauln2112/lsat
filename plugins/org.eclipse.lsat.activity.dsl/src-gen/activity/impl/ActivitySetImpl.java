/**
 */
package activity.impl;

import activity.Activity;
import activity.ActivityPackage;
import activity.ActivitySet;

import activity.Event;
import java.util.Collection;
import machine.impl.ImportContainerImpl;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link activity.impl.ActivitySetImpl#getActivities <em>Activities</em>}</li>
 *   <li>{@link activity.impl.ActivitySetImpl#getEvents <em>Events</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActivitySetImpl extends ImportContainerImpl implements ActivitySet {
	/**
     * The cached value of the '{@link #getActivities() <em>Activities</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getActivities()
     * @generated
     * @ordered
     */
	protected EList<Activity> activities;

	/**
     * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getEvents()
     * @generated
     * @ordered
     */
    protected EList<Event> events;

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActivitySetImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ActivityPackage.Literals.ACTIVITY_SET;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Activity> getActivities() {
        if (activities == null)
        {
            activities = new EObjectContainmentEList<Activity>(Activity.class, this, ActivityPackage.ACTIVITY_SET__ACTIVITIES);
        }
        return activities;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Event> getEvents()
    {
        if (events == null)
        {
            events = new EObjectContainmentEList<Event>(Event.class, this, ActivityPackage.ACTIVITY_SET__EVENTS);
        }
        return events;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY_SET__ACTIVITIES:
                return ((InternalEList<?>)getActivities()).basicRemove(otherEnd, msgs);
            case ActivityPackage.ACTIVITY_SET__EVENTS:
                return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY_SET__ACTIVITIES:
                return getActivities();
            case ActivityPackage.ACTIVITY_SET__EVENTS:
                return getEvents();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY_SET__ACTIVITIES:
                getActivities().clear();
                getActivities().addAll((Collection<? extends Activity>)newValue);
                return;
            case ActivityPackage.ACTIVITY_SET__EVENTS:
                getEvents().clear();
                getEvents().addAll((Collection<? extends Event>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY_SET__ACTIVITIES:
                getActivities().clear();
                return;
            case ActivityPackage.ACTIVITY_SET__EVENTS:
                getEvents().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY_SET__ACTIVITIES:
                return activities != null && !activities.isEmpty();
            case ActivityPackage.ACTIVITY_SET__EVENTS:
                return events != null && !events.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //ActivitySetImpl
