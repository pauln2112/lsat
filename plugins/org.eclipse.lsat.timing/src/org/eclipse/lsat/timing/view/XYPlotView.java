/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.view;

import org.eclipse.core.runtime.Status;
import org.eclipse.lsat.timing.Activator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.trace4cps.common.jfreechart.ui.widgets.ChartPanelComposite;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.statushandlers.StatusManager;
import org.jfree.chart.JFreeChart;

public class XYPlotView extends ViewPart {
    private ChartPanelComposite chartComposite;

    /**
     * The ID of the view as specified by the extension.
     */
    public static final String ID = "org.eclipse.lsat.timing.view.XYPlotView";

    public static void showJFreeChart(final JFreeChart jfreechart) {
        XYPlotView[] view = new XYPlotView[1];
        PlatformUI.getWorkbench().getDisplay().syncExec(() -> view[0] = getDefault());
        if (view[0] != null) {
            view[0].setJFreeChart(jfreechart);
        }
    }

    private static XYPlotView getDefault() {
        IWorkbench wb = PlatformUI.getWorkbench();
        IWorkbenchWindow awbw = wb.getActiveWorkbenchWindow();
        for (IWorkbenchWindow window: wb.getWorkbenchWindows()) {
            for (IWorkbenchPage page: window.getPages()) {
                for (IViewReference viewRef: page.getViewReferences()) {
                    if (ID.equals(viewRef.getId())) {
                        XYPlotView view = (XYPlotView)viewRef.getView(true);
                        awbw.getActivePage().activate(view);
                        return view;
                    }
                }
            }
        }
        try {
            return (XYPlotView)awbw.getActivePage().showView(ID, null, IWorkbenchPage.VIEW_ACTIVATE);
        } catch (PartInitException e) {
            StatusManager.getManager().handle(new Status(Status.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
            return null;
        }
    }

    /**
     * This is a callback that will allow us to create the viewer and initialize it.
     */
    @Override
    public void createPartControl(Composite parent) {
        chartComposite = new ChartPanelComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
    }

    /**
     * Passing the focus request to the viewer's control.
     */
    @Override
    public void setFocus() {
        if (chartComposite != null) {
            chartComposite.setFocus();
        }
    }

    private void setJFreeChart(JFreeChart jfreechart) {
        if (null == chartComposite) {
            System.err.println("Not initialized yet!");
            return;
        }
        chartComposite.setChart(jfreechart);
    }
}
