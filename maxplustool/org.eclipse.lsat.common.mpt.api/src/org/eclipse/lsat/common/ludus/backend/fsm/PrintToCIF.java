/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.fsm;

import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;

/**
 * Print a given finite-state machine to CIF syntax. See also:
 * <a href="https://www.eclipse.org/escet/cif/language-reference/index.html">CIF Language reference</a>
 *
 * @author Bram van der Sanden
 */
public class PrintToCIF {
    private PrintToCIF() {
        // Empty
    }

    public static String print(FSM<Location, Edge> fsm, String name) {
        StringBuilder sb = new StringBuilder();

        for (String cEvent: fsm.getControllable()) {
            sb.append("controllable ");
            sb.append(cEvent);
            sb.append(";\n");
        }

        for (String uEvent: fsm.getUncontrollable()) {
            sb.append("uncontrollable ");
            sb.append(uEvent);
            sb.append(";\n");
        }

        sb.append("\nplant automaton ");
        name = name.replaceAll("-", "_");
        sb.append(name);
        sb.append(":\n");

        for (Location l: fsm.getVertices()) {
            sb.append("  location ");
            sb.append(l.getName());
            if (fsm.outgoingEdgesOf(l).isEmpty() && !l.equals(fsm.getInitial()) && !fsm.isMarked(l)) {
                sb.append(";\n");
            } else {
                sb.append(":\n");
            }
            if (l.equals(fsm.getInitial())) {
                sb.append("    initial;\n");
            }
            if (fsm.isMarked(l)) {
                sb.append("    marked;\n");
            }

            for (Edge e: fsm.outgoingEdgesOf(l)) {
                sb.append("    edge ");
                sb.append(e.getEvent());
                sb.append(" goto ");
                sb.append(e.getTarget().getName());
                sb.append(";\n");
            }
        }

        sb.append("end");
        return sb.toString();
    }
}
