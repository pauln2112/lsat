/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package setting;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.lsat.common.emf.common.util.URIHelper;

import machine.Import;
import machine.ImportContainer;

public final class SettingUtil {
    private SettingUtil() {
        // Empty for utilities
    }

    /**
     * Get the settings from the first imported settings file or else get the first settings file that imports this
     * container or else get the project configured one.
     */
    public static Settings getSettings(ImportContainer importContainer) throws IOException {
        // if a setting file is imported by the import container return it.
        Optional<Settings> settings = importContainer.loadAll().stream().filter(Settings.class::isInstance)
                .map(Settings.class::cast).findFirst();
        if (settings.isPresent()) {
            return settings.get();
        }
        return getSettings(importContainer.eResource());
    }

    /**
     * Get the setting from the same folder of the workspace. If the resource is a setting files that one is used or If
     * the resource import a setting file that one is used or If the resource is imported by a setting file that one is
     * used If multiple matches are possible the project configured one is used.
     */
    public static Settings getSettings(Resource context) throws IOException {
        return getSettings(context, null);
    }

    /**
     * Get the setting from the provided context
     *
     * @param context with resource set containing the settings
     * @param settingIResource a possible ref to the setting file. If <code>null</code> then the setting is derived from
     *     the workspace. First a setting file that imports context if search other the configured
     */
    public static Settings getSettings(Resource context, IResource settingIResource) throws IOException {
        if (settingIResource == null) {
            URI contextURI = context.getURI();
            try {
                Collection<Settings> matches = new LinkedHashSet<>();
                for (IResource potentialMatch: SettingActivator.getDefault()
                        .getSettingIResources(URIHelper.asIResource(contextURI)))
                {
                    Settings setting = loadSettings(URIHelper.asURI(potentialMatch), context.getResourceSet());
                    for (Import imp: setting.getImports()) {
                        URI uri = URI.createURI(imp.getImportURI()).resolve(setting.eResource().getURI());
                        if (uri.equals(contextURI)) {
                            matches.add(setting);
                        }
                    }
                }
                if (matches.size() == 1) {
                    return matches.iterator().next();
                }

                IResource configuredSettingsResource = SettingActivator.getDefault()
                        .getSettingIResource(URIHelper.asIResource(contextURI));
                Settings defaultSettings = loadSettings(URIHelper.asURI(configuredSettingsResource),
                        context.getResourceSet());
                if (matches.isEmpty()) {
                    return defaultSettings;
                }
                for (Settings match: matches) {
                    if (EcoreUtil.equals(defaultSettings, match)) {
                        return match;
                    }
                }
                // return the first applicable settings
                return matches.iterator().next();
            } catch (CoreException e) {
                throw new IOException(e);
            }
        }
        return loadSettings(URIHelper.asURI(settingIResource), context.getResourceSet());
    }

    /**
     * Get the setting from the same folder of the workspace
     *
     * @return
     */
    private static Settings loadSettings(URI settingURI, ResourceSet resourceSet) {
        Resource settingResource = resourceSet.getResource(settingURI, true);
        return settingResource.getContents().isEmpty() ? null : (Settings)settingResource.getContents().get(0);
    }
}
