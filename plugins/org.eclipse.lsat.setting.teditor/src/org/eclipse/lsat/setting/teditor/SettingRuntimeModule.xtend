/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.setting.teditor

import org.eclipse.lsat.machine.teditor.ImportResourceDescriptionStrategy
import org.eclipse.lsat.machine.teditor.scoping.ImportScopeProvider
import org.eclipse.lsat.setting.teditor.formatting.SettingFormatter
import org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class SettingRuntimeModule extends AbstractSettingRuntimeModule {

    override bindIFormatter() {
        return SettingFormatter
    }

    override bindIGlobalScopeProvider() {
        return  ImportScopeProvider
    }

    def Class<? extends IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
        ImportResourceDescriptionStrategy
    }
}
