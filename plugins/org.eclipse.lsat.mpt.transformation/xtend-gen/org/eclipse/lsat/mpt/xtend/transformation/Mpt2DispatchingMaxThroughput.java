/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.xtend.transformation;

import activity.ActivitySet;
import org.eclipse.lsat.common.mpt.api.MaximumThroughputResult;
import org.eclipse.lsat.mpt.xtend.transformation.Mpt2Dispatching;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class Mpt2DispatchingMaxThroughput {
  public CharSequence transformModel(final ActivitySet inActivity, final MaximumThroughputResult throughputResult) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// Maximum throughput analysis result");
    _builder.newLine();
    _builder.append("import \"");
    String _string = inActivity.eResource().getURI().toString();
    _builder.append(_string);
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    String _createSequence = Mpt2Dispatching.createSequence(throughputResult.getTransientStateActivities(), "Startup phase");
    _builder.append(_createSequence);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("// Repeatable activity sequence, achieving the maximum throughput of ");
    String _round = Mpt2Dispatching.round(Double.valueOf(throughputResult.getThroughput()));
    _builder.append(_round);
    _builder.append(":");
    _builder.newLineIfNotEmpty();
    String _createSequence_1 = Mpt2Dispatching.createSequence(throughputResult.getSteadyStateActivities(), "Steady phase");
    _builder.append(_createSequence_1);
    _builder.newLineIfNotEmpty();
    return _builder;
  }
}
