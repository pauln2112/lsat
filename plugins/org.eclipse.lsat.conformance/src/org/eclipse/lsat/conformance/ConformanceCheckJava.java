/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance;

import static java.lang.String.format;
import static org.eclipse.lsat.common.queries.QueryableIterable.from;
import static org.eclipse.lsat.conformance.internal.UniqueTracePoint.create;
import static org.eclipse.lsat.conformance.internal.UniqueTracePoint.matches;
import static org.eclipse.lsat.conformance.internal.UniqueTracePoint.matchesOneOf;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.eclipse.lsat.common.graph.directed.Node;
import org.eclipse.lsat.common.graph.directed.util.DirectedGraphQueries;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.common.util.CollectionUtil;
import org.eclipse.lsat.conformance.ConformanceCheckInput.Level;
import org.eclipse.lsat.conformance.internal.ConformanceCheckQueries;
import org.eclipse.lsat.conformance.internal.UniqueTracePoint;
import org.eclipse.lsat.petri_net.PetriNet;
import org.eclipse.lsat.petri_net.Place;
import org.eclipse.lsat.petri_net.Transition;
import org.eclipse.lsat.trace.TraceLine;
import org.eclipse.lsat.trace.TraceModel;

import activity.Action;
import activity.Claim;
import activity.Release;
import activity.ResourceAction;
import activity.impl.ActivityQueries;
import machine.IResource;

/**
 * This is a trial java transformation for the Conformance check. TODO: Claim level not supported yet
 *
 * @author yblanken
 */
public class ConformanceCheckJava {
    private final HashMap<IResource, Set<UniqueTracePoint>> tracePointsInPlay = new HashMap<>();

    private final Level level;

    public ConformanceCheckJava(Level aLevel) {
        this.level = aLevel;
    }

    public PetriNet transformModel(ConformanceCheckInput input) throws QvtoTransformationException {
        tracePointsInPlay.clear();
        Iterable<TraceLine> traceLines = from(input.getTraces()).collect(TraceModel::getLines)
                .sortedBy(TraceLine::getTimestamp);

        Iterable<Node> allNodes = DirectedGraphQueries.allSubNodes(input.getPetriNet());

        if (Level.Dispatching == level) {
            // Wrapping trace points with UniqueTracePoint and adding them to a Set removes all duplicates and saves
            // time!
            Set<UniqueTracePoint> allTracepoints = from(allNodes).objectsOfKind(Transition.class)
                    .xcollectOne(t -> create(t.getTracePoint())).asSet();
            tracePointsInPlay.put(null, allTracepoints);
        }

        // Creating the initial tokens
        input.getPetriNet().getInitialPlaces().forEach(p -> p.setToken(true));

        // Lets play the game!
        // As we're using the allPlaces a lot of times it is best to cache the query result
        List<Place> allPlaces = from(allNodes).objectsOfKind(Place.class).asList();
        // NOTE: Do not add this to a collection as the iterable will query the tokens on iteration
        Iterable<Place> placesWithToken = from(allPlaces).select(p -> p.isToken());

        // First play all tokens that don't need a trace line
        while (play(placesWithToken, null)) {
            /* Establish a stable petri net */
        }

        if (input.getPetriNet().getFinalPlaces().isEmpty()) {
            // If a petri net doesn't have final places, it means that there are
            // loops. In this case we decide that 1 or more loop iterations are
            // expected. After each iteration the tokens will be put in the
            // initial places, but as the first transitions might be tau
            // transitions this means that we should expect the token to be
            // located as the for the first stable petri net
            CollectionUtil.addAll(input.getPetriNet().getFinalPlaces(), placesWithToken);
        }

        for (TraceLine traceLine: traceLines) {
            if (!play(placesWithToken, traceLine)) {
                for (Set<UniqueTracePoint> tracePoints: tracePointsInPlay.values()) {
                    if (matchesOneOf(traceLine, tracePoints)) {
                        throw new QvtoTransformationException(format(
                                "Trace not expected, see petri net for details. %s, line %s: %s",
                                traceLine.getModel().getName(), traceLine.getLineNumber(), traceLine.getTracePoint()));
                    }
                }
            }

            // Finally play all tokens that don't need a trace line
            while (play(placesWithToken, null)) {
                /* Establish a stable petri net */
            }
        }

        for (Place place: input.getPetriNet().getFinalPlaces()) {
            if (!place.isToken()) {
                throw new QvtoTransformationException(
                        "Conformance check failed, unexpected end of trace, see petri net for details");
            }
        }
        return input.getPetriNet();
    }

    @SuppressWarnings("unused")
    private void setToken(Iterable<Place> places, boolean token) {
        for (Place place: places) {
            setToken(place, token);
        }
    }

    private void setToken(Place place, boolean token) {
        place.setToken(token);

        if (token && Level.Claim == level) {
            final Action action = place.getAction();
            if (action instanceof Claim) {
                // Find the claimed trace points for this resource and add them to the validation
                final IResource resource = ((Claim)action).getResource();
                Iterable<ResourceAction> actions = ActivityQueries.getActionsFor(resource, ResourceAction.class,
                        action.getGraph().getNodes());
                // Wrapping trace points with UniqueTracePoint and adding them to a Set removes all duplicates and saves
                // time!
                Set<UniqueTracePoint> claimedTracepoints = from(actions)
                        .xcollect(ConformanceCheckQueries::getClaimedTracePoints).asSet();
                tracePointsInPlay.put(resource, claimedTracepoints);
            } else if (action instanceof Release) {
                tracePointsInPlay.remove(((Release)action).getResource());
            }
        }
    }

    private boolean play(Iterable<Place> places, TraceLine traceLine) {
        for (Place place: places) {
            if (play(place, traceLine)) {
                return true;
            }
        }
        return false;
    }

    private boolean play(Place place, TraceLine traceLine) {
        // If a place has multiple outgoing transitions this means that it is a choice
        // In that case just move the token via the first enabled transition
        for (Transition transition: from(place.getOutgoingEdges()).xcollectOne(e -> e.getTargetNode())
                .objectsOfKind(Transition.class))
        {
            // Only enabled transitions can be played
            if (!isEnabled(transition)) {
                continue;
            }

            // trace line check
            if (matches(traceLine, transition.getTracePoint())) {
                if (null != traceLine) {
                    transition.getTraceLines().add(traceLine);
                }
            } else {
                continue;
            }

            // Move the token(s)
            from(transition.getIncomingEdges()).xcollectOne(e -> e.getSourceNode()).objectsOfKind(Place.class)
                    .forEach(p -> p.setToken(false));
            from(transition.getOutgoingEdges()).xcollectOne(e -> e.getTargetNode()).objectsOfKind(Place.class)
                    .forEach(p -> p.setToken(true));
            return true;
        }
        return false;
    }

    /**
     * Checks whether the transition is enabled (i.e. all incoming places contain a token)
     */
    private boolean isEnabled(Transition transition) {
        for (Place place: from(transition.getIncomingEdges()).xcollectOne(e -> e.getSourceNode())
                .objectsOfKind(Place.class))
        {
            if (!place.isToken()) {
                return false;
            }
        }
        return true;
    }
}
