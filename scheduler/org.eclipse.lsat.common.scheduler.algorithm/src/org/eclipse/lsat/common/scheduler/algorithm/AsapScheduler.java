/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.scheduler.algorithm;

import java.math.BigDecimal;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.scheduler.algorithm.internal.ExecutiontimesValidator;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.scheduler.schedule.Sequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsapScheduler<T extends Task> extends SchedulerBase<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AsapScheduler.class);

    @Override
    protected Logger getLog() {
        return LOGGER;
    }

    @Override
    protected EValidator[] getComplementaryGraphValidators() {
        return new EValidator[] {ExecutiontimesValidator.INSTANCE};
    }

    @Override
    protected final void performScheduling() throws SchedulerException {
        Iterable<? extends T> tasks = getAllTasksInTopologicalOrder();
        if (null == tasks) {
            throw new CycleFoundException("Cycle exists in graph!!");
        }

        for (T task: tasks) {
            ScheduledTask<T> scheduledTask = createScheduledTask(task, true);

            // Determine the sequence that is available at the earliest
            SequenceMetaData asapSequenceMetaData = null;
            for (Sequence<T> sequence: getSequences(task.getResources())) {
                SequenceMetaData sequenceMetaData = startTimeOnSequence(scheduledTask, sequence);
                if (null == asapSequenceMetaData
                        || sequenceMetaData.getTime().compareTo(asapSequenceMetaData.getTime()) < 0)
                {
                    asapSequenceMetaData = sequenceMetaData;
                }
            }
            if (null == asapSequenceMetaData) {
                throw new SchedulerException("Could not determine sequence for task: " + scheduledTask.getName());
            }

            // Now add this task to the asap sequence
            scheduledTask.setStartTime(asapSequenceMetaData.getTime());
            scheduledTask.setEndTime(asapSequenceMetaData.getTime().add(scheduledTask.getTask().getExecutionTime()));
            asapSequenceMetaData.getSequence().getScheduledTasks().add(asapSequenceMetaData.getIndex(), scheduledTask);
        }
    }

    /**
     * Allows to be overridden by sub classes to massage the topological order
     */
    protected Iterable<? extends T> getAllTasksInTopologicalOrder() {
        return getGraph().allNodesInTopologicalOrder();
    }

    protected SequenceMetaData startTimeOnSequence(ScheduledTask<T> scheduledTask, Sequence<T> sequence) {
        // Earliest start time for this task
        BigDecimal earliestStartTime = getEarliestStartTime(scheduledTask, sequence);
        if (getLog().isTraceEnabled()) {
            getLog().trace("Earliest start time for task '{}' = {}", scheduledTask.getName(), earliestStartTime);
        }
        // TODO: We do not support filling the gaps yet so just append
        int index = sequence.getScheduledTasks().size();
        BigDecimal startTime = index > 0
                ? earliestStartTime.max(sequence.getScheduledTasks().get(index - 1).getEndTime()) : earliestStartTime;
        return new SequenceMetaData(sequence, startTime, index);
    }

    /**
     * Given a Task find the completion time of predecessors (on all resources) and adhere to the start time of the
     * resource.
     */
    protected BigDecimal getEarliestStartTime(ScheduledTask<T> scheduledTask, Sequence<T> sequence) {
        BigDecimal earliestStartTime = sequence.getResource().getStart();
        for (Edge edge: scheduledTask.getIncomingEdges()) {
            @SuppressWarnings("unchecked")
            // We can safely cast this source to a ScheduledTask<T> as the Schedule enforces this
            ScheduledTask<T> precedingTask = (ScheduledTask<T>)edge.getSourceNode();
            earliestStartTime = earliestStartTime.max(precedingTask.getEndTime());
        }
        return earliestStartTime;
    }

    protected class SequenceMetaData {
        private final Sequence<T> sequence;

        private final BigDecimal time;

        private final int index;

        SequenceMetaData(Sequence<T> sequence, BigDecimal time, int index) {
            this.sequence = sequence;
            this.time = time;
            this.index = index;
        }

        public Sequence<T> getSequence() {
            return sequence;
        }

        public BigDecimal getTime() {
            return time;
        }

        public int getIndex() {
            return index;
        }
    }
}
