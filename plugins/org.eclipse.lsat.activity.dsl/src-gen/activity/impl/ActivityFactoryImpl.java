/**
 */
package activity.impl;

import activity.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActivityFactoryImpl extends EFactoryImpl implements ActivityFactory {
	/**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static ActivityFactory init() {
        try
        {
            ActivityFactory theActivityFactory = (ActivityFactory)EPackage.Registry.INSTANCE.getEFactory(ActivityPackage.eNS_URI);
            if (theActivityFactory != null)
            {
                return theActivityFactory;
            }
        }
        catch (Exception exception)
        {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new ActivityFactoryImpl();
    }

	/**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ActivityFactoryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject create(EClass eClass) {
        switch (eClass.getClassifierID())
        {
            case ActivityPackage.ACTIVITY_SET: return createActivitySet();
            case ActivityPackage.ACTIVITY: return createActivity();
            case ActivityPackage.CLAIM: return createClaim();
            case ActivityPackage.RELEASE: return createRelease();
            case ActivityPackage.LOCATION_PREREQUISITE: return createLocationPrerequisite();
            case ActivityPackage.TRACE_POINT: return createTracePoint();
            case ActivityPackage.SYNC_BAR: return createSyncBar();
            case ActivityPackage.SIMPLE_ACTION: return createSimpleAction();
            case ActivityPackage.MOVE: return createMove();
            case ActivityPackage.RAISE_EVENT: return createRaiseEvent();
            case ActivityPackage.REQUIRE_EVENT: return createRequireEvent();
            case ActivityPackage.EVENT: return createEvent();
            case ActivityPackage.EVENT_ITEM: return createEventItem();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID())
        {
            case ActivityPackage.SCHEDULING_TYPE:
                return createSchedulingTypeFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID())
        {
            case ActivityPackage.SCHEDULING_TYPE:
                return convertSchedulingTypeToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ActivitySet createActivitySet() {
        ActivitySetImpl activitySet = new ActivitySetImpl();
        return activitySet;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Activity createActivity() {
        ActivityImpl activity = new ActivityImpl();
        return activity;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Claim createClaim() {
        ClaimImpl claim = new ClaimImpl();
        return claim;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Release createRelease() {
        ReleaseImpl release = new ReleaseImpl();
        return release;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public LocationPrerequisite createLocationPrerequisite() {
        LocationPrerequisiteImpl locationPrerequisite = new LocationPrerequisiteImpl();
        return locationPrerequisite;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public TracePoint createTracePoint() {
        TracePointImpl tracePoint = new TracePointImpl();
        return tracePoint;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SyncBar createSyncBar() {
        SyncBarImpl syncBar = new SyncBarImpl();
        return syncBar;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SimpleAction createSimpleAction() {
        SimpleActionImpl simpleAction = new SimpleActionImpl();
        return simpleAction;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Move createMove() {
        MoveImpl move = new MoveImpl();
        return move;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public RaiseEvent createRaiseEvent() {
        RaiseEventImpl raiseEvent = new RaiseEventImpl();
        return raiseEvent;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public RequireEvent createRequireEvent() {
        RequireEventImpl requireEvent = new RequireEventImpl();
        return requireEvent;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Event createEvent()
    {
        EventImpl event = new EventImpl();
        return event;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EventItem createEventItem()
    {
        EventItemImpl eventItem = new EventItemImpl();
        return eventItem;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public SchedulingType createSchedulingTypeFromString(EDataType eDataType, String initialValue) {
        SchedulingType result = SchedulingType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String convertSchedulingTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ActivityPackage getActivityPackage() {
        return (ActivityPackage)getEPackage();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
	@Deprecated
	public static ActivityPackage getPackage() {
        return ActivityPackage.eINSTANCE;
    }

} //ActivityFactoryImpl
