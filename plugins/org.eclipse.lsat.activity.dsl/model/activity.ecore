<?xml version="1.0" encoding="UTF-8"?>
<ecore:EPackage xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="activity" nsURI="http://www.eclipse.org/lsat/activity" nsPrefix="activity">
  <eClassifiers xsi:type="ecore:EClass" name="ActivitySet" eSuperTypes="../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//ImportContainer">
    <eStructuralFeatures xsi:type="ecore:EReference" name="activities" upperBound="-1"
        eType="#//Activity" containment="true" eKeys="../../org.eclipse.lsat.common.graph.directed.editable/model/edg.ecore#//EditableDirectedGraph/name"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="events" upperBound="-1"
        eType="#//Event" containment="true" eKeys="../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//IResource/name"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Activity" eSuperTypes="../../org.eclipse.lsat.common.graph.directed.editable/model/edg.ecore#//EditableDirectedGraph">
    <eOperations name="isExpanded" lowerBound="1" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="if (getOriginalName()==null) return false; &#xA;return  !java.util.Objects.equals(getName(),getOriginalName());"/>
      </eAnnotations>
    </eOperations>
    <eOperations name="getResourcesNeedingItem" upperBound="-1" eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//Resource">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="return new org.eclipse.emf.common.util.BasicEList&lt;Resource>(&#xA;    getNodes().stream().filter(ResourceAction.class::isInstance).map(ResourceAction.class::cast)&#xA;    .map(ResourceAction::getResource).filter(machine.Resource.class::isInstance)&#xA;    .map(machine.Resource.class::cast).filter(r -> !r.getItems().isEmpty())&#xA;    .distinct()&#xA;    .sorted( (r1,r2) ->  r1.getName().compareTo(r2.getName()))&#xA;    .collect(java.util.stream.Collectors.toCollection(java.util.LinkedHashSet::new)));"/>
      </eAnnotations>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="prerequisites" upperBound="-1"
        eType="#//LocationPrerequisite" containment="true" eOpposite="#//LocationPrerequisite/activity"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="originalName" unique="false"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString" transient="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Action" abstract="true" eSuperTypes="../../org.eclipse.lsat.common.graph.directed.editable/model/edg.ecore#//Node">
    <eStructuralFeatures xsi:type="ecore:EReference" name="outerEntry" eType="#//TracePoint"
        containment="true" resolveProxies="false"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="entry" eType="#//TracePoint"
        containment="true" resolveProxies="false"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="exit" eType="#//TracePoint"
        containment="true" resolveProxies="false"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="outerExit" eType="#//TracePoint"
        containment="true" resolveProxies="false"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="PeripheralAction" abstract="true" eSuperTypes="../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//HasResourcePeripheral #//ResourceAction">
    <eStructuralFeatures xsi:type="ecore:EReference" name="peripheral" lowerBound="1"
        eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//Peripheral"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="schedulingType" eType="#//SchedulingType"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Claim" eSuperTypes="#//ResourceAction">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="passive" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"
        defaultValueLiteral="false"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Release" eSuperTypes="#//ResourceAction"/>
  <eClassifiers xsi:type="ecore:EClass" name="LocationPrerequisite" eSuperTypes="../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//HasResourcePeripheral">
    <eStructuralFeatures xsi:type="ecore:EReference" name="resource" lowerBound="1"
        eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//IResource"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="peripheral" lowerBound="1"
        eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//Peripheral"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="position" lowerBound="1"
        eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//SymbolicPosition"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="activity" lowerBound="1"
        eType="#//Activity" eOpposite="#//Activity/prerequisites"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="TracePoint">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="value" lowerBound="1" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="regex" lowerBound="1" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="SyncBar" eSuperTypes="../../org.eclipse.lsat.common.graph.directed.editable/model/edg.ecore#//Node"/>
  <eClassifiers xsi:type="ecore:EClass" name="SimpleAction" eSuperTypes="#//PeripheralAction">
    <eStructuralFeatures xsi:type="ecore:EReference" name="type" lowerBound="1" eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//ActionType"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EEnum" name="SchedulingType">
    <eLiterals name="ASAP" literal="As Soon As Possible (ASAP)"/>
    <eLiterals name="ALAP" value="1" literal="As Late As Possible (ALAP)"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Move" eSuperTypes="#//PeripheralAction">
    <eStructuralFeatures xsi:type="ecore:EReference" name="profile" lowerBound="1"
        eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//Profile"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="successorMove" eType="#//Move"
        changeable="false" volatile="true" transient="true" derived="true">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="get" value="org.eclipse.lsat.common.queries.QueryableIterable&lt;org.eclipse.lsat.common.graph.directed.editable.Node> successorNodes = &#xA;&#x9;&#x9;org.eclipse.lsat.common.graph.directed.editable.EdgQueries.allSuccessors(this);&#xA;org.eclipse.lsat.common.queries.QueryableIterable&lt;?  extends Move> successorMoves = ActivityQueries.getActionsFor(&#xA;&#x9;&#x9;getResource(), getPeripheral(), getClass(), successorNodes);&#xA;return successorMoves.first();"/>
        <details key="documentation" value="Returns the consecutive move for this peripheral (if any). A consecutive move is a move that is immediately executed after this move (not delayed by any other action), this means that there's either a direct dependency from this move to the consecutive move, or there's a syncbar in between with 1 input dependency from this move only.&lt;br>&#xD;&#xA;&lt;b>NOTE:&lt;/b> If the model would contain two or more consecutive moves (=invalid model), this method rturns &lt;tt>null&lt;/tt>&#xD;&#xA;"/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="predecessorMove" eType="#//Move"
        changeable="false" volatile="true" transient="true" derived="true">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="get" value="org.eclipse.lsat.common.queries.QueryableIterable&lt;org.eclipse.lsat.common.graph.directed.editable.Node> predecessorNodes = &#xA;&#x9;&#x9;org.eclipse.lsat.common.graph.directed.editable.EdgQueries.allPredecessors(this);&#xA;org.eclipse.lsat.common.queries.QueryableIterable&lt;? extends Move> predecessorMoves = ActivityQueries.getActionsFor(&#xA;&#x9;&#x9;getResource(), getPeripheral(), getClass(), predecessorNodes);&#xA;return predecessorMoves.first();"/>
        <details key="documentation" value="Returns the previous move for this peripheral (if any)."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="stopAtTarget" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean" volatile="true"
        transient="true">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="get" value="boolean result =  isPositionMove() ? isPassing(): isContinuing();&#xA;return !result;"/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="positionMove" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean" changeable="false"
        volatile="true" transient="true">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="get" value="return getPeripheral() != null &amp;&amp; getPeripheral().getDistances().isEmpty();"/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="targetPosition" eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//SymbolicPosition"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="sourcePosition" eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//SymbolicPosition"
        changeable="false" volatile="true" transient="true" derived="true">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="get" value="Object previousMove = getPredecessorMove();&#xA;if (previousMove instanceof Move) {&#xA;&#x9;return ((Move)previousMove).getTargetPosition();&#xA;}&#xA;if (getGraph() instanceof activity.Activity) {&#xA;&#x9;for (activity.LocationPrerequisite locationPrerequisite : ((activity.Activity)getGraph()).getPrerequisites()) {&#xA;&#x9;&#x9;if (rpEquals(locationPrerequisite)) {&#xA;&#x9;&#x9;&#x9;return locationPrerequisite.getPosition();&#xA;&#x9;&#x9;}&#xA;&#x9;}&#xA;}&#xA;return null;"/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="distance" eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//Distance"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="passing" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="continuing" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="EventAction" abstract="true" eSuperTypes="#//ResourceAction"/>
  <eClassifiers xsi:type="ecore:EClass" name="RaiseEvent" eSuperTypes="#//EventAction"/>
  <eClassifiers xsi:type="ecore:EClass" name="RequireEvent" eSuperTypes="#//EventAction"/>
  <eClassifiers xsi:type="ecore:EClass" name="ResourceAction" abstract="true" eSuperTypes="#//Action">
    <eStructuralFeatures xsi:type="ecore:EReference" name="resource" lowerBound="1"
        eType="ecore:EClass ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//IResource"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Event" eSuperTypes="../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//Resource">
    <eOperations name="getResourceType" lowerBound="1" eType="ecore:EEnum ../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//ResourceType"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="EventItem" eSuperTypes="../../org.eclipse.lsat.machine.dsl/model/machine.ecore#//ResourceItem"/>
</ecore:EPackage>
