/**
 */
package lsat_graph;

import activity.Claim;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Claim Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lsat_graph.lsat_graphPackage#getClaimTask()
 * @model
 * @generated
 */
public interface ClaimTask extends ActionTask<Claim> {
} // ClaimTask
