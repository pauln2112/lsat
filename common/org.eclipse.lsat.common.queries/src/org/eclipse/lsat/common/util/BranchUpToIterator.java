/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.function.Predicate;

public class BranchUpToIterator<E> extends ProcessingIterator<E> implements BranchIterator<E> {
    private final BranchIterator<? extends E> source;

    private final Predicate<? super E> predicate;

    private final boolean isIncluding;

    private boolean canPrune = false;

    private boolean isSourcePruned = false;

    public BranchUpToIterator(BranchIterator<E> source, Predicate<? super E> predicate, boolean isIncluding) {
        this.source = source;
        this.predicate = predicate;
        this.isIncluding = isIncluding;
    }

    @Override
    protected boolean toNext() {
        while (source.hasNext()) {
            final E next = source.next();
            isSourcePruned = false;
            if (predicate.test(next)) {
                source.prune();
                isSourcePruned = true;
                if (isIncluding) {
                    return setNext(next);
                }
            } else {
                return setNext(next);
            }
        }
        return done();
    }

    @Override
    public boolean hasNext() {
        canPrune = false;
        return super.hasNext();
    }

    @Override
    public E next() {
        canPrune = true;
        return super.next();
    }

    @Override
    public void prune() {
        if (canPrune) {
            if (!isSourcePruned) {
                source.prune();
                isSourcePruned = true;
            }
            canPrune = false;
        } else {
            throw new IllegalStateException("Prune is not allowed");
        }
    }
}
