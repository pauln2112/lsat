/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.layout;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import org.eclipse.elk.graph.ElkEdgeSection;
import org.eclipse.elk.graph.ElkGraphElement;
import org.eclipse.elk.graph.ElkLabel;
import org.eclipse.elk.graph.ElkNode;
import org.eclipse.elk.graph.ElkPort;
import org.eclipse.elk.graph.ElkShape;

@SuppressWarnings("all")
public class ElkGlobalCoordinates {
  private ElkGlobalCoordinates() {
  }
  
  public static Point2D.Double getGlobalLocation(final ElkShape shape) {
    final ElkGraphElement parent = ElkGlobalCoordinates.getParent(shape);
    Point2D.Double _xifexpression = null;
    if ((parent instanceof ElkShape)) {
      _xifexpression = ElkGlobalCoordinates.getGlobalLocation(((ElkShape)parent));
    } else {
      _xifexpression = new Point2D.Double();
    }
    final Point2D.Double anchor = _xifexpression;
    return ElkGlobalCoordinates.translate(anchor, shape.getX(), shape.getY());
  }
  
  public static Rectangle2D.Double getGlobalBounds(final ElkShape shape) {
    final Point2D.Double globalLocation = ElkGlobalCoordinates.getGlobalLocation(shape);
    double _width = shape.getWidth();
    double _height = shape.getHeight();
    return new Rectangle2D.Double(globalLocation.x, globalLocation.y, _width, _height);
  }
  
  public static void setGlobalLocation(final ElkShape shape, final double x, final double y) {
    final ElkGraphElement parent = ElkGlobalCoordinates.getParent(shape);
    if ((parent instanceof ElkShape)) {
      final Point2D.Double parentLocation = ElkGlobalCoordinates.getGlobalLocation(((ElkShape)parent));
      shape.setLocation((x - parentLocation.x), (y - parentLocation.y));
    } else {
      shape.setLocation(x, y);
    }
  }
  
  public static Point2D.Double getGlobalStartLocation(final ElkEdgeSection section) {
    final ElkNode container = section.getParent().getContainingNode();
    Point2D.Double _xifexpression = null;
    if ((container == null)) {
      double _startX = section.getStartX();
      double _startY = section.getStartY();
      _xifexpression = new Point2D.Double(_startX, _startY);
    } else {
      _xifexpression = ElkGlobalCoordinates.translate(ElkGlobalCoordinates.getGlobalLocation(container), section.getStartX(), section.getStartY());
    }
    return _xifexpression;
  }
  
  public static void setGlobalStartLocation(final ElkEdgeSection section, final double x, final double y) {
    final ElkNode container = section.getParent().getContainingNode();
    if ((container == null)) {
      section.setStartLocation(x, y);
    } else {
      final Point2D.Double containerLocation = ElkGlobalCoordinates.getGlobalLocation(container);
      section.setStartLocation((x - containerLocation.x), (y - containerLocation.y));
    }
  }
  
  public static Point2D.Double getGlobalEndLocation(final ElkEdgeSection section) {
    final ElkNode container = section.getParent().getContainingNode();
    Point2D.Double _xifexpression = null;
    if ((container == null)) {
      double _endX = section.getEndX();
      double _endY = section.getEndY();
      _xifexpression = new Point2D.Double(_endX, _endY);
    } else {
      _xifexpression = ElkGlobalCoordinates.translate(ElkGlobalCoordinates.getGlobalLocation(container), section.getEndX(), section.getEndY());
    }
    return _xifexpression;
  }
  
  public static void setGlobalEndLocation(final ElkEdgeSection section, final double x, final double y) {
    final ElkNode container = section.getParent().getContainingNode();
    if ((container == null)) {
      section.setEndLocation(x, y);
    } else {
      final Point2D.Double containerLocation = ElkGlobalCoordinates.getGlobalLocation(container);
      section.setEndLocation((x - containerLocation.x), (y - containerLocation.y));
    }
  }
  
  private static ElkGraphElement getParent(final ElkShape shape) {
    ElkGraphElement _switchResult = null;
    final ElkShape it = shape;
    boolean _matched = false;
    if (it instanceof ElkNode) {
      _matched=true;
      _switchResult = ((ElkNode)it).getParent();
    }
    if (!_matched) {
      if (it instanceof ElkPort) {
        _matched=true;
        _switchResult = ((ElkPort)it).getParent();
      }
    }
    if (!_matched) {
      if (it instanceof ElkLabel) {
        _matched=true;
        _switchResult = ((ElkLabel)it).getParent();
      }
    }
    if (!_matched) {
      _switchResult = null;
    }
    return _switchResult;
  }
  
  public static Point2D.Double translate(final Point2D.Double point, final double deltaX, final double deltaY) {
    point.x = (point.x + deltaX);
    point.y = (point.y + deltaY);
    return point;
  }
  
  public static Rectangle2D.Double translate(final Rectangle2D.Double bounds, final double deltaX, final double deltaY) {
    bounds.x = (bounds.x + deltaX);
    bounds.y = (bounds.y + deltaY);
    return bounds;
  }
}
