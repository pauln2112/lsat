/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler

import activity.Move
import java.util.Collection
import lsat_graph.PeripheralActionTask
import org.eclipse.lsat.common.graph.directed.DirectedGraphFactory
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.schedule.Schedule
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask
import java.util.Arrays
import java.util.LinkedHashSet
import java.math.BigDecimal

class ConcatenatedMoveAnalysis<T extends Task> {
    
    public static val PASSING_MOVE_TIMING_GAP ='PassingMoveTimingGap'


    def Collection<Collection<Move>> getErroneousPassingMoves(Schedule<T> schedule) {
        val result = new LinkedHashSet<Collection<Move>>
        for (sequence : schedule.allNodesInTopologicalOrder.filter[move !== null].groupBy[move.resource].values) {
            sequence.forEach [ it, index |
                if (move.passing) {
                    if (!eq(sequence.get(index + 1).startTime,endTime)) {
                        result.add(Arrays.asList(move,sequence.get(index + 1).move))
                    }
                }
            ]
        }
        return result;
    }
    
    def void annotateErroneousPassingMoves(Schedule<T> schedule) {
        
        val aspect = DirectedGraphFactory.eINSTANCE.createAspect();
        aspect.name = PASSING_MOVE_TIMING_GAP
        schedule.allNodesInTopologicalOrder.filter[move !== null].groupBy[move.resource].values.forEach [ sequence |
         
            sequence.forEach [ it, index |
                if (move.passing) {
                    if (!eq(sequence.get(index + 1).startTime,endTime)) {
                        aspect.nodes += sequence.get(index + 1)
                        aspect.nodes += sequence.get(index)
                    }
                }
            ]
        ]
        
        if(!aspect.nodes.empty){
            aspect.graph = schedule
        }
    }

    def eq(BigDecimal a, BigDecimal b){
        if( a === b ) return true;
        if (a === null || b === null) return false;
        return a.compareTo(b) == 0
    }
    
    def static <T extends Task> Move getMove(ScheduledTask<T> task){
        var childTask = task.task
        if(childTask instanceof PeripheralActionTask){
            if(childTask.action instanceof Move){
                return childTask.action as Move
            }
        }
        return null;
    }
}
