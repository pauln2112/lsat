<?xml version="1.0"?>
<!--

    Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:y="http://www.yworks.com/xml/graphml" xmlns:gml="http://graphml.graphdrawing.org/xmlns"
    xmlns:graph="http://www.eclipse.org/lsat/scheduler/graph" xmlns:lsat="http://www.eclipse.org/lsat/scheduler/graph"
    xmlns="http://www.eclipse.org/lsat/graph">
    <xsl:output method="xml" indent="yes" />

    <xsl:template match='/'>
        <graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
            <key id="name" for="node" attr.name="name" attr.type="string" />
            <key id="executionTime" for="node" attr.name="executionTime" attr.type="double" />
            <key id="type" for="node" attr.name="type" attr.type="string" />
            <key for="node" id="d10" yfiles.type="nodegraphics" />
            <key id="symbol" for="node" attr.name="symbol" attr.type="string" />
            <key id="lineNumber" for="node" attr.name="lineNumber" attr.type="int" />
            <key id="type" for="edge" attr.name="type" attr.type="string" />
            <key id="sourceLineNumber" for="edge" attr.name="sourceLineNumber" attr.type="int" />
            <key id="labelE" for="edge" attr.name="labelE" attr.type="string" />
            <key id="labelV" for="node" attr.name="labelV" attr.type="string" />
            <graph edgedefault="directed">
                <xsl:apply-templates select="*" />
            </graph>
        </graphml>
    </xsl:template>


    <!-- edge id="e0" source="n0" target="n1" -->
    <xsl:template match="edges">
        <edge source="{@sourceNode}" target="{@targetNode}" xmlns="http://graphml.graphdrawing.org/xmlns">
            <xsl:attribute name="id">//<xsl:if test='parent::subGraphs'>@subGraphs.<xsl:value-of
                select='count(../preceding-sibling::subGraphs)' />/</xsl:if>@edges.<xsl:value-of
                select='count(preceding-sibling::edges)' /></xsl:attribute>
        </edge>
    </xsl:template>

    <!-- <node id="n0"> <data key="d4" xml:space="preserve"><![CDATA[Top Description]]></data> -->
    <xsl:template match="nodes">
        <node labels=":{@name}({@executionTime})" xmlns="http://graphml.graphdrawing.org/xmlns">
            <xsl:attribute name="id">//<xsl:if test='parent::subGraphs'>@subGraphs.<xsl:value-of
                select='count(../preceding-sibling::subGraphs)' />/</xsl:if>@nodes.<xsl:value-of
                select='count(preceding-sibling::nodes)' /></xsl:attribute>
            <xsl:apply-templates select="@name" mode="key"/>
            <xsl:apply-templates select="@executionTime" mode="key"/>
            <data key="labelV" xmlns="http://graphml.graphdrawing.org/xmlns">
                <xsl:value-of select='@name' />
            </data>
            <data key="d10" xmlns="http://graphml.graphdrawing.org/xmlns">
           <y:ShapeNode>
                  <y:Geometry width="200" >
                    <xsl:choose>
                      <xsl:when test="@executionTime">
                        <xsl:attribute name="height"><xsl:value-of select="number(30.0 + 5*number(@executionTime))"/></xsl:attribute>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:attribute name="height">30</xsl:attribute>
                      </xsl:otherwise>
                    </xsl:choose>
                  </y:Geometry>
                  <y:NodeLabel>
                      <xsl:value-of select='@name' /><xsl:if test="@executionTime and not(number(@executionTime)=0)">(<xsl:value-of select='@executionTime' />)</xsl:if>
                  </y:NodeLabel>
                </y:ShapeNode>
            </data>

            <desc xmlns="http://graphml.graphdrawing.org/xmlns">
                <xsl:value-of select='@name' />
            </desc>
        </node>
    </xsl:template>

    <xsl:template match="@*" mode="key">
        <data key="{name(.)}" xmlns="http://graphml.graphdrawing.org/xmlns">
            <xsl:value-of select='.' />
        </data>
    </xsl:template>

    <xsl:template match="subGraphs">
        <node id="n10" yfiles.foldertype="group" xmlns="http://graphml.graphdrawing.org/xmlns">
            <xsl:attribute name="id">//@subGraphs.<xsl:value-of
                select='count(preceding-sibling::subGraphs)' /></xsl:attribute>
            <data key="d10" xmlns="http://graphml.graphdrawing.org/xmlns">
            <y:ProxyAutoBoundsNode>
                <y:Realizers active="0">
                    <y:GroupNode>
                        <y:Fill color="#F8ECC9" transparent="false"/>
                        <y:NodeLabel modelName="internal" modelPosition="tr" y="0.0">
                            <xsl:value-of select='@name' />
                        </y:NodeLabel>
                    </y:GroupNode>
                </y:Realizers>
            </y:ProxyAutoBoundsNode>
            </data>

            <graph edgedefault="directed">
                <xsl:apply-templates select="node()|@*" />
            </graph>
        </node>
    </xsl:template>


    <xsl:template match="*">
        <xsl:apply-templates select="node()|@*" />
    </xsl:template>

    <xsl:template match="@*" />


</xsl:stylesheet>   