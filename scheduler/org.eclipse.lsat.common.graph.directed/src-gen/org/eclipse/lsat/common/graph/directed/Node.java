/**
 */
package org.eclipse.lsat.common.graph.directed;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Node#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Node#getOutgoingEdges <em>Outgoing Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Node#getIncomingEdges <em>Incoming Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Node#getGraph <em>Graph</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Node#getAspects <em>Aspects</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getNode()
 * @model
 * @generated
 */
public interface Node extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getNode_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.Node#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Outgoing Edges</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.Edge}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Edge#getSourceNode <em>Source Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Edges</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getNode_OutgoingEdges()
	 * @see org.eclipse.lsat.common.graph.directed.Edge#getSourceNode
	 * @model opposite="sourceNode"
	 * @generated
	 */
	EList<Edge> getOutgoingEdges();

	/**
	 * Returns the value of the '<em><b>Incoming Edges</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.Edge}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Edge#getTargetNode <em>Target Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Edges</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getNode_IncomingEdges()
	 * @see org.eclipse.lsat.common.graph.directed.Edge#getTargetNode
	 * @model opposite="targetNode"
	 * @generated
	 */
	EList<Edge> getIncomingEdges();

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' container reference.
	 * @see #setGraph(DirectedGraph)
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getNode_Graph()
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraph#getNodes
	 * @model opposite="nodes" required="true" transient="false"
	 * @generated
	 */
	DirectedGraph<?, ?> getGraph();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.Node#getGraph <em>Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' container reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(DirectedGraph<?, ?> value);

	/**
	 * Returns the value of the '<em><b>Aspects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.Aspect}<code>&lt;?, ?&gt;</code>.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Aspect#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspects</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getNode_Aspects()
	 * @see org.eclipse.lsat.common.graph.directed.Aspect#getNodes
	 * @model opposite="nodes"
	 * @generated
	 */
	EList<Aspect<?, ?>> getAspects();

} // Node
