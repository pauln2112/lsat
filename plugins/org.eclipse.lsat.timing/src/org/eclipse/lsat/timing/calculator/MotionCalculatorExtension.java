/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.calculator;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.lsat.motioncalculator.MotionCalculator;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileParameter;
import org.eclipse.lsat.motioncalculator.MotionProfileProvider;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.motioncalculator.PositionInfo;
import org.eclipse.lsat.timing.Activator;

public class MotionCalculatorExtension implements MotionCalculator, Comparable<MotionCalculatorExtension> {
    public static final String EXTENSION_POINT = "org.eclipse.lsat.timing.calculator";

    private static final Pattern KEY_PATTERN = Pattern.compile("^[a-zA-Z_][a-zA-Z_0-9]*$");

    private static MotionCalculatorExtension selectedMotionCalculator = null;

    private final IConfigurationElement configurationElement;

    private MotionCalculator delegate = null;

    private Map<String, MotionProfile> motionProfiles = null;

    private MotionProfile defaultProfile;

    static {
        final IPreferenceStore preferenceStore = Activator.getDefault().getPreferenceStore();
        preferenceStore.setDefault(MotionCalculatorExtension.EXTENSION_POINT, PointToPointMotionCalculator.ID);
    }

    public static final List<MotionCalculatorExtension> getAvailableMotionCalculators() {
        IConfigurationElement[] configurationElements = Platform.getExtensionRegistry()
                .getConfigurationElementsFor(EXTENSION_POINT);
        List<MotionCalculatorExtension> result = new ArrayList<>(configurationElements.length);
        for (IConfigurationElement configurationElement: configurationElements) {
            if (configurationElement.isValid()) {
                result.add(new MotionCalculatorExtension(configurationElement));
            }
        }
        return result;
    }

    public static final MotionCalculatorExtension getSelectedMotionCalculator() throws MotionException {
        final IPreferenceStore preferenceStore = Activator.getDefault().getPreferenceStore();
        String motionCalculatorId = preferenceStore.getString(EXTENSION_POINT);
        if (null == selectedMotionCalculator || !selectedMotionCalculator.configurationElement.isValid()
                || !selectedMotionCalculator.getId().equals(motionCalculatorId))
        {
            List<MotionCalculatorExtension> availableMotionCalculators = getAvailableMotionCalculators();
            Optional<MotionCalculatorExtension> motionCalculator = availableMotionCalculators.stream()
                    .filter(e -> e.getId().equals(motionCalculatorId)).findFirst();
            if (!motionCalculator.isPresent()) {
                throw new MotionException("No motion calculator found for id: " + motionCalculatorId);
            }
            motionCalculator.get().initialize();
            selectedMotionCalculator = motionCalculator.get();
        }
        return selectedMotionCalculator;
    }

    private MotionCalculatorExtension(IConfigurationElement configurationElement) {
        this.configurationElement = configurationElement;
    }

    @Override
    public int compareTo(MotionCalculatorExtension o) {
        return getName().compareToIgnoreCase(o.getName());
    }

    public String getId() {
        return configurationElement.getAttribute("id");
    }

    public String getName() {
        return configurationElement.getAttribute("name");
    }

    private void initialize() throws MotionException {
        try {
            delegate = (MotionCalculator)configurationElement.createExecutableExtension("class");
        } catch (CoreException e) {
            throw new MotionException("Failed to instantiate motion calculator: " + e.getMessage(), e);
        }

        motionProfiles = new LinkedHashMap<>();
        if (delegate instanceof MotionProfileProvider) {
            MotionProfileProvider provider = (MotionProfileProvider)delegate;
            provider.getSupportedProfiles().stream().forEach(p -> motionProfiles.put(p.getKey(), p));
        }
        IConfigurationElement[] motionProfileElements = configurationElement.getChildren("MotionProfile");
        for (IConfigurationElement motionProfileElement: motionProfileElements) {
            String motionProfileKey = motionProfileElement.getAttribute("key");
            if (!KEY_PATTERN.matcher(motionProfileKey).matches()) {
                throw new MotionException(String.format("Encountered problem while configuring MotionCalculator '%s': "
                        + "MotionProfile key '%s' should start with a letter ('a'..'z'|'A'..'Z') or underscore '_' "
                        + "followed by any number of letters, underscores and numbers ('0'..'9').", getName(),
                        motionProfileKey));
            }
            String motionProfileName = motionProfileElement.getAttribute("name");
            String motionProfileURL = motionProfileElement.getAttribute("url");
            String motionProfileDefaultProfile = motionProfileElement.getAttribute("defaultProfile");

            List<MotionProfileParameter> parameters = new ArrayList<>();
            for (IConfigurationElement parameterElement: motionProfileElement.getChildren("Parameter")) {
                String parameterKey = parameterElement.getAttribute("key");
                if (!KEY_PATTERN.matcher(parameterKey).matches()) {
                    if (!KEY_PATTERN.matcher(motionProfileKey).matches()) {
                        throw new MotionException(String.format(
                                "Encountered problem while configuring MotionCalculator '%s': "
                                        + "Parameter key '%s' of MotionProfile '%s' should start with a letter ('a'..'z'|'A'..'Z') or underscore '_' "
                                        + "followed by any number of letters, underscores and numbers ('0'..'9').",
                                getName(), parameterKey, motionProfileKey));
                    }
                }
                String parematerName = parameterElement.getAttribute("name");
                boolean parameterRequired = Boolean.parseBoolean(parameterElement.getAttribute("required"));
                parameters.add(new MotionProfileParameter(parameterKey, parematerName, parameterRequired));
            }

            try {
                MotionProfile motionProfile = new MotionProfile(motionProfileKey, motionProfileName,
                        motionProfileURL == null ? null : new URL(motionProfileURL), parameters);
                motionProfile.setDefaultProfile(Boolean.valueOf(motionProfileDefaultProfile));
                MotionProfile previousValue = motionProfiles.put(motionProfileKey, motionProfile);
                if (null != previousValue) {
                    throw new MotionException("MotionProfile keys should be unique: " + motionProfileKey);
                }
            } catch (MalformedURLException e) {
                throw new MotionException(String.format("Failed to parse URL for motion profile %s: %s",
                        motionProfileName, e.getMessage()), e);
            } catch (IllegalArgumentException e) {
                throw new MotionException(e.getMessage(), e);
            }
        }
        Set<MotionProfile> defaultProfiles = motionProfiles.values().stream()
                .filter(p -> motionProfiles.size() == 1 || p.isDefaultProfile())
                .collect(Collectors.toCollection(LinkedHashSet::new));
        if (defaultProfiles.size() == 0) {
            throw new MotionException("No default MotionProfile specified");
        }
        if (defaultProfiles.size() > 1) {
            throw new MotionException(
                    String.join(",", defaultProfiles.stream().map(MotionProfile::getKey).toArray(String[]::new))
                            + " are default! Only one default MotionProfile is allowed");
        }

        defaultProfile = defaultProfiles.stream().findFirst().get();
    }

    public MotionProfile getMotionProfile(String key) {
        validate();
        if (key == null || key.isEmpty()) {
            return defaultProfile;
        }
        return motionProfiles.get(key);
    }

    public Collection<MotionProfile> getMotionProfiles() {
        validate();
        return motionProfiles.values();
    }

    @Override
    public void validate(List<MotionSegment> segments) throws MotionValidationException {
        validate();
        delegate.validate(segments);
    }

    @Override
    public List<Double> calculateTimes(List<MotionSegment> segments) throws MotionException {
        validate();
        return delegate.calculateTimes(segments);
    }

    @Override
    public Collection<PositionInfo> getPositionInfo(List<MotionSegment> segments) throws MotionException {
        validate();
        return delegate.getPositionInfo(segments);
    }

    private void validate() {
        if (null == delegate || null == motionProfiles || defaultProfile == null) {
            throw new IllegalStateException("MotionCalculatorExtension not initialized: " + getId());
        }
    }
}
