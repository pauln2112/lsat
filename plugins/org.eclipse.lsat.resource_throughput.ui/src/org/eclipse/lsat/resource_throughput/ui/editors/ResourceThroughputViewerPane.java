/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.lsat.common.emf.ui.ActionsViewerPane;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.ui.progress.UIJob;

public class ResourceThroughputViewerPane extends ActionsViewerPane {
    protected final IAction reloadAction = new Action("Reload", ResourceThroughputEditorPlugin.getPlugin()
            .getImageRegistry().getDescriptor(ResourceThroughputEditorPlugin.IMAGE_RELOAD))
    {
        @Override
        public void run() {
            reloadViewer();
        }
    };

    protected final IAction selectPhaseAction = new Action("Select phase", ResourceThroughputEditorPlugin.getPlugin()
            .getImageRegistry().getDescriptor(ResourceThroughputEditorPlugin.IMAGE_FILTER))
    {
        @Override
        public void run() {
            selectPhase();
        }
    };

    private final String title;

    private final Image image;

    public ResourceThroughputViewerPane(String title, Image image, IWorkbenchPage page,
            ResourceThroughputEditorPart part)
    {
        super(page, part);
        this.title = title;
        this.image = image;
        selectPhaseAction.setEnabled(domain().getDispatchPhases().length > 1);
    }

    @Override
    public void showFocus(boolean inFocus) {
        if (inFocus) {
            refreshTitle();
        }
        super.showFocus(inFocus);
    }

    public void refreshTitle() {
        setTitle(title + getSelectedPhasesString(), image);
    }

    @Override
    public Viewer createViewer(Composite composite) {
        return new TableViewer(composite, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
    }

    @Override
    protected void refreshViewer() {
        UIJob job = new UIJob("Refreshing editor") {
            @Override
            public IStatus runInUIThread(IProgressMonitor monitor) {
                getResourceThroughputEditorPart().refresh(true);
                return Status.OK_STATUS;
            }
        };
        job.setUser(true);
        job.schedule();
    }

    protected void reloadViewer() {
        UIJob job = new UIJob("Refreshing editor") {
            @Override
            public IStatus runInUIThread(IProgressMonitor monitor) {
                getResourceThroughputEditorPart().reload();
                return Status.OK_STATUS;
            }
        };
        job.setUser(true);
        job.schedule();
    }

    protected void selectPhase() {
        UIJob job = new UIJob("Refreshing editor") {
            @Override
            public IStatus runInUIThread(IProgressMonitor monitor) {
                askUserForSelection();
                return Status.OK_STATUS;
            }
        };
        job.setUser(true);
        job.schedule();
    }

    @Override
    public void createControl(Composite parent) {
        super.createControl(parent);

        getMenuManager().insertBefore(GROUP_ACTIONS, selectPhaseAction);
        getMenuManager().insertAfter(GROUP_ACTIONS, reloadAction);
        getToolBarManager().insertBefore(GROUP_ACTIONS, selectPhaseAction);
        getToolBarManager().insertAfter(GROUP_ACTIONS, reloadAction);

        updateActionBars();
    }

    private String getSelectedPhasesString() {
        if (domain().getDispatchPhases().length < 2) {
            return "";
        }
        String[] phases = domain().getSelectedDispatchPhases();
        return Stream.of(phases).collect(Collectors.joining(", ", " (showing dispatching phases [", "])"));
    }

    private ResourceThroughputEditorPart getResourceThroughputEditorPart() {
        return (ResourceThroughputEditorPart)getPart();
    }

    private ResourceThroughputEditingDomain domain() {
        return (ResourceThroughputEditingDomain)getEditingDomain();
    }

    private void askUserForSelection() {
        String[] phases = domain().getDispatchPhases();
        if (phases.length > 1) {
            ILabelProvider labelProvider = new LabelProvider();
            ListSelectionDialog dialog = new ListSelectionDialog(
                    PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), domain().getDispatchPhases(),
                    ArrayContentProvider.getInstance(), labelProvider, "Select dispatching phase for analysis");

            dialog.setTitle("Dispatching phase(s)");
            dialog.setInitialElementSelections(Arrays.asList(domain().getSelectedDispatchPhases()));
            dialog.open();
            if (dialog.getReturnCode() == Window.OK) {
                domain().setDispatchPhases(Stream.of(dialog.getResult()).map(Object::toString).toArray(String[]::new));
                refreshTitle();
                refreshViewer();
            }
        }
    }
}
