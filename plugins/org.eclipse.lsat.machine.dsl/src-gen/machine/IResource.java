/**
 */
package machine;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IResource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.IResource#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getIResource()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IResource extends EObject {
	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see machine.MachinePackage#getIResource_Name()
     * @model required="true"
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link machine.IResource#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     * @generated
     */
	Resource getResource();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true"
     * @generated
     */
	String fqn();

} // IResource
