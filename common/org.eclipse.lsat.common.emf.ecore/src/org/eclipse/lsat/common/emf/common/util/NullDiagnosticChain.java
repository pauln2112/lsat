/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.util;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

public final class NullDiagnosticChain implements DiagnosticChain {
    public static final DiagnosticChain INSTANCE = new NullDiagnosticChain();

    private NullDiagnosticChain() {
        /* Empty */
    }

    @Override
    public void add(Diagnostic diagnostic) {
        /* Empty */
    }

    @Override
    public void addAll(Diagnostic diagnostic) {
        /* Empty */
    }

    @Override
    public void merge(Diagnostic diagnostic) {
        /* Empty */
    }
}
