/**
 */
package lsat_graph;

import activity.ResourceAction;
import org.eclipse.lsat.common.scheduler.graph.Task;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.ActionTask#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @see lsat_graph.lsat_graphPackage#getActionTask()
 * @model abstract="true"
 * @generated
 */
public interface ActionTask<T extends ResourceAction> extends Task {
	/**
     * Returns the value of the '<em><b>Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Action</em>' reference.
     * @see #setAction(ResourceAction)
     * @see lsat_graph.lsat_graphPackage#getActionTask_Action()
     * @model required="true"
     * @generated
     */
	T getAction();

	/**
     * Sets the value of the '{@link lsat_graph.ActionTask#getAction <em>Action</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Action</em>' reference.
     * @see #getAction()
     * @generated
     */
	void setAction(T value);

} // ActionTask
