/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import activity.SchedulingType;
import com.google.common.base.Objects;
import java.math.BigDecimal;
import java.util.function.Consumer;
import lsat_graph.PeripheralActionTask;
import lsat_graph.ReleaseTask;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;
import org.eclipse.lsat.common.graph.directed.util.DirectedGraphQueries;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class ALAPScheduler {
  private static final Logger LOGGER = LoggerFactory.getLogger(ALAPScheduler.class);
  
  public static <T extends Task> Schedule<T> applyALAPScheduling(final Schedule<T> schedule) {
    ALAPScheduler.LOGGER.debug("Starting transformation");
    final Function1<ScheduledTask<T>, Boolean> _function = (ScheduledTask<T> it) -> {
      return Boolean.valueOf(ALAPScheduler.<T>supportsALAP(it));
    };
    final Consumer<ScheduledTask<T>> _function_1 = (ScheduledTask<T> it) -> {
      ALAPScheduler.<T>moveALAP(it);
    };
    IterableExtensions.<ScheduledTask<T>>filter(DirectedGraphQueries.<ScheduledTask<T>>reverseTopologicalOrdering(DirectedGraphQueries.<ScheduledTask<T>, ScheduledDependency>allSubNodes(schedule)), _function).forEach(_function_1);
    final Function1<ScheduledTask<T>, Boolean> _function_2 = (ScheduledTask<T> it) -> {
      return Boolean.valueOf(ALAPScheduler.<T>supportsASAP(it));
    };
    final Consumer<ScheduledTask<T>> _function_3 = (ScheduledTask<T> it) -> {
      ALAPScheduler.<T>moveASAP(it);
    };
    IterableExtensions.<ScheduledTask<T>>filter(DirectedGraphQueries.<ScheduledTask<T>>topologicalOrdering(DirectedGraphQueries.<ScheduledTask<T>, ScheduledDependency>allSubNodes(schedule)), _function_2).forEach(_function_3);
    ALAPScheduler.LOGGER.debug("Finished transformation");
    return schedule;
  }
  
  private static <T extends Task> boolean supportsALAP(final ScheduledTask<T> scheduledTask) {
    boolean _switchResult = false;
    T _task = scheduledTask.getTask();
    final T it = _task;
    boolean _matched = false;
    if (it instanceof PeripheralActionTask) {
      _matched=true;
      SchedulingType _schedulingType = ((PeripheralActionTask)it).getAction().getSchedulingType();
      _switchResult = Objects.equal(_schedulingType, SchedulingType.ALAP);
    }
    if (!_matched) {
      if (it instanceof ReleaseTask) {
        _matched=true;
        _switchResult = false;
      }
    }
    if (!_matched) {
      _switchResult = true;
    }
    return _switchResult;
  }
  
  private static <T extends Task> void moveALAP(final ScheduledTask<T> scheduledTask) {
    boolean _isEmpty = scheduledTask.getOutgoingEdges().isEmpty();
    if (_isEmpty) {
      return;
    }
    final Function1<Edge, ScheduledTask<?>> _function = (Edge it) -> {
      Node _targetNode = it.getTargetNode();
      return ((ScheduledTask<?>) _targetNode);
    };
    final Function1<ScheduledTask<?>, BigDecimal> _function_1 = (ScheduledTask<?> it) -> {
      return it.getStartTime();
    };
    final BigDecimal newEndTime = IterableExtensions.<BigDecimal>min(ListExtensions.<ScheduledTask<?>, BigDecimal>map(ListExtensions.<Edge, ScheduledTask<?>>map(scheduledTask.getOutgoingEdges(), _function), _function_1));
    BigDecimal _endTime = scheduledTask.getEndTime();
    boolean _greaterThan = (newEndTime.compareTo(_endTime) > 0);
    if (_greaterThan) {
      BigDecimal _endTime_1 = scheduledTask.getEndTime();
      final BigDecimal timeShift = newEndTime.subtract(_endTime_1);
      BigDecimal _startTime = scheduledTask.getStartTime();
      scheduledTask.setStartTime(_startTime.add(timeShift).stripTrailingZeros());
      BigDecimal _endTime_2 = scheduledTask.getEndTime();
      scheduledTask.setEndTime(_endTime_2.add(timeShift).stripTrailingZeros());
    }
  }
  
  private static <T extends Task> boolean supportsASAP(final ScheduledTask<T> scheduledTask) {
    boolean _switchResult = false;
    T _task = scheduledTask.getTask();
    final T it = _task;
    boolean _matched = false;
    if (it instanceof PeripheralActionTask) {
      _matched=true;
      SchedulingType _schedulingType = ((PeripheralActionTask)it).getAction().getSchedulingType();
      _switchResult = Objects.equal(_schedulingType, SchedulingType.ASAP);
    }
    if (!_matched) {
      _switchResult = true;
    }
    return _switchResult;
  }
  
  private static <T extends Task> void moveASAP(final ScheduledTask<T> scheduledTask) {
    final Function1<Edge, ScheduledTask<?>> _function = (Edge it) -> {
      Node _sourceNode = it.getSourceNode();
      return ((ScheduledTask<?>) _sourceNode);
    };
    final Function1<ScheduledTask<?>, BigDecimal> _function_1 = (ScheduledTask<?> it) -> {
      return it.getEndTime();
    };
    final BigDecimal newStartTime = Queries.<BigDecimal>max(ListExtensions.<ScheduledTask<?>, BigDecimal>map(ListExtensions.<Edge, ScheduledTask<?>>map(scheduledTask.getIncomingEdges(), _function), _function_1), BigDecimal.ZERO);
    BigDecimal _startTime = scheduledTask.getStartTime();
    boolean _lessThan = (newStartTime.compareTo(_startTime) < 0);
    if (_lessThan) {
      BigDecimal _startTime_1 = scheduledTask.getStartTime();
      final BigDecimal timeShift = newStartTime.subtract(_startTime_1);
      BigDecimal _startTime_2 = scheduledTask.getStartTime();
      scheduledTask.setStartTime(_startTime_2.add(timeShift).stripTrailingZeros());
      BigDecimal _endTime = scheduledTask.getEndTime();
      scheduledTask.setEndTime(_endTime.add(timeShift).stripTrailingZeros());
    }
  }
}
