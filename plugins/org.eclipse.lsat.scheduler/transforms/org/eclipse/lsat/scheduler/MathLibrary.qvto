/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
modeltype m_ecore "strict" uses ecore('http://www.eclipse.org/emf/2002/Ecore');
	

library MathLibrary;

blackbox query sqrt(a : EBigDecimal) : EBigDecimal;
blackbox query pow(a : EBigDecimal, b : Integer) : EBigDecimal;
blackbox query absolute(a : EBigDecimal) : EBigDecimal;
blackbox query negate(a : EBigDecimal) : EBigDecimal;
blackbox query add(value : EBigDecimal, augend : EBigDecimal) : EBigDecimal;
blackbox query subtract(value : EBigDecimal, subtrahend : EBigDecimal) : EBigDecimal;
blackbox query divide(value : EBigDecimal, divisor : EBigDecimal) : EBigDecimal;
blackbox query multiply(value : EBigDecimal, multiplicand : EBigDecimal) : EBigDecimal;
blackbox query round(value : EBigDecimal, scale : Integer) : EBigDecimal;
blackbox query randomInteger(bound : Integer) : Integer;
blackbox query random() : EBigDecimal;
blackbox query negative_infinity() : EDouble;
