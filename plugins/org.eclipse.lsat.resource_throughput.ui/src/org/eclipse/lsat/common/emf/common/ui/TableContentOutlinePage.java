/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.ui;

import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.emf.common.ui.viewer.IViewerProvider;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

/**
 * An abstract base class for content outline pages.
 * <p>
 * Clients who are defining an editor may elect to provide a corresponding content outline page. This content outline
 * page will be presented to the user via the standard Content Outline View (the user decides whether their workbench
 * window contains this view) whenever that editor is active. This class should be subclassed.
 * </p>
 * <p>
 * Internally, each content outline page consists of a standard table viewer; selections made in the table viewer are
 * reported as selection change events by the page (which is a selection provider). The table viewer is not created
 * until <code>createPage</code> is called; consequently, subclasses must extend <code>createControl</code> to configure
 * the table viewer with a proper content provider, label provider, and input element.
 * </p>
 * <p>
 * Subclasses may provide a hint for constructing the table viewer using {@link #getTableStyle()}.
 * </p>
 * <p>
 * Note that those wanting to use a control other than internally created <code>TableViewer</code> will need to
 * implement <code>IContentOutlinePage</code> directly rather than subclassing this class.
 * </p>
 */
public abstract class TableContentOutlinePage extends Page
        implements IContentOutlinePage, ISelectionChangedListener, IViewerProvider
{
    private ListenerList<ISelectionChangedListener> selectionChangedListeners = new ListenerList<>();

    private TableViewer tableViewer;

    /**
     * Create a new content outline page.
     */
    protected TableContentOutlinePage() {
        super();
    }

    @Override
    public Viewer getViewer() {
        return tableViewer;
    }

    @Override
    public void addSelectionChangedListener(ISelectionChangedListener listener) {
        selectionChangedListeners.add(listener);
    }

    /**
     * The <code>ContentOutlinePage</code> implementation of this <code>IContentOutlinePage</code> method creates a
     * table viewer. Subclasses must extend this method configure the table viewer with a proper content provider, label
     * provider, and input element.
     *
     * @param parent
     */
    @Override
    public void createControl(Composite parent) {
        tableViewer = new TableViewer(parent, getTableStyle());
        tableViewer.addSelectionChangedListener(this);
    }

    /**
     * A hint for the styles to use while constructing the TableViewer.
     * <p>
     * Subclasses may override.
     * </p>
     *
     * @return the table styles to use. By default, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL
     * @since 3.6
     */
    protected int getTableStyle() {
        return SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL;
    }

    /**
     * Fires a selection changed event.
     *
     * @param selection the new selection
     */
    protected void fireSelectionChanged(ISelection selection) {
        // create an event
        final SelectionChangedEvent event = new SelectionChangedEvent(this, selection);

        // fire the event
        Object[] listeners = selectionChangedListeners.getListeners();
        for (Object listener: listeners) {
            final ISelectionChangedListener l = (ISelectionChangedListener)listener;
            SafeRunner.run(new SafeRunnable() {
                @Override
                public void run() {
                    l.selectionChanged(event);
                }
            });
        }
    }

    @Override
    public Control getControl() {
        if (tableViewer == null) {
            return null;
        }
        return tableViewer.getControl();
    }

    @Override
    public ISelection getSelection() {
        if (tableViewer == null) {
            return StructuredSelection.EMPTY;
        }
        return tableViewer.getSelection();
    }

    /**
     * Returns this page's table viewer.
     *
     * @return this page's table viewer, or <code>null</code> if <code>createControl</code> has not been called yet
     */
    protected TableViewer getTableViewer() {
        return tableViewer;
    }

    @Override
    public void init(IPageSite pageSite) {
        super.init(pageSite);
        pageSite.setSelectionProvider(this);
    }

    @Override
    public void removeSelectionChangedListener(ISelectionChangedListener listener) {
        selectionChangedListeners.remove(listener);
    }

    @Override
    public void selectionChanged(SelectionChangedEvent event) {
        fireSelectionChanged(event.getSelection());
    }

    /**
     * Sets focus to a part in the page.
     */
    @Override
    public void setFocus() {
        tableViewer.getControl().setFocus();
    }

    @Override
    public void setSelection(ISelection selection) {
        if (tableViewer != null) {
            tableViewer.setSelection(selection);
        }
    }
}
