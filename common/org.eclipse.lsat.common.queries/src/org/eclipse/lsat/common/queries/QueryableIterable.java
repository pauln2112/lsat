/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.queries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.lsat.common.util.CollectionUtil;
import org.eclipse.lsat.common.util.IterableIterator;
import org.eclipse.lsat.common.util.IterableUtil;
import org.eclipse.lsat.common.util.LoggableIterable;
import org.eclipse.lsat.common.util.ObjIntFunction;
import org.eclipse.lsat.common.util.StreamIterator;

/**
 * More convenient API for {@link Queries} in java.
 *
 * @param <Input>
 */
public class QueryableIterable<Input> extends LoggableIterable<Input> {
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static final QueryableIterable EMPTY = new QueryableIterable(Collections.EMPTY_LIST);

    private Iterable<Input> input;

    /**
     * Creates a {@link QueryableIterable} from the input
     *
     * @param input input
     * @return the input as {@link QueryableIterable}
     * @see Arrays#asList(Object...)
     * @see #from(Iterable)
     */
    @SafeVarargs
    public static <T> QueryableIterable<T> from(T... input) {
        return input == null ? empty() : new QueryableIterable<T>(IterableUtil.asList(input));
    }

    /**
     * Creates a {@link QueryableIterable} from the input
     *
     * @param input input
     * @return the input as {@link QueryableIterable}
     */
    public static <T> QueryableIterable<T> from(Iterable<T> input) {
        if (input == null) {
            return empty();
        } else if (input instanceof QueryableIterable) {
            return (QueryableIterable<T>)input;
        } else {
            return new QueryableIterable<T>(input);
        }
    }

    /**
     * Creates a {@link QueryableIterable} from the input<br>
     * <b>NOTE:</b> The input can only be queried once! For multiple queries the result should be consolidated first
     * using {@link #asList()} or {@link #asOrderedSet()}.
     *
     * @param input input
     * @return the input as {@link QueryableIterable}
     */
    public static <T> QueryableIterable<T> from(Iterator<T> input) {
        return input == null ? empty() : new QueryableIterable<T>(new IterableIterator<T>(input));
    }

    /**
     * Creates a {@link QueryableIterable} from the input<br>
     * <b>NOTE:</b> The input can only be queried once! For multiple queries the result should be consolidated first
     * using {@link #asList()} or {@link #asOrderedSet()}.
     *
     * @param input input
     * @return the input as {@link QueryableIterable}
     */
    public static <T> QueryableIterable<T> from(Stream<T> input) {
        return input == null ? empty()
                : new QueryableIterable<T>(new IterableIterator<T>(new StreamIterator<T>(input)));
    }

    /**
     * Creates a {@link QueryableIterable} from the input
     *
     * @param input input
     * @return the input as {@link QueryableIterable}
     */
    public static <K, V> QueryableIterable<Map.Entry<K, V>> from(Map<K, V> input) {
        return input == null ? empty() : new QueryableIterable<>(input.entrySet());
    }

    public static <T, O> QueryableIterable<T> from(O owner, ObjIntFunction<? super O, T> getter,
            ToIntFunction<? super O> size)
    {
        return owner == null ? empty() : new QueryableIterable<>(IterableUtil.iterate(owner, getter, size));
    }

    @SuppressWarnings("unchecked")
    public static <T> QueryableIterable<T> empty() {
        return EMPTY;
    }

    /**
     * Constructor
     */
    protected QueryableIterable(Iterable<Input> input) {
        this.input = input;
    }

    /**
     * Subclasses may override this to create an instance of their own class
     */
    protected <T> QueryableIterable<T> wrap(Iterable<T> input) {
        return new QueryableIterable<T>(input);
    }

    @Override
    public Iterator<Input> iterator() {
        return input.iterator();
    }

    public Stream<Input> stream() {
        return StreamSupport.stream(input.spliterator(), false);
    }

    /**
     * Behaves like OCL <tt>this</tt>->union(<tt>other</tt>)
     *
     * @see #including(Iterable)
     */
    public QueryableIterable<Input> union(Iterable<? extends Input> other) {
        return including(other);
    }

    /**
     * Behaves like OCL <tt>this</tt>->union(<tt>other</tt>)
     *
     * @see #including(Object...)
     */
    @SuppressWarnings("unchecked")
    public QueryableIterable<Input> union(Input... other) {
        return including(other);
    }

    /**
     * Behaves like OCL <tt>this</tt>->union(<tt>other</tt>)
     *
     * @see IterableQueries#including(Iterable, Iterable)
     */
    public QueryableIterable<Input> including(Iterable<? extends Input> includes) {
        return wrap(IterableQueries.including(input, includes));
    }

    /**
     * Behaves like OCL <tt>this</tt>->including(<tt>other</tt>)
     *
     * @see IterableQueries#including(Iterable, Object...)
     */
    @SafeVarargs
    public final QueryableIterable<Input> including(Input... includes) {
        return wrap(IterableQueries.including(input, includes));
    }

    /**
     * Behaves like OCL <tt>this</tt>->includes(<tt>other</tt>)
     */
    public final boolean includes(Input includes) {
        return exists(i -> i == includes);
    }

    /**
     * Behaves like OCL <tt>this</tt>->excluding(<tt>other</tt>)
     */
    public QueryableIterable<Input> excluding(Iterable<?> excludes) {
        return wrap(IterableQueries.excluding(input, excludes));
    }

    /**
     * Behaves like OCL <tt>this</tt>->excluding(<tt>other</tt>)
     */
    @SafeVarargs
    public final QueryableIterable<Input> excluding(Object... excludes) {
        return wrap(IterableQueries.excluding(input, excludes));
    }

    /**
     * Behaves like OCL <tt>this</tt>->excludes(<tt>other</tt>)
     */
    public final boolean excludes(Input excludes) {
        return !includes(excludes);
    }

    /**
     * Collects all children and children's children without the need for creating a temporary Collection instance.<br>
     * Behaves like OCL <tt>this</tt>->closure(e | <tt>functor.closureOf(e)</tt>)
     */
    public QueryableIterable<Input> closure(Function<Input, Iterable<? extends Input>> functor) {
        return wrap(IterableQueries.closure(input, functor));
    }

    /**
     * Collects self, all children and children's children without the need for creating a temporary Collection
     * instance.<br>
     * Behaves like OCL <tt>this</tt>->union(<tt>this</tt>->closure(e | <tt>functor.closureOf(e)</tt>))
     */
    public QueryableIterable<Input> closure(boolean includeSelf, Function<Input, Iterable<? extends Input>> functor) {
        return wrap(IterableQueries.closure(input, includeSelf, functor));
    }

    /**
     * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
     * <code>false</code>. No need for creating a temporary Collection instance.<br>
     *
     * @see #closure(Function)
     */
    public QueryableIterable<Input> closureWhile(boolean includeSelf,
            Function<Input, Iterable<? extends Input>> functor, Predicate<? super Input> predicate)
    {
        return wrap(IterableQueries.closureWhile(input, includeSelf, functor, predicate));
    }

    /**
     * Collects child, child's child, etc without the need for creating a temporary Collection instance. Behaves like
     * OCL <tt>iterator</tt>->closure(e | <tt>functor.getOne(e)</tt>)
     */
    public QueryableIterable<Input> closureOne(Function<Input, ? extends Input> functor) {
        return wrap(IterableQueries.closureOne(input, functor));
    }

    /**
     * Collects child, child's child, etc without the need for creating a temporary Collection instance. Behaves like
     * OCL <tt>this</tt>->union(<tt>this</tt>->closure(e | <tt>functor.getOne(e)</tt>))
     */
    public QueryableIterable<Input> closureOne(boolean includeSelf, Function<Input, ? extends Input> functor) {
        return wrap(IterableQueries.closureOne(input, includeSelf, functor));
    }

    /**
     * Collects all children and children's children and stops the sub branch as soon as the predicate evaluates to
     * <code>false</code>. No need for creating a temporary Collection instance.<br>
     *
     * @see #closure(Function)
     */
    public QueryableIterable<Input> closureOneWhile(boolean includeSelf, Function<Input, ? extends Input> functor,
            Predicate<? super Input> predicate)
    {
        return wrap(IterableQueries.closureOneWhile(input, includeSelf, functor, predicate));
    }

    /**
     * Collects all ancestors (parent and parent's parent) per element in this iterable without the need for creating a
     * temporary Collection instance. Note that the resulting iterator might contain a tree node multiple times if
     * elements in this iterable share this tree node as common ancestor. You can use {@link #unique()} to filter these
     * duplicates.
     */
    public QueryableIterable<Input> climbTree(Function<Input, ? extends Input> functor) {
        return wrap(IterableQueries.climbTree(input, functor));
    }

    /**
     * Collects self (only if <tt>includeSelf</tt> is <code>true</code>), parent and parent's parents per element in
     * this iterable without the need for creating a temporary Collection instance. Note that the resulting iterator
     * might contain a tree node multiple times if elements in this iterable share this tree node as common ancestor.
     * You can use {@link #unique()} to filter these duplicates.
     */
    public QueryableIterable<Input> climbTree(boolean includeSelf, Function<Input, ? extends Input> functor) {
        return wrap(IterableQueries.climbTree(input, includeSelf, functor));
    }

    /**
     * Collects all descendants (children and children's children; depth first) without the need for creating a
     * temporary Collection instance.
     */
    public QueryableIterable<Input> walkTree(Function<Input, Iterable<? extends Input>> functor) {
        return wrap(IterableQueries.walkTree(input, functor));
    }

    /**
     * Collects self (only if <tt>includeSelf</tt> is <code>true</code>), all children and children's children - depth
     * first - without the need for creating a temporary Collection instance.
     */
    public QueryableIterable<Input> walkTree(boolean includeSelf, Function<Input, Iterable<? extends Input>> functor) {
        return wrap(IterableQueries.walkTree(input, includeSelf, functor));
    }

    /**
     * Selects elements without the need for creating a temporary Collection instance.<br>
     * Behaves like OCL <tt>this</tt>->select(e | functor.test(e))
     */
    public QueryableIterable<Input> select(Predicate<? super Input> functor) {
        return wrap(IterableQueries.select(input, functor));
    }

    /**
     * Same as {@link #select(Predicate)}, but <tt>null</tt> values in output are skipped.<br>
     * <br>
     * Behaves like OCL <tt>this</tt>->xselect(e | functor.test(e))
     */
    public QueryableIterable<Input> xselect(Predicate<? super Input> functor) {
        return wrap(IterableQueries.xselect(input, functor));
    }

    /**
     * Rejects elements without the need for creating a temporary Collection instance.<br>
     * Behaves like OCL <tt>this</tt>->reject(e | functor.test(e))
     */
    public QueryableIterable<Input> reject(Predicate<? super Input> functor) {
        return wrap(IterableQueries.reject(input, functor));
    }

    /**
     * Filters this iterable, only keeping items that are instance of the specified type. <br>
     * Behaves like OCL <tt>this</tt>->objectsOfKind(<tt>Output</tt>)
     */
    public <Output> QueryableIterable<Output> objectsOfKind(Class<Output> outputType) {
        return wrap(IterableQueries.objectsOfKind(input, outputType));
    }

    /**
     * Filters this iterable, only keeping item of a specific type. <br>
     * <b>NOTE:</b> If <tt>outputType</tt> is an interface, only items are selected which directly implement the
     * interface. <br>
     * Behaves like OCL <tt>this</tt>->objectsOfType(<tt>Output</tt>)
     */
    public <Output> QueryableIterable<Output> objectsOfType(Class<Output> outputType) {
        return wrap(IterableQueries.objectsOfType(input, outputType));
    }

    /**
     * Cast this iterable to a specific type. <br>
     * Behaves like OCL <tt>this</tt>->oclAsType(<tt>Output</tt>)
     */
    public <Output> QueryableIterable<Output> asType(Class<Output> outputType) {
        return wrap(IterableQueries.asType(input, outputType));
    }

    /**
     * Collects elements without the need for creating a temporary Collection instance.<br>
     * Behaves like OCL <tt>this</tt>->collect(e | functor.collectFrom(e))
     */
    public <Output> QueryableIterable<Output> collect(Function<? super Input, Iterable<? extends Output>> functor) {
        return wrap(IterableQueries.collect(input, functor));
    }

    /**
     * Collects elements without the need for creating a temporary Collection instance.<br>
     * Behaves like QVTo <tt>iterable</tt>->xcollect(e | functor.collectFrom(e)) Same as
     * {@link #collect(CollectFunctor)}, but <tt>null</tt> values in output are skipped.
     */
    public <Output> QueryableIterable<Output> xcollect(Function<? super Input, Iterable<? extends Output>> functor) {
        return wrap(IterableQueries.xcollect(input, functor));
    }

    /**
     * Collects single attribute values without the need for creating a temporary Collection instance.
     */
    public <Output> QueryableIterable<Output> collectOne(Function<? super Input, Output> functor) {
        return wrap(IterableQueries.collectOne(input, functor));
    }

    /**
     * Collects single attribute values without the need for creating a temporary Collection instance, skipping
     * <tt>null</tt> values.
     */
    public <Output> QueryableIterable<Output> xcollectOne(Function<? super Input, Output> functor) {
        return wrap(IterableQueries.xcollectOne(input, functor));
    }

    /**
     * Behaves like OCL <tt>this</tt>->forAll(e | functor.test(e))
     */
    public boolean forAll(Predicate<? super Input> functor) {
        return IterableQueries.forAll(input, functor);
    }

    /**
     * Return true if this iterable contains an element that matches the predicate or false otherwise.<br>
     * Behaves like QVTo <tt>iterator</tt>->exists(e | functor.test(e))
     */
    public boolean exists(Predicate<? super Input> functor) {
        return IterableQueries.exists(input, functor);
    }

    /**
     * Return first element that matches the predicate or null if no match.<br>
     * Behaves like QVTo <tt>iterator</tt>->any(e | functor.test(e))
     */
    public Input any(Predicate<? super Input> functor) {
        return IterableQueries.any(input, functor);
    }

    /**
     * Return first element of this {@link QueryableIterable}, or null if empty.<br>
     * Behaves like QVTo <tt>iterator</tt>->first()
     *
     * @return the first result, or {@code null}.
     */
    public Input first() {
        return IterableUtil.first(input);
    }

    /**
     * Return last element of this {@link QueryableIterable}, or null if empty.<br>
     * <b>NOTE:</b> This method will consume the whole query, but is memory efficient<br>
     * Behaves like QVTo <tt>iterator</tt>->last()
     *
     * @return the last result, or {@code null}.
     */
    public Input last() {
        return IterableUtil.last(input);
    }

    /**
     * @see IterableUtil#toArray(Iterable)
     */
    public Object[] toArray() {
        return IterableUtil.toArray(input);
    }

    /**
     * @see IterableUtil#toArray(Iterable, Class)
     */
    public <Output> Output[] toArray(Class<Output> type) {
        return IterableUtil.toArray(input, type);
    }

    /**
     * @see IterableUtil#asList(Iterable)
     */
    public LinkedList<Input> asList() {
        return IterableUtil.asList(input);
    }

    /**
     * @see IterableUtil#asList(Iterable, int)
     */
    public ArrayList<Input> asList(int initialCapacity) {
        return IterableUtil.asList(input, initialCapacity);
    }

    /**
     * @see CollectionUtil#asSet(Iterable)
     */
    public Set<Input> asSet() {
        return IterableUtil.asSet(input);
    }

    /**
     * @see CollectionUtil#asOrderedSet(Iterable)
     */
    public LinkedHashSet<Input> asOrderedSet() {
        return IterableUtil.asOrderedSet(input);
    }

    /**
     * @see CollectionUtil#size(Iterable)
     */
    public int size() {
        return IterableUtil.size(input);
    }

    /**
     * @see CollectionUtil#isEmpty(Iterable)
     */
    public boolean isEmpty() {
        return IterableUtil.isEmpty(input);
    }

    /**
     * Sorts the {@link Iterable} by the output of the functor.<br>
     * <b>NOTE:</b> This method will consume the whole {@link Iterable} on invocation as it needs an intermediate
     * {@link List} for the sort.
     */
    public <Output extends Comparable<? super Output>> QueryableIterable<Input>
            sortedBy(Function<? super Input, Output> functor)
    {
        return wrap(IterableUtil.sortedBy(input, functor));
    }

    /**
     * Sorts the {@link Iterable} by the output of the functor.<br>
     * <b>NOTE:</b> This method will consume the whole {@link Iterable} on invocation as it needs an intermediate
     * {@link List} for the sort.
     */
    public <Output> QueryableIterable<Input> sortedBy(Function<? super Input, Output> functor,
            Comparator<? super Output> comparator)
    {
        return wrap(IterableUtil.sortedBy(input, functor, comparator));
    }

    /**
     * Sorts the elements in this {@link Iterable}.<br>
     * <b>NOTE:</b> This method will consume the whole {@link Iterable} on invocation as it needs an intermediate
     * {@link List} for the sort.
     */
    public QueryableIterable<Input> sorted(Comparator<? super Input> comparator) {
        return wrap(IterableUtil.sortedBy(input, i -> i, comparator));
    }

    /**
     * @see IterableUtil#unique(Iterable)
     */
    public QueryableIterable<Input> unique() {
        return wrap(IterableUtil.unique(input));
    }

    /**
     * Creates a {@link String} representation of this {@link Iterable} using the {@link Object#toString()} method and
     * <code>separator</code> to separate the elements. The resulting {@link String} is wrapped with <code>begin</code>
     * and <code>end</code>.
     *
     * @param separator The spearator
     * @param begin The begin
     * @param end The end
     * @see IterableUtil#joinfields(Iterable, CharSequence, CharSequence, CharSequence)
     */
    public String joinfields(CharSequence separator, CharSequence begin, CharSequence end) {
        return IterableUtil.joinfields(input, separator, begin, end);
    }

    /**
     * Returns an {@link QueryableIterable} which pairs the elements of <tt>this</tt> and <tt>iterableB</tt> such that
     * Pair{n}.a = this{n} and Pair{n}.b = iterableB{n}. The amount of elements in the resulting iterator will be
     * deduced from either <tt>this</tt> or <tt>iterableB</tt> depending on which of both produces the least amount of
     * elements.
     *
     * @see IterableQueries#zip(Iterable, Iterable)
     */
    public <InputB> QueryableIterable<Pair<Input, InputB>> zip(Iterable<? extends InputB> iterableB) {
        return wrap(IterableQueries.zip(input, iterableB));
    }

    /**
     * Returns an iterator which pairs the elements of <tt>iteratorA</tt> and <tt>iteratorB</tt> such that <br>
     * <code>comparator.compare(Pair.a, Pair.b) == 0</code>.<br>
     * For elements which only occur in <tt>iteratorA</tt> or <tt>iteratorB</tt> an incomplete pair will be returned
     * (meaning respectively either a or b will be <tt>null</tt>)<br>
     * <br>
     * <b>NOTE:</b> Both this iterable as <tt>iterableB</tt> should be sorted with respect to the <tt>comparator</tt>
     */
    public <InputB> QueryableIterable<Pair<Input, InputB>> zip(Iterable<? extends InputB> iterableB,
            ToIntBiFunction<? super Input, ? super InputB> comparator)
    {
        return wrap(IterableQueries.zip(input, iterableB, comparator));
    }

    /**
     * Behaves like QVTo <tt>this</tt>->product(<tt>iterableB</tt>)
     *
     * @see IterableQueries#product(Iterable, Iterable)
     */
    public <InputB> Iterable<Pair<Input, InputB>> product(Iterable<? extends InputB> iterableB) {
        return wrap(IterableQueries.product(input, iterableB));
    }

    /**
     * Returns a map for which the Map.values are the given elements in the given order, and each key is the product of
     * invoking a supplied function computeKeys on its corresponding value. If the function produces the same key for
     * different values, the last one will be contained in the map.
     */
    public <K> Map<K, Input> toMap(Function<? super Input, K> computeKeys) {
        return IterableQueries.toMap(input, computeKeys);
    }

    /**
     * Returns a map for which the Map.values are the product of invoking supplied function computeValues on input
     * iterable elements, and each key is the product of invoking a supplied function computeKeys on same elements. If
     * the function produces the same key for different values, the last one will be contained in the map.
     */
    public <K, V> Map<K, V> toMap(Function<? super Input, K> computeKeys, Function<? super Input, V> computeValues) {
        return IterableQueries.toMap(input, computeKeys, computeValues);
    }

    /**
     * Returns a map for which the Map.values is a collection of lists, where the elements in the list will appear in
     * the order as they appeared in the iterable. Each key is the product of invoking the supplied function computeKeys
     * on its corresponding value. So a key of that map groups a list of values for which the function produced exactly
     * that key.
     */
    public <K> Map<K, List<Input>> groupBy(Function<? super Input, K> computeKeys) {
        return IterableQueries.groupBy(input, computeKeys);
    }

    /**
     * Groups this iterable into groups of equivalent inputs.
     *
     * @param <Input> type of input
     * @param iterable source of inputs
     * @param equivalence defines the equivalence relation of two inputs, e.g. {@link Objects#equals(Object, Object)}.
     * @return a list of lists where every list contains equivalent inputs.
     * @see #groupBy(Comparator, BiPredicate)
     */
    public List<List<Input>> groupBy(Iterable<Input> iterable, BiPredicate<? super Input, ? super Input> equivalence) {
        return IterableQueries.groupBy(input, equivalence);
    }

    /**
     * Groups this iterable into groups of equivalent inputs.
     *
     * @param <Input> type of input
     * @param iterable source of inputs
     * @param comparator If not {@code null}, sorts the groups for performance reasons, e.g. ascending list size when
     *     all groups are expected to contain the same amount of elements or descending list size when a few groups are
     *     expected to contain most of the elements.
     * @param equivalence defines the equivalence relation of two inputs, e.g. {@link Objects#equals(Object, Object)}.
     * @return a list of lists where every list contains equivalent inputs.
     */
    public List<List<Input>> groupBy(Comparator<? super List<Input>> comparator,
            BiPredicate<? super Input, ? super Input> equivalence)
    {
        return IterableQueries.groupBy(input, comparator, equivalence);
    }

    /**
     * Returns a map for which the Map.Entry.value is the reduced value of all values found for a key. Each key is the
     * product of invoking the supplied function computeKeys on its corresponding value.
     */
    public <K, V> Map<K, V> groupByAndReduce(Function<? super Input, K> computeKeys,
            BiFunction<? super Input, V, ? extends V> computeValues)
    {
        return IterableQueries.groupByAndReduce(input, computeKeys, computeValues);
    }

    /**
     * Segments this iterable into groups, based on the {@code predicate}.
     *
     * @param predicate defines where to segment, based on two subsequent entries of the {@code iterable}.
     * @return a queryable iterable of lists (segments).
     */
    public QueryableIterable<List<Input>> segment(BiPredicate<? super Input, ? super Input> predicate) {
        return wrap(IterableQueries.segment(input, predicate));
    }
}
