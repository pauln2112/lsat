/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance.ui;

import java.util.ArrayList;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.jface.resource.ResourceLocator;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.lsat.common.emf.ui.wizards.ElementTreeSelectionWizardPage;
import org.eclipse.lsat.trace.TracePackage;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.statushandlers.StatusManager;

public class ConformanceCheckWizard extends Wizard {
    private static final IContentType TRACE_CONTENT_TYPE = Platform.getContentTypeManager()
            .getContentType(TracePackage.eCONTENT_TYPE);

    private final ConformanceCheckOptions options;

    private final ElementTreeSelectionWizardPage traceFileSelectionPage;

    private final ConformanceCheckOptionsPage conformancePropertiesPage;

    public ConformanceCheckWizard(ConformanceCheckOptions options) {
        this.options = options;
        traceFileSelectionPage = createTraceFileSelectionPage();
        conformancePropertiesPage = new ConformanceCheckOptionsPage(options);
        setWindowTitle("Run Conformance Check");
        setDefaultPageImageDescriptor(ResourceLocator
                .imageDescriptorFromBundle(ConformanceCheckWizard.class, "icons/run_wiz.png").orElse(null));
        setHelpAvailable(false);
    }

    @Override
    public void addPages() {
        addPage(traceFileSelectionPage);
        addPage(conformancePropertiesPage);
    }

    @Override
    public boolean performFinish() {
        for (Object traceFile: traceFileSelectionPage.getResult()) {
            if (traceFile instanceof IFile) {
                options.addTraceFile((IFile)traceFile);
            }
        }
        // Options page directly changes the options
        return true;
    }

    private ElementTreeSelectionWizardPage createTraceFileSelectionPage() {
        ElementTreeSelectionWizardPage traceFileSelectionPage = new ElementTreeSelectionWizardPage(
                "TraceFileSelectionPage", new WorkbenchLabelProvider(), new BaseWorkbenchContentProvider());
        traceFileSelectionPage.setTitle("Select traces");
        traceFileSelectionPage.setMessage("Select throughput traces to check against the specification");
        traceFileSelectionPage.setInput(options.getWorkspace());
        traceFileSelectionPage.addFilter(new ViewerFilter() {
            @Override
            public boolean select(Viewer viewer, Object parentElement, Object element) {
                if (element instanceof IFile) {
                    return isTraceFile((IFile)element);
                }
                return element instanceof IContainer;
            }
        });
        traceFileSelectionPage.setValidator(new ISelectionStatusValidator() {
            @Override
            public IStatus validate(Object[] selection) {
                if (null == selection || selection.length == 0) {
                    return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Please select a trace!", null);
                }
                for (Object selected: selection) {
                    if (!(selected instanceof IFile)) {
                        return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Please select trace files only!",
                                null);
                    }
                }
                return Status.OK_STATUS;
            }
        });

        try {
            // Convenience: try to select the trace files with the same name as the dispatching file
            final String traceFileName = options.getModelFile().getFullPath().removeFileExtension()
                    .addFileExtension("trace").lastSegment();
            final ArrayList<IResource> selections = new ArrayList<IResource>();
            options.getModelFile().getParent().accept(new IResourceVisitor() {
                @Override
                public boolean visit(IResource resource) throws CoreException {
                    if (resource.getName().startsWith(traceFileName)) {
                        selections.add(resource);
                    }
                    return true;
                }
            });
            traceFileSelectionPage.setInitialSelections(selections);
        } catch (CoreException e) {
            // Initial selection failed, ignore
        }
        return traceFileSelectionPage;
    }

    private boolean isTraceFile(IFile file) {
        try {
            IContentDescription contentDescription = file.getContentDescription();
            if (null == contentDescription) {
                return false;
            }
            IContentType contentType = contentDescription.getContentType();
            return null != contentType && contentType.isKindOf(TRACE_CONTENT_TYPE);
        } catch (CoreException e) {
            StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
            return false;
        }
    }
}
