/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timinganalysis.ui;

import java.awt.BasicStroke;
import java.awt.Color;

import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.BrowserFunction;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IPartService;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.internal.browser.WebBrowserEditor;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.panel.CrosshairOverlay;
import org.jfree.chart.plot.Crosshair;

@SuppressWarnings("restriction")
public class AnimationView extends WebBrowserEditor {
    private final DomainCrosshairOverlay traceOverlay = new DomainCrosshairOverlay();

    private TraceView traceView;

    @Override
    public void createPartControl(Composite parent) {
        super.createPartControl(parent);
        installBrowserFunctions(webBrowser.getBrowser());
    }

    protected void installBrowserFunctions(Browser browser) {
        new BrowserFunction(browser, "setAnimationTime") {
            @Override
            public Object function(Object[] arguments) {
                double value = (double)arguments[0];
                traceOverlay.getDomainCrosshair().setValue(value);
                return null;
            }
        };
    }

    @Override
    public void init(IEditorSite site, IEditorInput input) throws PartInitException {
        super.init(site, input);
        final IPartService service = (IPartService)getSite().getService(IPartService.class);
        service.addPartListener(new IPartListener() {
            @Override
            public void partOpened(IWorkbenchPart part) {
            }

            @Override
            public void partDeactivated(IWorkbenchPart part) {
            }

            @Override
            public void partClosed(IWorkbenchPart part) {
                if (part == traceView || part == AnimationView.this) {
                    // Break the link
                    unlinkTraceEditor();
                }
                if (part == AnimationView.this) {
                    service.removePartListener(this);
                }
            }

            @Override
            public void partBroughtToTop(IWorkbenchPart part) {
            }

            @Override
            public void partActivated(IWorkbenchPart part) {
            }
        });
    }

    public synchronized void linkTraceEditor(TraceView traceView) {
        // Unlink the previous editor (if any)
        unlinkTraceEditor();

        this.traceView = traceView;
        ChartPanel chartPanel = traceView.getViewer().getChartPanel();
        if (null != chartPanel) {
            chartPanel.addOverlay(traceOverlay);
        }
        getSite().getShell().getDisplay().asyncExec(webBrowser::refresh);
    }

    private synchronized void unlinkTraceEditor() {
        if (null != traceView) {
            ChartPanel chartPanel = traceView.getViewer().getChartPanel();
            if (null != chartPanel) {
                chartPanel.removeOverlay(traceOverlay);
            }
        }
        traceView = null;
    }

    private static class DomainCrosshairOverlay extends CrosshairOverlay {
        private static final long serialVersionUID = -241836797060724404L;

        private final Crosshair domainCrosshair = new Crosshair(0, new Color(70, 130, 180, 192), new BasicStroke(4.0f));

        public DomainCrosshairOverlay() {
            addDomainCrosshair(domainCrosshair);
        }

        public Crosshair getDomainCrosshair() {
            return domainCrosshair;
        }
    }
}
