/**
 */
package activity;

import machine.ActionType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.SimpleAction#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getSimpleAction()
 * @model
 * @generated
 */
public interface SimpleAction extends PeripheralAction {
	/**
     * Returns the value of the '<em><b>Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Type</em>' reference.
     * @see #setType(ActionType)
     * @see activity.ActivityPackage#getSimpleAction_Type()
     * @model required="true"
     * @generated
     */
	ActionType getType();

	/**
     * Sets the value of the '{@link activity.SimpleAction#getType <em>Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type</em>' reference.
     * @see #getType()
     * @generated
     */
	void setType(ActionType value);

} // SimpleAction
