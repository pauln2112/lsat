/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import static java.math.BigDecimal.valueOf;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import org.eclipse.m2m.qvt.oml.blackbox.java.Module;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;

@Module
public class MathLibrary {
    final Random random = new Random(1618033989);

    /**
     * @see Math#sqrt(double)
     */
    @Operation
    public BigDecimal sqrt(BigDecimal a) {
        return valueOf(Math.sqrt(a.doubleValue()));
    }

    /**
     * @see BigDecimal#pow(int)
     */
    @Operation
    public BigDecimal pow(BigDecimal a, int b) {
        return a.pow(b);
    }

    /*
     * EBigDecimal math operations to avoid rounding errors
     */

    /**
     * Somehow this operation fails when making it contextual.
     *
     * @see BigDecimal#abs()
     */
    @Operation
    public BigDecimal absolute(BigDecimal x) {
        return x.abs();
    }

    /**
     * @see BigDecimal#add(BigDecimal)
     */
    @Operation
    public BigDecimal add(BigDecimal value, BigDecimal augend) {
        return value.add(augend);
    }

    /**
     * @see BigDecimal#divide(BigDecimal)
     */
    @Operation
    public BigDecimal divide(BigDecimal value, BigDecimal divisor) {
        return value.divide(divisor);
    }

    /**
     * @see BigDecimal#multiply(BigDecimal)
     */
    @Operation
    public BigDecimal multiply(BigDecimal value, BigDecimal multiplicand) {
        return value.multiply(multiplicand);
    }

    /**
     * Somehow this operation fails when making it contextual.
     *
     * @see BigDecimal#negate()
     */
    @Operation
    public BigDecimal negate(BigDecimal x) {
        return x.negate();
    }

    /**
     * Somehow this operation fails when making it contextual.
     *
     * @see BigDecimal#subtract(BigDecimal)
     */
    @Operation
    public BigDecimal subtract(BigDecimal value, BigDecimal subtrahend) {
        return value.subtract(subtrahend);
    }

    /**
     * Somehow this operation fails when making it contextual.
     *
     * @see BigDecimal#setScale(int, RoundingMode)
     * @see RoundingMode#HALF_UP
     */
    @Operation
    public BigDecimal round(BigDecimal value, int scale) {
        return (value.scale() > scale) ? value.setScale(scale, RoundingMode.HALF_UP) : value;
    }

    /**
     * Returns a pseudorandom, uniformly distributed int value between 0 (inclusive) and the specified value
     * (exclusive), drawn from this random number generator's sequence.
     *
     * @see Random#nextInt(int)
     */
    @Operation
    public int randomInteger(int bound) {
        return random.nextInt(bound);
    }

    /**
     * Returns the next pseudorandom, uniformly distributed double value between 0.0 and 1.0 from this random number
     * generator's sequence.
     *
     * @see Random#nextDouble()
     */
    @Operation
    public BigDecimal random() {
        return valueOf(random.nextDouble());
    }

    /**
     * Returns a constant holding the negative infinity of type double.
     *
     * @see Double#NEGATIVE_INFINITY
     */
    @Operation
    public double negative_infinity() {
        return Double.NEGATIVE_INFINITY;
    }
}
