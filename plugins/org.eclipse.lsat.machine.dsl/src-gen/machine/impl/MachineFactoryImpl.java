/**
 */
package machine.impl;

import java.util.Map;

import machine.*;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MachineFactoryImpl extends EFactoryImpl implements MachineFactory {
	/**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static MachineFactory init() {
        try
        {
            MachineFactory theMachineFactory = (MachineFactory)EPackage.Registry.INSTANCE.getEFactory(MachinePackage.eNS_URI);
            if (theMachineFactory != null)
            {
                return theMachineFactory;
            }
        }
        catch (Exception exception)
        {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new MachineFactoryImpl();
    }

	/**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MachineFactoryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject create(EClass eClass) {
        switch (eClass.getClassifierID())
        {
            case MachinePackage.PERIPHERAL_TYPE: return createPeripheralType();
            case MachinePackage.SYMBOLIC_POSITION: return createSymbolicPosition();
            case MachinePackage.RESOURCE: return createResource();
            case MachinePackage.ACTION_TYPE: return createActionType();
            case MachinePackage.PERIPHERAL: return createPeripheral();
            case MachinePackage.MACHINE: return createMachine();
            case MachinePackage.IMPORT: return createImport();
            case MachinePackage.PROFILE: return createProfile();
            case MachinePackage.AXIS: return createAxis();
            case MachinePackage.POSITION: return createPosition();
            case MachinePackage.SET_POINT: return createSetPoint();
            case MachinePackage.AXIS_POSITION_MAP_ENTRY: return (EObject)createAxisPositionMapEntry();
            case MachinePackage.AXIS_POSITIONS_MAP_ENTRY: return (EObject)createAxisPositionsMapEntry();
            case MachinePackage.PATH_TARGET_REFERENCE: return createPathTargetReference();
            case MachinePackage.UNIDIRECTIONAL_PATH: return createUnidirectionalPath();
            case MachinePackage.BIDIRECTIONAL_PATH: return createBidirectionalPath();
            case MachinePackage.FULL_MESH_PATH: return createFullMeshPath();
            case MachinePackage.PATH_ANNOTATION: return createPathAnnotation();
            case MachinePackage.RESOURCE_ITEM: return createResourceItem();
            case MachinePackage.DISTANCE: return createDistance();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object createFromString(EDataType eDataType, String initialValue)
    {
        switch (eDataType.getClassifierID())
        {
            case MachinePackage.RESOURCE_TYPE:
                return createResourceTypeFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String convertToString(EDataType eDataType, Object instanceValue)
    {
        switch (eDataType.getClassifierID())
        {
            case MachinePackage.RESOURCE_TYPE:
                return convertResourceTypeToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PeripheralType createPeripheralType() {
        PeripheralTypeImpl peripheralType = new PeripheralTypeImpl();
        return peripheralType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SymbolicPosition createSymbolicPosition() {
        SymbolicPositionImpl symbolicPosition = new SymbolicPositionImpl();
        return symbolicPosition;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Resource createResource() {
        ResourceImpl resource = new ResourceImpl();
        return resource;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ActionType createActionType() {
        ActionTypeImpl actionType = new ActionTypeImpl();
        return actionType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Peripheral createPeripheral() {
        PeripheralImpl peripheral = new PeripheralImpl();
        return peripheral;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Machine createMachine() {
        MachineImpl machine = new MachineImpl();
        return machine;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Import createImport() {
        ImportImpl import_ = new ImportImpl();
        return import_;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Profile createProfile() {
        ProfileImpl profile = new ProfileImpl();
        return profile;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Axis createAxis() {
        AxisImpl axis = new AxisImpl();
        return axis;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Position createPosition() {
        PositionImpl position = new PositionImpl();
        return position;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SetPoint createSetPoint() {
        SetPointImpl setPoint = new SetPointImpl();
        return setPoint;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<Axis, Position> createAxisPositionMapEntry() {
        AxisPositionMapEntryImpl axisPositionMapEntry = new AxisPositionMapEntryImpl();
        return axisPositionMapEntry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<Axis, EList<Position>> createAxisPositionsMapEntry() {
        AxisPositionsMapEntryImpl axisPositionsMapEntry = new AxisPositionsMapEntryImpl();
        return axisPositionsMapEntry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PathTargetReference createPathTargetReference() {
        PathTargetReferenceImpl pathTargetReference = new PathTargetReferenceImpl();
        return pathTargetReference;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public UnidirectionalPath createUnidirectionalPath() {
        UnidirectionalPathImpl unidirectionalPath = new UnidirectionalPathImpl();
        return unidirectionalPath;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BidirectionalPath createBidirectionalPath() {
        BidirectionalPathImpl bidirectionalPath = new BidirectionalPathImpl();
        return bidirectionalPath;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public FullMeshPath createFullMeshPath() {
        FullMeshPathImpl fullMeshPath = new FullMeshPathImpl();
        return fullMeshPath;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PathAnnotation createPathAnnotation() {
        PathAnnotationImpl pathAnnotation = new PathAnnotationImpl();
        return pathAnnotation;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ResourceItem createResourceItem() {
        ResourceItemImpl resourceItem = new ResourceItemImpl();
        return resourceItem;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Distance createDistance() {
        DistanceImpl distance = new DistanceImpl();
        return distance;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ResourceType createResourceTypeFromString(EDataType eDataType, String initialValue)
    {
        ResourceType result = ResourceType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertResourceTypeToString(EDataType eDataType, Object instanceValue)
    {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public MachinePackage getMachinePackage() {
        return (MachinePackage)getEPackage();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
	@Deprecated
	public static MachinePackage getPackage() {
        return MachinePackage.eINSTANCE;
    }

} //MachineFactoryImpl
