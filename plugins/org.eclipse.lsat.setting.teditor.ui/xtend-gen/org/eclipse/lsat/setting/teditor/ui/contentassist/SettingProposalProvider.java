/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.setting.teditor.ui.contentassist;

import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileParameter;
import org.eclipse.lsat.setting.teditor.ui.contentassist.AbstractSettingProposalProvider;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import setting.MotionProfileSettings;

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
@SuppressWarnings("all")
public class SettingProposalProvider extends AbstractSettingProposalProvider {
  @Override
  public void completeMotionProfileSettings_MotionProfile(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completeMotionProfileSettings_MotionProfile(model, assignment, context, acceptor);
    try {
      final MotionCalculatorExtension motionCalculator = MotionCalculatorExtension.getSelectedMotionCalculator();
      final Consumer<MotionProfile> _function = (MotionProfile it) -> {
        acceptor.accept(this.createCompletionProposal(it.getKey(), it.getName(), null, context));
      };
      motionCalculator.getMotionProfiles().forEach(_function);
    } catch (final Throwable _t) {
      if (_t instanceof MotionException) {
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  @Override
  public void completeMotionArgumentsMapEntry_Key(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completeMotionArgumentsMapEntry_Key(model, assignment, context, acceptor);
    try {
      final MotionProfileSettings motionProfileSettings = ((MotionProfileSettings) model);
      final MotionCalculatorExtension motionCalculator = MotionCalculatorExtension.getSelectedMotionCalculator();
      final MotionProfile motionProfile = motionCalculator.getMotionProfile(motionProfileSettings.getMotionProfile());
      if ((motionProfile != null)) {
        final Set<String> alreadySpecified = motionProfileSettings.getMotionArguments().keySet();
        final Function1<MotionProfileParameter, Boolean> _function = (MotionProfileParameter it) -> {
          boolean _contains = alreadySpecified.contains(it.getKey());
          return Boolean.valueOf((!_contains));
        };
        final Consumer<MotionProfileParameter> _function_1 = (MotionProfileParameter it) -> {
          acceptor.accept(this.createCompletionProposal(it.getKey(), it.getName(), null, context));
        };
        IterableExtensions.<MotionProfileParameter>filter(motionProfile.getParameters(), _function).forEach(_function_1);
      }
    } catch (final Throwable _t) {
      if (_t instanceof MotionException) {
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
}
