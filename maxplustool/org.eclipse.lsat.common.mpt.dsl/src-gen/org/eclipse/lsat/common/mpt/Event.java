/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends NamedObject {
} // Event
