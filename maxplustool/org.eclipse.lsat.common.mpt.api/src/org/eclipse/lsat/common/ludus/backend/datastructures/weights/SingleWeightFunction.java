/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.datastructures.weights;

/**
 * @author Bram van der Sanden
 * @param <T> value type
 * @param <E> edge type
 */
public interface SingleWeightFunction<T, E> {
    T getWeight(E edge);

    /**
     * Get the maximum absolute value of all weights in the graph.
     *
     * @return maximum absolute value of all weights
     */
    T getMaxAbsValue();
}
