/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance.ui;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class ConformanceCheckHandler {
    @Inject
    UISynchronize sync;

    @Execute
    public void execute(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
            Shell shell, IWorkspace workspace)
    {
        if (null == selection || !(selection.getFirstElement() instanceof IFile)) {
            return;
        }
        if (!PlatformUI.getWorkbench().saveAllEditors(true)) {
            return;
        }

        ConformanceCheckOptions options = new ConformanceCheckOptions((IFile)selection.getFirstElement(), workspace);
        WizardDialog dialog = new WizardDialog(shell, new ConformanceCheckWizard(options));
        if (Dialog.OK == dialog.open()) {
            ConformanceCheckJob job = new ConformanceCheckJob(options, sync, shell);
            job.setUser(true);
            job.schedule();
        }
    }
}
