/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.util;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import machine.Axis;
import machine.Peripheral;
import machine.PeripheralType;
import machine.SymbolicPosition;

public final class AxesLocation extends HashMap<Axis, BigDecimal> {
    private static final long serialVersionUID = 6227801704225324973L;

    private final SymbolicPosition position;

    AxesLocation(SymbolicPosition position) {
        super(safeGetAxes(position).size());
        this.position = position;
    }

    public SymbolicPosition getPosition() {
        return position;
    }

    public PeripheralType getPeripheralType() {
        return position.getPeripheral().getType();
    }

    public Set<Axis> getMovingAxes(AxesLocation other) {
        if (!keySet().equals(other.keySet())) {
            throw new IllegalArgumentException(String.format("Expected axes to be the same! %s.%s%s != %s.%s%s",
                    getPosition().getPeripheral().fqn(), getPosition().getName(),
                    from(keySet()).collectOne(a -> a.getName()), other.getPosition().getPeripheral().fqn(),
                    other.getPosition().getName(), from(other.keySet()).collectOne(a -> a.getName())));
        }
        Set<Axis> movingAxes = new LinkedHashSet<Axis>(getPeripheralType().getAxes().size());
        for (Map.Entry<Axis, BigDecimal> entry: entrySet()) {
            if (0 != entry.getValue().compareTo(other.get(entry.getKey()))) {
                movingAxes.add(entry.getKey());
            }
        }
        return movingAxes;
    }

    static final List<Axis> safeGetAxes(SymbolicPosition position) {
        if (null == position)
            return Collections.emptyList();
        Peripheral peripheral = position.getPeripheral();
        if (null == peripheral)
            return Collections.emptyList();
        PeripheralType peripheralType = peripheral.getType();
        if (null == peripheralType)
            return Collections.emptyList();
        return peripheralType.getAxes();
    }
}
