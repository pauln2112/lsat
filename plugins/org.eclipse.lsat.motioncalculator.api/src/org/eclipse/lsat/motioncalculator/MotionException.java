/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator;

public class MotionException extends Exception {
    private static final long serialVersionUID = -6551082129259301057L;

    public MotionException(String message) {
        super(message);
    }

    public MotionException(String message, Throwable cause) {
        super(message, cause);
    }
}
