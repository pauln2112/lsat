/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.util;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.motioncalculator.PositionInfo;

public final class PositionInfoUtilities {
    private PositionInfoUtilities() {
        // Empty for utility classes
    }

    public static Set<String> getSetPointIds(Collection<PositionInfo> positionInfo) throws MotionValidationException {
        return positionInfo.stream().map(PositionInfo::getSetPointId)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
