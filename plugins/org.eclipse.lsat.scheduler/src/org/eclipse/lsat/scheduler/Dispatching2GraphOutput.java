/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.common.scheduler.resources.ResourceModel;

public final class Dispatching2GraphOutput<T extends Task> {
    private final ResourceModel itsResourceModel;

    private final TaskDependencyGraph<T> itsTaskDependencyGraph;

    Dispatching2GraphOutput(ResourceModel resourceModel, TaskDependencyGraph<T> taskDependencyGraph) {
        itsTaskDependencyGraph = taskDependencyGraph;
        itsResourceModel = resourceModel;
    }

    public ResourceModel getResourceModel() {
        return itsResourceModel;
    }

    public TaskDependencyGraph<T> getTaskDependencyGraph() {
        return itsTaskDependencyGraph;
    }
}
