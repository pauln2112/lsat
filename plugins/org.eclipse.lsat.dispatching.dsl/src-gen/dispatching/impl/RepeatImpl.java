/**
 */
package dispatching.impl;

import dispatching.DispatchingPackage;
import dispatching.Repeat;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Repeat</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dispatching.impl.RepeatImpl#getStart <em>Start</em>}</li>
 *   <li>{@link dispatching.impl.RepeatImpl#getCount <em>Count</em>}</li>
 *   <li>{@link dispatching.impl.RepeatImpl#getEnd <em>End</em>}</li>
 *   <li>{@link dispatching.impl.RepeatImpl#getNumRepeats <em>Num Repeats</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RepeatImpl extends MinimalEObjectImpl.Container implements Repeat {
	/**
	 * The default value of the '{@link #getStart() <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected static final int START_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getStart() <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected int start = START_EDEFAULT;

	/**
	 * The default value of the '{@link #getCount() <em>Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCount()
	 * @generated
	 * @ordered
	 */
	protected static final int COUNT_EDEFAULT = 1;

	/**
	 * The default value of the '{@link #getEnd() <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected static final int END_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEnd() <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected int end = END_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumRepeats() <em>Num Repeats</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumRepeats()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_REPEATS_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getNumRepeats() <em>Num Repeats</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumRepeats()
	 * @generated
	 * @ordered
	 */
	protected int numRepeats = NUM_REPEATS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RepeatImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DispatchingPackage.Literals.REPEAT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getStart() {
		return start;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStart(int newStart) {
		int oldStart = start;
		start = newStart;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.REPEAT__START, oldStart, start));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getCount() {
		return eIsSet(DispatchingPackage.REPEAT__END) ?  getEnd()+1-getStart() : getNumRepeats();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getEnd() {
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEnd(int newEnd) {
		int oldEnd = end;
		end = newEnd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.REPEAT__END, oldEnd, end));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumRepeats() {
		return numRepeats;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumRepeats(int newNumRepeats) {
		int oldNumRepeats = numRepeats;
		numRepeats = newNumRepeats;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DispatchingPackage.REPEAT__NUM_REPEATS, oldNumRepeats, numRepeats));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DispatchingPackage.REPEAT__START:
				return getStart();
			case DispatchingPackage.REPEAT__COUNT:
				return getCount();
			case DispatchingPackage.REPEAT__END:
				return getEnd();
			case DispatchingPackage.REPEAT__NUM_REPEATS:
				return getNumRepeats();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DispatchingPackage.REPEAT__START:
				setStart((Integer)newValue);
				return;
			case DispatchingPackage.REPEAT__END:
				setEnd((Integer)newValue);
				return;
			case DispatchingPackage.REPEAT__NUM_REPEATS:
				setNumRepeats((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DispatchingPackage.REPEAT__START:
				setStart(START_EDEFAULT);
				return;
			case DispatchingPackage.REPEAT__END:
				setEnd(END_EDEFAULT);
				return;
			case DispatchingPackage.REPEAT__NUM_REPEATS:
				setNumRepeats(NUM_REPEATS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DispatchingPackage.REPEAT__START:
				return start != START_EDEFAULT;
			case DispatchingPackage.REPEAT__COUNT:
				return getCount() != COUNT_EDEFAULT;
			case DispatchingPackage.REPEAT__END:
				return end != END_EDEFAULT;
			case DispatchingPackage.REPEAT__NUM_REPEATS:
				return numRepeats != NUM_REPEATS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (start: ");
		result.append(start);
		result.append(", end: ");
		result.append(end);
		result.append(", numRepeats: ");
		result.append(numRepeats);
		result.append(')');
		return result.toString();
	}

} //RepeatImpl
