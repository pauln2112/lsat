/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.setting.teditor.scoping;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import expressions.Declaration;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Consumer;
import machine.Axis;
import machine.IResource;
import machine.Import;
import machine.Peripheral;
import machine.PeripheralType;
import machine.Position;
import machine.Resource;
import machine.SymbolicPosition;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import setting.PhysicalSettings;
import setting.Settings;
import setting.impl.MotionSettingsMapEntryImpl;

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it s
 */
@SuppressWarnings("all")
public class SettingScopeProvider extends AbstractDeclarativeScopeProvider {
  /**
   * Scoping for declaration
   */
  public IScope scope_DeclarationRef_declaration(final EObject context, final EReference ref) {
    if ((context instanceof Declaration)) {
      final Settings settings = EcoreUtil2.<Settings>getContainerOfType(context, Settings.class);
      final Set<Declaration> importedDeclarations = this.findImportedDeclarations(settings, CollectionLiterals.<Declaration>newLinkedHashSet());
      final Function1<Declaration, Boolean> _function = (Declaration it) -> {
        return Boolean.valueOf((!Objects.equal(it, context)));
      };
      final Iterable<Declaration> priorDeclarations = IterableExtensions.<Declaration>takeWhile(settings.getDeclarations(), _function);
      Iterable<Declaration> _plus = Iterables.<Declaration>concat(priorDeclarations, importedDeclarations);
      return Scopes.scopeFor(_plus);
    }
    return null;
  }
  
  public Set<Declaration> findImportedDeclarations(final Settings settings, final Set<Declaration> declarations) {
    EList<Import> _imports = settings.getImports();
    for (final Import import_ : _imports) {
      final Consumer<Settings> _function = (Settings impSettings) -> {
        boolean _addAll = declarations.addAll(impSettings.getDeclarations());
        if (_addAll) {
          this.findImportedDeclarations(impSettings, declarations);
        }
      };
      Iterables.<Settings>filter(import_.load(), Settings.class).forEach(_function);
    }
    return declarations;
  }
  
  /**
   * Scoping for peripheral
   */
  public IScope scope_Peripheral(final PhysicalSettings entry, final EReference ref) {
    IResource _resource = null;
    if (entry!=null) {
      _resource=entry.getResource();
    }
    Resource _resource_1 = null;
    if (_resource!=null) {
      _resource_1=_resource.getResource();
    }
    boolean _tripleEquals = (null == _resource_1);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(entry.getResource().getResource().getPeripherals());
  }
  
  /**
   * Scoping for simple action of timing_settings_map_entry
   */
  public IScope scope_ActionType(final PhysicalSettings entry, final EReference ref) {
    Peripheral _peripheral = null;
    if (entry!=null) {
      _peripheral=entry.getPeripheral();
    }
    PeripheralType _type = null;
    if (_peripheral!=null) {
      _type=_peripheral.getType();
    }
    boolean _tripleEquals = (_type == null);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(entry.getPeripheral().getType().getActions());
  }
  
  /**
   * Scoping for peripheral
   */
  public IScope scope_Axis(final PhysicalSettings entry, final EReference ref) {
    Peripheral _peripheral = null;
    if (entry!=null) {
      _peripheral=entry.getPeripheral();
    }
    PeripheralType _type = null;
    if (_peripheral!=null) {
      _type=_peripheral.getType();
    }
    boolean _tripleEquals = (_type == null);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(entry.getPeripheral().getType().getAxes());
  }
  
  /**
   * Scoping for profiles
   */
  public IScope scope_Profile(final PhysicalSettings entry, final EReference ref) {
    Peripheral _peripheral = null;
    if (entry!=null) {
      _peripheral=entry.getPeripheral();
    }
    boolean _tripleEquals = (_peripheral == null);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(entry.getPeripheral().getProfiles());
  }
  
  /**
   * Scoping for positions
   */
  public IScope scope_Position(final MotionSettingsMapEntryImpl entry, final EReference ref) {
    Axis _key = null;
    if (entry!=null) {
      _key=entry.getKey();
    }
    final Axis axis = _key;
    PhysicalSettings _settings = null;
    if (entry!=null) {
      _settings=entry.getSettings();
    }
    Peripheral _peripheral = null;
    if (_settings!=null) {
      _peripheral=_settings.getPeripheral();
    }
    final Peripheral peripheral = _peripheral;
    if (((axis == null) || (peripheral == null))) {
      return IScope.NULLSCOPE;
    }
    final LinkedHashSet<Position> candidates = CollectionLiterals.<Position>newLinkedHashSet();
    final EList<Position> axisPositions = peripheral.getAxisPositions().get(axis);
    if ((axisPositions != null)) {
      Iterables.<Position>addAll(candidates, axisPositions);
    }
    final Function1<SymbolicPosition, Position> _function = (SymbolicPosition it) -> {
      return it.getPosition(axis);
    };
    Iterable<Position> _filterNull = IterableExtensions.<Position>filterNull(ListExtensions.<SymbolicPosition, Position>map(peripheral.getPositions(), _function));
    Iterables.<Position>addAll(candidates, _filterNull);
    return Scopes.scopeFor(candidates);
  }
  
  /**
   * Scoping for distances
   */
  public IScope scope_Distance(final MotionSettingsMapEntryImpl entry, final EReference ref) {
    PhysicalSettings _settings = null;
    if (entry!=null) {
      _settings=entry.getSettings();
    }
    Peripheral _peripheral = null;
    if (_settings!=null) {
      _peripheral=_settings.getPeripheral();
    }
    final Peripheral peripheral = _peripheral;
    if ((peripheral == null)) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(peripheral.getDistances());
  }
}
