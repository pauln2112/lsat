/**
 */
package machine;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Symbolic Position</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.SymbolicPosition#getAxisPosition <em>Axis Position</em>}</li>
 *   <li>{@link machine.SymbolicPosition#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link machine.SymbolicPosition#getTargetReferences <em>Target References</em>}</li>
 *   <li>{@link machine.SymbolicPosition#getSourceReferences <em>Source References</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getSymbolicPosition()
 * @model
 * @generated
 */
public interface SymbolicPosition extends Position {
	/**
     * Returns the value of the '<em><b>Axis Position</b></em>' map.
     * The key is of type {@link machine.Axis},
     * and the value is of type {@link machine.Position},
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Axis Position</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Axis Position</em>' map.
     * @see machine.MachinePackage#getSymbolicPosition_AxisPosition()
     * @model mapType="machine.AxisPositionMapEntry&lt;machine.Axis, machine.Position&gt;"
     * @generated
     */
	EMap<Axis, Position> getAxisPosition();

	/**
     * Returns the value of the '<em><b>Peripheral</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link machine.Peripheral#getPositions <em>Positions</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Peripheral</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripheral</em>' container reference.
     * @see #setPeripheral(Peripheral)
     * @see machine.MachinePackage#getSymbolicPosition_Peripheral()
     * @see machine.Peripheral#getPositions
     * @model opposite="positions" required="true" transient="false"
     * @generated
     */
	Peripheral getPeripheral();

	/**
     * Sets the value of the '{@link machine.SymbolicPosition#getPeripheral <em>Peripheral</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Peripheral</em>' container reference.
     * @see #getPeripheral()
     * @generated
     */
	void setPeripheral(Peripheral value);

	/**
     * Returns the value of the '<em><b>Target References</b></em>' reference list.
     * The list contents are of type {@link machine.PathTargetReference}.
     * It is bidirectional and its opposite is '{@link machine.PathTargetReference#getPosition <em>Position</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target References</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Target References</em>' reference list.
     * @see machine.MachinePackage#getSymbolicPosition_TargetReferences()
     * @see machine.PathTargetReference#getPosition
     * @model opposite="position" resolveProxies="false" transient="true"
     * @generated
     */
	EList<PathTargetReference> getTargetReferences();

	/**
     * Returns the value of the '<em><b>Source References</b></em>' reference list.
     * The list contents are of type {@link machine.UnidirectionalPath}.
     * It is bidirectional and its opposite is '{@link machine.UnidirectionalPath#getSource <em>Source</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source References</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Source References</em>' reference list.
     * @see machine.MachinePackage#getSymbolicPosition_SourceReferences()
     * @see machine.UnidirectionalPath#getSource
     * @model opposite="source" resolveProxies="false" transient="true"
     * @generated
     */
	EList<UnidirectionalPath> getSourceReferences();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true"
     * @generated
     */
	Position getPosition(Axis axis);

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation"
     * @generated
     */
	EList<Path> getOutgoingPaths();

} // SymbolicPosition
