/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;

public class NullSkippingIterable<E> extends LoggableIterable<E> {
    private final Iterable<? extends E> source;

    public NullSkippingIterable(Iterable<? extends E> source) {
        this.source = source;
    }

    public Iterator<E> iterator() {
        return new NullSkippingIterator<E>(source.iterator());
    }
}
