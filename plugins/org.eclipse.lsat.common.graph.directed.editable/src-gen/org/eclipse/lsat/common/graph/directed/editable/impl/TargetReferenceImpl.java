/**
 */
package org.eclipse.lsat.common.graph.directed.editable.impl;

import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.graph.directed.editable.TargetReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Target Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.TargetReferenceImpl#getEdge <em>Edge</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.TargetReferenceImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.TargetReferenceImpl#getNode <em>Node</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TargetReferenceImpl extends MinimalEObjectImpl.Container implements TargetReference {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNode() <em>Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNode()
	 * @generated
	 * @ordered
	 */
	protected Node node;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TargetReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EdgPackage.Literals.TARGET_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Edge getEdge() {
		if (eContainerFeatureID() != EdgPackage.TARGET_REFERENCE__EDGE) return null;
		return (Edge)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEdge(Edge newEdge, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newEdge, EdgPackage.TARGET_REFERENCE__EDGE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEdge(Edge newEdge) {
		if (newEdge != eInternalContainer() || (eContainerFeatureID() != EdgPackage.TARGET_REFERENCE__EDGE && newEdge != null)) {
			if (EcoreUtil.isAncestor(this, newEdge))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newEdge != null)
				msgs = ((InternalEObject)newEdge).eInverseAdd(this, EdgPackage.EDGE__TARGET, Edge.class, msgs);
			msgs = basicSetEdge(newEdge, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EdgPackage.TARGET_REFERENCE__EDGE, newEdge, newEdge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		// We need this trick as we can only put the genmodel annotation on the DependencyTarget interface.
		// This causes this code to be generated in all implementers of this interface and therefore the code should compile 
		if (EdgeImpl.class.isInstance(this)) {
			// Dependency.name = sourceTask.name -> targetTask.name
			return (null == EdgeImpl.class.cast(this).source ? null : EdgeImpl.class.cast(this).source.getName()) + 
					" -> " + 
					(null == EdgeImpl.class.cast(this).target ? null : EdgeImpl.class.cast(this).target.getName());
		} else if (TargetReferenceImpl.class.isInstance(this)) {
			// TaskRef.name = task.name 
			return null == TargetReferenceImpl.class.cast(this).node ? null : TargetReferenceImpl.class.cast(this).node.getName();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Node getNode() {
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNode(Node newNode, NotificationChain msgs) {
		Node oldNode = node;
		node = newNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EdgPackage.TARGET_REFERENCE__NODE, oldNode, newNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNode(Node newNode) {
		if (newNode != node) {
			NotificationChain msgs = null;
			if (node != null)
				msgs = ((InternalEObject)node).eInverseRemove(this, EdgPackage.NODE__TARGET_REFERENCES, Node.class, msgs);
			if (newNode != null)
				msgs = ((InternalEObject)newNode).eInverseAdd(this, EdgPackage.NODE__TARGET_REFERENCES, Node.class, msgs);
			msgs = basicSetNode(newNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EdgPackage.TARGET_REFERENCE__NODE, newNode, newNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EdgPackage.TARGET_REFERENCE__EDGE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetEdge((Edge)otherEnd, msgs);
			case EdgPackage.TARGET_REFERENCE__NODE:
				if (node != null)
					msgs = ((InternalEObject)node).eInverseRemove(this, EdgPackage.NODE__TARGET_REFERENCES, Node.class, msgs);
				return basicSetNode((Node)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EdgPackage.TARGET_REFERENCE__EDGE:
				return basicSetEdge(null, msgs);
			case EdgPackage.TARGET_REFERENCE__NODE:
				return basicSetNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case EdgPackage.TARGET_REFERENCE__EDGE:
				return eInternalContainer().eInverseRemove(this, EdgPackage.EDGE__TARGET, Edge.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EdgPackage.TARGET_REFERENCE__EDGE:
				return getEdge();
			case EdgPackage.TARGET_REFERENCE__NAME:
				return getName();
			case EdgPackage.TARGET_REFERENCE__NODE:
				return getNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EdgPackage.TARGET_REFERENCE__EDGE:
				setEdge((Edge)newValue);
				return;
			case EdgPackage.TARGET_REFERENCE__NODE:
				setNode((Node)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EdgPackage.TARGET_REFERENCE__EDGE:
				setEdge((Edge)null);
				return;
			case EdgPackage.TARGET_REFERENCE__NODE:
				setNode((Node)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EdgPackage.TARGET_REFERENCE__EDGE:
				return getEdge() != null;
			case EdgPackage.TARGET_REFERENCE__NAME:
				return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
			case EdgPackage.TARGET_REFERENCE__NODE:
				return node != null;
		}
		return super.eIsSet(featureID);
	}

} //TargetReferenceImpl
