/**
 */
package setting.impl;

import machine.Axis;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import setting.MotionSettings;
import setting.PhysicalSettings;
import setting.SettingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Motion Settings Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link setting.impl.MotionSettingsMapEntryImpl#getName <em>Name</em>}</li>
 *   <li>{@link setting.impl.MotionSettingsMapEntryImpl#getTypedKey <em>Key</em>}</li>
 *   <li>{@link setting.impl.MotionSettingsMapEntryImpl#getTypedValue <em>Value</em>}</li>
 *   <li>{@link setting.impl.MotionSettingsMapEntryImpl#getSettings <em>Settings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MotionSettingsMapEntryImpl extends MinimalEObjectImpl.Container implements BasicEMap.Entry<Axis,MotionSettings> {
	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getTypedKey() <em>Key</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTypedKey()
     * @generated
     * @ordered
     */
	protected Axis key;

	/**
     * The cached value of the '{@link #getTypedValue() <em>Value</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTypedValue()
     * @generated
     * @ordered
     */
	protected MotionSettings value;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected MotionSettingsMapEntryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return SettingPackage.Literals.MOTION_SETTINGS_MAP_ENTRY;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String getName() {
        return null == key ? null : key.getName();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Axis getTypedKey() {
        if (key != null && key.eIsProxy())
        {
            InternalEObject oldKey = (InternalEObject)key;
            key = (Axis)eResolveProxy(oldKey);
            if (key != oldKey)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, SettingPackage.MOTION_SETTINGS_MAP_ENTRY__KEY, oldKey, key));
            }
        }
        return key;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Axis basicGetTypedKey() {
        return key;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setTypedKey(Axis newKey) {
        Axis oldKey = key;
        key = newKey;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.MOTION_SETTINGS_MAP_ENTRY__KEY, oldKey, key));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MotionSettings getTypedValue() {
        return value;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetTypedValue(MotionSettings newValue, NotificationChain msgs) {
        MotionSettings oldValue = value;
        value = newValue;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE, oldValue, newValue);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setTypedValue(MotionSettings newValue) {
        if (newValue != value)
        {
            NotificationChain msgs = null;
            if (value != null)
                msgs = ((InternalEObject)value).eInverseRemove(this, SettingPackage.MOTION_SETTINGS__ENTRY, MotionSettings.class, msgs);
            if (newValue != null)
                msgs = ((InternalEObject)newValue).eInverseAdd(this, SettingPackage.MOTION_SETTINGS__ENTRY, MotionSettings.class, msgs);
            msgs = basicSetTypedValue(newValue, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE, newValue, newValue));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public PhysicalSettings getSettings() {
        if (eContainerFeatureID() != SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS) return null;
        return (PhysicalSettings)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetSettings(PhysicalSettings newSettings, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newSettings, SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setSettings(PhysicalSettings newSettings) {
        if (newSettings != eInternalContainer() || (eContainerFeatureID() != SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS && newSettings != null))
        {
            if (EcoreUtil.isAncestor(this, newSettings))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newSettings != null)
                msgs = ((InternalEObject)newSettings).eInverseAdd(this, SettingPackage.PHYSICAL_SETTINGS__MOTION_SETTINGS, PhysicalSettings.class, msgs);
            msgs = basicSetSettings(newSettings, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS, newSettings, newSettings));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE:
                if (value != null)
                    msgs = ((InternalEObject)value).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE, null, msgs);
                return basicSetTypedValue((MotionSettings)otherEnd, msgs);
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetSettings((PhysicalSettings)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE:
                return basicSetTypedValue(null, msgs);
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS:
                return basicSetSettings(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID())
        {
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS:
                return eInternalContainer().eInverseRemove(this, SettingPackage.PHYSICAL_SETTINGS__MOTION_SETTINGS, PhysicalSettings.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__NAME:
                return getName();
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__KEY:
                if (resolve) return getTypedKey();
                return basicGetTypedKey();
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE:
                return getTypedValue();
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS:
                return getSettings();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__KEY:
                setTypedKey((Axis)newValue);
                return;
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE:
                setTypedValue((MotionSettings)newValue);
                return;
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS:
                setSettings((PhysicalSettings)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__KEY:
                setTypedKey((Axis)null);
                return;
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE:
                setTypedValue((MotionSettings)null);
                return;
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS:
                setSettings((PhysicalSettings)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__NAME:
                return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__KEY:
                return key != null;
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE:
                return value != null;
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS:
                return getSettings() != null;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected int hash = -1;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getHash() {
        if (hash == -1)
        {
            Object theKey = getKey();
            hash = (theKey == null ? 0 : theKey.hashCode());
        }
        return hash;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setHash(int hash) {
        this.hash = hash;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Axis getKey() {
        return getTypedKey();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setKey(Axis key) {
        setTypedKey(key);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public MotionSettings getValue() {
        return getTypedValue();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public MotionSettings setValue(MotionSettings value) {
        MotionSettings oldValue = getValue();
        setTypedValue(value);
        return oldValue;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	public EMap<Axis, MotionSettings> getEMap() {
        EObject container = eContainer();
        return container == null ? null : (EMap<Axis, MotionSettings>)container.eGet(eContainmentFeature());
    }

} //MotionSettingsMapEntryImpl
