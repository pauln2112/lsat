/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api.adapter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.lsat.common.ludus.backend.algebra.DenseMatrix;
import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.mpt.Matrix;
import org.eclipse.lsat.common.mpt.api.MaxPlusAlgorithms;

/**
 * Convert a matrix to a Ludus dense matrix.
 *
 * @author Bram van der Sanden
 * @since 2017-04-28
 */
public class MatrixAdapter {
    private MatrixAdapter() {
        // Empty
    }

    /**
     * Return the max-plus matrix, and the resource names corresponding to the rows/columns.
     *
     * @param matrix
     * @return
     */
    public static Tuple<DenseMatrix, List<String>> getMatrix(Matrix matrix) {
        return getMatrix(matrix, new HashSet<>());
    }

    /**
     * Return the max-plus matrix, and the resource names corresponding to the rows/columns.
     *
     * @param matrix
     * @return
     */
    public static Tuple<DenseMatrix, List<String>> getMatrix(Matrix matrix, Collection<Integer> resourcesToRemove) {
        int size = matrix.getRows().size() - resourcesToRemove.size();
        DenseMatrix dm = new DenseMatrix(size, size);

        List<Integer> idMapping = new ArrayList<>(size);
        for (int resId = 0; resId < matrix.getRows().size(); resId++) {
            if (!resourcesToRemove.contains(resId)) {
                idMapping.add(resId);
            }
        }

        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                // Copy the values from the original matrix, using the mapping.
                Double value = matrix.getRows().get(idMapping.get(row)).getValues().get(idMapping.get(col));
                Double scaledValue = value * MaxPlusAlgorithms.SCALING_FACTOR;
                Double newValue = round(scaledValue, 0);
                dm.put(row, col, new Value(newValue));
            }
        }

        // Resources corresponding to the rows in the new matrix.
        List<String> resourceNames = idMapping.stream().map(resId -> matrix.getRows().get(resId).getName())
                .collect(Collectors.toList());

        return Tuple.of(dm, resourceNames);
    }

    private static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }
        if (value < 0) {
            return value;
        }
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
