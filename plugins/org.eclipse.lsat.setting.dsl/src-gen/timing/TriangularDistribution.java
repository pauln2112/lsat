/**
 */
package timing;

import expressions.Expression;
import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triangular Distribution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * @see https://en.wikipedia.org/wiki/Triangular_distribution
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link timing.TriangularDistribution#getMode <em>Mode</em>}</li>
 *   <li>{@link timing.TriangularDistribution#getMin <em>Min</em>}</li>
 *   <li>{@link timing.TriangularDistribution#getMax <em>Max</em>}</li>
 *   <li>{@link timing.TriangularDistribution#getModeExp <em>Mode Exp</em>}</li>
 *   <li>{@link timing.TriangularDistribution#getMinExp <em>Min Exp</em>}</li>
 *   <li>{@link timing.TriangularDistribution#getMaxExp <em>Max Exp</em>}</li>
 * </ul>
 *
 * @see timing.TimingPackage#getTriangularDistribution()
 * @model
 * @generated
 */
public interface TriangularDistribution extends Distribution {
	/**
     * Returns the value of the '<em><b>Mode</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Mode</em>' attribute.
     * @see timing.TimingPackage#getTriangularDistribution_Mode()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getMode();

	/**
     * Returns the value of the '<em><b>Min</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Min</em>' attribute.
     * @see timing.TimingPackage#getTriangularDistribution_Min()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getMin();

	/**
     * Returns the value of the '<em><b>Max</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Max</em>' attribute.
     * @see timing.TimingPackage#getTriangularDistribution_Max()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getMax();

	/**
     * Returns the value of the '<em><b>Mode Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Mode Exp</em>' containment reference.
     * @see #setModeExp(Expression)
     * @see timing.TimingPackage#getTriangularDistribution_ModeExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getModeExp();

	/**
     * Sets the value of the '{@link timing.TriangularDistribution#getModeExp <em>Mode Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Mode Exp</em>' containment reference.
     * @see #getModeExp()
     * @generated
     */
	void setModeExp(Expression value);

	/**
     * Returns the value of the '<em><b>Min Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Min Exp</em>' containment reference.
     * @see #setMinExp(Expression)
     * @see timing.TimingPackage#getTriangularDistribution_MinExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getMinExp();

	/**
     * Sets the value of the '{@link timing.TriangularDistribution#getMinExp <em>Min Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Min Exp</em>' containment reference.
     * @see #getMinExp()
     * @generated
     */
	void setMinExp(Expression value);

	/**
     * Returns the value of the '<em><b>Max Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Max Exp</em>' containment reference.
     * @see #setMaxExp(Expression)
     * @see timing.TimingPackage#getTriangularDistribution_MaxExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getMaxExp();

	/**
     * Sets the value of the '{@link timing.TriangularDistribution#getMaxExp <em>Max Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Max Exp</em>' containment reference.
     * @see #getMaxExp()
     * @generated
     */
	void setMaxExp(Expression value);

} // TriangularDistribution
