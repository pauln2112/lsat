/**
 */
package lsat_graph.util;

import activity.EventAction;
import activity.ResourceAction;
import lsat_graph.*;
import org.eclipse.lsat.common.graph.directed.Aspect;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;

import org.eclipse.lsat.common.scheduler.resources.AbstractResource;
import org.eclipse.lsat.common.scheduler.resources.NamedResource;
import org.eclipse.lsat.common.scheduler.resources.Resource;

import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see lsat_graph.lsat_graphPackage
 * @generated
 */
public class lsat_graphSwitch<T1> extends Switch<T1> {
	/**
     * The cached model package
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static lsat_graphPackage modelPackage;

	/**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public lsat_graphSwitch() {
        if (modelPackage == null)
        {
            modelPackage = lsat_graphPackage.eINSTANCE;
        }
    }

	/**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

	/**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
	@Override
	protected T1 doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID)
        {
            case lsat_graphPackage.PERIPHERAL_ACTION_TASK:
            {
                PeripheralActionTask peripheralActionTask = (PeripheralActionTask)theEObject;
                T1 result = casePeripheralActionTask(peripheralActionTask);
                if (result == null) result = caseActionTask(peripheralActionTask);
                if (result == null) result = caseTask(peripheralActionTask);
                if (result == null) result = caseNode(peripheralActionTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.CLAIM_TASK:
            {
                ClaimTask claimTask = (ClaimTask)theEObject;
                T1 result = caseClaimTask(claimTask);
                if (result == null) result = caseActionTask(claimTask);
                if (result == null) result = caseTask(claimTask);
                if (result == null) result = caseNode(claimTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.RELEASE_TASK:
            {
                ReleaseTask releaseTask = (ReleaseTask)theEObject;
                T1 result = caseReleaseTask(releaseTask);
                if (result == null) result = caseActionTask(releaseTask);
                if (result == null) result = caseTask(releaseTask);
                if (result == null) result = caseNode(releaseTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.CLAIM_RELEASE_RESOURCE:
            {
                ClaimReleaseResource claimReleaseResource = (ClaimReleaseResource)theEObject;
                T1 result = caseClaimReleaseResource(claimReleaseResource);
                if (result == null) result = caseResource(claimReleaseResource);
                if (result == null) result = caseAbstractResource(claimReleaseResource);
                if (result == null) result = caseNamedResource(claimReleaseResource);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.DISPATCH_GROUP_TASK:
            {
                DispatchGroupTask dispatchGroupTask = (DispatchGroupTask)theEObject;
                T1 result = caseDispatchGroupTask(dispatchGroupTask);
                if (result == null) result = caseTask(dispatchGroupTask);
                if (result == null) result = caseNode(dispatchGroupTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.DISPATCH_GROUP_RESOURCE:
            {
                DispatchGroupResource dispatchGroupResource = (DispatchGroupResource)theEObject;
                T1 result = caseDispatchGroupResource(dispatchGroupResource);
                if (result == null) result = caseResource(dispatchGroupResource);
                if (result == null) result = caseAbstractResource(dispatchGroupResource);
                if (result == null) result = caseNamedResource(dispatchGroupResource);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.PERIPHERAL_RESOURCE:
            {
                PeripheralResource peripheralResource = (PeripheralResource)theEObject;
                T1 result = casePeripheralResource(peripheralResource);
                if (result == null) result = caseResource(peripheralResource);
                if (result == null) result = caseAbstractResource(peripheralResource);
                if (result == null) result = caseNamedResource(peripheralResource);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.DISPATCH_GRAPH:
            {
                DispatchGraph dispatchGraph = (DispatchGraph)theEObject;
                T1 result = caseDispatchGraph(dispatchGraph);
                if (result == null) result = caseTaskDependencyGraph(dispatchGraph);
                if (result == null) result = caseDirectedGraph(dispatchGraph);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.CLAIMED_BY_SCHEDULED_TASK:
            {
                ClaimedByScheduledTask claimedByScheduledTask = (ClaimedByScheduledTask)theEObject;
                T1 result = caseClaimedByScheduledTask(claimedByScheduledTask);
                if (result == null) result = caseScheduledTask(claimedByScheduledTask);
                if (result == null) result = caseNode(claimedByScheduledTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.ACTION_TASK:
            {
                ActionTask<?> actionTask = (ActionTask<?>)theEObject;
                T1 result = caseActionTask(actionTask);
                if (result == null) result = caseTask(actionTask);
                if (result == null) result = caseNode(actionTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.STOCHASTIC_ANNOTATION:
            {
                StochasticAnnotation stochasticAnnotation = (StochasticAnnotation)theEObject;
                T1 result = caseStochasticAnnotation(stochasticAnnotation);
                if (result == null) result = caseAspect(stochasticAnnotation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.EVENT_STATUS_TASK:
            {
                EventStatusTask eventStatusTask = (EventStatusTask)theEObject;
                T1 result = caseEventStatusTask(eventStatusTask);
                if (result == null) result = caseScheduledTask(eventStatusTask);
                if (result == null) result = caseNode(eventStatusTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.EVENT_ANNOTATION:
            {
                EventAnnotation<?> eventAnnotation = (EventAnnotation<?>)theEObject;
                T1 result = caseEventAnnotation(eventAnnotation);
                if (result == null) result = caseAspect(eventAnnotation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.EVENT_TASK:
            {
                EventTask<?> eventTask = (EventTask<?>)theEObject;
                T1 result = caseEventTask(eventTask);
                if (result == null) result = caseTask(eventTask);
                if (result == null) result = caseNode(eventTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.REQUIRE_TASK:
            {
                RequireTask requireTask = (RequireTask)theEObject;
                T1 result = caseRequireTask(requireTask);
                if (result == null) result = caseEventTask(requireTask);
                if (result == null) result = caseTask(requireTask);
                if (result == null) result = caseNode(requireTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case lsat_graphPackage.RAISE_TASK:
            {
                RaiseTask raiseTask = (RaiseTask)theEObject;
                T1 result = caseRaiseTask(raiseTask);
                if (result == null) result = caseEventTask(raiseTask);
                if (result == null) result = caseTask(raiseTask);
                if (result == null) result = caseNode(raiseTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Peripheral Action Task</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Peripheral Action Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 casePeripheralActionTask(PeripheralActionTask object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Claim Task</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Claim Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseClaimTask(ClaimTask object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Release Task</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Release Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseReleaseTask(ReleaseTask object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Claim Release Resource</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Claim Release Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseClaimReleaseResource(ClaimReleaseResource object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Dispatch Group Task</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Dispatch Group Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseDispatchGroupTask(DispatchGroupTask object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Dispatch Group Resource</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Dispatch Group Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseDispatchGroupResource(DispatchGroupResource object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Peripheral Resource</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Peripheral Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 casePeripheralResource(PeripheralResource object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Dispatch Graph</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Dispatch Graph</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseDispatchGraph(DispatchGraph object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Claimed By Scheduled Task</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Claimed By Scheduled Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseClaimedByScheduledTask(ClaimedByScheduledTask object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Action Task</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Action Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public <T extends ResourceAction> T1 caseActionTask(ActionTask<T> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Stochastic Annotation</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Stochastic Annotation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseStochasticAnnotation(StochasticAnnotation object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Event Status Task</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Event Status Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseEventStatusTask(EventStatusTask object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Event Annotation</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Event Annotation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public <T extends Task> T1 caseEventAnnotation(EventAnnotation<T> object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Event Task</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Event Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public <T extends EventAction> T1 caseEventTask(EventTask<T> object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Require Task</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Require Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseRequireTask(RequireTask object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Raise Task</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Raise Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T1 caseRaiseTask(RaiseTask object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Node</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseNode(Node object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Task</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseTask(Task object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Named Resource</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Named Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseNamedResource(NamedResource object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Abstract Resource</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseAbstractResource(AbstractResource object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Resource</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T1 caseResource(Resource object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Directed Graph</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Directed Graph</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public <N extends Node, E extends Edge> T1 caseDirectedGraph(DirectedGraph<N, E> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Task Dependency Graph</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Task Dependency Graph</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public <T extends Task> T1 caseTaskDependencyGraph(TaskDependencyGraph<T> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Scheduled Task</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Scheduled Task</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public <T extends Task> T1 caseScheduledTask(ScheduledTask<T> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Aspect</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Aspect</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public <N extends Node, E extends Edge> T1 caseAspect(Aspect<N, E> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
	@Override
	public T1 defaultCase(EObject object) {
        return null;
    }

} //lsat_graphSwitch
