/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import activity.Activity;
import activity.ActivitySet;
import activity.util.ActivityUtil;
import dispatching.ActivityDispatching;
import dispatching.Dispatch;
import dispatching.DispatchGroup;
import java.util.LinkedHashSet;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class ExpandActivities {
  private ExpandActivities() {
  }
  
  /**
   * If {@link Dispatch} instances contain {@link ResourceItem}s then {@link Activity} are expanded.
   * 
   * @return A copy with expanded activities or the original if there is nothing to do.
   */
  public static ActivityDispatching transformModel(final ActivityDispatching dispatching) {
    final ActivityDispatching out = EcoreUtil.<ActivityDispatching>copy(dispatching);
    final LinkedHashSet<Activity> remove = CollectionLiterals.<Activity>newLinkedHashSet();
    final Function1<DispatchGroup, EList<Dispatch>> _function = (DispatchGroup it) -> {
      return it.getDispatches();
    };
    final Function1<Dispatch, Boolean> _function_1 = (Dispatch it) -> {
      boolean _isEmpty = it.getResourceItems().isEmpty();
      return Boolean.valueOf((!_isEmpty));
    };
    final Consumer<Dispatch> _function_2 = (Dispatch it) -> {
      Activity _activity = it.getActivity();
      remove.add(_activity);
      it.setActivity(ActivityUtil.queryCreateExpandedActivity(it.getActivity(), it.getResourceItems()));
      it.getResourceItems().clear();
    };
    IterableExtensions.<Dispatch>filter(IterableExtensions.<DispatchGroup, Dispatch>flatMap(out.getDispatchGroups(), _function), _function_1).forEach(_function_2);
    boolean _isEmpty = remove.isEmpty();
    if (_isEmpty) {
      return dispatching;
    }
    final Consumer<Activity> _function_3 = (Activity it) -> {
      EObject _eContainer = it.eContainer();
      EList<Activity> _activities = ((ActivitySet) _eContainer).getActivities();
      _activities.remove(it);
    };
    remove.forEach(_function_3);
    return out;
  }
}
