/**
 */
package activity;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Release</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activity.ActivityPackage#getRelease()
 * @model
 * @generated
 */
public interface Release extends ResourceAction {
} // Release
