/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.design;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.sirius.business.api.action.AbstractExternalJavaAction;

import machine.Position;
import machine.impl.AxisPositionsMapEntryImpl;

public class DeleteAxis extends AbstractExternalJavaAction {
    @Override
    public boolean canExecute(Collection<? extends EObject> selections) {
        return null != selections && selections.size() == 1;
    }

    @Override
    public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
        // Deleting AxisPositions in the Axis Positions Editor
        for (EObject selection: selections) {
            if (selection instanceof AxisPositionsMapEntryImpl) {
                delete((AxisPositionsMapEntryImpl)selection);
            }
        }
    }

    private static void delete(AxisPositionsMapEntryImpl axisPos) {
        deletePosMaps(axisPos);
        List<Position> toRemove = new ArrayList<>();

        // Deleting all positions contained in a axisPosition
        for (Position pos: axisPos.getValue()) {
            if (axisPos.getValue().contains(pos)) {
                toRemove.add(pos);
            }
        }

        for (Position pos: toRemove) {
            EcoreUtil.remove(pos);
        }

        // Deleting the axisPosition
        EcoreUtil.remove(axisPos);
    }

    private static void deletePosMaps(AxisPositionsMapEntryImpl axisPos) {
        for (Position pos: axisPos.getValue()) {
            Collection<EStructuralFeature.Setting> posMaps = EcoreUtil.UsageCrossReferencer.find(pos,
                    pos.eResource().getResourceSet());
            for (Iterator<EStructuralFeature.Setting> iter = posMaps.iterator(); iter.hasNext();) {
                EStructuralFeature.Setting posMap = iter.next();
                // Delete all Maps where the position "pos" is either a value or
                // a key
                if ("value".equals(posMap.getEStructuralFeature().getName())
                        || "key".equals(posMap.getEStructuralFeature().getName()))
                {
                    EcoreUtil.remove(posMap.getEObject());
                }
            }
        }
    }
}
