/**
 */
package org.eclipse.lsat.common.scheduler.graph;

import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.common.scheduler.resources.Resource;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.graph.Task#getExecutionTime <em>Execution Time</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.graph.Task#getResources <em>Resources</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.scheduler.graph.GraphPackage#getTask()
 * @model
 * @generated
 */
public interface Task extends Node {
	/**
     * Returns the value of the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Execution Time</em>' attribute.
     * @see #setExecutionTime(BigDecimal)
     * @see org.eclipse.lsat.common.scheduler.graph.GraphPackage#getTask_ExecutionTime()
     * @model
     * @generated
     */
	BigDecimal getExecutionTime();

	/**
     * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.graph.Task#getExecutionTime <em>Execution Time</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Execution Time</em>' attribute.
     * @see #getExecutionTime()
     * @generated
     */
	void setExecutionTime(BigDecimal value);

	/**
     * Returns the value of the '<em><b>Resources</b></em>' reference list.
     * The list contents are of type {@link org.eclipse.lsat.common.scheduler.resources.Resource}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Resources</em>' reference list.
     * @see org.eclipse.lsat.common.scheduler.graph.GraphPackage#getTask_Resources()
     * @model required="true"
     * @generated
     */
	EList<Resource> getResources();

} // Task
