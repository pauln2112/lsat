/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.simulator.common;

import java.util.Collection;
import java.util.Collections;
import machine.Distance;
import machine.HasResourcePeripheral;
import machine.IResource;
import machine.Peripheral;
import machine.Position;
import machine.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class Common {
  public static final String INITIAL_POSITION = "_INITIAL_POSITION";
  
  public static final String CURRENT_POSITION = "_CURRENT_POSITION";
  
  public static String getID(final HasResourcePeripheral rp) {
    return Common.getID(rp.getResource(), rp.getPeripheral());
  }
  
  public static String getID(final IResource resource, final Peripheral peripheral) {
    StringConcatenation _builder = new StringConcatenation();
    String _replaceAll = resource.fqn().replaceAll("\\W+", "_");
    _builder.append(_replaceAll);
    _builder.append("_");
    String _name = peripheral.getName();
    _builder.append(_name);
    return _builder.toString();
  }
  
  public static String getPositionID(final HasResourcePeripheral rp, final Position position) {
    return Common.getPositionID(rp.getResource(), rp.getPeripheral(), position);
  }
  
  public static String getPositionID(final IResource resource, final Peripheral peripheral, final Position position) {
    StringConcatenation _builder = new StringConcatenation();
    String _iD = Common.getID(resource, peripheral);
    _builder.append(_iD);
    _builder.append("_");
    String _name = position.getName();
    _builder.append(_name);
    return _builder.toString();
  }
  
  public static String getPositionID(final HasResourcePeripheral rp, final String position) {
    return Common.getPositionID(rp.getResource(), rp.getPeripheral(), position);
  }
  
  public static String getPositionID(final IResource resource, final Peripheral peripheral, final String position) {
    StringConcatenation _builder = new StringConcatenation();
    String _iD = Common.getID(resource, peripheral);
    _builder.append(_iD);
    _builder.append("_");
    String _replaceAll = position.replaceAll("\\W+", "_");
    _builder.append(_replaceAll);
    return _builder.toString();
  }
  
  public static String getDistanceID(final HasResourcePeripheral rp, final Distance distance) {
    return Common.getDistanceID(rp.getResource(), rp.getPeripheral(), distance);
  }
  
  public static String getDistanceID(final IResource resource, final Peripheral peripheral, final Distance distance) {
    StringConcatenation _builder = new StringConcatenation();
    String _iD = Common.getID(resource, peripheral);
    _builder.append(_iD);
    _builder.append("_");
    String _name = distance.getName();
    _builder.append(_name);
    return _builder.toString();
  }
  
  public static Collection<? extends IResource> getItemsOrResource(final IResource resource) {
    Collection<? extends IResource> _switchResult = null;
    boolean _matched = false;
    if (resource instanceof Resource) {
      boolean _isEmpty = ((Resource)resource).getItems().isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        _matched=true;
        _switchResult = ((Resource)resource).getItems();
      }
    }
    if (!_matched) {
      _switchResult = Collections.<IResource>singleton(resource);
    }
    return _switchResult;
  }
}
