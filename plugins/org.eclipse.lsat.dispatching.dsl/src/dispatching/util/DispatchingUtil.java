/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */

package dispatching.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import activity.Activity;
import activity.ActivitySet;
import activity.ResourceAction;
import activity.util.ActivityUtil;
import dispatching.ActivityDispatching;
import dispatching.Attribute;
import dispatching.Dispatch;
import dispatching.DispatchGroup;
import dispatching.DispatchingFactory;
import dispatching.DispatchingPackage;
import machine.IResource;
import machine.ResourceItem;

public class DispatchingUtil {
    private DispatchingUtil() {
        // Empty
    }

    /**
     * If {@link Dispatch} instances contain {@link ResourceItem}s or {@link DispatchGroup} instances contain repeats>1
     * then dispatching is expanded.
     *
     * @return A copy with expanded activities or the original if there is nothing to do.
     */

    public static void expand(ActivityDispatching dispatching) {
        unfold(dispatching);
        expandActivities(dispatching);
    }

    /**
     * If {@link Dispatch} instances contain {@link ResourceItem}s then {@link Activity} are expanded.
     *
     * @return A copy with expanded activities or the original if there is nothing to do.
     */
    public static void expandActivities(ActivityDispatching dispatching) {
        Set<Activity> remove = new LinkedHashSet<>();

        dispatching.getDispatchGroups().stream().flatMap(dg -> dg.getDispatches().stream())
                .filter(d -> !d.getResourceItems().isEmpty()).forEach(d -> {
                    remove.add(d.getActivity());
                    d.setActivity(ActivityUtil.queryCreateExpandedActivity(d.getActivity(), d.getResourceItems()));
                    d.getResourceItems().clear();
                });
    }

    /**
     * If {@link DispatchGroup} instances contain repeats then {@link DispatchGroups} are unfolded.
     *
     * @return A copy with expanded activities or the original if there is nothing to do.
     */
    public static void unfold(ActivityDispatching dispatching) {
        convertYield(dispatching);
        for (DispatchGroup dg: new ArrayList<>(dispatching.getDispatchGroups())) {
            DispatchGroup newGroup = EcoreUtil.copy(dg);
            dispatching.getDispatchGroups().add(dispatching.getDispatchGroups().indexOf(dg), newGroup);
            int numberRepeats = getNumberRepeats(dg);
            newGroup.setYield(getYield(dg) * numberRepeats);
            newGroup.getResourceYield().forEach(d -> { d.setValue(numberRepeats * getYield(newGroup, d.getKey())); });
            newGroup.getRepeats().clear();
            newGroup.eUnset(DispatchingPackage.Literals.DISPATCH_GROUP__ITERATOR_NAME);
            newGroup.getDispatches().clear();
            for (Integer iteration: getRepeatSeries(dg)) {
                for (Dispatch d: dg.getDispatches()) {
                    Dispatch newDispatch = EcoreUtil.copy(d);
                    if (newDispatch.getDescription() != null) {
                        userAttr(newDispatch, "description", d.getDescription());
                        newDispatch.setDescription(null);
                    }
                    userAttr(newDispatch, "phase", dg.getName());
                    userAttr(newDispatch, dg.getIteratorName(), Integer.toString(iteration));
                    newGroup.getDispatches().add(newDispatch);
                }
            }
            dispatching.getDispatchGroups().remove(dg);
        }
    }

    /**
     * Moves throughput to the dispatch groups
     *
     * @return
     */
    public static ActivityDispatching convertYield(ActivityDispatching dispatching) {
        if (dispatching.eIsSet(DispatchingPackage.Literals.ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS)
                || dispatching.getResourceIterations().size() > 0)
        {
            for (DispatchGroup dg: new ArrayList<>(dispatching.getDispatchGroups())) {
                dg.setYield(getYield(dg));
                Collection<IResource> resources = getResources(dg);
                dg.getResourceYield().forEach(d -> { resources.remove(d.getKey()); });
                dispatching.getResourceIterations().stream().filter(r -> resources.contains(r.getKey()))
                        .forEach(r -> dg.getResourceYield().put(r.getKey(), r.getValue()));
            }
            dispatching.eUnset(DispatchingPackage.Literals.ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS);
            dispatching.getResourceIterations().clear();
        }
        return dispatching;
    }

    /**
     * Removes all activities from {@link ActivitySet} that are not used in {@link ActivityDispatching}
     */
    public static void removeUnusedActivities(ActivityDispatching dispatching) {
        Collection<Activity> used = dispatching.getDispatchGroups().stream().flatMap(dg -> dg.getDispatches().stream())
                .map(d -> d.getActivity()).collect(Collectors.toCollection(LinkedHashSet::new));
        Collection<ActivitySet> activitySets = used.stream().map(EObject::eContainer)
                .filter(ActivitySet.class::isInstance).map(ActivitySet.class::cast)
                .collect(Collectors.toCollection(LinkedHashSet::new));
        Collection<Activity> remove = activitySets.stream().flatMap(as -> as.getActivities().stream())
                .filter(a -> !used.contains(a)).collect(Collectors.toCollection(LinkedHashSet::new));
        remove.forEach(a -> ((ActivitySet)a.eContainer()).getActivities().remove(a));
    }

    public static Collection<IResource> getResources(DispatchGroup group) {
        return group.getDispatches().stream().map(Dispatch::getActivity).flatMap(a -> a.getNodes().stream())
                .filter(ResourceAction.class::isInstance).map(ResourceAction.class::cast)
                .map(ResourceAction::getResource).distinct().collect(Collectors.toSet());
    }

    public static int getNumberRepeats(DispatchGroup group) {
        return getRepeatSeries(group).length;
    }

    public static int getYield(DispatchGroup dg, IResource resource) {
        Integer result = dg.getResourceYield().get(resource);
        return result != null ? result : dg.eIsSet(DispatchingPackage.Literals.DISPATCH_GROUP__YIELD) ? dg.getYield()
                : getYield((ActivityDispatching)dg.eContainer(), resource);
    }

    private static Integer[] getRepeatSeries(DispatchGroup group) {
        if (group.getRepeats().size() == 0) {
            return new Integer[] {1};
        }
        return group.getRepeats().stream()
                .flatMap(r -> IntStream.range(r.getStart(), r.getStart() + r.getCount()).boxed())
                .toArray(Integer[]::new);
    }

    private static int getYield(DispatchGroup dg) {
        return dg.eIsSet(DispatchingPackage.Literals.DISPATCH_GROUP__YIELD) ? dg.getYield()
                : ((ActivityDispatching)dg.eContainer()).getNumberOfIterations();
    }

    private static int getYield(ActivityDispatching ad, IResource resource) {
        if (ad != null) {
            Integer result = ad.getResourceIterations().get(resource);
            return result != null ? result : ad.getNumberOfIterations();
        }
        return 1;
    }

    private static void userAttr(Dispatch group, String name, String value) {
        Attribute attribute = DispatchingFactory.eINSTANCE.createAttribute();
        attribute.setName(name);
        group.getUserAttributes().put(attribute, value);
    }
}
