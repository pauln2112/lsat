/**
 */
package lsat_graph;

import org.eclipse.lsat.common.scheduler.graph.Task;

import dispatching.DispatchGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dispatch Group Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.DispatchGroupTask#getDispatchGroup <em>Dispatch Group</em>}</li>
 * </ul>
 *
 * @see lsat_graph.lsat_graphPackage#getDispatchGroupTask()
 * @model
 * @generated
 */
public interface DispatchGroupTask extends Task {
	/**
     * Returns the value of the '<em><b>Dispatch Group</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatch Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Dispatch Group</em>' reference.
     * @see #setDispatchGroup(DispatchGroup)
     * @see lsat_graph.lsat_graphPackage#getDispatchGroupTask_DispatchGroup()
     * @model required="true"
     * @generated
     */
	DispatchGroup getDispatchGroup();

	/**
     * Sets the value of the '{@link lsat_graph.DispatchGroupTask#getDispatchGroup <em>Dispatch Group</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Dispatch Group</em>' reference.
     * @see #getDispatchGroup()
     * @generated
     */
	void setDispatchGroup(DispatchGroup value);

} // DispatchGroupTask
