/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionSetPoint;
import org.eclipse.lsat.motioncalculator.MotionValidationException;

public final class MotionSegmentUtilities {
    private MotionSegmentUtilities() {
        // Empty for utility classes
    }

    public static boolean isMotionProfile(Collection<MotionSegment> segments, String motionProfileKey) {
        return segments.stream().flatMap(s -> s.getMotionSetpoints().stream()).map(MotionSetPoint::getMotionProfile)
                .allMatch(p -> Objects.equals(p.getKey(), motionProfileKey));
    }

    public static Set<MotionProfile> getMotionProfiles(Collection<MotionSegment> segments) {
        return segments.stream().flatMap(s -> s.getMotionSetpoints().stream()).map(MotionSetPoint::getMotionProfile)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public static Set<String> getSetPointIds(MotionSegment segment) throws MotionValidationException {
        return segment.getMotionSetpoints().stream().map(m -> m.getId())
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public static List<MotionSetPoint> getMotionSetPoints(List<MotionSegment> segments, String setPointId)
            throws MotionValidationException
    {
        return segments.stream().map(s -> s.getMotionSetpoint(setPointId)).collect(Collectors.toList());
    }

    public static Set<String> getSetPointIds(Collection<MotionSegment> segments) throws MotionValidationException {
        Set<String> setPointIds = getSetPointIds(segments.iterator().next());
        for (MotionSegment segment: segments) {
            if (!setPointIds.equals(getSetPointIds(segment))) {
                throw new MotionValidationException(
                        "Programming error: A concatenated move cannot have different setpoints. "
                                + "Please contact LSAT support.",
                        segment);
            }
        }
        return setPointIds;
    }

    /**
     * Returns all set-points that move in <tt>segments</tt>.
     */
    public static Collection<String> getMovingSetPointsIds(Collection<MotionSegment> segments)
            throws MotionValidationException
    {
        return getSetPointIds(segments).stream()
                .filter(s -> getMotionSetPoints(segments, s).stream().anyMatch(MotionSetPoint::doesMove))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Returns all set-points that settled at the end of <tt>segments</tt>, see {@link MotionSetPoint#isSettling()}.
     */
    public static Set<String> getSettledSetPointIds(Collection<MotionSegment> segments)
            throws MotionValidationException
    {
        return segments.stream().reduce((first, second) -> second).get().getMotionSetpoints().stream()
                .filter(MotionSetPoint::isSettling).map(MotionSetPoint::getId)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * Returns all {@link MotionSetPoint}s in order for a <tt>setPoint</tt> in <tt>segments</tt>.
     */
    public static List<MotionSetPoint> getMotionSetPoints(Collection<MotionSegment> segments, String setPointId) {
        return segments.stream().map(s -> s.getMotionSetpoint(setPointId))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Returns the {@link MotionSetPoint}s that move for all set-points in all <tt>segments</tt>.
     */
    public static List<MotionSetPoint> getMovingMotionSetPoints(Collection<MotionSegment> segments) {
        return segments.stream().flatMap(s -> s.getMotionSetpoints().stream()).filter(MotionSetPoint::doesMove)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
