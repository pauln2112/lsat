/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api.adapter;

import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.common.ludus.backend.fsm.impl.FSMImpl;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;
import org.eclipse.lsat.common.mpt.Edge;
import org.eclipse.lsat.common.mpt.FSM;
import org.eclipse.lsat.common.mpt.FSMState;
import org.eclipse.lsat.common.mpt.FSMTransition;
import org.eclipse.lsat.common.mpt.MaxPlusSpecification;
import org.eclipse.lsat.common.mpt.Vertex;
import org.eclipse.lsat.common.mpt.api.MaxPlusException;

public class FSMAdapter {
    private FSMAdapter() {
        // Empty
    }

    /**
     * Converts an FSM from the mpt metamodel into a Ludus FSM.
     *
     * @param mptFSM MPT FSM
     * @return Ludus FSM
     * @throws MaxPlusException thrown when the fsm contains an unnamed location
     */
    public static FSMImpl getFSM(MaxPlusSpecification specification, FSM mptFSM) throws MaxPlusException {
        // Get uncontrollable events
        Set<String> uncontrollable = specification.getUncontrollable().stream().map(event -> event.getName())
                .collect(Collectors.toSet());

        FSMImpl fsmImpl = new FSMImpl();

        // Locations
        for (Vertex v: mptFSM.getVertices()) {
            FSMState s = (FSMState)v;

            // Check that the name is not empty.
            if (s.getName() == null || s.getName().isEmpty()) {
                throw new MaxPlusException(
                        "Automaton " + mptFSM.getName() + " has a location without name. This is not supported.");
            }

            Location location = new org.eclipse.lsat.common.ludus.backend.fsm.impl.Location(s.getName());
            fsmImpl.addLocation(location);
            if (s.isMarked()) {
                fsmImpl.setMarked(location);
            }
        }

        // Initial location
        fsmImpl.setInitial(fsmImpl.getLocation((mptFSM.getInitial()).getName()));

        // Edges
        for (Edge e: mptFSM.getEdges()) {
            org.eclipse.lsat.common.ludus.backend.fsm.impl.Location src = fsmImpl
                    .getLocation(((FSMState)e.getSource()).getName());
            org.eclipse.lsat.common.ludus.backend.fsm.impl.Location tgt = fsmImpl
                    .getLocation(((FSMState)e.getTarget()).getName());
            String event = ((FSMTransition)e).getName();

            fsmImpl.addEdge(src, tgt, event);

            if (uncontrollable.contains(event)) {
                fsmImpl.addUncontrollable(event);
            } else {
                fsmImpl.addControllable(event);
            }
        }

        return fsmImpl;
    }
}
