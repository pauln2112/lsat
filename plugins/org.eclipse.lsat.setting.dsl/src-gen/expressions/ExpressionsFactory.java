/**
 */
package expressions;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see expressions.ExpressionsPackage
 * @generated
 */
public interface ExpressionsFactory extends EFactory {
	/**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	ExpressionsFactory eINSTANCE = expressions.impl.ExpressionsFactoryImpl.init();

	/**
     * Returns a new object of class '<em>Declaration</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Declaration</em>'.
     * @generated
     */
	Declaration createDeclaration();

	/**
     * Returns a new object of class '<em>Add</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Add</em>'.
     * @generated
     */
	Add createAdd();

	/**
     * Returns a new object of class '<em>Subtract</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Subtract</em>'.
     * @generated
     */
	Subtract createSubtract();

	/**
     * Returns a new object of class '<em>Multiply</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Multiply</em>'.
     * @generated
     */
	Multiply createMultiply();

	/**
     * Returns a new object of class '<em>Big Decimal Constant</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Big Decimal Constant</em>'.
     * @generated
     */
	BigDecimalConstant createBigDecimalConstant();

	/**
     * Returns a new object of class '<em>Declaration Ref</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Declaration Ref</em>'.
     * @generated
     */
	DeclarationRef createDeclarationRef();

	/**
     * Returns a new object of class '<em>Divide</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Divide</em>'.
     * @generated
     */
	Divide createDivide();

	/**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
	ExpressionsPackage getExpressionsPackage();

} //ExpressionsFactory
