/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.simulator.common

import java.util.Collections
import machine.Distance
import machine.HasResourcePeripheral
import machine.IResource
import machine.Peripheral
import machine.Position
import machine.Resource

class Common {
    public static val INITIAL_POSITION = '_INITIAL_POSITION'
    public static val CURRENT_POSITION = '_CURRENT_POSITION'

    static def getID(HasResourcePeripheral rp) {
       return getID(rp.resource, rp.peripheral)
    }

    static def getID(IResource resource, Peripheral peripheral) {
        return '''«resource.fqn.replaceAll('\\W+', '_')»_«peripheral.name»'''
    }
    
    static def getPositionID(HasResourcePeripheral rp, Position position) {
        return getPositionID(rp.resource, rp.peripheral, position)
    }

    static def getPositionID(IResource resource, Peripheral peripheral, Position position) {
        return '''«getID(resource, peripheral)»_«position.name»'''
    }

    static def getPositionID(HasResourcePeripheral rp, String position) {
        return getPositionID(rp.resource, rp.peripheral, position)
    }

    static def getPositionID(IResource resource, Peripheral peripheral, String position) {
        return '''«getID(resource, peripheral)»_«position.replaceAll('\\W+', '_')»'''
    }

    static def getDistanceID(HasResourcePeripheral rp, Distance distance) {
        return getDistanceID(rp.resource, rp.peripheral, distance)
    }

    static def getDistanceID(IResource resource, Peripheral peripheral, Distance distance) {
        return '''«getID(resource, peripheral)»_«distance.name»'''
    }

    static def getItemsOrResource(IResource resource) {
        return switch (resource) {
        	Resource case !resource.items.isEmpty: resource.items
        	default: Collections.singleton(resource)
        } 
    }
}