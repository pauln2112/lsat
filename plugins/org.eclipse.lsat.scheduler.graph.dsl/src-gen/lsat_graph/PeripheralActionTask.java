/**
 */
package lsat_graph;

import activity.PeripheralAction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Peripheral Action Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lsat_graph.lsat_graphPackage#getPeripheralActionTask()
 * @model
 * @generated
 */
public interface PeripheralActionTask extends ActionTask<PeripheralAction> {
} // PeripheralActionTask
