/**
 */
package machine.util;

import java.util.Map;

import machine.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see machine.MachinePackage
 * @generated
 */
public class MachineAdapterFactory extends AdapterFactoryImpl {
	/**
     * The cached model package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static MachinePackage modelPackage;

	/**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MachineAdapterFactory() {
        if (modelPackage == null)
        {
            modelPackage = MachinePackage.eINSTANCE;
        }
    }

	/**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
	@Override
	public boolean isFactoryForType(Object object) {
        if (object == modelPackage)
        {
            return true;
        }
        if (object instanceof EObject)
        {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

	/**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected MachineSwitch<Adapter> modelSwitch =
		new MachineSwitch<Adapter>()
        {
            @Override
            public Adapter casePeripheralType(PeripheralType object)
            {
                return createPeripheralTypeAdapter();
            }
            @Override
            public Adapter casePath(Path object)
            {
                return createPathAdapter();
            }
            @Override
            public Adapter caseSymbolicPosition(SymbolicPosition object)
            {
                return createSymbolicPositionAdapter();
            }
            @Override
            public Adapter caseResource(Resource object)
            {
                return createResourceAdapter();
            }
            @Override
            public Adapter caseActionType(ActionType object)
            {
                return createActionTypeAdapter();
            }
            @Override
            public Adapter casePeripheral(Peripheral object)
            {
                return createPeripheralAdapter();
            }
            @Override
            public Adapter caseMachine(Machine object)
            {
                return createMachineAdapter();
            }
            @Override
            public Adapter caseImportContainer(ImportContainer object)
            {
                return createImportContainerAdapter();
            }
            @Override
            public Adapter caseImport(Import object)
            {
                return createImportAdapter();
            }
            @Override
            public Adapter caseProfile(Profile object)
            {
                return createProfileAdapter();
            }
            @Override
            public Adapter caseAxis(Axis object)
            {
                return createAxisAdapter();
            }
            @Override
            public Adapter casePosition(Position object)
            {
                return createPositionAdapter();
            }
            @Override
            public Adapter caseSetPoint(SetPoint object)
            {
                return createSetPointAdapter();
            }
            @Override
            public Adapter caseAxisPositionMapEntry(Map.Entry<Axis, Position> object)
            {
                return createAxisPositionMapEntryAdapter();
            }
            @Override
            public Adapter caseAxisPositionsMapEntry(Map.Entry<Axis, EList<Position>> object)
            {
                return createAxisPositionsMapEntryAdapter();
            }
            @Override
            public Adapter casePathTargetReference(PathTargetReference object)
            {
                return createPathTargetReferenceAdapter();
            }
            @Override
            public Adapter caseUnidirectionalPath(UnidirectionalPath object)
            {
                return createUnidirectionalPathAdapter();
            }
            @Override
            public Adapter caseBidirectionalPath(BidirectionalPath object)
            {
                return createBidirectionalPathAdapter();
            }
            @Override
            public Adapter caseFullMeshPath(FullMeshPath object)
            {
                return createFullMeshPathAdapter();
            }
            @Override
            public Adapter casePathAnnotation(PathAnnotation object)
            {
                return createPathAnnotationAdapter();
            }
            @Override
            public Adapter caseResourceItem(ResourceItem object)
            {
                return createResourceItemAdapter();
            }
            @Override
            public Adapter caseIResource(IResource object)
            {
                return createIResourceAdapter();
            }
            @Override
            public Adapter caseDistance(Distance object)
            {
                return createDistanceAdapter();
            }
            @Override
            public Adapter caseHasResourcePeripheral(HasResourcePeripheral object)
            {
                return createHasResourcePeripheralAdapter();
            }
            @Override
            public Adapter caseHasSettling(HasSettling object)
            {
                return createHasSettlingAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object)
            {
                return createEObjectAdapter();
            }
        };

	/**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
	@Override
	public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject)target);
    }


	/**
     * Creates a new adapter for an object of class '{@link machine.PeripheralType <em>Peripheral Type</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.PeripheralType
     * @generated
     */
	public Adapter createPeripheralTypeAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.Path <em>Path</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Path
     * @generated
     */
	public Adapter createPathAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.SymbolicPosition <em>Symbolic Position</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.SymbolicPosition
     * @generated
     */
	public Adapter createSymbolicPositionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.Resource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Resource
     * @generated
     */
	public Adapter createResourceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.ActionType <em>Action Type</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.ActionType
     * @generated
     */
	public Adapter createActionTypeAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.Peripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Peripheral
     * @generated
     */
	public Adapter createPeripheralAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.Machine <em>Machine</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Machine
     * @generated
     */
	public Adapter createMachineAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.ImportContainer <em>Import Container</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.ImportContainer
     * @generated
     */
	public Adapter createImportContainerAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.Import <em>Import</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Import
     * @generated
     */
	public Adapter createImportAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.Profile <em>Profile</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Profile
     * @generated
     */
	public Adapter createProfileAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.Axis <em>Axis</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Axis
     * @generated
     */
	public Adapter createAxisAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.Position <em>Position</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Position
     * @generated
     */
	public Adapter createPositionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.SetPoint <em>Set Point</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.SetPoint
     * @generated
     */
	public Adapter createSetPointAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Axis Position Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
	public Adapter createAxisPositionMapEntryAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Axis Positions Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see java.util.Map.Entry
     * @generated
     */
	public Adapter createAxisPositionsMapEntryAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.PathTargetReference <em>Path Target Reference</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.PathTargetReference
     * @generated
     */
	public Adapter createPathTargetReferenceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.UnidirectionalPath <em>Unidirectional Path</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.UnidirectionalPath
     * @generated
     */
	public Adapter createUnidirectionalPathAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.BidirectionalPath <em>Bidirectional Path</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.BidirectionalPath
     * @generated
     */
	public Adapter createBidirectionalPathAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.FullMeshPath <em>Full Mesh Path</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.FullMeshPath
     * @generated
     */
	public Adapter createFullMeshPathAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.PathAnnotation <em>Path Annotation</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.PathAnnotation
     * @generated
     */
	public Adapter createPathAnnotationAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.ResourceItem <em>Resource Item</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.ResourceItem
     * @generated
     */
	public Adapter createResourceItemAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.IResource <em>IResource</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.IResource
     * @generated
     */
	public Adapter createIResourceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.Distance <em>Distance</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.Distance
     * @generated
     */
	public Adapter createDistanceAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.HasResourcePeripheral <em>Has Resource Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.HasResourcePeripheral
     * @generated
     */
	public Adapter createHasResourcePeripheralAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link machine.HasSettling <em>Has Settling</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see machine.HasSettling
     * @generated
     */
	public Adapter createHasSettlingAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
	public Adapter createEObjectAdapter() {
        return null;
    }

} //MachineAdapterFactory
