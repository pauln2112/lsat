/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json;

import java.util.List;

import org.eclipse.lsat.motioncalculator.MotionSegment;

public class JsonRequest {
    private final JsonRequestType requestType;

    private final List<MotionSegment> segments;

    public JsonRequest(JsonRequestType requestType, List<MotionSegment> segments) {
        this.requestType = requestType;
        this.segments = segments;
    }

    public List<MotionSegment> getSegments() {
        return segments;
    }

    public JsonRequestType getRequestType() {
        return requestType;
    }
}
