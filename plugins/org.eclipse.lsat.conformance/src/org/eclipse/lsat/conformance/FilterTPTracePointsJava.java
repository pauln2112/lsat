/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;
import static org.eclipse.lsat.conformance.internal.UniqueTracePoint.matchesOneOf;

import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.conformance.internal.ConformanceCheckQueries;
import org.eclipse.lsat.conformance.internal.UniqueTracePoint;
import org.eclipse.lsat.trace.TraceLine;
import org.eclipse.lsat.trace.TraceModel;

import activity.Action;
import activity.Activity;

public class FilterTPTracePointsJava {
    public List<TraceModel> transformModel(FilterTPTracePointsInput input) throws QvtoTransformationException {
        // Wrapping trace points with UniqueTracePoint and adding them to a Set removes all duplicates and saves time!
        Set<UniqueTracePoint> tracePoints = from(input.getActivities()).xcollect(Activity::getNodes)
                .objectsOfKind(Action.class).xcollect(ConformanceCheckQueries::getTracePoints).asSet();

        for (TraceModel traceModel: input.getTraceModels()) {
            // Using iterator with i.remove() to avoid concurrent modification exception
            for (ListIterator<TraceLine> traceLines = traceModel.getLines().listIterator(); traceLines.hasNext();) {
                TraceLine traceLine = traceLines.next();

                if (!matchesOneOf(traceLine, tracePoints)) {
                    traceLines.remove();
                    EcoreUtil.remove(traceLine);
                }
            }
        }
        return input.getTraceModels();
    }
}
