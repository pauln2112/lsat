/**
 */
package activity.util;

import activity.*;

import machine.HasResourcePeripheral;
import machine.IResource;
import machine.ImportContainer;
import machine.Resource;
import machine.ResourceItem;
import org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph;
import org.eclipse.lsat.common.graph.directed.editable.Node;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see activity.ActivityPackage
 * @generated
 */
public class ActivitySwitch<T> extends Switch<T> {
	/**
     * The cached model package
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static ActivityPackage modelPackage;

	/**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ActivitySwitch() {
        if (modelPackage == null)
        {
            modelPackage = ActivityPackage.eINSTANCE;
        }
    }

	/**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

	/**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID)
        {
            case ActivityPackage.ACTIVITY_SET:
            {
                ActivitySet activitySet = (ActivitySet)theEObject;
                T result = caseActivitySet(activitySet);
                if (result == null) result = caseImportContainer(activitySet);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.ACTIVITY:
            {
                Activity activity = (Activity)theEObject;
                T result = caseActivity(activity);
                if (result == null) result = caseEditableDirectedGraph(activity);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.ACTION:
            {
                Action action = (Action)theEObject;
                T result = caseAction(action);
                if (result == null) result = caseNode(action);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.PERIPHERAL_ACTION:
            {
                PeripheralAction peripheralAction = (PeripheralAction)theEObject;
                T result = casePeripheralAction(peripheralAction);
                if (result == null) result = caseHasResourcePeripheral(peripheralAction);
                if (result == null) result = caseResourceAction(peripheralAction);
                if (result == null) result = caseAction(peripheralAction);
                if (result == null) result = caseNode(peripheralAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.CLAIM:
            {
                Claim claim = (Claim)theEObject;
                T result = caseClaim(claim);
                if (result == null) result = caseResourceAction(claim);
                if (result == null) result = caseAction(claim);
                if (result == null) result = caseNode(claim);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.RELEASE:
            {
                Release release = (Release)theEObject;
                T result = caseRelease(release);
                if (result == null) result = caseResourceAction(release);
                if (result == null) result = caseAction(release);
                if (result == null) result = caseNode(release);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.LOCATION_PREREQUISITE:
            {
                LocationPrerequisite locationPrerequisite = (LocationPrerequisite)theEObject;
                T result = caseLocationPrerequisite(locationPrerequisite);
                if (result == null) result = caseHasResourcePeripheral(locationPrerequisite);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.TRACE_POINT:
            {
                TracePoint tracePoint = (TracePoint)theEObject;
                T result = caseTracePoint(tracePoint);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.SYNC_BAR:
            {
                SyncBar syncBar = (SyncBar)theEObject;
                T result = caseSyncBar(syncBar);
                if (result == null) result = caseNode(syncBar);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.SIMPLE_ACTION:
            {
                SimpleAction simpleAction = (SimpleAction)theEObject;
                T result = caseSimpleAction(simpleAction);
                if (result == null) result = casePeripheralAction(simpleAction);
                if (result == null) result = caseHasResourcePeripheral(simpleAction);
                if (result == null) result = caseResourceAction(simpleAction);
                if (result == null) result = caseAction(simpleAction);
                if (result == null) result = caseNode(simpleAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.MOVE:
            {
                Move move = (Move)theEObject;
                T result = caseMove(move);
                if (result == null) result = casePeripheralAction(move);
                if (result == null) result = caseHasResourcePeripheral(move);
                if (result == null) result = caseResourceAction(move);
                if (result == null) result = caseAction(move);
                if (result == null) result = caseNode(move);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.EVENT_ACTION:
            {
                EventAction eventAction = (EventAction)theEObject;
                T result = caseEventAction(eventAction);
                if (result == null) result = caseResourceAction(eventAction);
                if (result == null) result = caseAction(eventAction);
                if (result == null) result = caseNode(eventAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.RAISE_EVENT:
            {
                RaiseEvent raiseEvent = (RaiseEvent)theEObject;
                T result = caseRaiseEvent(raiseEvent);
                if (result == null) result = caseEventAction(raiseEvent);
                if (result == null) result = caseResourceAction(raiseEvent);
                if (result == null) result = caseAction(raiseEvent);
                if (result == null) result = caseNode(raiseEvent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.REQUIRE_EVENT:
            {
                RequireEvent requireEvent = (RequireEvent)theEObject;
                T result = caseRequireEvent(requireEvent);
                if (result == null) result = caseEventAction(requireEvent);
                if (result == null) result = caseResourceAction(requireEvent);
                if (result == null) result = caseAction(requireEvent);
                if (result == null) result = caseNode(requireEvent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.RESOURCE_ACTION:
            {
                ResourceAction resourceAction = (ResourceAction)theEObject;
                T result = caseResourceAction(resourceAction);
                if (result == null) result = caseAction(resourceAction);
                if (result == null) result = caseNode(resourceAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.EVENT:
            {
                Event event = (Event)theEObject;
                T result = caseEvent(event);
                if (result == null) result = caseResource(event);
                if (result == null) result = caseIResource(event);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ActivityPackage.EVENT_ITEM:
            {
                EventItem eventItem = (EventItem)theEObject;
                T result = caseEventItem(eventItem);
                if (result == null) result = caseResourceItem(eventItem);
                if (result == null) result = caseIResource(eventItem);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Set</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseActivitySet(ActivitySet object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Activity</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Activity</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseActivity(Activity object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseAction(Action object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Peripheral Action</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Peripheral Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePeripheralAction(PeripheralAction object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Claim</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Claim</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseClaim(Claim object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Release</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Release</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseRelease(Release object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Location Prerequisite</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Location Prerequisite</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseLocationPrerequisite(LocationPrerequisite object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Trace Point</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Trace Point</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseTracePoint(TracePoint object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Sync Bar</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sync Bar</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseSyncBar(SyncBar object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Simple Action</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Simple Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseSimpleAction(SimpleAction object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Move</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Move</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseMove(Move object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Event Action</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Event Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEventAction(EventAction object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Raise Event</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Raise Event</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseRaiseEvent(RaiseEvent object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Require Event</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Require Event</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseRequireEvent(RequireEvent object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Resource Action</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Resource Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseResourceAction(ResourceAction object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Event</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEvent(Event object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Event Item</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Event Item</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEventItem(EventItem object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Import Container</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Import Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseImportContainer(ImportContainer object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Editable Directed Graph</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Editable Directed Graph</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseEditableDirectedGraph(EditableDirectedGraph object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Node</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseNode(Node object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Has Resource Peripheral</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Has Resource Peripheral</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseHasResourcePeripheral(HasResourcePeripheral object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>IResource</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IResource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIResource(IResource object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Resource</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseResource(Resource object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Resource Item</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Resource Item</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseResourceItem(ResourceItem object)
    {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
	@Override
	public T defaultCase(EObject object) {
        return null;
    }

} //ActivitySwitch
