/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// TODO: Adapt these locations to match the screen resolution
var width = 600;
var height = 250; 

var LoadRobot_XY_IN_X = 100;
var LoadRobot_XY_COND_X = LoadRobot_XY_IN_X + width * 1/3;
var LoadRobot_XY_DRILL_X = LoadRobot_XY_IN_X + width * 2/3;
var LoadRobot_XY_ABOVE_Y = 50;
var LoadRobot_XY_AT_Y = LoadRobot_XY_ABOVE_Y + height;
var LoadRobot_XY_OUT_DRILL_Y = LoadRobot_XY_AT_Y - 40;

var UnloadRobot_XY_OUT_X = LoadRobot_XY_IN_X + width;// * 3/3;
// Reusing locations of load robot!
//var UnloadRobot_XY_COND_X = 2;
//var UnloadRobot_XY_DRILL_X = 3;
//var UnloadRobot_XY_ABOVE_Y = 0;
//var UnloadRobot_XY_OUT_DRILL_Y = 0.8;
//var UnloadRobot_XY_AT_Y = 2;

var Drill_ZR_UP_Z = LoadRobot_XY_AT_Y - 20;
var Drill_ZR_DOWN_Z = LoadRobot_XY_AT_Y + 80 ;

// Auto generated, do not modify!
var Drill_ZR_UP = {Z:Drill_ZR_UP_Z};
var Drill_ZR_DOWN = {Z:Drill_ZR_DOWN_Z};
var LoadRobot_XY_ABOVE_IN = {X:LoadRobot_XY_IN_X, Y:LoadRobot_XY_ABOVE_Y};
var LoadRobot_XY_ABOVE_COND = {X:LoadRobot_XY_COND_X, Y:LoadRobot_XY_ABOVE_Y};
var LoadRobot_XY_ABOVE_DRILL = {X:LoadRobot_XY_DRILL_X, Y:LoadRobot_XY_ABOVE_Y};
var LoadRobot_XY_AT_IN = {X:LoadRobot_XY_IN_X, Y:LoadRobot_XY_AT_Y};
var LoadRobot_XY_AT_COND = {X:LoadRobot_XY_COND_X, Y:LoadRobot_XY_AT_Y};
var LoadRobot_XY_AT_DRILL = {X:LoadRobot_XY_DRILL_X, Y:LoadRobot_XY_AT_Y};
var LoadRobot_XY_OUT_DRILL = {X:LoadRobot_XY_DRILL_X, Y:LoadRobot_XY_OUT_DRILL_Y};
var UnloadRobot_XY_ABOVE_OUT = {X:UnloadRobot_XY_OUT_X, Y:LoadRobot_XY_ABOVE_Y}; //{X:UnloadRobot_XY_OUT_X, Y:UnloadRobot_XY_ABOVE_Y};
var UnloadRobot_XY_ABOVE_COND = LoadRobot_XY_ABOVE_COND; //{X:UnloadRobot_XY_COND_X, Y:UnloadRobot_XY_ABOVE_Y};
var UnloadRobot_XY_ABOVE_DRILL = LoadRobot_XY_ABOVE_DRILL; //{X:UnloadRobot_XY_DRILL_X, Y:UnloadRobot_XY_ABOVE_Y};
var UnloadRobot_XY_AT_OUT = {X:UnloadRobot_XY_OUT_X, Y:LoadRobot_XY_AT_Y}; //{X:UnloadRobot_XY_OUT_X, Y:UnloadRobot_XY_AT_Y};
var UnloadRobot_XY_AT_COND = LoadRobot_XY_AT_COND; //{X:UnloadRobot_XY_COND_X, Y:UnloadRobot_XY_AT_Y};
var UnloadRobot_XY_AT_DRILL = LoadRobot_XY_AT_DRILL; //{X:UnloadRobot_XY_DRILL_X, Y:UnloadRobot_XY_AT_Y};
var UnloadRobot_XY_OUT_DRILL = LoadRobot_XY_OUT_DRILL; //{X:UnloadRobot_XY_DRILL_X, Y:UnloadRobot_XY_OUT_DRILL_Y};
