/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.lsat.petri_net.PetriNet;
import org.eclipse.lsat.trace.TraceModel;

public class ConformanceCheckInput {
    public static enum Level {
        Minimal, Claim, Dispatching
    }

    private final List<TraceModel> traces = new ArrayList<TraceModel>();

    private final PetriNet petriNet;

    public ConformanceCheckInput(PetriNet petriNet) {
        this.petriNet = petriNet;
    }

    public PetriNet getPetriNet() {
        return petriNet;
    }

    public List<TraceModel> getTraces() {
        return traces;
    }
}
