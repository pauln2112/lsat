/**
 */
package expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Add</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see expressions.ExpressionsPackage#getAdd()
 * @model
 * @generated
 */
public interface Add extends BinaryExpression {

} // Add
