/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.util;

import static java.lang.String.format;
import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.script.ScriptException;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.lsat.activity.teditor.validation.ActivityValidator;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileParameter;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionSetPoint;
import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.timing.Activator;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.xtext.EcoreUtil2;

import activity.Activity;
import activity.Move;
import expressions.Expression;
import machine.Axis;
import machine.Distance;
import machine.HasResourcePeripheral;
import machine.IResource;
import machine.PathTargetReference;
import machine.Peripheral;
import machine.PeripheralType;
import machine.Position;
import machine.Profile;
import machine.Resource;
import machine.ResourceItem;
import machine.SetPoint;
import machine.SymbolicPosition;
import machine.impl.MachineQueries;
import machine.util.ResourcePeripheralKey;
import setting.MotionProfileSettings;
import setting.MotionSettings;
import setting.PhysicalLocation;
import setting.PhysicalSettings;
import setting.Settings;

public class MotionCalculatorHelper {

    private final Settings settings;

    private final MotionCalculatorExtension motionCalculator;

    public MotionCalculatorHelper(Settings settings, MotionCalculatorExtension motionCalculator) {
        this.settings = settings;
        this.motionCalculator = motionCalculator;
    }

    public Settings getSettings() {
        return settings;
    }

    public MotionCalculatorExtension getMotionCalculator() {
        return motionCalculator;
    }

    public List<Move> getConcatenatedMove(Move self) {
        LinkedList<Move> fullMove = new LinkedList<Move>();
        Move predecessorMove = self;
        do {
            fullMove.offerFirst(predecessorMove);
            predecessorMove = predecessorMove.getPredecessorMove();
        } while (predecessorMove != null && !predecessorMove.isStopAtTarget());

        while (!fullMove.getLast().isStopAtTarget()) {
            fullMove.offerLast(fullMove.getLast().getSuccessorMove());
        }
        return fullMove;
    }

    public void validate(List<Move> concatenatedMove) throws SpecificationException {
        validate(concatenatedMove, concatenatedMove.get(0).getPeripheral().getType().getAxes());
    }

    public List<MotionSegment> createMotionSegments(List<Move> concatenatedMove) throws SpecificationException {
        return concatenatedMove.isEmpty() ? Collections.emptyList()
                : createMotionSegments(concatenatedMove, concatenatedMove.get(0).getPeripheral().getType().getAxes());
    }

    public void validate(List<Move> concatenatedMove, Collection<Axis> axes) throws SpecificationException {
        ArrayList<MotionSegment> segments = new ArrayList<>(concatenatedMove.size());
        for (Move move: concatenatedMove) {
            if (!canValidate(move)) {
                return;
            }
            if (move.isPositionMove()) {
                segments.add(createPositionMotionSegment(move, axes));
            } else {
                segments.add(createDistanceMotionSegment(move, axes));
            }
        }
        try {
            motionCalculator.validate(segments);
        } catch (MotionValidationException e) {
            Move[] moves = e.getSegments().stream().map(s -> concatenatedMove.get(segments.indexOf(s)))
                    .toArray(Move[]::new);
            throw new SpecificationException(e.getMessage(), e, moves);
        }
    }

    public List<MotionSegment> createMotionSegments(List<Move> concatenatedMove, Collection<Axis> axes)
            throws SpecificationException
    {
        ArrayList<MotionSegment> segments = new ArrayList<>(concatenatedMove.size());
        for (Move move: concatenatedMove) {
            if (move.isPositionMove()) {
                segments.add(createPositionMotionSegment(move, axes));
            } else {
                segments.add(createDistanceMotionSegment(move, axes));
            }
        }
        try {
            motionCalculator.validate(segments);
        } catch (MotionValidationException e) {
            Move[] moves = e.getSegments().stream().map(s -> concatenatedMove.get(segments.indexOf(s)))
                    .toArray(Move[]::new);
            Activity act = EcoreUtil2.getContainerOfType(concatenatedMove.get(0), Activity.class);
            String prefix = Stream.of(moves).map(Move::getName)
                    .collect(Collectors.joining("->", "", " in activity " + act.getName() + ":\n\n"));
            throw new SpecificationException(prefix + e.getMessage(), e, moves);
        }
        return segments;
    }

    private MotionSegment createDistanceMotionSegment(Move move, Collection<Axis> axes) throws SpecificationException {
        final Peripheral peripheral = move.getPeripheral();
        final PeripheralType peripheralType = peripheral.getType();
        final Distance distance = move.getDistance();
        final Profile profile = move.getProfile();

        if (null == distance) {
            throw new SpecificationException(format(
                    "Don't know how to move peripheral %s using speed profile %s, because the distance is not set.",
                    peripheral.fqn(), profile.getName()), move);
        }

        final AxesDistance axesDistance = getAxesDistance(move.getResource(), distance);
        final Map<SetPoint, BigDecimal> setPoints = toSetPoint(peripheralType, axesDistance);

        final MotionSegment result = new MotionSegment(ActivityValidator.id(move));
        for (SetPoint setPoint: from(axes).collect(Axis::getSetPoints).asOrderedSet()) {
            final Set<Axis> motionAxes = new LinkedHashSet<>(setPoint.getAxes());
            if (motionAxes.isEmpty()) {
                throw new SpecificationException(
                        format("Setpoint %s should refer to at least one axis for peripheral type %s",
                                setPoint.getName(), peripheral.getType().getName()),
                        move, setPoint);
            } else if (!axes.containsAll(motionAxes)) {
                throw new IllegalArgumentException("Argument 'axes' should contain all axes for setpoint");
            }

            final Set<Axis> settlingAxes = new LinkedHashSet<>(distance.getSettling());
            settlingAxes.retainAll(motionAxes);

            final MotionSetPoint motionSetPoint = new MotionSetPoint(setPoint.getName());
            motionSetPoint.setDistance(setPoints.get(setPoint));
            motionSetPoint.setSettling(!settlingAxes.isEmpty());
            final Set<Axis> movingAxes = axesDistance.getMovingAxes();

            setProfileArguments(motionSetPoint, move, motionAxes, settlingAxes, movingAxes);

            result.addMotionSetpoint(motionSetPoint);
        }
        return result;
    }

    private MotionSegment createPositionMotionSegment(Move move, Collection<Axis> axes) throws SpecificationException {
        final Peripheral peripheral = move.getPeripheral();
        final PeripheralType peripheralType = peripheral.getType();
        final SymbolicPosition source = move.getSourcePosition();
        final SymbolicPosition target = move.getTargetPosition();
        final Profile profile = move.getProfile();

        if (null == source) {
            throw new SpecificationException(format(
                    "Don't know how to move peripheral %s to %s using speed profile %s, because the source position is not set.",
                    peripheral.fqn(), target.getName(), profile.getName()), move);
        }

        final PathTargetReference targetReference = MachineQueries.findPath(source, target, profile);
        if (null == targetReference) {
            throw new SpecificationException(
                    format("Don't know how to move peripheral %s from %s to %s using speed profile %s",
                            peripheral.fqn(), source.getName(), target.getName(), profile.getName()),
                    move);
        }

        final AxesLocation sourceAxesLocation = getAxesLocation(move.getResource(), source);
        final Map<SetPoint, BigDecimal> sourceSetPointsLocation = toSetPoint(peripheralType, sourceAxesLocation);
        final AxesLocation targetAxesLocation = getAxesLocation(move.getResource(), target);
        final Map<SetPoint, BigDecimal> targetSetPointsLocation = toSetPoint(peripheralType, targetAxesLocation);

        final MotionSegment result = new MotionSegment(ActivityValidator.id(move));
        for (SetPoint setPoint: from(axes).collect(Axis::getSetPoints).asOrderedSet()) {
            final Set<Axis> motionAxes = new LinkedHashSet<>(setPoint.getAxes());
            if (motionAxes.isEmpty()) {
                throw new SpecificationException(
                        format("Setpoint %s should refer to at least one axis for peripheral type %s",
                                setPoint.getName(), peripheral.getType().getName()),
                        move, setPoint);
            } else if (!axes.containsAll(motionAxes)) {
                throw new IllegalArgumentException("Argument 'axes' should contain all axes for setpoint");
            }
            final Set<Axis> settlingAxes = new LinkedHashSet<>(targetReference.getSettling());
            settlingAxes.retainAll(motionAxes);

            final MotionSetPoint motionSetPoint = new MotionSetPoint(setPoint.getName());
            motionSetPoint.setFrom(sourceSetPointsLocation.get(setPoint));
            motionSetPoint.setTo(targetSetPointsLocation.get(setPoint));
            motionSetPoint.setDistance(motionSetPoint.getTo().subtract(motionSetPoint.getFrom()));
            motionSetPoint.setSettling(!settlingAxes.isEmpty());
            final Set<Axis> movingAxes = sourceAxesLocation.getMovingAxes(targetAxesLocation);
            setProfileArguments(motionSetPoint, move, motionAxes, settlingAxes, movingAxes);
            result.addMotionSetpoint(motionSetPoint);
        }
        return result;
    }

    private void setProfileArguments(final MotionSetPoint motionSetPoint, Move move, final Set<Axis> motionAxes,
            Set<Axis> settlingAxes, Set<Axis> movingAxes) throws SpecificationException
    {
        Peripheral peripheral = move.getPeripheral();
        Profile profile = move.getProfile();
        if (motionAxes.size() > 1) {
            // Trying to resolve the ambiguity
            // This setpoint is adjusted by multiple axes, which in their
            // turn may define different motion profiles.
            // We need to determine which motion profile to use as input. We
            // assume that if a setpoint is not adjusted and settling is not
            // defined for the setpoint, then the motion time will always be
            // 0 seconds. Only in that case the motion profile to use is a
            // don't
            // care.
            movingAxes.retainAll(motionAxes);

            if (movingAxes.isEmpty()) {
                if (settlingAxes.isEmpty()) {
                    // No move and no settling, so motion profile is don't
                    // care
                    retainHead(motionAxes);
                } else {
                    motionAxes.retainAll(settlingAxes);
                }
            } else {
                motionAxes.retainAll(movingAxes);
            }
        }

        final Iterator<Axis> motionAxesIterator = motionAxes.iterator();
        final Axis motionAxis = motionAxesIterator.next();
        MotionProfileSettings motionProfileSettings = getMotionProfileSettings(move, profile, motionAxis);

        while (motionAxesIterator.hasNext()) {
            // The ambiguity couldn't be resolved. As long as the motion
            // axes define exactly the same motion profile settings, this
            // doesn't matter
            final MotionProfileSettings otherMmotionProfileSettings = getMotionProfileSettings(move, profile,
                    motionAxesIterator.next());
            if (!deepEquals(motionProfileSettings, otherMmotionProfileSettings)) {
                throw new SpecificationException(format(
                        "Multiple axes adjust the same setpoints with different settings when peripheral %s moves for %s.",
                        peripheral.fqn(), MoveHelper.getName(move)), move, motionProfileSettings,
                        otherMmotionProfileSettings);
            }
        }

        final MotionProfile motionProfile = motionCalculator.getMotionProfile(motionProfileSettings.getMotionProfile());
        if (motionProfile == null) {
            throw new SpecificationException(format("Selected motion calculator %s does not support motion profile %s",
                    motionCalculator.getName(), motionProfileSettings.getMotionProfile()), move);
        }
        motionSetPoint.setMotionProfile(motionProfile);

        for (MotionProfileParameter parameter: motionProfile.getParameters()) {
            Expression expression = motionProfileSettings.getMotionArguments().get(parameter.getKey());
            if (expression != null) {
                final BigDecimal argument = expression.evaluate();
                if (argument != null) {
                    motionSetPoint.setMotionProfileArgument(parameter.getKey(), argument);
                } else if (parameter.isRequired()) {
                    throw new SpecificationException(format(
                            "Required motion profile argument %s is not set for profile %s on axes %s of peripheral %s",
                            parameter.getKey(), profile.getName(), motionAxis.getName(), peripheral.fqn()), move,
                            motionProfileSettings);
                }
            }
        }

    }

    private MotionProfileSettings getMotionProfileSettings(Move move, Profile profile, final Axis motionAxis)
            throws SpecificationException
    {
        EMap<Profile, MotionProfileSettings> profileSettings = getMotionSettings(move, motionAxis).getProfileSettings();

        MotionProfileSettings motionProfileSettings = profileSettings.get(profile);

        if (motionProfileSettings == null) {
            profileSettings.entrySet().stream().filter(e -> e.getKey() == profile).map(e -> e.getValue()).findFirst()
                    .orElse(null);
        }
        return motionProfileSettings;
    }

    private static Map<SetPoint, BigDecimal> toSetPoint(PeripheralType peripheralType, Map<Axis, BigDecimal> axesSettings) throws SpecificationException {
        return (null == peripheralType.getConversion()) ?
            toSetPointDirect(peripheralType, axesSettings) :
            toSetPointUsingScript(peripheralType, axesSettings);
    }

    private static Map<SetPoint, BigDecimal> toSetPointDirect(PeripheralType peripheralType,
            Map<Axis, BigDecimal> axesSettings) throws SpecificationException
    {
        final Map<SetPoint, BigDecimal> conversion = new HashMap<SetPoint, BigDecimal>(
                peripheralType.getSetPoints().size());
        for (Map.Entry<Axis, BigDecimal> entry: axesSettings.entrySet()) {
            for (SetPoint setPoint: entry.getKey().getSetPoints()) {
                if (!Objects.equals(entry.getKey().getUnit(), setPoint.getUnit())) {
                    throw new SpecificationException(format(
                            "Don\'t know how to convert %s axis unit [%s] to %s setpoint unit [%s], "
                                    + "please specifiy a conversion in peripheral type %s",
                                    entry.getKey().getName(), entry.getKey().getUnit(), setPoint.getName(),
                                    setPoint.getUnit(), peripheralType.getName()));
                } else if (null != conversion.put(setPoint, entry.getValue())) {
                    String axesNames = from(setPoint.getAxes()).collectOne(Axis::getName).joinfields(", ", "(",
                            ")");
                    throw new SpecificationException(format(
                            "Multiple axes %s adjust setpoint %s, "
                                    + "please specifiy a conversion in peripheral type %s",
                                    axesNames, setPoint.getName(), peripheralType.getName()));
                }
            }
        }
        return conversion;
    }

    private static Map<SetPoint, BigDecimal> toSetPointUsingScript(PeripheralType peripheralType, Map<Axis, BigDecimal> args ) throws SpecificationException {
        Map<String, Object> scriptArgs = from(args).toMap(e->e.getKey().getName(), Entry::getValue);
        try {
            Activator.getDefault().evalJavaScript(peripheralType.getConversion(), scriptArgs);
            return from(peripheralType.getSetPoints())
                    .toMap(sp->sp, sp->toBigDecimal(scriptArgs.get(sp.getName())));
        } catch (ScriptException e) {
            throw new SpecificationException(e.getMessage(), e, peripheralType);
        }
    }

    private static BigDecimal toBigDecimal(Object object) throws IllegalArgumentException {
        if (null == object) {
            return null;
        }
        if (object instanceof BigDecimal) {
            return (BigDecimal)object;
        }
        if (object instanceof Number) {
            return new BigDecimal(((Number)object).doubleValue());
        }
        throw new IllegalArgumentException("Cannot convert value to BigDecimal: " + object);
    }

    public AxesLocation getAxesLocation(IResource resource, SymbolicPosition position) throws SpecificationException {
        final AxesLocation axesLocation = new AxesLocation(position);
        for (Axis axis: AxesLocation.safeGetAxes(position)) {
            MotionSettings ms = getMotionSettings(resource, position.getPeripheral(), axis);
            Position axisPosition = position.getPosition(axis);
            PhysicalLocation axisLocation = ms.getLocationSettings().get(axisPosition);
            if (null == axisLocation) {
                throw new SpecificationException(
                        format("Axis location not specified for position %s of axis %s in peripheral %s",
                                axisPosition.getName(), axis.getName(), position.getPeripheral().fqn()),
                        ms);
            }
            axesLocation.put(axis, axisLocation.getDefault());
        }
        return axesLocation;
    }

    public AxesDistance getAxesDistance(IResource resource, Distance distance) throws SpecificationException {
        final AxesDistance axesDistance = new AxesDistance(distance);
        for (Axis axis: AxesDistance.safeGetAxes(distance)) {
            MotionSettings ms = getMotionSettings(resource, distance.getPeripheral(), axis);
            BigDecimal value = BigDecimal.ZERO;
            Expression expression = ms.getDistanceSettings().get(distance);
            if (null == expression) {
                throw new SpecificationException(
                        format("Axis distance not specified for distance %s of axis %s in peripheral %s",
                                distance.getName(), axis.getName(), distance.getPeripheral().fqn()),
                        ms);
            }
            value = expression.evaluate();
            axesDistance.put(axis, value);
        }
        return axesDistance;
    }

    /**
     * a validation can be done if:
     * <ul>
     *   <li>resource is resource item</li>
     *   <li>resource has no items</li>
     *   <li>resource has settings specified on resource level (iso item level)</li>
     * </ul>
     */
    private boolean canValidate(HasResourcePeripheral rp) {
        if (rp.getResource() instanceof ResourceItem) {
            // is resource items
            return true;
        }
        Resource resource = rp.getResource().getResource();
        if (resource.getItems().isEmpty()) {
            // resource has no items
            return true;
        }
        // resource has settings specified on resource level
        return settings.getPhysicalSettings(resource, rp.getPeripheral()) != null;
    }

    private MotionSettings getMotionSettings(HasResourcePeripheral rp, Axis axis) throws SpecificationException {
        return getMotionSettings(rp.getResource(), rp.getPeripheral(), axis);
    }

    private MotionSettings getMotionSettings(IResource resource, Peripheral peripheral, Axis axis)
            throws SpecificationException
    {
        ResourcePeripheralKey rp = new ResourcePeripheralKey(resource, peripheral);
        PhysicalSettings ps = settings.getPhysicalSettings(resource, peripheral);
        if (null == ps) {
            throw new SpecificationException(format("Physical settings not specified for peripheral %s", rp.fqn()),
                    settings);
        }
        MotionSettings ms = ps.getMotionSettings().get(axis);
        if (null == ms) {
            throw new SpecificationException(
                    format("Motion settings not specified for axis %s in peripheral %s", axis.getName(), rp.fqn()), ps);
        }
        return ms;
    }

    private void retainHead(Collection<?> collection) {
        Iterator<?> iterator = collection.iterator();
        if (iterator.hasNext()) {
            iterator.next();
        }
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }

    private boolean deepEquals(MotionProfileSettings s1, MotionProfileSettings s2) {
        if (!Objects.equals(s1.getMotionProfile(), s2.getMotionProfile())) {
            return false;
        }
        if (!s1.getMotionArguments().keySet().equals(s2.getMotionArguments().keySet())) {
            return false;
        }
        for (Map.Entry<String, Expression> entry: s1.getMotionArguments().entrySet()) {
            if (entry.getValue().evaluate().compareTo(s2.getMotionArguments().get(entry.getKey()).evaluate()) != 0) {
                return false;
            }
        }
        return true;
    }
}
