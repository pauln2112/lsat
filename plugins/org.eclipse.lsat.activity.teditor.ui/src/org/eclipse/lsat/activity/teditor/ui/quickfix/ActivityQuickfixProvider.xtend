/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/*
* generated by Xtext
*/
package org.eclipse.lsat.activity.teditor.ui.quickfix

import org.eclipse.xtext.diagnostics.Diagnostic
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import activity.ActivitySet
import activity.ActivityFactory
import org.eclipse.xtext.validation.Issue
import org.eclipse.xtext.ui.editor.quickfix.Fix
import activity.Activity

//import org.eclipse.xtext.ui.editor.quickfix.Fix
//import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
//import org.eclipse.xtext.validation.Issue

/**
 * Custom quickfixes.
 *
 * see http://www.eclipse.org/Xtext/documentation.html#quickfixes
 */
class ActivityQuickfixProvider extends org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider {

//	@Fix(MyDslValidator::INVALID_NAME)
//	def capitalizeName(Issue issue, IssueResolutionAcceptor acceptor) {
//		acceptor.accept(issue, 'Capitalize name', 'Capitalize the name.', 'upcase.png') [
//			context |
//			val xtextDocument = context.xtextDocument
//			val firstLetter = xtextDocument.get(issue.offset, 1)
//			xtextDocument.replace(issue.offset, 1, firstLetter.toUpperCase)
//		]
//	}


    @Fix(Diagnostic::LINKING_DIAGNOSTIC)
    def createEvent(Issue issue, IssueResolutionAcceptor acceptor) {
       acceptor.accept(issue,"Create event","Create event", "" ,
            [element, context | 
                val activity = element.eContainer as Activity
                val aSet = activity.eContainer() as ActivitySet
                val event = ActivityFactory.eINSTANCE.createEvent();
                val text = context.xtextDocument.get(issue.offset,issue.length)
                event.name = text;
                aSet.events += event
            ]
        );
    }


}
