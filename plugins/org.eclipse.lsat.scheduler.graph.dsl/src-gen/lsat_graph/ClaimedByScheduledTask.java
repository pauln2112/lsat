/**
 */
package lsat_graph;

import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Claimed By Scheduled Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.ClaimedByScheduledTask#getReleases <em>Releases</em>}</li>
 *   <li>{@link lsat_graph.ClaimedByScheduledTask#getClaims <em>Claims</em>}</li>
 * </ul>
 *
 * @see lsat_graph.lsat_graphPackage#getClaimedByScheduledTask()
 * @model
 * @generated
 */
public interface ClaimedByScheduledTask extends ScheduledTask<Task> {
	/**
     * Returns the value of the '<em><b>Releases</b></em>' reference list.
     * The list contents are of type {@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask}<code>&lt;org.eclipse.lsat.common.scheduler.graph.Task&gt;</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Releases</em>' reference list.
     * @see lsat_graph.lsat_graphPackage#getClaimedByScheduledTask_Releases()
     * @model
     * @generated
     */
    EList<ScheduledTask<Task>> getReleases();

    /**
     * Returns the value of the '<em><b>Claims</b></em>' reference list.
     * The list contents are of type {@link org.eclipse.lsat.common.scheduler.schedule.ScheduledTask}<code>&lt;org.eclipse.lsat.common.scheduler.graph.Task&gt;</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Claims</em>' reference list.
     * @see lsat_graph.lsat_graphPackage#getClaimedByScheduledTask_Claims()
     * @model required="true"
     * @generated
     */
    EList<ScheduledTask<Task>> getClaims();

} // ClaimedByScheduledTask
