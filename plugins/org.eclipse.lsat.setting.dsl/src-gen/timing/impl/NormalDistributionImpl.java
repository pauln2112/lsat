/**
 */
package timing.impl;

import expressions.Expression;
import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import timing.NormalDistribution;
import timing.TimingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Normal Distribution</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link timing.impl.NormalDistributionImpl#getMean <em>Mean</em>}</li>
 *   <li>{@link timing.impl.NormalDistributionImpl#getSd <em>Sd</em>}</li>
 *   <li>{@link timing.impl.NormalDistributionImpl#getMeanExp <em>Mean Exp</em>}</li>
 *   <li>{@link timing.impl.NormalDistributionImpl#getSdExp <em>Sd Exp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NormalDistributionImpl extends DistributionImpl implements NormalDistribution {
	/**
     * The default value of the '{@link #getMean() <em>Mean</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMean()
     * @generated
     * @ordered
     */
	protected static final BigDecimal MEAN_EDEFAULT = null;

	/**
     * The default value of the '{@link #getSd() <em>Sd</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSd()
     * @generated
     * @ordered
     */
	protected static final BigDecimal SD_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getMeanExp() <em>Mean Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMeanExp()
     * @generated
     * @ordered
     */
	protected Expression meanExp;

	/**
     * The cached value of the '{@link #getSdExp() <em>Sd Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSdExp()
     * @generated
     * @ordered
     */
	protected Expression sdExp;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected NormalDistributionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return TimingPackage.Literals.NORMAL_DISTRIBUTION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getMean() {
        return meanExp==null ? null:  meanExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getSd() {
        return sdExp==null ? null:  sdExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getMeanExp() {
        return meanExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetMeanExp(Expression newMeanExp, NotificationChain msgs) {
        Expression oldMeanExp = meanExp;
        meanExp = newMeanExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimingPackage.NORMAL_DISTRIBUTION__MEAN_EXP, oldMeanExp, newMeanExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMeanExp(Expression newMeanExp) {
        if (newMeanExp != meanExp)
        {
            NotificationChain msgs = null;
            if (meanExp != null)
                msgs = ((InternalEObject)meanExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimingPackage.NORMAL_DISTRIBUTION__MEAN_EXP, null, msgs);
            if (newMeanExp != null)
                msgs = ((InternalEObject)newMeanExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimingPackage.NORMAL_DISTRIBUTION__MEAN_EXP, null, msgs);
            msgs = basicSetMeanExp(newMeanExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, TimingPackage.NORMAL_DISTRIBUTION__MEAN_EXP, newMeanExp, newMeanExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getSdExp() {
        return sdExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetSdExp(Expression newSdExp, NotificationChain msgs) {
        Expression oldSdExp = sdExp;
        sdExp = newSdExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimingPackage.NORMAL_DISTRIBUTION__SD_EXP, oldSdExp, newSdExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setSdExp(Expression newSdExp) {
        if (newSdExp != sdExp)
        {
            NotificationChain msgs = null;
            if (sdExp != null)
                msgs = ((InternalEObject)sdExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimingPackage.NORMAL_DISTRIBUTION__SD_EXP, null, msgs);
            if (newSdExp != null)
                msgs = ((InternalEObject)newSdExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimingPackage.NORMAL_DISTRIBUTION__SD_EXP, null, msgs);
            msgs = basicSetSdExp(newSdExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, TimingPackage.NORMAL_DISTRIBUTION__SD_EXP, newSdExp, newSdExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case TimingPackage.NORMAL_DISTRIBUTION__MEAN_EXP:
                return basicSetMeanExp(null, msgs);
            case TimingPackage.NORMAL_DISTRIBUTION__SD_EXP:
                return basicSetSdExp(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case TimingPackage.NORMAL_DISTRIBUTION__MEAN:
                return getMean();
            case TimingPackage.NORMAL_DISTRIBUTION__SD:
                return getSd();
            case TimingPackage.NORMAL_DISTRIBUTION__MEAN_EXP:
                return getMeanExp();
            case TimingPackage.NORMAL_DISTRIBUTION__SD_EXP:
                return getSdExp();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case TimingPackage.NORMAL_DISTRIBUTION__MEAN_EXP:
                setMeanExp((Expression)newValue);
                return;
            case TimingPackage.NORMAL_DISTRIBUTION__SD_EXP:
                setSdExp((Expression)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case TimingPackage.NORMAL_DISTRIBUTION__MEAN_EXP:
                setMeanExp((Expression)null);
                return;
            case TimingPackage.NORMAL_DISTRIBUTION__SD_EXP:
                setSdExp((Expression)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case TimingPackage.NORMAL_DISTRIBUTION__MEAN:
                return MEAN_EDEFAULT == null ? getMean() != null : !MEAN_EDEFAULT.equals(getMean());
            case TimingPackage.NORMAL_DISTRIBUTION__SD:
                return SD_EDEFAULT == null ? getSd() != null : !SD_EDEFAULT.equals(getSd());
            case TimingPackage.NORMAL_DISTRIBUTION__MEAN_EXP:
                return meanExp != null;
            case TimingPackage.NORMAL_DISTRIBUTION__SD_EXP:
                return sdExp != null;
        }
        return super.eIsSet(featureID);
    }

} //NormalDistributionImpl
