/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend.annotations;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.function.Consumer;
import org.eclipse.lsat.common.util.BranchIterable;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.xtend.lib.macro.declaration.MethodDeclaration;
import org.eclipse.xtend.lib.macro.declaration.ParameterDeclaration;
import org.eclipse.xtend.lib.macro.declaration.ResolvedMethod;
import org.eclipse.xtend.lib.macro.declaration.TypeReference;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class TypeReferenceUtil {
  private static final Function2<TypeReference, TypeReference, Integer> TYPE_HIERARCHY_COMPARATOR = ((Function2<TypeReference, TypeReference, Integer>) (TypeReference t1, TypeReference t2) -> {
    final Object _switchValue = null;
    boolean _matched = false;
    boolean _equals = Objects.equal(t1, t2);
    if (_equals) {
      _matched=true;
      return 0;
    }
    if (!_matched) {
      boolean _isAssignableFrom = t1.isAssignableFrom(t2);
      if (_isAssignableFrom) {
        _matched=true;
        return 1;
      }
    }
    if (!_matched) {
      boolean _isAssignableFrom_1 = t2.isAssignableFrom(t1);
      if (_isAssignableFrom_1) {
        _matched=true;
        return (-1);
      }
    }
    return 0;
  });
  
  public static TreeSet<TypeReference> getCommonBaseTypes(final Iterable<TypeReference> typeReferences) {
    final TreeSet<TypeReference> commonTypeReferences = CollectionLiterals.<TypeReference>newTreeSet(new Comparator<TypeReference>() {
        public int compare(TypeReference o1, TypeReference o2) {
          return TypeReferenceUtil.TYPE_HIERARCHY_COMPARATOR.apply(o1, o2);
        }
    });
    boolean _isEmpty = IterableExtensions.isEmpty(typeReferences);
    boolean _not = (!_isEmpty);
    if (_not) {
      BranchIterable<TypeReference> _assignableTypeReferences = TypeReferenceUtil.getAssignableTypeReferences(IterableExtensions.<TypeReference>head(typeReferences));
      Iterables.<TypeReference>addAll(commonTypeReferences, _assignableTypeReferences);
      final Consumer<TypeReference> _function = (TypeReference it) -> {
        commonTypeReferences.retainAll(IterableExtensions.<TypeReference>toSet(TypeReferenceUtil.getAssignableTypeReferences(it)));
      };
      IterableExtensions.<TypeReference>drop(typeReferences, 1).forEach(_function);
    }
    return commonTypeReferences;
  }
  
  public static BranchIterable<TypeReference> getAssignableTypeReferences(final TypeReference typeReference) {
    final Function1<TypeReference, Iterable<? extends TypeReference>> _function = (TypeReference it) -> {
      return it.getDeclaredSuperTypes();
    };
    return Queries.<TypeReference>closure(Collections.<TypeReference>unmodifiableList(CollectionLiterals.<TypeReference>newArrayList(typeReference)), true, _function);
  }
  
  public static MethodDeclaration findResolvedMethod(final TypeReference typeReference, final String name, final TypeReference... parameterTypes) {
    final Function1<ResolvedMethod, MethodDeclaration> _function = (ResolvedMethod it) -> {
      return it.getDeclaration();
    };
    final Function1<MethodDeclaration, Boolean> _function_1 = (MethodDeclaration it) -> {
      String _simpleName = it.getSimpleName();
      return Boolean.valueOf(Objects.equal(_simpleName, name));
    };
    final Function1<MethodDeclaration, Boolean> _function_2 = (MethodDeclaration it) -> {
      final Function1<ParameterDeclaration, TypeReference> _function_3 = (ParameterDeclaration it_1) -> {
        return it_1.getType();
      };
      Iterable<TypeReference> _map = IterableExtensions.map(it.getParameters(), _function_3);
      return Boolean.valueOf(Objects.equal(_map, parameterTypes));
    };
    return IterableExtensions.<MethodDeclaration>findFirst(IterableExtensions.<MethodDeclaration>filter(IterableExtensions.map(typeReference.getAllResolvedMethods(), _function), _function_1), _function_2);
  }
}
