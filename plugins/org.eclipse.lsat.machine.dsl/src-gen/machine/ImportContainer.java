/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.ImportContainer#getImports <em>Imports</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getImportContainer()
 * @model abstract="true"
 * @generated
 */
public interface ImportContainer extends EObject {
	/**
     * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
     * The list contents are of type {@link machine.Import}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Imports</em>' containment reference list.
     * @see machine.MachinePackage#getImportContainer_Imports()
     * @model containment="true"
     * @generated
     */
	EList<Import> getImports();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * the transitive list of imported root objects. Parent first
     * <!-- end-model-doc -->
     * @model
     * @generated
     */
	EList<EObject> loadAll();

} // ImportContainer
