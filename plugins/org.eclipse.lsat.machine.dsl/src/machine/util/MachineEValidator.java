/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package machine.util;

import static java.lang.String.format;
import static org.eclipse.emf.common.util.Diagnostic.ERROR;
import static org.eclipse.emf.common.util.Diagnostic.WARNING;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.lsat.common.emf.common.util.BufferedDiagnosticChain;

import machine.Activator;
import machine.MachinePackage;
import machine.Path;
import machine.PathTargetReference;
import machine.Peripheral;
import machine.Profile;
import machine.SymbolicPosition;

/**
 * This validator validates whether:
 * <ul>
 * <li>Peripheral contains incompatible paths, see {@link #getIncompatibleTargetReferences(Path, Path)}</li>
 * </ul>
 */
public class MachineEValidator implements EValidator {
    public static final MachineEValidator INSTANCE = new MachineEValidator();

    @Override
    public boolean validate(EDataType eDataType, Object value, DiagnosticChain diagnostics,
            Map<Object, Object> context)
    {
        return true;
    }

    @Override
    public boolean validate(EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return null == eObject ? true : validate(eObject.eClass(), eObject, diagnostics, context);
    }

    @Override
    public boolean validate(EClass eClass, EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        BufferedDiagnosticChain bufferedDiagnostics = new BufferedDiagnosticChain(diagnostics);
        if (MachinePackage.PERIPHERAL == eClass.getClassifierID()) {
            validatePeripheral((Peripheral)eObject, bufferedDiagnostics);
        }
        return bufferedDiagnostics.getMaxSeverity() < WARNING;
    }

    private void validatePeripheral(Peripheral peripheral, DiagnosticChain diagnostics) {
        // Complex double loop to be as efficient as possible
        for (int i = 0; i < peripheral.getPaths().size(); i++) {
            Path p1 = peripheral.getPaths().get(i);
            for (int j = i + 1; j < peripheral.getPaths().size(); j++) {
                Path p2 = peripheral.getPaths().get(j);

                Set<String> incompatiblePositions = getIncompatibleTargetReferences(p1, p2);
                if (!incompatiblePositions.isEmpty()) {
                    writeDiagnostic(diagnostics, ERROR,
                            format("Path '%s' is incompatible with path '%s' as settling is different for locations %s",
                                    p1.getName(), p2.getName(), incompatiblePositions),
                            p1, p2);
                }
            }
        }
    }

    /**
     * If PathTargetReferences of p1 and p2 have different settling and share one or more common sources, then these
     * targets are incompatible. Resulting set will contain all the names of these incompatible targets.
     */
    private Set<String> getIncompatibleTargetReferences(Path p1, Path p2) {
        Set<Profile> profilesIntersection = new HashSet<Profile>(p1.getProfiles());
        profilesIntersection.retainAll(p2.getProfiles());
        if (profilesIntersection.isEmpty()) {
            // Paths do not share profiles, thus can never be incompatible
            return Collections.emptySet();
        }

        Set<SymbolicPosition> sourcesIntersection = new HashSet<>(p1.getSources());
        sourcesIntersection.retainAll(p2.getSources());
        if (sourcesIntersection.isEmpty()) {
            // Paths do not share sources, thus can never be incompatible
            return Collections.emptySet();
        }

        Set<String> result = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        Map<SymbolicPosition, PathTargetReference> t2 = indexPathTargetReferences(p2.getTargets());
        // for different value when key1 == key2 validate:
        // when sources p1 - key1 intersect with sources p2 - key2: fail
        for (PathTargetReference t1: p1.getTargets()) {
            SymbolicPosition pos = t1.getPosition();
            if (t2.containsKey(pos) && !equals(t1, t2.get(pos))) {
                // OK, so both paths go to position t1 with different settling, but do they also share the same sources?
                // The rule applies: if the sourcesIntersection excluding pos is not empty => fail
                // This is rewritten as: if size greater than 0 (if source not contains pos) or 1 (if source contains
                // pos)
                if (sourcesIntersection.size() > (sourcesIntersection.contains(pos) ? 1 : 0)) {
                    // Paths also share source, so they are incompatible
                    result.add(t1.getPosition().getName());
                }
            }
        }
        return result;
    }

    /**
     * Index the PathTargetReferences on their SymbolicPosition
     */
    private Map<SymbolicPosition, PathTargetReference> indexPathTargetReferences(EList<PathTargetReference> targets) {
        HashMap<SymbolicPosition, PathTargetReference> result = new HashMap<>(targets.size());
        for (PathTargetReference target: targets) {
            result.put(target.getPosition(), target);
        }
        return result;
    }

    private boolean equals(PathTargetReference t1, PathTargetReference t2) {
        if (t1 == t2)
            return true;
        if (null == t1 || null == t2)
            return false;
        return t1.getPosition().equals(t2.getPosition()) && t1.getSettling().equals(t2.getSettling());
    }

    private void writeDiagnostic(DiagnosticChain diagnostics, int severity, String message, EObject... data) {
        diagnostics.add(new BasicDiagnostic(severity, Activator.BUNDLE_NAME, 0, message, data));
    }
}
