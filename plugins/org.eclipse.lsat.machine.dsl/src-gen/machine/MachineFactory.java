/**
 */
package machine;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see machine.MachinePackage
 * @generated
 */
public interface MachineFactory extends EFactory {
	/**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	MachineFactory eINSTANCE = machine.impl.MachineFactoryImpl.init();

	/**
     * Returns a new object of class '<em>Peripheral Type</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Peripheral Type</em>'.
     * @generated
     */
	PeripheralType createPeripheralType();

	/**
     * Returns a new object of class '<em>Symbolic Position</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Symbolic Position</em>'.
     * @generated
     */
	SymbolicPosition createSymbolicPosition();

	/**
     * Returns a new object of class '<em>Resource</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Resource</em>'.
     * @generated
     */
	Resource createResource();

	/**
     * Returns a new object of class '<em>Action Type</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Action Type</em>'.
     * @generated
     */
	ActionType createActionType();

	/**
     * Returns a new object of class '<em>Peripheral</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Peripheral</em>'.
     * @generated
     */
	Peripheral createPeripheral();

	/**
     * Returns a new object of class '<em>Machine</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Machine</em>'.
     * @generated
     */
	Machine createMachine();

	/**
     * Returns a new object of class '<em>Import</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Import</em>'.
     * @generated
     */
	Import createImport();

	/**
     * Returns a new object of class '<em>Profile</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Profile</em>'.
     * @generated
     */
	Profile createProfile();

	/**
     * Returns a new object of class '<em>Axis</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Axis</em>'.
     * @generated
     */
	Axis createAxis();

	/**
     * Returns a new object of class '<em>Position</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Position</em>'.
     * @generated
     */
	Position createPosition();

	/**
     * Returns a new object of class '<em>Set Point</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Set Point</em>'.
     * @generated
     */
	SetPoint createSetPoint();

	/**
     * Returns a new object of class '<em>Path Target Reference</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Path Target Reference</em>'.
     * @generated
     */
	PathTargetReference createPathTargetReference();

	/**
     * Returns a new object of class '<em>Unidirectional Path</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Unidirectional Path</em>'.
     * @generated
     */
	UnidirectionalPath createUnidirectionalPath();

	/**
     * Returns a new object of class '<em>Bidirectional Path</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Bidirectional Path</em>'.
     * @generated
     */
	BidirectionalPath createBidirectionalPath();

	/**
     * Returns a new object of class '<em>Full Mesh Path</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Full Mesh Path</em>'.
     * @generated
     */
	FullMeshPath createFullMeshPath();

	/**
     * Returns a new object of class '<em>Path Annotation</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Path Annotation</em>'.
     * @generated
     */
	PathAnnotation createPathAnnotation();

	/**
     * Returns a new object of class '<em>Resource Item</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Resource Item</em>'.
     * @generated
     */
	ResourceItem createResourceItem();

	/**
     * Returns a new object of class '<em>Distance</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Distance</em>'.
     * @generated
     */
	Distance createDistance();

	/**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
	MachinePackage getMachinePackage();

} //MachineFactory
