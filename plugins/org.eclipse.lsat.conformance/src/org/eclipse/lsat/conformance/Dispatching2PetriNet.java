/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance;

import org.eclipse.lsat.common.qvto.util.AbstractModelTransformer;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.petri_net.PetriNet;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;

import dispatching.ActivityDispatching;

public class Dispatching2PetriNet extends AbstractModelTransformer<ActivityDispatching, PetriNet> {
    public Dispatching2PetriNet(boolean createLoop) {
        setConfigProperty("LOOP", createLoop);
    }

    @Override
    protected String getDefaultTransformation() {
        return "/transforms/dispatching2PetriNet.qvto";
    }

    @Override
    protected PetriNet doTransformModel(ActivityDispatching input) throws QvtoTransformationException {
        // Input
        BasicModelExtent inActivityDispatching = new BasicModelExtent();
        inActivityDispatching.add(input);

        // Output
        BasicModelExtent outPetriNet = new BasicModelExtent();

        execute(inActivityDispatching, outPetriNet);

        return validateOneAndOnlyOne(PetriNet.class, outPetriNet);
    }
}
