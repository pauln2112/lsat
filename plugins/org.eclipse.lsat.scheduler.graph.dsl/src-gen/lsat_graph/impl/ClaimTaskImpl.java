/**
 */
package lsat_graph.impl;

import activity.Claim;
import lsat_graph.ClaimTask;
import lsat_graph.lsat_graphPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Claim Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClaimTaskImpl extends ActionTaskImpl<Claim> implements ClaimTask {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ClaimTaskImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.CLAIM_TASK;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * This is specialized for the more specific type known in this context.
     * @generated
     */
	@Override
	public void setAction(Claim newAction) {
        super.setAction(newAction);
    }

} //ClaimTaskImpl
