/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import com.google.common.collect.Iterables;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import lsat_graph.ClaimTask;
import lsat_graph.EventAnnotation;
import lsat_graph.ReleaseTask;
import lsat_graph.lsat_graphFactory;
import machine.IResource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.lsat.common.graph.directed.Aspect;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;
import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.GraphFactory;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Removes the generated event resources and inserts a direct dependency between the actions
 */
@SuppressWarnings("all")
public class ShortcutClaimRelease<T extends Task> {
  private static final Logger LOGGER = LoggerFactory.getLogger(ShortcutClaimRelease.class);
  
  private ShortcutClaimRelease() {
  }
  
  public static <T extends Task> TaskDependencyGraph<T> transformModel(final TaskDependencyGraph<T> graph) {
    try {
      ShortcutClaimRelease.LOGGER.debug("Starting transformation");
      ShortcutClaimRelease.<T, Dependency>processGraph(graph);
      ShortcutClaimRelease.LOGGER.debug("Finished transformation");
      return graph;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  private static <T extends Task, E extends Edge> void processGraph(final DirectedGraph<T, E> graph) throws SpecificationException, MotionException {
    final Consumer<DirectedGraph<T, E>> _function = (DirectedGraph<T, E> it) -> {
      try {
        ShortcutClaimRelease.<T, E>processGraph(it);
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    };
    ((List<DirectedGraph<T, E>>)Conversions.doWrapArray(((DirectedGraph<T, E>[])Conversions.unwrapArray(graph.getSubGraphs(), DirectedGraph.class)).clone())).forEach(_function);
    final Consumer<T> _function_1 = (T it) -> {
      try {
        ShortcutClaimRelease.shortcutClaimRelease(it);
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    };
    ((List<T>)Conversions.doWrapArray(((T[])Conversions.unwrapArray(graph.getNodes(), Task.class)).clone())).forEach(_function_1);
  }
  
  private static void _shortcutClaimRelease(final Node node) {
  }
  
  private static void _shortcutClaimRelease(final ClaimTask claimTask) throws SpecificationException, MotionException {
    final ReleaseTask releaseTask = ShortcutClaimRelease.getCorrespondingReleaseTask(claimTask);
    if ((releaseTask != null)) {
      final Function1<Edge, Node> _function = (Edge it) -> {
        return it.getSourceNode();
      };
      final Consumer<Node> _function_1 = (Node s) -> {
        final Function1<Edge, Node> _function_2 = (Edge it) -> {
          return it.getTargetNode();
        };
        final Consumer<Node> _function_3 = (Node t) -> {
          ShortcutClaimRelease.<Task, Edge>addDependency(s, t, claimTask.getAction().getResource().getName());
        };
        ListExtensions.<Edge, Node>map(releaseTask.getOutgoingEdges(), _function_2).forEach(_function_3);
      };
      ListExtensions.<Edge, Node>map(claimTask.getIncomingEdges(), _function).forEach(_function_1);
      final Consumer<Edge> _function_2 = (Edge it) -> {
        ShortcutClaimRelease.delete(it);
      };
      ((List<Edge>)Conversions.doWrapArray(((Edge[])Conversions.unwrapArray(claimTask.getIncomingEdges(), Edge.class)).clone())).forEach(_function_2);
      final Consumer<Edge> _function_3 = (Edge it) -> {
        ShortcutClaimRelease.delete(it);
      };
      ((List<Edge>)Conversions.doWrapArray(((Edge[])Conversions.unwrapArray(claimTask.getOutgoingEdges(), Edge.class)).clone())).forEach(_function_3);
      final Consumer<Edge> _function_4 = (Edge it) -> {
        ShortcutClaimRelease.delete(it);
      };
      ((List<Edge>)Conversions.doWrapArray(((Edge[])Conversions.unwrapArray(releaseTask.getOutgoingEdges(), Edge.class)).clone())).forEach(_function_4);
      ShortcutClaimRelease.delete(claimTask);
      ShortcutClaimRelease.delete(releaseTask);
    }
  }
  
  private static ReleaseTask getCorrespondingReleaseTask(final ClaimTask claimTask) {
    int _size = claimTask.getOutgoingEdges().size();
    boolean _notEquals = (_size != 1);
    if (_notEquals) {
      return null;
    }
    final Function1<Edge, Node> _function = (Edge it) -> {
      return it.getTargetNode();
    };
    final Function1<ReleaseTask, Boolean> _function_1 = (ReleaseTask it) -> {
      IResource _resource = it.getAction().getResource();
      IResource _resource_1 = claimTask.getAction().getResource();
      return Boolean.valueOf((_resource == _resource_1));
    };
    return IterableExtensions.<ReleaseTask>findFirst(Iterables.<ReleaseTask>filter(ListExtensions.<Edge, Node>map(claimTask.getOutgoingEdges(), _function), ReleaseTask.class), _function_1);
  }
  
  private static void delete(final Edge edge) {
    edge.setTargetNode(null);
    edge.setSourceNode(null);
    edge.setGraph(null);
    EcoreUtil.delete(edge);
  }
  
  private static void delete(final Node node) {
    node.setGraph(null);
    EcoreUtil.delete(node);
  }
  
  private static <T extends Task, E extends Edge> void addDependency(final Node src, final Node target, final String eventName) {
    Dependency dep = GraphFactory.eINSTANCE.createDependency();
    EventAnnotation<Task> eventAnnotation = lsat_graphFactory.eINSTANCE.<Task>createEventAnnotation();
    eventAnnotation.setName(eventName);
    EList<Aspect<?, ?>> _aspects = dep.getAspects();
    _aspects.add(eventAnnotation);
    dep.setGraph(src.getGraph());
    dep.setSourceNode(src);
    dep.setTargetNode(target);
  }
  
  private static void shortcutClaimRelease(final Node claimTask) throws SpecificationException, MotionException {
    if (claimTask instanceof ClaimTask) {
      _shortcutClaimRelease((ClaimTask)claimTask);
      return;
    } else if (claimTask != null) {
      _shortcutClaimRelease(claimTask);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(claimTask).toString());
    }
  }
}
