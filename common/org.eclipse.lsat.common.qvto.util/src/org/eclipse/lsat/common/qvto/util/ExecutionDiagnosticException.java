/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util;

import java.util.List;

import org.eclipse.lsat.common.emf.common.util.DiagnosticException;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ExecutionStackTraceElement;

public class ExecutionDiagnosticException extends DiagnosticException {
    private static final long serialVersionUID = 1L;

    public ExecutionDiagnosticException(ExecutionDiagnostic execDiagnostic) {
        super(execDiagnostic);

        List<ExecutionStackTraceElement> diagTrace = execDiagnostic.getStackTrace();
        StackTraceElement[] stackTrace = new StackTraceElement[diagTrace.size()];
        for (int i = 0; i < diagTrace.size(); i++) {
            ExecutionStackTraceElement exElem = diagTrace.get(i);
            stackTrace[i] = new StackTraceElement(exElem.getModuleName(), exElem.getOperationName(),
                    exElem.getUnitName(), exElem.getLineNumber());
        }
        setStackTrace(stackTrace);
    }
}
