/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors;

import activity.Activity;
import activity.Claim;
import activity.PeripheralAction;
import activity.Release;
import activity.ResourceAction;
import com.google.common.base.Objects;
import java.util.Set;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.lsat.common.graph.directed.editable.EdgQueries;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.lsat.resource_throughput.ui.editors.ResourceThroughputEditingDomain;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class ResourceThroughputTableContentProvider implements IStructuredContentProvider {
  private final String resourceFqn;
  
  public ResourceThroughputTableContentProvider(final String resourceFqn) {
    this.resourceFqn = resourceFqn;
  }
  
  @Override
  public Object[] getElements(final Object editingDomain) {
    if ((editingDomain instanceof ResourceThroughputEditingDomain)) {
      final Function1<Activity, Iterable<Node>> _function = (Activity it) -> {
        return ResourceThroughputTableContentProvider.getResourceActions(it, this.resourceFqn);
      };
      return ((Object[])Conversions.unwrapArray(Queries.<PeripheralAction>objectsOfKind(Queries.<Activity, Node>collect(ResourceThroughputTableContentProvider.getResourceActivities(((ResourceThroughputEditingDomain)editingDomain).getActivities(), this.resourceFqn), _function), PeripheralAction.class), Object.class));
    } else {
      throw new IllegalArgumentException();
    }
  }
  
  /**
   * Returns all Activities in which resource participates
   */
  public static Iterable<Activity> getResourceActivities(final Iterable<Activity> activities, final String resourceFqn) {
    final Function1<Activity, Boolean> _function = (Activity it) -> {
      final Function1<ResourceAction, Boolean> _function_1 = (ResourceAction it_1) -> {
        String _fqn = it_1.getResource().fqn();
        return Boolean.valueOf(Objects.equal(_fqn, resourceFqn));
      };
      return Boolean.valueOf(IterableExtensions.<ResourceAction>exists(Queries.<ResourceAction>objectsOfKind(it.getNodes(), ResourceAction.class), _function_1));
    };
    return Queries.<Activity>select(activities, _function);
  }
  
  /**
   * The nodes which are applicable are the ones that are both in the closure of
   * <ul><li>outgoing edges of the claim</li>
   * <li>incoming edges of the release</li></ul>
   */
  public static Iterable<Node> getResourceActions(final Activity activity, final String resourceFqn) {
    final Function1<Claim, Boolean> _function = (Claim it) -> {
      String _fqn = it.getResource().fqn();
      return Boolean.valueOf(Objects.equal(_fqn, resourceFqn));
    };
    final Iterable<Claim> claim = Queries.<Claim>select(Queries.<Claim>objectsOfKind(activity.getNodes(), Claim.class), _function);
    final Function1<Node, Iterable<? extends Node>> _function_1 = (Node n) -> {
      final Function1<Edge, Node> _function_2 = (Edge it) -> {
        return it.getTargetNode();
      };
      return Queries.<Edge, Node>collectOne(n.getOutgoingEdges(), _function_2);
    };
    final Set<Node> claimOutgoingNodes = IterableExtensions.<Node>toSet(Queries.<Node>closure(claim, _function_1));
    final Function1<Release, Boolean> _function_2 = (Release it) -> {
      String _fqn = it.getResource().fqn();
      return Boolean.valueOf(Objects.equal(_fqn, resourceFqn));
    };
    final Iterable<Release> release = Queries.<Release>select(Queries.<Release>objectsOfKind(activity.getNodes(), Release.class), _function_2);
    final Function1<Node, Iterable<? extends Node>> _function_3 = (Node n) -> {
      final Function1<Edge, Node> _function_4 = (Edge it) -> {
        return it.getSourceNode();
      };
      return Queries.<Edge, Node>collectOne(n.getIncomingEdges(), _function_4);
    };
    final Set<Node> releaseIncomingNodes = IterableExtensions.<Node>toSet(Queries.<Node>closure(release, _function_3));
    final Function1<Node, Boolean> _function_4 = (Node it) -> {
      return Boolean.valueOf((claimOutgoingNodes.contains(it) && releaseIncomingNodes.contains(it)));
    };
    return Queries.<Node>select(EdgQueries.<Node>topologicalOrdering(activity.getNodes()), _function_4);
  }
}
