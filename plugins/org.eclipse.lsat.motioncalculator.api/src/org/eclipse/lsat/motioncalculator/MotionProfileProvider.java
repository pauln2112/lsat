/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator;

import java.util.Set;

public interface MotionProfileProvider {
    /**
     * Provides the supported profiles and its parameters.
     *
     * <p>
     * MotionCalculator providers that also implement this API can expect that only supported profiles will be used
     * </p>
     * Example of a profile: <pre>
     *   {@link MotionProfile}
     *           key="ThirdOrderP2P"
     *           name="Third order point-to-point"
     *           url="optional url to online specification"
     *   {@link MotionProfileParameter} key="V" name="V" required="true"
     *   {@link MotionProfileParameter} key="A" name="A" required="true"
     *   {@link MotionProfileParameter} key="J" name="J" required="true"
     *   {@link MotionProfileParameter} key="S" name="S" required="false"
     * </pre>
     *
     * @return The supported profiles for an implementation
     * @throws MotionException
     */
    Set<MotionProfile> getSupportedProfiles() throws MotionException;
}
