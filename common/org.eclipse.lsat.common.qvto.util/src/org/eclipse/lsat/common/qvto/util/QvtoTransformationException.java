/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util;

public class QvtoTransformationException extends Exception {
    private static final long serialVersionUID = 1L;

    public QvtoTransformationException(String aDeveloperMessage) {
        super(aDeveloperMessage);
    }

    public QvtoTransformationException(Throwable aCause) {
        this(aCause.getMessage(), aCause);
    }

    public QvtoTransformationException(String message, Throwable cause) {
        super(message, cause);
    }
}
