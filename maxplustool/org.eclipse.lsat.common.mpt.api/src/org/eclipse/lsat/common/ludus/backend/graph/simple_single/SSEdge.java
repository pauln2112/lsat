/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.graph.simple_single;

/**
 * @author Bram van der Sanden
 */
public class SSEdge {
    Double w;

    SSVertex src;

    SSVertex tgt;

    public SSEdge(SSVertex source, SSVertex target, Double weight) {
        this.w = weight;
        this.src = source;
        this.tgt = target;
    }

    public Double getWeight() {
        return w;
    }

    public SSVertex getSource() {
        return src;
    }

    public SSVertex getTarget() {
        return tgt;
    }
}
