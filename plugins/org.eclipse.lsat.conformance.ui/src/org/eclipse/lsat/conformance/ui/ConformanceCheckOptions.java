/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.lsat.conformance.ConformanceCheckInput;
import org.eclipse.lsat.conformance.ConformanceCheckInput.Level;

public class ConformanceCheckOptions {
    private final List<IFile> traceFiles = new ArrayList<>();

    private final IFile modelFile;

    private final IWorkspace workspace;

    private boolean dipatchingLoop = false;

    private ConformanceCheckInput.Level conformanceLevel = Level.Claim;

    public ConformanceCheckOptions(IFile modelFile, IWorkspace workspace) {
        this.modelFile = modelFile;
        this.workspace = workspace;
    }

    public IFile getModelFile() {
        return modelFile;
    }

    public IWorkspace getWorkspace() {
        return workspace;
    }

    public void setDipatchingLoop(boolean dipatchingLoop) {
        this.dipatchingLoop = dipatchingLoop;
    }

    public boolean isDipatchingLoop() {
        return dipatchingLoop;
    }

    public void setConformanceLevel(ConformanceCheckInput.Level conformanceLevel) {
        this.conformanceLevel = conformanceLevel;
    }

    public ConformanceCheckInput.Level getConformanceLevel() {
        return conformanceLevel;
    }

    public void addTraceFile(IFile traceFile) {
        this.traceFiles.add(traceFile);
    }

    public List<IFile> getTraceFiles() {
        return traceFiles;
    }
}
