/**
 */
package activity;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see activity.ActivityPackage
 * @generated
 */
public interface ActivityFactory extends EFactory {
	/**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	ActivityFactory eINSTANCE = activity.impl.ActivityFactoryImpl.init();

	/**
     * Returns a new object of class '<em>Set</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Set</em>'.
     * @generated
     */
	ActivitySet createActivitySet();

	/**
     * Returns a new object of class '<em>Activity</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Activity</em>'.
     * @generated
     */
	Activity createActivity();

	/**
     * Returns a new object of class '<em>Claim</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Claim</em>'.
     * @generated
     */
	Claim createClaim();

	/**
     * Returns a new object of class '<em>Release</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Release</em>'.
     * @generated
     */
	Release createRelease();

	/**
     * Returns a new object of class '<em>Location Prerequisite</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Location Prerequisite</em>'.
     * @generated
     */
	LocationPrerequisite createLocationPrerequisite();

	/**
     * Returns a new object of class '<em>Trace Point</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Trace Point</em>'.
     * @generated
     */
	TracePoint createTracePoint();

	/**
     * Returns a new object of class '<em>Sync Bar</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Sync Bar</em>'.
     * @generated
     */
	SyncBar createSyncBar();

	/**
     * Returns a new object of class '<em>Simple Action</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Simple Action</em>'.
     * @generated
     */
	SimpleAction createSimpleAction();

	/**
     * Returns a new object of class '<em>Move</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Move</em>'.
     * @generated
     */
	Move createMove();

	/**
     * Returns a new object of class '<em>Raise Event</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Raise Event</em>'.
     * @generated
     */
	RaiseEvent createRaiseEvent();

	/**
     * Returns a new object of class '<em>Require Event</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Require Event</em>'.
     * @generated
     */
	RequireEvent createRequireEvent();

	/**
     * Returns a new object of class '<em>Event</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Event</em>'.
     * @generated
     */
    Event createEvent();

    /**
     * Returns a new object of class '<em>Event Item</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Event Item</em>'.
     * @generated
     */
    EventItem createEventItem();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
	ActivityPackage getActivityPackage();

} //ActivityFactory
