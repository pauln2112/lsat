/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.layout;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.elk.conn.gmf.GmfDiagramLayoutConnector;
import org.eclipse.elk.core.service.LayoutMapping;
import org.eclipse.elk.graph.ElkGraphElement;
import org.eclipse.elk.graph.properties.IPropertyHolder;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.ZOrderRequest;
import org.eclipse.lsat.common.graph.directed.editable.EdgQueries;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.util.IterableUtil;
import org.eclipse.sirius.viewpoint.DSemanticDecorator;
import org.eclipse.ui.IWorkbenchPart;

import com.google.common.collect.Lists;
import com.google.inject.Singleton;

import activity.Activity;
import activity.ActivityPackage;
import activity.Claim;
import activity.EventAction;
import activity.PeripheralAction;
import activity.RequireEvent;
import activity.SchedulingType;
import activity.SyncBar;
import machine.IResource;
import machine.Peripheral;

@Singleton
public class ActivityDiagramLayoutConnector extends GmfDiagramLayoutConnector {
    @Override
    public void applyLayout(LayoutMapping mapping, IPropertyHolder settings) {
        // Move syncbars to front
        Map<EditPart, List<EditPart>> toFrontRequests = new HashMap<>();
        for (Map.Entry<ElkGraphElement, Object> entry: mapping.getGraphMap().entrySet()) {
            if (ActivityPackage.Literals.SYNC_BAR
                    .equals(entry.getKey().getProperty(ActivityDiagramLayoutOptions.E_CLASS)))
            {
                IGraphicalEditPart editPart = (IGraphicalEditPart)entry.getValue();

                List<EditPart> partsToOrder = toFrontRequests.computeIfAbsent(editPart.getParent(),
                        e -> new ArrayList<>());
                partsToOrder.add(editPart);
            }
        }
        for (Map.Entry<EditPart, List<EditPart>> entry: toFrontRequests.entrySet()) {
            ZOrderRequest request = new ZOrderRequest(ZOrderRequest.REQ_BRING_TO_FRONT);
            request.setPartsToOrder(entry.getValue());
            entry.getKey().performRequest(request);
        }

        super.applyLayout(mapping, settings);
    }

    @Override
    protected LayoutMapping buildLayoutGraph(IGraphicalEditPart layoutRootPart, List<ShapeNodeEditPart> selection,
            IWorkbenchPart workbenchPart)
    {
        LayoutMapping layoutMapping = super.buildLayoutGraph(layoutRootPart, selection, workbenchPart);

        EObject modelElement = getModelElement(layoutRootPart);
        if (modelElement instanceof Activity) {
            addNodeProperties((Activity)modelElement, layoutMapping);
        }

        return layoutMapping;
    }

    private void addNodeProperties(Activity activity, LayoutMapping layoutMapping) {
        HashMap<Node, Integer> layoutRows = determineNodeLayoutRows(activity);

        // Iterate of the elements of the layoutMapping and add relevant
        // properties
        for (Map.Entry<ElkGraphElement, Object> entry: layoutMapping.getGraphMap().entrySet()) {
            // get the ECore node
            EObject modelElement = getModelElement((IGraphicalEditPart)entry.getValue());
            // get its layoutData in the KGraph
            ElkGraphElement elkGraphElement = entry.getKey();

            if (modelElement != null) {
                // Add the eClass of the node
                elkGraphElement.setProperty(ActivityDiagramLayoutOptions.E_CLASS, modelElement.eClass());

                // Add the name, convenient for debugging the algorithm
                if (modelElement instanceof Node) {
                    elkGraphElement.setProperty(ActivityDiagramLayoutOptions.NAME, ((Node)modelElement).getName());
                } else if (modelElement instanceof IResource) {
                    elkGraphElement.setProperty(ActivityDiagramLayoutOptions.NAME, ((IResource)modelElement).getName());
                } else if (modelElement instanceof Peripheral) {
                    elkGraphElement.setProperty(ActivityDiagramLayoutOptions.NAME,
                            ((Peripheral)modelElement).getName());
                }

                // Add the name, convenient for debugging the algorithm
                if (modelElement instanceof EventAction) {
                    elkGraphElement.setProperty(ActivityDiagramLayoutOptions.EVENT_NAME,
                            ((EventAction)modelElement).getResource().fqn());
                }

                // Add layout row
                if (modelElement instanceof Node && layoutRows.containsKey(modelElement)) {
                    int layoutRow = layoutRows.get(modelElement);
                    elkGraphElement.setProperty(ActivityDiagramLayoutOptions.LAYOUT_ROW, layoutRow);
                } else {
                    elkGraphElement.setProperty(ActivityDiagramLayoutOptions.LAYOUT_ROW, -1);
                }
            }
        }
    }

    private HashMap<Node, Integer> determineNodeLayoutRows(Activity activity) {
        EList<Node> topologicalSortedNodes = EdgQueries.topologicalOrdering(activity.getNodes(),
                Comparator.comparing(ActivityDiagramLayoutConnector::isALAP).thenComparing(Node::getName));
        int maxRow = 0;
        HashMap<Node, Integer> layoutRows = new HashMap<Node, Integer>();
        for (Node node: topologicalSortedNodes) {
            int row;
            if (node instanceof SyncBar) {
                // Put all syncbars in their own row
                row = maxRow + 1;
            } else {
                row = getMaxParentRow(node, layoutRows) + 1;
                if (isALAP(node)) {
                    row = Math.max(row, maxRow);
                }
                // Put all syncbars in their own row
                while (rowContainsSyncbar(row, layoutRows)) {
                    row++;
                }
            }
            layoutRows.put(node, row);
            maxRow = Math.max(maxRow, row);
        }
        // Enhance layout for ALAP (as late as possible) nodes
        Lists.reverse(topologicalSortedNodes);
        for (Node node: topologicalSortedNodes) {
            if (isALAP(node) && !(node instanceof SyncBar)) {
                int row = getMinLeafRow(node, layoutRows) - 1;
                while (rowContainsSyncbar(row, layoutRows)) {
                    row--;
                }
                layoutRows.put(node, row);
            }
        }
        return layoutRows;
    }

    private static boolean isALAP(Node node) {
        if (node instanceof PeripheralAction) {
            return ((PeripheralAction)node).getSchedulingType() == SchedulingType.ALAP;
        } else if (node instanceof SyncBar) {
            return node.getIncomingEdges().size() > 1;
        }
        return node instanceof Claim || node instanceof RequireEvent;
    }

    private static EObject getModelElement(IGraphicalEditPart editPart) {
        EObject semanticDecorator = editPart.resolveSemanticElement();
        return semanticDecorator instanceof DSemanticDecorator ? ((DSemanticDecorator)semanticDecorator).getTarget()
                : null;
    }

    private static int getMaxParentRow(Node node, HashMap<Node, Integer> layoutRows) {
        return IterableUtil
                .max(from(node.getIncomingEdges()).xcollectOne(Edge::getSourceNode).xcollectOne(layoutRows::get), -1);
    }

    private static int getMinLeafRow(Node node, HashMap<Node, Integer> layoutRows) {
        final int maxRow = IterableUtil.max(layoutRows.values(), -1);
        return IterableUtil.min(
                from(node.getOutgoingEdges()).xcollectOne(Edge::getTargetNode).xcollectOne(layoutRows::get), maxRow);
    }

    private static boolean rowContainsSyncbar(int row, HashMap<Node, Integer> layoutRows) {
        return from(layoutRows.entrySet()).select(e -> e.getValue() == row).exists(e -> e.getKey() instanceof SyncBar);
    }
}
