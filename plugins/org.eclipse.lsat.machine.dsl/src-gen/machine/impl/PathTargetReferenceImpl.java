/**
 */
package machine.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import machine.MachinePackage;
import machine.PathTargetReference;
import machine.SymbolicPosition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Path Target Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.PathTargetReferenceImpl#getName <em>Name</em>}</li>
 *   <li>{@link machine.impl.PathTargetReferenceImpl#getPosition <em>Position</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PathTargetReferenceImpl extends HasSettlingImpl implements PathTargetReference {
	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getPosition() <em>Position</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPosition()
     * @generated
     * @ordered
     */
	protected SymbolicPosition position;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected PathTargetReferenceImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.PATH_TARGET_REFERENCE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
        return null == position ? null : position.getName();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SymbolicPosition getPosition() {
        return position;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetPosition(SymbolicPosition newPosition, NotificationChain msgs) {
        SymbolicPosition oldPosition = position;
        position = newPosition;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MachinePackage.PATH_TARGET_REFERENCE__POSITION, oldPosition, newPosition);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPosition(SymbolicPosition newPosition) {
        if (newPosition != position)
        {
            NotificationChain msgs = null;
            if (position != null)
                msgs = ((InternalEObject)position).eInverseRemove(this, MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES, SymbolicPosition.class, msgs);
            if (newPosition != null)
                msgs = ((InternalEObject)newPosition).eInverseAdd(this, MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES, SymbolicPosition.class, msgs);
            msgs = basicSetPosition(newPosition, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.PATH_TARGET_REFERENCE__POSITION, newPosition, newPosition));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.PATH_TARGET_REFERENCE__POSITION:
                if (position != null)
                    msgs = ((InternalEObject)position).eInverseRemove(this, MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES, SymbolicPosition.class, msgs);
                return basicSetPosition((SymbolicPosition)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.PATH_TARGET_REFERENCE__POSITION:
                return basicSetPosition(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.PATH_TARGET_REFERENCE__NAME:
                return getName();
            case MachinePackage.PATH_TARGET_REFERENCE__POSITION:
                return getPosition();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.PATH_TARGET_REFERENCE__POSITION:
                setPosition((SymbolicPosition)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.PATH_TARGET_REFERENCE__POSITION:
                setPosition((SymbolicPosition)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.PATH_TARGET_REFERENCE__NAME:
                return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
            case MachinePackage.PATH_TARGET_REFERENCE__POSITION:
                return position != null;
        }
        return super.eIsSet(featureID);
    }

} //PathTargetReferenceImpl
