/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor.conversion;

import org.eclipse.lsat.dispatching.teditor.conversion.IDStringValueConverter;
import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;

@SuppressWarnings("all")
public class DispatchingXtextValueConverterService extends DefaultTerminalConverters {
  @ValueConverter(rule = "IDString")
  public IValueConverter<String> getIDStringConverter() {
    IValueConverter<String> _ID = this.ID();
    IValueConverter<String> _STRING = this.STRING();
    return new IDStringValueConverter(_ID, _STRING);
  }
}
