/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.petri_net.design;

import static java.lang.String.format;

import org.eclipse.lsat.petri_net.Place;
import org.eclipse.lsat.petri_net.Transition;
import org.eclipse.lsat.trace.TraceLine;

import activity.Action;
import activity.Claim;
import activity.Move;
import activity.Release;
import activity.SimpleAction;

public class PetriNetService {
    public String getTooltip(Place place) {
        final Action action = place.getAction();
        String tooltip = (null == place.getName()) ? "" : place.getName();
        if (action instanceof Claim) {
            tooltip += format(": claim %s", ((Claim)action).getResource().getName());
        } else if (action instanceof Release) {
            tooltip += format(": release %s", ((Release)action).getResource().getName());
        } else if (action instanceof SimpleAction) {
            SimpleAction pa = (SimpleAction)place.getAction();
            tooltip += format(": %s.%s.%s", pa.getResource().fqn(), pa.getPeripheral().getName(),
                    pa.getType().getName());
        } else if (action instanceof Move) {
            Move move = (Move)action;
            if (move.isPositionMove()) {
                tooltip += format(": move %s.%s to %s using speed profile %s", move.getResource().fqn(),
                        move.getPeripheral().getName(), move.getTargetPosition().getName(),
                        move.getProfile().getName());
            } else {
                tooltip += format(": move %s.%s for %s using speed profile %s", move.getResource().fqn(),
                        move.getPeripheral().getName(), move.getDistance().getName(), move.getProfile().getName());
            }
        }
        return tooltip.isEmpty() ? null : tooltip;
    }

    public String getTooltip(Transition transition) {
        String tooltip = "";
        for (TraceLine tl: transition.getTraceLines()) {
            if (!tooltip.isEmpty()) {
                tooltip += '\n';
            }
            tooltip += format("%s, line %s: %s", tl.getModel().getName(), tl.getLineNumber(), tl.getTracePoint());
        }
        return tooltip.isEmpty() ? null : tooltip;
    }
}
