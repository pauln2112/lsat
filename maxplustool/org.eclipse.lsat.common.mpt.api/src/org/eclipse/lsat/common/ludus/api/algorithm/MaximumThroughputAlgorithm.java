/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.api.algorithm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.lsat.common.ludus.api.MaxPlusException;
import org.eclipse.lsat.common.ludus.api.MaximumThroughputResult;
import org.eclipse.lsat.common.ludus.backend.algebra.Matrix;
import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.algorithms.Dijkstra;
import org.eclipse.lsat.common.ludus.backend.algorithms.Howard;
import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.fsm.FSM;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;
import org.eclipse.lsat.common.ludus.backend.statespace.ComputeStateSpace;
import org.eclipse.lsat.common.ludus.backend.statespace.Configuration;
import org.eclipse.lsat.common.ludus.backend.statespace.MaxPlusStateSpace;
import org.eclipse.lsat.common.ludus.backend.statespace.Transition;
import org.eclipse.lsat.common.mpt.api.NotAllResourcesLinkedException;
import org.eclipse.lsat.common.mpt.api.UnconnectedResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Compute the maximum (best-case) throughput using Howard's minimum cycle ratio algorithm on a max-plus state space
 * where the activity rewards and activity timings are swapped.
 *
 */
@SuppressWarnings("rawtypes")
public class MaximumThroughputAlgorithm extends MaxPlusAlgorithm {
    private static final Logger LOGGER = LoggerFactory.getLogger(MaximumThroughputAlgorithm.class);

    public static MaximumThroughputResult run(FSM<Location, Edge> fsm, Map<String, Matrix> matrixMap)
            throws MaxPlusException, NotAllResourcesLinkedException, UnconnectedResourceException
    {
        // Check pre-conditions.
        checkCyclic(fsm);
        checkNoDeadlocks(fsm);
        checkAllMatricesSameSize(matrixMap.values());
        checkEventMapping(fsm, matrixMap);
        // checkResourcesConnected(fsm,matrixMap);

        // Number of resources.
        Integer resourceCount = matrixMap.values().iterator().next().getRows();

        long startTime, endTime;

        // Compute the max-plus state space.
        startTime = System.currentTimeMillis();
        MaxPlusStateSpace stateSpaceOriginal = ComputeStateSpace.computeMaxPlusStateSpace(fsm, resourceCount,
                matrixMap);
        endTime = System.currentTimeMillis();

        // Swap the reward and duration weights.
        MaxPlusStateSpace stateSpace = ComputeStateSpace.swapWeights(stateSpaceOriginal);

        LOGGER.info("Max-plus state space constructed: " + stateSpace.getVertices().size() + " states and "
                + stateSpace.getEdges().size() + " edges. Generation took " + (endTime - startTime) + " ms.");

        // Perform the minimum cycle mean computation on the strongly connected
        // components.
        startTime = System.currentTimeMillis();
        List<MaxPlusStateSpace> mpsSCCs = ComputeStateSpace.getSCCs(stateSpace);
        endTime = System.currentTimeMillis();

        LOGGER.info(
                "Found " + mpsSCCs.size() + " strongly connected component(s) in " + (endTime - startTime) + " ms.");

        startTime = System.currentTimeMillis();
        int i = 1;
        Tuple<Value, List<Transition>> howardResult = Tuple.of(Value.POSITIVE_INFINITY, new LinkedList<Transition>());
        for (MaxPlusStateSpace mpsSCC: mpsSCCs) {
            Tuple<Value, List<Transition>> sccResult = Howard.runHoward(mpsSCC);
            LOGGER.info("Running Howard on component " + i);
            if (sccResult.getRight() == null) {
                throw new MaxPlusException("Throughput for component " + i + " cannot be determined "
                        + "due to floating-point precision issues.");
            }
            if (sccResult.getLeft().smallerThan(howardResult.getLeft())) {
                howardResult = sccResult;
            }
            i++;
        }
        endTime = System.currentTimeMillis();

        Double throughput = (new Value(1.0d)).divide(howardResult.getLeft()).toDouble();

        LOGGER.info("Maximum throughput computed using Howards's MCR algorithm in " + (endTime - startTime) + " ms.");

        // Compute transient event sequence leading to the steady state (repeatable) event sequence.
        List<Configuration> cycleConfigurations = howardResult.getRight().stream().map(t -> t.getSource())
                .collect(Collectors.toList());
        Tuple<Value, List<Transition>> dijkstraResult = Dijkstra.runDijkstra(stateSpace,
                stateSpace.getInitialConfiguration(), cycleConfigurations);

        // Create the output.
        if (!howardResult.getLeft().equals(Value.NEGATIVE_INFINITY)) {
            // Find the starting point (configuration) on the cycle and create the permutation of the cycle
            // starting from this configuration. If there is no startup behavior, Dijkstra yields an empty list,
            // and we can return only the steady-state cycle.

            List<String> listOfEventNamesSteadyState;
            List<String> listOfEventNamesTransientState;

            if (!dijkstraResult.getRight().isEmpty()) {
                Configuration startingConfigurationOnCycle = dijkstraResult.getRight()
                        .get(dijkstraResult.getRight().size() - 1).getTarget();
                int cycleIndex = -1;

                for (int index = 0; index < howardResult.getRight().size(); index++) {
                    if (howardResult.getRight().get(index).getSource().equals(startingConfigurationOnCycle)) {
                        cycleIndex = index;
                        break;
                    }
                }

                List<Transition> cyclePermutation = new ArrayList<>();
                cyclePermutation.addAll(howardResult.getRight().subList(cycleIndex, howardResult.getRight().size()));
                cyclePermutation.addAll(howardResult.getRight().subList(0, cycleIndex));

                listOfEventNamesSteadyState = cyclePermutation.stream().map(Transition::getEvent)
                        .collect(Collectors.toList());

                listOfEventNamesTransientState = dijkstraResult.getRight().stream().map(Transition::getEvent)
                        .collect(Collectors.toList());
            } else {
                // No startup sequence, only steady state.
                listOfEventNamesSteadyState = howardResult.getRight().stream().map(Transition::getEvent)
                        .collect(Collectors.toList());
                listOfEventNamesTransientState = new ArrayList<>();
            }

            return new MaximumThroughputResult(throughput, listOfEventNamesSteadyState, listOfEventNamesTransientState);
        } else {
            return new MaximumThroughputResult(throughput);
        }
    }
}
