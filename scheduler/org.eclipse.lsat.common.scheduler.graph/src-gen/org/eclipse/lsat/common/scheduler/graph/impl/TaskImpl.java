/**
 */
package org.eclipse.lsat.common.scheduler.graph.impl;

import org.eclipse.lsat.common.graph.directed.impl.NodeImpl;

import org.eclipse.lsat.common.scheduler.graph.GraphPackage;
import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.resources.Resource;

import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.graph.impl.TaskImpl#getExecutionTime <em>Execution Time</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.graph.impl.TaskImpl#getResources <em>Resources</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskImpl extends NodeImpl implements Task {
	/**
     * The default value of the '{@link #getExecutionTime() <em>Execution Time</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getExecutionTime()
     * @generated
     * @ordered
     */
	protected static final BigDecimal EXECUTION_TIME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getExecutionTime() <em>Execution Time</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getExecutionTime()
     * @generated
     * @ordered
     */
	protected BigDecimal executionTime = EXECUTION_TIME_EDEFAULT;

	/**
     * The cached value of the '{@link #getResources() <em>Resources</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getResources()
     * @generated
     * @ordered
     */
	protected EList<Resource> resources;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected TaskImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return GraphPackage.Literals.TASK;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getExecutionTime() {
        return executionTime;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setExecutionTime(BigDecimal newExecutionTime) {
        BigDecimal oldExecutionTime = executionTime;
        executionTime = newExecutionTime;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, GraphPackage.TASK__EXECUTION_TIME, oldExecutionTime, executionTime));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Resource> getResources() {
        if (resources == null)
        {
            resources = new EObjectResolvingEList<Resource>(Resource.class, this, GraphPackage.TASK__RESOURCES);
        }
        return resources;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case GraphPackage.TASK__EXECUTION_TIME:
                return getExecutionTime();
            case GraphPackage.TASK__RESOURCES:
                return getResources();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case GraphPackage.TASK__EXECUTION_TIME:
                setExecutionTime((BigDecimal)newValue);
                return;
            case GraphPackage.TASK__RESOURCES:
                getResources().clear();
                getResources().addAll((Collection<? extends Resource>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case GraphPackage.TASK__EXECUTION_TIME:
                setExecutionTime(EXECUTION_TIME_EDEFAULT);
                return;
            case GraphPackage.TASK__RESOURCES:
                getResources().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case GraphPackage.TASK__EXECUTION_TIME:
                return EXECUTION_TIME_EDEFAULT == null ? executionTime != null : !EXECUTION_TIME_EDEFAULT.equals(executionTime);
            case GraphPackage.TASK__RESOURCES:
                return resources != null && !resources.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (executionTime: ");
        result.append(executionTime);
        result.append(')');
        return result.toString();
    }

} //TaskImpl
