/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.design;

import java.util.ArrayList;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import activity.ActivityFactory;
import activity.Move;
import activity.PeripheralAction;
import activity.SchedulingType;
import activity.SimpleAction;
import machine.ActionType;
import machine.Distance;
import machine.IResource;
import machine.Peripheral;
import machine.Profile;
import machine.SymbolicPosition;

public class AddPeripheralActionPage extends WizardPage {
    private enum MoveType {
        MoveTo("Point-to-point (stop at target position)"), MovePassing("On-the-fly (don't stop at target position)");

        private final String description;

        private MoveType(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    private final String initialActionName;

    private final AdapterFactory adapterFactory;

    private Peripheral peripheral;

    private final IResource initialResource;

    private Text textName;

    private Button btnAction;

    private Button btnMove;

    private ComboViewer comboAction;

    private ComboViewer comboPosition;

    private ComboViewer comboMoveType;

    private ComboViewer comboProfile;

    private ComboViewer comboSchedulingType;

    private ComboViewer comboResource;

    private Label lblType;

    public AddPeripheralActionPage(IResource resource, String initialActionName, AdapterFactory adapterFactory) {
        super(AddPeripheralActionPage.class.getSimpleName());
        setTitle("Action details");
        setDescription("Provide the details for the action to add");
        this.initialActionName = initialActionName;
        this.adapterFactory = adapterFactory;
        this.initialResource = resource;
        setPageComplete(false);
    }

    @Override
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NULL);
        setControl(container);
        container.setLayout(new GridLayout(2, false));

        Label lblName = new Label(container, SWT.NONE);
        lblName.setAlignment(SWT.RIGHT);
        lblName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblName.setText("Name:");

        textName = new Text(container, SWT.BORDER);
        textName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        textName.setText(initialActionName);

        btnAction = new Button(container, SWT.RADIO);
        btnAction.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        btnAction.setAlignment(SWT.RIGHT);
        btnAction.setSelection(true);
        btnAction.setText("Action:");
        btnAction.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                comboAction.getCombo().setEnabled(btnAction.getSelection());
                updatePageComplete();
            }

            @Override
            public void widgetSelected(SelectionEvent e) {
                comboAction.getCombo().setEnabled(btnAction.getSelection());
                updatePageComplete();
            }
        });

        comboAction = new ComboViewer(container, SWT.NONE);
        comboAction.setContentProvider(ArrayContentProvider.getInstance());
        comboAction.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
        comboAction.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboAction.getCombo().setEnabled(btnAction.getSelection());
        comboAction.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                updatePageComplete();
            }
        });

        btnMove = new Button(container, SWT.RADIO);
        btnMove.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        btnMove.setAlignment(SWT.RIGHT);

        btnMove.setText(
                "Move" + ((peripheral == null) ? "" : peripheral.getDistances().isEmpty() ? " to" : " for") + ':');
        btnMove.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                comboPosition.getCombo().setEnabled(btnMove.getSelection());
                comboMoveType.getCombo().setEnabled(btnMove.getSelection());
                comboProfile.getCombo().setEnabled(btnMove.getSelection());
                updatePageComplete();
            }

            @Override
            public void widgetSelected(SelectionEvent e) {
                comboPosition.getCombo().setEnabled(btnMove.getSelection());
                comboMoveType.getCombo().setEnabled(btnMove.getSelection());
                comboProfile.getCombo().setEnabled(btnMove.getSelection());
                updatePageComplete();
            }
        });

        comboPosition = new ComboViewer(container, SWT.NONE);
        comboPosition.setContentProvider(ArrayContentProvider.getInstance());
        comboPosition.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
        comboPosition.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboPosition.getCombo().setEnabled(btnMove.getSelection());
        comboPosition.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                updatePageComplete();
            }
        });

        lblType = new Label(container, SWT.NONE);
        lblType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblType.setText("Move type:");

        comboMoveType = new ComboViewer(container, SWT.NONE);
        comboMoveType.setContentProvider(ArrayContentProvider.getInstance());
        comboMoveType.setLabelProvider(new LabelProvider());
        comboMoveType.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboMoveType.getCombo().setEnabled(btnMove.getSelection());
        comboMoveType.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                updatePageComplete();
            }
        });
        comboMoveType.setInput(MoveType.values());

        Label lblProfile = new Label(container, SWT.NONE);
        lblProfile.setAlignment(SWT.RIGHT);
        lblProfile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblProfile.setText("Speed Profile:");

        comboProfile = new ComboViewer(container, SWT.NONE);
        comboProfile.setContentProvider(ArrayContentProvider.getInstance());
        comboProfile.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
        comboProfile.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboProfile.getCombo().setEnabled(btnMove.getSelection());
        comboProfile.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                updatePageComplete();
            }
        });

        Label lblSchedulingType = new Label(container, SWT.NONE);
        lblSchedulingType.setAlignment(SWT.RIGHT);
        lblSchedulingType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblSchedulingType.setText("Scheduling type:");

        comboSchedulingType = new ComboViewer(container, SWT.NONE);
        comboSchedulingType.setContentProvider(ArrayContentProvider.getInstance());
        comboSchedulingType.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
        comboSchedulingType.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboSchedulingType.setInput(SchedulingType.VALUES);
        comboSchedulingType.setSelection(new StructuredSelection(SchedulingType.ASAP));

        Label resourceLabel = new Label(container, SWT.NONE);
        resourceLabel.setAlignment(SWT.RIGHT);
        resourceLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        resourceLabel.setText("Resource:");

        comboResource = new ComboViewer(container, SWT.NONE);
        comboResource.setContentProvider(ArrayContentProvider.getInstance());
        comboResource.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
        comboResource.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboResource.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                updatePageComplete();
            }
        });
    }

    public void setPeripheral(Peripheral peripheral) {
        if (peripheral != this.peripheral) {
            this.peripheral = peripheral;
            this.comboAction.setInput(peripheral.getType().getActions());
            this.comboPosition.setInput(
                    peripheral.getDistances().isEmpty() ? peripheral.getPositions() : peripheral.getDistances());
            this.comboProfile.setInput(peripheral.getProfiles());
            java.util.List<String> resources = new ArrayList<>();
            if (!peripheral.getResource().getItems().isEmpty()) {
                resources.add("Select resource or resource item");
            }
            resources.add(peripheral.getResource().fqn());
            peripheral.getResource().getItems().forEach(i -> resources.add(i.fqn()));
            this.comboResource.setInput(resources.stream().toArray(String[]::new));
            String select = initialResource == null ? resources.get(0) : initialResource.fqn();
            this.comboResource.setSelection(new StructuredSelection(select));

            updatePageComplete();
        }
    }

    public PeripheralAction createPeripheralAction() {
        final PeripheralAction result;
        if (btnAction.getSelection() && !comboAction.getSelection().isEmpty()) {
            result = ActivityFactory.eINSTANCE.createSimpleAction();
            ((SimpleAction)result)
                    .setType((ActionType)((StructuredSelection)comboAction.getSelection()).getFirstElement());
        } else if (btnMove.getSelection() && !comboPosition.getSelection().isEmpty()
                && !comboMoveType.getSelection().isEmpty() && !comboProfile.getSelection().isEmpty())
        {
            MoveType selectedMoveType = (MoveType)((StructuredSelection)comboMoveType.getSelection()).getFirstElement();
            Move move = ActivityFactory.eINSTANCE.createMove();
            if (peripheral.getDistances().isEmpty()) {
                move.setTargetPosition(
                        (SymbolicPosition)((StructuredSelection)comboPosition.getSelection()).getFirstElement());
            } else {
                move.setDistance((Distance)((StructuredSelection)comboPosition.getSelection()).getFirstElement());
            }
            move.setStopAtTarget(MoveType.MoveTo == selectedMoveType);
            move.setProfile((Profile)((StructuredSelection)comboProfile.getSelection()).getFirstElement());
            result = move;
        } else {
            return null;
        }

        StructuredSelection sel = (StructuredSelection)comboSchedulingType.getSelection();
        SchedulingType selectedSchedulingType = (SchedulingType)sel.getFirstElement();

        StructuredSelection resourceSel = (StructuredSelection)comboResource.getSelection();
        String selectedResource = (String)resourceSel.getFirstElement();
        IResource resource = peripheral.getResource();
        for (IResource item: peripheral.getResource().getItems()) {
            if (item.fqn().equals(selectedResource)) {
                resource = item;
                break;
            }
        }

        result.setName(textName.getText());
        result.setSchedulingType(selectedSchedulingType);
        result.setResource(resource);
        result.setPeripheral(peripheral);
        return result;
    }

    private void updatePageComplete() {
        boolean pageComplete = true;
        pageComplete &= !textName.getText().isEmpty();

        boolean actionEnabled = comboAction.getCombo().getItemCount() > 0;
        boolean moveEnabled = comboPosition.getCombo().getItemCount() > 0 && comboProfile.getCombo().getItemCount() > 0;
        boolean resourceSet = comboResource.getCombo().getItemCount() == 1
                || comboResource.getCombo().getSelectionIndex() > 0;
        if (btnAction.getSelection() && !actionEnabled) {
            btnAction.setSelection(false);
            btnMove.setSelection(moveEnabled);
        } else if (btnMove.getSelection() && !moveEnabled) {
            btnMove.setSelection(false);
            btnAction.setSelection(actionEnabled);
        }

        if (actionEnabled && comboAction.getSelection().isEmpty()) {
            comboAction.getCombo().select(0);
        }
        btnAction.setEnabled(actionEnabled);
        comboAction.getCombo().setEnabled(btnAction.getSelection());

        pageComplete &= !btnAction.getSelection() || !comboAction.getSelection().isEmpty();

        pageComplete &= resourceSet;

        if (moveEnabled) {
            if (comboPosition.getSelection().isEmpty()) {
                comboPosition.getCombo().select(0);
            }
            if (comboMoveType.getSelection().isEmpty()) {
                comboMoveType.getCombo().select(0);
            }
            if (comboProfile.getSelection().isEmpty()) {
                comboProfile.getCombo().select(0);
            }
        }
        btnMove.setEnabled(moveEnabled);
        comboPosition.getCombo().setEnabled(btnMove.getSelection());
        pageComplete &= !btnMove.getSelection() || !comboPosition.getSelection().isEmpty();
        comboMoveType.getCombo().setEnabled(btnMove.getSelection());
        pageComplete &= !btnMove.getSelection() || !comboMoveType.getSelection().isEmpty();
        comboProfile.getCombo().setEnabled(btnMove.getSelection());
        pageComplete &= !btnMove.getSelection() || !comboProfile.getSelection().isEmpty();

        setPageComplete(pageComplete);
    }
}
